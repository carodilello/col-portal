$('.date').datepicker({
	language: 'es',
	todayHighlight: true
}); // Primero ejecutamos el date y luego el form

$('.form').validator({
	feedback:{
		error:'fa-times'
	}
}).on('submit', function (e){
	if (!e.isDefaultPrevented()){
		$('.submit, .submit_presupuesto').prop('disabled', true);
		$('.submit, .submit_presupuesto').addClass('disabled', true);

		if($(e.currentTarget).hasClass('ajax')){
			e.preventDefault();
			$.post($(e.currentTarget).prop('action'),$(e.currentTarget).serialize(),function(data){
				if(data){
					$('.modal-content').html('<div id="gracias" class="gracias-home"><div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3><strong>Gracias</strong>, El envio se ha realizado exitosamente!</h3></div></div>');
					setTimeout(function(){
						$('#enviar-amigo').modal('hide');
					}, 3000);
				}
            });
		}
	}
	window.setTimeout(function () {
		var errors = $('.has-error');
		if (errors.length) {
		   $(document).scrollTop(errors.offset().top - 45);
		}
	}, 300); // El valor debe ser mayor a 250 por el efecto ANIMATE de la libreria validator que hace el scrollTo
});

$('.submit_presupuesto').click(function(e){
	if($('.minisitios').length){
		if($('.minisitios:checked').length > 0){
			$('.error-zonas').hide();
		}else{
			$('.error-zonas').show();
			e.preventDefault();	
		}	
	}

	if($('.empresas_checkbox').length){
		if($('.empresas_checkbox:checked').length > 0){
			$('.error-empresas').hide();
		}else{
			$('.error-empresas').show();
			e.preventDefault();	
		}	
	}
});

$('.submit_derivador').click(function(e){
	if($('.require-one').length){
		if($('.require-one:checked').length > 0){
			$('#summary').hide();
		}else{
			$('#summary').show();
			e.preventDefault();	
		}	
	}
});

$('.minisitios').change(function(){
    if(this.checked) {
   		$('.error-zonas').hide();
    }else{
    	$('.error-zonas').show();
    }
});