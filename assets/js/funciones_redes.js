// FACEBOOK
$.ajaxSetup({ cache: true });
$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
    FB.init({
        appId  : '206856159344730',
        status : true,
        cookie : true,
        xfbml  : true,
        oauth  : true,
        version: 'v2.7'
    });
    
    $('.compartir_facebook').removeAttr('disabled');

    $('.compartir_facebook').click(function() {
        FB.ui({
            method: 'share',
            display: 'popup',
            href: $('.base_url').val() + $(this).siblings('.id_redes').val()
        }, function(response){});
    });
});

// TWITTER
function reward_user( event ) {
    return null;
}
window.twttr = (function (d,s,id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
    js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
    }(document, "script", "twitter-wjs"));

twttr.ready(function (twttr) {
    twttr.events.bind('tweet', reward_user);
});