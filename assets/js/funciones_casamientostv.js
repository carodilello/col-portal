$('#big_video_slider').on('slid.bs.carousel', function(e){
	var currentIndex = $('div.active').index() + 1;
	var video = videos[currentIndex - 1];
	var proveedores = '';
	var rubros = '';
	var tags = '';
	var base_url = $('.box').data('base-url');
	var base_url_subdomain = $('.base_url_subdomain').val();


	$('.box .titulo_box').html('<a href="' + base_url + 'casamientos-tv/' + video.titulo_seo + '_CO_v' + video.id + '">' + video.titulo + '</a>');
	$('.box .descripcion').html(video.descripcion);

	$(video.proveedores).each(function(index, el){
		var str = base_url_subdomain;
		var subdomain = str.replace("{?}", el[9]);
		if (el[0]) proveedores += '<a href="' + subdomain + el[8] + '">' + el[1] + '</a>, ';
	});

	$(video.rubros).each(function(index, el){
		if (el[0]) rubros += '<a href="' + base_url + el[5] + '/proveedor/' + el[4] + '_CO_r' + el[0] + '">' + el[1] + '</a>, ';
	});

	$(video.tags).each(function(index, el){
		if (el[1] && el[0] != '22898') tags += '<a href="' + base_url + 'casamientos-tv/' + el[2] + '_CO_t' + el[0] + '">' + el[1] + '</a>, ';
	});

	proveedores = proveedores.replace(/,\s*$/, "");
	$('.box .proveedor h3').html('<i class="fa fa-suitcase"></i>' + (proveedores ? proveedores : '---'));

	rubros = rubros.replace(/,\s*$/, "");
	$('.box .rubros').html('<i class="fa fa-calendar"></i>Rubros: ' + (rubros ? rubros : '---'));

	tags = tags.replace(/,\s*$/, "");
	$('.box .tags').html('<i class="fa fa-tag"></i>Tags: ' + (tags ? tags : '---'));

	if(tags){
		$('.box .tags').show();
	}else{
		$('.box .tags').hide();
	}

	$('.id_redes').val('casamientos-tv/' + video.titulo_seo + 'CO_v' + video.id + '/' + (video.video_prov == 1 ? 'e' : 'c'));
	$('.twitter').attr('href', 'https://twitter.com/intent/tweet?url=' + base_url + 'casamientos-tv/' + video.titulo_seo + 'CO_v' + video.id + '/' + (video.video_prov == 1 ? 'e' : 'c') + '&text=' + $(".desc_encoded_" + (currentIndex - 1)).val());

	$(this).find('iframe').each(function(i, e){
		yt_players['player' + i].pauseVideo();
	});
});

$('.change_seccion').change(function(){
	if($(this).val()){
		$('.col').hide(0);
		$('.col-'+$(this).val()).fadeIn(150);
	}else{
		$('.col').fadeIn(150);
	}
});
$('.change_seccion').trigger('change_seccion');

if($('.tag_span').length > 0){
	$('.tag_span').hide();
	if($('.tag_a').length < 1){
		$('.tags').hide();
	}
}

$('.select_secciones').change(function(){
	var url;
	if($(this).val()){
		url = $(".base_javascript").val() + '/' + $(this).children(':selected').data('url') + '_CO_ct' + $(this).val() + '#grid_galeria';
	}else{
		url = $(".base_javascript").val() + '#grid_galeria';
	}

	window.location.href = url;
});

$('.select_canales').change(function(){
	var url;

	if($(this).val()){
		url = $(".base_javascript").val() + '/' + (typeof $('.select_secciones').children(':selected').data('url') !== 'undefined' ? $('.select_secciones').children(':selected').data('url') : 'casamientos-tv') + '_CO_ct' + ($('.select_secciones').val() ? $('.select_secciones').val() : 0) + '_ch' + $(this).val() + '#grid_galeria';
	}else{
		url = $(".base_javascript").val() + '/' + (typeof $('.select_secciones').children(':selected').data('url') !== 'undefined' ? $('.select_secciones').children(':selected').data('url') : 'casamientos-tv') + '_CO_ct' + ($('.select_secciones').val() ? $('.select_secciones').val() : 0) + '#grid_galeria';
	}

	window.location.href = url;
});