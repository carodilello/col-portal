if(window.location.hash){
	setTimeout(function(){
		if($(window.location.hash).length) $('body').scrollTop($(window.location.hash).offset().top - 45);
	}, 1);
}

function isInt(n) {
   return n % 1 === 0;
}

function imgError(image) {
    image.onerror = "";
    image.src = $('.base_url').val() + 'assets/images/pic_default.jpg';
    return true;
}

$('p.proveedor').each(function(a,e){
	if(!$(e).children().length){
		$(e).remove();
	} 
});

/* GOOGLE MAPS */
//<![CDATA[
var gmap = {
	addMarkers: function(jsonData) {
		var bounds = new google.maps.LatLngBounds();
		for (var i=0; i<jsonData.markers.length; i++) {
			var point = new google.maps.LatLng(jsonData.markers[i].latitud, jsonData.markers[i].longitud);
			bounds.extend(point);
			gmap.createMarker(point, jsonData.markers[i].icontype, jsonData.markers[i].label, jsonData.markers[i].id_minisitio);
		}
		gmap.el.fitBounds(bounds);

		var clusterStyles = [{
		    textColor: '#59c5bd',
		    fontWeight: 'bold',
		    textSize: '16',
		    anchor:[0,0],
		    url: 'http://www.casamientosonline.com/images/gmap-pin-agrupado.png', //small
		    url: '/assets/images/pin_map_grupal.png', //small
		    height: 70,
		    width: 46,
		    lineHeight: 44
		  }
		];

		var mcOptions = {
	        styles: clusterStyles,
	        gridSize: 60
        };

		var markerCluster = new MarkerClusterer(gmap.el, gmap.markers, mcOptions);
	},
	center: function(){
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0, len = gmap.markers.length; i < len; i++) {
			bounds.extend(gmap.markers[i].getPosition());
		}
		gmap.el.fitBounds(bounds);
		return gmap;
	},
	createMarker: function (point,icontype,title,id_minisitio) {
		if (typeof icontype == 'undefined'){
		  	icontype = '';
	  	}

		var marker = new google.maps.Marker({
			position: point,
			icon: '/assets/images/pin_map'+icontype+'.png',
			title: title,
			map: gmap.el
		});
		if (typeof id_minisitio != 'undefined'){
			google.maps.event.addListener(marker, 'click', function() {
				if (typeof gmap.iw != 'undefined'){
					gmap.iw.close();
				}
				marker.setIcon(marker.getIcon().replace(/pin-([a-c])/,'pin-b'));
				gmap.iw = new google.maps.InfoWindow({
					content: 'Cargando...'
				});
	  			gmap.iw.open(gmap.el,marker);

	  			$.ajax({
					type: 'GET',
					url: '/ajax/get_mapas_proveedor/'+id_minisitio,
					success: function(html){
						gmap.iw.setContent(html);
						gmap.iw.open(gmap.el,marker);
					}
				});
			});
		}

		gmap.markers.push(marker);
		return gmap;

	},
	load: function(idx, elem){
		if($(elem).hasClass('street_view')){
			options = {
				position: new google.maps.LatLng(gmap.dom.data('latitude'),gmap.dom.data('longitude')),
				streetViewControl: false,
				scrollwheel: false,
				pov: {
		          heading: 0,
		          pitch: 0
		        }
			};
			
			gmap.dom = $(elem);
			
			if (gmap.dom.length){
				gmap.el = new google.maps.StreetViewPanorama(gmap.dom[0], options);
				setTimeout(function(){
					gmap.dom.children('div:eq(0)').children('div:eq(1),div:eq(2)').hide();
				},2000); // código para ocultar footer google maps
			}
		}else{
			var zoom = 11, 
	    	options = {
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				streetViewControl: false,
				scrollwheel: false,
			}, createMarker;
			gmap.dom = $(elem);
		    if ($('#js_id_rubro').length){
	    		options.zoom = 11;
				options.mapTypeControl = true;
				options.zoomControl = true;
				options.mapTypeControlOptions = {
					style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
				},
		        $.getJSON('/ajax/get_mapas_rubro/'+$('#js_id_rubro').text()+'/'+$('#js_filtros').text(),null,gmap.addMarkers);
		        options.disableDefaultUI = true;
		    }else{
		    	createMarker = 1;
	    		options.center = new google.maps.LatLng(gmap.dom.data('latitude'),gmap.dom.data('longitude'));
	    		options.zoom = 14;
				options.mapTypeControl = false;
		    }
		    if (gmap.dom.length){
				gmap.el = new google.maps.Map(gmap.dom[0], options);
				setTimeout(function(){
					gmap.dom.children('div:eq(0)').children('div:eq(1),div:eq(2)').hide();
				},2000); // código para ocultar footer google maps
			}
			if (createMarker){
				gmap.createMarker(options.center, '', $('#minisitio_title').text(), $('#id_minisitio').text());
			}	
		}
	},
	markers: [],
	resize: function(){
		google.maps.event.trigger(gmap.el, 'resize');
	}
}
var last_map;
$.fn.gmap = function(){
	last_map = this.each(gmap.load);
}
//]]>
/* GOOGLE MAPS */
$('[data-gmap]').gmap();

var last_data;
$("#blueimp-gallery").on('slide', function (event, index, slide){
	var el = false;

	var url = '';
	var url_minisitio = '';
	url = $('.item_gal').eq(index).children().data('url');
	url_seo = $('.item_gal').eq(index).children().data('urlminisitio');
	if(typeof url !== 'undefined'){
		el = url;
		url_minisitio = url_seo;
	}

	url = $('.item').eq(index).data('url'); 
	url_seo = $('.item').eq(index).data('urlminisitio'); 
	if(typeof url !== 'undefined'){
		el = url;
		url_minisitio = url_seo;
	}

	url = $('.foto-gallery').eq(index).children().children().data('url');
	url_seo = $('.foto-gallery').eq(index).children().children().data('urlminisitio');
	if(typeof url !== 'undefined'){
		el = url;
		url_minisitio = url_seo;
	}

	url = $('.item_minis').eq(index).data('url');
	url_seo = $('.item_minis').eq(index).data('urlminisitio');
	if(typeof url !== 'undefined'){
		el = url;
		url_minisitio = url_seo;
	}

	url = $('.item').eq(index).children().data('url');
	url_seo = $('.item').eq(index).children().data('urlminisitio');
	if(typeof url !== 'undefined'){
		el = url;
		url_minisitio = url_seo;
	}

	url = $('.item').eq(index + $('.item_video').length).children('a').data('url');
	url_seo = $('.item').eq(index + $('.item_video').length).children('a').data('urlminisitio');
	if(typeof url !== 'undefined'){
		el = url;
		url_minisitio = url_seo;
	}

	$(slide).find('.pedir-presupuesto').attr('href', el);
	$(slide).find('.link_minisitio_popup').attr('href', url_minisitio);

	var page = index / 20;
	
	if ((index > 0) && ((page) % 1 == 0)){
		$.ajax({
	        type: "POST",
	        dataType: 'json',
	        url: "/ajax/obtener_mas_imagenes/"+$('.id_rubro').val()+'/'+(page+1+($('.pag-galeria').val()-1))+'/'+index,
	        success: function(data){
	        	last_data = data;
	        	var gallery = $('#blueimp-gallery').data('gallery');
	        	gallery.add(data);
	        }
	    });
	}
	
	$.each(last_data,function(i,e){
		if(e.index[index]) $(slide).find('.pedir-presupuesto').attr('href', e.index[index]);
	});
});

$(".solapa").click(function(){ // Para guia rubros
	if($(this).data('id')){
		$(".item-rubro").hide();
		$(".padre_"+$(this).data('id')).show();
		$('.solapa').removeClass('active');
		$('.solapa_'+$(this).data('id')).addClass('active');
	}else{
		$(".item-rubro").show();
		$('.solapa').removeClass('active');
		$('.solapa_todos').addClass('active');
	}
	$('body').scrollTop($('#rubros').offset().top - 50);
});

$('.provincias').change(function(){
	if($(this).val()){
		$.ajax({
	        type: "POST",
	        data: { texto_por_defecto: $('.texto_por_defecto').val() },
	        url: "/ajax/get_localidad_from_provincia/"+$(this).val(),
	        success: function(data){
	        	$('.localidades').html(data);
	        }
	    });
	}
});

if($('.provincias').val() && !$('.localidades').val()) $('.provincias').trigger('change');

setTimeout(function(){
	$('.gracias-home').fadeOut(200);
}, 4000);
setTimeout(function(){
	$('#gracias').fadeOut(200);
}, 15000);

if($('.trigger-telefono-popup').val() == 1){
	$('.telefono').trigger('click');
}

$('[data-toggle="tooltip"]').tooltip();

$('.ver-mas-empresas').click(function(){
	if($('.empresa-container').hasClass('empresa-hidden')){
		$('.empresa-hidden').show(50).removeClass('empresa-hidden').addClass('empresa-showed');
		$('#listado_presupuesto_grupal').css('overflow', 'auto');
		$('.text-container').text('Ver menos');
		$('.empresas-restantes').hide();
		$('.text-container-restantes').hide();
		$(this).children('.fa').addClass('fa-minus-circle').removeClass('fa-plus-circle');
	}else{
		$('.empresa-showed').hide(50).removeClass('empresa-showed').addClass('empresa-hidden');
		$('#listado_presupuesto_grupal').css('overflow', 'hidden');
		$('.text-container').text('Ver las');
		$('.empresas-restantes').show();
		$('.text-container-restantes').show();
		$(this).children('.fa').addClass('fa-plus-circle').removeClass('fa-minus-circle');
	}
});

$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).fadeIn(50);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).fadeOut(50);
});

$('.vista-roadmap').click(function(){
	$('.roadmap').fadeIn(50);
	$('.street_view').hide(0);
	gmap.resize();
});
$('.vista-street_view').click(function(){
	$('.street_view').fadeIn(50).css('width','%50');
	$('.roadmap').hide(0);
	gmap.resize();
});

var _timer = 0;
function delayFunction(func, delay) {
    if (_timer) {
        window.clearTimeout(_timer);
    }
    _timer = window.setTimeout(function() {
        func.call();
    }, delay);
}

$(".input-buscar").bind('keyup', function(e){
	if(window.outerWidth >= 938){
		e.preventDefault();
	    var el = $(this).val();

	    if(el && el.length > 2 && e.which != 38 && e.which != 40){
	        delayFunction(function(){
	            $.ajax({
	                type: "POST",
	                url: "/ajax/sugerencias_buscador_home/"+el+"/"+$('.select-sucursal').val(),
	                success: function(data){
	                    if(data){
	                        $('.menu_busqueda_inner').html(data);
	                        $('.menu-sugerencias').hide();
	                        $('.menu-busqueda').show();
	                    }else{
	                        $('.menu-busqueda').hide();
	                    }
	                }
	            }); 
	        }, 500);
	    }
	}
});

// OVERLAY
$('.input-buscar').focusin(function(){
    $('.menu-sugerencias').show(0);
    var body = document.body,
        html = document.documentElement;
    var h = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
    $('.overlay-full').css('height',h);

    if(window.outerWidth >= 1280){
    	$('.overlay-full').fadeIn(200);
    }
    
    $('.menu_busqueda_inner').html('');
    $('.menu-busqueda').hide();
    $('.inner_buscador').css('z-index','1001');
});

$('.input-buscar').focusout(function(){
    $('.overlay-full').fadeOut(200);
    $('.menu-sugerencias').delay(100).fadeOut(50);
    $('.menu-busqueda').delay(100).fadeOut(50);
    delayFunction(function(){
        $('.inner_buscador').delay(100).css('z-index','1');
    }, 150);
});

$('.compartir_imprimir').click(function(){
	window.print();
});

$('.mailto-amigo').click(function(e){
	e.preventDefault();
	var link_href = $(this).attr('href');

	if(link_href){
		if($('#destinatario').val()){
			link_href = link_href.replace("{email}", $('#destinatario').val());
		}else{
			link_href = link_href.replace("{email}", '');
		}
		if($('#asunto').val()){
			link_href = link_href.replace("{subject}", $('#asunto').val());
		}else{
			link_href = link_href.replace("{subject}", '');
		}
		$(this).attr('href', link_href);
	}

	window.location.href = link_href;
	$('.modal').modal('hide');
});

$('.seguir_leyendo').click(function(){ // MINISITIO VER MAS
	if($(this).hasClass('open')){
		$(this).removeClass('open');
		$('#descripcion_proveedor').css('height', '400px');
		$('#gradient').show(0);
		$('.seguir_leyendo_texto').text('Ver más');
		$('.seguir_leyendo_icon').addClass('fa-chevron-down').removeClass('fa-chevron-up');
	}else{
		$(this).addClass('open');
		$('#descripcion_proveedor').css('height', '100%');
		$('#gradient').hide(0);
		$('.seguir_leyendo_texto').text('Ver menos');
		$('.seguir_leyendo_icon').addClass('fa-chevron-up').removeClass('fa-chevron-down');
	}
});

$('#modal-derivador').modal('show');

$('.change-sucursal').click(function(){
	if($(this).data('id')){
		$('.name-sucursal-ms').html($(this).text());
		$('.select-sucursal').val($(this).data('id'));

		$.ajax({
		    type: "GET",
		    url: "/ajax/get_menu_sugerencias/" + $('.select-sucursal option:selected').val(),
		    success: function(data){
		    	$('.sugerencias_container').html(data);
		    	$('#buscador').removeClass().addClass($('.sugerencias_container').children().data('class')).addClass('buscador_global');
		    	$('#header_search').removeClass().addClass($('.sugerencias_container').children().data('class')).addClass('buscador_global');
		    }
		});
	}
});

var descripcion_proveedor = $('#descripcion_proveedor');
var desc_height = descripcion_proveedor.css('height', '100%').height();
if(desc_height > 400){
	descripcion_proveedor.css('height', '400px');
	$("#box_ver_mas").show();
}else{
	$("#box_ver_mas").hide();
}

$('.galeria_tipo').change(function(){
	if($(this).val() == 1){
		$('.galeria_subseccion').show();
		$('.galeria_seccion').show();
		$('.galeria_sucursal').hide();
		$('.galeria_rubro').hide();
	}else{
		if($(this).val() == 2){
			$('.galeria_sucursal').show();
			$('.galeria_rubro').show();
			$('.galeria_subseccion').hide();
			$('.galeria_seccion').hide();	
		}else{
			$('.galeria_sucursal').hide();
			$('.galeria_rubro').hide();
			$('.galeria_subseccion').hide();
			$('.galeria_seccion').hide();
		}
	}
});
$('.galeria_tipo').trigger('change');

$('.galeria_seccion').change(function(){
	if($(this).val() == 98 || $(this).val() == 1055){
		var param = $('.base_url').val() + 'fotos-casamiento/' + $(".galeria_tipo").children(':selected').data('name') + '/' +  $(".galeria_seccion").children(':selected').data('name') + '_CO_t' + $(".galeria_tipo").val() + '_sc' + $(".galeria_seccion").val() + '#galeria_subseccion';
		if($(this).parent().hasClass('filtros')){ // NOTAS
			var param = $('.base_url').val() + 'listado-de-consejos-' + $(".galeria_seccion").children(':selected').data('name') + '_CO_cn' + $(".galeria_seccion").val();
		}

		window.location.href = param;
	}else{
		$.ajax({
			type: 'POST',
			url: '/ajax/get_secciones/' + $(this).val() + '/1',
			success: function(data){
				$('.galeria_subseccion').html(data);
				$('.galeria_subseccion').show();
			}
		});	
	}
});

$('.galeria_subseccion').change(function(){
	var param = $('.base_url').val() + 'fotos-casamiento/' + $(".galeria_tipo").children(':selected').data('name') + '/' +  $(".galeria_subseccion").children(':selected').data('name') + '_CO_t' + $(".galeria_tipo").val() + '_sc' + $(".galeria_seccion").val() + '_sb' + $(".galeria_subseccion").val() + '#galeria_subseccion';
	if($(this).parent().hasClass('filtros')){ // NOTAS
		var param = $('.base_url').val() + 'listado-de-consejos-' + $(".galeria_seccion").children(':selected').data('name') + '/' + $(this).children(':selected').data('name') + '_CO_cn' + $(".galeria_seccion").val() + '_s' + $(this).val() + '#galeria_subseccion';
	}

	window.location.href = param;
});

$('.sel_sucursal').change(function(){
	$.ajax({
		type: 'POST',
		data: { activo: $('.rubro_activo').val() },
		url: '/ajax/get_rubro/' + $(this).val(),
		success: function(data){
			$('.sel_rubro').html(data);
		}
	});
});

$('.galeria_sucursal').change(function(){
	$.ajax({
		type: 'POST',
		data: { activo: $('.rubro_activo').val() },
		url: '/ajax/get_rubro/' + $(this).val(),
		success: function(data){
			$('.galeria_rubro').html(data);
		}
	});
});

$('.galeria_rubro').change(function(){
	var param = $('.base_url').val() + 'fotos-casamiento/' + $(".galeria_sucursal").children(':selected').data('name') + '/' + $(".galeria_rubro").find(':selected').data('name') + '_CO_t' + $(".galeria_tipo").val() + '_su' + $(".galeria_sucursal").val() + '_r' + $(this).val() + '#galeria_rubro';
	window.location.href = param;
});

$('#carousel-minisitio').on('slid.bs.carousel', function (e) {
	var currentIndex = $('.align_c.active').index() + 1;

	$(this).find('iframe').each(function(i, e){
		if(flag){
			yt_players['player' + i].pauseVideo();
		}
	});

	$("#carousel-thumbs").carousel(Math.trunc((currentIndex - 1) / ( $('.cant_thumbs').val() ? $('.cant_thumbs').val() : 6 )));
});

$('.mostrar_tags').click(function(){
	if($('.can_hide').hasClass('hidden')){
		$('.can_hide').show(10).removeClass('hidden').addClass('showed');
		$(this).text('Ver menos');
	}else if($('.can_hide').hasClass('showed')){
		$('.can_hide').hide(10).removeClass('showed').addClass('hidden');
		$(this).text('Ver más');
	}
});

$('.volver').click(function(){
	$('#modal-derivador').modal('hide');
});

$('.push').css('height', $('.push object embed').height());
function tam_banner(alto){
	$('.push, .push object').css('height', alto+'px');
	$('.push object embed').attr('height', alto);
}

$('.thumb_c').click(function(){
	if($('#galeria_container').length > 0) $('body').scrollTop($('#galeria_container').offset().top - 45);
});

$('.jornada_tipo').change(function(){
	if($(this).val() == 'empresa'){
		$('.datos_casamiento').hide(100);
		$('.datos_casamiento').find('input').removeAttr('required');
		$('.datos_casamiento').find('select').removeAttr('required');
		$('.datos_empresa').show(100);
		$('.datos_empresa').find('input').prop('required', true);
		$('.datos_empresa').find('select').prop('required', true);
		$('.id_evento').val($('.id_evento_empresa').val());
		$('.invitados').prop('placeholder', 'Acompañantes');
	}else{
		$('.datos_casamiento').show(100);
		$('.datos_casamiento').find('input').prop('required', true);
		$('.datos_casamiento').find('select').prop('required', true);
		$('.datos_empresa').hide(100);
		$('.datos_empresa').find('input').removeAttr('required');
		$('.datos_empresa').find('select').removeAttr('required');
		$('.id_evento').val($('.id_evento_novios').val());
		$('.invitados').prop('placeholder', 'Cantidad de Invitados');
	}
	$('.form').validator('update');
});
$('.jornada_tipo').trigger('change');

$('#buscador, #header_search').keydown(function(e) {
	switch(e.which) {
    	case 13:
    		e.preventDefault();
    		if($('#menu_busqueda_inner .active').length > 0){
    			window.location.href = $('#menu_busqueda_inner .active').attr('href');
    		}else{
    			$('.submit').trigger('click');
    		}
    	break;

        case 38: // up
        	if($('#menu_busqueda_inner').html()){
        		if($('#menu_busqueda_inner .active').length < 1){
        			$('.sug').last().addClass('active');
        		}else{
        			$('#menu_busqueda_inner .active').removeClass('active').prev().addClass('active');
        			if($('#menu_busqueda_inner .active').length < 1){
        				$('.sug').last().addClass('active');	
        			}
        		}
        	}
        break;

        case 40: // down
        	if($('#menu_busqueda_inner').html()){
        		if($('#menu_busqueda_inner .active').length < 1){
        			$('.sug').first().addClass('active');
        		}else{
        			$('#menu_busqueda_inner .active').removeClass('active').next().addClass('active');
        			if($('#menu_busqueda_inner .active').length < 1){
        				$('.sug').first().addClass('active');	
        			}
        		}
        	}
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});

$('.ver_mas').click(function(){
	$('#testimonios_jornada').children('.testimonios_container').height($('#testimonios_jornada').find('ul').height());
});

$('.anio_infonovias').change(function(){
	window.location.href = '/infonovias-' + $(this).val();
});

$('.ubicacion_registro_civil').change(function(){
	switch ($(this).val()){
	    case 'caba':
	    	$('.directorio_cont').hide();
	        $('.caba').fadeIn(100);
	        break;
	    case 'bsas':
	    	$('.directorio_cont').hide();
	        $('.bsas').fadeIn(100);
	        break;
	    case 'pais':
	    	$('.directorio_cont').hide();
	        $('.pais').fadeIn(100);
	        break;
	}
});
$('.ubicacion_registro_civil').trigger('change');

$('.ubicacion_datos_utiles').change(function(){
	switch ($(this).val()){
	    case 'caba':
	    	$('.datos_utiles_cont').hide();
	        $('.caba').fadeIn(100);
	        break;
	    case 'bsas':
	    	$('.datos_utiles_cont').hide();
	        $('.bsas').fadeIn(100);
	        break;
	    case 'sf':
	    	$('.datos_utiles_cont').hide();
	        $('.santa_fe').fadeIn(100);
	        break;
	    case 'ros':
	    	$('.datos_utiles_cont').hide();
	        $('.rosario').fadeIn(100);
	        break;
	}
});
$('.ubicacion_datos_utiles').trigger('change');

$('.ubicacion_iglesias').change(function(){
	if(!$(this).val()){
		$('.directorio_cont tbody tr').fadeIn(150);
	}else{
		$('.directorio_cont tbody tr').hide();
		$('.directorio_cont tbody .tr_' + $(this).val()).fadeIn(150);
	}
});
$('.ubicacion_iglesias').trigger('change');

/* funciones responsive para los carruseles */
function get_responsive_data(cols){
	$('.carousel_responsive').each(function(index, el){
		var vars = Array();

		if (typeof $(el).data("vars") != 'undefined'){
		  	var parts = $(el).data("vars").split(',');
		  	vars = parts;
	  	}

		if($(el).data('content')){
			$.ajax({
				type: 'GET',
				url: '/ajax/get_carousel_responsive/' + $(el).data("content") + '/' + cols + '/' + vars[0] + '/' + vars[1],
				success: function(res){
					$(el).find('.carousel-inner').html(res);
				}
			});	
		}
	});
}

var flag = 0;
var flag_2 = 0;
var notas_href = '';
var el_nav = $('.nav-images').find("a span:contains('Todas las Notas')").closest('.a_parent');
$(window).resize(function(){
	if(window.outerWidth <= 480 && flag != 1){
		get_responsive_data(1);
		$('.seo_s').hide();
		$('.seo_n').show();
		flag = 1;
	}
	if(window.outerWidth > 480 && window.outerWidth <= 992 && flag != 2){
		get_responsive_data(2);
		$('.seo_s').hide();
		$('.seo_n').show();
		flag = 2;
	}
	if(window.outerWidth > 992 && flag != 3){
		get_responsive_data(4);
		$('.seo_s').show();
		$('.seo_n').hide();
		flag = 3;
	}
	if(window.outerWidth <= 1170 && flag_2 != 1){
		$('.nav .dropdown').each(function(index, el){
			if($(el).find('.submenu').length > 0){
				$(el).find('.dropdown-toggle').attr('href','#');
			}
		});
		notas_href = el_nav.attr('href');
		el_nav.attr('href', el_nav.closest('.submenu').siblings('a').data('url'));
		flag_2 = 1;
	}
	if(window.outerWidth > 1170 && flag_2 != 2){
		$('.dropdown-toggle').each(function(index, el){
			$(el).attr('href', $(el).data('url'));
		});
		if(notas_href){
			el_nav.attr('href', notas_href);
		}
		flag_2 = 2;
	}
	if(window.outerWidth < 1280){
		$('.sugerencias_container').hide();
		$('#menu_sugerencias_wrapper').hide();
		$('.overlay-full').fadeOut(200);
	}else{
		$('.sugerencias_container').show();
	}
});
if(window.outerWidth <= 1170) $(window).trigger('resize');
/* FIN funciones responsive para los carruseles */

if(window.outerWidth < 1280){
	$('.sugerencias_container').hide();
}

$('.button-search').click(function(){
	if($(this).closest('form').find('.input-buscar').val()){
		var form = $(this).closest('form');
		window.location.href = form.attr('action') + form.find('.select-sucursal').val() + '/' + $(this).closest('form').find('.tipo').val() + '/' + $(this).closest('form').find('.input-buscar').val();
	}
});

$('.cambiar_tipo_jornada').change(function(){
	var d = new Date();
	var n = d.getFullYear();
	if($(this).val() == 1){
		window.location.href = $('.base_url').val() + 'eventos/expo-novias-' + n + '/registro/novios';
	}else{
		window.location.href = $('.base_url').val() + 'eventos/expo-novias-' + n + '/registro/empresas';		
	}
});

$('#minisitio_acciones a').click(function(){
	$.ajax({
		url: '/ajax/estadistica/' + $(this).data('id-minisitio') + '/' + $(this).data('idtipo'),
		type: 'GET'
	});
});