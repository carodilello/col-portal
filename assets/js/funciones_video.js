var flag = false;
var yt_int, yt_players={},
initYT = function() {
    $(".ytplayer").each(function() {
        yt_players[this.id] = new YT.Player(this.id);
    });
};

$.getScript("//www.youtube.com/player_api", function() {
    yt_int = setInterval(function(){
        if(typeof YT === "object"){
            initYT();
            clearInterval(yt_int);
            flag = true;
        }
    }, 1000);
});