var bg = $('.preload').css('background-image');
bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");

$('.preload_back').attr('src', bg).load(function(){
   $(this).remove(); // prevent memory leaks as @benweet suggested
   $('.preload').css('background-image', 'url(' + bg + ')');
   $('#slides').show().css('visibility','visible');
});