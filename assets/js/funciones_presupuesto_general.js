function validarpresupuestoRubros(){
  	var checkboxSelecionados = $('input:checkbox:checked.check-rubros').map(function () { 
		return this.value; 
	}).get();
	
	checkboxSelecionados =  checkboxSelecionados.toString();
	
	if(checkboxSelecionados == ""){
		$('#txt-rubros').show();
		setTimeout(function(){
			$('body').scrollTop($('#txt-rubros').offset().top - 45);
		}, 350);
	}else{
		$('#txt-rubros').hide();
	}
	
	return checkboxSelecionados;
}

function rubroCheck(id, rubro){
	var h_rubros = $('#rubrosID').val();
	
	if(document.getElementById(id).checked){
		var coma = ",";
		if(h_rubros == ""){
			coma = "";
		}
		h_rubros+= coma+rubro;
	}
	else{
		var v_rubros = h_rubros.split(',');
		var coma = "";
		var h_rubros = "";
		for(i=0; i<v_rubros.length; i++){
			if(v_rubros[i] != rubro){
			h_rubros+= coma+v_rubros[i];
				coma = ",";	
			}
		}
	}
	$('#rubrosID').val(h_rubros);
	
	var rubros = h_rubros.split(',');
	
	//-- Item SALON --// 
	validSalon(rubros);			
	//-- Item PAQUETES --// 
	organizaPaquetes(rubros);
}

function validSalon(rubros){
	var siSalon = new Array(32,5,7,31,46,44,45,46,49,60,61,62,71,156,174,179,182,245,249,257,279);
	var noSalon = new Array(30,17,81,47,63,154,176,155,252,253,280);
	var ocultar = true;
	
	for(i=0; i<rubros.length; i++){
		if(jQuery.inArray(parseInt(rubros[i]), siSalon) >= 0){
			ocultar = false;
		}
		else if(jQuery.inArray(parseInt(rubros[i]), noSalon) >= 0){
			ocultar = true;
			i = rubros.length;
		}
	}
	
	if(ocultar == true){
		//$('.hijo_34, .hijo_35, .hijo2_39').attr("disabled", true);
		$('#padre_33').hide();
		$('.hijo_34, .hijo_35').attr("checked", false); 
		$('.hijo2_39').val();
	}
	else{
		//$('.hijo_34, .hijo_35').attr("disabled", false);
		$('#padre_33').show();
	}
}

function getSalones(id, valores){ 
	$("#"+id).autocomplete({ 
	    source: function(request, response) { 
	        $.ajax({ 
	            url: '/ajax/accion_ajax_get_salones/',
	            data: {
					valores:valores
				},
				type: 'post',
	            dataType: "json", 
	            success: function( data ) { 
	                response(data); 
	            } 
	        }); 
	    } 
	}); 
}

function organizaPaquetes(rubros){
	validCheckPaquetes(true);
	if(rubros != ""){
		$.ajax({ 
	        url: '/ajax/accion_organiza_paquetes/',
	        data: {
				rubros:rubros
			},
			type: 'post',
	        success: function(data){
	        	if(data){
	           		validCheckPaquetes(false);
	          	}
	        } 
	    });
    }
}

function validCheckPaquetes(ocultar){
	$('.hijo_37').val(1);
   	$('.hijo_38').val(0);
	
	if(ocultar == true){
		//$('.hijo_37, .hijo_38').attr("disabled", true);
		$('#padre_36').hide();
		$('.hijo_38').attr("checked", false);
		$('.hijo_37').attr("checked", false);
	}
	else{
		//$('.hijo_37, .hijo_38').attr("disabled", false);
		$('#padre_36').show();
		$('.hijo_38').attr("checked", false);
		$('.hijo_37').attr("checked", true);
	}
}


function activarRubros(id_zona){
	$('.rubrosBA').html('');
	$('#loading').removeClass('none');
	$('#rubrosID').val('');
	
	$.ajax({ 
        url: '/ajax/accion_get_rubros/',
        data: {
			id_zona:id_zona
		},
		type: 'post',
        success: function(data) {
            var rubros = jQuery.parseJSON(data);
           	for(i=0; i < rubros.length; i++){
              	$('.rubrosBA').append('<div class="form-group has-feedback col-md-3"><div class="checkbox check_form"><label for="rubro-'+rubros[i].id+'"><input type="checkbox" name="rubros[]" value="'+rubros[i].id+'" id="rubro-'+rubros[i].id+'" onchange="javascript:rubroCheck(this.id, this.value)" class="check-rubros" required />'+rubros[i].rubro+'</label></div></div>');
            }
            $('#loading').addClass('none');
        } 
    }); 
}

$('.date').datepicker({
	language: 'es',
	todayHighlight: true
}); // Primero ejecutamos el date y luego el form

$('#form_presupuesto_general').validator({
	feedback:{
		error:'fa-times'
	}
}).on('submit', function (e) {
	checkboxSelecionados = validarpresupuestoRubros();
	if(checkboxSelecionados == ""){
		window.location.href = '#form_presupuesto_general';
		return false;
	}else{
		if (!e.isDefaultPrevented()){
			$('.submit').prop('disabled', true);
		}
	}
});

var rubros = new Array(0);
validSalon(rubros);
organizaPaquetes(rubros);
activarRubros($('.id_zona').val());