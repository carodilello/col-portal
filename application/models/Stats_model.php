<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stats_model extends CI_Model {
	public $id;
	public $referencia;
	public $resultados;
	public $medir;

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function stats($id=null){
		$this->id = $id;
		$this->referencia = 1;
		$this->resultados = 0;
		$this->medir = $this->config->item('id_sucursal');
	}
	
	public function add($data){
		//mido impresion
		if ($this->medir){
			if (!empty($data['id_rubro']) && isset($data['id_origen']) && $data['id_origen'] >= 0){
				$this->db->query('INSERT INTO stats_rubros_views (id_rubro, id_origen, id_user, ip, url, referida) VALUES ('.$data['id_rubro'].', '.$data['id_origen'].', '.$data['id_user'].', "'.$data['ip'].'", "'.$data['url'].'", "'.$data['referida'].'");');
			} elseif (!empty($data['id_proveedor'])){
				$this->db->query('INSERT INTO stats_proveedores_views (id_proveedor, id_user, ip, url, referida) VALUES ('.$data['id_proveedor'].', '.$data['id_user'].', "'.$data['ip'].'", "'.$data['url'].'", "'.$data['referida'].'");');
			} elseif (!empty($data['id_nota'])){
				$this->db->query('INSERT INTO stats_notas_views (id_nota, id_user, ip, url, referida) VALUES ('.$data['id_nota'].', '.$data['id_user'].', "'.$data['ip'].'", "'.$data['url'].'", "'.$data['referida'].'");');
			}
		}
	}
}