<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubros_model extends CI_Model {
	private $rubros_padre = array();
	public $solapas = array();

	public function __construct(){
		parent::__construct();
		$this->load->helper('common_helper');
	}

	public function get_listado_rubros_guia($id_sucursal = 1, $separar = 0, $lado = 1, $orden = "r2.orden, r1.rubro", $combo = 0, $solo_promo = 0){
		$this->load->library('parseo_library');

		$and = ($separar)? (($lado == 1)? ' AND r2.posicion < 11' : 'AND r2.posicion > 10') : '';
		$select = 'r1.id id_rubro, r1.rubro, r2.id id_padre, r2.rubro padre, COUNT(DISTINCT p.id_proveedor) total, r2.orden, r1.imagen, nn.href';
		if ($combo){
			$select = 'r1.id id_rubro, r1.rubro';
		}
		if ($solo_promo){
			$and .= ' AND p.id_proveedor IN (SELECT DISTINCT id_proveedor FROM site_productos_activos WHERE promocion = 1) ';
		}
		$sql = 'SELECT ' . $select . '
				FROM prov_rubros r1
				LEFT JOIN prov_rubros r2 ON r1.padre = r2.id
				LEFT JOIN new_nav nn ON nn.id_rubro = r1.id AND nn.nivel = 2
				JOIN site_proveedores_activos p ON r1.id = p.id_rubro
				WHERE r1.id_sucursal = ?
				AND r1.padre IS NOT NULL
				' . $and . '
				GROUP BY r1.id
				ORDER BY ' . $orden;

		$query = $this->db->query($sql, array('id_sucursal' => $id_sucursal));

		$listado = array();

		if($combo){
			foreach($query->result_array() as $row){
			    $listado[$row["id_rubro"]] = $row["rubro"];
			}
		}else{
			$array = array(); 
			$listado = $query->result_array();
			if($listado) foreach ($listado as $k => $row){
				$array[$k] = $row;
				$array[$k]['url'] = $this->parseo_library->clean_url($row['rubro']);
				$this->solapas[$row['id_padre']] = $row['padre'];
			}
			$listado = $array;
		}

		return $listado;
	}

	public function get_rubro($id_rubro){
		$sql = 'SELECT r.*, s.sucursal
						FROM prov_rubros r
						JOIN sys_sucursales s ON r.id_sucursal = s.id
						WHERE r.id = ?';

		$query = $this->db->query($sql, array($id_rubro));

		return $query->result_array();
	}

	public function get_rubroid($rubro, $idsucursal){
		
		$sql = "SELECT r.id FROM prov_rubros r
				WHERE r.padre IS NOT NULL and r.rubro = '".$rubro."' and r.id_sucursal = ".$idsucursal." AND r.id in ( select id_rubro from site_proveedores_activos )";

		$query = $this->db->query($sql);
		return $query->result_array();
		
	}

	public function get_pyp_rubroid($rubro, $idsucursal,$tipo='productos'){

 		switch ($tipo){
            case 'productos':		
				$sql = "SELECT r.id FROM prov_rubros r
					WHERE r.padre IS NOT NULL and r.rubro = '".$rubro."' and 
					r.id_sucursal = ".$idsucursal." AND r.id in ( select id_rubro 
					from site_productos_activos where promocion = 0 )";
				break;
			case 'promociones':
				$sql = "SELECT r.id FROM prov_rubros r
					WHERE r.padre IS NOT NULL and r.rubro = '".$rubro."' and 
					r.id_sucursal = ".$idsucursal." AND r.id in ( select id_rubro 
					from site_productos_activos where promocion = 1 )";
				break;
			case 'paquetes':
				$sql = "SELECT r.id FROM prov_rubros r
					WHERE r.padre IS NOT NULL and r.rubro = '".$rubro."' and 
					r.id_sucursal = ".$idsucursal." AND r.id in ( select id_rubro 
					from site_paquetes_activos )";
				break;
			}
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_rubro_filtros_pendientes($id_rubro, $filtros = '', $solo_mapa = 0){
		$this->load->library('config_library');
		$and = '';

		if (!empty($filtros)){
			//proceso las opciones seleccionadas
			//$opciones = doubleExplode('|', '-', $filtros);

			$opciones = $this->config_library->get_filtros_aplicados($filtros, 1, $id_rubro);
			
			$ids = NULL;
			$exc = NULL;
			foreach ($opciones as $o){
				$ids .= $o['id_opcion'].',';
				if ($o['excluyente'] == 1){
						$exc .= $o['id_filtro'].',';
				}
			}
			
			if ($ids){
				$provs = $this->config_library->get_proveedores_library($id_rubro,'proveedor',0,0,$filtros,$solo_mapa==1?2:0);

				$ids_ms = array();
				foreach($provs as $p){
					array_push($ids_ms,$p['id_minisitio']);
				}

				$and = ' AND f.id_opcion NOT IN ('.substr($ids,0,-1).') ';

				if($ids_ms){
					$and .= ' AND f.id_minisitio IN ('.implode(',',array_unique($ids_ms)).')';	
				}
							 

				if (isset($exc)&&$exc){
					$and .= ' AND f.id_filtro NOT IN ('.substr($exc,0,-1).') ';
				}
			}
		}

		
		$join = ($solo_mapa == 0)? '' : ' JOIN site_proveedores_activos p ON f.id_minisitio = p.id_minisitio AND p.latitud IS NOT NULL ';

		$sql = 'SELECT f.id_filtro, f.filtro, f.id_opcion, f.opcion, COUNT(DISTINCT f.id_minisitio) total
						FROM site_proveedores_activos_filtros f
						'.$join.'
						WHERE f.id_rubro = '.$id_rubro.'
						'.$and.'
						GROUP BY f.id_opcion
						ORDER BY f.orden_filtro, f.filtro, f.orden_opcion';

		$query = $this->db->query($sql);
		$res = $query->result_array();
		$ret = array();
		$i = -1;
		$id = 0;
		foreach($res as $r){
			if ($id != $r['id_filtro']){
				$id = $r['id_filtro'];
				$i++;
				$j=0;
			}
			$ret[$i]['id'] = $r['id_filtro'];
			$ret[$i]['filtro'] = $r['filtro'];
			$ret[$i]['opciones'][$j]['id'] = $r['id_opcion'];
			$ret[$i]['opciones'][$j]['opcion'] = $r['opcion'];
			$ret[$i]['opciones'][$j]['total'] = $r['total'];
			$j++;
		}
		
		return $ret;
	}

	public function get_rubro_solapas($id_rubro){
		if(!$id_rubro) return NULL;

		$query = $this->db->query('SELECT COUNT(*) cant FROM site_productos_activos WHERE id_rubro = ' . $this->db->escape($id_rubro).' AND promocion = 0');
		$ret['productos'] = $query->row_array();
		$query = $this->db->query('SELECT COUNT(*) cant FROM site_productos_activos WHERE id_rubro = ' . $this->db->escape($id_rubro).' AND promocion = 1');
		$ret['promociones'] = $query->row_array();
		$query = $this->db->query('SELECT COUNT(*) cant FROM site_proveedores_activos WHERE id_rubro = ' . $this->db->escape($id_rubro).' AND latitud IS NOT NULL');
		$ret['mapas'] = $query->row_array();
		$query = $this->db->query('SELECT COUNT(*) cant FROM site_paquetes_activos WHERE id_rubro = ' . $this->db->escape($id_rubro));
		$ret['paquetes'] = $query->row_array();
		$query = $this->db->query('SELECT SUM(cupones) cant FROM site_proveedores_activos WHERE id_rubro = ' . $this->db->escape($id_rubro));
		$ret['cupones'] = $query->row_array();
		return $ret;
	}

	public function get_rubros_padre($id_sucursal){
		if(!$id_sucursal) return NULL;

		$sql = 'SELECT *
				FROM prov_rubros
				WHERE padre IS NULL AND id_sucursal = ' . $id_sucursal . ' AND estado > 1
				ORDER BY posicion';

		$query = $this->db->query($sql);
		
		$this->rubros_padre = $query->result_array();

		return $this->rubros_padre;
	}

	public function opcion_fecha_variable($id_rubro = NULL){
		if(empty($id_rubro)) return FALSE;

		$sql = 'SELECT COUNT(id_rubro) cant FROM prov_rubros_fecha_variable WHERE id_rubro = '.$id_rubro;

		$query = $this->db->query($sql);
		$cant = $query->row_array();

		if(isset($cant)) $cant = $cant['cant'];

		if($cant > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function get_pyp_rubros($id_sucursal = 1, $promocion = NULL, $id_grupo = NULL, $table = 'site_productos_activos'){
		$this->load->library('parseo_library');

		$sql = 'SELECT DISTINCT p.id_rubro, p.rubro '.($id_grupo?', nn.id_ref':'').'
						FROM ' . $table . ' p
						'.($id_grupo?'LEFT JOIN new_nav nn ON nn.id_rubro = p.id_rubro AND nn.nivel = 2':'').'
						WHERE p.id_sucursal = ' . $id_sucursal;
		
		if($promocion !== NULL) $sql .= ' AND p.promocion = ' . $promocion;
		if($id_grupo != NULL) $sql .= ' AND nn.id_ref = '.$id_grupo;
		
		$sql .= ' ORDER BY p.rubro';

		$query = $this->db->query($sql);

		$ret = $query->result_array();
		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			if($el['rubro']) $arr[$k]['rubro_seo'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		return $ret;
	}

	public function get_form_zonas_sucursal($id_zona){
		$sql = "SELECT DISTINCT(r.id) as id, r.rubro
		FROM prov_rubros r
		INNER JOIN site_proveedores_activos activos ON activos.id_rubro = r.id
		INNER JOIN form_gral_zonas_rel_sucursal z ON r.id_sucursal = z.id_sucursal
		WHERE z.id_zona = ".(int) $id_zona." AND r.estado = 2
		ORDER BY r.rubro";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function guardar_grupos($grupos = array(), $id_sucursal, $equivalencias){
		if(!$id_sucursal || !$equivalencias) return NULL;

		$equivalencias_str = implode(',', $equivalencias);

		$sql = " UPDATE new_nav SET se_muestra = 0 WHERE nivel = 2 AND id_ref IN (".$equivalencias_str.") AND imagen IS NULL ";

		$query = $this->db->query($sql);

		foreach ($grupos as $k => $grupo){
			foreach ($grupo as $j => $elem){
				$arr = array(
					'id_sucursal' => $id_sucursal,
					'id_estado'	  => 1,
					'id_rubro'	  => $elem['id_rubro'],
					'nombre'	  => $elem['rubro'],
					'nivel'		  => 2,
					'id_ref'	  => $k
				);

				$sql = "INSERT INTO new_nav (id_sucursal, id_estado, id_rubro, nombre, nivel, id_ref, se_muestra) 
					VALUES ('".$arr['id_sucursal']."', '".$arr['id_estado']."', '".$arr['id_rubro']."', '".$arr['nombre']."', '".$arr['nivel']."', '".$arr['id_ref']."', 1) ON DUPLICATE KEY 
					UPDATE id_sucursal = '".$arr['id_sucursal']."', id_estado = '".$arr['id_estado']."', id_rubro = '".$arr['id_rubro']."', nombre = '".$arr['nombre']."', nivel = '".$arr['nivel']."', id_ref = '".$arr['id_ref']."', se_muestra = '1'";

				$query = $this->db->query($sql);
			}
		}
	}

	public function get_grupo($id){
		$sql = "SELECT * FROM new_nav WHERE id = ".$this->db->escape($id);

		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_grupoid($grupo, $idsucursal,$tipo='productos'){

 		switch ($tipo){
            case 'productos':		
				$sql = "SELECT DISTINCT n.id_ref from new_nav as n where id_rubro is not null 
				and id_ref in ( SELECT r.id FROM new_nav r WHERE r.nombre = '".$grupo."' and r.id_sucursal = ".$idsucursal." )
				and id_rubro in ( select id_rubro from site_productos_activos where promocion = 0 ) ";
				break;
			case 'promociones':
				$sql = "SELECT DISTINCT n.id_ref from new_nav as n where id_rubro is not null 
				and id_ref in ( SELECT r.id FROM new_nav r WHERE r.nombre = '".$grupo."' and r.id_sucursal = ".$idsucursal." )
				and id_rubro in ( select id_rubro from site_productos_activos where promocion = 1 ) ";
				break;
			case 'paquetes':
				$sql = "SELECT DISTINCT n.id_ref from new_nav as n where id_rubro is not null 
				and id_ref in ( SELECT r.id FROM new_nav r WHERE r.nombre = '".$grupo."' and r.id_sucursal = ".$idsucursal." )
				and id_rubro in ( select id_rubro from site_paquetes_activos ) ";
				break;
			}
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_localidad($id_localidad){
		$sql = "SELECT l.localidad, p.provincia FROM sys_localidades l JOIN sys_provincias p ON l.id_provincia = p.id WHERE l.id = " . $this->db->escape($id_localidad);
		
		$query = $this->db->query($sql);
		return $query->row_array();	
	}

	public function get_rubros_mas_consultados($id_sucursal, $en_columnas = FALSE){
		$this->load->library('parseo_library');

		$sql = 'SELECT imagen_home imagen, icon, columna, alias, id, rubro
				FROM prov_rubros
				WHERE id_sucursal = ? AND mas_consultados = 1
				ORDER BY columna, posicion_consultados';

		$query = $this->db->query($sql, array($id_sucursal));
		if($en_columnas){
			$array = array();
			$res = $query->result_array();
			foreach ($res as $key => $value){
				$value['nombre_seo'] = $this->parseo_library->clean_url($value['rubro']);
				$columna = $value['columna'];
				unset($value['columna']);
				$array[$columna][] = $value;
			}
			$res = $array;
		}else{
			$res = $query->result_array();
		}
		return $res;
	}
}