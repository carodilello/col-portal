<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| RUBROS-MODEL
| -------------------------------------------------------------------
| Este model es parte del desglose de la vieja clase class.comercial.php
|
| En este caso apuntamos a lo relacionado con los rubros
|
*/

class Proveedores_model extends CI_Model {
	public $resultados = 0;

	public function __construct(){
		parent::__construct();
	}

	public function get_proveedores_destacados($id_seccion = 0, $cantidad = 0){
		$this->load->library('parseo_library');

		$sql = "SELECT SQL_CALC_FOUND_ROWS ";
		$sql .= ' p.id_minisitio id , p.proveedor , p.rubro, p.sucursal, p.comentarios, p.cantidad_puntuados, p.promedio, CONCAT(sm.name,"_mediana.",sat.extension) imagen, p.id_rubro, p.id_sucursal, p.breve, p.zonas, pm.short_url subdominio ';
		$sql .= " FROM sys_home_proveedores shp ";
		$sql .= " JOIN sys_asociados_proveedores sap ON shp.id = sap.id_padre AND sap.id_referencia = 37 AND sap.activo = 1 ";
		$sql .= ((empty($id_seccion))? '' : 'JOIN sys_areas_asociadas saa ON shp.id = saa.id_padre AND saa.id_referencia = 37 AND saa.activo = 1 AND saa.id_seccion = '.$id_seccion);
		$sql .= " JOIN site_proveedores_activos p ON sap.id_proveedor = p.id_proveedor ";
		$sql .= " Left JOIN sys_medias sm ON sm.estado IN (2,4) AND sm.eliminado = 0 ";
		$sql .= " LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1 ";
		$sql .= " LEFT JOIN sys_rel_medias_referencias srmr ON (sm.id = srmr.id_media AND p.id_proveedor = srmr.id_padre AND srmr.id_referencia = 37) ";
		$sql .= " LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio ";
		$sql .= " WHERE sm.eliminado = 0 AND srmr.estado >= 2 AND col = 1 AND shp.id_estado = 2 AND shp.eliminado = 0 ORDER BY srmr.orden ";
		$sql .= (($cantidad == 0)? '' : 'LIMIT '.$cantidad);

		$query = $this->db->query($sql);
		$ret = $query->result_array();
		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['nombre_seo'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
		$this->resultados = $query2->row()->rows;

		return $ret;
	}

	public function get_proveedores($id_rubro, $orden='nivel', $cantidad = 0, $pagina = 1, $filtros = '', $solo_mapa = 0, $combo = 0, $no_id = ''){
		$this->load->library('config_library');
		$this->load->helper('common_helper');

		$pagina = ($pagina<1)? 1 : $pagina;
		$cantidad = ($cantidad<0)? 25 : $cantidad;

		$and = '';

		$orden = str_replace("-DESC"," DESC",$orden);


		if (!empty($filtros)){
			$opc = $this->config_library->get_filtros_aplicados($filtros,1,$id_rubro);
			foreach($opc as $o){
				$and .= ' AND p.id_minisitio IN (SELECT DISTINCT id_minisitio FROM site_proveedores_activos_filtros WHERE id_opcion = '.$o['id_opcion'].') ';
			}
		}

		$and .= ($solo_mapa == 0)? '' : ' AND p.latitud IS NOT NULL ';
		$select = 'SQL_CALC_FOUND_ROWS p.*, p.nivel, pm.short_url subdominio
		, (SELECT CONCAT(sm.name,"_chica.",sat.extension) FROM prov_minisitios pm1
			LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
			LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
			JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
			WHERE pm1.id_proveedor = p.id_proveedor ORDER BY srmr.orden ASC LIMIT 1) imagen_chica
		, (SELECT CONCAT(sm.name,"_grande.",sat.extension) FROM prov_minisitios pm1
			LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
			LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
			JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
			WHERE pm1.id_proveedor = p.id_proveedor ORDER BY srmr.orden ASC LIMIT 1) imagen_gde
		, IFNULL((SELECT COUNT(id_proveedor)*30 cant FROM new_faq_respuestas nfr
			WHERE nfr.id_proveedor = p.id_proveedor
			GROUP BY nfr.id_proveedor), 0)+p.orden puntuacion ';
		if ($combo){
			$select = 'p.id_minisitio, p.proveedor';
		}

		if($no_id){
			$and .= ' AND p.id_proveedor != ' . $this->db->escape($no_id);
		}

		$sql = 'SELECT '.$select.'
						FROM site_proveedores_activos p
						LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id_proveedor
						WHERE p.id_rubro = ?
						'.$and.'
						GROUP BY p.id_proveedor ';
		$sql .= 'ORDER BY '.$orden.', puntuacion DESC';

		$sql .=	(($cantidad == 0)? '' : ' LIMIT '.($pagina-1)*$cantidad.','.$cantidad);

		$query = $this->db->query($sql,array($id_rubro));

		if ($combo){
			foreach($query->result_array() as $row){
			    $listado[$row["id_minisitio"]] = $row["proveedor"];
			}
			return $listado;
		}

		$ret = $query->result_array();

		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			if($el['rubro']) $arr[$k]['rubro_seo'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
		$this->resultados = $query2->row()->rows;


		if ($solo_mapa == 1){
			$maps['markers'] = array();
			$cant_provs = count($ret);

			$icon = '';
			foreach($ret as $r){
				switch ($r['nivel']){
					case 1:
						$icon = '_destacado';
						break;
					case 2:
						$icon = '_destacado';
						break;
					case 3: case 4: case 5:
						$icon = '';
						break;
				}

				$maps['markers'][] = array(
					"latitud" => $r['latitud'],
					"longitud" => $r['longitud'],
					"id_minisitio" => $r["id_minisitio"],
					"label" => $r['proveedor'],
					"rubro_seo" => $r['rubro_seo'],
					"subdominio" => $r['subdominio'],
					"icontype" => $icon
				);

			}
			$ret = json_encode($maps);
		}

		return $ret;
	}

	public function get_proveedor($id_ms){
		$this->load->library('parseo_library');
		$this->load->helper('common_helper');

		$sql = "SELECT
		 		p.*,
		 		CASE WHEN p.id_sucursal!=1 THEN '' ELSE p.zonas END AS zonas,
		 		GROUP_CONCAT(DISTINCT CONCAT('%&@',tel.id_telefono_tipo,';', tel.telefono) SEPARATOR '') tels
		 		, (SELECT CONCAT(sm.name,'_chica.',sat.extension) FROM prov_minisitios pm1
					LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
					LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
					JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
					WHERE pm1.id_proveedor = p.id_proveedor ORDER BY srmr.orden ASC LIMIT 1) imagen_chica
				, (SELECT CONCAT(sm.name,'_grande.',sat.extension) FROM prov_minisitios pm1
					LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
					LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
					JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
					WHERE pm1.id_proveedor = p.id_proveedor ORDER BY srmr.orden ASC LIMIT 1) imagen_gde
				, (SELECT COUNT(id) FROM new_faq_respuestas WHERE id_proveedor = p.id_proveedor) faqs
		 		, pp.portada
		 		, pm.usuario_facebook
		 		, pm.short_url subdominio
		 	FROM site_proveedores_activos p
		 	LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id_proveedor
		 	LEFT JOIN sys_telefonos tel ON tel.id_padre = p.id_minisitio AND tel.id_referencia = 5 AND tel.estado > 0
		 	LEFT JOIN prov_proveedores pp ON pp.id = pm.id_proveedor
		 	WHERE p.id_minisitio = ?
		 	GROUP BY p.id_minisitio";

		$query = $this->db->query($sql, array($id_ms));
		$ret = $query->row_array();

		if($ret){
			$ret['whatsapp'] = '';
			if($ret['tels']){
				$tels = doubleExplodeFull('%&@', ';', $ret['tels']);
				$whatsapp = FALSE;
				$messenger = FALSE;
				if(is_array($tels)){
					$tels_p = array();
					foreach ($tels as $k => $t){
						if(isset($t[0]) && $t[0]) $tels_p[] = $this->parseo_library->procesar_telefono($t);
						if(isset($t[0]) && $t[0] && $t[0] == '7') $whatsapp = TRUE;
						if(isset($t[0]) && $t[0] && $t[0] == '8') $messenger = TRUE;
					}
				}
				$ret['tels'] = $tels_p;
				$ret['whatsapp'] = $whatsapp;
				$ret['messenger'] = $messenger;
			}
		}

		if($ret['rubro']) $ret['seo_url'] = $this->parseo_library->clean_url($ret['rubro']);

		return $ret;
	}

	public function get_proveedor_vencido($id_ms){
		$sql = "SELECT
			p.id id_proveedor
			, p.logo
			, r.id id_rubro
			, r.rubro
			, suc.id id_sucursal
			, suc.sucursal
			, m.id id_minisitio
			, m.proveedor
			, IFNULL(m.slogan,'') slogan
			, m.titulo
			, m.short_url subdominio
			, IFNULL(m.subtitulo,'') subtitulo
			, IFNULL(m.breve,'') breve
			, m.contenido
			, m.meta_descripcion
			, m.meta_palabras meta_keywords
			, m.mostrar_inactivo
			, CASE WHEN r.id_sucursal!=1 THEN '' ELSE GROUP_CONCAT(DISTINCT fo.opcion ORDER BY fo.orden)  END AS zonas
			, p.portada
			, CONCAT(sm.name,'_grande.',sat.extension) imagen_gde
			, m.usuario_facebook
		FROM prov_proveedores p
		JOIN prov_minisitios m ON m.id_proveedor = p.id
		JOIN prov_rubros r ON m.id_rubro = r.id
		JOIN sys_sucursales suc ON r.id_sucursal = suc.id
		LEFT JOIN prov_minisitios_filtros_opciones mf ON m.id = mf.id_minisitio
		LEFT JOIN prov_filtros_opciones fo ON mf.id_opcion = fo.id AND fo.id_filtro = 36
		LEFT JOIN sys_rel_medias_referencias srmr ON m.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
		LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = m.id_proveedor
		JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
		WHERE m.id = ?
		GROUP BY p.id";

		$query = $this->db->query($sql, array($id_ms));
		$ret = $query->row_array();

		if($ret['rubro']) $ret['seo_url'] = $this->parseo_library->clean_url($ret['rubro']);

		return $ret;
	}

	public function get_proveedor_hash($id_ms, $hash){
		$this->load->library('parseo_library');

		$sql = "SELECT p.id id_proveedor, p.logo, r.id id_rubro, suc.id id_sucursal, m.id id_minisitio, p.proveedor, r.rubro, suc.sucursal, IFNULL(m.slogan,'') slogan, m.titulo, IFNULL(m.subtitulo,'') subtitulo, p.id imagen, IFNULL(m.breve,'') breve, GROUP_CONCAT(DISTINCT fo.opcion ORDER BY fo.orden) zonas, 0 fotos, 0 comentarios, (SELECT COUNT(*) FROM sys_rel_medias_referencias srmr JOIN sys_medias sm ON sm.id = srmr.id_media AND sm.id_tipo IN (44,45,46,47,48,49,50,51,52,53,54,55) WHERE srmr.id_padre = m.id AND srmr.id_referencia = 5 AND srmr.estado = 2) audios, (SELECT COUNT(srmr.id) FROM sys_rel_medias_referencias srmr
				JOIN sys_medias sm ON sm.id = srmr.id_media AND sm.id_tipo IN (56,77,78,79,80,81,82,83,87)
				WHERE srmr.id_padre = m.id AND srmr.id_referencia = 5) videos, (SELECT COUNT(*) FROM prov_minisitios_productos WHERE id_minisitio = m.id AND promocion = 0 AND estado IN (2,3)) productos, (SELECT COUNT(*) FROM prov_minisitios_productos WHERE id_minisitio = m.id AND promocion = 1 AND estado IN (2,3)) promociones, (SELECT COUNT(*) FROM paq_paquetes WHERE id_proveedor = p.id AND estado = 4) paquetes, 0 dias_online, 0 cupones, 0 orden, IFNULL(CONCAT(dir.calle,IFNULL(CONCAT(' ',dir.numero),''),IFNULL(CONCAT(' ',dir.departamento),''),IFNULL(CONCAT(' - ',loc.localidad),''),IFNULL(CONCAT(' - ',pv.provincia),''),IFNULL(CONCAT(' - ',pai.pais),'')),'') direccion, dir.latitud, dir.longitud, IFNULL(GROUP_CONCAT(DISTINCT tel.telefono ORDER BY tel.orden SEPARATOR ' / '),'') telefonos, IFNULL(dm.pago_var_ms,0) dm, m.contenido, NULL promedio, NULL atencion, NULL calidad, NULL servicio, NULL producto, NULL presentacion, 0 cantidad_puntuados, 0 posicion, m.meta_descripcion, m.meta_palabras meta_keywords
				, p.portada
				, (SELECT CONCAT(sm.name,'_grande.',sat.extension) FROM prov_minisitios pm1
					LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
					LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
					JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
					WHERE pm1.id_proveedor = p.id ORDER BY srmr.orden ASC LIMIT 1) imagen_gde
				, (SELECT COUNT(id) FROM new_faq_respuestas WHERE id_proveedor = p.id) faqs
				, GROUP_CONCAT(DISTINCT CONCAT('%&@',tel.id_telefono_tipo,';', tel.telefono) SEPARATOR '') tels
				, m.usuario_facebook
				, m.short_url subdominio
				FROM prov_proveedores p
				JOIN prov_minisitios m ON p.id = m.id_proveedor
				JOIN prov_rubros r ON p.id_rubro = r.id
				JOIN sys_sucursales suc ON r.id_sucursal = suc.id
				LEFT JOIN prov_dineromail dm ON m.id = dm.id_minisitio AND dm.operar = 1 AND dm.estado = 3
				LEFT JOIN sys_rel_medias_referencias srmr ON m.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden = 1
				LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0
				LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id
				LEFT JOIN prov_minisitios_filtros_opciones mf ON m.id = mf.id_minisitio
				LEFT JOIN prov_filtros_opciones fo ON mf.id_opcion = fo.id AND fo.id_filtro = 36
				LEFT JOIN sys_direcciones dir ON m.id = dir.id_padre AND dir.id_referencia = 5 AND dir.estado > 0 AND dir.orden = 1
				LEFT JOIN sys_localidades loc ON dir.id_localidad = loc.id AND loc.localidad <> 'Sin Asignar'
				LEFT JOIN sys_provincias pv ON loc.id_provincia = pv.id AND pv.id <> 26
				LEFT JOIN sys_paises pai ON pv.id_pais = pai.id
				LEFT JOIN sys_telefonos tel ON m.id = tel.id_padre AND tel.id_referencia = 5 AND tel.estado > 0
				WHERE m.id = ?
				AND MD5(p.proveedor) = ?
				GROUP BY p.id";

		$query = $this->db->query($sql, array($id_ms, $hash));

		$ret = $query->row_array();

		if($ret){
			$ret['whatsapp'] = '';
			if($ret['tels']){
				$tels = doubleExplodeFull('%&@', ';', $ret['tels']);
				$whatsapp = FALSE;
				$messenger = FALSE;
				if(is_array($tels)){
					$tels_p = array();
					foreach ($tels as $k => $t){
						if(isset($t[0]) && $t[0]) $tels_p[] = $this->parseo_library->procesar_telefono($t);
						if(isset($t[0]) && $t[0] && $t[0] == '7') $whatsapp = TRUE;
						if(isset($t[0]) && $t[0] && $t[0] == '8') $messenger = TRUE;
					}
				}
				$ret['tels'] = $tels_p;
				$ret['whatsapp'] = $whatsapp;
				$ret['messenger'] = $messenger;
			}
		}

		if($ret['rubro']) $ret['seo_url'] = $this->parseo_library->clean_url($ret['rubro']);

		return $ret;
	}


	public function get_proveedor_media($id_ms, $tipo = 1){
		$where = ' WHERE m.id_minisitio = ' . $id_ms;
		switch ($tipo){
			//imagenes
			case 1:
				$select = 'CONCAT(med.name,"_chica.",sat.extension) imagen, m.proveedor';
				//$where .= ' AND srmr.orden < 50';
				$limit = '50';
				break;
			//audios
			case 2:
				$select = 'CONCAT(med.name,".",sat.extension) item';
				//$where .= ' AND srmr.orden < 16';
				$limit = '15';
				break;
			//videos
			case 3:
				$select = 'med.name item, med.video_thumb_url thumb';
				//$where .= ' AND srmr.orden < 6';
				$limit = '5';
				break;
		}
		$sql = 'SELECT med.id, '.$select.', med.titulo, med.descripcion, GROUP_CONCAT(t.id,"@",t.descripcion ORDER BY atg.orden, t.descripcion SEPARATOR "~") tags
						FROM site_proveedores_activos m
						JOIN sys_rel_medias_referencias srmr ON m.id_minisitio = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.estado = 2
						JOIN sys_medias med ON srmr.id_media = med.id AND med.estado IN (2,4) AND med.eliminado = 0
						JOIN sys_archivos_tipo sat ON med.id_tipo = sat.id AND sat.id_tipo = '.$tipo.'
						LEFT JOIN sys_asociados_tags atg ON med.id = atg.id_padre AND atg.activo = 1
						LEFT JOIN sys_tags t ON atg.id_tag = t.id AND t.id_referencia = 15 AND t.activo = 1
						'.$where.'
						GROUP BY med.id
						ORDER BY srmr.orden LIMIT '.$limit;

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function get_proveedor_asociados($id_ms, $visible = -1, $recibe = -1){
		$and = '';
		if ($visible != -1){
			$and = ' AND a.visible = '.$visible.' ';}
		if ($recibe != -1){
			$and .= ' AND a.recibe = '.$recibe.' ';}

		$sql = 'SELECT DISTINCT ms.proveedor, a.id_asociado, r.rubro, pa.id_minisitio, r.id id_rubro, pa.id_sucursal, pa.sucursal
						FROM prov_minisitios_asociados a
						JOIN prov_proveedores p ON a.id_asociado = p.id
						JOIN prov_minisitios ms ON ms.id_proveedor = p.id
						JOIN prov_rubros r ON p.id_rubro = r.id
						LEFT JOIN site_proveedores_activos pa ON a.id_asociado = pa.id_proveedor
						WHERE a.id_minisitio = ?
						AND a.estado > 0 AND p.estado=6
						'.$and.'
						ORDER BY r.rubro, a.orden';

		$query = $this->db->query($sql, array($id_ms));

		return $query->result_array();
	}

	public function get_proveedor_asociados_ms($id_ms, $recibe = -1){
/*	public function get_proveedor_asociados_ms($id_ms){*/
		/* 06062017 agregue parametro $recibe que envia 1 desde formularios para que solo tome los recibe = 1 y no traiga todos los proveedores asociados*/

		$and = '';
		if ($recibe != -1){
			$and .= ' AND p.estado = 6 and a.recibe = '.$recibe.' ';}
		$sql = 'SELECT DISTINCT ms.proveedor, a.id_asociado, r.rubro, pa.id_minisitio, r.id id_rubro, pa.id_sucursal, pa.sucursal, p.logo, ms.short_url subdominio
					FROM prov_minisitios_asociados a
					JOIN prov_proveedores p ON a.id_asociado = p.id
					JOIN prov_minisitios ms ON ms.id_proveedor = p.id
					JOIN prov_rubros r ON p.id_rubro = r.id
					LEFT JOIN site_proveedores_activos pa ON a.id_asociado = pa.id_proveedor
					WHERE a.id_minisitio = ? AND a.estado > 0
					AND a.visible = 1
					'.$and.'
					ORDER BY r.rubro, a.orden';

		$query = $this->db->query($sql, array($id_ms));
		$ret = $query->result_array();
		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['url_seo'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		return $ret;
	}

	public function get_proveedor_comentarios($id_ms, $limit = NULL){
		$this->load->helper('common_helper');

		$return = NULL;

		$sql = 'SELECT CONCAT_WS("#", c.id, REPLACE(IFNULL(c.comentario,""),"#","")
		, IF(c.autor != "",c.autor,IF(CONCAT( u.nombre," ",apellido) != "" ,CONCAT( u.nombre," ",apellido),u.user))
		, "", IFNULL(c.email,""), c.fecha_alta, IFNULL(c.id_user,""), IFNULL(c.atencion,""), IFNULL(c.calidad,""), IFNULL(c.servicio,""), IFNULL(c.producto,""), IFNULL(c.presentacion,"")) as comentario
		 FROM prov_minisitios_comentarios c
		 LEFT JOIN  sys_usuarios u ON u.id = c.id_user
		 WHERE c.id_minisitio = ?
		 AND c.estado = 2 ORDER BY c.fecha_alta DESC';

		if($limit) $sql .= ' LIMIT '.$limit;

		$query = $this->db->query($sql, array($id_ms));

		if($arr = $query->result_array()) foreach ($arr as $k => $val){
			$return[$k] = $val['comentario'] . '#' . (toColor($k+4)); // Sumo cuatro para empezar con colores menos chillones
		}

		return $return;
	}

	public function get_proveedores_navegacion($posicion, $id_rubro){
		$sql = 'SELECT DISTINCT p.proveedor, p.id_minisitio, p.posicion, p.logo
						FROM site_proveedores_activos p
						WHERE p.id_rubro = ?
						AND (p.posicion = '.($posicion-1).' OR p.posicion = '.($posicion+1).')
						ORDER BY p.posicion';

		$query = $this->db->query($sql, array($id_rubro));

		return $query->result_array();
	}

	public function get_cupones($filtros = null){
		$referencia = 38;
		$where = ' WHERE cmrc.vencimiento > NOW() AND cmrc.eliminado = 0 AND cmrc.id_estado = 2';

		if(isset($filtros['rubro']) && $filtros['rubro']){
			$where .= ' AND provp.id_rubro = ' . $this->db->escape($filtros['rubro']);
		}

		if(isset($filtros['id_minisitio']) && $filtros['id_minisitio']){
			$where .= ' AND provp.id_minisitio = ' . $this->db->escape($filtros['id_minisitio']);
		}

		$sql = 'SELECT SQL_CALC_FOUND_ROWS cmrc.id, cmrc.titulo, TIMEDIFF(cmrc.vencimiento, NOW()) AS diff, DATE_FORMAT(cmrc.vencimiento,"%m/%d/%Y %h:00 %p") AS target_date, cmrc.vencimiento, cmrc.compraron, cmrc.precio, cmrc.descuento, cmrc.flags, provp.id_rubro, provp.rubro, provp.proveedor, provp.id_minisitio, cmrc.descripcion, cmrc.legales, cmrc.dinero_mail, provp.sucursal, provp.id_sucursal
		FROM cmr_cupones cmrc
		LEFT JOIN sys_asociados_proveedores sysap ON cmrc.id = sysap.id_padre AND sysap.id_referencia = ' . $referencia . ' AND sysap.activo = 1
		LEFT JOIN site_proveedores_activos provp ON sysap.id_proveedor = provp.id_proveedor
		'.$where.'
		GROUP BY cmrc.id
		ORDER BY cmrc.flags DESC, vencimiento ASC';

		$query = $this->db->query($sql);

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
		$this->resultados = $query2->row()->rows;

		return $query->result_array();
	}

	public function buscar_contenido_proveedores($search, $id_sucursal, $sucursal){
		$this->load->library('parseo_library');

		$sql = "(SELECT 'fa-list' class, r1.rubro nombre, IF(nn.href IS NULL,CONCAT('" . $sucursal . "/proveedor/???_CO_r',r1.id),nn.href) url, 'rubro' tipo, NULL rubro, NULL subdominio, r1.id id_elemento
				FROM prov_rubros r1
				LEFT JOIN prov_rubros r2 ON r1.padre = r2.id
				LEFT JOIN new_nav nn ON nn.id_rubro = r1.id AND nn.nivel = 2
				JOIN site_proveedores_activos p ON r1.id = p.id_rubro
				WHERE r1.id_sucursal = ".$this->db->escape($id_sucursal)." AND r1.rubro LIKE '%".$this->db->escape_like_str($search)."%' AND r1.padre IS NOT NULL
				GROUP BY r1.id
				LIMIT 3)
				UNION ALL
				(SELECT 'fa-suitcase' class, p.proveedor nombre, CONCAT('???_CO_m', pm.id) url, 'proveedor' tipo, p.rubro, pm.short_url subdominio, pm.id id_elemento
				FROM site_proveedores_activos p
				LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id_proveedor
				WHERE p.id_sucursal = ".$this->db->escape($id_sucursal)." AND p.rubro LIKE '%".$this->db->escape_like_str($search)."%'
				ORDER BY p.nivel, p.orden DESC
				LIMIT 5)
				UNION ALL
				(SELECT 'fa-suitcase' class, p.proveedor nombre, CONCAT('???_CO_m', pm.id) url, 'proveedor' tipo, p.rubro, pm.short_url subdominio, pm.id id_elemento
				FROM site_proveedores_activos p
				LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.id_sucursal = ".$this->db->escape($id_sucursal)."
				HAVING nombre LIKE '%".$this->db->escape_like_str($search)."%'
				ORDER BY p.nivel, p.orden DESC
				LIMIT 5)
				UNION ALL
				(SELECT 'fa-tag' class, p.titulo nombre, CONCAT('/proveedores/promocion/',p.id_producto,'#pp_minisitio') url, 'promoción' tipo, pa.rubro, pm.short_url subdominio, p.id_producto id_elemento
				FROM site_productos_activos p
				LEFT JOIN site_proveedores_activos pa ON pa.id_proveedor = p.id_proveedor
				LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.promocion = 1 AND p.id_sucursal = ".$this->db->escape($id_sucursal)."
				HAVING p.titulo LIKE '%".$this->db->escape_like_str($search)."%'
				ORDER BY p.nivel, p.orden, pa.nivel, pa.orden DESC
				LIMIT 2)
				UNION ALL
				(SELECT 'fa-star' class, p.titulo nombre, CONCAT('/proveedores/producto/',p.id_producto,'#pp_minisitio') url, 'producto' tipo, pa.rubro, pm.short_url subdominio, p.id_producto id_elemento
				FROM site_productos_activos p
				LEFT JOIN site_proveedores_activos pa ON pa.id_proveedor = p.id_proveedor
				LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.promocion = 0 AND p.id_sucursal = ".$this->db->escape($id_sucursal)."
				HAVING p.titulo LIKE '%".$this->db->escape_like_str($search)."%'
				ORDER BY p.nivel, p.orden, pa.nivel, pa.orden DESC
				LIMIT 2)
				UNION ALL
				(SELECT 'fa-gift' class, p.titulo nombre, CONCAT('/proveedores/paquete/',p.id_paquete,'#pp_minisitio') url, 'paquete' tipo, p.rubro, pm.short_url subdominio, p.id_paquete id_elemento
				FROM site_paquetes_activos p
				LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.id_sucursal = ".$this->db->escape($id_sucursal)."
				HAVING nombre LIKE '%".$this->db->escape_like_str($search)."%'
				LIMIT 2)
				UNION ALL
				(SELECT 'fa-file-text-o' class, etya.titulo nombre, CONCAT('/consejos/???_CO_n',etya.id_nota) url, 'nota' tipo, NULL rubro, NULL subdominio, etya.id_nota id_elemento
				FROM edt_notas_tya etya
				LEFT JOIN edt_notas e ON e.id = etya.id_nota
				WHERE id_autor IS NOT NULL AND e.id_estado = 3
				HAVING titulo LIKE '%".$this->db->escape_like_str($search)."%'
				LIMIT 2)
				LIMIT 10";

		$query = $this->db->query($sql);
		$res_busqueda = $query->result_array();

		$res = array();
		if(!empty($res_busqueda)) foreach ($res_busqueda as $k => $el) {
			$res[$k] = $el;
			if($el['tipo'] == 'rubro'){
				$res[$k]['url'] = str_replace('???', $this->parseo_library->clean_url($el['nombre']), $res[$k]['url']);
			}elseif($el['tipo'] == 'proveedor'){
				$rubro_cleaned = $this->parseo_library->clean_url($el['rubro']);
				$res[$k]['url'] = str_replace('???', $rubro_cleaned, $res[$k]['url']);
				if($el['subdominio']) $res[$k]['url'] = str_replace('{?}', $el['subdominio'], 'http://{?}.' . DOMAIN . '/') . $rubro_cleaned;
			}elseif($el['tipo'] == 'promoción'){
				$codigo = '_CO_pr';
			}elseif($el['tipo'] == 'producto'){
				$codigo = '_CO_pd';
			}elseif($el['tipo'] == 'paquete'){
				$codigo = '_CO_pa';
			}elseif($el['tipo'] == 'nota'){
				$rubro_cleaned = $this->parseo_library->clean_url($el['nombre']);
				$res[$k]['url'] = str_replace('???', $rubro_cleaned, $res[$k]['url']);
			}

			if($el['tipo'] == 'promoción' || $el['tipo'] == 'producto' || $el['tipo'] == 'paquete'){
				$rubro_cleaned = $this->parseo_library->clean_url($el['rubro']);
				$res[$k]['url'] = str_replace('???', $rubro_cleaned, $res[$k]['url']);
				if($el['subdominio']) $res[$k]['url'] = str_replace('{?}', $el['subdominio'], 'http://{?}.' . DOMAIN . '/') . $rubro_cleaned . '/' . $this->parseo_library->clean_url($el['nombre']) . $codigo . $el['id_elemento'];
			}
		}

		return $res;
	}

	public function search_rubro($search){
		if(is_array($search)){
			$sql = "";
		}
	}

	public function get_presupuesto_filtro($id_rubro, $id_filtro = 36){ // Por default es zona
		$sql = 'SELECT f.opcion, GROUP_CONCAT(f.id_minisitio) minisitios
				FROM site_proveedores_activos p
				JOIN site_proveedores_activos_filtros f ON p.id_minisitio = f.id_minisitio AND f.id_filtro = ?
				WHERE p.id_rubro = ?
				GROUP BY f.id_opcion
				ORDER BY f.orden_opcion';

		$query = $this->db->query($sql, array($id_filtro, $id_rubro));
		return $query->result_array();
	}

	public function get_foto($id_foto = 0){
		$this->load->library('parseo_library');

		$sql = 'SELECT sm.id, sm.titulo, sm.descripcion, p.proveedor, p.id_minisitio, p.id_proveedor, p.id_sucursal, CONCAT(sm.name,"_chica.",sat.extension) imagen, CONCAT("/minisitio.php?id_minisitio=",p.id_minisitio) url, p.id_rubro, p.sucursal, p.rubro, pm.short_url subdominio
						FROM site_proveedores_activos p
						JOIN sys_rel_medias_referencias srmr ON p.id_minisitio = srmr.id_padre AND srmr.id_referencia = 5
						JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1
						JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
						LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
						WHERE sm.id = ' . $this->db->escape($id_foto);

		$query = $this->db->query($sql);
		$res = $query->row_array();

		return $res;
	}

	public function buscar_proveedores($term, $orden = 'p.proveedor', $cantidad = 0, $pagina = 1, $id_sucursal = 1){
		$this->load->library('parseo_library');

		$sql = 'SELECT 
					SQL_CALC_FOUND_ROWS p.*
					, (SELECT CONCAT(sm.name,"_chica.",sat.extension) FROM prov_minisitios pm1
						LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
						LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
						JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
						WHERE pm1.id_proveedor = p.id_proveedor ORDER BY srmr.orden ASC LIMIT 1) imagen_chica
					, (SELECT CONCAT(sm.name,"_grande.",sat.extension) FROM prov_minisitios pm1
						LEFT JOIN sys_rel_medias_referencias srmr ON pm1.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
						LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm1.id_proveedor
						JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
						WHERE pm1.id_proveedor = p.id_proveedor ORDER BY srmr.orden ASC LIMIT 1) imagen_gde
					, pm.short_url subdominio
				FROM site_proveedores_activos p
				LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id_proveedor
				LEFT JOIN sys_rel_medias_referencias srmr ON pm.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
				LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm.id_proveedor
				JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
				WHERE (p.proveedor LIKE "%'.$this->db->escape_like_str($term).'%" OR p.rubro LIKE "%'.$this->db->escape_like_str($term).'%" OR p.breve LIKE "%'.$this->db->escape_like_str($term).'%") AND id_sucursal = '.$this->db->escape($id_sucursal).'
				GROUP BY p.id_minisitio
				ORDER BY ' . $orden . (($cantidad == 0) ? '' : ' LIMIT '.($pagina-1)*$cantidad.','.$cantidad);

		$query = $this->db->query($sql);

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
		$this->resultados = $query2->row()->rows;

		$ret = $query->result_array();
		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['seo_url'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		return $ret;
	}

	public function get_seccion($id_rubro){
		$sql = 'SELECT a.id_seccion
						FROM sys_areas_asociadas a
						WHERE a.id_referencia = 40
						AND a.id_padre = ?
						ORDER BY a.orden
						LIMIT 1';

		$query = $this->db->query($sql, array($id_rubro));

		return $query->row_array();
	}

	public function get_faqs($id_proveedor, $id_rubro){
		$sql = 'SELECT pregunta, value respuesta, nfro.id_tipo, nfro.label, GROUP_CONCAT(nfro.label) labels, fp.icono
				FROM new_faq_rel_pregunta_opcion_rubro por
				LEFT JOIN new_faq_respuestas fr ON fr.id_pregunta = por.id_pregunta AND fr.id_proveedor = ?
				LEFT JOIN new_faq_preguntas fp ON fp.id = por.id_pregunta
				LEFT JOIN new_faq_respuestas_opciones nfro ON nfro.id = por.id_opcion
				WHERE por.id_rubro = ?
				GROUP BY por.id_pregunta';

		$query = $this->db->query($sql, array($id_proveedor, $id_rubro));
		return $query->result_array();
	}

	public function get_faqs_respuestas($id_rubro){
		$sql = 'SELECT nfro.id id_name, label
				FROM new_faq_rel_pregunta_opcion_rubro nfqpr
				LEFT JOIN new_faq_respuestas_opciones nfro ON nfro.id = nfqpr.id_opcion
				WHERE nfqpr.id_rubro = ?
				HAVING id_name IS NOT NULL';

		$query = $this->db->query($sql, array($id_rubro));
		$res = $query->result_array();

		if(!empty($res)){
			$array = array();
			foreach ($res as $k => $value) {
				$array[$value['id_name']] = $value['label'];
			}
			$res = $array;
		}

		return $res;
	}

	public function get_faqs_rangos(){
		$sql = "SELECT id, value
				FROM new_faq_rangos";

		$query = $this->db->query($sql);

		$res = $query->result_array();

		$array_res = array();
		foreach ($res as $key => $value) {
			$array_res[$value['id']] = $value['value'];
		}

		return $array_res;
	}

	public function get_minisitio_subdominio($subdominio = ''){
		$sql = 'SELECT p.id_minisitio
				FROM site_proveedores_activos p
				LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id_proveedor
				WHERE short_url = ?
				ORDER BY id ASC
				LIMIT 1';

		$query = $this->db->query($sql, array($subdominio));
		return $query->row_array();
	}

	public function get_minisitio_subdominio_vencido($subdominio = '') {
		$sql = 'SELECT pm.id AS id_minisitio
				FROM prov_proveedores p
				LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id
				WHERE short_url = ?
				AND p.id_portal = 1
				ORDER BY pm.id ASC
				LIMIT 1';

		$query = $this->db->query($sql, array($subdominio));
		return $query->row_array();
	}

	public function minisitios_archivos_autoreply($id_minisitio = ''){
		$sql = "SELECT archivo FROM prov_minisitios_archivos_autoreply WHERE id_minisitio = ? ";

		$query = $this->db->query($sql, array($id_minisitio));

		return $query->result_array();
	}

	public function get_envio_proveedor($id_pres){
		$sql = " SELECT u.id id_usuario_proveedor
    	FROM prov_presupuestos pp
    	JOIN prov_minisitios m ON m.id = pp.id_minisitio
    	JOIN prov_proveedores p ON p.id = m.id_proveedor
    	JOIN sys_emails e ON m.id = e.id_padre AND e.id_referencia = 5 AND e.estado > 0
    	JOIN prov_contactos c ON c.id_proveedor = p.id
    	JOIN sys_usuarios u ON u.id = c.id_user
    	WHERE pp.id = '" . $this->db->escape((int) $id_pres) . "'
    	GROUP BY pp.id ";

		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function insertar_estadistica_telefono($id_minisitio, $id_tipo = '1'){
		$sql = "SELECT id FROM prov_estadistica_telefonos
				WHERE id_minisitio = " . $this->db->escape($id_minisitio) . " AND id_tipo = " . $this->db->escape($id_tipo) . " AND mes = MONTH(NOW()) AND anio = YEAR(NOW()) ";

		$query = $this->db->query($sql);
		$ret = $query->row_array();

		$id = NULL;
		if(!empty($ret['id'])) $id = $ret['id'];

		if ($id){
			$sql = 'UPDATE prov_estadistica_telefonos SET total = (total + 1) WHERE id = ' . $this->db->escape($id);
		}else{
			$sql = 'INSERT INTO prov_estadistica_telefonos (id_minisitio, id_tipo, mes, anio, total) ';
			$sql .= ' VALUES(' . $this->db->escape($id_minisitio) . ', ' . $this->db->escape($id_tipo) . ', MONTH(NOW()), YEAR(NOW()), 1) ';
		}

		$this->db->query($sql);
	}
}