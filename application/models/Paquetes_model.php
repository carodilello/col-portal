<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquetes_model extends CI_Model{
	public $id;
	public $resultados = 0;
	public $referencia = 8;
	public $resIntegrados = NULL;

	public function __construct(){
		parent::__construct();
	}
	
	public function getResultados(){
		return $this->resultados;
	}

	public function getListadoHomeCol($id_sucursal=1){
		/*
		$sql = 'SELECT paqp.id, GROUP_CONCAT(DISTINCT paqz.zona ORDER BY paqz.zona SEPARATOR \', \') AS zonas, provp.id_minisitio AS id_minisitio, provp.proveedor AS organizador, paqp.paquete AS titulo, paqp.precio, sysmo.simbolo AS moneda, paqpret.tipo AS precio_tipo, GROUP_CONCAT(DISTINCT provr.id,\'*\',provr.rubro ORDER BY provr.orden) AS rubros, paqpt.tipo AS paquete_tipo FROM paq_paquetes paqp LEFT JOIN paq_rel_paquetes_zonas paqrpz ON paqp.id = paqrpz.id_paquete LEFT JOIN paq_zonas paqz ON paqrpz.id_zona = paqz.id JOIN site_proveedores_activos provp ON paqp.id_proveedor = provp.id_proveedor LEFT JOIN sys_monedas sysmo ON paqp.id_moneda = sysmo.id LEFT JOIN paq_precios_tipo paqpret ON paqp.id_precio_tipo = paqpret.id LEFT JOIN paq_rel_paquetes_proveedores paqrpp ON paqp.id = paqrpp.id_paquete LEFT JOIN prov_rubros provr ON provp.id_rubro = provr.id OR paqrpp.id_rubro = provr.id LEFT JOIN sys_destacados sysd ON paqp.id = sysd.id_padre AND sysd.id_referencia = '.$this->referencia.' LEFT JOIN paq_paquetes_tipo paqpt ON paqp.id_paquete_tipo = paqpt.id WHERE sysd.id_tipo = 1 AND paqp.estado = 3 GROUP BY paqp.id';
		*/
		$sql = 'SELECT p.id_paquete id, p.zonas, p.id_minisitio, p.proveedor organizador, p.titulo, p.precio, p.precio_tipo, p.rubros, p.tipo AS paquete_tipo, p.rubro, p.sucursal, p.id_sucursal, p.id_rubro, CONCAT(p.tipo,"/",p.rubro,"/",p.proveedor,"/",p.id_paquete,"/",p.titulo) folders, "http://casamientosonline.espacioeventos.com/" address
						FROM site_paquetes_activos p
						LEFT JOIN sys_destacados d ON p.id_paquete = d.id_padre AND d.id_referencia = '.$this->referencia.'
						WHERE d.id_tipo = 1
						AND d.activo = 1
						AND p.id_sucursal = ' . $this->db->escape($id_sucursal) . '
						GROUP BY p.id_paquete';
		$res = $this->db->queryAll($sql);
		foreach($res as $dato){
			$dato["url"] = $dato["address"] . $dato["folders"];
			$dev[] = $dato;
		}
		return $dev;
	}

	public function get_filtros_aplicados($filtros = ''){
		$this->load->helper('common_helper');

		//no hay ningun filtro aplicado
		if (empty($filtros)){
			return 0;
		}
		//proceso las opciones
		$opciones = doubleExplode('|', '-', $filtros);

		$ids = '';
		foreach ($opciones as $opcs){
			if($opcs) foreach ($opcs as $o){
				if (!empty($o)){
					$ids[] = $o;
				}
			}
		}
		
		if($ids){
			$sql = 'SELECT f.id id_filtro, f.caracteristica filtro, o.id id_opcion, o.opcion, f.excluyente
						FROM paq_opciones o
						JOIN paq_caracteristicas f ON o.id_caracteristica = f.id
						WHERE o.id IN ?
						ORDER BY f.orden, o.orden';

			$query = $this->db->query($sql, array($ids));
			$res = $query->result_array();
		}else{
			$res = array();
		}

		return $res;
	}

	public function get_paquetes($id_rubro = 0, $orden = 'nivel', $cantidad = 0, $pagina = 1, $filtros = '', $id_minisitio = 0, $ids_not = '', $filtros_primarios = array(), $id_grupo = FALSE){
		$this->load->library('parseo_library');

		$pagina = ($pagina<1)? 1 : $pagina;
		$cantidad = ($cantidad<0)? 25 : $cantidad;

		$orden = str_replace("-DESC"," DESC",$orden);

		$and = $join = '';

		if (!empty($filtros)){
			$opc = $this->get_filtros_aplicados($filtros);
			foreach($opc as $o){
				$and .= ' AND p.id_paquete IN (SELECT DISTINCT id_paquete FROM site_paquetes_activos_filtros WHERE id_opcion = '.$o['id_opcion'].') ';
			}
		}

		if (isset($filtros_primarios['invitados_hasta']) && $filtros_primarios['invitados_hasta']){
			$and .= ' AND p.invitados <= ' . $this->db->escape($filtros_primarios['invitados_hasta']) . ' AND (p.maximo >= ' . $this->db->escape($filtros_primarios['invitados_hasta']) . ' OR p.maximo = 0) ';
		}
		if (isset($filtros_primarios['zona'])&&$filtros_primarios['zona']){
			$and .= ' AND paqrpz.id_zona = ' . $this->db->escape($filtros_primarios['zona']);
			$join .= ' LEFT JOIN paq_rel_paquetes_zonas paqrpz ON p.id_paquete = paqrpz.id_paquete';
		}
		if (isset($filtros_primarios['tipo_evento'])&&$filtros_primarios['tipo_evento']){
			$and .= ' AND p.id_paquete_tipo = ' . $this->db->escape($filtros_primarios['tipo_evento']);
		}
		if (isset($filtros_primarios['precio_tipo'])&&$filtros_primarios['precio_tipo']){
			$and .= ' AND p.id_precio_tipo = ' . $this->db->escape($filtros_primarios['precio_tipo']);
		}
		if ((isset($filtros_primarios['precio_desde'])&&$filtros_primarios['precio_desde']) || (isset($filtros_primarios['precio_hasta'])&&$filtros_primarios['precio_hasta'])){
			$join .= ' LEFT JOIN sys_monedas sysmo ON p.id_moneda = sysmo.id';
			
			if (isset($filtros_primarios['precio_desde'])&&$filtros_primarios['precio_desde']){
				$and .= ' AND FLOOR(p.precio_valor/sysmo.cambio) >= ' . $this->db->escape($filtros_primarios['precio_desde']);
			}
			
			if (isset($filtros_primarios['precio_hasta'])&&$filtros_primarios['precio_hasta']){
				$and .= ' AND FLOOR(p.precio_valor/sysmo.cambio) <= ' . $this->db->escape($filtros_primarios['precio_hasta']);
			}
		}
		if (isset($filtros_primarios['term'])&&$filtros_primarios['term']){
			$and .= ' AND (p.titulo LIKE \'%' . $this->db->escape_like_str($filtros_primarios['term']) . '%\' OR p.contenido LIKE \'%' . $this->db->escape_like_str($filtros_primarios['term']) . '%\' OR p.proveedor LIKE \'%' . $this->db->escape_like_str($filtros_primarios['term']) . '%\' OR provppart.proveedor LIKE \'%' . $this->db->escape_like_str($filtros_primarios['term']) . '%\' OR p.rubro LIKE \'%' . $this->db->escape_like_str($filtros_primarios['term']) . '%\' OR provrpart.rubro LIKE \'%' . $this->db->escape_like_str($filtros_primarios['term']) . '%\')';
			$join .= ' LEFT JOIN paq_rel_paquetes_proveedores paqrpp ON p.id_paquete = paqrpp.id_paquete
			LEFT JOIN site_proveedores_activos provppart ON paqrpp.id_proveedor = provppart.id_proveedor
			LEFT JOIN prov_rubros provrpart ON paqrpp.id_rubro = provrpart.id';
		}
		if(isset($filtros_primarios['id_sucursal'])&&$filtros_primarios['id_sucursal']){
			$and .= 'AND p.id_sucursal = ' . $this->db->escape($filtros_primarios['id_sucursal']) . ' ';
		}

		if($id_grupo){
			$and .= ' AND nn.id_ref = ' . $this->db->escape($id_grupo) . ' ';
		}

		$and .= (($id_minisitio)? ' AND p.id_minisitio = ' . $this->db->escape($id_minisitio) . (($ids_not) ? ' AND p.id_paquete NOT IN (' . $ids_not . ')' : '') : (($id_rubro) ? ' AND p.id_rubro = ' . $id_rubro . ' ' : ''));
		$group_by = ' GROUP BY p.id_paquete';
		$sql = 'SELECT SQL_CALC_FOUND_ROWS p.*, p.id_paquete id_producto, "paquetes" tipo, "http://casamientosonline.espacioeventos.com/" address, CONCAT(p.tipo,"/",p.rubro,"/",p.proveedor,"/",p.id_paquete,"/",p.titulo) folders, pp.logo, pm.short_url subdominio
						FROM site_paquetes_activos p
						INNER JOIN prov_proveedores pp ON pp.id = p.id_proveedor
						LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
						LEFT JOIN new_nav nn ON nn.id_rubro = p.id_rubro AND nn.nivel = 2
						' . $join . '
						WHERE 1
						' . $and . $group_by . '
						ORDER BY ' . $orden . ', posicion '. (($cantidad == 0)? '' : ' LIMIT ' . ($pagina-1) * $cantidad . ',' . $cantidad);

		$query = $this->db->query($sql);
		$ret = $query->result_array();

		$dev = NULL;
		foreach($ret as $dato){
			$dato["url"] = $dato["address"] . $dato["folders"];
			$dev[] = $dato;
		}

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
        $this->resultados = $query2->row()->rows;

        $arr = array();
        if(!empty($dev)) foreach ($dev as $k => $el) {
        	$arr[$k] = $el;
        	$arr[$k]['seo_url'] = $this->parseo_library->clean_url($el['titulo']);
        	$arr[$k]['seo_rubro'] = $this->parseo_library->clean_url($el['rubro']);
        }
        $dev = $arr;

		return $dev;
	}

	//-- PAQUETES DE OTROS PROVEEDORES AL Q FORMA PARTE --//
	public function get_paquetes_integrados($orden = 'nivel', $cantidad= 0 , $id_minisitio = 0){
		$this->load->library('parseo_library');

		$pagina = 1;
		$cantidad = ($cantidad<0)?25:$cantidad;
		$ret = NULL;

		$sql = "SELECT p.id_proveedor as id 
			FROM paq_rel_paquetes_proveedores pp 
			INNER JOIN paq_paquetes p ON p.id = pp.id_paquete 
			INNER JOIN prov_minisitios ms ON ms.id_proveedor = pp.id_proveedor 
			WHERE ms.id = ? AND p.estado IN (3, -1) AND p.id_proveedor != pp.id_proveedor
			GROUP BY p.id_proveedor ";

		$query = $this->db->query($sql, array($id_minisitio));
		$paq = $query->result_array();
		$array = ''; 
		$coma = '';
		foreach($paq as $p){
			$array .= $coma.$p['id'];
			$coma = ',';
		}

		if($array !=""){
			$sql  = " SELECT SQL_CALC_FOUND_ROWS p.*, p.id_paquete id_producto, 'paquetes' tipo, 'http://casamientosonline.espacioeventos.com/' address, CONCAT(p.tipo,'/',p.rubro,'/',p.proveedor,'/',p.id_paquete,'/',p.titulo) folders
			, CONCAT('Organiza este paquete: <strong>', p.proveedor,'</strong>') as contenido, prop.logo, ms.short_url subdominio
			FROM site_paquetes_activos p
			INNER JOIN prov_proveedores prop ON prop.id = p.id_proveedor
			INNER JOIN paq_rel_paquetes_proveedores pp ON pp.id_paquete = p.id_paquete
			INNER JOIN prov_minisitios ms ON ms.id_proveedor = pp.id_proveedor
			WHERE p.id_proveedor IN (" . $array . ") AND ms.id = ?
			GROUP BY p.id_paquete
			ORDER BY " . $orden;
			$sql .= (($cantidad == 0)? '' : ' LIMIT ' . ($pagina-1) * $cantidad . ',' . $cantidad);

			$query = $this->db->query($sql, array($id_minisitio));
			$res = $query->result_array();
			foreach($res as $dato){
				$dato["url"] = $dato["address"] . $dato["folders"];
				$ret[] = $dato;
			}
			
			$query2 = $this->db->query('SELECT FOUND_ROWS() rows');

        	$this->resIntegrados = $query2->row()->rows;
		}

		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el){
			$arr[$k] = $el;
			$arr[$k]['seo_url'] = $this->parseo_library->clean_url($el['titulo']);
			$arr[$k]['seo_rubro'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		return $ret;
	}

	public function get_filtros_pendientes($id_rubro, $filtros = ''){
		$this->load->helper('common_helper');

		$and = '';

		if (!empty($filtros)){
			//proceso las opciones seleccionadas
			$opciones = $this->get_filtros_aplicados($filtros);

			$ids = '';
			$exc = '';
			foreach ($opciones as $o){
				$ids .= $o['id_opcion'].',';
				if ($o['excluyente'] == 1){
						$exc .= $o['id_filtro'].',';
				}
			}

			$prods = $this->get_paquetes($id_rubro,'nivel',0,0,$filtros);
			if(!$prods) return NULL;

			$ids_paq = array();
			foreach($prods as $p){
				array_push($ids_paq,$p['id_paquete']);
			}

			$and = '';
			if($ids){
				$and.= ' AND f.id_opcion NOT IN ('.substr($ids,0,-1).')';
			}
			$and .= ' AND p.id_paquete IN ('.implode(',',array_unique($ids_paq)).')';
			
			if ($exc){
				$and .= ' AND f.id_filtro NOT IN ('.substr($exc,0,-1).') ';
			}
		}

		$sql = 'SELECT f.id_filtro, f.filtro, f.id_opcion, f.opcion, COUNT(DISTINCT f.id_paquete) total
						FROM site_paquetes_activos_filtros f
						JOIN site_paquetes_activos p ON f.id_paquete = p.id_paquete AND p.id_rubro = ?
						WHERE 1
						'.$and.'
						GROUP BY f.id_opcion
						ORDER BY f.orden_filtro, f.filtro, f.orden_opcion';

		$query = $this->db->query($sql, array($id_rubro));
		$res = $query->result_array();

		$ret = array();
		$i = -1;
		$id = 0;
		foreach($res as $r){
			if ($id != $r['id_filtro']){
				$id = $r['id_filtro'];
				$i++;
				$j=0;
			}
			$ret[$i]['id'] = $r['id_filtro'];
			$ret[$i]['filtro'] = $r['filtro'];
			$ret[$i]['opciones'][$j]['id'] = $r['id_opcion'];
			$ret[$i]['opciones'][$j]['opcion'] = $r['opcion'];
			$ret[$i]['opciones'][$j]['total'] = $r['total'];
			$j++;
		}
		return $ret;
	}

	public function get_paquete($id_paquete){
		$this->load->library('parseo_library');

		$sql = 'SELECT 
					p.*
					, CONCAT("http://casamientosonline.espacioeventos.com/",p.tipo,"/",p.rubro,"/",p.proveedor,"/",p.id_paquete,"/",p.titulo) url, spa.imagen logo
					, pq.valido_dias
					, pq.valido_turnos
					, GROUP_CONCAT(DISTINCT CONCAT(paqc.caracteristica,"*",(SELECT GROUP_CONCAT(DISTINCT po.opcion SEPARATOR ", ") FROM paq_opciones po LEFT JOIN paq_rel_paquetes_opciones paqrpo2 ON po.id = paqrpo2.id_opcion WHERE po.id_caracteristica = paqc.id AND paqrpo2.id_paquete = pq.id )) ORDER BY paqc.id SEPARATOR "`") as caracteristicas
					, pm.short_url subdominio
				FROM site_paquetes_activos p
				LEFT JOIN site_proveedores_activos spa ON spa.id_proveedor = p.id_proveedor
				LEFT JOIN paq_paquetes pq ON pq.id = p.id_paquete
				LEFT JOIN prov_proveedores provpint ON p.id_proveedor = provpint.id
				LEFT JOIN prov_rubros provrint ON provpint.id_rubro = provrint.id
				LEFT JOIN paq_rel_paquetes_opciones paqrpo ON p.id_paquete = paqrpo.id_paquete
                LEFT JOIN paq_opciones paqo ON paqrpo.id_opcion = paqo.id
                LEFT JOIN paq_caracteristicas paqc ON paqo.id_caracteristica = paqc.id AND (paqc.id_rubro = provrint.id OR paqc.id_rubro = provrint.padre)
                LEFT JOIN paq_opciones paqoint ON paqc.id = paqoint.id_caracteristica
                LEFT JOIN paq_caracteristicas paqcgen ON paqo.id_caracteristica = paqcgen.id AND paqcgen.id_rubro IS NULL
                LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.id_paquete = ? ';

		$query = $this->db->query($sql, array($id_paquete));
		$res = $query->row_array();
		
		if($res['rubro']) $res['rubro_seo'] = $this->parseo_library->clean_url($res['rubro']);

		return $res;
	}

	public function get_caracteristicas_paquetes_rubros($id_paquete = NULL){
		if(!$id_paquete) return NULL;

		$sql = "SELECT 
					provr.id AS id_rubro
					, provr.rubro,provp.id AS id_proveedor
				    , provr.id_sucursal,sys_s.sucursal,provr.rubro,provp.id_empresa,ms.proveedor
					, IF(provp.proveedor IS NULL,paqrpp.nombre,ms.proveedor) AS proveedor
					, paqrpp.detalle
					, GROUP_CONCAT(DISTINCT CONCAT(paqc.caracteristica,'*',(SELECT GROUP_CONCAT(DISTINCT po.opcion SEPARATOR ', ') FROM paq_opciones po LEFT JOIN paq_rel_paquetes_opciones paqrpo2 ON po.id = paqrpo2.id_opcion WHERE po.id_caracteristica = paqc.id AND paqrpo2.id_paquete = paqp.id )) SEPARATOR '`') as caracteristicas
				FROM paq_paquetes paqp
				LEFT JOIN paq_rel_paquetes_proveedores paqrpp ON paqp.id = paqrpp.id_paquete
				LEFT JOIN prov_proveedores provp ON paqrpp.id_proveedor = provp.id
				LEFT JOIN prov_minisitios ms ON ms.id_proveedor = provp.id
				LEFT JOIN prov_rubros provr ON paqrpp.id_rubro = provr.id
				LEFT JOIN paq_rel_paquetes_opciones paqrpo ON paqp.id = paqrpo.id_paquete
				LEFT JOIN paq_opciones paqo ON paqrpo.id_opcion = paqo.id
				LEFT JOIN paq_caracteristicas paqc ON paqo.id_caracteristica = paqc.id AND (provr.id = paqc.id_rubro OR provr.padre = paqc.id_rubro)
				LEFT JOIN sys_urls sysu ON provp.id = sysu.id_padre AND sysu.id_referencia = 5 AND sysu.estado = 2
				LEFT JOIN sys_sucursales sys_s ON sys_s.id = provr.id_sucursal
				WHERE paqp.id = ?
				GROUP BY provr.id
				ORDER BY provr.orden,provr.rubro";

		$query = $this->db->query($sql, array($id_paquete));
		$res = $query->result_array();
		
		return $res;
	}

	public function get_paquete_vencido($id_paquete){
		$this->load->library('parseo_library');

		$sql = " SELECT ms.id id_minisitio, paq.id_proveedor, paq.id id_paquete, ms.id_rubro, r.id_sucursal, ((paq.destacado*-1)+2) nivel, ms.proveedor, TRIM(paq.paquete) titulo, paq.descripcion contenido, CONCAT(mon.simbolo,' ',replace(paq.precio,'.',',')) precio, ppt.tipo precio_tipo, CONCAT(sm.name,'_chica.',sat.extension) imagen, paq.invitados, paq.fecha_inicio_valido, paq.fecha_vencimiento_valido, paq.fecha_vencimiento_visible, paq.fecha_contratando, paq.legales legal, GROUP_CONCAT(DISTINCT r.id,'*',r.rubro) AS rubros, ifnull(GROUP_CONCAT(DISTINCT z.zona ORDER BY z.zona SEPARATOR ', '),'Todas las zonas') AS zonas, r.rubro, s.sucursal, paq.id_moneda, paq.id_paquete_tipo, paq.id_precio_tipo, paq.precio precio_valor, paq.maximo, t.tipo, ms.short_url subdominio, paq.valido_dias, paq.valido_turnos
				, GROUP_CONCAT(DISTINCT CONCAT(paqc.caracteristica,'*',(SELECT GROUP_CONCAT(DISTINCT po.opcion SEPARATOR ', ') FROM paq_opciones po LEFT JOIN paq_rel_paquetes_opciones paqrpo2 ON po.id = paqrpo2.id_opcion WHERE po.id_caracteristica = paqc.id AND paqrpo2.id_paquete = paq.id )) ORDER BY paqc.id SEPARATOR '`') as caracteristicas
				FROM paq_paquetes paq
				INNER JOIN prov_minisitios ms ON paq.id_proveedor = ms.id_proveedor
				INNER JOIN prov_rubros r2 ON r2.id = ms.id_rubro
				INNER JOIN sys_sucursales s ON s.id = r2.id_sucursal
				LEFT JOIN paq_precios_tipo ppt ON paq.id_precio_tipo = ppt.id
				LEFT JOIN sys_monedas mon ON paq.id_moneda = mon.id
				LEFT JOIN sys_rel_medias_referencias srmr ON paq.id = srmr.id_padre AND srmr.id_referencia = 8 AND srmr.orden = 1 AND srmr.estado = 2 
				LEFT JOIN sys_medias sm ON srmr.id_media = sm.id
				LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id
				LEFT JOIN paq_rel_paquetes_proveedores rp ON paq.id = rp.id_paquete
				LEFT JOIN prov_rubros r ON ms.id_rubro = r.id OR rp.id_rubro = r.id
				LEFT JOIN paq_rel_paquetes_zonas pz ON paq.id = pz.id_paquete
				LEFT JOIN paq_zonas z ON pz.id_zona = z.id
				LEFT JOIN prov_dineromail dm ON ms.id = dm.id_minisitio AND dm.operar = 1 AND dm.estado = 3
				LEFT JOIN paq_paquetes_tipo t ON paq.id_paquete_tipo = t.id
				LEFT JOIN prov_proveedores provpint ON ms.id_proveedor = provpint.id
				LEFT JOIN prov_rubros provrint ON provpint.id_rubro = provrint.id
				LEFT JOIN paq_rel_paquetes_opciones paqrpo ON paq.id = paqrpo.id_paquete
                LEFT JOIN paq_opciones paqo ON paqrpo.id_opcion = paqo.id
                LEFT JOIN paq_caracteristicas paqc ON paqo.id_caracteristica = paqc.id AND (paqc.id_rubro = provrint.id OR paqc.id_rubro = provrint.padre)
				WHERE paq.estado = 2 AND paq.id = ?
				GROUP BY paq.id ";
		
		$query = $this->db->query($sql, array($id_paquete));
		$res = $query->row_array();
		
		if($res['rubro']) $res['rubro_seo'] = $this->parseo_library->clean_url($res['rubro']);

		return $res;
	}

	public function get_organiza_paquetes($rubros){
		$sql = "SELECT organiza_paquetes FROM prov_rubros WHERE id IN (" . $rubros . ") GROUP BY organiza_paquetes HAVING organiza_paquetes = 1 ";
		
		$query = $this->db->query($sql);
		$res = $query->row_array();

		return $res;
	}
}