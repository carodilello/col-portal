<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners_model extends CI_Model{
	public $id;
	public $medir = 1;
	public $referencia = 1;
	public $resultados = 0;

	public function __construct(){
		parent::__construct();
	}
	
	public function limpiarArray($data){
		//limpieza de las variables vacias
		foreach($data as $k=>$v){
			if (empty($v)){
				unset($data[$k]);
			} else {
				$data[$k] = trim($v);
			}
		}
		return $data;
	}

	public function views($id_seccion){
		$sql = "SELECT
			b.id_producto
			, b.id_banner
			, b.url
			, b.id_tipo
			, b.banner
			, b.id_pedido
			, b.width
			, b.height
			, bc.banner AS banner_complemento
			, bc.id_tipo AS tipo_complemento
		FROM site_banners_activos b
		INNER JOIN site_banners_activos_secciones s ON b.id_pedido = s.id_pedido
		LEFT JOIN sys_banners_complemento bc ON bc.id_banner = b.id_banner
		WHERE s.id_seccion = ?
		GROUP BY b.id_producto ";

		$ret = array();

		if ($query = $this->db->query($sql, array((int)$id_seccion))){
		    
		    $banners = $query->result_array();

		    $target = '_self';

		    ##-- Se ferifica el dispositivo, en caso q sea Ipod, Ipad o Iphone, se remplaza banner x banner complementario en caso en q lo tenga.--##
			// ipad
			if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strpos($_SERVER['HTTP_USER_AGENT'],'iPad')){
				foreach($banners as $k => $item){
					if(!empty($item['banner_complemento']) && !empty($item['tipo_complemento'])){
						$banners[$k]['banner'] = $item['banner_complemento'];
						$banners[$k]['id_tipo'] = $item['tipo_complemento'];
					}
				}					
			}
			##-- --##

			foreach ($banners as $banner){
				//if($this->medir) $this->estadistica_banners_view($banner); //mido impresión (DESABILITADO EN PROCESO DE DESARROLLO)

				$link = base_url('/home/banner_hit/' . $banner["id_pedido"] . '/' . $banner["id_banner"] . '?backto=' . urlencode(str_replace('://', '*', $banner['url'])));

				switch($banner['id_tipo']){
					case 1: // 7,0,19,0 SWF
						$ret[$banner['id_producto']] = '<object width="'.$banner['width'].'" height="'.$banner['height'].'" type="application/vnd.adobe.flash-movie" data="http://www.casamientosonline.com/~media/banners/'.$banner['banner'].'?target='.$target.'&link='.urlencode($link).'">
						<param name="movie" value="http://www.casamientosonline.com/~media/banners/'.$banner['banner'].'?target='.$target.'&link='.urlencode($link).'" /><param name="quality" value="high" />
						<param name="allowScriptAccess" value="always" />
						<param name="wmode" value="opaque" />
						<param name="scale" value="noscale">
						<embed src="http://www.casamientosonline.com/~media/banners/'.$banner['banner'].'?target='.$target.'&link='.urlencode($link).'" width="'.$banner['width'].'" height="'.$banner['height'].'" allowScriptAccess="always" wmode="opaque" quality="high" scale="noscale" />
						</object>';
						break;

					case 2: case 3: case 4: // IMG
						$ret[$banner['id_producto']] = '<a href="'.$link.'" target="'.$target.'"><img width="'.$banner['width'].'" height="'.$banner['height'].'" src="http://media.casamientosonline.com/banners/'.$banner['banner'].'" alt="Banner"></a>';
						break;
					
					case 5: // Script
						$ret[$banner['id_producto']] = $banner['banner'];
						break;
				}
			}
		}

		return $ret;
	}
	
	public function view($data){
		
		$join = $and = $html = '';
		
		$target = '_blank';
		
		//banner externo de un proveedor
		if(!empty($data['id_pedido']) && !empty($data['id_proveedor'])){
			$and = ' AND b.id_pedido = ' . $this->db->escape($data['id_pedido']) .'
							AND b.id_proveedor = ' . $this->db->escape($data['id_proveedor']);
		}
		
		//banner interno de COL
		if(!empty($data['id_producto']) && !empty($data['id_seccion'])){
			$and = ' AND b.id_producto = ' . $this->db->escape($data['id_producto']) . '
							AND s.id_seccion = ' . $this->db->escape($data['id_seccion']);
			$join = ' JOIN site_banners_activos_secciones s ON b.id_pedido = s.id_pedido';
		}
		
		$sql = 'SELECT b.id_banner, b.url, b.id_tipo, b.banner, b.id_pedido
						FROM site_banners_activos b
						'.$join.'
						WHERE b.width = ' . $this->db->escape($data['width']) . '
						AND b.height = ' . $this->db->escape($data['height']) . '
						' . $and . '
						LIMIT 1';
		
		/*
		if (!empty($and) && ($banner = $this->db->queryRow($sql))){
			global $navegacion;
			
			if ($this->medir){//mido impresion
				$this->estadistica_banners_view($banner);}
			
			$link = 'http://'.$_SERVER['HTTP_HOST'].'/banner_hit.php?id_pedido='.$banner['id_pedido'].'&id_banner='.$banner['id_banner'].'&url='. urlencode($banner['url']);
			
			switch($banner['id_tipo']){
				//SWF
				case 1:
					$html = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="'.$data['width'].'" height="'.$data['height'].'"><param name="movie" value="http://media.casamientosonline.com/banners/'.$banner['banner'].'?target='.$target.'&link='.urlencode($link).'" /><param name="quality" value="high" /><param name="allowScriptAccess" value="always"><param name="wmode" value="opaque"><embed src="http://media.casamientosonline.com/banners/'.$banner['banner'].'?target='.$target.'&link='.urlencode($link).'" width="'.$data['width'].'" height="'.$data['height'].'" allowScriptAccess="always" wmode="opaque"></embed></object>';
					break;
				//IMG
				case 2: case 3: case 4:
					$html = '<a href="'.$link.'" target="'.$target.'"><img valign="middle" width="'.$data['width'].'" height="'.$data['height'].'" border="0" src="http://media.casamientosonline.com/banners/'.$banner['banner'].'"></a>';
					break;
				//script
				case 5:
					$html = $banner['banner'];
					break;
			}
	
		} */

		return $html;
	}
	
	public function click($data){
		if (!empty($data['id_pedido']) && !empty($data['id_banner']) && !empty($data['url'])){
			if ($this->medir){ //mido clicks
				// $this->estadistica_banners_hits($data); // DESHABILITAMOS ESTO PARA QUE NO AFECTEN LOS REPORTES DE CLICKS DE BANNERS
			}
			return urldecode($data['url']);
		}else{
			return $_SERVER['HTTP_HOST'];
		}
	}
	
	public function estadistica_banners_view($data){
		$sql1 = "SELECT id FROM sys_banners_views WHERE id_pedido = ? AND id_banner = ? AND mes = MONTH(NOW()) AND ano = YEAR(NOW()) ";
		
		$query = $this->db->query($sql1, array((int) $data['id_pedido'], (int)$data['id_banner']));
		$res = $query->row(0);
		
		$id = $res->id;

		if($id){
			$sql = 'UPDATE sys_banners_views SET total = (total + 1) WHERE id = "' . $this->db->escape((int) $id) . '"';
		}else{
			$sql  = 'INSERT INTO sys_banners_views (id_pedido, id_banner, mes, ano, total) ';
			$sql .= ' VALUES(' . $this->db->escape((int) $data['id_pedido']) . ',' . $this->db->escape((int) $data['id_banner']) . ', MONTH(NOW()), YEAR(NOW()),1)';
		}

		$ret = $this->db->query($sql);

		return $ret;
	}
	
	public function estadistica_banners_hits($data){
		$sql1 = "SELECT id FROM sys_banners_hits WHERE id_pedido = '" . $this->db->escape((int)$data['id_pedido']) . "' AND id_banner = '" . $this->db->escape((int)$data['id_banner']) . "' AND mes = MONTH(NOW()) AND ano = YEAR(NOW()) ";
		
		$query = $this->db->query($sql1);

		$res = $query->row(0);
		


		$id = isset($res->id) ? $res->id : NULL;

		if($id){
			$sql = 'UPDATE sys_banners_hits SET total = (total + 1) WHERE id = "' . $this->db->escape((int)$id) . '"';
		}else{
			$sql  = 'INSERT INTO sys_banners_hits (id_pedido, id_banner, mes, ano, total) ';
			$sql .= ' VALUES(' . $this->db->escape((int) $data['id_pedido']) . ', ' . $this->db->escape((int) $data['id_banner']) . ', MONTH(NOW()), YEAR(NOW()),1)';
		}
		if($res = $this->db->query($sql)){
			return $res;	
		}else{
			redirect(base_url('/'));
		}
	}
}