<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model{
	public $pyp_filtros_extendido = 0;
	public $resultados = 0;
	public $vistaprevia = FALSE;

	public function __construct(){
		parent::__construct();
	}

	public function get_pyp($id_rubro, $promocion = 0, $orden = 'p.nivel', $cantidad = 0, $pagina = 1, $filtros = '', $id_tipo_destacado = 0, $id_minisitio = 0, $ids_not = '', $id_suc = 0, $todos_los_productos = FALSE, $id_grupo = FALSE, $mantener_cantidad = FALSE){
		$this->load->library('config_library');
		$this->load->library('parseo_library');

		$pagina = ($pagina<1)? 1 : $pagina;
		$cantidad = ($cantidad<0)? 25 : $cantidad;
		$id_sucursal = $this->session->userdata('id_sucursal');
		$and = $join = '';

		$orden = str_replace("-DESC"," DESC",$orden);

		if($id_minisitio){//-- VALIDADOR PARA MOSTRAR EN TOTAL 20 PRODUCTOS Y PROMOCIONES --/

			$sql_l = "SELECT SUM(productos) AS productos, SUM(promociones) AS promociones FROM ( 
			SELECT IF(promocion = 0, 1, 0) as productos, IF(promocion = 1, 1, 0) as promociones 
			FROM prov_minisitios_productos WHERE estado IN (2,3) AND id_minisitio = ? ORDER BY posicion, id DESC LIMIT 0,20 
			) AS totales";

			
			$query = $this->db->query($sql_l, array($id_minisitio));
			$res_l = $query->row_array();
			if(!$mantener_cantidad){
				if($promocion){
					$cantidad = $res_l['promociones'];
				}else{
					$cantidad = $res_l['productos'];
				}	
			}

		}

		if (!empty($filtros)){
			$opc = $this->config_library->get_filtros_aplicados($filtros,2,$id_rubro,$todos_los_productos);

			foreach($opc as $o){
				$and .= ' AND p.id_producto IN (SELECT DISTINCT id_producto FROM site_productos_activos_filtros WHERE id_opcion = ' . $this->db->escape($o['id_opcion']) . ') ';
			}

			if($this->pyp_filtros_extendido){
				$opc = $this->config_library->get_filtros_aplicados($filtros,1,$id_rubro);
				foreach($opc as $o){
					if((int)$o['id_opcion']){
						$and .= ' AND p.id_minisitio IN (SELECT DISTINCT id_minisitio FROM site_proveedores_activos_filtros WHERE id_opcion = ' . $this->db->escape($o['id_opcion']) . ') ';
					}
				}
			}
		}

		if ($id_suc){
			$and .= ' AND p.id_sucursal = ' . $this->db->escape($id_suc) . ' AND p.promocion = ' . $this->db->escape($promocion) . ' ';
		} elseif ($id_minisitio){
			$and .= ' AND p.id_minisitio = ' . $this->db->escape($id_minisitio) . (($ids_not) ? ' AND p.id_producto NOT IN ('.$ids_not.')' : ' AND p.promocion = ' . $this->db->escape($promocion) . ' ');
		} elseif ($id_tipo_destacado){
			$join = ' LEFT JOIN sys_destacados d ON p.id_producto = d.id_padre AND d.id_referencia = 10 AND d.activo = 1';
			$and .= ' AND d.id_tipo = ' . $this->db->escape($id_tipo_destacado) . ' AND p.id_sucursal = ' . $this->db->escape($id_sucursal) . ' ';
		} else {
			$and .= ' AND p.id_rubro = ' . $this->db->escape($id_rubro) . ' AND p.promocion = ' . $this->db->escape($promocion) . ' ';
		}

		if($id_grupo){
			$and .= ' AND nn.id_ref = ' . $this->db->escape($id_grupo) . ' ';
		}


		if (!$this->vistaprevia){

			$sql = 'SELECT SQL_CALC_FOUND_ROWS p.*, IF(p.promocion,"promociones","productos") tipo, pp.logo, pmp.regalo, pmp.oferta, pm.short_url subdominio
					, IFNULL((SELECT COUNT(id_proveedor)*30 cant FROM new_faq_respuestas nfr
						WHERE nfr.id_proveedor = p.id_proveedor
						GROUP BY nfr.id_proveedor), 0)+p.orden puntuacion 
					FROM site_productos_activos p
					LEFT JOIN prov_minisitios_productos pmp ON pmp.id = p.id_producto
					INNER JOIN prov_proveedores pp ON pp.id = p.id_proveedor
					LEFT JOIN site_proveedores_activos p_act ON p_act.id_proveedor = pp.id
					LEFT JOIN new_nav nn ON nn.id_rubro = p.id_rubro
					LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
					' . $join . ' WHERE 1 ' . $and . '
					GROUP BY p.id_producto
					ORDER BY ' . $orden . ' ,puntuacion, p_act.nivel, p_act.orden DESC';
			//$sql.= ' ORDER BY '.$orden.', p.orden '.

			$sql .= (($cantidad == 0)? '' : ' LIMIT '.($pagina-1) * $cantidad . ',' . $cantidad);


		}else{
			$sql = 'SELECT 
						SQL_CALC_FOUND_ROWS p.*
						, pm.id_proveedor
						, IF(p.promocion,"promociones","productos") tipo
						, (SELECT CONCAT(sm.name,".",sat.extension) FROM sys_rel_medias_referencias srmr
							LEFT JOIN sys_medias sm ON (sm.id = srmr.id_media AND sm.eliminado = 0)
							LEFT JOIN sys_archivos_tipo sat ON sat.id = sm.id_tipo
							WHERE (srmr.id_padre = p.id AND srmr.id_referencia = 10 AND srmr.estado >= 2)
							ORDER BY srmr.orden,srmr.id DESC
							LIMIT 1) imagen
						, pp.logo
						, pm.short_url subdominio
						, r.rubro
						, p.id id_producto
						, pm.id_rubro
					FROM prov_minisitios_productos p
					LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
					INNER JOIN prov_proveedores pp ON pp.id = pm.id_proveedor
					INNER JOIN prov_rubros r ON r.id = pm.id_rubro
					WHERE 1 AND (p.estado = 2 OR p.estado = 3) ' . $and . '
					GROUP BY p.id
					ORDER BY p.posicion,p.id DESC ' . (($cantidad == 0)? '' : ' LIMIT ' . ($pagina-1) * $cantidad . ',' . $cantidad);
		}

		if($query = $this->db->query($sql)){
			$ret = $query->result_array();
		}

		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['seo_url'] = $this->parseo_library->clean_url($el['titulo']);
			$arr[$k]['seo_rubro'] = $this->parseo_library->clean_url($el['rubro']);
		}

		$ret = $arr;

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
        $this->resultados = $query2->row()->rows;
		$this->vistaprevia = FALSE;

		return $ret;
	}

	public function get_pyp_filtros_pendientes($id_rubro, $promocion = 0, $filtros = '', $no_filtro_zonas = FALSE){
		$this->load->library('config_library');
		$and = '';
		
		if (!empty($filtros)){
			//proceso las opciones seleccionadas

			$opciones = $this->config_library->get_filtros_aplicados($filtros, 2, $id_rubro);

			$ids = '';
			$exc = '';
			foreach ($opciones as $o){
				$ids .= $o['id_opcion'].',';
				if ($o['excluyente'] == 1){
						$exc .= $o['id_filtro'].',';
				}
			}
		
			$prods = $this->get_pyp($id_rubro, $promocion, 'proveedor', 0, 0, $filtros);

			$ids_prod = array();
		
			foreach($prods as $p){
				array_push($ids_prod,$p['id_producto']);
			}

			if($ids){
				$and .= ' AND f.id_opcion NOT IN (' . substr($ids,0,-1) . ')';
			}
			if($ids_prod){
				$and .= ' AND p.id_producto IN (' . implode(',',array_unique($ids_prod)) . ')';
			}

			if($exc){
				$and .= ' AND f.id_filtro NOT IN (' . substr($exc,0,-1) . ') ';
			}
		}

		$sql = ' SELECT f.id_filtro, f.filtro, f.id_opcion, f.opcion, COUNT(DISTINCT f.id_producto) total
						FROM site_productos_activos_filtros f
						JOIN site_productos_activos p ON f.id_producto = p.id_producto AND p.id_rubro = ' . $this->db->escape($id_rubro) . ' AND p.promocion = ' . $this->db->escape($promocion) . '
						WHERE 1
						' . $and . '
						GROUP BY f.id_opcion
						ORDER BY f.filtro, f.orden_filtro, f.orden_opcion';

		$query = $this->db->query($sql);
		$res = $query->result_array();

		if(!$no_filtro_zonas){
			##-- Filtros por Zonas --##
			$opciones_zonas = $this->config_library->get_filtros_aplicados($filtros, 1, $id_rubro);
			$op_aplicados = $coma = '';
			if($opciones_zonas){
				foreach($opciones_zonas as $item){
					$op_aplicados .= $coma . $item['id_opcion'];
					$coma = ',';
				}
			}
			$sql = ' SELECT f.id_filtro, f.filtro, f.id_opcion, f.opcion, COUNT(DISTINCT p.id_producto) total
						FROM site_proveedores_activos_filtros f
						INNER JOIN site_productos_activos p ON p.id_minisitio = f.id_minisitio AND p.id_rubro = ' . $this->db->escape($id_rubro) . ' AND p.promocion = ' . $this->db->escape($promocion) . '
						WHERE 1 AND f.id_filtro = 36 ' . $and;
			if(!empty($op_aplicados)){
				$sql.= ' AND f.id_opcion NOT IN (' . $op_aplicados . ') ';
			}

			$sql.= ' GROUP BY f.id_opcion
					 ORDER BY f.orden_filtro, f.filtro, f.orden_opcion ';

			$query = $this->db->query($sql);
			$res_zona = $query->result_array();

			foreach($res_zona as $arr){
				array_push($res,$arr);
			}
			##-- --##
		}

		$ret = array();
		$i = -1;
		$id = 0;
		foreach($res as $r){
			if ($id != $r['id_filtro']){
				$id = $r['id_filtro'];
				$i++;
				$j=0;
			}
			$ret[$i]['id'] = $r['id_filtro'];
			$ret[$i]['filtro'] = $r['filtro'];
			$ret[$i]['opciones'][$j]['id'] = $r['id_opcion'];
			$ret[$i]['opciones'][$j]['opcion'] = $r['opcion'];
			$ret[$i]['opciones'][$j]['total'] = $r['total'];
			$j++;
		}
		return $ret; 
	}

	public function get_productos_images($id_minisitio, $promocion){
		$sql = "SELECT pmp.id as id_producto, CONCAT(sm2.name,'_chica.',sat.extension) as img 
			FROM sys_medias sm2 
			INNER JOIN sys_rel_medias_referencias sysmr ON sysmr.id_media=sm2.id  AND sysmr.id_referencia = 10 
			JOIN prov_minisitios_productos pmp ON sysmr.id_padre=pmp.id AND pmp.estado IN (2,3) 
			LEFT JOIN sys_archivos_tipo sat ON sm2.id_tipo = sat.id 
			WHERE sm2.eliminado=0 AND sm2.estado IN (2,4) AND sysmr.estado >= 2 AND sm2.id_tipo BETWEEN 58 AND 65 
			AND pmp.id_minisitio = " . $this->db->escape((int) $id_minisitio) . " AND pmp.promocion = " . $this->db->escape((int) $promocion) . "
			ORDER BY sysmr.orden ";

		$query = $this->db->query($sql);
		$res = $query->result_array();

		$arrImg = array();
		foreach($res as $item){
			$arrImg[$item['id_producto']][] = $item['img'];
		}

		return $arrImg;
	}

	public function get_producto($id_producto){
		$this->load->library('parseo_library');

		$sql = 'SELECT p.*, IF(p.promocion,"promociones","productos") tipo, pp.logo, pm.short_url subdominio
				FROM site_productos_activos p
				INNER JOIN prov_proveedores pp ON pp.id = p.id_proveedor
				LEFT JOIN prov_rubros pr ON pr.id = p.id_rubro
				LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.id_producto = ' . $this->db->escape($id_producto);

		$query = $this->db->query($sql);

		$res = $query->row_array();

		if($res['rubro']) $res['rubro_seo'] = $this->parseo_library->clean_url($res['rubro']);

		return $res;
	}

	public function get_producto_varios($id_producto){
		$this->load->library('parseo_library');

/*		$sql = 'SELECT p.*, IF(p.promocion,"promociones","productos") tipo, pp.logo, pm.short_url subdominio
				FROM site_productos_activos p
				INNER JOIN prov_proveedores pp ON pp.id = p.id_proveedor
				LEFT JOIN prov_rubros pr ON pr.id = p.id_rubro
				LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
				WHERE p.id_producto = ' . $this->db->escape($id_producto);
*/
		$sql = 'SELECT p.id_minisitio, p.id_proveedor, p.id_producto, p.id_rubro, p.id_sucursal, p.dm, p.promocion, p.nivel, 
			p.proveedor, p.titulo, p.subtitulo, p.contenido, p.precio, p.moneda, CONCAT(sm.name,"_chica.",sat.extension) imagen, p.orden, 
			p.fecha_vencimiento, p.legal, p.aclaracion, p.dm_precio, p.dm_pagos, p.rubro, p.sucursal, p.precio_viejo, 
			p.posicion, p.porcentaje_descuento, IF(p.promocion,"promociones","productos") tipo, pp.logo, pm.short_url subdominio
			FROM site_productos_activos p
			INNER JOIN sys_rel_medias_referencias srm on srm.id_padre = p.id_producto
			INNER JOIN sys_medias sm ON sm.id = srm.id_media
			INNER JOIN prov_proveedores pp ON pp.id = p.id_proveedor
			LEFT JOIN prov_rubros pr ON pr.id = p.id_rubro
			LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
			LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
			WHERE p.id_producto =' . $this->db->escape($id_producto);


		$query = $this->db->query($sql);

		$res = $query->result_array();

/*		if($res['rubro']) $res['rubro_seo'] = $this->parseo_library->clean_url($res['rubro']);*/

		return $res;
	}

	public function tiene_producto_activo($id_minisitio, $id_tipo_producto){
		$sql = "SELECT COUNT(*)
				FROM prov_minisitios ms
				INNER JOIN prov_pedidos p ON p.id_proveedor = ms.id_proveedor
				INNER JOIN sys_productos sp ON sp.id = p.id_producto
				WHERE p.estado = 9 AND ms.id = " . $this->db->escape((int) $id_minisitio) . " AND sp.id_producto_tipo = " . $this->db->escape((int) $id_tipo_producto);

		$query = $this->db->query($sql);

		return $query->row_array();
	}

	public function get_producto_vencido($id_producto){
		$sql = " SELECT
		ms.id as id_minisitio, ms.id_proveedor, ms.id_rubro, ms.proveedor, ms.short_url subdominio
		, pmp.id id_producto, pmp.promocion, pmp.subtitulo, pmp.contenido, IFNULL(pmp.precio,'0.00') precio, pmp.valido_hasta fecha_vencimiento, pmp.legal, pmp.aclaracion, pmp.dm_precio, pmp.dm_pagos, pmp.precio_viejo, pmp.posicion ,IF(pmp.promocion,'promociones','productos') tipo
		, r.id_sucursal
		, IF(des.id IS NULL,2,1) nivel, TRIM(fnStripTags(fnHtmlUnencode(pmp.titulo))) titulo
		, (SELECT CONCAT(sm2.name,'_chica.',sat.extension) FROM sys_medias sm2
			INNER JOIN sys_rel_medias_referencias sysmr ON sysmr.id_media=sm2.id  AND sysmr.id_referencia = 10
			LEFT JOIN sys_archivos_tipo sat ON sm2.id_tipo = sat.id
			WHERE sm2.eliminado=0 AND sm2.estado IN (2,4) AND sysmr.estado >= 2 AND sm2.id_tipo BETWEEN 58 AND 65 AND sysmr.id_padre=pmp.id
			ORDER BY sysmr.orden LIMIT 1) AS imagen
		, des.orden, r.rubro, s.sucursal, pp.logo
		FROM prov_minisitios ms
		INNER JOIN prov_proveedores pp ON pp.id = ms.id_proveedor
		INNER JOIN prov_rubros r ON r.id = ms.id_rubro
		INNER JOIN sys_sucursales s ON s.id = r.id_sucursal
		INNER JOIN prov_minisitios_productos pmp ON ms.id = pmp.id_minisitio
		LEFT JOIN sys_rel_medias_referencias srmr ON pmp.id = srmr.id_padre AND srmr.id_referencia = 10 AND srmr.orden = 1 AND srmr.estado = 2 
		LEFT JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0
		LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id
		LEFT JOIN sys_destacados des ON pmp.id = des.id_padre AND des.id_referencia = 10 AND des.activo = 1 AND des.id_tipo = 3
		WHERE pmp.id = " . $this->db->escape($id_producto) . "
		GROUP BY pmp.id ";

		$query = $this->db->query($sql);
		
		$res = $query->row_array();

		if($res['rubro']) $res['rubro_seo'] = $this->parseo_library->clean_url($res['rubro']);

		return $res;
	}

	public function buscar_pyp($search, $promocion = 0, $orden = 'p.nivel', $cantidad = 0, $pagina = 1, $id_sucursal = 1){
		$and = ' AND p.id_sucursal = ' . $this->db->escape($id_sucursal) . ' AND p.titulo LIKE "%' . $this->db->escape_like_str($search) . '%" AND p.promocion = ' . $this->db->escape($promocion) . ' ';


		$sql = 'SELECT SQL_CALC_FOUND_ROWS p.*, IF(p.promocion,"promociones","productos") tipo, pp.logo, pm.short_url subdominio
				FROM site_productos_activos p
				INNER JOIN prov_proveedores pp ON pp.id = p.id_proveedor
				LEFT JOIN prov_minisitios pm ON pm.id_proveedor = p.id_proveedor
				LEFT JOIN site_proveedores_activos p_act ON p_act.id_proveedor = pp.id
				WHERE 1 ' . $and . '
				GROUP BY p.id_producto
				ORDER BY '.$orden.' ,p.orden, p_act.nivel, p_act.orden DESC ';

		$sql.= (($cantidad == 0)? '' : ' LIMIT '.($pagina-1)*$cantidad.','.$cantidad);


		$query = $this->db->query($sql);
		$res = $query->result_array();
		
		$arr = array();
		if(!empty($res)) foreach ($res as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['seo_url'] = $this->parseo_library->clean_url($el['titulo']);
			$arr[$k]['seo_rubro'] = $this->parseo_library->clean_url($el['rubro']);
		}

		$res = $arr;

		$query2 = $this->db->query('SELECT FOUND_ROWS() rows');
        $this->resultados = $query2->row()->rows;

		return $res;
	}
}