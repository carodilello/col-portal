<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generales_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	#id_referencia: 1->proveedores, 2->paquetes, 3->productos, 4->promociones
	public function get_mas_consultados($id_referencia = 1, $id_sucursal = 1, $cantidad = 1, $id_rubro = 0, $left_join = '', $select = '', $where = ''){
		$and = ($id_rubro == 0)? ' AND smc.orden = 1 ' : ' AND smc.id_rubro = ' . $this->db->escape($id_rubro);
		if($where) $and .= $where;
		$sql = 'SELECT * ';
		if($select) $sql .= $select;
		$sql .= 'FROM site_mas_consultados smc ';
		if($left_join) $sql .= $left_join;
		$sql .= 'WHERE smc.id_referencia = ' . $this->db->escape($id_referencia) . '
						AND smc.id_sucursal = ' . $this->db->escape($id_sucursal) . '
						'.$and.'
						ORDER BY smc.total
						'.(($cantidad == 0)? '' : 'LIMIT ' . $cantidad);

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_como_mostrar($id, $tipo){
		$sql = " SELECT ms.mostrar_inactivo, ms.id_rubro, r.rubro, r.id_sucursal, suc.sucursal ";
		$sql.= " FROM prov_minisitios ms ";
		$sql.= " INNER JOIN prov_rubros r ON r.id = ms.id_rubro ";
		$sql.= " INNER JOIN sys_sucursales suc ON suc.id = r.id_sucursal ";

		if($tipo == 'Paquete'){
			$sql.= " INNER JOIN paq_paquetes paq ON paq.id_proveedor = ms.id_proveedor ";
			$sql.= " WHERE paq.id = ? ";
		}elseif($tipo == 'Promocion' || $tipo == 'Producto'){
			$sql.= " INNER JOIN prov_minisitios_productos pmp ON pmp.id_minisitio = ms.id ";
			$sql.= " WHERE pmp.id = ? ";
		}
		elseif($tipo == 'Galeria'){
			$sql.= " INNER JOIN site_proveedores_activos spa ON spa.id_minisitio = ms.id ";
			$sql.= " WHERE ms.id = ? ";
		}

		$query = $this->db->query($sql, array((int)$id));
		return $query->row_array();
	}

	public function get_zonas_e_items(){
		$sql = " SELECT c_padre.id, c_padre.descripcion, c_padre.objeto_novio as objeto, c_padre.requerido_novio as requerido
        	,GROUP_CONCAT(CONCAT(IFNULL(c_hijo.id_ref,c_hijo.id),'#',c_hijo.descripcion,'#',IFNULL(c_hijo.accion_novio,'')) ORDER BY c_hijo.orden, c_hijo.descripcion SEPARATOR '@') as hijo
        	,GROUP_CONCAT(CONCAT(IFNULL(c_hijo2.id_ref,c_hijo2.id),'**',c_hijo2.descripcion,'**',c_hijo2.objeto_novio,'**',c_hijo2.requerido_novio,'**',IFNULL(c_hijo2.accion_novio,'')) ORDER BY c_hijo2.orden, c_hijo2.descripcion SEPARATOR '@') as hijo2
        	FROM form_gral_caracteristicas c_padre
        	LEFT JOIN form_gral_caracteristicas c_hijo ON  (c_hijo.padre = c_padre.id AND c_padre.padre is null)
        	LEFT JOIN form_gral_caracteristicas c_hijo2 ON c_hijo2.padre = c_hijo.id
        	WHERE c_padre.estado = 2 AND c_hijo.estado = 2 ";

        $sql_sin_zona = " AND c_padre.id != 8 "; //-- porq la ZONA se cambio como primer paso del formulario --//
        $sql_group = " GROUP BY c_padre.id ORDER BY c_padre.orden, c_padre.descripcion ";
        
        $query = $this->db->query($sql.$sql_sin_zona.$sql_group);
        $items = $query->result_array();

        $sql_con_zona = " AND c_padre.id = 8 "; // -- Consulta agregada para obtener las zonas para colocarlo en primer lugar del formulario --//
        $query = $this->db->query($sql.$sql_con_zona.$sql_group);
        $zonas = $query->row_array();

        $res = array(
			'items' => $items,
			'zonas' => $zonas
		);

		return $res;
	}

	public function get_id_zona(){
		$sql = "SELECT id_zona FROM form_gral_zonas_rel_sucursal WHERE id_sucursal = ? LIMIT 1";

		$res = array();

		$query = $this->db->query($sql, array($this->session->userdata('id_sucursal')));
		if($query->num_rows) $res = $query->row()->id_zona;
        
        return $res;
	}

	public function get_salones($salon){
		$sql = " SELECT DISTINCT(proveedor) proveedor
			FROM prov_minisitios ms
			INNER JOIN prov_rubros r ON r.id = ms.id_rubro
			WHERE r.padre IN (123,133,140,153,168,208,223,238) AND r.estado = 2 AND ms.proveedor LIKE '%" . $this->db->escape_like_str($salon) . "%'
			GROUP BY proveedor";

		$query = $this->db->query($sql);
		$res = $query->row()->proveedor;

		return $res;
	}

	public function get_invitados_form_general($resp_24){
		$sql = "SELECT descripcion FROM form_gral_caracteristicas c WHERE c.id = ? ";

     	$query = $this->db->query($sql, array((int) $resp_24));
		$res = $query->row()->descripcion;

		return $res;
	}

	public function get_no_mostrar_rubros($id_sucursal){
		$sql = "SELECT * FROM new_home_mostrar_rubros_seo WHERE id_sucursal = ? ";

     	$query = $this->db->query($sql, array('id_sucursal' => $id_sucursal));
		$res = $query->result_array();

		$arr = array();
		if(is_array($res)) foreach ($res as $key => $val) {
			$arr[$val['id_sucursal']][] = $val['id_rubro'];
		}

		return $arr;
	}

	public function get_nombres_rubros_seo($id_sucursal){
		$sql = "SELECT * FROM new_home_nombres_rubros_seo WHERE id_sucursal = ? ";

     	$query = $this->db->query($sql, array('id_sucursal' => $id_sucursal));
		$res = $query->result_array();

		$arr = array();
		if(is_array($res)) foreach ($res as $key => $val) {
			if($val['id_rubro']) $arr[$val['id_sucursal']][$val['id_rubro']][] = $val['nombre_seo'];
		}

		return $arr;
	}

	public function get_estructura_nav($id_sucursal){
		$sql = "SELECT * FROM new_nav_estructura WHERE id_sucursal = ? ";

     	$query = $this->db->query($sql, array($id_sucursal));
		$res = $query->result_array();

		$arr = array();
		if(is_array($res)) foreach ($res as $key => $val) {
			$arr[$val['id_sucursal']][$val['id_grupo']]['columnas'] = $val['columnas'];
			$arr[$val['id_sucursal']][$val['id_grupo']]['imagenes'] = $val['imagenes'];
		}

		return $arr;
	}

	public function get_buscador_estructura($id_sucursal){
		$sql = "SELECT * FROM new_buscador_estructura WHERE id_sucursal = ? ORDER BY orden";

     	$query = $this->db->query($sql, array($id_sucursal));
		$res = $query->result_array();

		$arr = array();
		if(is_array($res)) foreach ($res as $key => $val) {
			$arr[$val['id_sucursal']][$val['orden']][$val['icon']] = $val['id_rubro'];
		}

		return $arr;
	}

	public function get_doble_columna($id_sucursal){
		$sql = "SELECT id_rubro FROM new_buscador_estructura WHERE id_sucursal = ? AND doble_columna = 1 ";

		$query = $this->db->query($sql, array($id_sucursal));
		$res = $query->row_array();

		$arr = array();
		if(is_array($res)){
			$arr[] = $res['id_rubro'];
		}

		return $arr;
	}

	public function grabar_envio_amigo($data, $post){
		$res = $this->db->insert('emailer_newsletters', $data);
		if($res){
			$env = array();
			$env['id_newsletter'] = $this->db->insert_id();
			$env['destinatario'] = $post['dest_name'].' '.$post['dest_apellido'];
			$env['email'] = $post['dest_email'];
			/*
			if ($_SESSION['id_user']){
				$env['id_user'] = $_SESSION['id_user'];
			}
			*/

			$res = $this->db->insert('base_amigos_envios', $env);
		}
		return $res;
	}

	public function get_barrios($ids = NULL){
		$sql = "SELECT * FROM new_barrios WHERE 1 ";

		if($ids){
			$ids_str = '';
			$sql .= "AND id IN ( ";
			foreach ($ids as $id) {
				$ids_str .= $this->db->escape($id) . ',';
			}
			$sql .= rtrim($ids_str, ',') . ")";
		}

		$query = $this->db->query($sql);
		
		$res = $query->result_array();

		return $res;
	}

	public function get_iglesias($id_barrio = NULL){
		$sql = "SELECT * FROM new_directorios WHERE 1 ";
		if($id_barrio) $sql .= " AND id_barrio = ? ";

		$query = $this->db->query($sql, array($id_barrio));
		
		$res = $query->result_array();

		return $res;
	}

	public function get_templos($id_barrio = NULL, $groupby = NULL){
		$sql = "SELECT * FROM new_directorios_templos WHERE 1 ";
		if($id_barrio) $sql .= " AND id_barrio = ? ";

		if($groupby) $sql .= " GROUP BY id_barrio ";

		$query = $this->db->query($sql, array($id_barrio));
		
		$res = $query->result_array();

		return $res;
	}

	public function get_presupuestos_envios(){
		$sql = "SELECT m.id_proveedor, env.email email_novio, m.proveedor, env.destinatario nombre_novio, env.id_presupuesto, n.asunto
                , (SELECT email FROM sys_emails e WHERE e.id_padre = m.id AND e.id_referencia = 5 AND e.estado >= 1 LIMIT 1) email_prov
                , env.id id_envio
                FROM prov_presupuestos_envios_novios env
                LEFT JOIN prov_presupuestos pres ON pres.id = env.id_presupuesto
                LEFT JOIN prov_minisitios m ON m.id = pres.id_minisitio
                LEFT JOIN emailer_newsletters n ON n.id = env.id_newsletter
                WHERE env.error IS NOT NULL
                AND env.error_notificado = 0
                AND m.nuevo_sistema = 1
                HAVING email_prov IS NOT NULL
                ORDER BY env.id";

        $query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}