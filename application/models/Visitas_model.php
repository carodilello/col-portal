<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitas_model extends CI_Model{
	protected $id_rubro;
	protected $id_origen;
	protected $id_proveedor;
	protected $mes;
	protected $ano;

	public function __construct(){
		parent::__construct();
	}

	public function visitas($id_rubro, $id_origen, $id_proveedor = NULL){
		$this->id_rubro 	= $id_rubro;
		$this->id_origen 	= $id_origen;
		$this->id_proveedor = $id_proveedor;
		$this->mes 			= (int)date("m");
		$this->ano 			= (int)date("Y");
	}
	
	public function sumar_visita_rubro(){
		// Primero busco cuantas visitas tengo en el rubro, mes, anio y origen
		$query = $this->db->select('total')
				->from('prov_visitas_rubros')
					->where('id_rubro', $this->id_rubro)
					->where('mes', $this->mes)
					->where('ano', $this->ano)
					->where('id_origen', $this->id_origen)
				->get();
		
		$total = $query->row_array();

		if(isset($total["total"])) $total = $total["total"];
		
		// Si no encuentro registro, creo uno nuevo con total = 1
		if(!isset($total)||!$total){
			$data["id_rubro"]  = $this->id_rubro;
			$data["id_origen"] = $this->id_origen;
			$data["mes"] 	   = $this->mes;
			$data["ano"] 	   = $this->ano;
			$data["total"]     = 1;
			
			$this->db->insert('prov_visitas_rubros', $data);
		}else{ // si encuentro registro, le sumo uno al total
			unset($data);
			
			$res = $this->db->set('total', $total+1)
					 ->where('id_rubro', $this->id_rubro)
					 ->where('mes', $this->mes)
					 ->where('ano', $this->ano)
					 ->where('id_origen', $this->id_origen)
					 ->update('prov_visitas_rubros');
		}
	}
	
	public function sumar_visita_proveedor(){
		// Primero busco cuantas visitas tiene el proveedor, mes, anio y origen
		$sql = "SELECT total 
				FROM prov_visitas_proveedores
				WHERE id_proveedor = ? AND mes = ? AND ano = ? AND id_origen = ? ";
		
		$query = $this->db->query($sql,array($this->id_proveedor, $this->mes, $this->ano, $this->id_origen));

		$total = $query->row_array();

		if(isset($total["total"])) $total = $total["total"];

		
		if(!$total){
			$data["id_proveedor"] = $this->id_proveedor;
			$data["id_origen"] = $this->id_origen;
			$data["mes"] = $this->mes;
			$data["ano"] = $this->ano;
			$data["total"] = 1;
			
			$this->db->insert('prov_visitas_proveedores', $data);
		}else{ // si encuentro registro, le sumo uno al total
			unset($data);

			$res = $this->db->set('total', $total+1)
					->where('id_proveedor', $this->id_proveedor)
					->where('mes', $this->mes)
					->where('ano', $this->ano)
					->where('id_origen', $this->id_origen)
					->update('prov_visitas_proveedores');
		}
	}

}
?>