<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navegacion_model extends CI_Model{
	public $id;
	
	public function __construct(){
		parent::__construct();
	}

	public function navegacion($id=NULL){
		$this->id = $id;
		$this->db = MDB2::singleton();
	}
	
	public function getURL(){
		$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
		$protocol = 'http'.$s;
		$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
		return $protocol."://".$_SERVER['HTTP_HOST'].$port.$_SERVER['REQUEST_URI'];
	}
	
	public function get_secciones($id_padre, $where = '1', $with_name = FALSE){
		$this->load->library('parseo_library');

		$where = ' WHERE ' . $where;
		$sql = 'SELECT syss.id, syss.seccion FROM sys_secciones syss ' . $where . ' AND syss.padre = ? ';
		
		$query = $this->db->query($sql, array($id_padre));

		$arr = $query->result_array();

		$res = array();

		if($with_name){
			foreach ($arr as $val){
				$res[$val['id']]['text'] = $val['seccion'];
				$res[$val['id']]['name'] = $this->parseo_library->clean_url($val['seccion']);
			}
		}else{
			foreach ($arr as $val){
				$res[$val['id']] = $val['seccion'];
			}	
		}

		return $res;
	}

	public function getSeccionesHijos($id_padre){
		$sql = 'SELECT id, seccion FROM sys_secciones WHERE padre = ? ';
		return $this->db->queryAll($sql, array($id_padre));
	}
	public function getSeccionesDescendentes($id_seccion){
		if (is_array($id_seccion)){
			$total = array();
			foreach ($id_seccion as $secc){
				$sql = 'SELECT id FROM sys_secciones WHERE padre = ' . $this->db->escape($secc);
				$query = $this->db->query($sql);
				$hijos = $query->result_array();

				array_push($total, $secc);
				if ($hijos){
					foreach ($hijos as $hijo){
						$total = array_merge($total,$this->getSeccionesDescendentes($hijo));
					}
				}
			}
		}else{
			$sql = 'SELECT id FROM sys_secciones WHERE padre = ? ';
			$query = $this->db->query($sql, array($id_seccion));
			$hijos = $query->result_array();
			$total = array($id_seccion);
			if ($hijos){
				foreach ($hijos as $hijo){
					$total = array_merge($total,$this->getSeccionesDescendentes($hijo));
				}
			}
		}
		return $total;
	}
	public function get_seccion($id_seccion){ 
		$sql  = ' SELECT syss.*, syssp.seccion AS padre_seccion, syssp.url as padre_seccion_url, syssp2.seccion as padre_gral, syssp2.url as padre_gral_url ';
		$sql .= ' FROM sys_secciones syss ';
		$sql .= ' LEFT JOIN sys_secciones syssp ON syss.padre = syssp.id ';
		$sql .= ' LEFT JOIN sys_secciones syssp2 ON syssp.padre = syssp2.id ';
		if ($id_seccion){
			$sql .= ' WHERE syss.id = '. $this->db->escape($id_seccion);
		}else{
			return NULL;
		}

		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function get_ubicaciones_col_tv($id_portal = 1, $id_tipo_evento = 1, $id_sucursal = 1, $filtros = NULL){
		$where = ' WHERE sysrsp.id_portal = ' . $this->db->escape($id_portal) .' AND sysrsp.id_tipo_evento = ' . $this->db->escape($id_tipo_evento) . ' AND sysrsp.id_sucursal = ' . $this->db->escape($id_sucursal) . ' AND syss.padre IS NULL AND syss.activo = 1 AND syss.id = 99';

		$sql = 'SELECT CONCAT_WS(\'@\',syss2.id,IFNULL(syss2.seccion,\'\'),IFNULL(syss2.url,\'\'),(SELECT GROUP_CONCAT(CONCAT_WS(\'#\',syss3.id,IFNULL(syss3.seccion,\'\'),IFNULL(syss3.url,\'\'),IFNULL(syss3.imagen,\'\')) ORDER BY syss3.orden SEPARATOR \'$\') FROM sys_secciones syss3 WHERE syss2.id = syss3.padre AND syss3.activo = 1 ORDER BY syss3.orden)) AS hijos
				FROM sys_secciones syss LEFT JOIN sys_rel_secciones_portales sysrsp ON syss.id = sysrsp.id_seccion LEFT JOIN sys_secciones syss2 ON syss2.padre = syss.id AND syss2.activo = 1
				'.$where.' ORDER BY syss2.orden';

		$query = $this->db->query($sql);

		$res = $query->result_array();

		if(!empty($res)) foreach ($res as $k => $el) {
			$tmp_ubicacion = explode('@', $el['hijos']);
			if(isset($tmp_ubicacion[3]) && $tmp_ubicacion[3]){
				$tmp_listado = explode('$', $tmp_ubicacion[3]);
				$tmp_ubicacion[3] = '';
				if(!empty($tmp_listado)) foreach ($tmp_listado as $key => $listado){
					$data_rubro = explode('#', $listado);
					if($data_rubro[1]) $data_rubro[4] = $this->parseo_library->clean_url($data_rubro[1]);
					$tmp_ubicacion[3] .= $data_rubro[0] . '#' . $data_rubro[1] . '#' . $data_rubro[2] . '#' . $data_rubro[3] . '#' . $data_rubro[4] . '$';
				}
				$tmp_ubicacion[3] = rtrim($tmp_ubicacion[3], '$');
			}
			$res[$k]['hijos'] = $tmp_ubicacion[0] . '@' . $tmp_ubicacion[1] . '@' . $tmp_ubicacion[2] . '@' . $tmp_ubicacion[3] . '@' . $this->parseo_library->clean_url($tmp_ubicacion[1]);
		}

		return $res; 
	}

	public function getSucursales($id_portal = 1, $id_evento_tipo = 1){ 
		$sql = "SELECT sc.id, sc.sucursal, sc.nombre_seo
				FROM sys_sucursales sc
				LEFT JOIN sys_rel_sucursales_portales srsp ON srsp.id_sucursal = sc.id
				WHERE sc.estado = 2
				AND srsp.id_portal = " . $this->db->escape($id_portal) . "
				AND srsp.id_evento_tipo = " . $this->db->escape($id_evento_tipo) . "
				ORDER BY sc.sucursal";

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getSucursal($id_sucursal = 1){
		$this->load->library('parseo_library');

		$sql = 'SELECT * FROM sys_sucursales WHERE id = ?';
		
		$query =  $this->db->query($sql, array($id_sucursal));
		$res = $query->row_array();
		$res['class'] = $this->parseo_library->clean_url($res['sucursal']);

		return $res;
	}

	public function get_paises(){
		$sql = "SELECT id,pais 
		FROM sys_paises
		WHERE id = 10 "; // argentina y uruguay
	
		$query = $this->db->query($sql);

		if($arr = $query->result_array()){
			$return = array();
			foreach ($arr as $k => $val) {
				$return[$val['id']] = $val['pais'];
			}
		}

		return $return;
	}
	
	public function get_provincias($dm = 0){ 
		if ($dm){
			$select = 'id_dm id, provincia';
			$and = ' AND id_dm <> 0 ';
		}else{
			$select = 'id, provincia';
			$and = '';
		}

		$sql = 'SELECT '.$select.'
						FROM sys_provincias
						WHERE estado = 2
						'.$and.'
						ORDER BY orden';
		
		$query = $this->db->query($sql);

		if($arr = $query->result_array()){
			$return = array();
			foreach ($arr as $k => $val) {
				$return[$val['id']] = $val['provincia'];
			}
		}

		return $return;
	}
	
	public function get_localidades($id_provincia){
		$sql = 'SELECT id, localidad
						FROM sys_localidades
						WHERE estado = 2
						AND id_provincia = ' . $this->db->escape($id_provincia) . '
						ORDER BY localidad';
		
		$query = $this->db->query($sql);

		if($arr = $query->result_array()){
			$return = array();
			foreach ($arr as $k => $val) {
				$return[$val['id']] = $val['localidad'];
			}
		}

		return $return;
	}

	public function get_rubros_gracias($id_rubro){
		$this->load->library('parseo_library');

		$sql = 'SELECT id_sucursal FROM prov_rubros WHERE id = ' . $this->db->escape($id_rubro);

		$query = $this->db->query($sql);

		$row = $query->row_array();

		switch ($row['id_sucursal']){
			case 1:
				$id_padre = 108;
				break;
			case 2:
				$id_padre = 124;
				break;
			case 3:
				$id_padre = 134;
				break;
			case 4:
				$id_padre = 148;
				break;
			case 5:
				$id_padre = 163;
				break;
			case 6:
				$id_padre = 203;
				break;
			case 7:
				$id_padre = 218;
				break;
			case 8:
				$id_padre = 233;
				break;
			case 10:
				$id_padre = 379;
				break;
			case 11:
				$id_padre = 390;
				break;
			case 12:
				$id_padre = 401;
				break;
		}

		$sql = '(SELECT DISTINCT p.id_rubro, p.rubro, p.id_sucursal, p.sucursal, 1 orden, r2.imagen
						FROM prov_rubros r
						JOIN prov_rubros r2 ON r.padre = r2.padre
						JOIN site_proveedores_activos p ON r2.id = p.id_rubro
						WHERE r.id = ' . $this->db->escape($id_rubro) . '
						AND r2.id <> ' . $this->db->escape($id_rubro) . ')
						UNION
						(SELECT DISTINCT p.id_rubro, p.rubro, p.id_sucursal, p.sucursal, 2 orden, r.imagen
						FROM prov_rubros r
						JOIN site_proveedores_activos p ON r.id = p.id_rubro
						WHERE r.padre = ' . $this->db->escape($id_padre) . ')
						ORDER BY orden, RAND()
						LIMIT 5';

		$query = $this->db->query($sql);

		$ret = $query->result_array();

		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['seo_rubro'] = $this->parseo_library->clean_url($el['rubro']);
		}
		$ret = $arr;

		return $ret;
	}

	public function getRubrosComboWOGroup($id_sucursal = 1){ // AUN SIN TERMINAR
		$sql = 'SELECT r1.id id_rubro, r1.rubro
						FROM prov_rubros r1
						LEFT JOIN prov_rubros r2 ON r1.padre = r2.id
						JOIN site_proveedores_activos p ON r1.id = p.id_rubro
						WHERE r1.id_sucursal = ' . $this->db->escape($id_sucursal) . '
						AND r1.padre IS NOT NULL
						GROUP BY r1.id
						ORDER BY r1.rubro';

		$query = $this->db->query($sql);
		$res = $query->result_array();

		return $res;
	}
	public function get_rubros_combo($id_sucursal = 1, $id_portal = 1, $activo = TRUE){
		$sql = "SELECT provr.rubro
		, GROUP_CONCAT(provr2.id,'+++',provr2.rubro,'+++',provr2.estado ORDER BY provr2.rubro SEPARATOR '---') AS hijos
		FROM prov_rubros provr
		LEFT JOIN prov_rubros provr2 ON provr2.padre = provr.id AND provr2.estado = 2 ";
		
		if($activo) $sql .= " AND (SELECT COUNT(*) FROM site_proveedores_activos WHERE id_rubro = provr2.id)>0 ";
		
		$sql .= " LEFT JOIN sys_rel_sucursales_portales srsp ON srsp.id_sucursal = provr.id_sucursal
		WHERE provr.padre IS NULL
		AND provr.id_sucursal = " . $this->db->escape($id_sucursal) . "
		AND srsp.id_portal = " . $this->db->escape($id_portal) . "
		GROUP BY provr.id
		ORDER BY provr.orden ASC,rubro ASC";

		$query = $this->db->query($sql);

		$arr = array();

		if($resultados = $query->result_array()){
			foreach ($resultados as $val) {
				$arr[$val["rubro"]] = $val["hijos"];
			}
		}

		$params = array();

		foreach($arr as $padre => $hijos){
			if ($hijos){
				$hijos = explode('---',$hijos);
				$params[$padre] = array();
				foreach($hijos as $hijo){
					$hijo_exp = explode('+++',$hijo);
					$params[$padre][$hijo_exp[0]] = $hijo_exp[1];
				}
			}
		}
		return $params;
	}


	public function page($sql, $page_id, $results, $uri = NULL){
		global $smarty;
		$currentPage = mysql_escape_string($page_id);
		$startOffset = (int)($currentPage-1)*$results;
		$sql_limit = sprintf("%s LIMIT %d, %d", $sql, $startOffset, $results);

		if ($_SERVER['REQUEST_URI'] == '/fotos-de-casamientos') $uri = $uri='/fotos-de-casamientos/listado/id_tag=&results=12&pageID=' . $_GET['pageID'] . '&filtro_toggle=&id_seccion=&id_seccion_hijo=&sucursal=&rubro=&empresa='; // CORREGIR! PASAR PARAMETRO URI DESDE LA PAGINA Y SACAR ESTA LINEA DE LA FUNCION

		if (isset($uri)) $_SERVER['REQUEST_URI'] = $uri;

		$pagerOptions = array('mode'     => 'Sliding',
			'delta'    => 2,
			'perPage'  => $results,
			'httpMethod' => 'GET',
			'itemData' => $this->db->queryAll($sql),
			'clearIfVoid' => TRUE,
			'prevImg' => '<span class="pag_prev"></span>',
			'nextImg' => '<span class="pag_next"></span>',
			'curPageSpanPre' => '<strong>',
			'curPageSpanPost' => '</strong>',
			'firstPagePre' => '',
			'firstPageText' => ' ',
			'firstPagePost' => '',
			'lastPagePre' => '',
			'lastPageText' => ' ',
			'lastPagePost' => '',
			'separator' => '<span class="pipe">|</span>',
			'spacesBeforeSeparator' => 0,
			'spacesAfterSeparator' => 0,
			'append'   => FALSE,
			'fileName' => str_replace('&pageID=' . $_GET['pageID'], '', $_SERVER['REQUEST_URI']) . '&pageID=%d',
		);

		$smarty->assign('pager', Pager::factory($pagerOptions));

		$res = $this->db->queryAll($sql_limit);
		require_once('class.comercial.php');
		$comercial = new Comercial();
		foreach($res as $k => $item){
			$res[$k]['url'] = $comercial->clean_url($comercial->clean_url_2($item['url']));
		}

		return $res;
	}

	public function get_elementos($id_sucursal){
		$this->load->library('parseo_library');

        $sql = 'SELECT id, nombre, href, grupo_imagen FROM new_nav WHERE id_sucursal = ' . $this->db->escape($id_sucursal) . ' AND nivel = 1 AND id_estado = 1 ORDER BY orden';
        $sql_hijo = 'SELECT id_ref, nombre, href, imagen, id_rubro, url_sucursal FROM new_nav WHERE id_sucursal = ' . $this->db->escape($id_sucursal) . ' AND nivel = 2 AND id_estado = 1 AND (se_muestra = 1 OR imagen IS NOT NULL) ';

        $sql_hijo .= 'ORDER BY orden';

        $ret = array();

        $query = $this->db->query($sql);
        $query_hijos = $this->db->query($sql_hijo);
        foreach($query->result_array() as $padre){
            $ret[$padre['id']]['padre'] = $padre;
            foreach($query_hijos->result_array() as $hijo){
            	$hijo['nombre_url'] = $this->parseo_library->clean_url($hijo['nombre']);
                if($hijo['id_ref'] == $padre['id']){
                	if(!$hijo['imagen']){
                		$ret[$hijo['id_ref']]['hijos'][] = $hijo;
                	}else{
                		$ret[$hijo['id_ref']]['imagenes'][] = $hijo;
                	}
                }
            }
        }

        return $ret;
    }

    public function get_id_evento($tipo = 1, $id_sucursal = NULL){
    	$sql = "SELECT id FROM evn_eventos WHERE estado = 1 AND id_tipo = ? ";

    	if($id_sucursal) $sql .= " AND id_sucursal = " . $this->db->escape($id_sucursal);

 		$query = $this->db->query($sql, array($tipo));

 		$res = NULL;
 		if($arr = $query->row_array()){
 			$res = $arr['id'];
 		}

		return $res;
    }

    public function get_rubros(){
		$sql = 'SELECT 
					r.id
					, r.rubro
				FROM prov_rubros r
				WHERE r.id_sucursal = 1
				AND r.padre IS NOT NULL
				AND r.estado = 2
				ORDER BY r.rubro';

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function listado_infonovias($anio){
		$sql = 'SELECT 
					n.id
					, n.asunto titulo
					, n.header_contenido contenido
					, n.fecha_visual
				FROM news_letters n
				WHERE n.estado = 6
				AND n.id_tipo <> 11
				AND YEAR(n.fecha_visual) = ' . $this->db->escape((($anio)? $anio : date('Y'))) . '
				ORDER BY n.fecha_visual DESC';

		$query = $this->db->query($sql);

		return $query->result_array();
	}
}