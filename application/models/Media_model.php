<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media_model extends CI_Model{
	public $referencia = 15;

	public function get_galeria($data, $row = FALSE){
		$this->load->model('navegacion_model');
		$this->load->library('parseo_library');

		$where = ' WHERE 1';
		$orden = '';
		$group = '';
		if (isset($data['id_minisitio']) && $data['id_minisitio']){
			$this->referencia = 5;
			$select = ', prin.rubro, prin.id_rubro, prin.proveedor, prin.sucursal, prin.id_sucursal

			, GROUP_CONCAT(DISTINCT prin.id_proveedor,\'@\',prin.proveedor,\'@\',prin.id_minisitio,\'@\',prin.rubro,\'@\',prin.sucursal,\'@\',prin.id_rubro,\'@\',prin.id_sucursal,\'@\',prin.logo,\'@\',pm.short_url SEPARATOR \'~\') AS proveedores

			, GROUP_CONCAT(DISTINCT syst.id,\'@\',syst.descripcion ORDER BY sysatg.orden ASC SEPARATOR \'~\') AS tags, prin.breve AS descripcion_galeria';
			$table = 'site_proveedores_activos';
			$where .= ' AND prin.id_minisitio = ' . $this->db->escape($data['id_minisitio']);
			$left_join = ' LEFT JOIN sys_rel_medias_referencias sysrmr ON prin.id_minisitio = sysrmr.id_padre AND sysrmr.id_referencia = '.$this->referencia.' AND sysrmr.estado = 2
		 JOIN sys_medias sysm ON sysrmr.id_media = sysm.id AND sysm.eliminado = 0 AND sysm.estado IN (2,4)
		 JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id AND sysat.id_tipo = 1
		 JOIN sys_rel_medias_referencias sysrmr_prin ON prin.id_minisitio = sysrmr_prin.id_padre AND sysrmr_prin.id_referencia = '.$this->referencia.' AND sysrmr_prin.estado = 2 #AND sysrmr_prin.orden = 1
		 JOIN sys_medias sysm_prin ON sysrmr_prin.id_media = sysm_prin.id AND sysm_prin.eliminado = 0 AND sysm_prin.estado IN (2,4)
		 JOIN sys_archivos_tipo sysat_prin ON sysm_prin.id_tipo = sysat_prin.id AND sysat_prin.id_tipo = 1
		LEFT JOIN sys_asociados_tags sysatg ON sysm.id = sysatg.id_padre AND sysatg.activo = 1
		LEFT JOIN sys_tags syst ON sysatg.id_tag = syst.id AND syst.id_referencia = 15 AND syst.activo = 1
		LEFT JOIN prov_minisitios pm ON pm.id = prin.id_minisitio';


		// emi dice: deje comentado el "AND sysrmr_prin.orden = 1", asi puede traer todo sin necesidad de que esten ordenadas y la primera con valor 1. Con tiempo hay que solucionar este tema.

		}elseif($data['id_galeria']){
			$where .= ' AND prin.id = '.$data['id_galeria'];
			$data['show_gal'] = 1;
		}
		if (isset($data['show_gal']) && $data['show_gal']){
			$table = 'sys_galerias';
			$this->referencia = 14;
			$select = ', prin.id, prin.titulo AS titulo_galeria, prin.descripcion AS descripcion_galeria, GROUP_CONCAT(DISTINCT provr.id,\'@\',provr.rubro,\'@\',syssuc.sucursal,\'@\',syssuc.id ORDER BY sysar.orden SEPARATOR \'~\') AS rubros, GROUP_CONCAT(DISTINCT provp.id,\'@\',provp.proveedor,\'@\',spa.id_minisitio,\'@\',spa.rubro,\'@\',spa.sucursal,\'@\',spa.id_rubro,\'@\',spa.id_sucursal,\'@\',spa.logo,\'@\',pmini.short_url ORDER BY sysap.orden SEPARATOR \'~\') AS proveedores, GROUP_CONCAT(DISTINCT CONCAT_WS(\'#\',sysc.id,sysc.comentario, IFNULL(sysc.nombre,\'\'), IFNULL(sysc.apellido,\'\'), sysc.email, sysc.fecha) SEPARATOR \'~\') AS comentarios, GROUP_CONCAT(DISTINCT syst.id,\'@\',syst.descripcion ORDER BY sysatg.orden ASC SEPARATOR \'~\') AS tags, COUNT(DISTINCT sysc.id) AS cantidad_comentarios, (SELECT CONCAT_WS(\'~\', CONCAT_WS(\'@\', syss1.id, syss1.seccion, syss1.url, syss1.padre), CONCAT_WS(\'@\', syss2.id, syss2.seccion, syss2.url, syss2.padre), CONCAT_WS(\'@\', syss3.id, syss3.seccion, syss3.url, syss3.padre)) FROM sys_areas_asociadas sysaa LEFT JOIN sys_secciones syss3 ON sysaa.id_seccion = syss3.id LEFT JOIN sys_secciones syss2 ON syss3.padre = syss2.id LEFT JOIN sys_secciones syss1 ON syss2.padre = syss1.id WHERE sysaa.id_referencia = '.$this->referencia.' AND sysaa.id_padre = prin.id ORDER BY sysaa.orden ASC, sysaa.id ASC LIMIT 1) AS seccion';
			$left_join = ' LEFT JOIN sys_rel_medias_referencias sysrmr ON prin.id = sysrmr.id_padre AND sysrmr.id_referencia = '.$this->referencia.' AND sysrmr.estado = 2
		LEFT JOIN sys_medias sysm ON sysrmr.id_media = sysm.id AND sysm.eliminado = 0 AND sysm.estado IN (2,4) AND sysm.col = 1
		LEFT JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id
		LEFT JOIN sys_rel_medias_referencias sysrmr_prin ON prin.id = sysrmr_prin.id_padre AND sysrmr_prin.id_referencia = '.$this->referencia.' AND sysrmr_prin.estado = 2 AND sysrmr_prin.orden = 1
		LEFT JOIN sys_medias sysm_prin ON sysrmr_prin.id_media = sysm_prin.id AND sysm_prin.eliminado = 0 AND sysm_prin.estado IN (2,4)
		LEFT JOIN sys_archivos_tipo sysat_prin ON sysm_prin.id_tipo = sysat_prin.id
		LEFT JOIN sys_asociados_proveedores sysap ON prin.id = sysap.id_padre AND sysap.id_referencia = '.$this->referencia.' AND sysap.activo = 1
		LEFT JOIN prov_proveedores provp ON sysap.id_proveedor = provp.id
		LEFT JOIN prov_minisitios pmini ON pmini.id_proveedor = provp.id
		LEFT JOIN site_proveedores_activos spa ON provp.id = spa.id_proveedor
		LEFT JOIN sys_asociados_rubros sysar ON prin.id = sysar.id_padre AND sysar.id_referencia = '.$this->referencia.' AND sysar.activo = 1
		LEFT JOIN prov_rubros provr ON sysar.id_rubro = provr.id
		LEFT JOIN sys_sucursales syssuc ON provr.id_sucursal = syssuc.id
		LEFT JOIN sys_comentarios sysc ON prin.id = sysc.id_padre AND sysc.id_referencia = '.$this->referencia.' AND sysc.id_estado = 2 AND sysc.activo = 1
		LEFT JOIN sys_asociados_tags sysatg ON prin.id = sysatg.id_padre AND sysatg.activo = 1
		LEFT JOIN sys_tags syst ON sysatg.id_tag = syst.id AND syst.id_referencia = '.$this->referencia.' AND syst.activo = 1';
		}
		if (isset($data['id_seccion']) && $data['id_seccion']){
			$left_join .= ' LEFT JOIN sys_areas_asociadas sysaa ON prin.id = sysaa.id_padre AND sysaa.id_referencia = '.$this->referencia.' AND sysaa.activo = 1
				LEFT JOIN sys_secciones syss ON sysaa.id_seccion = syss.id';
			$where .= ' AND syss.id IN ('.implode(',',$this->navegacion_model->getSeccionesDescendentes($data['id_seccion'])).')';
		}
		if (isset($data['ubicacion']) && $data['ubicacion']){
			$left_join .= '
				LEFT JOIN sys_rel_destaques_galerias sysrdg ON prin.id = sysrdg.id_galeria
				LEFT JOIN sys_destaques_galerias sysdg ON sysrdg.id_destaque = sysdg.id AND sysdg.activo = 1';
			$where .= ' AND sysdg.id = ' . $this->db->escape($data['ubicacion']);
		}
		if (isset($data['orden']) && $data['orden']){
			$orden = ' ORDER BY '.$data['orden'];
		}
		if (isset($data['group']) && $data['group']){
			$group = ' GROUP BY '.$data['group'];
		}
		$sql = 'SELECT sysm_prin.titulo, sysm_prin.descripcion, sysm_prin.name AS pic_name, sysat_prin.extension AS pic_extension, GROUP_CONCAT(DISTINCT CONCAT_WS(\'@\', sysm.name, sysat.extension, sysm.titulo, sysm.descripcion) ORDER BY sysrmr.orden ASC, sysrmr.id DESC SEPARATOR \'~\') AS thumbs '.$select
		.' FROM '.$table.' prin'.$left_join.$where.$group.$orden;

		$query = $this->db->query($sql);

		if ($row){
			return $query->row_array();
		}

		$res = $query->result_array();

		//***** Para los casos en que la empresa no esta activa y se pueda ingresar a la galeria de imagenes, es para solucionar error 404 de URL indexadas *****//
		if(empty($res[0]['titulo'])  && !empty($data['id_minisitio'])  && isset($data['do']) && $data['do'] == 'see'){
			$sql = " SELECT sysm_prin.titulo, sysm_prin.descripcion, sysm_prin.name AS pic_name, sysat_prin.extension AS pic_extension, GROUP_CONCAT(DISTINCT CONCAT_WS('@', sysm.name, sysat.extension, sysm.titulo, sysm.descripcion) ORDER BY sysrmr.orden ASC, sysrmr.id DESC SEPARATOR '~') AS thumbs, r.rubro, prin.id_rubro, prin.proveedor, s.sucursal
, r.id_sucursal , GROUP_CONCAT(DISTINCT prin.id_proveedor,'@',prin.proveedor,'@',prin.id,'@',r.rubro,'@',s.sucursal,'@',prin.id_rubro,'@',r.id_sucursal,'@',prov.logo SEPARATOR '~') AS proveedores , GROUP_CONCAT(DISTINCT syst.id,'@',syst.descripcion ORDER BY sysatg.orden ASC SEPARATOR '~') AS tags, prin.breve AS descripcion_galeria  ";
			$sql.= " FROM prov_minisitios prin ";
			$sql.= " INNER JOIN prov_proveedores prov On prov.id = prin.id_proveedor ";
			$sql.= " INNER JOIN prov_rubros r ON r.id = prin.id_rubro ";
			$sql.= " INNER JOIN sys_sucursales s On s.id =  r.id_sucursal ";
			$sql.= " LEFT JOIN sys_rel_medias_referencias sysrmr ON prin.id = sysrmr.id_padre AND sysrmr.id_referencia = 5 AND sysrmr.estado = 2 ";
			$sql.= " JOIN sys_medias sysm ON sysrmr.id_media = sysm.id AND sysm.eliminado = 0 AND sysm.estado IN (2,4) ";
			$sql.= " JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id AND sysat.id_tipo = 1 ";
			$sql.= " JOIN sys_rel_medias_referencias sysrmr_prin ON prin.id = sysrmr_prin.id_padre AND sysrmr_prin.id_referencia = 5 AND sysrmr_prin.estado = 2 AND sysrmr_prin.orden = 1 ";
			$sql.= " JOIN sys_medias sysm_prin ON sysrmr_prin.id_media = sysm_prin.id AND sysm_prin.eliminado = 0 AND sysm_prin.estado IN (2,4) ";
			$sql.= " JOIN sys_archivos_tipo sysat_prin ON sysm_prin.id_tipo = sysat_prin.id AND sysat_prin.id_tipo = 1 ";
			$sql.= " LEFT JOIN sys_asociados_tags sysatg ON sysm.id = sysatg.id_padre AND sysatg.activo = 1 ";
			$sql.= " LEFT JOIN sys_tags syst ON sysatg.id_tag = syst.id AND syst.id_referencia = 15 AND syst.activo = 1 ";
			$sql.= " WHERE 1 AND prin.id = " . $this->db->escape((int)$data['id_minisitio']);
			$query = $this->db->query($sql);
			$res = $query->result_array();
		}
		//***** *****//

		if(!empty($res)){
			foreach ($res as $k => $el) {
				$rubros = array();
				if(!empty($el['rubros'])){
					$tmp_rubros = explode('~', $el['rubros']);
					if(is_array($tmp_rubros)) foreach ($tmp_rubros as $proveedor_str){
						if($proveedor_str){
							$tmp_rubro = explode('@', $proveedor_str);
							if(isset($tmp_rubro[1])){
								$tmp_rubro[4] = $this->parseo_library->clean_url($tmp_rubro[1]);
								$tmp_rubro[5] = $this->parseo_library->clean_url($tmp_rubro[2]);
								$rubros[] = $tmp_rubro;
							}
						}
					}	
				}
				$proveedores = array();
				if(!empty($el['proveedores'])){
					$tmp_proveedores = explode('~', $el['proveedores']);
					if(is_array($tmp_proveedores)) foreach ($tmp_proveedores as $proveedor_str){
						if($proveedor_str){
							$tmp_rubro = explode('@', $proveedor_str);
							if(isset($tmp_rubro[3])){
								$tmp_rubro[9] = $this->parseo_library->clean_url($tmp_rubro[3]);
								$proveedores[] = $tmp_rubro;
							}
						}
					}	
				}
				$tags = array();
				if(!empty($el['tags'])){
					$tmp_tags = explode('~', $el['tags']);
					if(is_array($tmp_tags)) foreach ($tmp_tags as $proveedor_str){
						if($proveedor_str){
							$tmp_rubro = explode('@', $proveedor_str);
							if(isset($tmp_rubro[1])){
								$tmp_rubro[2] = $this->parseo_library->clean_url($tmp_rubro[1]);
								$tags[] = $tmp_rubro;
							}
						}
					}	
				}

				$aux = '';
				if($tags) foreach ($tags as $el) {
					$aux2 = '';
					foreach ($el as $l => $v) {
						$aux2 .= $v . '@';
					}
					$aux .= rtrim($aux2, '@');
					$aux .= '~';
				}

				$res[$k]['tags'] = rtrim($aux, '@');

				$aux = '';
				if($rubros) foreach ($rubros as $el) {
					$aux2 = '';
					foreach ($el as $l => $v) {
						$aux2 .= $v . '@';
					}
					$aux .= rtrim($aux2, '@');
					$aux .= '~';
				}

				$res[$k]['rubros'] = rtrim($aux, '@');

				$aux = '';
				if($proveedores) foreach ($proveedores as $el) {
					$aux2 = '';
					foreach ($el as $l => $v) {
						$aux2 .= $v . '@';
					}
					$aux .= rtrim($aux2, '@');
					$aux .= '~';
				}

				$res[$k]['proveedores'] = rtrim($aux, '@');
			}
		}

		return $res;
	}

	public function get_galerias($id_seccion = NULL, $filtros = NULL, $page_id = NULL, $results = NULL){
		$this->load->model('navegacion_model');
		$this->load->library('parseo_library');

		$this->referencia = 14;
		$where = ' WHERE sysg.id_estado = 4 AND sysrmr_prin.orden = 1 AND sysatall.id_tipo = 1 AND sysat.id_tipo = 1';
		$where_prov = ' WHERE 1';
		$left_join = $join_prov = $select_cont = $select_prov = '';
		$order = ' ORDER BY fecha_alta DESC';
		if ($id_seccion){
			$where .= ' AND syss.id IN ('.implode(',',$this->navegacion_model->getSeccionesDescendentes($id_seccion)).')';
		}
		if (isset($filtros['empresa']) && $filtros['empresa']){
			$left_join .= 'LEFT JOIN sys_asociados_proveedores sysap ON sysg.id = sysap.id_padre AND sysap.id_referencia = '.$this->referencia.' AND sysap.activo = 1';
			$where .= ' AND sysap.id_proveedor = ' . $this->db->escape($filtros['empresa']);
			$where_prov .= ' AND provp.id_proveedor = ' . $this->db->escape($filtros['empresa']);
		}
		else if(isset($filtros['id_minisitio']) && $filtros['id_minisitio']){
			$left_join .= 'LEFT JOIN sys_asociados_proveedores sysap ON sysg.id = sysap.id_padre AND sysap.id_referencia = '.$this->referencia.' AND sysap.activo = 1';
			$where_prov .= ' AND provp.id_minisitio = ' . $this->db->escape($filtros['id_minisitio']);
		}
		else if (isset($filtros['rubro']) && $filtros['rubro']){
			$left_join .= 'LEFT JOIN sys_asociados_rubros sysar ON sysg.id = sysar.id_padre AND sysar.id_referencia = '.$this->referencia.' AND sysar.activo = 1';
			$where .= ' AND sysar.id_rubro = ' . $this->db->escape($filtros['rubro']);
			$where_prov .= ' AND provp.id_rubro = ' . $this->db->escape($filtros['rubro']);
		}
		if (isset($filtros['id_tag']) && $filtros['id_tag']){
			$left_join .= ' JOIN sys_asociados_tags sysatg ON sysg.id = sysatg.id_padre AND sysatg.activo = 1 AND sysatg.id_tag = ' . $this->db->escape($filtros['id_tag']);
			$join_prov .= ' JOIN sys_rel_medias_referencias sysrmr ON provp.id_minisitio = sysrmr.id_padre AND sysrmr.id_referencia = 5 AND sysrmr.estado = 2
			JOIN sys_medias sysm ON sysrmr.id_media = sysm.id AND sysm.eliminado = 0 AND sysm.estado IN (2,4)
			JOIN sys_asociados_tags sysatg ON sysm.id = sysatg.id_padre AND sysatg.activo = 1 AND sysatg.id_tag = ' . $this->db->escape($filtros['id_tag']);
		}
		if (isset($filtros['ubicacion']) && $filtros['ubicacion']){
			$left_join .= '
				LEFT JOIN sys_rel_destaques_galerias sysrdg ON sysg.id = sysrdg.id_galeria AND sysrdg.id_destaque = ' . $this->db->escape($filtros['ubicacion']) . '
				LEFT JOIN sys_destaques_galerias sysdg ON sysrdg.id_destaque = sysdg.id AND sysdg.activo = 1';
			$select_cont .= ', sysrdg.id_destaque IS NOT NULL AS en_ubicacion';
			$select_prov .= ', \'\' AS en_ubicacion';
			$order = ' ORDER BY en_ubicacion DESC, fecha_alta DESC';
			if (isset($filtros['solo_ubicacion']) && $filtros['solo_ubicacion']){
				$where .= ' AND sysdg.id = ' . $this->db->escape($filtros['ubicacion']);
				$filtros['filtro_toggle'] = '1'; // para que no traiga de proveedor
			}
		}
		if (isset($filtros['orden']) && $filtros['orden']){
			$order = ' ORDER BY ' . $filtros['orden'];
		}
		if (isset($filtros['term']) && $filtros['term']){
			$where .= ' AND (sysg.titulo LIKE \'%'.$filtros['term'].'%\' OR sysg.descripcion LIKE \'%'.$filtros['term'].'%\' OR sysm.titulo LIKE \'%'.$filtros['term'].'%\')';
			$where_prov .= ' AND sysmprin.titulo LIKE \'%'.$filtros['term'].'%\'';
		}
		if(!empty($filtros['ultimo_anio'])){
			$where .= ' AND YEAR(sysg.fecha_alta) = YEAR(CURRENT_DATE()) ';
			$where_prov .= ' AND YEAR(sysmprin.fecha_alta) = YEAR(CURRENT_DATE()) ';
		}

		$sql_prov = 'SELECT provp.id_minisitio AS id, provp.proveedor AS titulo, provp.breve AS descripcion, CONCAT(\'@\',provp.sucursal,\'~@\',provp.rubro) AS secciones, provp.fotos AS cantidad_fotos, sysmprin.name AS pic_name, sysat.extension AS pic_extension, CONCAT(\'/fotos-de-casamientos/empresas-\',REPLACE(LOWER(provp.proveedor),\'\/\',\'-\'),\'/\',provp.id_minisitio) AS url, \'empresas\' tipo_url, sysmprin.fecha_alta '.$select_prov.'
		FROM site_proveedores_activos provp
			JOIN sys_rel_medias_referencias sysrmfprin ON provp.id_minisitio = sysrmfprin.id_padre AND sysrmfprin.id_referencia = 5 AND sysrmfprin.estado = 2 AND sysrmfprin.orden = 1
			JOIN sys_medias sysmprin ON sysrmfprin.id_media = sysmprin.id AND sysmprin.eliminado = 0 AND sysmprin.estado IN (2,4)
			JOIN sys_archivos_tipo sysat ON sysmprin.id_tipo = sysat.id AND sysat.id_tipo = 1
		'.$join_prov.$where_prov.'
		GROUP BY provp.id_minisitio HAVING cantidad_fotos > 0';

		$sql_cont = 'SELECT sysg.id, sysg.titulo, sysg.descripcion, GROUP_CONCAT(DISTINCT syss.id,\'@\',syss.seccion,\'@\',IFNULL(syss.padre,0),\'@\',IFNULL(syssp.seccion,0),\'@\',IFNULL(syssp.padre,0),\'@\',IFNULL(syssa.seccion,0) ORDER BY sysaa.orden SEPARATOR \'~\') AS secciones, COUNT(DISTINCT sysmall.id) AS cantidad_fotos, sysm.name AS pic_name, sysat.extension AS pic_extension, CONCAT(\'/fotos-de-casamientos/casamientos-\',REPLACE(LOWER(sysg.titulo),\'\/\',\'-\'),\'/\',sysg.id) AS url,\'casamientos\' tipo_url, sysg.fecha_alta '.$select_cont.'
		FROM sys_galerias sysg
			LEFT JOIN sys_areas_asociadas sysaa ON sysg.id = sysaa.id_padre AND sysaa.id_referencia = '.$this->referencia.' AND sysaa.activo = 1
			LEFT JOIN sys_secciones syss ON sysaa.id_seccion = syss.id
			LEFT JOIN sys_secciones syssp ON syss.padre = syssp.id
			LEFT JOIN sys_secciones syssa ON syssp.padre = syssa.id
			LEFT JOIN sys_rel_medias_referencias sysrmr ON sysg.id = sysrmr.id_padre AND sysrmr.id_referencia = '.$this->referencia.' AND sysrmr.estado = 2
			LEFT JOIN sys_medias sysmall ON sysrmr.id_media = sysmall.id AND sysmall.eliminado = 0 AND sysmall.col = 1 AND sysmall.estado IN (2,4)
			LEFT JOIN sys_archivos_tipo sysatall ON sysmall.id_tipo = sysatall.id
			LEFT JOIN sys_rel_medias_referencias sysrmr_prin ON sysg.id = sysrmr_prin.id_padre AND sysrmr_prin.id_referencia = '.$this->referencia.' AND sysrmr_prin.estado = 2
			LEFT JOIN sys_medias sysm ON sysrmr_prin.id_media = sysm.id
			LEFT JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id
			'.$left_join.'
			'.$where.'
			GROUP BY id HAVING cantidad_fotos > 0';

		$sql = '';
		if (isset($filtros['filtro_toggle']) && $filtros['filtro_toggle'] == '2'){
			$sql = $sql_prov;
		}else if (isset($filtros['filtro_toggle']) && $filtros['filtro_toggle'] == '1'){
			$sql = $sql_cont;
		}else{
			$sql .= '('.$sql_cont.') UNION ('.$sql_prov.')';
		}
		$sql .= $order;
		if (isset($filtros['limit']) && $filtros['limit']){
			$sql .= ' LIMIT '.$filtros['limit'];
		}elseif($results){
			$sql .= ' LIMIT ' . $page_id . ', ' . $results;
		}

		$query = $this->db->query($sql);
		$ret = $query->result_array();
		$arr = array();
		if(!empty($ret)) foreach ($ret as $k => $el){
			$arr[$k] = $el;
			$arr[$k]['seo_titulo'] = $this->parseo_library->clean_url($el['titulo']);
		}
		$ret = $arr;

		return $ret;
	}

	public function get_col_tvs($id_seccion = NULL, $filtros = NULL, $page_id = NULL, $results = NULL){
		$this->load->model('navegacion_model');

		$where = ' WHERE 1 ';
		$where_prov = '';

		if (isset($filtros['sucursalID']) && $filtros['sucursalID']){
			$where .= ' AND sucursalID IN ('.$filtros['sucursalID'].')';
		}

		if (isset($filtros['rubro']) && $filtros['rubro']){
			if (is_array($filtros['rubro'])){
				$where .= ' AND rubroID IN (' . implode(',',$filtros['rubro']) . ')';
			}else{
				$where .= ' AND rubroID = '.$filtros['rubro'];
			}
			$filtros['show_prov'] = 1;
		}

		if (isset($filtros['id_tag']) && $filtros['id_tag']){
			$where .= ' AND tags LIKE "%'.$filtros['id_tag'].'%"';

		}

		if (isset($filtros['show_prov']) && $filtros['show_prov']){
				if (isset($filtros['term']) && $filtros['term']){
					$where_prov .= ' AND sysm_titulo LIKE \'%'.$filtros['term'].'%\'';
				}

			if($id_seccion){
				$where .= " OR (seccionesID LIKE '%".$id_seccion."%' AND seccionesID LIKE '%401%') ";
			}
		}else{
			if ($id_seccion){
				$secArray = $this->navegacion_model->getSeccionesDescendentes($id_seccion);
				$secIDs = implode(',',$secArray);
				$whereSec = '';
				foreach($secArray as $ids){
					$whereSec .= ' OR seccionesID like "%'.$ids.'%" ';
				}
				$where .= ' AND (seccionID IN ('.$secIDs.') '.$whereSec.')';
			}

			if (isset($filtros['ubicacion'])){
				$where .= ' AND id_destaque = '.$filtros['ubicacion'];
			}

			if (isset($filtros['term']) && $filtros['term']){
				$where .= ' AND (titulo LIKE \'%'.$filtros['term'].'%\' OR descripcion LIKE \'%'.$filtros['term'].'%\' OR sysm_titulo LIKE \'%'.$filtros['term'].'%\')';
			}

		}

		$order_by = ' ORDER BY video_prov ASC, orden ASC, id DESC';
		
		if (isset($filtros['orden']) && $filtros['orden']){
			$order_by = ' ORDER BY ' . $filtros['orden'];
		}

		$sql = 'SELECT * FROM vista_media_galeria ' . $where . ' ' . $where_prov . $order_by;

		if(isset($filtros['limit']) && $filtros['limit']){
			$sql .= ' LIMIT ' . $filtros['limit'];
		}elseif($results){
			$sql .= ' LIMIT ' . $page_id . ', ' . $results;
		}
		
		/*
		if($page_id && $results){
			return $this->navegacion->page($sql, $page_id, $results);
		} */

		$query = $this->db->query($sql);

		if (isset($filtros['combo']) && $filtros['combo']){
			return $query->row_array();
		}
		
		$res = $query->result_array();
		$arr = array();
		if(!empty($res)) foreach ($res as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['titulo_seo'] = $this->parseo_library->clean_url($el['titulo']);
			if($arr[$k]['proveedores']){
				$tmp = explode('@', $arr[$k]['proveedores']);
				$tmp[8] = $this->parseo_library->clean_url($tmp[3]);
				
				$sql_minisitio = 'SELECT short_url FROM prov_minisitios WHERE id = ?';
				$query = $this->db->query($sql_minisitio, array($tmp[2]));

				$tmp[9] = !empty($query->row()->short_url) ? $query->row()->short_url : '';
				
				$arr[$k]['proveedores'] = '';
				foreach ($tmp as $v) {
					$arr[$k]['proveedores'] .= $v . '@';
				}
				$arr[$k]['proveedores'] = rtrim($arr[$k]['proveedores'], '@');
			}
			if($arr[$k]['rubros']){
				$tmp = explode('@', $arr[$k]['rubros']);
				$tmp[4] = $this->parseo_library->clean_url($tmp[1]);
				$tmp[5] = $this->parseo_library->clean_url($tmp[2]);
				$arr[$k]['rubros'] = '';
				foreach ($tmp as $v) {
					$arr[$k]['rubros'] .= $v . '@';
				}
				$arr[$k]['rubros'] = rtrim($arr[$k]['rubros'], '@');
			}
			if($arr[$k]['tags']){
				$tmp2 = explode('~', $arr[$k]['tags']);
				if(!empty($tmp2)){
					$arr[$k]['tags'] = '';
					foreach ($tmp2 as $valor) {
						$tmp3 = explode('@', $valor);
						$tmp3[2] = $this->parseo_library->clean_url($tmp3[1]);
						foreach ($tmp3 as $v) {
							$arr[$k]['tags'] .= $v . '@';
						}
						$arr[$k]['tags'] = rtrim($arr[$k]['tags'], '@') . '~';
					}
					$arr[$k]['tags'] = rtrim($arr[$k]['tags'], '~');
				} 
			}
		}
		$res = $arr;

		return $res; 
	}

	public function get_nombre_seccion($id_seccion){
		if(empty($id_seccion)) return NULL;

		$sql = "SELECT seccion FROM sys_secciones WHERE id = " . $this->db->escape($id_seccion);

		$query = $this->db->query($sql);

		if($query->row()){
			$res = $query->row()->seccion;
		}else{
			$res = NULL;
		}
		return $res;
	}

	public function get_video_proveedor($id, $row){
		$sql = ' SELECT vmg.id as activo_media, sysm.id, sysm.titulo AS titulo_galeria, sysm.descripcion AS descripcion_galeria, sysm.name AS pic_name, sysat.extension AS pic_extension
		, GROUP_CONCAT(DISTINCT provp.id_rubro,\'@\',provp.rubro,\'@\',provp.sucursal,\'@\',provp.id_sucursal,\'@\',provr.padre SEPARATOR \'~\') AS rubros
		, GROUP_CONCAT(DISTINCT provp.id_proveedor,\'@\',provp.proveedor,\'@\',provp.id_minisitio,\'@\',provp.rubro,\'@\',provp.sucursal,\'@\',provp.id_rubro,\'@\',provp.id_sucursal,\'@\',provp.logo,\'@\',provp.breve,\'@\',pm.short_url SEPARATOR \'~\') AS proveedores
		, GROUP_CONCAT(DISTINCT syst.id,\'@\',syst.descripcion ORDER BY sysatg.orden ASC SEPARATOR \'~\') AS tags
		FROM sys_medias sysm
		LEFT JOIN vista_media_galeria vmg ON sysm.id = vmg.id
		LEFT JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id
		LEFT JOIN sys_rel_medias_referencias sysrmr ON sysm.id = sysrmr.id_media AND sysrmr.id_referencia = 5
		LEFT JOIN site_proveedores_activos provp ON sysrmr.id_padre = provp.id_minisitio
		LEFT JOIN prov_minisitios pm ON pm.id = provp.id_minisitio
		LEFT JOIN prov_rubros provr ON provr.id=provp.id_rubro
		LEFT JOIN sys_asociados_tags sysatg ON sysm.id = sysatg.id_padre AND sysatg.activo = 1
		LEFT JOIN sys_tags syst ON sysatg.id_tag = syst.id AND syst.id_referencia = ' . $this->referencia . ' AND syst.activo = 1
		WHERE sysm.id = ' . $this->db->escape($id);

		$query = $this->db->query($sql);

		if ($row){
			$res = $query->row_array();
			$tmp = explode('@', $res['proveedores']);
			if(!empty($tmp[3])) $tmp[10] = $this->parseo_library->clean_url($tmp[3]);
			$res['proveedores'] = '';
			foreach ($tmp as $el) {
				$res['proveedores'] .= $el . '@';
			}
			$res['proveedores'] = rtrim($res['proveedores'], '@');

			return $res;
		}
		return $query->result_array();
	}
}