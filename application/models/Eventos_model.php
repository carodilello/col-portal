<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_datos_registrado($id, $tipo){
		
		if($tipo == 'novios'){
			$tabla 	= 'evn_novios';
			$campos = ', n.fecha_evento as casamiento, n.dni';
			$inner 	= '';
		}
		if($tipo == 'empresas'){
			$tabla 	= 'evn_proveedores'; 
			$campos = ', n.celular, n.empresa, n.otro_rubro, n.id_rubro, r.rubro';
			$inner 	= ' LEFT JOIN prov_rubros r ON r.id = n.id_rubro ';
		}

		$sql = " SELECT 
					n.id
					, n.invitados
					, n.nombre
					, n.apellido
					, n.email
					, n.telefono " . $campos . "
					, d.id_localidad
					, l.id_provincia
					, GROUP_CONCAT(bcs.id_suscripcion,'@',bcs.estado) as suscrip
					, l.localidad
					, p.provincia
				FROM " . $tabla . " n ";
		
		$sql .=	$inner;
		
		$sql .= " LEFT JOIN sys_direcciones d ON d.id_padre = n.id AND d.id_referencia = 48
				  LEFT JOIN sys_localidades l ON l.id = d.id_localidad
				  LEFT JOIN sys_provincias p ON p.id = l.id_provincia
				  LEFT JOIN base_contactos bc ON bc.email = n.email
				  LEFT JOIN base_contactos_suscripciones bcs ON bcs.id_contacto = bc.id
				  WHERE n.id = " . $this->db->escape($id);

		$query = $this->db->query($sql);
		
		$res = $query->row_array();
		
		return $res;
	}
}