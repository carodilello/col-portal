<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| RUBROS-MODEL
| -------------------------------------------------------------------
| Este model es parte del desglose de la vieja clase class.comercial.php
|
| En este caso apuntamos a lo relacionado con los rubros
|
*/

class Usuarios_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}

	public function get_novio_info($id_user){
		$sql = 'SELECT IFNULL(u.email,"") email, IFNULL(u.nombre,"") nombre, IFNULL(u.apellido,"") apellido, IFNULL(DAY(u.fecha_nacimiento),"") dia_nacimiento, IFNULL(MONTH(u.fecha_nacimiento),"") mes_nacimiento, IFNULL(YEAR(u.fecha_nacimiento),"") anio_nacimiento, IFNULL(DAY(i.fecha),"") dia_evento, IFNULL(MONTH(i.fecha),"") mes_evento, IFNULL(YEAR(i.fecha),"") anio_evento, IFNULL(t.telefono,"") telefono, IFNULL(tc.telefono,"") celular, IFNULL(d.id_localidad,"") id_localidad, IFNULL(l.id_provincia,"") id_provincia, IFNULL(i.cant_invitados,"") invitados, IFNULL(s.infonovia,1) infonovias, IFNULL(s.cupones,1) promociones, IFNULL(d.calle,"") calle, IFNULL(d.numero,"") numero, IFNULL(d.piso,"") piso, IFNULL(d.departamento,"") departamento
						FROM sys_usuarios u
						LEFT JOIN sys_telefonos t ON u.id = t.id_padre AND t.id_referencia = 4 AND t.id_telefono_tipo <> 4
						LEFT JOIN sys_telefonos tc ON u.id = tc.id_padre AND tc.id_referencia = 4 AND tc.id_telefono_tipo = 4
						LEFT JOIN sys_direcciones d ON u.id = d.id_padre AND d.id_referencia = 4
						LEFT JOIN sys_localidades l ON d.id_localidad = l.id
						LEFT JOIN sys_eventos i ON u.id = i.id_padre AND i.id_referencia = 4
						LEFT JOIN nov_novios_preferencias s ON u.id = s.id_novios
						WHERE u.id = '.$id_user.'
						GROUP BY u.id';

		$query = $this->db->query($sql);

		return $query->result_array();
	}
}