<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Caracteristicas_model extends CI_Model{
	public $id_minisitio = NULL;

	public function __construct(){
		parent::__construct();
	}

	public function caracteristicas($id_minisitio){
		$this->carpeta = "/home/media/public_html/archivos/";

		$this->id_minisitio = $id_minisitio;
	}

	public function get_rubro_minisitio(){
		// obtengo el id_rubro de prov_minisitios segun el id_minisitio
		$sql = "SELECT id_rubro FROM prov_minisitios WHERE id = ? ";
		
		$query = $this->db->query($sql, array($this->id_minisitio));
		$id_rubro = $query->row()->id_rubro;

		return $id_rubro;
	}

	public function tiene_caracs(){
		$id_rubro = $this->get_rubro_minisitio();

		$sql = "SELECT COUNT(*) cant FROM cs_rubro_grupo_carac WHERE id_rubro = ? ";

		$query = $this->db->query($sql, array($id_rubro));
		$cant = $query->row()->cant;

		return $cant;
	}

	public function get_caracs_proveedor(){
		$caracs = '';

		$sql = "SELECT d.id,d.descripcion,  g.id id_grupo, g.grupo, g.en_tabla_sitio, c.caracteristica, (IF(cuto.id_tipo IN (3,6), (SELECT opcion FROM cs_opciones WHERE id = vp.valor), IF(cuto.id_tipo IN (5),'Si',vp.valor))) valor, u.id id_unidad, u.unidad, c.ui_class
			FROM cs_valores_proveedor vp
			LEFT JOIN cs_descripciones d ON d.id = vp.id_descripcion
			LEFT JOIN cs_rubro_grupo_carac rgc ON rgc.id = vp.id_relacion
			LEFT JOIN cs_grupos g ON g.id = rgc.id_grupo
			LEFT JOIN cs_caracteristicas c ON c.id = rgc.id_caracteristica
			LEFT JOIN cs_rel_carac_unidad_tipo_opcion cuto ON cuto.id_caracteristica = rgc.id_caracteristica
			LEFT JOIN cs_unidades u ON u.id = cuto.id_unidad
			WHERE d.id_minisitio = ? 
			ORDER BY id,en_tabla_sitio DESC,c.orden,g.orden,rgc.orden ";

		$query = $this->db->query($sql, array($this->id_minisitio));

		$res = $query->result_array();

		// -- estructuro todo el resultset en arrays multiples q me sirvan para armar el listado --
		foreach($res as $k=>$datos){

			unset($valores);
			$valores["valor"] = $datos["valor"];
			if($datos["unidad"]) $valores["unidad"] = $datos["unidad"];
			if($datos["ui_class"]) $valores["ui_class"] = $datos["ui_class"];

			$grupo[$datos["caracteristica"]] = $valores;

			if(isset($res[$k+1]) && $res[$k+1]["id_grupo"] != $datos["id_grupo"]){
				if($datos["en_tabla_sitio"]){
					$caracs[$datos["descripcion"]]["en_tabla"][$datos["grupo"]] = $grupo;
				}else{
					$caracs[$datos["descripcion"]]["fuera_tabla"][$datos["grupo"]] = $grupo;
				}
				unset($grupo);
			}
		}
		// --

		if(is_array($caracs)){
			return array_filter($caracs);
		}else{
			return NULL;
		}
	}
}