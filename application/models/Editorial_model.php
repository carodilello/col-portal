<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editorial_model extends CI_Model{
	public $id;
	public $referencia;
	public $cantidad_notas = 0;
	public $seccionPadre = '';
	public $seccionHijo = '';

	public function __construct($id = NULL){
		$this->id = $id;
		// $this->navegacion = $navegacion;
	}
	
	public function get_ultimas_notas($id_seccion = NULL, $cantidad = 3, $not_id_seccion = NULL, $filtros = NULL, $destaque = TRUE, $limit = '10'){
		$this->load->model('navegacion_model');
		$this->load->library('parseo_library');

		$this->referencia = 12;
		$join = '';
		$select = '';
		$where = ' WHERE edtn.id_estado IN (2,3,6) AND edtn.fecha_online <= NOW() AND edtntya.titulo IS NOT NULL';
		$order = isset($filtros['orden']) && $filtros['orden'] ? $this->db->escape($filtros['orden']) : ' edtn.id DESC';
		
		if ($id_seccion){
			$where .= ' AND sysaa.id_seccion IN ('.implode(',',$this->navegacion_model->getSeccionesDescendentes($this->db->escape($id_seccion))).')';
		}
		if (isset($filtros['id_seccion']) && $filtros['id_seccion']){
			$where .= ' AND syss.id = ' . $this->db->escape($filtros['id_seccion']);
		}
		if (isset($filtros['not_ids']) && $filtros['not_ids']){
			$where .= ' AND edtn.id NOT IN (' . $filtros['not_ids'] . ')'; // El $this->db->escape debe estar afuera
		}
		if ($not_id_seccion){
			$where .= ' AND syss.id NOT IN ('.$not_id_seccion.') ';
		}
		if (isset($filtros['id_seccion_hijo']) && in_array($filtros['id_seccion_hijo'],array(116,264,319,402))){
			$select .= ', GROUP_CONCAT(DISTINCT provr.id,\'@\',provr.rubro ORDER BY sysar.orden ASC SEPARATOR \'~\') AS secciones ';
			$join .= ' LEFT JOIN sys_asociados_rubros sysar ON edtn.id = sysar.id_padre AND sysar.id_referencia = ' . $this->referencia . ' AND sysar.activo = 1
				LEFT JOIN prov_rubros provr ON sysar.id_rubro = provr.id ';
		}else{
			$select .= ', GROUP_CONCAT(DISTINCT syss.id,\'@\',syss.seccion,\'@\',IFNULL(syss.padre,0),\'@\',IFNULL(syssp.seccion,0),\'@\',IFNULL(syssp.padre,0),\'@\',IFNULL(syssa.seccion,0),\'@\',IFNULL(syss.url,0) ORDER BY sysaa.orden SEPARATOR \'~\') AS secciones ';
			$join .= ' LEFT JOIN sys_secciones syssp ON syss.padre = syssp.id
								 LEFT JOIN sys_secciones syssa ON syssp.padre = syssa.id
			';
			$where .= ' AND (syss.id NOT IN (116,264,319,402) OR syss.id IS NULL)';
		}
		if (isset($filtros['ubicacion']) && $filtros['ubicacion']){
			$join .= ' LEFT JOIN sys_rel_destaques_referencias_portal_nuevo sysrdr ON edtn.id = sysrdr.id_padre AND sysrdr.id_referencia = ' . $this->referencia;
			if($destaque) $where .= ' AND sysrdr.id_destaque = ' . $this->db->escape($filtros['ubicacion']);
		}
		if (isset($filtros['anio']) && $filtros['anio']){
			$where .= ' AND YEAR(edtn.fecha_online) = ' . $this->db->escape($filtros['anio']) . ' ';
		}
		if (isset($filtros['mes']) && $filtros['mes']){
			$where .= ' AND MONTH(edtn.fecha_online) = ' . $this->db->escape($filtros['mes']) . ' ';
		}
		if (isset($filtros['join_ubicacion']) && $filtros['join_ubicacion']){
			$select .= ', sysrdr.id_destaque AS en_ubicacion';
			$join .= ' LEFT JOIN sys_rel_destaques_referencias sysrdr ON edtn.id = sysrdr.id_padre AND sysrdr.id_referencia = ' . $this->escape->escape($this->referencia) . ' AND sysrdr.id_destaque = ' . $this->db->escape($filtros['join_ubicacion']);
			//$order = 'en_ubicacion DESC, edtn.id DESC'; se comento esta linea, xq al ordenar las ultimas notas por ubicación traían notas viejas, siendo q existen notas mas nuevas.
			$order = ' edtn.id DESC';
		}
		if (isset($filtros['orden']) && $filtros['orden']){
			$order = $filtros['orden'];
		}
		if (isset($filtros['id_tag']) && $filtros['id_tag']){
			$join .= ' LEFT JOIN sys_asociados_tags sysat ON edtn.id = sysat.id_padre AND sysat.activo = 1
							JOIN sys_tags syst ON sysat.id_tag = syst.id AND syst.activo = 1 AND syst.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysat.id_tag = ' . $this->db->escape($filtros['id_tag']);
		}
		if (isset($filtros['term']) && $filtros['term']){
			$where .= ' AND (edtntya.titulo LIKE \'%' . $this->db->escape_like_str($filtros['term']).'%\')';
		}
		if(isset($filtros['id_seccion_padre']) && $filtros['id_seccion_padre']){
			$where .= ' AND syss.padre = ' . $this->db->escape($filtros['id_seccion_padre']);
		}

		$sql_1 = 'SELECT edtn.id
					, edtntya.titulo
					, edtntya.copete
					, edtntya.contenido
					, COUNT(DISTINCT sysc.id) AS cantidad_comentarios
					, (SELECT CONCAT_WS(\'@\',sysm.name,sysart.extension) FROM sys_rel_medias_referencias sysrmr LEFT JOIN sys_medias sysm ON sysrmr.id_media = sysm.id LEFT JOIN sys_archivos_tipo sysart ON sysm.id_tipo = sysart.id LEFT JOIN sys_tipos syst ON sysart.id_tipo = syst.id WHERE edtn.id = sysrmr.id_padre AND sysrmr.id_referencia = '.$this->referencia.' AND sysrmr.estado = 2 AND syst.id = 1 AND sysm.estado IN(2,4) AND sysm.eliminado = 0 ORDER BY sysrmr.orden ASC LIMIT 1) AS pic
					, (SELECT sysm.titulo FROM sys_rel_medias_referencias sysrmr LEFT JOIN sys_medias sysm ON sysrmr.id_media = sysm.id LEFT JOIN sys_archivos_tipo sysart ON sysm.id_tipo = sysart.id LEFT JOIN sys_tipos syst ON sysart.id_tipo = syst.id WHERE edtn.id = sysrmr.id_padre AND sysrmr.id_referencia = '.$this->referencia.' AND sysrmr.estado = 2 AND syst.id = 1 AND sysm.estado IN(2,4) AND sysm.eliminado = 0 ORDER BY sysrmr.orden ASC LIMIT 1) AS titulo_pic ' . $select;
		$sql_1 .= ' , IFNULL(CONCAT(edta.nombre," ",edta.apellido), edta.nombre) autor ';

		$sql_2 = ' FROM edt_notas edtn
				LEFT JOIN edt_notas_tya edtntya ON edtn.id = edtntya.id_nota
				LEFT JOIN sys_areas_asociadas sysaa ON edtn.id = sysaa.id_padre AND sysaa.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysaa.activo = 1 /* AND sysaa.orden = 1*/
				LEFT JOIN sys_secciones syss ON sysaa.id_seccion = syss.id
				LEFT JOIN sys_comentarios sysc ON edtn.id = sysc.id_padre AND sysc.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysc.id_estado = 2 AND sysc.activo = 1
				LEFT JOIN edt_autores edta ON edtntya.id_autor = edta.id ' . $join . $where;

		$sql_3 = ' GROUP BY edtn.id ORDER BY '.$order;
		
		$sql = $sql_1.$sql_2.$sql_3;

		if($limit) $sql .= ' LIMIT ' . $limit; // El $this->db->escape debe estar afuera

		$query = $this->db->query($sql);

		if (isset($filtros['combo']) && $filtros['combo']){
			return $query->row_array();
		}

		$this->cantidad_notas = $query->num_rows;

		$res = $query->result_array();

		$arr = array();
		if(!empty($res)) foreach ($res as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['titulo_seo'] = $this->parseo_library->clean_url($el['titulo']);
			if($arr[$k]['secciones']){
				$tmp = explode('@',$arr[$k]['secciones']);
				if(!empty($tmp[3])) $tmp[] = $this->parseo_library->clean_url($tmp[3]);
				$arr[$k]['secciones'] = implode('@', $tmp);
			}
			
		}
		$res = $arr;

		return $res;
	}

	public function get_home_destacados($id_seccion = NULL){
		$this->referencia = 34;
		$where = 'WHERE syshg.eliminado = 0 AND syshg.id_estado = 2';
		if ($id_seccion) $where .= ' AND (syshg.id_seccion =' . $this->db->escape($id_seccion) . ' OR syshg.id_seccion IN (SELECT padre FROM sys_secciones WHERE id=' . $this->db->escape($id_seccion) . ') ) ';
		
		$order = ' ORDER BY syshg.orden ASC, syshg.id DESC ';
		
		$sql = 'SELECT syshg.id, syshg.area, syshg.titulo, syshg.descripcion, syshg.link, syshg.pie, syshg.pie_link, (SELECT CONCAT_WS(\'@\',sysm.name,sysart.extension) FROM sys_rel_medias_referencias sysrmr LEFT JOIN sys_medias sysm ON sysrmr.id_media = sysm.id LEFT JOIN sys_archivos_tipo sysart ON sysm.id_tipo = sysart.id LEFT JOIN sys_tipos syst ON sysart.id_tipo = syst.id WHERE syshg.id = sysrmr.id_padre AND sysrmr.id_referencia = '.$this->referencia.' AND sysrmr.estado = 2 AND syst.id = 1 AND sysm.estado IN(2,4) AND sysm.eliminado = 0 ORDER BY sysrmr.orden ASC LIMIT 1) AS pic FROM sys_home_galerias syshg ' . $where . $order;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function get_slides(){
		$where = ' WHERE s.eliminado = 0 AND s.id_estado = 2 and s.vencimiento > NOW()';
		
		$order = ' ORDER BY s.orden DESC';
		
		$sql = 'SELECT s.id, s.descripcion, s.linklogo, s.textosucur FROM cmr_slide s' . $where . $order;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function get_mas_leidos($id_seccion = 0, $cantidad = 5){
		$sql = 'SELECT l.*, CASE WHEN s.url="" AND s.id = 361 THEN "/actualidad-para-novias/vestidos/colecciones/361" ELSE s.url END url
				FROM site_nota_mas_leidas l
				JOIN sys_areas_asociadas a ON l.id = a.id_padre AND a.id_referencia = 12
				JOIN sys_secciones s ON a.id_seccion = s.id AND s.activo = 1 ';
		
		if($id_seccion) $sql .= ' AND s.id = ' . $this->db->escape($id_seccion);
		
		$sql .= ' ORDER BY l.total DESC LIMIT ' . $this->db->escape($cantidad);
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function get_nota($id){
		$this->load->library('parseo_library');

		$this->referencia = 12;
		$sql = 'SELECT edtn.id, edtn.keywords, edtn.metadata, edtntya.titulo, edtntya.copete, edtn.fecha_alta, edtn.fecha_online, edta.nombre, edta.apellido, edta.avatar, edtntya.contenido, COUNT(DISTINCT sysc.id) AS cantidad_comentarios, GROUP_CONCAT(DISTINCT sysu.id,\'@\',sysu.url,\'@\',sysu.observaciones ORDER BY sysu.id ASC SEPARATOR \'~\') AS urls, GROUP_CONCAT(DISTINCT syst.id,\'@\',syst.descripcion ORDER BY sysat.orden ASC SEPARATOR \'~\') AS tags, GROUP_CONCAT(DISTINCT provr.id,\'@\',provr.rubro,\'@\',syss.sucursal,\'@\',syss.id ORDER BY sysar.orden ASC SEPARATOR \'~\') AS rubros
		, GROUP_CONCAT(DISTINCT provp.id,\'@\',provp.proveedor,\'@\',spa.id_minisitio,\'@\',spa.rubro,\'@\',spa.sucursal,\'@\',spa.id_rubro,\'@\',spa.id_sucursal,\'@\',spa.logo,\'@\',spa.breve,\'@\',IFNULL(spa.zonas, ""),\'@\',pm.short_url ORDER BY sysap.orden ASC SEPARATOR \'~\') AS proveedores ';

		$sql.=', (SELECT CONCAT_WS(\'~\', CONCAT_WS(\'@\', syss1.id, syss1.seccion, syss1.url), CONCAT_WS(\'@\', syss2.id, syss2.seccion, syss2.url), CONCAT_WS(\'@\', syss3.id, syss3.seccion, syss3.url)) FROM sys_secciones syss3 LEFT JOIN sys_secciones syss2 ON syss3.padre = syss2.id LEFT JOIN sys_secciones syss1 ON syss2.padre = syss1.id WHERE syss3.id = sysaa.id_seccion ORDER BY sysaa.orden ASC, sysaa.id ASC LIMIT 1) AS seccion, sysaa.id_seccion,	(SELECT CONCAT_WS(\'@\',sysm.name,sysart.extension)
					FROM sys_rel_medias_referencias sysrmr
					LEFT JOIN sys_medias sysm ON sysrmr.id_media = sysm.id
					LEFT JOIN sys_archivos_tipo sysart ON sysm.id_tipo = sysart.id
					LEFT JOIN sys_tipos syst ON sysart.id_tipo = syst.id
					WHERE edtn.id = sysrmr.id_padre
					AND sysrmr.id_referencia = ' . $this->db->escape($this->referencia) . '
					AND sysrmr.estado = 2
					AND syst.id = 1
					AND sysm.estado IN(2,4)
					AND sysm.eliminado = 0
					ORDER BY sysrmr.orden ASC LIMIT 1) AS pic ';
			$sql .= ' ,(	SELECT sysg.id
							FROM sys_galerias sysg
							INNER JOIN sys_rel_galerias_referencias sysrgr ON sysrgr.id_galeria = sysg.id AND sysg.id_estado = 4
							WHERE sysrgr.id_padre=edtn.id AND sysrgr.id_referencia = ' . $this->db->escape($this->referencia) . ' LIMIT 1) AS id_galeria ';
			$sql .= '
					, (SELECT CONCAT(en.id, "@&$", ent.titulo)
					   FROM edt_notas en
					   LEFT JOIN edt_notas_tya ent ON ent.id_nota = en.id
					   WHERE en.id > edtn.id AND en.id_estado = 3
					   LIMIT 1) nota_siguiente
					, (SELECT CONCAT(en.id, "@&$", ent.titulo)
					   FROM edt_notas en
					   LEFT JOIN edt_notas_tya ent ON ent.id_nota = en.id
					   WHERE en.id < edtn.id AND en.id_estado = 3
				  	   ORDER BY DATE(en.fecha_alta) DESC
				  	   LIMIT 1) nota_anterior
				FROM edt_notas edtn
				LEFT JOIN edt_notas_tya edtntya ON edtn.id = edtntya.id_nota
				LEFT JOIN edt_autores edta ON edtntya.id_autor = edta.id
				LEFT JOIN sys_comentarios sysc ON edtn.id = sysc.id_padre AND sysc.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysc.id_estado = 2 AND sysc.activo = 1
				LEFT JOIN sys_asociados_tags sysat ON edtn.id = sysat.id_padre AND sysat.activo = 1
				LEFT JOIN sys_tags syst ON sysat.id_tag = syst.id AND syst.id_referencia = ' . $this->db->escape($this->referencia) . ' AND syst.activo = 1
				LEFT JOIN sys_asociados_rubros sysar ON edtn.id = sysar.id_padre AND sysar.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysar.activo = 1
				LEFT JOIN prov_rubros provr ON sysar.id_rubro = provr.id /*AND provr.id_sucursal = ' . $this->db->escape($this->session->userdata("id_sucursal")) . '*/
				LEFT JOIN sys_sucursales syss ON provr.id_sucursal = syss.id
				LEFT JOIN sys_urls sysu ON sysu.id_padre = edtn.id AND sysu.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysu.estado = 2
				LEFT JOIN sys_asociados_proveedores sysap ON edtn.id = sysap.id_padre AND sysap.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysap.activo = 1 AND sysap.id_portal = 1
				LEFT JOIN prov_proveedores provp ON sysap.id_proveedor = provp.id
				LEFT JOIN prov_minisitios pm ON pm.id_proveedor = provp.id
				LEFT JOIN site_proveedores_activos spa ON provp.id = spa.id_proveedor ';
			$sql .= '
				LEFT JOIN sys_areas_asociadas sysaa ON edtn.id = sysaa.id_padre AND sysaa.id_referencia = ' . $this->db->escape($this->referencia) . ' AND sysaa.activo = 1
				WHERE edtn.id_estado IN (2,3,6) AND edtn.fecha_online <= NOW() AND edtn.id = '. $this->db->escape($id) .'
				GROUP BY edtn.id';

		$query = $this->db->query($sql);
		
		$res = $query->row_array();

		if(!empty($res)) foreach ($res as $k => $el) {
			if($k == 'tags' && $res['tags']){
				$res[$k] = '';
				$tmp = explode('~', $el);
				if(!empty($tmp)) foreach ($tmp as $val) {
					$tmp2 = explode('@', $val);
					$res[$k] .= $val . '@' . $this->parseo_library->clean_url($tmp2[1]) . '~';
				}
				$res[$k] = rtrim($res[$k], '~');
			}
			if($k == 'nota_anterior' && $res['nota_anterior']){
				$tmp = explode('@&$', $el);
				$res[$k] = $tmp[0] . '@&$' . $tmp[1] . '@&$' . $this->parseo_library->clean_url($tmp[1]);
				
			}
			if($k == 'nota_siguiente' && $res['nota_siguiente']){
				$tmp = explode('@&$', $el);
				$res[$k] = $tmp[0] . '@&$' . $tmp[1] . '@&$' . $this->parseo_library->clean_url($tmp[1]);
				
			}
			if($k == 'rubros' && $res['rubros']){
				$res[$k] = '';
				$tmp = explode('~', $el);
				if(!empty($tmp)) foreach ($tmp as $val) {
					$tmp2 = explode('@', $val);
					$res[$k] .= $tmp2[0] . '@' . $tmp2[1] . '@' . $tmp2[2] . '@' . $tmp2[3] . '@' . $this->parseo_library->clean_url($tmp2[1]) . '@' . $this->parseo_library->clean_url($tmp2[2]) . '~';
				}
				$res[$k] = rtrim($res[$k], '~');
			}
			if($k == 'proveedores' && $res['proveedores']){
				$res[$k] = '';
				$tmp = explode('~', $el);
				if(!empty($tmp)) foreach ($tmp as $val) {
					$tmp2 = explode('@', $val);
					$res[$k] .= $tmp2[0] . '@' . $tmp2[1] . '@' . $tmp2[2] . '@' . $tmp2[3] . '@' . $tmp2[4] . '@' . $tmp2[5] . '@' . $tmp2[6] . '@' . $tmp2[7] . '@' . $tmp2[8] . '@' . $tmp2[9] . '@' . $tmp2[10] . '@' . $this->parseo_library->clean_url($tmp2[3]) . '~';
				}
				$res[$k] = rtrim($res[$k], '~');
			}
		}

		if(!empty($res['titulo'])) $res['titulo_seo'] = $this->parseo_library->clean_url($res['titulo']);

		return $res;
	}

	public function get_nota_comentarios($id){
		$this->load->helper('common_helper');

		$this->referencia = 12;
		$sql = 'SELECT GROUP_CONCAT(DISTINCT CONCAT_WS(\'#\',sysc.id,sysc.comentario, IFNULL(sysc.nombre,\'\'), IFNULL(sysc.apellido,\'\'), sysc.email, sysc.fecha, IFNULL(sysc.id_user,\'\')) ORDER BY sysc.id DESC SEPARATOR \'~\') AS comentarios ';
		$sql .= '	FROM edt_notas edtn
		LEFT JOIN sys_comentarios sysc ON edtn.id = sysc.id_padre AND sysc.id_referencia = '.$this->referencia.' AND sysc.id_estado = 2 AND sysc.activo = 1 ';
			$sql .= '	WHERE edtn.id_estado IN (2,3,6) AND edtn.fecha_online <= NOW() AND edtn.id = '. $this->db->escape($id) . '
				GROUP BY sysc.id order by sysc.fecha desc';

		$query = $this->db->query($sql);
		$res = array();
		foreach ($query->result_array() as $k => $value){
			$tmp = explode('#', $value['comentarios']);
			if($tmp[0]) $res[] = $value['comentarios'].'#'.(toColor($k+4));
		}

		return $res;
	}

	public function get_seccion($id){
		$sql = "SELECT padre FROM sys_secciones WHERE id='".$id."'";
		
		$query = $this->db->query($sql);

		$res = $query->row_array();
		
		if($res['padre']!=""){
				$this->seccionPadre=$res['padre'];
		}
		$this->seccionHijo=$id;
	}

	public function get_new_seccion($id_seccion){
		$sql = "SELECT * FROM new_nota_seccion WHERE id_seccion = " . $this->db->escape($id_seccion);

		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function save_visita($id_nota){
		$this->db->set('visitas', 'visitas+1', FALSE);
		$this->db->where('id', $id_nota);
		$this->db->update('edt_notas');
	}

	public function get_tags($filtros){
		$where = ' WHERE 1';
		$group = ' GROUP BY syst.id';
		$order = ' ORDER BY cant DESC';
		$select = 'SELECT syst.id, syst.descripcion AS data, COUNT(sysat.id_padre) cant';
		$from = ' FROM sys_tags syst
				JOIN sys_asociados_tags sysat ON syst.id = sysat.id_tag AND sysat.activo = 1
				';
		$join = '';

		if (isset($filtros['having']) && $filtros['having']){
			$group .= ' HAVING '.$filtros['having'];
		}
		switch ($filtros['tipo_nota']){
			case 12:
			case 24:
			case 25:
			case 26:
			case 27:
			case 29:
			case 30:
			case 31:
				$where .= ' AND syst.id_referencia = '.$filtros['tipo_nota'].' AND edtn.id_estado IN (2,3,6) AND edtn.fecha_online <= NOW()';
				$join .= ' JOIN edt_notas edtn ON sysat.id_padre = edtn.id';
				break;
			case 1415: //galerías y fotos de proveedores
				$select = 'SELECT syst.id, syst.descripcion AS data, COUNT(DISTINCT sysat.id_padre) cant';
				$where .= ' AND syst.id_referencia IN(14,15) AND sysg.id_estado = 4 AND sysrmr.orden = 1 AND sysatall.id_tipo = '.$filtros['tipo'];
				$join .= ' LEFT JOIN sys_galerias sysg ON sysat.id_padre = sysg.id
			LEFT JOIN sys_rel_medias_referencias sysrmr ON sysg.id = sysrmr.id_padre AND sysrmr.id_referencia = 14 AND sysrmr.estado = 2
			LEFT JOIN sys_medias sysmall ON sysrmr.id_media = sysmall.id AND sysmall.eliminado = 0 AND sysmall.col = 1 AND sysmall.estado IN (2,4)
			LEFT JOIN sys_archivos_tipo sysatall ON sysmall.id_tipo = sysatall.id
			';
				break;
			default:
				break;
		}
		$sql = $select.$from.$join.$where.$group.$order;
		
		$tags = array();

		$query = $this->db->query($sql);
		$tags['data'] = $query->result_array();

		$arr = array();
		if(!empty($tags['data'])) foreach ($tags['data'] as $k => $el) {
			$arr[$k] = $el;
			$arr[$k]['data_seo'] = $this->parseo_library->clean_url($el['data']);
		}
		$tags['data'] = $arr;

		$sql2 = 'SELECT MAX(t1.cant) AS maximo, MIN(t1.cant) AS minimo FROM (SELECT COUNT(sysat.id_padre) AS cant '.$from.$join.$where.$group.') t1';

		$query = $this->db->query($sql2);

		$tags['cant_refs'] = $query->row_array();
		return $tags;
	}

	public function get_tag($id){
		$query = $this->db->query('SELECT descripcion FROM sys_tags WHERE id = ' . $this->db->escape($id));
		return $query->row_array();
	}
}