<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('doubleExplode')){
	function doubleExplode($del1, $del2, $array){
		$array1 = explode("$del1", $array);
		foreach($array1 as $key=>$value) {
			$array2 = explode("$del2", $value);
			if (!isset($array3[$array2[0]]))
				$array3[$array2[0]] = array();
			array_push($array3[$array2[0]],(isset($array2[1])&&$array2[1]?$array2[1]:NULL));
		}
		return $array3;
	}	
}

if (!function_exists('doubleExplodeFull')){
	function doubleExplodeFull($del1, $del2, $array){
		$array3 = array();
		$array1 = explode("$del1", $array);
		foreach($array1 as $key=>$value) {
			$array2 = explode("$del2", $value);
			array_push($array3,$array2);
		}
		return $array3;
	}
}

if (!function_exists('limpiarArray')){
	function limpiarArray($data){
		foreach($data as $k=>$v){
			if (empty($v)){
				unset($data[$k]);
			} else {
				if (!is_array($v)){
					$data[$k] = trim($v);
				}
			}
		}
		return $data;
	}
}

if (!function_exists('toColor')){
	function toColor($n) {
	    $n = crc32($n);
	    $n &= 0xffffffff;
	    return("#".substr("000000".dechex($n),-6));
	}
}

if (!function_exists('enUbicacion')){
	function enUbicacion($val){
		return $val['en_ubicacion'];
	}
}