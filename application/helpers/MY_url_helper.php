<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function current_uri() {
	return get_instance()->uri->uri_string();
}

function current_url() {
	return (!empty($_SERVER['QUERY_STRING'])) ? current_uri() . '?' . $_SERVER['QUERY_STRING'] : current_uri();
}