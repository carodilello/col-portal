<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parseo_library{
    protected $CI;
    protected $datos_de_viaje_rubros;
    private $meses = array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
    private $dias = array('domingo','lunes','martes','miercoles','jueves','viernes','sábado');
    private $turnos = array('mañana','mediodía','tarde','noche','todos');

    public function __construct(){
        $this->CI =& get_instance();
    }

    public function url_has_space(){
        if(strstr(urldecode($_SERVER['REQUEST_URI']),' ')){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function clean_url($cadena, $acepta_barras = FALSE, $acepta_puntos = FALSE){ // EX clean_url_3
        $cadena = strtolower($cadena);
        $tofind = array();
        $replac = array();

        $tofind = array_merge($tofind,array('À','Á','Â','Ã','Ä','Å','à','á','â','ã','ä','å','Ò','Ó','Ô','Õ','Ö','Ø','ò','ó','ô','õ','ö','ø','È','É','Ê','Ë','è','é','ê','ë'));
        $replac = array_merge($replac,array('a','a','a','a','a','a','a','a','a','a','a','a','o','o','o','o','o','o','o','o','o','o','o','o','e','e','e','e','e','e','e','e'));

        $tofind = array_merge($tofind,array('ç','Ì','Í','Î','Ï','ì','í','î','ï','Ù','Ú','Û','Ü','ù','ú','û','ü','ÿ','Ñ','ñ','(',')'));
        $replac = array_merge($replac,array('c','i','i','i','i','i','i','i','i','u','u','u','u','u','u','u','u','y','n','n','' ,'' ));

        $tofind = array_merge($tofind,array(',',' ','#','?','%','!','¡','´','–','…',"`",'&','{','}','“','”','"','·','•','*','Ç','ç','°','º','®','¨',"’",'¿',"'",'ª','©',"´",":"));
        $replac = array_merge($replac,array('' ,'-','' ,'' ,'' ,'' ,'' ,'' ,'-','' ,'' ,'y','' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'c','c','' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'','' ));

        $tofind = array_merge($tofind,array('$','+','=','[',']','~','^','\\','|','@'));
        $replac = array_merge($replac,array('' ,'' ,'' ,'-','-','-','' ,''  ,'' ,''));


        if(!$acepta_barras){
            $tofind = array_merge($tofind,array('/'));
            $replac = array_merge($replac,array('-'));
        }

        if(!$acepta_puntos){
            $tofind = array_merge($tofind,array('.'));
            $replac = array_merge($replac,array('-'));
        }

        $cadena_ret = str_replace($tofind,$replac,$cadena);
        $cadena_ret = preg_replace('/-{2,}/','-',$cadena_ret);

        return $cadena_ret;
    }

    public function url_sin_ultima_barra(){
        $url = $this->clean_url(urldecode($_SERVER['REQUEST_URI']), TRUE);

        // limpio lo que venga detras de la ultima barra /
        $tmp = explode("/",$url);
        array_pop($tmp);
        $url = implode("/",$tmp)."/";

        return $url;
    }

    public function filtros_empresas($tipo_listado = 'empresas'){
        $filtros = NULL;
        $pagina = NULL;

        $tipo = '';
        if(isset($tipo_listado) && $tipo_listado){
            $tipo = '?tipo=' . $tipo_listado;
        }

        if(isset($_GET['param'])){
            $param = explode('|',$_GET['param'],2);
            $pagina = $param[0];
            $filtros = '';
            if(isset($param[1]) && $param[1]) $filtros = ($param[1])? $param[1] : '';
        }

        if (!$filtros){
            $pos = strpos(urldecode($_SERVER['REQUEST_URI']),'|');
            $filtros = ($pos === FALSE)?'':substr(urldecode($_SERVER['REQUEST_URI']),$pos);
        }

        if(strpos($filtros,'_')){
            $url = $this->url_sin_ultima_barra() . explode("_",$filtros)[0];
            redirect(base_url($url));
        }

        if(strpos($filtros,'M')){
            $url = $this->url_sin_ultima_barra() . explode("M",$filtros)[0];
            redirect(base_url($url));
        }

        // ---- ## parche redirect para los filtros viejos de "zona", que ahora se llaman "ubicacion del salon" ## ----

        if($filtros == '|36-526'){ // zona: capital federal
            $url = $this->url_sin_ultima_barra().'|115-4479'.(strpos($filtros,'|')?$filtros:'|'.$filtros).$tipo;
            redirect(base_url($url));
        }

        if($filtros == '|36-4355'){ // zona: GBA Zona oeste
            $url = $this->url_sin_ultima_barra().'|115-4482'.(strpos($filtros,'|')?$filtros:'|'.$filtros).$tipo;
            redirect(base_url($url));
        }

        if($filtros == '|36-4358'){ // zona: GBA Zona norte
            $url = $this->url_sin_ultima_barra().'|115-4480'.(strpos($filtros,'|')?$filtros:'|'.$filtros).$tipo;
            redirect(base_url($url));
        }

        if($filtros == '|36-4354'){ // zona: GBA Zona sur
            $url = $this->url_sin_ultima_barra().'|115-4481'.(strpos($filtros,'|')?$filtros:'|'.$filtros).$tipo;
            redirect(base_url($url));
        }

        // ---- ## fin parche ## ----

        return array("pagina" => $pagina, "filtros" => $filtros);
    }

    public function criterios_ordenar($tipo='empresas',$destacado=1){
        $ret = array();
        switch ($tipo){
            case 'empresas':
                if ($destacado){
                    array_push($ret,array('value'=> 'nivel','option' => 'Destacados'));
                }
                array_push($ret,array('value'=> 'proveedor','option' => 'Empresas AZ'));
                array_push($ret,array('value'=> 'proveedor-DESC','option' => 'Empresas ZA'));
                break;
            case 'paquetes':
                if ($destacado){
                    array_push($ret,array('value'=> 'nivel','option' => 'Destacados'));
                }
                array_push($ret,array('value'=> 'titulo','option' => 'Titulo AZ'));
                array_push($ret,array('value'=> 'titulo DESC','option' => 'Titulo ZA'));
                array_push($ret,array('value'=> 'invitados','option' => 'Invitados (Menor a Mayor)'));
                array_push($ret,array('value'=> 'invitados-DESC','option' => 'Invitados (Mayor a Menor)'));
                break;
            case 'productos': case 'promociones':
                if ($destacado){
                    array_push($ret,array('value'=> 'nivel','option' => 'Destacados'));
                }
                array_push($ret,array('value'=> 'titulo','option' => 'Titulo AZ'));
                array_push($ret,array('value'=> 'titulo-DESC','option' => 'Titulo ZA'));
                break;
        }
        return $ret;
    }

    public function foto_para_listado($array){ // Parsea el array para obtener una foto para mostrar en el listado
        if(!is_array($array) || empty($array)) return NULL;

        foreach ($array as $key => $value) {
            $res_array[$value["id_proveedor"]] = $value;
        }

        return $res_array;
    }

    public function equivalencia_iconos(){
        /* salones de fiesta */
        $icon[17] = 'salones_de_fiesta';
        $icon[47] = 'salones_de_fiesta';
        $icon[154] = 'salones_de_fiesta';
        $icon[63] = 'salones_de_fiesta';
        $icon[252] = 'salones_de_fiesta';
        $icon[381] = 'salones_de_fiesta';
        $icon[298] = 'salones_de_fiesta';
        $icon[169] = 'salones_de_fiesta';
        $icon[403] = 'salones_de_fiesta';
        $icon[392] = 'salones_de_fiesta';
        $icon[370] = 'salones_de_fiesta';

        /* catering */
        $icon[5] = 'catering';
        $icon[46] = 'catering';
        $icon[156] = 'catering';
        $icon[62] = 'catering';
        $icon[719] = 'food_truck';
        $icon[383] = 'catering';
		$icon[291] = 'catering';
		$icon[171] = 'catering';
		$icon[405] = 'catering';
		$icon[394] = 'catering';
		$icon[372] = 'catering';
		$icon[718] = 'food_truck';

        /* alquiler de livings */
        $icon[36] = 'alquiler_de_livings';
        $icon[88] = 'alquiler_de_livings';
        $icon[195] = 'alquiler_de_livings';
        $icon[89] = 'alquiler_de_livings';

        /* mesas dulces */
        $icon[37] = 'mesa_dulce';
        $icon[183] = 'mesa_dulce';

        /* bebidas */
        $icon[4] = 'bebidas';
        $icon[92] = 'bebidas';
        $icon[178] = 'bebidas';
        $icon[93] = 'bebidas';
        $icon[313] = 'bebidas';

        /* cotillon */
        $icon[29] = 'cotillon';
        $icon[51] = 'cotillon';
        $icon[181] = 'cotillon';
        $icon[67] = 'cotillon';
        $icon[246] = 'cotillon';

        /* disc jockey*/
        $icon[7] = 'disc_jockeys';
        $icon[49] = 'disc_jockeys';
        $icon[182] = 'disc_jockeys';
        $icon[71] = 'disc_jockeys';
        $icon[679] = 'disc_jockeys';
        $icon[292] = 'disc_jockeys';
        $icon[172] = 'disc_jockeys';
        $icon[669] = 'disc_jockeys';
        $icon[702] = 'disc_jockeys';

        /* ambientacion y centros de mesa*/
        $icon[31] = 'ambientacion';
        $icon[45] = 'ambientacion';
        $icon[174] = 'ambientacion';
        $icon[61] = 'ambientacion';
        $icon[245] = 'ambientacion';

        /* autos para casamiento */
        $icon[3] = 'autos';
        $icon[72] = 'autos';
        $icon[179] = 'autos';
        $icon[73] = 'autos';

        /* shows musicales */
        $icon[86] = 'shows_musicales';
        $icon[48] = 'shows_musicales';
        $icon[196] = 'shows_musicales';
        $icon[64] = 'shows_musicales';
        $icon[744] = 'shows_musicales';
        $icon[337] = 'shows_musicales';
        $icon[335] = 'shows_musicales';
        $icon[745] = 'shows_musicales';

        /* propuestas originales */
        $icon[39] = 'propuestas_originales';
        $icon[90] = 'propuestas_originales';
        $icon[274] = 'propuestas_originales';
        $icon[91] = 'propuestas_originales';

        /* foto y video */
        $icon[8] = 'foto_y_video';
        $icon[50] = 'foto_y_video';
        $icon[157] = 'foto_y_video';
        $icon[66] = 'foto_y_video';
        $icon[384] = 'foto_y_video';
        $icon[247] = 'foto_y_video';
        $icon[173] = 'foto_y_video';
        $icon[395] = 'foto_y_video';
        $icon[373] = 'foto_y_video';

        /* casas de regalos */
        $icon[35] = 'casas_de_regalos';
        $icon[52] = 'casas_de_regalos';
        $icon[185] = 'casas_de_regalos';
        $icon[76] = 'casas_de_regalos';
        $icon[725] = 'casas_de_regalos';
        $icon[714] = 'casas_de_regalos';
        $icon[704] = 'casas_de_regalos';

        /* participaciones */
        $icon[9] = 'participaciones';
        $icon[74] = 'participaciones';
        $icon[184] = 'participaciones';
        $icon[75] = 'participaciones';

        /* wedding planner */
        $icon[32] = 'wedding_planner';
        $icon[44] = 'wedding_planner';
        $icon[179] = 'wedding_planner';
        $icon[60] = 'wedding_planner';
        $icon[288] = 'wedding_planner';

        /* shows de entretenimiento */
        $icon[87] = 'shows_de_entretenimiento';
        $icon[175] = 'shows_de_entretenimiento';

        /* salones de hoteles */
        $icon[81] = 'salones_de_hoteles';
        $icon[176] = 'salones_de_hoteles';
        $icon[306] = 'salones_de_hoteles';
        $icon[686] = 'salones_de_hoteles';
        $icon[703] = 'salones_de_hoteles';

        /* alianzas y accesorios */
        $icon[2] = 'alianzas';
        $icon[43] = 'alianzas';
        $icon[272] = 'alianzas';
        $icon[59] = 'alianzas';
        $icon[740] = 'alianzas';
        $icon[732] = 'alianzas';
        $icon[733] = 'alianzas';
        $icon[742] = 'alianzas';
        $icon[741] = 'alianzas';
        $icon[743] = 'alianzas';

        /* maquillaje */
        $icon[12] = 'maquillaje';
        $icon[40] = 'maquillaje';
        $icon[187] = 'maquillaje';
        $icon[56] = 'maquillaje';
        $icon[34] = 'cuidado_piel';
        $icon[190] = 'cuidado_piel';
        $icon[309] = 'maquillaje';
        $icon[301] = 'maquillaje';
        $icon[319] = 'maquillaje';

        /* zapatos */
        $icon[83] = 'zapatos';
        $icon[186] = 'zapatos';
        $icon[639] = 'zapatos';

        /* trajes de etiqueta */
        $icon[15] = 'trajes';
        $icon[42] = 'trajes';
        $icon[189] = 'trajes';
        $icon[58] = 'trajes';

        /* peinados */
        $icon[82] = 'peinados';
        $icon[273] = 'peinados';

        /* ajuar */
        $icon[1] = 'ajuar';
        $icon[276] = 'ajuar';

        /* ramos y tocados */
        $icon[26] = 'ramos';
        $icon[188] = 'ramos';

        /* vestidos usados y terminados */
        $icon[97] = 'vestidos_usados';
        $icon[41] = 'vestidos_usados';

        /* vestidos de novia */
        $icon[14] = 'vestidos_de_novia';
        $icon[191] = 'vestidos_de_novia';
        $icon[57] = 'vestidos_de_novia';
        $icon[388] = 'vestidos_de_novia';
        $icon[296] = 'vestidos_de_novia';
        $icon[254] = 'vestidos_de_novia';
        $icon[304] = 'vestidos_de_novia';
        $icon[410] = 'vestidos_de_novia';
        $icon[399] = 'vestidos_de_novia';
        $icon[377] = 'vestidos_de_novia';

        /* luna de miel */
        $icon[10] = 'luna_de_miel';
        $icon[69] = 'luna_de_miel';
        $icon[332] = 'luna_de_miel';
        $icon[53] = 'luna_de_miel';

        /* quintas y estancias */
        $icon[30] = 'quintas';
        $icon[155] = 'quintas';
        $icon[382] = 'quintas';
        $icon[280] = 'quintas';
        $icon[404] = 'quintas';
        $icon[393] = 'quintas';

        /* civil */
        $icon[77] = 'civil';
        $icon[94] = 'civil';
        $icon[177] = 'civil';
        $icon[95] = 'civil';

        /* noche de bodas */
        $icon[13] = 'noche_de_bodas';
        $icon[193] = 'noche_de_bodas';
        $icon[70] = 'noche_de_bodas';

        /* souvenirs */
        $icon[84] = 'souvenirs';
        $icon[275] = 'souvenirs';
        $icon[251] = 'souvenirs';


        return $icon;
    }

    public function procesar_telefonos($telefonos){
        $tels = explode(' / ',$telefonos);
        $ret = '';
        foreach ($tels as $tel){
            $tel_temp = explode('@',$tel);
            if (count($tel_temp) > 1){
                $ret .= '('.$tel_temp[0].') '.$tel_temp[1].((isset($tel_temp[2])&&$tel_temp[2])? ' int. '.$tel_temp[2] : '').' / ';
            } else {
                $ret .= $tel.' / ';
            }
        }
        return substr($ret,0,-3);
    }

    public function procesar_telefono($telefono){
        if(isset($telefono[1])){
            $tel_temp = explode('@', $telefono[1]);
            if(is_array($tel_temp)){
                if (count($tel_temp) > 1){
                    $ret = ($tel_temp[0]?'('.$tel_temp[0].') ':'').$tel_temp[1].((isset($tel_temp[2])&&$tel_temp[2])? ' int. '.$tel_temp[2] : '');
                } else {
                    $ret = $telefono[1];
                }

            }
            return array($telefono[0], $ret);
        }
    }

    public function asociados_trabaja($array){
        $return = NULL;

        if(is_array($array)){
            $rubros = array();
            foreach ($array as $k => $val) {
                $rubros[$val["rubro"]][$k] = $val;
            }
            $return = $rubros;
        }

        return $return;
    }

    //
    // Ordenamos el array para que el orden de los elementos sea:
    // 1.- Promociones
    // 2.- Productos
    // 3.- Paquetes
    //
    // En caso de que no haya valores, se rellenará con lo que haya disponible pero siempre siguiendo ese criterio de orden
    //
    //
    // Ejemplo (1): 4 promociones, 4 productos = 2 promociones - 4 productos
    // Ejemplo (2): 5 promociones, 5 productos, 1 paquete = 2 promociones - 3 productos - 1 paquetes
    // Ejemplo (3): 1 promocion, 1 producto, 5 paquetes = 1 promocion - 1 producto - 4 paquetes
    //
    // De esta manera siempre se mostraran los 6 valores que exige el diseño
    //
    public function minisitio_ordenar_pyp($elementos){
        for ($i=0; $i < 6; $i++) {
            $res[$i] = array();
        }
        $aux = array();

        $no_hay = array(
            'promociones' => FALSE,
            'productos'   => FALSE,
            'paquetes'    => FALSE
        );
        $countTot = 0;
        $count = 0;

        if($elementos['promociones']){
            foreach ($elementos['promociones'] as $k => $elemento){
                if($k < 6){
                    $res[$k] = $elemento;
                    $countTot += 1;

                }else{
                    break;
                }
            }
        }else{
            $no_hay['promociones'] = TRUE;
        }

        if($elementos['productos']){

            if($countTot > 2){
                $countTot = 2;
            }

            foreach ($elementos['productos'] as $k => $elemento){
                $k = $k + $countTot;
                if($k < 6){
                    if($k < 2){

                        if(!$res[$k]){
                            $res[$k] = $elementos['productos'][$count];
                             $count += 1;
                        }
                    }else{
                        if($res[$k]) $aux[] = $res[$k];
                        $res[$k] = $elementos['productos'][$count];
                        $count += 1;
                    }

                }else{
                    break;
                }
            }
        }else{
            $no_hay['productos'] = TRUE;
        }

        $countTot = $countTot + $count;

        if($elementos['paquetes']){
            if($countTot > 4){
                $countTot = 4;
            }
            foreach ($elementos['paquetes'] as $k => $elemento){
                $k = $k + $countTot;
                if($k < 6){
                    if($k < 4){
                        if(!$res[$k]){
                            $res[$k] = $elemento;
                        }
                    }else{
                        if($res[$k]) $aux[] = $res[$k];
                        $res[$k] = $elemento;
                    }
                }else{
                    break;
                }
            }
        }else{
            $no_hay['paquetes'] = TRUE;
        }

        $necesita_reordenar = FALSE;
        if($res && $aux){
            $m = 0;
            for($i=0; $i < 6; $i++){
                if(!$res[$i] && !empty($aux[$m])){
                    $res[$i] = $aux[$m];
                    $m += 1;
                    $necesita_reordenar = TRUE;
                }
            }
        }

        if($necesita_reordenar){
            $l = 0;
            $res_ordenado = array();
            foreach ($res as $k => $val) {
                if(!empty($val) && $val['tipo'] == 'promociones'){
                    $res_ordenado[$l] = $val;
                    $l += 1;
                }
            }
            foreach ($res as $k => $val) {
                if(!empty($val) && $val['tipo'] == 'productos'){
                    $res_ordenado[$l] = $val;
                    $l += 1;
                }
            }
            foreach ($res as $k => $val) {
                if(!empty($val) && $val['tipo'] == 'paquetes'){
                    $res_ordenado[$l] = $val;
                    $l += 1;
                }
            }
            $res = $res_ordenado;
        }

        if($no_hay['promociones'] && $no_hay['productos'] && $no_hay['paquetes']){
            $res = NULL;
        }

        if(is_array($res)){
            $res2 = array();
            foreach ($res as $k => $v){
                if($v) $res2[$k] = $v;
            }
            $res = $res2;
        }

        return $res;
    }

    public function promedios_puntajes_comentarios($comentarios = NULL){
        $res = NULL;

        $promedio = array();
        $promedio_total = 0;
        if($comentarios){
            $sum_estrellas_total = 0;
            $cant_estrellas_total = 0;
            foreach ($comentarios as $k => $comentario) {
                $com = explode("#", $comentario);
                $sum_estrellas = 0;
                $cant_estrellas = 0;
                for ($i=7; $i <= 11; $i++) {
                    if($com[$i]){
                        $sum_estrellas += $com[$i];
                        $cant_estrellas += 1;
                    }
                }
                if($sum_estrellas && $cant_estrellas){
                    $promedio[$k] = $sum_estrellas/$cant_estrellas;
                    $sum_estrellas_total += $promedio[$k];
                    $cant_estrellas_total += 1;
                }
            }

            if($sum_estrellas_total && $cant_estrellas_total){
                $promedio_total = $sum_estrellas_total/$cant_estrellas_total;
            }
        }

        return array(
            'puntajes'       => $promedio,
            'puntajes_total' => $promedio_total
        );
    }

    public function agrupar_fotos($fotos, $cantidad = 6){
        if(!$fotos) return NULL;

        $res = array();

        $i = 0;
        $j = 0;
        if(is_array($fotos)) foreach ($fotos as $k => $foto){
            $i += 1;
            if($i < $cantidad){
                $res[$j][$k] = $foto;
            }else{
                $res[$j][$k] = $foto;
                $i = 0;
                $j += 1;
            }
        }
        return $res;
    }

    public function dir_html_strong($direccion = NULL){ // PARA MAPA MINISITIO
        if(!$direccion) return NULL;

        if(is_string($direccion)){
            $tmp = explode('-', $direccion);
            if(is_array($tmp)){
                $dir_strong = $tmp[0];
                unset($tmp[0]);
                $dir = array(
                    'strong' => $dir_strong
                );
                if(count($tmp) > 0){
                    $dir['normal'] = $tmp;
                }
                $ret = $dir;
            }
        }else{
            $ret = NULL;
        }

        return $ret;
    }

    public function minisitio_form_data($id_minisitio, $proveedor, $id_origen = NULL, $id_item = NULL, $prov_vencido = FALSE, $item_vencido = FALSE){ // FORMULARIO LATERAL
        $this->CI->load->model('rubros_model');
        $this->CI->load->model('usuarios_model');
        $this->CI->load->model('navegacion_model');

        $this->CI->load->helper('cookie');

        if($prov_vencido){
            $hidden[] = array('name' => 'id_origen', 'value' => 46);
        }elseif($item_vencido){
            $hidden[] = array('name' => 'id_origen', 'value' => 47);
        }else{
            $hidden[] = array('name' => 'id_origen', 'value' => $id_origen?$id_origen:31);
        }


        $hidden[] = array('name' => 'id_minisitio', 'value' => $id_minisitio);
        $hidden[] = array('name' => 'id_rubro', 'value' => $proveedor['id_rubro']);
        $hidden[] = array('name' => 'referer', 'value' => $_SERVER["REQUEST_URI"]);

        if($id_item){
            $hidden[] = array('name' => 'id_item', 'value' => $id_item);
            if(!$item_vencido){
                $hidden[] = array('name' => 'id_landing', 'value' => 0);
                $hidden[] = array('name' => 'id_user', 'value' => NULL);
                $hidden[] = array('name' => 'id_tipo_evento', 'value' => 1);
                $hidden[] = array('name' => 'id_pais', 'value' => 10);
            }
        }

        $fecha_variable = $this->CI->rubros_model->opcion_fecha_variable($proveedor['id_rubro']);

        if (!empty($_SESSION['id_user'])){
            $data_form = $this->CI->usuarios_model->get_novio_info($_SESSION['id_user']);
        }else{
            $data_form = $this->asignar_mayoria_campos_formulario();
        }
        $data_form['verificador_captcha'] = get_cookie("verificador_captcha");

        $provincias = $this->CI->navegacion_model->get_provincias(0);
        $localidades = NULL;
        if(!empty($data_form['id_provincia'])){
            $localidades = $this->CI->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        $datos_viaje   = $this->datos_viaje($proveedor);
        $salon_elegido = $this->salon_elegido($proveedor);

        $ret = array(
            'hidden'         => $hidden,
            'data_form'      => $data_form,
            'datos_viaje'    => $datos_viaje,
            'localidades'    => $localidades,
            'provincias'     => $provincias,
            'salon_elegido'  => $salon_elegido,
            'fecha_variable' => $fecha_variable
        );

        return $ret;
    }

    public function asignar_mayoria_campos_formulario($id_evn = NULL, $tipo = 'novios'){
        $this->CI->load->helper('cookie');

        if(!$id_evn){
            
            $res = array(
                'anio_evento'     => get_cookie("anio_evento"),
                'mes_evento'      => get_cookie("mes_evento"),
                'dia_evento'      => get_cookie("dia_evento"),
                'ubicacion'       => get_cookie("ubicacion"),
                'invitados'       => get_cookie("invitados"),
                'nombre'          => get_cookie("nombre"),
                'apellido'        => get_cookie("apellido"),
                'anio_nacimiento' => get_cookie("anio_nacimiento"),
                'mes_nacimiento'  => get_cookie("mes_nacimiento"),
                'dia_nacimiento'  => get_cookie("dia_nacimiento"),
                'email'           => get_cookie("email"),
                'telefono'        => get_cookie("telefono"),
                'id_provincia'    => get_cookie("id_provincia"),
                'id_localidad'    => get_cookie("id_localidad"),
                'infonovias'      => 1,
                'promociones'     => 1
            );

        }else{

            $this->CI->load->model('eventos_model');

            $novio = $this->CI->eventos_model->get_datos_registrado($id_evn, $tipo);

            if($novio['id']){

                if(!empty($novio['casamiento'])){
                    $fecha = explode('-', $novio['casamiento']);
                    $dia_evento = (!empty($fecha[2]) ? ($fecha[2] . '/') : '') . (!empty($fecha[1]) ? ($fecha[1] . '/') : '') . (!empty($fecha[0]) ? $fecha[0] : '');
                }

                $res = array(
                    'ubicacion'         => !empty($novio['ubicacion']) ? $novio['ubicacion'] : '',
                    'invitados'         => !empty($novio['invitados']) ? $novio['invitados'] : '',
                    'nombre'            => !empty($novio['nombre']) ? $novio['nombre'] : '',
                    'apellido'          => !empty($novio['apellido']) ? $novio['apellido'] : '',
                    'anio_evento'       => !empty($fecha[0]) ? $fecha[0] : '',
                    'mes_evento'        => !empty($fecha[1]) ? $fecha[1] : '',
                    'dia_evento'        => !empty($dia_evento) ? $dia_evento : '',
                    'dni'               => !empty($novio['dni']) ? $novio['dni'] : '',
                    'email'             => !empty($novio['email']) ? $novio['email'] : '',
                    'telefono'          => !empty($novio['telefono']) ? $novio['telefono'] : '',
                    'celular'           => !empty($novio['celular']) ? $novio['celular'] : '',
                    'id_provincia'      => !empty($novio['id_provincia']) ? $novio['id_provincia'] : '',
                    'id_localidad'      => !empty($novio['id_localidad']) ? $novio['id_localidad'] : '',
                    'id_rubro'          => !empty($novio['id_rubro']) ? $novio['id_rubro'] : '',
                    'otro_rubro'        => !empty($novio['otro_rubro']) ? $novio['otro_rubro'] : '',
                    'empresa'           => !empty($novio['empresa']) ? $novio['empresa'] : '',
                    'infonovias'        => 1,
                    'promociones'       => 1
                );

                if(!empty($novio['suscrip'])){
                    $subsc = explode(',',$novio['suscrip']);
                    for($i=0; $i < count($subsc); $i++){
                        if(!empty($subsc[$i])){
                            $sub_1 = explode("@", $subsc[$i]);
                            if(!empty($sub_1[0]) && $sub_1[0] == 1) $res['infonovias']   = $sub_1[1];
                            if(!empty($sub_1[0]) && $sub_1[0] == 3) $res['promociones']  = $sub_1[1];    
                        }
                    }    
                }

                $res['id_novio'] = $id_evn;
                $res['update_data'] = TRUE;
            }

        }

        return $res;
    }

    public function datos_viaje($proveedor){
        $this->datos_de_viaje_rubros = array(10,258,259,260,261,262,263,264,265,266,267,268,269,270,271,194);
        $datos_viaje = NULL;
        if(in_array($proveedor['id_rubro'],$this->datos_de_viaje_rubros)){
            $datos_viaje = 1;
        }

        return $datos_viaje;
    }

    public function salon_elegido($proveedor){
        $salon_elegido_rubro=array(30,47,63,17,81,154,252,155,280,176,253);
        $salon_elegido = NULL;

        if(!in_array($proveedor['id_rubro'],$salon_elegido_rubro) && !in_array($proveedor['id_rubro'],$this->datos_de_viaje_rubros)){
            $salon_elegido = 1;
        }

        return $salon_elegido;
    }

    public function get_padres_de_rubros($data){ // PARSEAR LISTADO DE RUBROS
        $rubros = array();
        $col = 1;
        $subcol = 1;
        if(is_array($data)){
            $m = 1;
            $n = 1;
            foreach ($data as $k => $rubro){
                if(strtolower($rubro['padre']) == 'la fiesta' || strtolower($rubro['padre']) == 'lista de regalos' || strtolower($rubro['padre']) == 'trajes'){
                    $m += 1;
                }
            }

            foreach ($data as $k => $rubro){
                if(strtolower($rubro['padre']) == 'salones' || strtolower($rubro['padre']) == 'vestidos' || strtolower($rubro['padre']) == 'civiles'){
                    $col = 1;
                    $subcol = 1;
                }

                if(strtolower($rubro['padre']) == 'la fiesta'){
                    $col = 2;
                    $subcol = ((($m/2)-2)>$n)&&$m>10?2:1;
                    $n += 1;
                }

                if(strtolower($rubro['padre']) == 'lista de regalos'){
                    $col = 2;
                    $subcol = 3;
                }

                if(strtolower($rubro['padre']) == 'trajes'){
                    $col = 2;
                    $subcol = 4;
                }

                if(strtolower($rubro['padre']) == 'belleza' || strtolower($rubro['padre']) == 'luna de miel' || strtolower($rubro['padre']) == 'noche de bodas'){
                    $col = 3;
                    $subcol = 1;
                }

                $rubros[$col][$subcol][$rubro['padre']][] = $rubro;
                $col = 1;
                $subcol = 1;
            }

        }

        return $rubros;
    }

    public function resultados_busqueda_home($arr){
        $arr_res = '';
        $puntos_suspensivos = '';
        if(is_array($arr)) foreach ($arr as $k => $val){
            if(strlen(strip_tags($val["nombre"])) >= (!$val['rubro'] ? 50: 25)) $puntos_suspensivos = "...";
            if($val['subdominio']){
                $arr_res .= '<a href="' . $val['url'];
            }else{
                $arr_res .= '<a href="' . base_url($val['url']);
            }

            $arr_res .= '" class="block sug" title="'.$val['nombre'].'">'.(mb_substr(strip_tags($val['nombre']),0,(!$val['rubro'] ? 50: 25)).$puntos_suspensivos).($val['rubro']?(' <span class="rubro" >(' . $val['rubro'] . ')'):'').'</span> <i class="fa '.$val['class'].' pull-right"></i></a>';
            $puntos_suspensivos = '';
        }

        return $arr_res;
    }

    public function titulo_pedido_presupuesto($area, $tipo, $rubro, $empresa, $opciones, $filtros_aplicados){
        if($area == 'presupuesto'){
            $titulo = 'Pedir presupuesto';

            if($tipo != 'resultados'){
                $titulo .= ' a ';
                $titulo_color = $empresa['proveedor'];
            }
        }elseif($area == 'registro'){
            $titulo = 'Formulario de registro';
        }else{
            $titulo = 'Envianos tus comentarios y sugerencias';
        }

        if($tipo == 'empresa'){
            $titulo .= ' rubro ';
            $titulo_color = $empresa['rubro'];
        }elseif($tipo == 'resultados'){
            if($opciones){
                $titulo .= ' a todas las empresas del rubro ';
                $titulo_color = $rubro['rubro'];
            }else{
                $titulo .= ' a todas las empresas '.($filtros_aplicados?' seleccionadas':'').' del rubro ';
                $titulo_color = $rubro['rubro'];

            }
        }

        return array('titulo' => $titulo, 'titulo_color' => $titulo_color);
    }

    public function paginador_desde_hasta($pagina, $paginas, $paginas_margen){
        if($pagina <= $paginas_margen){
            $desde = 1;
            if ($paginas < ($pagina+($paginas_margen*2+1-$pagina))){
                $hasta = $paginas;
            }else{
                $hasta = ($pagina+($paginas_margen*2+1-$pagina));
            }
        }elseif($pagina >= ($paginas-$paginas_margen)){
            $desde = ($pagina-$paginas_margen);
            $hasta = $paginas;
        }else{
            $desde = $pagina-$paginas_margen;
            $hasta = $pagina+$paginas_margen;
        }

        return array('desde' => $desde, 'hasta' => $hasta);
    }

    public function secciones_para_home_notas($arr, $esquema){
        $this->CI->load->model('editorial_model');

        $arr_res = array();
        $arr_res_limit = array();
        $ids_array = array();
        if(is_array($arr)){
            foreach ($arr as $v) {
                $tmp = explode('@', $v['secciones']);
                if($tmp[2]){
                    $id_el = $tmp[2];
                }else{
                    $id_el = $tmp[0];
                }
                $arr_res[$id_el][] = $v;
                $ids_array[$id_el][] = $v['id'];
            }

            foreach ($arr_res as $k => $data){
                $count_data = count($data);
                if($count_data < 2){
                    $ids_string = implode(',', $ids_array[$k]);
                    $mas_notas = $this->CI->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 1, 'not_ids' => $ids_string, 'id_seccion' => $k), FALSE, 2-$count_data);
                    if(is_array($mas_notas) && count($mas_notas) > 0) foreach ($mas_notas as $nota){
                        $arr_res[$k][] = $nota;
                    }
                }
            }

            foreach ($arr_res as $k => $val){
                foreach ($val as $key => $value){
                    if($key < 2) $arr_res_limit[$k][] = $value;
                }
            }


            // AGREGADO TEMPORAL
            $sum_array = array();
            foreach ($arr_res_limit as $k => $val){
                if(in_array($k,$esquema)) $sum_array[] = $k;
            }

            if(count($sum_array) != count($esquema)){
                foreach ($esquema as $v) {
                    if(!in_array($v, $sum_array)){
                        $mas_notas = $this->CI->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 1, 'id_seccion' => $v), FALSE, 2);
                        if($mas_notas) foreach ($mas_notas as $k => $nota) {
                            $arr_res_limit[$v][] = $nota;
                        }
                    }
                }
            }
            // FIN AGREGADO TEMPORAL
        }

        return $arr_res_limit;
    }

    public function proveedores_para_carousel($arr, $div = 4){ // A LOS PROVEEDORES LOS SEPARAMOS POR GRUPOS
        if(is_array($arr)){
            $arr_res = array();
            $i = 0;
            $key = 0;
            foreach ($arr as $k => $value) {
                $arr_res[$key][] = $value;

                $i += 1;
                if($i == $div){
                    $key += 1;
                    $i = 0;
                }
            }

            return $arr_res;
        }
    }

    public function listado_rubros_seo($arr, $cols = 4){ // PONEMOS TODOS LOS ELEMENTOS HIJOS EN COLUMNAS
        $arr_res = array();
        $sum = 0;
        if(is_array($arr)){
            foreach ($arr as $key => $value) {
                $sum += count($value['hijos']);
            }

            $res = $sum/$cols;
            $sum_col = round($sum/$cols, 0, PHP_ROUND_HALF_UP);

            if($res > $sum_col) $sum_col += 1;

            $i = 0;
            $j = 0;
            foreach ($arr as $k => $v){
                foreach ($v['hijos'] as $key => $value) {
                    $arr_res[$i][] = $value;
                    $j += 1;
                    if($j == $sum_col){
                        $i += 1;
                        $j = 0;
                    }
                }
            }

            return $arr_res;
        }
    }

    public function listado_rubros_seo_from_rubros($arr, $cols = 4){ // PONEMOS TODOS LOS ELEMENTOS HIJOS EN COLUMNAS
        $arr_res = array();
        if(is_array($arr)){
            $count = count($arr);
            $res = $count/$cols;
            $sum_col = round($count/$cols, 0, PHP_ROUND_HALF_UP);

            if($res > $sum_col) $sum_col += 1;

            $i = 0;
            $j = 0;
            foreach ($arr as $key => $value){
                $arr_res[$i][$key] = $value;
                $j += 1;
                if($j == $sum_col){
                    $i += 1;
                    $j = 0;
                }
            }

            return $arr_res;
        }
    }

    public function get_meses(){
        return $this->meses;
    }

    public function get_dias(){
        return $this->dias;
    }

    public function get_turnos(){
        return $this->turnos;
    }

    public function videos_to_json($videos){
        $videos_json = array();
        foreach ($videos as $k => $video){
            $tmp_rubros = explode('@', $video['rubros']);
            $video['rubros'] = array();
            if(!empty($tmp_rubros)) $video['rubros'][] = $tmp_rubros;

            $tmp_proveedores = explode('@', $video['proveedores']);
            $video['proveedores'] = array();
            if(!empty($tmp_proveedores)) $video['proveedores'][] = $tmp_proveedores;

            $tmp_tags_groups = explode('~', $video['tags']);
            $video['tags'] = array();

            foreach ($tmp_tags_groups as $key => $v) {
                $tmp_tags = explode('@', $v);

                if(!empty($tmp_tags)) $video['tags'][] = $tmp_tags;
            }


            $videos_json[] = $video;
        }
        return json_encode($videos_json);
    }

    public function get_rubro_coltv($id){
        switch ($id){
            case 326:
                return 98;
            case 327:
                return 97;
            case 328:
                return 14;
            case 329:
                return 17;
            case 330:
                return 81;
            case 331:
                return 30;
            case 332:
                return 15;
            case 333:
                return 12;
            case 334:
                return 82;
            case 335:
                return 2;
            case 336:
                return 34;
            case 337:
                return 26;
            case 338:
                return 1;
            case 339:
                return 83;
            case 340:
                return 36;
            case 341:
                return 31;
            case 342:
                return 3;
            case 343:
                return 4;
            case 344:
                return 5;
            case 345:
                return 29;
            case 346:
                return 7;
            case 347:
                return 8;
            case 348:
                return 37;
            case 349:
                return 9;
            case 350:
                return 39;
            case 351:
                return 87;
            case 352:
                return 86;
            case 353:
                return 84;
            case 354:
                return 32;
            case 355:
                return 35;
            case 356:
                return 10;
            case 357:
                return 77;
            case 398:
                return 13;
            case 399:
                return 10;
            case 400:
                return 77;
            case 100:
                return array($this->get_rubro_coltv('333'),$this->get_rubro_coltv('334'),$this->get_rubro_coltv('335'),$this->get_rubro_coltv('336'),$this->get_rubro_coltv('337'),$this->get_rubro_coltv('338'),$this->get_rubro_coltv('339'));
            case 101:
                return array($this->get_rubro_coltv('326'),$this->get_rubro_coltv('327'),$this->get_rubro_coltv('328'));
            case 102:
                return array($this->get_rubro_coltv('332'));
            case 103:
                return array($this->get_rubro_coltv('329'),$this->get_rubro_coltv('330'),$this->get_rubro_coltv('331'));
            case 104:
                return array($this->get_rubro_coltv('340'),$this->get_rubro_coltv('341'),$this->get_rubro_coltv('342'),$this->get_rubro_coltv('343'),$this->get_rubro_coltv('344'),$this->get_rubro_coltv('345'),$this->get_rubro_coltv('346'),$this->get_rubro_coltv('347'),$this->get_rubro_coltv('348'),$this->get_rubro_coltv('349'),$this->get_rubro_coltv('350'),$this->get_rubro_coltv('351'),$this->get_rubro_coltv('352'),$this->get_rubro_coltv('353'),$this->get_rubro_coltv('354'));
            case 105:
                return $this->get_rubro_coltv('355');
            case 106:
                return $this->get_rubro_coltv('399');
            case 107:
                return $this->get_rubro_coltv('400');
            case 360:
                return $this->get_rubro_coltv('398');
            default:
                return 0;
        }
    }

    public function elementos_random_seo($array_base, $array_variaciones, $id_sucursal, $nombre_seo = FALSE){
        if(!is_array($array_base) || empty($array_base)) return NULL;

        $elementos_para_seo = array();
        foreach ($array_base as $k => $col){
            foreach ($col as $j => $elem){
                if(isset($array_variaciones[$id_sucursal][$elem['id_rubro']]) && $array_variaciones[$id_sucursal][$elem['id_rubro']]){
                    if($nombre_seo){
                        $elementos_para_seo[$elem['id_rubro']] = $this->clean_url($elem['rubro']);
                    }else{
                        $elementos_para_seo[$elem['id_rubro']] = $array_variaciones[$id_sucursal][$elem['id_rubro']][rand(0, count($array_variaciones[$id_sucursal][$elem['id_rubro']])-1)];
                    }
                }else{
                    if($nombre_seo){
                        $elementos_para_seo[$elem['id_rubro']] = $this->clean_url($elem['rubro']);
                    }else{
                        $elementos_para_seo[$elem['id_rubro']] = $elem['rubro'];
                    }
                }
            }
        }
        return $elementos_para_seo;
    }

    public function parsear_faqs($faqs, $respuestas, $rangos = NULL){
        if(empty($faqs) || empty($respuestas)) return NULL;

        $array_res = array();

        foreach ($faqs as $k => $value) {
            $array_res[$value['pregunta']]['icono'] = $value['icono'];
            $tmp = array();
            switch ($value['id_tipo']) {
                case '1':
                    $array_res[$value['pregunta']]['values'] = $value['respuesta'];
                    break;
                case '2':
                    $tmp = explode(',', $value['respuesta']);
                    break;
                case '4':
                    if(empty($value['respuesta'])) {
                        $array_res[$value['pregunta']]['values'] = '-';
                    } else {
                        $tmp_rangos = explode(',',$value['respuesta']);

                        if(!empty($tmp_rangos[0])) foreach ($tmp_rangos as $j => $v) {
                            $value['label'] = str_replace('{' . $j . '}', $rangos[$v], $value['label']);
                        }
                        $array_res[$value['pregunta']]['values'] = $value['label'];
                    }

                    break;
                case '5':
                    $array_res[$value['pregunta']]['values'] = isset($respuestas[$value['respuesta']]) && $respuestas[$value['respuesta']] ? $respuestas[$value['respuesta']] : $value['respuesta'];
                    break;
                case '6':
                    $tmp_ci = explode('|', $value['respuesta']);
                    foreach ($tmp_ci as $l => $valor){
                        $tmp_ci_2 = explode(',', $valor);
                        $labels = explode(',', $value['labels']);
                        foreach ($tmp_ci_2 as $u => $dat) {
                            $array_res[$value['pregunta']]['values'][] = isset($respuestas[$dat]) && $respuestas[$dat] ? $respuestas[$dat] : $dat;
                        }
                        $labels = array();
                    }
                    break;
                case '7':
                    $labels = explode(',', $value['labels']);
                    $tmp_si = explode('|', $value['respuesta']);

                    if(!empty($tmp_si[0]) && !empty($tmp_si[1])) $array_res[$value['pregunta']]['values'] = $labels[1] . ' ' . '(' . $rangos[$tmp_si[0]] . ') ' . $tmp_si[1];
                    break;

            }
            if($tmp) foreach ($tmp as $k => $val){
                $array_res[$value['pregunta']]['values'][] = isset($respuestas[$val]) && $respuestas[$val] ? $respuestas[$val] : $val;
            }
        }
        return $array_res;
    }

    public function add_nombre_seo_a_listado($empresas_group = array()){
        $res = array();
        foreach ($empresas_group as $key => $empresas){
            foreach ($empresas as $j => $value) {
                $res[$key][$j] = $value;
                $res[$key][$j]['nombre_seo'] = $this->clean_url($value['rubro']);
            }
        }
        return $res;
    }

    public function reemplazar_nombres_seo($listado = array(), $reemplazos = array()){
        $res = array();
        if($listado) foreach ($listado as $k => $value) {
            $res[$k] = $reemplazos[$k];
        }
        return $res;
    }

    public function rubros_para_seo($rubros = array()){
        $arr = array();
        if(!empty($rubros)) foreach ($rubros as $k => $el) {
            if(!empty($el)) foreach ($el as $j => $val) {
                $arr[$k][$j]['nombre']      = $val;
                $arr[$k][$j]['nombre_seo']  = $this->clean_url($val);
            }
        }
        return $arr;
    }
}