<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Derivador_library{
	protected $CI;

	public function __construct(){
        $this->CI =& get_instance();
        $this->procesos_off = $this->CI->config->item('procesos_off');
    }

    public function get_rubros_derivados($id_rubro_derivador) {
        if (!$id_rubro_derivador) return FALSE;

        $sql = "SELECT d.*, r.rubro, p.proveedor
                , IF(d.id_prov_recibe AND p.id_proveedor IS NULL,0,1) prov_activo
                , IF(r.estado = 2,1,0) rubro_activo
                FROM derivaciones d
                LEFT JOIN prov_rubros r ON r.id = d.id_rubro_recibe
                LEFT JOIN site_proveedores_activos p ON p.id_proveedor = d.id_prov_recibe
                WHERE 1
                AND d.id_estado = 1
                AND d.id_rubro_deriva = " . $this->CI->db->escape($id_rubro_derivador) . "
                HAVING prov_activo = 1 AND rubro_activo = 1";
        
        $query = $this->CI->db->query($sql);
		$ret = $query->result_array();

        return $ret ? $ret : FALSE;
    }

    public function getNombreRubro($id_rubro) {
        if (!$id_rubro) return FALSE;

        $sql = "SELECT rubro FROM prov_rubros WHERE id = %d";
        $sql = sprintf($sql, $this->db->quote($id_rubro, 'integer'));

        $rubro = $this->db->queryOne($sql);
        return $rubro;
    }

    public function get_minisitios_from_rubros($rubros_csv){
        if (!$rubros_csv) return FALSE;

        $sql = "SELECT id_minisitio FROM site_proveedores_activos WHERE id_rubro IN (".$this->CI->db->escape($rubros_csv).")";
        
        $query = $this->CI->db->query($sql);
        $ret = $query->result_array();

        foreach ($ret as $dato){
            $id_minisitio[] = $dato["id_minisitio"];
        }

        return $id_minisitio;
    }

    public function get_minisitios_from_prov($id_prov_csv){
        if (!$id_prov_csv) return FALSE;

        $sql = "SELECT id_minisitio,id_rubro FROM site_proveedores_activos WHERE id_proveedor = ".$this->CI->db->escape($id_prov_csv);
        
        $query = $this->CI->db->query($sql);
        $rs = $query->row_array();

        return $rs;
    }
}