<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procesos_library{
	protected $CI;
	private $procesos_off; // APAGA LOS PROCESOS PARA ETAPA DE DESARROLLO
	public $proceso_status;
	public $prov_con_asociados;
	public $prov_asociados;

	public function __construct(){
        $this->CI =& get_instance();
        $this->procesos_off = $this->CI->config->item('procesos_off');
    }

    public function track($tipo, $padre = NULL, $referencia = NULL, $error = NULL, $db_envios = NULL){
		$log = array(
			'tipo'			=> $tipo,
			'log'			=> $error,
			'url'			=> $_SERVER['HTTP_HOST'],
			'uri'			=> $_SERVER['REQUEST_URI'],
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'navegador'		=> $_SERVER['HTTP_USER_AGENT'],
			'id_user'		=> $this->CI->session->userdata('id_user'),
			'id_padre'	    => $padre,
			'id_referencia' => $referencia,
			'estado'		=> 2
		);

		$res = $db_envios->insert('sys_logs', $log);

		$db_envios->db->close();

		return $log;
	}

	public function procesar_presupuesto($data){
		if(!$this->procesos_off){
			$this->CI->load->helper('common_helper');

			$data['ip'] = $_SERVER['REMOTE_ADDR']; // para levantar la ip del cliente que pide el presupuesto, el dato me sirve para ver si son spammers
			$data['url_referer'] = $_SERVER['HTTP_REFERER']; // Para identificar la url de donde se realizo el presupuesto

			//$randomID = $data['randomID'];// VERIFICAR DUPLICADOS: Cada form genera un ID random.
			unset($data['randomID']);
			if(isset($data['microtime'])) $microtime = (float)$data['microtime']; // PARA MEDIR EL TIEMPO DE LLENADO DEL FORMULARIO.
			unset($data['microtime']);
			unset($data['verificador_captcha']);

			if(substr_count($data['email'], "@") == 0){
				echo '<BR><BR>-- EMAIL NO VALIDA **Control de Spam** --<BR><BR>';//Control de SPAM
				echo '<script>location.href = "'.$_SERVER['HTTP_REFERER'].'"</script>';
				die();
			}

			$data = limpiarArray($data);

			$data['fecha_evento'] = $data['anio_evento'].'-'.$data['mes_evento'].'-'.$data['dia_evento'];
			unset($data['anio_evento']);
			unset($data['mes_evento']);
			unset($data['dia_evento']);

			$data['fecha_nacimiento'] = (!empty($data['anio_nacimiento']))? $data['anio_nacimiento'].'-'.$data['mes_nacimiento'].'-'.$data['dia_nacimiento'] : '0000-00-00';
			unset($data['anio_nacimiento']);
			unset($data['mes_nacimiento']);
			unset($data['dia_nacimiento']);

			$data['grupo'] 		  = md5(microtime(false));
			$data['estado'] 	  = 1;
			$data['aceptado'] 	  = 1;
			$data['id_landing']   = (isset($data['id_landing']) && $data['id_landing'])? $data['id_landing'] : 0;
			$data['id_origen'] 	  = (isset($data['id_origen']) && $data['id_origen'])? $data['id_origen'] : 0;
			$data['id_item'] 	  = (isset($data['id_item']) && $data['id_item'])? $data['id_item'] : 0;
			$data['id_sorteo'] 	  = (isset($data['id_sorteo']) && $data['id_sorteo'])? $data['id_sorteo'] : NUll;
			$data['id_localidad'] = (isset($data['id_localidad']) && $data['id_localidad'])? $data['id_localidad'] : (isset($data['localidad'])?$data['localidad']:'');
			unset($data['localidad']);
			unset($data['subdominio']);

			if(is_array($data['id_minisitio'])){
				$data['id_minisitio'] = implode($data['id_minisitio'],',');
			}
			
			if(is_array($data['id_rubro'])){
				$data['id_rubro'] = implode($data['id_rubro'],',');
			}
			
			if(isset($data['asociados']) && is_array($data['asociados'])){
				$data['asociados'] = implode($data['asociados'],',');
			}

			$db_envios = $this->CI->load->database('envios', TRUE);

			$res = $db_envios->insert('prov_presupuestos_cola', $data);
			$error = $db_envios->error();

			if($error['code']){
				$this->track('T',NULL,NULL,'Procesar Presupuesto Cola: (Microtime: '.$microtime.') <pre>'.print_r($data, true).'</pre>Detalle error: '.$error['message'].' '.$_SERVER['HTTP_REFERER'], $db_envios);
			}
		}else{
			return TRUE;
		}
	}

	public function procesar_nuevo_infonovias_corto($data){
		if(!$this->procesos_off){

			$this->CI->load->helper('common_helper');

			$data = limpiarArray($data);
			$infonovia = array();
			$contacto = array();
			$hist = array();

			$provincias = array(
				1  => 3,  // buenos aires
				2  => 28, // rosario
				3  => 22, // santa fe
				4  => 7,  // cordoba
				5  => 14, // mendoza
				6  => 3, // mar del plata
				7  => 3, // la plata
				8  => 25, // tucuman
				10 => 9,  // entre rios
				11 => 18, // salta
				12 => 15 // misiones
			);

			$localidades = array(
				2 => 2458,
				6 => 2710,
				7 => 2492
			);

			$data['id_provincia'] = 3;
			$data['id_localidad'] = 286;

			if(!empty($provincias[$data['id_sucursal']])){
				$data['id_provincia'] = $provincias[$data['id_sucursal']];
				$data['id_localidad'] = !empty($localidades[$data['id_sucursal']]) ? $localidades[$data['id_sucursal']] : 0;
			}
			
			$sql = "SELECT id FROM sys_usuarios WHERE email = " . $this->CI->db->escape($data['email']);

			$query = $this->CI->db->query($sql);
			$res = $query->row_array();
			$id_user = $res['id'];
			
			if($id_user){
				$infonovia['id_user'] = $id_user;
			}

			$data['fecha_evento'] = date("Y-m-d",strtotime(date('Y-m-d')." + 6 month")); //-- La fecha del evento se pone 6 meses, del día del registro
			$infonovia['fecha_casamiento'] = $data['fecha_evento'];
			$infonovia['email'] = $data['email'];
			$infonovia['estado'] = 1;
			$infonovia['id_provincia'] = $data['id_provincia'];
			$infonovia['id_localidad'] = $data['id_localidad'];
			$infonovia['id_origen'] = (isset($data['id_origen'])&&$data['id_origen'])? $data['id_origen'] : 99;

			$sql = 'SELECT id, fecha_casamiento FROM site_infonovias WHERE email = "'.$data['email'].'"';
			$query = $this->CI->db->query($sql);
			$res = $query->row_array();
			
			//-- Proceso Infonovia --//
			if($res){
				
				if($infonovia['fecha_casamiento'] > $res['fecha_casamiento']){
					unset($infonovia['id_provincia']);
					unset($infonovia['id_localidad']);

					$this->CI->db->set('fecha_casamiento', $infonovia['fecha_casamiento'])
								 ->where('id', $res['id'])
								 ->update('site_infonovias');
				}
				$id_info = $res['id'];
			}else{
				$this->CI->db->insert('site_infonovias', $infonovia);
				$id_info = $this->CI->db->insert_id();
			}

			//--- Proceso contacto ----//
			$contacto['email'] = $data['email'];
			$contacto['estado'] = 1;
			$contacto['fecha_modificacion'] = date('Y-m-d H:i:s');
			if ($id_user){
				$contacto['id_user'] = $id_user;
			}

			$sql = 'SELECT id, fecha_casamiento FROM base_contactos WHERE email = "'.$data['email'].'"';
			$query = $this->CI->db->query($sql);
			$res_base = $query->row_array();

			if($res_base){
				if($data['fecha_evento'] > $res['fecha_casamiento']){
					$contacto['fecha_casamiento'] = $data['fecha_evento'];
					$res = $this->CI->db->set('fecha_casamiento', $data['fecha_evento'])->where('id', $res_base['id'])->update('base_contactos');
				}
				$id_contacto = $res_base['id'];
			}else{
				$contacto['fecha_casamiento'] = $data['fecha_evento'];
				if ($data['id_provincia']) $contacto['id_provincia'] = $data['id_provincia'];
				if ($data['id_localidad']) $contacto['id_localidad'] = $data['id_localidad'];

				$this->CI->db->insert('base_contactos', $contacto);
				$id_contacto = $this->CI->db->insert_id();
			}


			//--- Proceso historia ----//
			$hist['id_contacto'] = $id_contacto;
			$hist['id_sucursal'] = $data['id_sucursal'];
			$hist['id_padre'] = $id_info;
			$hist['id_referencia'] = 20;
			$hist['id_portal'] = 1;
			$hist['id_evento_tipo'] = 1;
			$hist['id_origen'] = 99;
			$hist['fecha_evento'] = $data['fecha_evento'];

			$this->CI->db->insert('base_contactos_historial', $hist);

			
			//--- Proceso suscripción a infonovias ----//
			$estado_novia='1'; //infonovias
			$this->procesar_contacto_suscripcion($hist['id_contacto'], 1, $estado_novia);

			$estado_promo='1'; //promociones
			$this->procesar_contacto_suscripcion($hist['id_contacto'], 3, $estado_promo);
		}
	}

	public function procesar_contacto_suscripcion($id_contacto, $id_suscripcion, $estado){
		$sus = array();
		$sus['id_suscripcion'] = $id_suscripcion;
		$sus['id_contacto'] = $id_contacto;
		$sus['estado'] = $estado;

		$sql = 'SELECT estado FROM base_contactos_suscripciones WHERE id_contacto = '.$sus['id_contacto'].' AND id_suscripcion = '.$sus['id_suscripcion'];

		$query = $this->CI->db->query($sql);
		$res = $query->row_array();
		$est = $res['estado'];

		if($est){
			if($est!=='0' || $estado == '1'){
				$res = $this->CI->db->where('id_contacto', $sus['id_contacto'])
									->where('id_suscripcion', $sus['id_suscripcion'])
									->update('base_contactos_suscripciones', $sus);
			}
		}else{
			$this->CI->db->insert('base_contactos_suscripciones', $sus);
		}
	}

	public function procesar_presupuesto_general($pres){
		if(!$this->procesos_off){
			$db_envios = $this->CI->load->database('envios', TRUE);

			$res = $db_envios->insert('prov_presupuestos_cola_caracteristica', $pres);
			$error = $db_envios->error();

			if($error['code']){
				$this->track('T',NULL,NULL,'Procesar Presupuesto Cola: <pre>'.print_r($res, true).'</pre>Detalle error: '.$error['message'].' '.$_SERVER['HTTP_REFERER'], $db_envios);
			}	
		}else{
			return TRUE;
		}
	}

	public function procesar_pedidos_presupuesto($post) { // PARA DERIVADOR
        if ($post["id_rubro"] || $post["id_proveedor"]) {

        	$this->CI->load->library('derivador_library');

            unset($post["accion"]);

            if(!empty($post["id_rubro"])){
                foreach ($post["id_rubro"] as $id_rubro) {
                    unset($data_form);

                    $data_form = $post;
                    $data_form["id_origen"] = 98;
                    $data_form["id_rubro"] = $id_rubro;
                    $data_form["id_minisitio"] = $this->CI->derivador_library->get_minisitios_from_rubros($id_rubro);
                    unset($data_form["id_proveedor"]);

                    if($data_form['dia_evento']){
                    	$tmp = explode('/', $data_form['dia_evento']);
                    	$data_form['dia_evento'] = $tmp[0];
                    	$data_form['mes_evento'] = $tmp[1];
                    	$data_form['anio_evento'] = $tmp[2];
                    }

                    $this->procesar_presupuesto($data_form);
                }
            }

            if(!empty($post["id_proveedor"])){
                foreach ($post["id_proveedor"] as $id_proveedor) {
                    unset($data_form);

                    $data_form = $post;
                    $rs = $this->CI->derivador_library->get_minisitios_from_prov($id_proveedor);
                    $data_form["id_origen"] = 99;
                    $data_form["id_minisitio"] = $rs["id_minisitio"];
                    $data_form["id_rubro"] = $rs["id_rubro"];
                    unset($data_form["id_proveedor"]);
                    
                    if($data_form['dia_evento']){
                    	$tmp = explode('/', $data_form['dia_evento']);
                    	$data_form['dia_evento'] = $tmp[0];
                    	$data_form['mes_evento'] = $tmp[1];
                    	$data_form['anio_evento'] = $tmp[2];
                    }

                    $this->procesar_presupuesto($data_form);
                }
            }
            $r = TRUE;
        }else{
            $r = FALSE;
        }
        return $r;
    }

    public function procesar_nuevo_proveedor($data){
    	if(!$this->procesos_off){
    		$this->CI->load->helper('common_helper');

    		$this->CI->load->model('rubros_model');

    		$data = limpiarArray($data);

    		$empresa = $proveedor = $usuario = $prov_contacto = $email = $sitio_web = $telefono = $contacto = $interesado = array();

    		if(isset($data["source"]) && $data["source"] == "facebook") $data["comentario"] .= "

			(Origen Facebook)";
			
			//Insertamos la empresa
			$empresa['empresa'] = $data['empresa'];
			$empresa['observaciones'] = $data['comentario'];
			$empresa['estado'] = 2;

			$this->CI->db->insert('prov_empresas', $empresa);
  			$id_empresa = $this->CI->db->insert_id();

  			//Insertamos al proveedor
			$proveedor['id_empresa'] 	= $id_empresa;
			$proveedor['id_rubro'] 		= $data['id_rubro'];
			$proveedor['proveedor'] 	= $data['empresa'];
			$proveedor['observaciones'] = $data['comentario'];

			$this->CI->db->insert('prov_proveedores', $proveedor);
  			$id_proveedor = $this->CI->db->insert_id();

  			//intersado en
			switch ($data['opcion_publicacion']){
				case 'Minisitio (web interna)':
					$interesado['id_producto'] = 101;
					break;
				case 'Infonovias (newsletter)':
					$interesado['id_producto'] = 131;
					break;
				case 'Jornada (expo para novias y proveedores)':
					$interesado['id_producto'] = 198;
					break;
				case 'Banner en rubros':
					$interesado['id_producto'] = 111;
					break;
				case 'Banner en Home':
					$interesado['id_producto'] = 139;
					break;
				case 'Otro':
					$interesado['id_producto'] = NULL;
					break;
			}

			$interesado['id_proveedor'] = $id_proveedor;
			$interesado['id_portal'] = 1;
			$interesado['id_evento_tipo'] = 1;

			$res = $this->CI->db->insert('prov_productos_interesado', $interesado);
			
			//Insertamos al usuario
			//buscando un user unico
			$data['empresa'] = str_replace(' ','',$data['empresa']);
			$i = 1;
			$j = TRUE;
			while ($j !== FALSE) {
				$i += 1;
				$sql = 'SELECT id FROM sys_usuarios WHERE user = \''.$data['empresa'].$i.'\'';
				$query = $this->CI->db->query($sql);

				if(!$query->num_rows){
					$j = FALSE;					
				}
			}

			$usuario['user'] = $data['empresa'] . $i;

			//verificando que el email no este, si esta se usa el padre
			$sql = "SELECT id FROM sys_usuarios WHERE email = " . $this->CI->db->escape($data['email']);
			$query = $this->CI->db->query($sql);
			$row = $query->row_array();
			$padre = NULL;
			if(isset($row['id']) && $row['id']) $padre = $row['id'];
			if($padre){
				$usuario['padre'] = $padre;
			}else{
				$usuario['email'] = $data['email'];
			}
			$usuario['id_perfil'] 	= 1;
			$usuario['id_sucursal'] = $data['id_sucursal'];
			$usuario['nombre'] 		= $data['nombre'];
			$usuario['apellido'] 	= $data['apellido'];

			$this->CI->db->insert('sys_usuarios', $usuario);
  			$id_user = $this->CI->db->insert_id();

  			//Insertamos el contacto al proveedor
			$prov_contacto['id_proveedor'] = $id_proveedor;
			$prov_contacto['id_user'] = $id_user;

  			$this->CI->db->insert('prov_contactos', $prov_contacto);
  			$id_contacto = $this->CI->db->insert_id();

  			//Insertamos el email al proveedor
			$email['email'] 		= $data['email'];
			$email['id_email_tipo'] = 2;
			$email['estado'] 		= 2;
			$email['id_padre'] 		= $id_proveedor;
			$email['id_referencia'] = 1;

			$res = $this->CI->db->insert('sys_emails', $email);

			//Insertamos el email al contacto/usuario
			$email['id_padre'] = $id_user;
			$email['id_referencia'] = 2;
			$res = $this->CI->db->insert('sys_emails', $email);

			//Insertamos el sitio web, si tiene, al proveedor y al contacto/usuario 
			if (!empty($data['sitio_web'])){
				//proveedor
				$sitio_web['url'] = trim($data['sitio_web']);
				$sitio_web['estado'] = 2;
				$sitio_web['id_padre'] = $id_proveedor;
				$sitio_web['id_referencia'] = 1;

				$res = $this->CI->db->insert('sys_urls', $sitio_web);

				//contacto/usuario
				$sitio_web['id_padre'] = $id_user;
				$sitio_web['id_referencia'] = 2;

				$res = $this->CI->db->insert('sys_urls', $sitio_web);
			}

			//Insertamos los telefonos al proveedor
			$telefono['telefono'] 			= $data['telefono'];
			$telefono['orden'] 				= 1;
			$telefono['estado'] 			= 2;
			$telefono['id_telefono_tipo'] 	= 2; //trabajo
			$telefono['id_padre'] 			= $id_proveedor;
			$telefono['id_referencia'] 		= 1;

			$res = $this->CI->db->insert('sys_telefonos', $telefono);

			if(!empty($data['celular'])){
				$telefono['telefono'] = $data['celular'];
				$telefono['orden'] = 2;
				$telefono['id_telefono_tipo'] = 4; //celular

				$this->CI->db->insert('sys_telefonos', $telefono);
			}

			//Insertamos los telefonos al contacto/usuario
			$telefono['telefono'] 			= $data['telefono'];
			$telefono['orden'] 				= 1;
			$telefono['estado'] 			= 2;
			$telefono['id_telefono_tipo'] 	= 2; //trabajo
			$telefono['id_padre'] 			= $id_user;
			$telefono['id_referencia'] 		= 2;

			$res = $this->CI->db->insert('sys_telefonos', $telefono);

			if(!empty($data['celular'])){
				$telefono['telefono'] = $data['celular'];
				$telefono['orden'] = 2;
				$telefono['id_telefono_tipo'] = 4; //celular

				$this->CI->db->insert('sys_telefonos', $telefono);
			}

			//Direccion
			$dir['calle'] 			= 'sin calle';
			$dir['id_localidad']	= $data['id_localidad'];
			$dir['id_padre'] 		= $id_proveedor;
			$dir['id_referencia'] 	= 1;

			$res = $this->CI->db->insert('sys_direcciones', $dir);

			$dir['id_padre'] = $id_user;
			$dir['id_referencia'] = 2;
			$res = $this->CI->db->insert('sys_direcciones', $dir);

			//insertamos el contacto para la base de newsletters
			$contacto['nombre']   		= $data['nombre'];
			$contacto['apellido'] 		= $data['apellido'];
			$contacto['email'] 			= $data['email'];
			$contacto['id_provincia'] 	= $data['id_provincia'];
			$contacto['id_localidad'] 	= $data['id_localidad'];
			$contacto['id_pais'] 		= isset($data['id_pais'])&&$data['id_pais'] ? $data['id_pais'] : '';
			$contacto['telefono'] 		= $data['telefono'] . (isset($data['celular']) && $data['celular'] ? (' / ' . $data['celular']) : '');
			$contacto['comentario'] 	= $data['comentario'];
			$contacto['id_user'] 		= $id_user;
			$contacto['id_proveedor'] 	= $id_proveedor;
			$contacto['id_rubro'] 		= $data['id_rubro'];
			$contacto['id_portal'] 		= 1;
			$contacto['id_sucursal'] 	= $data['id_sucursal'];
			$contacto['infonovias'] 	= (!empty($data['infonovias'])) ? 1 : 0;
			
			$this->procesar_contacto_proveedor($contacto);

			//Envio de mail de aviso de nuevo proveedor
			$rubro = $this->CI->rubros_model->get_rubro($data['id_rubro']);
			$rubro = $rubro[0];
			$localidad = $this->CI->rubros_model->get_localidad($data['id_localidad']);

			//------ENVÍO DE NUEVO PROVEEDOR
			$message = '
			<html>
				<head>
					<title>Casamientos Online - Nuevo Cliente</title>
				</head>
				<body style="background:#eeeeee;">
					<table style="background:#ffffff;font-family:Arial;border:1px solid #cccccc;" width="600" cellpadding="0" cellspacing="0" border="0" id="hoja_blanca" align="center">
						<tr>
							<td>
								<p style="margin-top:20px;margin-right:20px;margin-left:20px;color:#333;font-size:10px;border-bottom:1px solid #ccc;">FORMULARIO DE CONTACTO DE PROVEEDORES</p>
								<p style="color:#B69;font-size:36px;margin-top:20px;margin-right:20px;margin-left:20px;">Nuevo Cliente</p>
								<p style="color:#666;margin-top:20px;margin-right:20px;margin-left:20px;">Empresa: <strong>' . $data['empresa'] . '</strong><br />
								Sucursal: <strong>' . $rubro['sucursal'] . '</strong><br />
								Rubro: <strong>' . $rubro['rubro'] . '</strong><br />
								Producto Interesado: <strong>' . $data['opcion_publicacion'] . '</strong><br />
								Contacto: <strong>' . $data['nombre'] . ' ' . $data['apellido'] . '</strong><br />
								Email: <strong>' . $data['email'] . '</strong><br />
								Telefono: <strong>' . $data['telefono'] . '</strong><br />
								Celular: <strong>' . (isset($data['celular']) && $data['celular'] ? $data['celular'] : '') . '</strong><br />
								Provincia: <strong>' . $localidad['provincia'] . '</strong><br />
								Localidad: <strong>' . $localidad['localidad'] . '</strong><br />
								Sitio Web: <strong>' . (isset($data['sitio_web']) && $data['sitio_web'] ? $data['sitio_web'] : '') . '</strong><br />
								Desea recibir Infonovias: <strong>' . (($data['infonovias'])? 'SI' : 'NO') . '</strong><br /><br />
								Comentario:</p>
								<p style="color:#666;margin:20px;">' . $data['comentario'] . '</p>
							</td>
						</tr>
					</table><!-- termina #hoja_blanca -->
				</body>
			</html>';

			if($rubro['sucursal'] == 'Córdoba'){
				$suc = 'cordoba';
			}else{
				$suc = $rubro['sucursal'];
			}

			$to_mail = 'anunciante.nuevo.'.strtolower(str_replace(' ','',$suc)).'@casamientosonline.com';

			#@@# Enviar el mail de aviso de nuevo proveedor
			$news = array();
			$news['id_tipo'] 	= 12;
			$news['remitente'] 	= $data['nombre'] . ' ' . $data['apellido'];
			$news['email'] 		= $data['email'];
			$news['asunto'] 	= 'Nueva Empresa para publicar en ' . $rubro['sucursal'];
			$news['mensaje'] 	= $message;
			$news['newsletter'] = 'Nueva Empresa para publicar en ' . $rubro['sucursal'];

			#emailer_newsletters - info a enviar
			$res_news = $this->CI->db->insert('emailer_newsletters', $news);

			$env = array();
			$env['id_newsletter'] = $this->CI->db->insert_id();
			$env['id_proveedor'] = $id_proveedor;
			$env['destinatario'] = 'Anunciante nuevo ' . $rubro['sucursal'];
			$env['email'] = $to_mail;
			#prov_mails_envios - destinatarios
			$res_env = $this->CI->db->insert('prov_mails_envios', $env);
    	}
	}

	public function procesar_contacto_proveedor($data){
		//inserta el historial
		$hist['id_contacto'] = $this->procesar_contacto($data);

		$hist['id_proveedor'] 	= $data['id_proveedor'];
		$hist['id_rubro'] 		= $data['id_rubro'];
		$hist['id_sucursal'] 	= $data['id_sucursal'];
		$hist['id_referencia'] 	= 1;
		$hist['id_portal'] 		= $data['id_portal'];
		$hist['id_evento_tipo'] = 1;
		$hist['id_padre'] 		= $data['id_proveedor'];
		$hist['id_origen'] 		= 60;

		$res_env = $this->CI->db->insert('base_contactos_historial', $hist);

		//inserta o modifica las suscripciones segun el formulario
		//id 1 infonovias
		if($data['infonovias']){
			$estado_novia = '1';
		}else{
			$estado_novia = '-1';
		}
		$this->procesar_contacto_suscripcion($hist['id_contacto'], 1, $estado_novia);
	}

	public function procesar_contacto($data){
		/*
		Presupuesto:
		id_minisitio - id_user - id_referencia - id_localidad - id_origen - id_item - id_sorteo - id_landing - id_tipo_evento - aceptado - estado -
		fecha_alta - fecha_evento - fecha_nacimiento - nombre - apellido - email - telefono - invitados - ubicacion - dni - grupo - comentario

		Contacto:
		id_user - nombre - apellido - email - estado - fecha_alta - fecha_modificacion - fecha_casamiento - fecha_nacimiento - invitados - id_provincia -
		id_localidad - telefono - salon - comentario

		Proveedor:
		id_user - nombre - apellido - email - estado - id_provincia - id_localidad - telefono/celular - comentario

		Infonovia:
		id_user - fecha_evento - invitados - nombre - apellido - email - estado - id_provincia - id_localidad - telefono - comentario
		*/
		$info = array();

		$info['nombre']   = $data['nombre'];
		$info['apellido'] = $data['apellido'];
		$info['email'] 	  = $data['email'];

		if(isset($data['fecha_evento_variable']) && $data['fecha_evento_variable']){
			$info['fecha_evento_variable'] = $data['fecha_evento_variable'];
		}
		$info['estado'] = 1;
		$info['fecha_modificacion'] = date('Y-m-d H:i:s');

		//opcionales segun el formulario
		if (isset($data['id_provincia']) && $data['id_provincia']){
			$info['id_provincia'] = $data['id_provincia'];
		}
		if (isset($data['id_localidad']) && $data['id_localidad']){
			$info['id_localidad'] = $data['id_localidad'];
		}
		if (isset($data['id_pais']) && $data['id_pais']){
			$info['id_pais'] = $data['id_pais'];
		}
		if (isset($data['telefono']) && $data['telefono']){
			$info['telefono'] = $data['telefono'];
		}
		if (isset($data['comentario']) && $data['comentario']){
			$info['comentario'] = $data['comentario'];
		}
		if (isset($data['id_user']) && $data['id_user']){
			$info['id_user'] = $data['id_user'];
		}
		if (isset($data['fecha_evento']) && $data['fecha_evento']){
			$info['fecha_casamiento'] = $data['fecha_evento'];
		}
		if(!isset($info['fecha_casamiento']) || !$info['fecha_casamiento']){
			if(isset($data['anio_evento']) && isset($data['mes_evento']) && isset($data['dia_evento'])){
				$info['fecha_casamiento'] = $data['anio_evento'] . "-" . $data['mes_evento'] . "-" . $data['dia_evento'];
			}
		}

		if (isset($data['invitados']) && $data['invitados']){
			$info['invitados'] = $data['invitados'];
		}
		if (isset($data['ubicacion']) && $data['ubicacion']){
			$info['salon'] = $data['ubicacion'];
		}
		if (isset($data['fecha_nacimiento']) && $data['fecha_nacimiento']){
			$info['fecha_nacimiento'] = $data['fecha_nacimiento'];
		}

		$sql = 'SELECT id FROM base_contactos WHERE email = ' . $this->CI->db->escape($info['email']);
		$query = $this->CI->db->query($sql);
		$row = $query->row_array();
		if(isset($row['id']) && $row['id']) $id_contacto = $row['id'];

		//existe actualizo los datos
		if (isset($id_contacto) && $id_contacto){
			if(isset($info['fecha_casamiento']) && ($info['fecha_casamiento'] == '0000-00-00' || $info['fecha_casamiento'] == "--")){
				unset($info['fecha_casamiento']);
			}

			$this->CI->db->where('id', $id_contacto)
						 ->update('base_contactos', $info);
		}else{ //crea uno nuevo
			$info['id_portal'] = isset($data['id_portal']) && $data['id_portal'] ? $data['id_portal'] : 1;

			$this->CI->db->insert('base_contactos', $info);
  			$id_contacto = $this->CI->db->insert_id();
		}

		return $id_contacto;
	}

	public function procesar_sugerencia($data){
		if(!$this->procesos_off){
    		$this->CI->load->helper('common_helper');

    		$this->CI->load->model('rubros_model');

    		$data = limpiarArray($data);

			$sugerencia = $contacto = array();

			$sql = "SELECT id FROM sys_usuarios WHERE email = " . $this->CI->db->escape($data['email']);		

			$query = $this->CI->db->query($sql);
			$res = $query->row_array();
			$id_user = $res['id'];
			
			if($id_user){
				$sugerencia['id_user'] = $id_user;
			}

			$sugerencia['nombre'] = $data['nombre'];
			$sugerencia['apellido'] = $data['apellido'];
			$sugerencia['email'] = $data['email'];
			$sugerencia['id_provincia'] = $data['id_provincia'];
			$sugerencia['id_localidad'] = $data['id_localidad'];
			$sugerencia['telefono'] = $data['telefono'];
			$sugerencia['movil'] = $data['celular'];
			$sugerencia['sugerencia'] = $data['comentario'];
			
			$this->CI->db->insert('site_sugerencias', $sugerencia);
  			$id_sug = $this->CI->db->insert_id();

  			//insertamos el contacto para la base de newsletters
			$contacto['nombre'] 		= $data['nombre'];
			$contacto['apellido'] 		= $data['apellido'];
			$contacto['email'] 			= $data['email'];
			$contacto['id_provincia'] 	= $data['id_provincia'];
			$contacto['id_localidad'] 	= $data['id_localidad'];
			$contacto['telefono'] 		= $data['telefono'].' / '.$data['celular'];
			$contacto['comentario'] 	= $data['comentario'];
			if ($id_user){
				$contacto['id_user'] 	= $id_user;
			}
			$contacto['id_sucursal'] = $this->CI->session->userdata('id_sucursal');
			$contacto['infonovias'] = (!empty($data['infonovias']))? 1 : 0;
			$contacto['id_sug'] = $id_sug;

			$this->procesar_contacto_sugerencia($contacto);
			
			//Envio de mail de aviso de nueva sugerencia
			$localidad = $this->CI->rubros_model->get_localidad($data['id_localidad']);

			// ------ ENVÍO DE COMENTARIO O SUGERENCIA
			$message = '
			<html>
				<head>
					<title>Casamientos Online - Nueva sugerencia o comentario</title>
				</head>
				<body style="background:#eeeeee;">
					<table style="background:#ffffff;font-family:Arial;border:1px solid #cccccc;" width="600" cellpadding="0" cellspacing="0" border="0" id="hoja_blanca" align="center">
						<tr>
							<td>
								<p style="margin-top:20px;margin-right:20px;margin-left:20px;color:#333;font-size:10px;border-bottom:1px solid #ccc;">FORMULARIO DE CONTACTO</p>
								<p style="color:#B69;font-size:36px;margin-top:20px;margin-right:20px;margin-left:20px;">Nuevo comentario o sugerencia</p>
								<p style="color:#666;margin-top:20px;margin-right:20px;margin-left:20px;">
								Contacto: <strong>' . $data['nombre'] . ' ' . $data['apellido'] . '</strong><br />
								Email: <strong>' . $data['email'] . '</strong><br />
								Telefono: <strong>' . $data['telefono'] . '</strong><br />
								Celular: <strong>' . $data['celular'] . '</strong><br />
								Provincia: <strong>' . $localidad['provincia'] . '</strong><br />
								Localidad: <strong>' . $localidad['localidad'] . '</strong><br />
								Desea recibir Infonovias: <strong>' . (($data['infonovias'])? 'SI' : 'NO') . '</strong><br /><br />
								Comentario:</p>
								<p style="color:#666;margin-top:20px;margin:20px;">' . $data['comentario'] . '</p>';

				if(isset($data['desde_contacto']) && $data['desde_contacto']) $message .= '<p style="color:#666;margin-top:20px;margin:20px;font-weight: bold;">Email enviado desde el formulario de contacto</p>';

				$message .='</td>
						</tr>
					</table><!-- termina #hoja_blanca -->
				</body>
			</html>';

			#@@# Enviar el mail de aviso de nuevo proveedor
			$titulo = 'Nueva sugerencia o comentario';
			if(isset($data['desde_contacto']) && $data['desde_contacto']) $titulo = 'Formulario de contacto';
			$news = array();
			$news['id_tipo']   	= 13;
			$news['remitente'] 	= $data['nombre'] . ' ' . $data['apellido'];
			$news['email'] 		= $data['email'];
			$news['asunto'] 	= $titulo;
			$news['mensaje'] 	= $message;
			$news['newsletter'] = $titulo;
			
			#emailer_newsletters - info a enviar
			$res_news = $this->CI->db->insert('emailer_newsletters', $news);
			$id_emailer = $this->CI->db->insert_id();
			
			$env = array();
			$env['id_newsletter'] 	= $id_emailer;
			$env['id_sugerencia'] 	= $id_sug;
			$env['destinatario'] 	= 'Info | Casamientos Online';
			$env['email'] 			= 'info@casamientosonline.com';
			// $env['email'] 			= 'pablo.fumarola@gmail.com';
			
			#prov_mails_envios - destinatarios
			$res = $this->CI->db->insert('site_sugerencias_envios', $env);
    	}
	}

	public function procesar_contacto_sugerencia($data){
		//inserta el historial
		$hist['id_contacto'] = $this->procesar_contacto($data);
		$hist['id_sucursal'] = $data['id_sucursal'];
		$hist['id_padre'] = $data['id_sug'];
		$hist['id_referencia'] = 19;
		$hist['id_portal'] = 1;
		$hist['id_evento_tipo'] = 1;
		$hist['id_origen'] = 9;

		$this->CI->db->insert('base_contactos_historial', $hist);

		//inserta o modifica las suscripciones segun el formulario
		//id 1 infonovias
		if (isset($data['infonovias']) && $data['infonovias']){
			$estado_novia = '1';
		}else{
			$estado_novia = '-1';
		}

		$this->procesar_contacto_suscripcion($hist['id_contacto'], 1, $estado_novia);

		//id 3 promociones
		if (isset($data['promociones']) && $data['promociones']){
			$estado_promo='1';
		}else{
			$estado_promo='-1';
		}
		$this->procesar_contacto_suscripcion($hist['id_contacto'], 3, $estado_promo);
	}

	public function procesar_form_novio($data){
		if(!$this->procesos_off){
			$this->CI->load->helper('common_helper');

			if(substr_count($data['email'], "@") == 0){
				echo '<BR><BR>-- EMAIL NO VALIDA **Control de Spam** --<BR><BR>'; //Control de SPAM
				echo '<script>location.href = "'.$_SERVER['HTTP_REFERER'].'"</script>';
				die();
			}
			
			unset($data['tipo']);
			$temp_data = $data = limpiarArray($data);

			$dir = array();

			$tmp = explode('/', $data['dia_evento']);

			$temp_data['fecha_evento'] = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
			if(!$temp_data["id_origen"]) $temp_data["id_origen"] = 0;

			unset($temp_data['anio_evento']);
			unset($temp_data['mes_evento']);
			unset($temp_data['dia_evento']);
			unset($temp_data['id_provincia']);
			unset($temp_data['id_localidad']);
			unset($temp_data['id_referencia']);
			unset($temp_data['id_origen_contacto']);
			unset($temp_data['infonovias']);
			unset($temp_data['promociones']);

			$data['id_user'] = $temp_data['id_user'] = $this->get_usuario_id($data['email']);

			$this->CI->db->insert('evn_novios', $temp_data);
			$dir['id_padre'] = $data['id'] = $this->CI->db->insert_id();
			$dir['id_localidad'] = $data['id_localidad'];
			$dir['id_referencia'] = $data['id_referencia'];

			$this->CI->db->insert('sys_direcciones', $dir);

			//piso el origen para el contacto
			$data['id_origen'] = $data['id_origen_contacto'];
			$data['id_tipo_evento'] = 1;
			$data['id_portal'] = 1;
			$data['id_referencia'] = $data['id_referencia'];
			$data['id_sucursal'] = 1;
			$data['id_rubro'] = NULL;

			$this->procesar_contacto_evento($data);
		}
	}

	public function procesar_form_proveedor($data){
		if(!$this->procesos_off){
			$this->CI->load->helper('common_helper');
			if(substr_count($data['email'], "@") == 0){
				echo '<BR><BR>-- EMAIL NO VALIDA **Control de Spam** --<BR><BR>';//Control de SPAM
				echo '<script>location.href = "'.$_SERVER['HTTP_REFERER'].'"</script>';
				die();
			}

			unset($data['tipo']);

			$temp_data = $data = limpiarArray($data);

			unset($temp_data['id_referencia']);
			unset($temp_data['id_origen_contacto']);
			unset($temp_data['infonovias']);
			unset($temp_data['promociones']);
			unset($temp_data['dia_evento']);
			unset($temp_data['dni']);
			unset($temp_data['id_provincia']);
			unset($temp_data['id_localidad']);

			$this->CI->db->insert('evn_proveedores', $temp_data);
			$data['id'] = $this->CI->db->insert_id();

			//piso el origen para el contacto
			$data['id_origen'] = $data['id_origen_contacto'];
			$data['id_tipo_evento'] = 1;
			$data['id_portal'] = 1;
			$data['id_referencia'] = $data['id_referencia'];
			$data['id_sucursal'] = 1;

			$this->procesar_contacto_evento($data);
			return $data['id'];
		}
	}

	public function procesar_contacto_evento($data){
		//inserta el historial
		$hist['id_contacto'] = $this->procesar_contacto($data);
		if(isset($data['id_rubro']) && $data['id_rubro']) $hist['id_rubro'] = $data['id_rubro'];
		$hist['id_sucursal'] = $data['id_sucursal'];
		$hist['id_referencia'] = $data['id_referencia'];
		$hist['id_portal'] = $data['id_portal'];
		$hist['id_evento_tipo'] = $data['id_tipo_evento'];
		$hist['id_padre'] = $data['id'];
		$hist['id_origen'] = $data['id_origen'];

		$this->CI->db->insert('base_contactos_historial', $hist);

		//inserta o modifica las suscripciones segun el presupuesto
		//id 1 infonovias
		if ($data['infonovias']){
			$estado_novia = '1';
		}else{
			$estado_novia = '-1';
		}
		$this->procesar_contacto_suscripcion($hist['id_contacto'], 1, $estado_novia);

		//id 3 promociones
		if (isset($data['promociones']) && $data['promociones']){
			$estado_promo = '1';
		}else{ 
			$estado_promo = '-1';
		}
		$this->procesar_contacto_suscripcion($hist['id_contacto'], 3, $estado_promo);
	}

	public function procesar_nuevo_infonovias($data){
		if(!$this->procesos_off){
			$this->CI->load->helper('common_helper');

			$data = limpiarArray($data);

			$infonovia = $contacto = array();

			$sql = "SELECT id FROM sys_usuarios WHERE email = " . $this->CI->db->escape($data['email']);

			$query = $this->CI->db->query($sql);
			$res = $query->row_array();
			$id_user = $res['id'];
			
			if($id_user){
				$infonovia['id_user'] = $id_user;
			}

			$data['fecha_evento'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['dia_evento'])));

			$infonovia['nombre'] = $data['nombre'];
			$infonovia['apellido'] = $data['apellido'];
			$infonovia['email'] = $data['email'];
			$infonovia['telefono'] = $data['telefono'];
			$infonovia['invitados'] = $data['invitados'];
			$infonovia['estado'] = 1;
			$infonovia['id_provincia'] = $data['id_provincia'];
			$infonovia['id_localidad'] = $data['id_localidad'];
			$infonovia['id_origen'] = (!empty($data['id_origen'])) ? $data['id_origen'] : 4;
			$infonovia['fecha_casamiento'] = $data['fecha_evento'];
			$infonovia['comentario'] = '';


			$sql = "SELECT id FROM site_infonovias WHERE email = " . $this->CI->db->escape($data['email']);

			$query = $this->CI->db->query($sql);
			$res = $query->row_array();
			$id_info = $res['id'];

			if ($id_info){
				$this->CI->db->where('id', $id_info)->update('site_infonovias', $infonovia);
			} else {
				$this->CI->db->insert('site_infonovias', $infonovia);
				$id_info = $this->CI->db->insert_id();
			}

			//insertamos el contacto para la base de newsletters
			$contacto['nombre'] = $data['nombre'];
			$contacto['apellido'] = $data['apellido'];
			$contacto['email'] = $data['email'];
			$contacto['id_provincia'] = $data['id_provincia'];
			$contacto['id_localidad'] = $data['id_localidad'];
			$contacto['telefono'] = $data['telefono'];
			$contacto['comentario'] = '';
			if ($id_user){
				$contacto['id_user'] = $id_user;
			}

			$contacto['id_sucursal'] = $data['id_sucursal'];
			$contacto['infonovias'] = (!empty($data['infonovias'])) ? 1 : 0;
			$contacto['promociones'] = (!empty($data['promociones'])) ? 1 : 0;
			if($infonovia['id_origen'] == 4){
				$contacto['infonovias'] = 1;
				$contacto['promociones'] = 1;
			}
			$contacto['id_info'] = $id_info;
			$contacto['fecha_evento'] = $data['fecha_evento'];

			$this->procesar_contacto_infonovia($contacto);
		}
	}

	public function procesar_contacto_infonovia($data){
		//inserta el historial
		$hist['id_contacto'] = $this->procesar_contacto($data);

		$hist['id_sucursal'] = $data['id_sucursal'];
		$hist['id_padre'] = $data['id_info'];
		$hist['id_referencia'] = 20;
		$hist['id_portal'] = 1;
		$hist['id_evento_tipo'] = 1;
		$hist['id_origen'] =  4;
		$hist['fecha_evento'] = $data['fecha_evento'];
		
		$this->CI->db->insert('base_contactos_historial', $hist);

		//inserta o modifica las suscripciones segun el formulario
		//id 1 infonovias
		if ($data['infonovias']){
			$estado_novia = '1';
		}else{
			$estado_novia = '-1';
		}
		$this->procesar_contacto_suscripcion($hist['id_contacto'], 1, $estado_novia);

		//id 3 promociones
		if ($data['promociones']){
			$estado_promo = '1';
		}else{
			$estado_promo = '-1';
		}
		$this->procesar_contacto_suscripcion($hist['id_contacto'], 3, $estado_promo);
	}

	public function procesar_presupuesto_sistemas_vencidos($data){
		unset($data['asociados']);
		$data['id_user'] = $this->get_usuario_id($data['email']);

		if(!empty($data['id_user'])){
			$data['id_referencia'] = 4;
		}
		
		// Presupuesto para estadisticas del rubro
		$this->insertar_presupuesto_rubro($data['id_rubro'], $data['id_origen'], $data['id_landing']);

		// Presupuesto para estadisticas del proveedor
		$prov = $this->get_proveedor_info($data['id_minisitio']);
		$this->insertar_presupuesto_proveedor($prov['id_proveedor'], $data['id_origen'], $data['id_landing']);
		
		$temp_data = $contacto = $data;

		unset($temp_data['id_rubro']);
		unset($temp_data['id_provincia']);
		unset($temp_data['id_pais']);
		unset($temp_data['id_portal']);
		unset($temp_data['ip']);
		unset($temp_data['infonovias']);
		unset($temp_data['promociones']);
		unset($temp_data['espacio_news']);

		if((int) $temp_data['id_item'] > 0){ // INDICA EN EL COMENTARIO TIPO (producto, promo, foto, ect) Y TITULO
			$temp_data['comentario'] = $this->get_item_presupuesto($temp_data['id_item'], $temp_data['id_origen']).'.  '.$temp_data['comentario'];
		}

		$this->CI->db->insert('prov_presupuestos', $temp_data);

		$contacto['id_pres'] = $temp_data['id_pres'] = $this->CI->db->insert_id();
		$contacto['id_portal'] = $data['id_portal'];
		$contacto['id_referencia'] = 16;
		$contacto['id_tipo_evento']= $data['id_tipo_evento'];
		
		$this->procesar_contacto_presupuesto($contacto);
	}

	public function get_usuario_id($email){
		$query = $this->CI->db->query('SELECT id FROM sys_usuarios WHERE email = ' . $this->CI->db->escape($email) . ' LIMIT 1');
		$res = $query->row_array();
		return $res['id'];
	}

	public function get_contacto_id($email){
		$query = $this->CI->db->query('SELECT id FROM base_contactos WHERE email = ' . $this->CI->db->escape($email) . ' LIMIT 1');
		$res = $query->row_array();
		return $res['id'];
	}

	public function insertar_presupuesto_rubro($id_rubro, $id_origen, $landing = 0){
		$sql1 = 'SELECT id FROM prov_presupuestos_rubros WHERE id_rubro = ' . $this->CI->db->escape($id_rubro) . ' AND id_origen = ' . $this->CI->db->escape($id_origen) . ' AND landing = ' . $this->CI->db->escape($landing) . ' AND mes = MONTH(NOW()) AND ano = YEAR(NOW())';
		$query = $this->CI->db->query($sql1);
		$res = $query->row();
		$id = NULL;
		if(!empty($res->id)) $id = $res->id;

		if ($id){
			$sql = 'UPDATE prov_presupuestos_rubros SET total = (total + 1) WHERE id = ' . $this->CI->db->escape($id);
		}else{
			$sql = 'INSERT INTO prov_presupuestos_rubros (id_rubro, mes, ano, total, id_origen, landing)
					VALUES(' . $id_rubro . ', MONTH(NOW()), YEAR(NOW()), 1, ' . $id_origen . ', ' . $landing . ')';
		}
		$res = $this->CI->db->query($sql);
		return $res;
	}

	public function get_proveedor_info($id_ms){
		$sql = "SELECT m.id_proveedor, m.id_rubro, r.rubro, r.id_sucursal, s.sucursal, m.proveedor, m.autorespuesta, m.autorespuesta_activa, IFNULL(CONCAT(dir.calle,IFNULL(CONCAT(' ',dir.numero),''),IFNULL(CONCAT(' ',dir.departamento),''),IFNULL(CONCAT(' - ',loc.localidad),''),IFNULL(CONCAT(' ',pv.provincia),''),IFNULL(CONCAT(' - ',pai.pais),'')),'') direccion, IFNULL(GROUP_CONCAT(DISTINCT tel.telefono ORDER BY tel.orden SEPARATOR ' / '),'') telefonos, em.email, p.logo
				FROM prov_minisitios m
				INNER JOIN prov_proveedores p ON p.id = m.id_proveedor
				LEFT JOIN prov_rubros r ON m.id_rubro = r.id
				LEFT JOIN sys_sucursales s ON r.id_sucursal = s.id
				LEFT JOIN sys_direcciones dir ON m.id = dir.id_padre AND dir.id_referencia = 5 AND dir.estado > 0 AND dir.orden = 1
				LEFT JOIN sys_localidades loc ON dir.id_localidad = loc.id
				LEFT JOIN sys_provincias pv ON loc.id_provincia = pv.id
				LEFT JOIN sys_paises pai ON pv.id_pais = pai.id
				LEFT JOIN sys_telefonos tel ON m.id = tel.id_padre AND tel.id_referencia = 5 AND tel.estado > 0
				LEFT JOIN sys_emails em ON m.id = em.id_padre AND em.id_referencia = 5 AND em.estado > 0 AND em.orden = 1
				WHERE m.id = " . $this->CI->db->escape($id_ms) . "
				LIMIT 1";
		$query = $this->CI->db->query($sql);
		$res = $query->row_array();
		return $res;
	}

	public function insertar_presupuesto_proveedor($id_proveedor, $id_origen = 0, $landing = 0){
		$sql1 = 'SELECT id FROM prov_presupuestos_proveedores WHERE id_proveedor = ' . $this->CI->db->escape($id_proveedor) . ' AND id_origen = ' . $this->CI->db->escape($id_origen) . ' AND landing = ' . $this->CI->db->escape($landing) . ' AND mes = MONTH(NOW()) AND ano = YEAR(NOW())';
		
		$query = $this->CI->db->query($sql1);
		$res = $query->row();
		$id = NULL;
		if(!empty($res->id)) $id = $res->id;

		if ($id){
			$sql = 'UPDATE prov_presupuestos_proveedores SET total = (total + 1) WHERE id = ' . $this->CI->db->escape($id);
		}else{
			$sql = 'INSERT INTO prov_presupuestos_proveedores (id_proveedor, mes, ano, total, id_origen, aceptado, landing)
					VALUES(' . $this->CI->db->escape($id_proveedor) . ', MONTH(NOW()), YEAR(NOW()), 1, ' . $this->CI->db->escape($id_origen) . ', 1, ' . $this->CI->db->escape($landing) . ')';
		}
		$res = $this->CI->db->query($sql);
		return $res;
	}

	function get_item_presupuesto($item, $id_origen){
		$paq_array = array(35,40,32,14);
		$foto_array = array(23);
		$video_array = array(30);

		if(in_array($id_origen,$paq_array)){//-- PAQUETE --//
			
			$sql = "SELECT paquete as titulo, 'el Paquete' as tipo
					FROM paq_paquetes
					WHERE id = " . $this->CI->db->escape($item);

		}elseif(in_array($id_origen,$foto_array)){//-- FOTO --//
			
			$sql = "SELECT titulo, 'la Foto' as tipo
					FROM sys_medias
					WHERE id = " . $this->CI->db->escape($item);

		}elseif(in_array($id_origen,$video_array)){//-- VIDEO --//
			
			$sql = "SELECT titulo, 'el Video' as tipo
					FROM sys_medias
					WHERE id = " . $this->CI->db->escape($item);

		}else{ //-- PRODUCTOS Y PROMOS --//
			$sql = "SELECT spa.titulo
					, CASE
					WHEN '".$id_origen."' IN (33,36,38,4) THEN 'el Producto'
					WHEN '".$id_origen."' IN (34,39,5) THEN 'la Promoción'
					END AS tipo
					FROM prov_minisitios_productos spa
					WHERE spa.id = " . $this->CI->db->escape($item);
		}
		
		$query = $this->CI->db->query($sql);
		$item = $query->row_array();

		if(count($item) > 0){
			$prod = "Presupuesto sobre " . $item['tipo'] . ": " . $item['titulo'];
		}

		return $prod;
	}

	public function procesar_contacto_presupuesto($data){
		//inserta el historial
		$hist = array();

		$hist['id_contacto'] = $this->procesar_contacto($data);
		$prov = $this->get_proveedor_info($data['id_minisitio']);
		if($prov['id_proveedor']) $hist['id_proveedor'] = $prov['id_proveedor'];
		if($prov['id_rubro']) $hist['id_rubro'] = $prov['id_rubro'];
		if($prov['id_sucursal']) $hist['id_sucursal'] = $prov['id_sucursal'];
		$hist['id_referencia'] = $data['id_referencia'];
		$hist['id_portal'] = $data['id_portal'];
		$hist['id_evento_tipo'] = $data['id_tipo_evento'];
		$hist['id_padre'] = $data['id_pres'];
		$data['id_origen'] = (int)$data['id_origen'];

		$sql = 'SELECT id FROM base_origenes WHERE id_origen_presupuesto = ' . $data['id_origen'];
		$query = $this->CI->db->query($sql);
		$tmp = $query->row();
		$hist['id_origen'] = $tmp->id;

		if(!empty($data["anio_evento"]) && !empty($data["mes_evento"]) && !empty($data["dia_evento"]) && empty($data["fecha_evento"])){
			$hist['fecha_evento'] = $data['anio_evento'] . "-" . $data['mes_evento'] . "-" . $data['dia_evento'];
		}elseif(!empty($data["fecha_evento"])){
			$hist["fecha_evento"] = $data["fecha_evento"];
		}

		$this->CI->db->insert('base_contactos_historial', $hist);

		//inserta o modifica las suscripciones segun el presupuesto
		if($data['id_portal'] == 1){
			//id 1 infonovias
			$estado_novia = $data['infonovias'] ? '1' : '-1';
			$this->procesar_contacto_suscripcion($hist['id_contacto'], 1, $estado_novia);

			//id 3 promociones
			$estado_promo = $data['promociones'] ? '1' : '-1';
			$this->procesar_contacto_suscripcion($hist['id_contacto'], 3, $estado_promo);
		}

		if($data['id_portal'] == 2){
			//id 6 espacio news
			$estado_espacio_news = $data['espacio_news'] ? '1' : '-1';
			$this->procesar_contacto_suscripcion($hist['id_contacto'], 6, $estado_espacio_news);
		}
	}

	public function procesar_presupuesto_sistemas($data){

		$asociados = $data['asociados'];
		unset($data['asociados']);

		$esAsociado = 0;
		if(is_array($asociados)){
			if($asociados[0] && count($asociados) > 0){
				$esAsociado = 1;
			}
		}
		elseif(!empty($asociados)){
			$esAsociado = 1;
		}

		if($esAsociado){
			$origenID = $data['id_origen'];
			switch($data['id_origen']){
				case 0: // Minisitio con asociados
					$origenID = 17;
					$origenAsociado = 18;
				break;
				case 31: // Minisitio Lateral con asociados
					$origenID = 41;
					$origenAsociado = 42;
				break;
				case 8: // Vista - desde rubro empresas con asociados
					$origenID = 19;
					$origenAsociado = 20;
				break;
				case 25: // Individual Mapa con asociados
					$origenID = 43;
					$origenAsociado = 44;
				break;
			}
			$data['id_origen'] = $origenID;
		}

		$data['id_user'] = $this->get_usuario_id($data['email']);

		if(!empty($data['id_user'])){
			$data['id_referencia'] = 4;
		}

		if(!empty($data['minisitios'])){
			foreach ($data['minisitios'] as $mss){
				$ms_s = explode(',',$mss);
				foreach($ms_s as $m){
					$data['id_minisitio'][] = $m;
				}
			}
			unset($data['minisitios']);
		}

		// Presupuesto para estadisticas del rubro
		if (is_array($data['id_rubro'])){
			foreach ($data['id_rubro'] as $rb){
				$this->insertar_presupuesto_rubro($rb, $data['id_origen'], $data['id_landing']);
			}
		} else {
			$this->insertar_presupuesto_rubro($data['id_rubro'], $data['id_origen'], $data['id_landing']);
		}

		
		// Presupuesto para estadisticas del proveedor
		if(is_array($data['id_minisitio'])){
			foreach ($data['id_minisitio'] as $ms){

				$prov = $this->get_proveedor_info($ms);
				$this->insertar_presupuesto_proveedor($prov['id_proveedor'], $data['id_origen'], $data['id_landing']);

				$temp_data = $contacto = $data;
				$temp_data['id_minisitio'] = $contacto['id_minisitio'] = $ms;
				$temp_data['id_localidad'] = ($temp_data['id_localidad'])? $temp_data['id_localidad'] : $data['localidad'];
				unset($temp_data['id_rubro']);
				unset($temp_data['id_pais']);
				unset($temp_data['id_portal']);
				unset($temp_data['ip']);
				unset($temp_data['id_provincia']);
				unset($temp_data['localidad']);
				unset($temp_data['infonovias']);
				unset($temp_data['promociones']);
				unset($temp_data['espacio_news']);
				unset($temp_data['id_pres']);

				if((int)$temp_data['id_item'] > 0){// INDICA EN EL COMENTARIO TIPO (producto, promo, foto, ect) Y TITULO
					$temp_data['comentario'] = $this->get_item_presupuesto($temp_data['id_item'], $temp_data['id_origen']).'.  '.$temp_data['comentario'];
				}

				$this->CI->db->insert('prov_presupuestos', $temp_data);

				$contacto['id_pres'] = $temp_data['id_pres'] = $this->CI->db->insert_id();
				$contacto['id_portal'] = $data['id_portal'];
				$contacto['id_referencia'] = 16;
				$contacto['id_tipo_evento']= $data['id_tipo_evento'];

				$this->procesar_contacto_presupuesto($contacto);

				// contenido a enviar por mail solo si el proveedor tiene un mail cargado en el MS, no importa si esta o no online
				if ($prov['email']){
					if($prov['autorespuesta_activa']){
						unset($prov['autorespuesta_activa']);
					}

					$this->procesar_presupuesto_envio($this->procesar_presupuesto_newsletter($temp_data, $prov), $prov, $temp_data);
				}
			}
		}else{			
			$prov = $this->get_proveedor_info($data['id_minisitio']);

			$this->insertar_presupuesto_proveedor($prov['id_proveedor'], $data['id_origen'], $data['id_landing']);
			
			$temp_data = $contacto = $data;

			unset($temp_data['id_rubro']);
			unset($temp_data['id_provincia']);
			unset($temp_data['id_pais']);
			unset($temp_data['id_portal']);
			unset($temp_data['ip']);
			unset($temp_data['infonovias']);
			unset($temp_data['promociones']);
			unset($temp_data['espacio_news']);

			if((int)$temp_data['id_item'] > 0){ // INDICA EN EL COMENTARIO TIPO (producto, promo, foto, ect) Y TITULO
				$temp_data['comentario'] = $this->get_item_presupuesto($temp_data['id_item'], $temp_data['id_origen']) . '.  ' . $temp_data['comentario'];
			}
			
			$this->CI->db->insert('prov_presupuestos', $temp_data);
			
			$contacto['id_pres'] = $temp_data['id_pres'] = $this->CI->db->insert_id();
			$contacto['id_portal'] = $data['id_portal'];
			$contacto['id_referencia'] = 16;
			$contacto['id_tipo_evento']= $data['id_tipo_evento'];

			$this->procesar_contacto_presupuesto($contacto);

			
			// contenido a enviar por mail solo si el proveedor tiene un mail cargado en el MS, no importa si esta o no online
			if ($prov['email']){
				if($prov['autorespuesta_activa']){
					$this->procesar_presupuesto_envio($this->procesar_presupuesto_newsletter($temp_data, $prov),$prov,$temp_data);
					unset($prov['autorespuesta_activa']);
				}

				$this->procesar_presupuesto_envio($this->procesar_presupuesto_newsletter($temp_data, $prov),$prov,$temp_data);				
			}
			

			if ($esAsociado){
				$this->prov_con_asociados = $prov['proveedor'];
				$this->prov_asociados = 1;

				$data['id_origen'] = $origenAsociado;
				$temp_data = $contacto = $data;

				unset($temp_data['id_rubro']);
				unset($temp_data['id_provincia']);
				unset($temp_data['id_pais']);
				unset($temp_data['id_portal']);
				unset($temp_data['ip']);
				unset($temp_data['infonovias']);
				unset($temp_data['promociones']);
				unset($temp_data['espacio_news']);

				$rubros_asociados = array();
				foreach ($asociados as $a){

					if($this->getProveedorRecibePresupuesto($data['id_minisitio'], $a)){ //-- si el asociado esta activo y recibe presupuesto => ENVIAR
						$sql = " SELECT id as id_minisitio FROM prov_minisitios WHERE id_proveedor = " . $this->CI->db->escape((int) $a) . " LIMIT 1 ";

						$query = $this->CI->db->query($sql);
						$tmp = $query->row();
						$id_minisitio = $tmp->id_minisitio;

						$prov = $this->get_proveedor_info($id_minisitio);

						if (!in_array($prov['id_rubro'], $rubros_asociados)) {
  	 						array_push($rubros_asociados, $prov['id_rubro']);
  	 						$res = $this->insertar_presupuesto_rubro($prov['id_rubro'], $data['id_origen'], $data['id_landing']);
  	 					}

  	 					

  	 					$res = $this->insertar_presupuesto_proveedor($prov['id_proveedor'], $data['id_origen'], $data['id_landing']);
  	 					$temp_data['id_minisitio'] = $contacto['id_minisitio'] = $id_minisitio;

  	 					unset($temp_data['id_pres']);

  	 					$this->CI->db->insert('prov_presupuestos', $temp_data);

  	 					$contacto['id_pres'] =  $temp_data['id_pres'] = $this->CI->db->insert_id();
						$contacto['id_portal'] = $data['id_portal'];
						$contacto['id_referencia'] = 16;
						$contacto['id_tipo_evento']= $data['id_tipo_evento'];

						// contenido a enviar por mail solo si el proveedor tiene un mail cargado en el MS, no importa si esta o no online
						if ($prov['email']){
							if($prov['autorespuesta_activa']){
								unset($prov['autorespuesta_activa']);
							}

							$this->procesar_presupuesto_envio($this->procesar_presupuesto_newsletter($temp_data, $prov),$prov,$temp_data);
						}

						$this->procesar_contacto_presupuesto($contacto);
					}
				}
			}
		}
	}

	public function getProveedorRecibePresupuesto($id_minisitio, $id_asociado){
		$sql  = " SELECT recibe FROM prov_minisitios_asociados pma ";
		$sql .= " INNER JOIN site_proveedores_activos spa ON spa.id_proveedor = pma.id_asociado ";
		$sql .= " INNER JOIN prov_proveedores p ON pma.id_asociado = p.id ";
		$sql .= " WHERE pma.id_asociado = '".$id_asociado."' AND pma.id_minisitio = '".$id_minisitio."'	AND recibe = 1 AND pma.estado > 0 AND p.estado= 6 ";

		$res = $this->CI->db->query($sql)->row_array();

		if(!empty($res["recibe"])){
			return true;
		}else{
			return false;
		}
	}

	public function procesar_presupuesto_newsletter($pres, $prov){
		$this->CI->load->model('rubros_model');
		$this->CI->load->model('proveedores_model');
		
		$this->CI->load->library('parseo_library');
		$this->CI->load->library('htmldom_library');

		// include_once($_SERVER["DOCUMENT_ROOT"] . '/includes/php/simple_html_dom.php');
		
		// -- segun el id de minisitio, relaciono con proveedor y saco cual es el id_portal para luego utilizar el template correspondiente
		if($pres["id_minisitio"]){
			$sql = "SELECT pp.id_portal,se.tipo
					FROM prov_minisitios pm
					LEFT JOIN prov_proveedores pp ON pp.id = pm.id_proveedor
					LEFT JOIN sys_eventos_tipo se ON se.id = pp.id_evento_tipo
					WHERE pm.id = " . $this->CI->db->escape($pres["id_minisitio"]);
			
			$query = $this->CI->db->query($sql);
			$res = $query->row_array();
			$id_portal = $res["id_portal"];
			$tipo_evento = $res["tipo"];
		}
		// --

		$data_news = array();

		$sql = 'SELECT id FROM prov_minisitios WHERE nuevo_sistema = 1'; // Emi dice: la consulta esta asi y no con GROUP_CONCAT porque no entran la cantidad de id minisitio en un mismo campo
		$query = $this->CI->db->query($sql);
		$rs_ms = $query->result_array();

		foreach($rs_ms as $tmp){
			$excepciones_webmail[] = $tmp["id"];
		}

		if (empty($prov['autorespuesta_activa'])){
			$sql = "SELECT spa.titulo, spa.sucursal, 'http://www.casamientosonline.com/productos-para-casamientos-en-' as imagen
					, pp.id_item, 'Producto' AS tipo, 'un' AS a
					FROM prov_presupuestos pp
					INNER JOIN site_productos_activos spa ON spa.id_producto=pp.id_item
					WHERE pp.id = " . $this->CI->db->escape($pres['id_pres']) . " AND pp.id_origen in (33,36,38,4)
					UNION
					SELECT spa.titulo, spa.sucursal, 'http://www.casamientosonline.com/promociones-para-casamientos-en-' as imagen
					, pp.id_item, 'Promoción' AS tipo , 'una' AS a
					FROM prov_presupuestos pp
					INNER JOIN site_productos_activos spa ON spa.id_producto=pp.id_item
					WHERE pp.id = " . $this->CI->db->escape($pres['id_pres']) . " AND pp.id_origen in (34,39,5,37)
					UNION
					SELECT sysm.titulo, '' AS sucursal, CONCAT('http://media.casamientosonline.com/images/',sysm.name,'_grande.',sysa.extension) as imagen
					, pp.id_item, 'Foto' AS tipo, 'una' AS a
					FROM prov_presupuestos pp
					INNER JOIN sys_medias sysm ON sysm.id=pp.id_item
					INNER JOIN sys_archivos_tipo sysa on sysa.id=sysm.id_tipo
					WHERE pp.id = " . $this->CI->db->escape($pres['id_pres']) . " AND pp.id_origen in (23)
					UNION
					SELECT sysm.titulo, '' AS sucursal, sysm.url as imagen
					, pp.id_item, 'Video' AS tipo, 'un' AS a
					FROM prov_presupuestos pp
					INNER JOIN sys_medias sysm ON sysm.id=pp.id_item
					INNER JOIN sys_archivos_tipo sysa on sysa.id=sysm.id_tipo
					WHERE pp.id = " . $this->CI->db->escape($pres['id_pres']) . " AND pp.id_origen in (30)
					UNION
					SELECT spa.titulo, spa.sucursal, 'http://www.casamientosonline.com/paquetes-para-casamientos-en-' as imagen, pp.id_item, 'Paquete' AS tipo, 'un' AS a
					FROM prov_presupuestos pp
					INNER JOIN site_paquetes_activos spa ON spa.id_paquete=pp.id_item
					WHERE pp.id = " . $this->CI->db->escape($pres['id_pres']) . " AND pp.id_origen in (35,40,32,14,21)";

			$query = $this->CI->db->query($sql);
			$item = $query->row_array();
			$prod_item = '';

			if($item['id_item'] != ''){
				if($item['tipo'] == 'Producto' || $item['tipo'] == 'Promoción' || $item['tipo'] == 'Paquete'){
					$imagen = $item['imagen'] . str_replace(" ", '-', $this->CI->parseo_library->clean_url($item['sucursal'])) . '/' . str_replace(" ", '-', $this->CI->parseo_library->clean_url($item['titulo'])) . '/' . $item['id_item'];
				}
				if($item['tipo'] == 'Foto' || $item['tipo'] == 'Video'){
					$imagen = $item['imagen'];
				}
				$prod_item = '<a style="color:#705; text-decoration:none;" href="'.$imagen.'">'.$item['titulo'].'</a>';
			}

			$data_news['remitente'] = $pres['nombre'] . ' ' . $pres['apellido'];
			$data_news['email'] = $pres['email'];

			$loc = $this->CI->rubros_model->get_localidad($pres['id_localidad']);

			if(in_array($pres["id_minisitio"],$excepciones_webmail)){
				// compongo la direccion de email del remitente "[NOVIO]-N-[ID CONTACTO]-[ID PRESUP]@col.com"
				$novio = $this->CI->parseo_library->clean_url(strtolower($pres["nombre"]."-".$pres["apellido"]));

				// le limito el nombre del novio a 25 caracteres
				$novio = substr($novio,0,25);

				// le quito las barras invertidas al novio por si clean_url_3 no lo hizo
				$novio = str_replace('\\', '', $novio);

				$id_contacto = $this->get_contacto_id($pres['email']);

				$data_news['email'] = "$novio-n-".$id_contacto."-".$pres["id_pres"]."@plataformaeventos.com";
				$data_news['remitente'] = $pres['nombre'] . ' ' . $pres['apellido'];
				$data_news['respuesta_email'] = $data_news['email'];
			}

			if($pres['fecha_evento'] && $pres['fecha_evento'] != '0000-00-00'){
				$aux_fec = explode("-", $pres['fecha_evento']);
				$pres['fecha_evento'] = $aux_fec[2].'/'.$aux_fec[1].'/'.$aux_fec[0];
			}else{
				$pres['fecha_evento'] = '-';
			}

			if($pres['fecha_nacimiento'] && $pres['fecha_nacimiento'] != '0000-00-00'){
				$aux_fec = explode("-", $pres['fecha_nacimiento']);
				$pres['fecha_nacimiento'] = $aux_fec[2].'/'.$aux_fec[1].'/'.$aux_fec[0];
			}else{
				$pres['fecha_nacimiento'] = '-';
			}

			$data_news['id_tipo'] = 2;

			// -- segun el id_portal, levanto el template en html
			if($id_portal == 2){ // espacioeventos
				$html = $this->CI->htmldom_library->file_get_html($_SERVER["DOCUMENT_ROOT"] . '/comunicaciones/presupuesto_ee.html');
			}else{
				$html = $this->CI->htmldom_library->file_get_html($_SERVER["DOCUMENT_ROOT"] . '/comunicaciones/nuevo_pedido_presupuesto.html');				
			}
			// --

			
			// -- valido algunas condiciones para mostrar o quitar campos que no necesite del template del mail
			if (empty($prod_item)){
				$tmp = $html->find('tr[id=prod_item]', 0);
				if(!empty($tmp->innertext)) $html->find('tr[id=prod_item]', 0)->innertext = '';
			}
			if (empty($this->prov_asociados)){
				$tmp = $html->find('tr[id=asociados]', 0);
				if(!empty($tmp->innertext)) $html->find('tr[id=asociados]', 0)->innertext = '';
				$tmp = $html->find('p[id=asociados]', 0);
				if(!empty($tmp->innertext)) $html->find('p[id=asociados]', 0)->innertext = '';
			}
			if (in_array($pres["id_minisitio"],$excepciones_webmail)){
				$tmp = $html->find('#email', 0);
				if(!empty($tmp->innertext)) $html->find('#email', 0)->innertext = '';
			}
			if (empty($pres['estimado'])){
				$tmp = $html->find('#presup_estimado', 0);
				if(!empty($tmp->innertext)) $html->find('#presup_estimado', 0)->innertext = '';
			}
			if (empty($pres['ubicacion'])){
				$tmp = $html->find('#ubicacion', 0);
				if(!empty($tmp->innertext)) $html->find('#ubicacion', 0)->innertext = '';
			}
			// -- fin validacion

			// -- reemplazo placeholders del template HTML con contenido real para el envio del mail
			$html = str_replace("{ITEM_PROD}", $prod_item, $html);
			$html = str_replace("{ITEM_TIPO}", $item['tipo'], $html);
			$html = str_replace("{PROV_CON_ASOCIADOS}", $this->prov_con_asociados, $html);
			$html = str_replace("{NOMBRE}", $pres["nombre"], $html);
			$html = str_replace("{APELLIDO}", $pres["apellido"], $html);
			$html = str_replace("{FECHA_EVENTO}", $pres["fecha_evento"], $html);
			$html = str_replace("{TIPO_EVENTO}", $tipo_evento, $html);
			$html = str_replace("{CANT_INVITADOS}", $pres["invitados"], $html);
			$html = str_replace("{PRESUP_ESTIMADO}", (!empty($pres["estimado"]) ? $pres["estimado"] : ' - '), $html);
			$html = str_replace("{TELEFONO}", $pres["telefono"], $html);
			$html = str_replace("{EMAIL}", $pres["email"], $html);
			$html = str_replace("{SUCURSAL}", $prov["sucursal"], $html);
			$html = str_replace("{PROVINCIA}", $loc["provincia"], $html);
			$html = str_replace("{LOCALIDAD}", $loc["localidad"], $html);
			$html = str_replace("{FECHA_NACIMIENTO}", $pres["fecha_nacimiento"], $html);
			$html = str_replace("{UBICACION}", $pres["ubicacion"], $html);
			$html = str_replace("{COMENTARIO}", $pres["comentario"], $html);

			if(in_array($pres["id_minisitio"],$excepciones_webmail)){
				$html = str_replace("{MAILTO_RESPONDER}", $data_news['email'], $html);
			}else{
				$html = str_replace("{MAILTO_RESPONDER}", $pres['email'], $html);
			}
			// -- fin reemplazo

			$mensaje_envio = $this->string_separator() . $html;
		}else{
			$tel = explode('@',$prov['telefonos']);
			$prov['telefonos'] = $tel[0].' '.$tel[1];

			$res = $this->CI->proveedores_model->minisitios_archivos_autoreply($pres["id_minisitio"]);

			if(in_array($pres["id_minisitio"],$excepciones_webmail)){
				$envio = $this->CI->proveedores_model->get_envio_proveedor($pres['id_pres']);

				// compongo la direccion de email del remitente "[PROVEEDOR]-P-[ID CONTACTO PROV]-[ID PRESUP]@col.com"
				$proveedor = $this->CI->parseo_library->clean_url(strtolower($prov["proveedor"]));

				//le limito el nombre del proveedor a 25 caracteres
				$proveedor = substr($proveedor,0,25);

				// le quito las barras invertidas al proveedor por si clean_url_3 no lo hizo
				$proveedor = str_replace('\\', '', $proveedor);

				$data_news['email'] = "$proveedor-p-".$envio["id_usuario_proveedor"]."-".$pres["id_pres"]."@plataformaeventos.com";
				$data_news['remitente'] = $prov["proveedor"];
				$data_news['respuesta_email'] = $data_news['email'];

			}

			$linea_corte = $this->string_separator();

			// -- segun el id_portal, levanto el template en html
           	if($id_portal == 2){ // espacioeventos
				$html = $this->CI->htmldom_library->file_get_html($_SERVER["DOCUMENT_ROOT"] . '/comunicaciones/autorespuesta_ee.html');
			}else{
				$html = $this->CI->htmldom_library->file_get_html($_SERVER["DOCUMENT_ROOT"] . '/comunicaciones/autorespuesta.html');
			}
			// --

			// -- valido algunas condiciones para mostrar o quitar campos que no necesite del template del mail
			if (!$res){
				$tmp = $html->find('#archivos_adjuntos', 0);
				if(!empty($tmp->innertext)) $html->find('#archivos_adjuntos', 0)->innertext = '';

				$tmp = $html->find('#white_space', 0);
				if(!empty($tmp->innertext)) $html->find('#white_space', 0)->innertext = '';
			}
			// -- fin validacion

			// -- reemplazo placeholders del template HTML con contenido real para el envio del mail
			$html = str_replace("{NOMBRE}", $pres["nombre"], $html);
			$html = str_replace("{APELLIDO}", $pres["apellido"], $html);
			$html = str_replace("{MENSAJE_AUTORESPUESTA}",$prov["autorespuesta"],$html);
			$html = str_replace("{PROVEEDOR}",$prov["proveedor"],$html);
			$html = str_replace("{LOGO}",$prov["logo"],$html);
			$html = str_replace("{DIRECCION}",$prov["direccion"],$html);
			$html = str_replace("{TELEFONOS}",$prov["telefonos"],$html);



			if(in_array($pres["id_minisitio"],$excepciones_webmail)){
				$html = str_replace("{MAILTO_RESPONDER}", $data_news['email'], $html);
			}else{
				$html = str_replace("{MAILTO_RESPONDER}", $prov['email'], $html);
			}

			$archivos = '';
			if($res){
				foreach($res as $archivos_adjuntos){
					$v = explode("/", $archivos_adjuntos["archivo"]);
					$filename = array_pop($v);
					$archivos .= '<a href="'.$archivos_adjuntos["archivo"].'" target="_blank" style="font-size:14px; color:#444; text-decoration:none; line-height:20px; margin-bottom:5px;" title="Descargar">'.$filename.'</a><br />';
				}
				$html = str_replace("{ARCHIVOS_ADJUNTOS}", $archivos, $html);
			}
			// -- fin reemplazo

			$mensaje_envio = $linea_corte . $html;

            if(!in_array($pres["id_minisitio"],$excepciones_webmail)){
				$data_news['remitente'] = $prov['proveedor'];
				$data_news['email'] = $prov['email'];
				$data_news['respuesta_email'] = $prov['email'];
			}

			$data_news['id_tipo'] = 14;

			// inserto comunicacion en prov_presupuestos_comunicaciones para que figure la autorespuesta en el detalle de presupuesto del proveedor
			$data_com["id_presupuesto"] = $pres["id_pres"];
			$data_com["id_user"] = !empty($envio["id_usuario_proveedor"]) ? $envio["id_usuario_proveedor"] : NULL;
			$data_com["estado"] = 2;
			$data_com["tipo"] = 3;
			$data_com["asunto"] = "Autorespuesta";
			$data_com["mensaje"] = $prov["autorespuesta"]?$prov["autorespuesta"]:"-";
			
			$this->CI->db->insert('prov_presupuestos_comunicaciones', $data_com);
			// --
		}

		
		$data_news['newsletter'] = 'Presupuesto: '.$pres['id_pres'];


		// Cambio el asunto del Email segun el portal de donde es, si no es de EE por default pondra COL.
		if($id_portal == 2){ // espacioeventos
				$data_news['asunto'] = 'EspacioEventos.com - Pedido de presupuesto a '.$prov['proveedor'].' ['.$prov['rubro'].']';
			}else{
				$data_news['asunto'] = 'CasamientosOnline.com - Pedido de presupuesto a '.$prov['proveedor'].' ['.$prov['rubro'].']';
			}

		$data_news['mensaje'] = $mensaje_envio;

		$checksum = md5($data_news['remitente'].$data_news['email'].$data_news['asunto'].$mensaje_envio);
		
		$envio = false;

		//ya estaba insertado en la base de envio
		if ($envio){
			return $envio;
		}else{
			$data_news['checksum'] = $checksum;

			if(!in_array($pres["id_minisitio"],$excepciones_webmail)){
				$data_news['remitente'] = 'PlataformaEventos';
				$data_news['email'] = 'presupuestos@plataformaeventos.com';
				if (empty($prov['autorespuesta_activa'])){
					$data_news['respuesta_email'] = $pres['email'];

					// -- si el mail del proveedor es de hotmail, hago que el email del remitente sea el mail original
					$m_array = explode("@", $prov['email']);
					$m_array = explode(".",$m_array[1]);
					array_pop($m_array);
					$domain = strtolower(implode(".",$m_array));

					if($domain == "hotmail"){
						$data_news['email'] = $pres['email'];
					}
					// --
				}else{
					$data_news['respuesta_email'] = $prov['email'];
				}
			}

			if (empty($prov['autorespuesta_activa'])){
				$data_news['respuesta_nombre'] = $pres['nombre'] . ' ' . $pres['apellido'];
			}else{
				$data_news['respuesta_nombre'] = $prov['proveedor'];
			}
			
			if(!empty($data_news['respuesta_nombre'])){
				if(!trim($data_news['respuesta_nombre'])) unset($data_news['respuesta_nombre']);
			}

			$this->CI->db->insert('emailer_newsletters', $data_news);

			return $this->CI->db->insert_id();
		}
	}

	public function string_separator(){
		//return '<span style="font-size:11px; color:#666">No responda debajo de esta linea</span><hr /><br /><br />';
		return '<p style="font-size:11px; color:#999; margin-bottom:20px;">---------- responda por encima de esta linea, el contenido por debajo no sera enviado --------------------</p>';
	}

	public function procesar_presupuesto_envio($id_news, $prov, $pres){
		$data_envio = array();
		$data_envio['id_presupuesto'] = $pres['id_pres'];
		$data_envio['id_newsletter'] = $id_news;

		if (empty($prov['autorespuesta_activa'])){
			$data_envio['destinatario'] = $prov['proveedor'];
			$data_envio['email'] = $prov['email'];
			$tabla = 'prov_presupuestos_envios';
		} else {
			$data_envio['destinatario'] = $pres['nombre'].' '.$pres['apellido'];
			$data_envio['email'] = $pres['email'];
			$tabla = 'prov_presupuestos_envios_novios';
		}
		
		$this->CI->db->insert($tabla, $data_envio);
	}

	public function actualizar_datos_novio($data){
		if(!$this->procesos_off){
			
			$this->CI->load->helper('common_helper');		

			unset($data['tipo']);
			$temp_data = $data = limpiarArray($data);
			$dir = array();
			if(!empty($data['anio_evento']) && !empty($data['mes_evento'])){
				$temp_data['fecha_evento'] = $data['anio_evento'] . '-' . $data['mes_evento'] . '-' . $data['dia_evento'];	
			}else{
				$tmp = explode('/', $data['dia_evento']);
				$temp_data['fecha_evento'] = $tmp[2] . '-' . $tmp[1] . '-' . $tmp[0];
			}
			
			unset($temp_data['anio_evento']);
			unset($temp_data['mes_evento']);
			unset($temp_data['dia_evento']);
			unset($temp_data['id_origen']);
			unset($temp_data['id_evento']);
			unset($temp_data['id_provincia']);
			unset($temp_data['id_localidad']);
			unset($temp_data['id_referencia']);
			unset($temp_data['id_origen_contacto']);
			unset($temp_data['infonovias']);
			unset($temp_data['promociones']);
			unset($temp_data['id_novio']);
			unset($temp_data['update_data']);
			
			$temp_data['estado'] = 0;

			if(!empty($data['id_novio'])) $this->CI->db->where('id', $data['id_novio'])
													->update('evn_novios', $temp_data);

			$data['id'] 			= $data['id_novio'];
			$dir['id_localidad'] 	= $data['id_localidad'];
			$dir['id_referencia'] 	= $data['id_referencia'];

			if(!empty($data['id_novio'])) $this->CI->db->where('id_padre', $data['id_novio'])
													->update('sys_direcciones', $dir);

			if(empty($temp_data["id_origen"])) $temp_data["id_origen"] = 0;

			if(!empty($data['email'])){
				$data['id_user'] 		= $temp_data['id_user'] = $this->get_usuario_id($data['email']);
				$data['id_origen'] 		= $data['id_origen_contacto'];
				$data['id_tipo_evento'] = 1;
				$data['id_portal'] 		= 1;
				$data['id_referencia'] 	= $data['id_referencia'];
				$data['id_sucursal'] 	= 1;
				$data['id_rubro'] 		= NULL;

				$this->procesar_contacto_evento($data);
			}
		}
	}

	public function actualizar_datos_proveedor($data){
		if(!$this->procesos_off){
			
			$this->CI->load->helper('common_helper');
			
			unset($data['tipo']);
			$temp_data = $data = limpiarArray($data);
			unset($temp_data['id_referencia']);
			unset($temp_data['id_origen_contacto']);
			unset($temp_data['id_novio']);
			unset($temp_data['update_data']);
			unset($temp_data['id_origen']);
			unset($temp_data['id_evento']);
			unset($temp_data['infonovias']);

			$temp_data['estado'] = 0;

			if(!empty($data['id_novio'])) $this->CI->db->where('id', $data['id_novio'])
													->update('evn_proveedores', $temp_data);

			$data['id'] 			= $data['id_novio'];
			//piso el origen para el contacto
			$data['id_origen'] 		= $data['id_origen_contacto'];
			$data['id_tipo_evento'] = 1;
			$data['id_portal'] 		= 1;
			$data['id_referencia'] 	= $data['id_referencia'];
			$data['id_sucursal'] 	= 1;
			$data['id_rubro'] 		= $data['id_rubro'];

			$this->procesar_contacto_evento($data);
		}
	}
}