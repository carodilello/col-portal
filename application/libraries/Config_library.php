<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_library{
    protected $CI;
    public $sucursal = array();
    public $sucursales = array();
    public $sucursales_seo = array();
    public $combo_rubros = array();
    public $id_seccion = "";
    public $id_origen = array(
        "empresas" => 0,
        "paquetes" => 3,
        "promociones" => 2,
        "productos" => 1,
        "mapa" => 4,
        "fotos_galerias" => 5,
        "fotos" => 6,
        "cupones" => 7
    );
    public $resultados = 0;
    public $er_fecha = "[0-9]{2}/[0-9]{2}/[0-9]{4}";
    public $proveedor_vencido = array();

    public function __construct(){
        $this->CI =& get_instance();
        $this->set_sucursal();
    }

    public function set_sucursal(){
        if(!$this->CI->session->userdata('id_sucursal')){ // Seteo session para sucursal
            $this->CI->session->set_userdata(array('id_sucursal' => $this->CI->config->item('id_sucursal')));
        }
        if($this->CI->session->userdata('id_sucursal') && !in_array($this->CI->session->userdata('id_sucursal'), array(1,2,3,4,5,6,7,8,10,11,12))){ // POR SI LA SESSION CARGADA NO ESTA ENTRE LAS PERMITIDAS
            $this->CI->session->set_userdata(array('id_sucursal' => $this->CI->config->item('id_sucursal')));
        }

        $this->CI->load->model('navegacion_model');
        $this->sucursal = $this->CI->navegacion_model->getSucursal($this->CI->session->userdata('id_sucursal'));
        $this->sucursales = $this->CI->navegacion_model->getSucursales();
        $this->combo_rubros = $this->CI->navegacion_model->get_rubros_combo($this->CI->session->userdata('id_sucursal'));
    }

    public function cambiar_sucursal($id){
        $sucursal = $this->CI->navegacion_model->getSucursal($id);

        if($sucursal["id"]){
            $this->CI->session->set_userdata(array('id_sucursal' => $sucursal["id"]));
            $this->set_sucursal();
        }else{
            redirect(base_url('/'));
        }
    }

    public function cambiar_sucursal_comercial($id_sucursal){
        $this->sucursales_seo = array(
            1   => "buenos-aires",
            4   => "cordoba",
            10  => "entre-rios",
            5   => "mendoza",
            6   => "mar-del-plata",
            7   => "la-plata",
            12  => "misiones",
            2   => "rosario",
            11  => "salta",
            3   => "santa-fe",
            8   => "tucuman"
        );

        if($id_sucursal != $this->CI->session->userdata('id_sucursal') && in_array($id_sucursal, array(1,2,3,4,5,6,7,8,10,11,12))){
            $this->cambiar_sucursal($id_sucursal);
            $r_uri = '';
            foreach ($this->sucursales_seo as $sucursal) {
                if(strpos($_SERVER['REQUEST_URI'], $sucursal)) $r_uri = str_replace($sucursal, $this->sucursales_seo[$id_sucursal], $_SERVER['REQUEST_URI']);
            }

            if($r_uri){
                redirect(base_url($r_uri));
            }else{
                redirect('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            }
        }

    }

    public function set_seccion(){
        switch($this->CI->session->userdata('id_sucursal')){
            case 1:
                $this->id_seccion = 123;
                break;
            case 2:
                $this->id_seccion = 179;
                break;
            case 3:
                $this->id_seccion = 208;
                break;
            case 4:
                $this->id_seccion = 407;
                break;
            case 6:
                $this->id_seccion = 490;
                break;
            case 5:
                $this->id_seccion = 559;
                break;
            case 7:
                $this->id_seccion = 707;
                break;
            case 8:
                $this->id_seccion = 889;
                break;
            case 10:
                $this->id_seccion = 890;
                break;
            case 11:
                $this->id_seccion = 836;
                break;
            case 12:
                $this->id_seccion = 888;
                break;
        }
    }

    public function basic_data_to_view($data = NULL){
        if(!isset($data["id_sucursal"]) || !$data["id_sucursal"]) $data["id_sucursal"] = $this->CI->session->userdata('id_sucursal');
        if(!isset($data["sucursales"]) || !$data["sucursales"]) $data["sucursales"] = $this->sucursales;
        if(!isset($data["sucursal"]) || !$data["sucursal"]) $data["sucursal"] = $this->sucursal;
        if(!isset($data["combo_rubros"]) || !$data["combo_rubros"]) $data["combo_rubros"] = $this->combo_rubros;
        if(!isset($data["sucursal_redirect"]) || !$data["sucursal_redirect"]) $data["sucursal_redirect"] = '/' . $this->CI->uri->segment(1);
        if(!isset($data["prov_vencido"]) || !$data["prov_vencido"]) $data["prov_vencido"] = FALSE;
        if(!isset($data["registro_infonovia_corto"]) || !$data["registro_infonovia_corto"]) $data["registro_infonovia_corto"] = $this->CI->session->userdata('registro_infonovia_corto');
        
        $data["hash"] = isset($_GET['hash']) && $_GET['hash'] ? ('?hash=' . $_GET['hash']) : '';
        
        if($this->CI->session->userdata('gracias')){
            $data['gracias'] = $this->CI->session->userdata('gracias');
            $this->CI->load->model('navegacion_model');
            if($this->CI->session->userdata('id_rubro_gracias')){
                $data['rubros_gracias'] = $this->CI->navegacion_model->get_rubros_gracias($this->CI->session->userdata('id_rubro_gracias'));
            }elseif($this->CI->session->userdata('id_rubro')){
                $data['rubros_gracias'] = $this->CI->navegacion_model->get_rubros_gracias($this->CI->session->userdata('id_rubro'));
            }
        }

        return $data;
    }

    public function send_data_to_view($data = NULL){ // DATOS EN COMUN EN GRAN PARTE DE LOS METODOS DEL CONTROLADOR PROVEEDORES
        $this->CI->load->library('parseo_library');

        $data = $this->basic_data_to_view($data);

        $data["er_fecha"] = $this->er_fecha;

        if(isset($data["proveedor"])){
            $vCard_data = 'BEGIN:VCARD
VERSION:3.0
N:'.$data["proveedor"]["proveedor"].';
FN:'.$data["proveedor"]["proveedor"].'
ORG:'.$data["proveedor"]["proveedor"].'
TITLE:'.$data["proveedor"]["titulo"]."\n";

        if(isset($data["proveedor"]["telefonos"]) && is_string($data["proveedor"]["telefonos"])){
            $vc_tels = explode('/',$data["proveedor"]["telefonos"]);

            if(is_array($vc_tels)) foreach ($vc_tels as $k => $tel) {
                $vCard_data .= 'TEL;TYPE=WORK,VOICE:'.$tel."\n";
            }
        }

        if(isset($data["proveedor"]["direccion"]) && $data["proveedor"]["direccion"]){
$vCard_data .= 'LABEL;TYPE=WORK:'.$data["proveedor"]["direccion"].'
REV:20080424T195243Z
END:VCARD';
        }

            $data['vcard'] = urlencode($vCard_data);
        }

        if(isset($_GET['no_empresas']) && $_GET['no_empresas']){
            $data['no_empresas'] = $_GET['no_empresas'];
        }

        return $data;
    }

    public function get_segundo_banner(){
        $this->CI->load->model('banners_model');

        switch($this->CI->session->userdata('id_sucursal')){
            case 1:
                $banners_sitio2 = $this->CI->banners_model->views(123);
                break;
            case 2:
                $banners_sitio2 = $this->CI->banners_model->views(179);
                break;
            case 3:
                $banners_sitio2 = $this->CI->banners_model->views(208);
                break;
            case 4:
                $banners_sitio2 = $this->CI->banners_model->views(407); // 407 es el id en sys_secciones de "Guia de Empresas", que est� relacionado con la sucursal 4 en sys_rel_secciones_portales
            break;
            case 6:
                $banners_sitio2 = $this->CI->banners_model->views(490);
            break;
            case 5:
                $banners_sitio2 = $this->CI->banners_model->views(559);
            break;
            case 7:
                $banners_sitio2 = $this->CI->banners_model->views(707);
            break;
            case 8:
                $banners_sitio2 = $this->CI->banners_model->views(889);
            break;
            case 10:
                $banners_sitio2 = $this->CI->banners_model->views(890);
            break;
            case 11:
                $banners_sitio2 = $this->CI->banners_model->views(836);
            break;
            case 12:
                $banners_sitio2 = $this->CI->banners_model->views(888);
            break;
        }

        return isset($banners_sitio2)&&$banners_sitio2?$banners_sitio2:NULL;
    }

    public function get_filtros_aplicados($filtros = '', $tipo = 1, $id_rubro, $todos_los_productos = FALSE){
        $this->CI->load->helper('common_helper');

        //no hay ningun filtro aplicado
        if (empty($filtros)){
            return 0;
        }

        //proceso las opciones
        $opciones = explode('&',urldecode($filtros));
        $opciones = doubleExplode('|', '-', $opciones[0]);

        $ids = '';
        foreach ($opciones as $opcs){
            foreach ($opcs as $o){
                if (!empty($o)){
                    $ids .= $o.',';
                }
            }
        }

        switch($tipo){
            //proveedores
            case 1:
                $tabla = 'site_proveedores_activos_filtros';
                break;
            //pyps
            case 2:
                $tabla = 'site_productos_activos_filtros';
                break;
            //paquetes
            case 3:
                $tabla = 'site_paquetes_activos_filtros';
                break;
        }

        if ($ids){
            $sql = 'SELECT DISTINCT f.id_filtro, f.filtro, f.id_opcion, f.opcion, f.excluyente
                        FROM '.$tabla.' f
                        WHERE f.id_opcion IN ('.substr($ids,0,-1).')
                        AND f.id_rubro = '.$id_rubro.'
                        ORDER BY f.orden_filtro, f.orden_opcion';

            $res_2 = array();

            /*
            if($todos_los_productos){
                $ids_zona = '';
                $tmp = explode(',',$ids);

                foreach ($tmp as $v){
                    if(in_array($v,array(526, 4355, 4358, 4354, 4356, 4357))){
                        $ids_zona .= $v.',';
                    }
                }

                $sql_zona = 'SELECT DISTINCT f.id_filtro, f.filtro, f.id_opcion, f.opcion, f.excluyente
                        FROM site_proveedores_activos_filtros f
                        WHERE f.id_opcion IN ('.substr($ids_zona,0,-1).')
                        AND f.id_rubro = '.$id_rubro.'
                        ORDER BY f.orden_filtro, f.orden_opcion';

                $query_2 = $this->CI->db->query($sql_zona);
                $res_2 = $query_2->result_array();
            } */

            $query = $this->CI->db->query($sql);
            $res = $query->result_array();

            /*
            if($res_2){
                $res = array_merge($res, $res_2);
            }*/

            return $res;
        } else {
            return array();
        }

    }

    public function get_fotos($id_padre = 0, $cantidad = 0, $pagina = 1, $filtros = '', $tipo = 0, $orden = 0){
        $this->CI->load->library('parseo_library');

        $pagina = ($pagina<1)? 1 : $pagina;
        $cantidad = ($cantidad<0)? 25 : $cantidad;

        $and = '';

        switch($orden){
            case 0:
                if($tipo == 2){
                    $criterio_orden = 'sat.id_tipo ASC, srmf.orden ASC, srmf.id DESC';
                }else{
                    $criterio_orden = '10, 11 DESC';
                }
                break;
            case 1:
                $criterio_orden = '10 DESC, 11';
                break;
            case 2:
                $criterio_orden = '10, 11';
                break;
            case 3:
                $criterio_orden = '10 DESC, 11 DESC';
                break;
            case 4:
                $criterio_orden = 'p.proveedor';
                break;
            case 5:
                $criterio_orden = 'p.proveedor DESC';
                break;
            case 6:
                $criterio_orden = 'p.id_minisitio';
                break;
            case 7:
                $criterio_orden = 'p.id_minisitio DESC';
                break;
            case 8:
                $criterio_orden = 'p.id_proveedor';
                break;
            case 9:
                $criterio_orden = 'p.id_proveedor DESC';
                break;
            case 10:
                $criterio_orden = 'imagen';
                break;
            case 11:
                $criterio_orden = 'imagen DESC';
                break;
            case 12:
                $criterio_orden = 'p.breve';
                break;
            case 13:
                $criterio_orden = 'p.breve DESC';
                break;
            case 14:
                $criterio_orden = 'sm.id';
                break;
            case 15:
                $criterio_orden = 'sm.id DESC';
                break;

        }

        switch($tipo){
            //solapa de un rubro
            case 0:
                $and .= ' AND p.id_rubro = '.$id_padre.' ';
                break;
            //galeria superior del rubro
            case 1:
                $and .= ' AND p.id_rubro = '.$id_padre.' AND srmr.orden < 3 ';
                break;
            //galeria del minisitio
            case 2:
                $and .= ' AND sm.estado IN (2,4) AND sm.eliminado = 0 AND srmf.estado >= 2 AND srmf.id_padre = '.$id_padre.' AND srmf.id_referencia = 5 AND referencia_owner = 1 AND srmf.orden<=50';
                break;
        }

        if (!empty($filtros)){
            $opc = $this->get_filtros_aplicados($filtros, 1 ,$id_padre);
            foreach($opc as $o){
                $and .= ' AND p.id_minisitio IN (SELECT DISTINCT id_minisitio FROM site_proveedores_activos_filtros WHERE id_opcion = '.$o['id_opcion'].') ';
            }
        }

        if($tipo == 4){
            $sql = 'SELECT SQL_CALC_FOUND_ROWS sm.id, sm.titulo, sm.descripcion, p.proveedor, pm.id, pm.id_proveedor, CONCAT(sm.name,"_chica.",sat.extension) imagen, CONCAT(sm.name,"_grande.",sat.extension) imagen_gde, pm.breve, srmr.orden, p.id_rubro, pr.rubro, pr.id_sucursal, suc.sucursal, spa.zonas, pm.short_url subdominio
                    FROM prov_proveedores p
                    JOIN prov_minisitios pm ON pm.id_proveedor = p.id
                    JOIN prov_rubros pr ON pr.id = p.id_rubro
                    JOIN sys_sucursales suc ON suc.id = pr.id_sucursal
                    JOIN sys_rel_medias_referencias srmr ON pm.id = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden <= 50  AND srmr.estado = 2
                    JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = pm.id_proveedor
                    JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
                    JOIN site_proveedores_activos spa ON spa.id_proveedor = p.id
                    WHERE 1 AND pm.id = '.$id_padre.'
                    GROUP BY sm.id ORDER BY orden,sm.id DESC LIMIT '.($pagina-1)*$cantidad.','.$cantidad;

        }elseif($tipo == 2){
            $sql = 'SELECT sm.id
                    FROM sys_medias sm
                    LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
                    LEFT JOIN sys_rel_medias_referencias srmf ON sm.id = srmf.id_media  AND srmf.estado = 2
                    LEFT JOIN site_proveedores_activos p ON p.id_proveedor = sm.id_owner
                    WHERE 1 AND sm.estado IN (2,4) AND sm.eliminado = 0 AND srmf.estado >= 2 AND srmf.id_padre = '.$id_padre.' AND srmf.id_referencia = 5 AND referencia_owner = 1 AND sat.extension IS NOT NULL
                    ORDER BY '.$criterio_orden.'
                    LIMIT 50';
            $query = $this->CI->db->query($sql);
            $ret = $query->result_array();
            foreach($ret as $r){
                $ids[] = $r["id"];
            }
            if(is_array($ids)) $ids = implode(",",$ids);

            if(!$ids) return;

            $sql = 'SELECT SQL_CALC_FOUND_ROWS
                    sm.id, sm.titulo, sm.descripcion,
                    p.proveedor, p.id_minisitio, p.id_proveedor, p.breve, p.orden, p.rubro, p.id_sucursal, p.sucursal, p.nivel,
                    CONCAT(sm.name,"_chica.",sat.extension) imagen,
                    CONCAT(sm.name,"_grande.",sat.extension) imagen_gde,
                    srmf.orden,
                    pm.short_url subdominio
                    FROM sys_medias sm
                    LEFT JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
                    LEFT JOIN sys_rel_medias_referencias srmf ON sm.id = srmf.id_media  AND srmf.estado = 2
                    LEFT JOIN site_proveedores_activos p ON p.id_proveedor = sm.id_owner
                    LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
                    WHERE 1 AND sm.estado IN (2,4) AND sm.eliminado = 0 AND srmf.estado >= 2 AND srmf.id_padre = '.$id_padre.' AND srmf.id_referencia = 5 AND referencia_owner = 1 AND sm.id IN ('.$ids.')
                    HAVING imagen IS NOT NULL
                    ORDER BY 9, ' . $criterio_orden . (($cantidad == 0)? '' : ' LIMIT ' . ($pagina - 1) * $cantidad . ',' . $cantidad);

        }else{

            $sql = 'SELECT SQL_CALC_FOUND_ROWS
                        sm.id, sm.titulo, sm.descripcion, p.proveedor, p.id_minisitio, p.id_proveedor, CONCAT(sm.name,"_chica.",sat.extension) imagen, p.breve, srmr.orden, MIN(p.nivel), p.orden, p.id_rubro, p.rubro, p.id_sucursal, p.sucursal, CONCAT(sm.name,"_grande.",sat.extension) imagen_gde, p.zonas, pm.short_url subdominio
                    FROM site_proveedores_activos p
                    JOIN sys_rel_medias_referencias srmr ON p.id_minisitio = srmr.id_padre AND srmr.id_referencia = 5 AND srmr.orden < 43 AND srmr.estado = 2
                    JOIN sys_medias sm ON srmr.id_media = sm.id AND sm.estado IN (2,4) AND sm.eliminado = 0 AND sm.referencia_owner = 1 AND sm.id_owner = p.id_proveedor
                    JOIN sys_archivos_tipo sat ON sm.id_tipo = sat.id AND sat.id_tipo = 1
                    LEFT JOIN prov_minisitios pm ON pm.id = p.id_minisitio
                    WHERE 1
                    '.$and.'
                    GROUP BY sm.id
                    ORDER BY 9, ' . $criterio_orden . (($cantidad == 0) ? '' : ' LIMIT ' . ($pagina-1) * $cantidad . ',' . $cantidad);
        }

        $query = $this->CI->db->query($sql);
        $ret = $query->result_array();

        $arr = array();
        if(!empty($ret)) foreach ($ret as $k => $el){
            $arr[$k] = $el;
            $arr[$k]['rubro_seo'] = ($el['rubro']) ? $this->CI->parseo_library->clean_url($el['rubro']) : null;
        }
        $ret = $arr;

        $query2 = $this->CI->db->query('SELECT FOUND_ROWS() rows');
        $this->resultados = $query2->row()->rows;

        return $ret;
    }

    public function get_tipo_listado($tipo_listado, $param){
        if(!$tipo_listado) return NULL;

        $orden = (!empty($_GET['orden']))? $_GET['orden'] : 'nivel';
        $meta_keywords = '';
        switch ($tipo_listado){
            case 'empresas':
                $this->CI->load->model('rubros_model');
                $this->CI->load->model('proveedores_model');

                $filtros_pendientes = $this->CI->rubros_model->get_rubro_filtros_pendientes($param['id_rubro'], $param['filtros']);
                $filtros_aplicados  = $this->get_filtros_aplicados($param['filtros'],1,$param['id_rubro']); //FILTRO SELECCIONADOS
                $proveedores = $this->CI->proveedores_model->get_proveedores($param['id_rubro'], $orden, $param['cantidad'], $param['pagina'], $param['filtros']);

                if (!$proveedores){
                    if(!isset($_GET["debug"])) show_404();
                }

                $resultados          = $this->CI->proveedores_model->resultados;
                $fotos               = $this->get_fotos($param['id_rubro'], $param['cantidad_galeria'] * 5, 0, $param['filtros'], 1, $param['rand']);
                $resultados_fotos    = $this->resultados;
                $paginar             = $mostrar_cantidad_filtros = 1;
                $pres_origen_grupal  = 11;
                $title               = 'Listado';
                $descripcion         = 'Te facilitamos nuestro listado de opciones de';
                $descripcion_2       = '. Aquí las encontrás fácil y rápido!';

                foreach($proveedores as $mp){
                    $meta_keywords .= ', '.$mp['proveedor'];
                }

                $ret = array(
                    'orden'                     => $orden,
                    'filtros_pendientes'        => $filtros_pendientes,
                    'filtros_aplicados'         => $filtros_aplicados,
                    'proveedores'               => $proveedores,
                    'paginar'                   => $paginar,
                    'resultados'                => $resultados,
                    'fotos'                     => $fotos,
                    'resultados_fotos'          => $resultados_fotos,
                    'paginar'                   => $paginar,
                    'mostrar_cantidad_filtros'  => $mostrar_cantidad_filtros,
                    'pres_origen_grupal'        => $pres_origen_grupal,
                    'title'                     => $title,
                    'meta_keywords'             => $meta_keywords,
                    'view'                      => "listado",
                    'descripcion'               => $descripcion,
                    'descripcion_2'             => $descripcion_2,
                    'elemento_url'              => '',
                    'id_tipo_formulario'        => 11
                );
                break;

            case 'promociones':
                $this->CI->load->model('productos_model');

                $this->CI->productos_model->pyp_filtros_extendido = 1; //-- Parametro q habilita el filtro de zonas en la solapa de Promociones --//

                $filtros_pendientes = $this->CI->productos_model->get_pyp_filtros_pendientes($param['id_rubro'], 1, $param['filtros']);
                $filtros_aplicados  = $this->get_filtros_aplicados($param['filtros'],2,$param['id_rubro']); //FILTRO SELECCIONADOS

                ##-- Filtros por Zonas --##
                $filtros_aplicados_zona = $this->get_filtros_aplicados($param['filtros'],1,$param['id_rubro']);
                if($filtros_aplicados_zona){
                    foreach($filtros_aplicados_zona as $item){
                        array_push($filtros_aplicados,$item);
                    }
                }
                ##-- --##
                $productos = $this->CI->productos_model->get_pyp($param['id_rubro'], 1, $orden, $param['cantidad'], $param['pagina'], $param['filtros']);

                if (!$productos){
                    if(!isset($_GET["debug"])) show_404();
                }

                $resultados = $this->CI->productos_model->resultados;
                $paginar = $mostrar_cantidad_filtros = 1;
                $title = 'Promociones';
                $descripcion = 'Todas las promociones de';
                $descripcion_2 = ' en un solo lugar! Organizar tu casamiento nunca fue tan fácil!';
                $tipo_producto = 'promocion';

                foreach($filtros_pendientes as $fp){
                    $meta_keywords .= ', '.$fp['filtro'];
                    foreach ($fp['opciones'] as $op){
                        $meta_keywords .= ', '.$op['opcion'];
                    }
                }

                $ret = array(
                    'orden'                     => $orden,
                    'filtros_pendientes'        => $filtros_pendientes,
                    'filtros_aplicados'         => $filtros_aplicados,
                    'productos'                 => $productos,
                    'paginar'                   => $paginar,
                    'resultados'                => $resultados,
                    'paginar'                   => $paginar,
                    'mostrar_cantidad_filtros'  => $mostrar_cantidad_filtros,
                    'title'                     => $title,
                    'descripcion'               => $descripcion,
                    'descripcion_2'             => $descripcion_2,
                    'meta_keywords'             => $meta_keywords,
                    'tipo_producto'             => $tipo_producto,
                    'view'                      => "listado_promociones",
                    'elemento_url'              => 'promocion',
                    'id_tipo_formulario'        => 90
                );
                break;

            case 'productos':
                $this->CI->load->model('productos_model');

                $this->CI->productos_model->pyp_filtros_extendido = 1; //-- Parametro q habilita el filtro de zonas en la solapa de Promociones --//
                $filtros_pendientes = $this->CI->productos_model->get_pyp_filtros_pendientes($param['id_rubro'], 0, $param['filtros']);
                $filtros_aplicados  = $this->get_filtros_aplicados($param['filtros'],2,$param['id_rubro']); //FILTRO SELECCIONADOS

                ##-- Filtros por Zonas --##
                $filtros_aplicados_zona = $this->get_filtros_aplicados($param['filtros'],1,$param['id_rubro']);
                if($filtros_aplicados_zona){
                    foreach($filtros_aplicados_zona as $item){
                        array_push($filtros_aplicados,$item);
                    }
                }
                ##-- --##

                $productos = $this->CI->productos_model->get_pyp($param['id_rubro'], 0, $orden, $param['cantidad'], $param['pagina'], $param['filtros']);

                if (!$productos){
                    if(!isset($_GET["debug"])) show_404();
                }

                $resultados = $this->CI->productos_model->resultados;
                $paginar = $mostrar_cantidad_filtros = 1;
                $title = 'Productos';
                $descripcion = 'Te facilitamos nuestro listado de opciones de';
                $descripcion_2 = '. Aquí las encontrás fácil y rápido!';
                $tipo_producto = 'producto';

                foreach($filtros_pendientes as $fp){
                    $meta_keywords .= ', '.$fp['filtro'];
                    foreach ($fp['opciones'] as $op){
                        $meta_keywords .= ', '.$op['opcion'];
                    }
                }

                $ret = array(
                    'orden'                     => $orden,
                    'filtros_pendientes'        => $filtros_pendientes,
                    'filtros_aplicados'         => $filtros_aplicados,
                    'productos'                 => $productos,
                    'paginar'                   => $paginar,
                    'resultados'                => $resultados,
                    'mostrar_cantidad_filtros'  => $mostrar_cantidad_filtros,
                    'title'                     => $title,
                    'descripcion'               => $descripcion,
                    'descripcion_2'             => $descripcion_2,
                    'meta_keywords'             => $meta_keywords,
                    'tipo_producto'             => $tipo_producto,
                    'view'                      => "listado_productos",
                    'elemento_url'              => 'producto',
                    'id_tipo_formulario'        => 91
                );
                break;

            case 'paquetes':
                $this->CI->load->model('paquetes_model');
                $this->CI->load->library('parseo_library');

                $filtros_pendientes = $this->CI->paquetes_model->get_filtros_pendientes($param['id_rubro'], $param['filtros']);
                $filtros_aplicados  = $this->get_filtros_aplicados($param['filtros'], 3, $param['id_rubro']); //FILTRO SELECCIONADOS

                $productos = $this->CI->paquetes_model->get_paquetes($param['id_rubro'], $orden, $param['cantidad'], $param['pagina'], $param['filtros'], 0, ''); // SI SE USARAN LOS FILTROS PRIMARIOS, HAY QUE AGREGARLE UN PARAMETRO MAS ', $filtros_primarios'


                if (!$productos){
                    if(!isset($_GET["debug"])) show_404();
                }
                $resultados = $this->CI->paquetes_model->resultados;
                $paginar = $mostrar_cantidad_filtros = 1;
                $show_paquete_detalle = 1;
                $title = 'Paquetes';
                $descripcion = 'Todos los paquetes de';
                $descripcion_2 = ' en un solo lugar! Organizar tu casamiento nunca fue tan fácil!';
                $tipo_producto = 'paquete';
                $iconos = $this->CI->parseo_library->equivalencia_iconos();

                foreach($filtros_pendientes as $fp){
                    $meta_keywords .= ', '.$fp['filtro'];
                    foreach ($fp['opciones'] as $op){
                        $meta_keywords .= ', '.$op['opcion'];
                    }
                }

                $ret = array(
                    'orden'                     => $orden,
                    'filtros_pendientes'        => $filtros_pendientes,
                    'filtros_aplicados'         => $filtros_aplicados,
                    'productos'                 => $productos,
                    'paginar'                   => $paginar,
                    'resultados'                => $resultados,
                    'mostrar_cantidad_filtros'  => $mostrar_cantidad_filtros,
                    'title'                     => $title,
                    'descripcion'               => $descripcion,
                    'descripcion_2'             => $descripcion_2,
                    'meta_keywords'             => $meta_keywords,
                    'tipo_producto'             => $tipo_producto,
                    'view'                      => "listado_paquetes",
                    'iconos_rubros'             => $iconos,
                    'elemento_url'              => 'paquete',
                    'id_tipo_formulario'        => 89
                );
                break;
            case 'mapa':
                $this->CI->load->model('rubros_model');
                $this->CI->load->model('proveedores_model');

                $filtros_pendientes = $this->CI->rubros_model->get_rubro_filtros_pendientes($param['id_rubro'], $param['filtros'], 1);
                $filtros_aplicados = $this->get_filtros_aplicados($param['filtros'], 1, $param['id_rubro']); //FILTROS SELECCIONADOS
                $proveedores = $this->CI->proveedores_model->get_proveedores($param['id_rubro'], $orden, $param['cantidad'], $param['pagina'], $param['filtros'], 1);

                if (!$proveedores){
                    if(!isset($_GET["debug"])) show_404();
                }

                $resultados = $this->CI->proveedores_model->resultados;
                $mostrar_cantidad_filtros = 1;
                $pres_origen_grupal = 24;
                $title = 'Mapa';
                $descripcion = 'Todas las opciones de';
                $descripcion_2 = ' ordenadas en un mapa. Organizar tu casamiento nunca fue tan fácil!';

                // foreach($proveedores as $mp){
                //  $meta_keywords .= ', '.$mp['proveedor'];
                // }

                $ret = array(
                    'orden'                     => $orden,
                    'filtros_pendientes'        => $filtros_pendientes,
                    'filtros_aplicados'         => $filtros_aplicados,
                    'proveedores'               => $proveedores,
                    'pres_origen_grupal'        => $pres_origen_grupal,
                    'resultados'                => $resultados,
                    'mostrar_cantidad_filtros'  => $mostrar_cantidad_filtros,
                    'title'                     => $title,
                    'descripcion'               => $descripcion,
                    'descripcion_2'             => $descripcion_2,
                    'meta_keywords'             => $meta_keywords,
                    'view'                      => "listado_mapa",
                    'elemento_url'              => 'mapa',
                    'id_tipo_formulario'        => 24
                );
                break;
            case 'fotos':
                $this->CI->load->model('rubros_model');

                $filtros_pendientes = $this->CI->rubros_model->get_rubro_filtros_pendientes($param['id_rubro'], $param['filtros']);
                $filtros_aplicados  = $this->get_filtros_aplicados($param['filtros'],1,$param['id_rubro']); //FILTRO SELECCIONADOS
                $fotos = $this->get_fotos($param['id_rubro'], $param['cantidad'], $param['pagina'], $param['filtros']);
                if (!$fotos){
                    if(!isset($_GET["debug"])) show_404();
                }

                $resultados = $this->resultados;
                $vistas = 1;
                $paginar = 1;

                foreach($filtros_pendientes as $fp){
                    $meta_keywords .= ', '.$fp['filtro'];
                    foreach ($fp['opciones'] as $op){
                        $meta_keywords .= ', '.$op['opcion'];
                    }
                }

                $ret = array(
                    'filtros_pendientes'        => $filtros_pendientes,
                    'filtros_aplicados'         => $filtros_aplicados,
                    'fotos'                     => $fotos,
                    'resultados'                => $resultados,
                    'title'                     => 'Fotos',
                    'descripcion'               => 'Las mejores imágenes de',
                    'descripcion_2'             => ' están aquí. Entrá ahora y llenate de ideas!',
                    'vistas'                    => $vistas,
                    'paginar'                   => $paginar,
                    'meta_keywords'             => $meta_keywords,
                    'view'                      => "listado_fotos",
                    'elemento_url'              => 'foto',
                    'id_tipo_formulario'        => 92
                );
                break;
        }

        return $ret;
    }

    public function get_proveedores_library($id_rubro, $orden='nivel', $cantidad=0, $pagina=1, $filtros='', $solo_mapa=0, $combo=0){
        $this->CI->load->model('proveedores_model');

        $res = $this->CI->proveedores_model->get_proveedores($id_rubro, $orden, $cantidad, $pagina, $filtros, $solo_mapa, $combo);

        return $res;
    }

    public function filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido){
        $this->CI->load->model('proveedores_model');
        $this->CI->load->library('parseo_library');

        if(empty($proveedor) && empty($_GET['hash'])){
            $proveedor = $this->CI->proveedores_model->get_proveedor_vencido($id_minisitio);
            ##-- Si el proveedor no se quiere ver como inactivo, lo redirecciono al rubro --##
            if(!$proveedor['mostrar_inactivo']){
                if($proveedor){
                    $url_bien = '/proveedores/listado_empresas/'.$proveedor["id_rubro"];
                    redirect(base_url($url_bien));
                }else{
                    show_404();
                }

            }
            ##-- --##
            $prov_vencido = 1;
        }

        if (!empty($_GET['hash'])){
            $proveedor = $this->CI->proveedores_model->get_proveedor_hash($id_minisitio,$_GET['hash']); // piso la variable $proveedor con la info de vista previa
        }elseif(!$proveedor){
            redirect(base_url('/'));
        }

        if (!$proveedor){
            show_404();
        }else{
            $this->proveedor_vencido = $proveedor;
        }

        return $prov_vencido;
    }

    public function validate_formulario($post, $redirect = TRUE){
        $this->CI->load->library('form_validation');

        $error_required = array(
                'required'      => 'Debe ingresar un %s.',
        );

        $this->CI->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
        $this->CI->form_validation->set_rules('apellido', 'Apellido', 'required', $error_required);
        $this->CI->form_validation->set_rules('dia_evento', 'Día del evento', 'required', $error_required);
        $this->CI->form_validation->set_rules('email', 'E-mail', 'required', array(
                'required'      => 'Debe ingresar un %s.'
        ));

        if(!empty($post['id_origen'])){
            if($post['id_origen'] != '46' && $post['id_origen'] != '47') $this->CI->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
        }

        if(isset($post['id_origen'])){
            if($post['id_origen'] != '45' && $post['id_origen'] != '46' && $post['id_origen'] != '47'){
                $this->CI->form_validation->set_rules('id_provincia', 'Provincia', 'required', array(
                        'required'      => 'Debe ingresar una %s.',
                ));
                $this->CI->form_validation->set_rules('id_localidad', 'Localidad', 'required', array(
                        'required'      => 'Debe ingresar una %s.',
                ));
            }
        }

        if($this->CI->form_validation->run() == TRUE){
            if($redirect){
                $this->CI->session->set_flashdata('post', $post);
                redirect(base_url('/formulario/procesar_pedido/'));
            }else{
                return TRUE;
            }
        }
    }

    public function get_tipo_pyp($tipo, $param, $no_filtro_zonas = FALSE){
        $orden = 'nivel';

        switch ($tipo){
            case 'productos':

                $this->CI->load->model('productos_model');

                $filtros_pendientes = $this->CI->productos_model->get_pyp_filtros_pendientes($param['id_rubro'], 0, $param['filtros'], $no_filtro_zonas);
                $productos = $this->CI->productos_model->get_pyp($param['id_rubro'], 0, $orden, $param['cantidad'], $param['pagina'], $param['filtros'], 0, 0, '', $param['pyp_id_sucursal'], TRUE, $param['id_grupo']);

                if (!$productos){
                    show_404();
                }

                $resultados = $this->CI->productos_model->resultados;
                $paginar = 1;
                $mostrar_cantidad_filtros = 1;
                $promo = 0;

                return array(
                    'productos' => $productos,
                    'resultados' => $resultados,
                    'paginar' => $paginar,
                    'mostrar_cantidad_filtros' => $mostrar_cantidad_filtros,
                    'promo' => $promo,
                    'tipo_producto' => 'producto',
                    'orden' => $orden,
                    'filtros_pendientes' => $filtros_pendientes
                );

                break;

            case 'promociones':

                $this->CI->load->model('productos_model');

                $filtros_pendientes = $this->CI->productos_model->get_pyp_filtros_pendientes($param['id_rubro'], 1, $param['filtros']);
                $productos = $this->CI->productos_model->get_pyp($param['id_rubro'], 1, $orden, $param['cantidad'], $param['pagina'], $param['filtros'], 0, 0, '', $param['pyp_id_sucursal'], FALSE, $param['id_grupo']);

                if (!$productos){
                    show_404();
                }

                $resultados = $this->CI->productos_model->resultados;
                $paginar = 1;
                $mostrar_cantidad_filtros = 1;
                $promo = 1;

                return array(
                    'productos' => $productos,
                    'resultados' => $resultados,
                    'paginar' => $paginar,
                    'mostrar_cantidad_filtros' => $mostrar_cantidad_filtros,
                    'promo' => $promo,
                    'tipo_producto' => 'promocion',
                    'orden' => $orden,
                    'filtros_pendientes' => $filtros_pendientes
                );

            case 'paquetes':

                $this->CI->load->model('paquetes_model');

                $filtros_pendientes = $this->CI->paquetes_model->get_filtros_pendientes($param['id_rubro'], $param['filtros']);
                $paquetes = $this->CI->paquetes_model->get_paquetes($param['id_rubro'], $orden, $param['cantidad'], $param['pagina'], $param['filtros'], 0, '', array('id_sucursal' => $param['pyp_id_sucursal']), $param['id_grupo']);

                if (!$paquetes){
                    show_404();
                }

                $resultados = $this->CI->paquetes_model->resultados;
                $paginar = 1;
                $mostrar_cantidad_filtros = 1;
                $promo = NULL;

                return array(
                    'productos' => $paquetes,
                    'resultados' => $resultados,
                    'paginar' => $paginar,
                    'promo' => $promo,
                    'mostrar_cantidad_filtros' => $mostrar_cantidad_filtros,
                    'tipo_producto' => 'paquete',
                    'orden' => $orden,
                    'filtros_pendientes' => $filtros_pendientes
                );

                break;
        }
    }

    public function item_vencido($id, $tipo_parametro){
        if($tipo_parametro == 'paquete'){
            $this->CI->load->model('paquetes_model');
            $elemento = 'Paquete';
            $item = $this->CI->paquetes_model->get_paquete_vencido($id);
            if($item){
                // $url_bien = '/paquetes-para-casamientos-en-'.$comercial->clean_url_3($item['sucursal']).'/'.$comercial->clean_url_3(trim($item['titulo'])).'/'.$comercial->clean_url_3($item['id_paquete']);

                $item['id_producto'] = $item['id_paquete'];
                $item['tipo'] = 'paquetes';
                $tipo = 'paquetes';
                $pre_titulo = 'Otros';
            }
        }else{
            $this->CI->load->model('productos_model');
            $item = $this->CI->productos_model->get_producto_vencido($id);
            if($item){
                // $url_bien = '/'.$item['tipo']."-para-casamientos-en-".$comercial->clean_url_3($item['sucursal']).'/'.$comercial->clean_url_3(trim($item['titulo'])).'/'.$comercial->clean_url_3($item['id_producto']);

                $dm_pagos = doubleExplodeFull (';', ',', $item['dm_pagos']);
                $item['cuotas'] = $dm_pagos[0][count($dm_pagos[0])-1];

                $elemento = ($item['promocion'])? 'Promocion' : 'Producto';
                $tipo = ($item['promocion'])? 'promociones' : 'productos';
                $pre_titulo = ($item['promocion']) ? 'Otras' : 'Otros';
            }
        }

        return array(
            'elemento'   => $elemento,
            'item'       => $item,
            'tipo'       => $tipo,
            'pre_titulo' => $pre_titulo
        );
    }

    public function get_info_home(){
        $this->CI->load->model('banners_model');

        $id_seccion_prov = NULL;
        $banners = array();
        switch($this->CI->session->userdata('id_sucursal')){
            case 1:
                $banners = $this->CI->banners_model->views(126);
                $id_seccion_prov = 126;
                break;
            case 2:
                $banners = $this->CI->banners_model->views(379);
                $id_seccion_prov = 379;
                break;
            case 3:
                $banners = $this->CI->banners_model->views(380);
                $id_seccion_prov = 380;
                break;
            case 4:
                $banners = $this->CI->banners_model->views(406);
                $id_seccion_prov = 406;
                break;
            case 6:
                $banners = $this->CI->banners_model->views(546);
                $id_seccion_prov = 546;
                break;
            case 5:
                $banners = $this->CI->banners_model->views(560);
                $id_seccion_prov = 560;
            break;
            case 7:
                $banners = $this->CI->banners_model->views(707);
                $id_seccion_prov = 707;
            break;
            case 8:
                $banners = $this->CI->banners_model->views(941);
                $id_seccion_prov = 941;
            break;
            case 10:
                $banners = $this->CI->banners_model->views(942);
                $id_seccion_prov = 942;
            break;
            case 11:
                $banners = $this->CI->banners_model->views(836);
                $id_seccion_prov = 836;
            break;
            case 12:
                $banners = $this->CI->banners_model->views(994);
                $id_seccion_prov = 994;
            break;
        }

        return array('id_seccion_prov' => $id_seccion_prov, 'banners' => $banners);
    }

    public function diccionario_alternativas($term){
        $term = strtolower($term);
        $alternativas = array();

        $alternativas['salones de fiestas'] = 'salones de fiesta';
        $alternativas['ajuar de novias'] = 'ajuar';
        $alternativas['quinta y estancia'] = 'quintas y estancias';
        $alternativas['salon de fiesta en uruguay'] = 'salones de fiesta en uruguay';
        $alternativas['salón de fiesta en uruguay'] = 'salones de fiesta en uruguay';
        $alternativas['salon de hotel'] = 'salones de hotel';
        $alternativas['salón de hotel'] = 'salones de hotel';
        $alternativas['vestido de novia'] = 'vestidos de novia';
        $alternativas['vestido usado'] = 'vestidos usados';
        $alternativas['alquiler de carpa'] = 'alquiler de carpas';
        $alternativas['auto para casamiento'] = 'autos para casamientos';
        $alternativas['cabina de mensaje'] = 'cabinas de mensajes';
        $alternativas['propuesta original'] = 'propuestas originales';
        $alternativas['tratamiento de belleza'] = 'tratamientos de belleza';
        $alternativas['traje de etiqueta'] = 'trajes de etiqueta';
        $alternativas['agencia de viaje'] = 'agencias de viaje';

        return !empty($alternativas[$term]) ?  $alternativas[$term] : '';
    }
}