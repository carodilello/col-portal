<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galerias extends MY_Controller {
	public function __construct(){
		parent::__construct();
        $this->load->library('config_library');
	}

	public function index($id_tipo = 0, $id_seccion = 0, $id_subseccion = 0, $id_sucursal = 0, $id_rubro = 0, $pag = 0, $rubro = ''){
        $this->load->model('media_model');
        $this->load->model('banners_model');
        $this->load->model('rubros_model');

        $this->load->library('parseo_library');

        $this->load->helper('common_helper');

        $filtros = array();

        if (!empty($id_seccion)){
            $filtros['ubicacion'] = 2;
        }else{
            $filtros['ubicacion'] = 4;
        }

        if(!empty($id_rubro)){
            $filtros['rubro'] = $id_rubro;
        }

        if(!empty($id_subseccion)){
            $seccion_id = $id_subseccion;
        }elseif(!empty($id_seccion)){
            $seccion_id = $id_seccion;
        }

        $rows = 18;
        
        $pag = !empty($_GET['pag']) ? $_GET['pag'] : 0;

        $galerias_all = $this->media_model->get_galerias(isset($seccion_id)&&$seccion_id?$seccion_id:NULL, $filtros, 0);
        $tot = count($galerias_all);
        $galerias = $this->media_model->get_galerias(isset($seccion_id)&&$seccion_id?$seccion_id:NULL, $filtros, ((!empty($pag)?$pag-1:0)*$rows), $rows);

        $galerias_destacadas = array_filter($galerias_all, "enUbicacion");

        $this->load->library('pagination');

        $url_para_base = '/fotos-casamiento' . ($id_tipo ? ('/' . $id_tipo) : '') . ($id_seccion ? ('/' . $id_seccion) : '') . ($id_subseccion ? ('/' . $id_subseccion) : '') . ($id_sucursal ? ('/' . $id_sucursal) : '') . ($id_rubro ? ('/' . $id_rubro) : '');
        if($id_rubro) $url_para_base = '/fotos-casamiento/' . $id_sucursal . '/' . $rubro . '_CO_r' . $id_rubro;

        $galeria_subseccion = $this->navegacion_model->get_secciones(!empty($id_seccion) ? $id_seccion : 76);

        $arr = array();
        if(!empty($galeria_subseccion)) foreach ($galeria_subseccion as $k => $el) {
            $arr[$k]['nombre'] = $el;
            $arr[$k]['nombre_seo'] = $this->parseo_library->clean_url($el);
        }
        $galeria_subseccion = $arr;

        $rubros = $this->navegacion_model->get_rubros_combo($id_sucursal ? $id_sucursal : $this->session->userdata('id_sucursal'));
        $rubros_original = $this->rubros_model->get_listado_rubros_guia($id_sucursal, 0, 1, 'r2.orden, r1.rubro', 1);
        $rubros = $this->parseo_library->rubros_para_seo($rubros);

        $title = 'Galería de Fotos e Imágenes Casamientos y Bodas | Casamientos Online';
        $descripcion = 'Fotos e imágenes geniales para que te llenes de ideas para tu casamiento! Date el gusto e ingresá a nuestra galería!';
        $keywords = 'Imágenes de Casamientos, casamiento, guía organización Casamiento, Boda, Civil, Fiesta, Evento, Buenos Aires, Rosario, Santa Fe, Córdoba, Mendoza, galerías de fotos, rubros casamientos, salones de fiesta, catering, participaciones, vestidos de novia, vestidos de fiesta, maquillaje, peinados, jornadas de casamientosonline, moda, tendencia, belleza';


        if($id_seccion){
            $secs = array(
                76 => 'Vestidos',
                81 => 'Belleza y Estilo',
                93 => 'La Organización',
                98 => 'Luna de Miel'
            );
            $title = "Fotos e Imágenes de " . $secs[$id_seccion] . " | Casamientos Online";
            $descripcion = "Fotos e imágenes de " . $secs[$id_seccion] . " para que te llenes de ideas para tu casamiento! Date el gusto e ingresá a nuestra galería!";
            if($id_subseccion){
                $title = "Fotos e Imágenes de " . $secs[$id_seccion] . " y " . $galeria_subseccion[$id_subseccion]['nombre'] . " | Casamientos Online";
                $descripcion = "Fotos e imágenes de " . $galeria_subseccion[$id_subseccion]['nombre'] . " y " . $secs[$id_seccion] . " para que te llenes de ideas para tu casamiento! Date el gusto e ingresá a nuestra galería!";
            }
        }

        $sucs = array();
        foreach ($this->config_library->sucursales as $key => $value) {
            $sucs[$value['id']] = $value;
        }

        if($id_sucursal && $id_rubro){
            $title       = 'Fotos e Imágenes de ' . $rubros_original[$id_rubro] . ' en ' . $sucs[$id_sucursal]['sucursal'] . ' | Casamientos Online';
            $descripcion = 'Fotos e imágenes de ' . $rubros_original[$id_rubro] . ' en ' . $sucs[$id_sucursal]['sucursal'] . ' para que te llenes de ideas para tu casamiento! Date el gusto e ingresá a nuestra galería!';
        }

        $banners = $this->banners_model->views(390);

        foreach ($galerias as $key => $gal){
            $imagen_tw = 'http://media.casamientosonline.com/images/' . $gal['pic_name'] . '.' . $gal['pic_extension'];
            if($imagen_tw) break;
        }

        if($id_rubro) $config['base_url'] = base_url('fotos-casamiento/' . $sucs[$id_sucursal]['nombre_seo'] . '/' . $this->parseo_library->clean_url($rubros_original[$id_rubro]) . '_CO_' . ($id_tipo ? ('t' . $id_tipo) : '') . ($id_sucursal ? ('_su' . $id_sucursal) : '') . ($id_rubro ? ('_r' . $id_rubro) : ''));

        if($id_tipo && $id_seccion)                   $config['base_url'] = base_url('fotos-casamiento/notas/' . $this->parseo_library->clean_url($secs[$id_seccion]) . '_CO_t' . $id_tipo . '_sc' . $id_seccion);
        if($id_tipo && $id_seccion && $id_subseccion) $config['base_url'] = base_url('fotos-casamiento/notas/' . $this->parseo_library->clean_url($galeria_subseccion[$id_subseccion]['nombre']) . '_CO_t' . $id_tipo . '_sc' . $id_seccion . '_sb' . $id_subseccion);

        $config['total_rows'] = $tot / $rows;
        $config['per_page'] = 1;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = 'pag';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
        $config['full_tag_close'] = '</div></div>';
        $config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
        $config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';

        $this->pagination->initialize($config);

        $canonical = explode('_CO_?pag', $_SERVER['REQUEST_URI']);

        if(!empty($canonical[0]) && count($canonical) > 1){
            $canonical = base_url($canonical[0]);
        }else{
            $canonical = explode('?pag', $_SERVER['REQUEST_URI']);
            if(!empty($canonical[0])) $canonical = base_url($canonical[0]);
        }

        $data = array(
            'banners'               => $banners,
            'canonical'             => $canonical,
            'galerias'              => $galerias,
            'galerias_destacadas'   => $galerias_destacadas,
            'galeria_subseccion'    => $galeria_subseccion,
            'get'                   => array(
                'id_tipo'               => $id_tipo,
                'id_seccion'            => $id_seccion,
                'id_subseccion'         => $id_subseccion,
                'id_sucursal'           => $id_sucursal,
                'id_rubro'              => $id_rubro
            ),
            'meta_descripcion'      => $descripcion,
            'meta_keywords'         => $keywords,
            'meta_title'            => $title,
            'rubros'                => $rubros,
            'twitter_image'         => $imagen_tw,
            'sucursal_redirect'     => $url_para_base
        );

        $data['twitter_title'] = $data['meta_title'];
        $data['twitter_descripcion'] = $data['meta_descripcion'];

        $this->load->view('galerias', $this->config_library->send_data_to_view($data));
    }

    public function detalle($id_galeria = NULL){
        $this->load->model('media_model');
        $this->load->model('banners_model');

        $galeria = $this->media_model->get_galeria(array('id_galeria' => $id_galeria));
        $galeria = $galeria[0];

        if(!$galeria['titulo_galeria']){
            $galeria = $this->media_model->get_galeria(array('id_minisitio' => $id_galeria));
            $galeria = $galeria[0];

            $galeria['titulo_galeria'] = $galeria['proveedor'];
        }

        if(empty($galeria['titulo']) && empty($galeria['thumbs'])) redirect(base_url('/fotos-casamiento'));

        foreach (explode('~',$galeria['proveedores']) as $p){
            $pp = explode('@',$p);
            $keywords = (isset($pp[1]) && $pp[1] ? ', ' .$pp[1] : '').(isset($pp[3]) && $pp[3] ? ', '.$pp[3] : '');
        }

        $title = 'Fotos e Imágenes de ' . $galeria['titulo_galeria'] . ' | Casamientos Online';
        $descripcion = 'Fotos e imágenes de ' . $galeria['titulo_galeria'] . ' para que te llenes de ideas para tu casamiento! Date el gusto e ingresá a nuestra galería!';
        $keywords = $galeria['titulo_galeria'].', Imágenes de Casamientos, casamiento, guía organización Casamiento, Boda, Civil, Fiesta, Evento, Buenos Aires, Rosario, Santa Fe, Córdoba, Mendoza'.$keywords;

        $banners = $this->banners_model->views(391);

        $data = array(
            'banners'           => $banners,
            'canonical'         => base_url($_SERVER['REQUEST_URI']),
            'galeria'           => $galeria,
            'meta_descripcion'  => $descripcion,
            'meta_keywords'     => $keywords,
            'meta_title'        => $title,
            'id_galeria'        => $id_galeria
        );

        $this->load->view('galerias_detalle', $this->config_library->send_data_to_view($data));
    }

    public function tags($id_tag = NULL){
        if(!$id_tag) redirect(base_url('/galerias'));

        $this->load->model('editorial_model');
        $this->load->model('media_model');
        $this->load->model('banners_model');

        $this->load->library('pagination');

        $filtros = array();

        if(isset($_GET)) $filtros = $_GET;

        // cantidad para la nube de tags
        $filtros_tag['having'] = 'cant >= 2';
        $filtros_tag['tipo_nota'] = 1415;
        $filtros_tag['tipo'] = 1;
        $filtros['id_tag'] = $id_tag;

        $tags = $this->editorial_model->get_tags($filtros_tag);
        $tag  = $this->editorial_model->get_tag($id_tag);
        $tag  = $tag['descripcion'];

        $galerias_all = $this->media_model->get_galerias(isset($seccion_id)&&$seccion_id?$seccion_id:NULL, $filtros, 0);
        $tot = count($galerias_all);
        $rows = 18;
        $galerias = $this->media_model->get_galerias(isset($seccion_id)&&$seccion_id?$seccion_id:NULL, $filtros, ((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']-1:0)*$rows), $rows);

        $title = 'Galería de Imágenes de Casamientos. Vestidos de Novia, la fiesta, belleza, tendencia y mucho más';
        $descripcion = $galerias[array_rand($galerias)]['descripcion'];
        if(!$descripcion) $descripcion = $galerias[array_rand($galerias)]['titulo'];
        $keywords = 'Imágenes de Casamientos, casamiento, guía organización Casamiento, Boda, Civil, Fiesta, Evento, Buenos Aires, Rosario, Santa Fe, Córdoba, Mendoza, galerías de fotos, rubros casamientos, salones de fiesta, catering, participaciones, vestidos de novia, vestidos de fiesta, maquillaje, peinados, jornadas de casamientosonline, moda, tendencia, belleza';

        $config['base_url'] = base_url('/galerias/tags/' . $id_tag);
        $config['total_rows'] = $tot / $rows;
        $config['per_page'] = 1;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'pag';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
        $config['full_tag_close'] = '</div></div>';
        $config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
        $config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';

        $this->pagination->initialize($config);

        $breadcrumbs = array(
            0 => array(
                'nombre' => 'Galerias',
                'url'   => '/galerias',
            )
        );
        $titulo = 'Galerias';
        $url_tag = '/fotos-casamiento/{?}_CO_t';

        $banners = $this->banners_model->views(390);

        $canonical = explode('?pag', $_SERVER['REQUEST_URI']);
        if(!empty($canonical[0])) $canonical = base_url($canonical[0]);

        $data = array(
            'banners'           => $banners,
            'breadcrumbs'       => $breadcrumbs,
            'canonical'         => $canonical,
            'galerias'          => $galerias,
            'meta_descripcion'  => $descripcion,
            'meta_keywords'     => $keywords,
            'meta_title'        => $title,
            'origen'            => 'galerias',
            'tag'               => $tag,
            'tags'              => $tags,
            'titulo'            => $titulo,
            'url_tag'           => $url_tag
        );

        $this->load->view('tags', $this->config_library->send_data_to_view($data));
    }
}