<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends MY_Controller {
	public function __construct(){
    	parent::__construct();
    	$this->load->library('config_library');
	}

	public function index(){
		redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));
	}

	public function guia(){
		$this->load->model('banners_model');
		$this->load->model('rubros_model');
		$this->load->model('proveedores_model');
		$this->load->model('generales_model');

		$this->config_library->set_seccion();

		$id_seccion = $this->config_library->id_seccion;

		$this->rubros_model->get_rubros_padre($this->session->userdata('id_sucursal'));
		$cantidad_consultados = 6;
		
		$banners 					 = $this->banners_model->views($id_seccion);

		$proveedores_destacados 	 = $this->proveedores_model->get_proveedores_destacados($id_seccion, 0);
		$rubros_listado 			 = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');

		$meta_rubros 				 = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r2.orden, r1.rubro', 1);
		$mas_consultados_proveedores = $this->generales_model->get_mas_consultados(1,$this->session->userdata('id_sucursal'),$cantidad_consultados);
		$mas_consultados_paquetes    = $this->generales_model->get_mas_consultados(2,$this->session->userdata('id_sucursal'),$cantidad_consultados);
		$mas_consultados_productos   = $this->generales_model->get_mas_consultados(3,$this->session->userdata('id_sucursal'),$cantidad_consultados);
		$mas_consultados_promociones = $this->generales_model->get_mas_consultados(4,$this->session->userdata('id_sucursal'),$cantidad_consultados);

		if($meta_rubros){
			$meta_keywords = "";
			foreach ($meta_rubros as $mr){
				$meta_keywords .= ', '.$mr;
			}
		}

		switch ($this->session->userdata('id_sucursal')) {
			case '1': // BUE
				$patron_orden = array(6,4,1,2,7,3,8,9,5);
				break;
			case '4': // COR
				$patron_orden = array(6,4,1,7,8,2,5,3);
				break;
			case '10': // ENT
				$patron_orden = array(4,1,5,2,3);
				break;
			case '7': // LA PLATA
				$patron_orden = array(4,1,5,2,3);
				break;
			case '6': // MARDEL
				$patron_orden = array(6,4,1,7,2,5,3);
				break;
			case '5': // MEN
				$patron_orden = array(5,4,1,6,2,3);
				break;
			case '12': // MIS
				$patron_orden = array(4,1,5,2,3);
				break;
			case '2': // ROS
				$patron_orden = array(6,4,1,7,8,9,2,5,3);
				break;
			case '11': // SALTA
				$patron_orden = array(4,1,5,2,3);
				break;
			case '3': // SANTA FE
				$patron_orden = array(3,4,1,5,2);
				break;
			case '8': // TUC
				$patron_orden = array(4,1,5,2,3);
				break;
		}

		$keys = array_keys($this->rubros_model->solapas);

		$count_patron = count($patron_orden);
		$count_solapas = count($this->rubros_model->solapas);

		if($count_patron != $count_solapas){
			if($count_patron > $count_solapas){
				$dif = $count_patron - $count_solapas;
				if($dif > 0) for ($i = 0; $i < $dif; $i++){
					array_pop($patron_orden);
				}
			}else{
				$dif = $count_solapas - $count_patron;
				if($dif > 0) for ($i = 0; $i < $dif; $i++){
					array_push($patron_orden, 1);
				}
			}
		}

		array_multisort($patron_orden, $this->rubros_model->solapas, $keys);

		$solapas = array_combine($keys,  $this->rubros_model->solapas);

		$data = array(
			'banners' 					  	  	=> $banners,
			'canonical'							=> base_url($_SERVER['REQUEST_URI']),
			'meta_descripcion' 				  	=> '¿Estás organizando tu fiesta de casamiento en ' . $this->config_library->sucursal['sucursal'] . '? No pierdas más tiempo! Encontrá aquí todas las opciones disponibles.',
			'meta_keywords' 				  	=> 'Casamientos Online, casamientos, novios, bodas, '.$this->config_library->sucursal['sucursal'].', Argentina'.$meta_keywords,
			'meta_title'					  	=> 'Proveedores para tu Fiesta en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online',
			'proveedores_destacados'          	=> $proveedores_destacados,
			'proveedores_destacados_cantidad' 	=> $this->proveedores_model->resultados,
			'proveedores_consultados' 		  	=> $mas_consultados_proveedores,
			'paquetes_consultados' 			  	=> $mas_consultados_paquetes,
			'productos_consultados' 		  	=> $mas_consultados_productos,
			'promociones_consultados' 		  	=> $mas_consultados_promociones,
			'rubros' 						  	=> $rubros_listado,
			'solapas'						  	=> $solapas,
			'sucursal_seo'						=> TRUE,
			'sucursal_redirect'					=> '/fiestas-de-casamiento'
		);

		$this->load->view('guia_empresas', $this->config_library->send_data_to_view($data));
	}

	// Esto es para cargar de forma forzada el mensaje de gracias
	public function gracias_externo($id_rubro = NULL, $param = 0, $tipo = 'empresas', $gracias_externo = NULL){
		if($id_rubro && !$param && $tipo == 'empresas' && $gracias_externo){

			$this->load->model('rubros_model');

			$this->session->set_flashdata('gracias', 1);
        	$this->session->set_flashdata('id_rubro', $id_rubro);

        	$rubro = $this->rubros_model->get_rubro($id_rubro);
        	if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
        	if(!$rubro) redirect(base_url('/'));

        	$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

        	redirect(base_url($this->config_library->sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_r' . $id_rubro . '#gracias'));
		}
	}

	public function listado_empresas($id_rubro = NULL, $param = 0, $tipo = 'empresas'){
		if(!$id_rubro || !is_numeric($id_rubro)) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));
		if(isset($param)&&$param) $_GET["param"] = urldecode($param);

		$this->load->driver('cache');

		$this->load->model('stats_model');
		$this->load->model('banners_model');
		$this->load->model('rubros_model');
		$this->load->model('visitas_model');
		$this->load->model('navegacion_model');

		$this->load->model('proveedores_model');

		$this->load->library('parseo_library');

		$this->stats_model->stats();

		if($this->parseo_library->url_has_space()){ // Si la url tiene espacios, se los quitamos y lo redireccionamos en un paso anterior
			$url = $this->parseo_library->url_sin_ultima_barra();
			if(isset($_GET["gracias"])&&$_GET["gracias"]) $url .= "1&gracias=".$_GET["gracias"]; // Todavia no analice su uso pero lo dejo para no olvidarme
			header("HTTP/1.1 301 Moved Permanently");
			redirect(base_url($url));
		}

		$tipo_listado = $tipo;

		$id_origen = isset($this->config_library->id_origen[$tipo_listado])&&$this->config_library->id_origen[$tipo_listado]?$this->config_library->id_origen[$tipo_listado]:0;

		$rubro   = $this->rubros_model->get_rubro($id_rubro);

		if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
		if(!$rubro) redirect(base_url('/'));

		$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

		$this->config_library->cambiar_sucursal_comercial($rubro['id_sucursal']);

		// STATS NO ESTA ACTIVADO, ENTONCES POR AHORA CUENTO VISITAS --
		$this->visitas_model->visitas($id_rubro, $id_origen);
		$this->visitas_model->sumar_visita_rubro();
		// --- //


		if(!empty($_POST) && isset($_POST['id_rubro']) && count($_POST['id_rubro']) > 0){
            $res = $this->config_library->validate_formulario($_POST, FALSE);
            if($res){

            	$this->load->library('procesos_library');
            	$this->procesos_library->procesar_pedidos_presupuesto($_POST);

                $this->session->set_flashdata('gracias', 1);
                $this->session->set_flashdata('id_rubro_gracias', $id_rubro);

                redirect(base_url($this->config_library->sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_r' . $id_rubro));
            }else{
                $this->session->set_flashdata('id_rubro', $id_rubro);
            }
        }

        $rubros_derivados = '';
		$session_form = '';
        if($this->session->userdata('id_rubro')){
        	$this->load->library('derivador_library');

            $rubros_derivados = $this->derivador_library->get_rubros_derivados($this->session->userdata('id_rubro'));
            $session_form = $this->session->userdata('session_form');
        }

        //- SE TOMA LA PRIMER PARTE DE LA URL PARA EL FILTRO DE RUBROS DEL MENU IZQ (porq para las solapas de paquetes, promos quedaban en un nivel superior) -//
		$url_actual = $_SERVER['REQUEST_URI'];
		//$uri = split('/', $url_actual);
		$uri = explode('/', $url_actual);
		$hasta = count($uri);
		$new_url = '';
		for($i=0; $i<$hasta; $i++){
			if($uri[$i] == 'guia-de-servicios'){
				$new_url .= $uri[$i];
				$i = $hasta;
			}
			else{
				$new_url .= $uri[$i].'/';
			}
		}
		//- SE TOMA LA PRIMER PARTE DE LA URL PARA EL FILTRO DE RUBROS DEL MENU IZQ -//

		if($tipo_listado != "empresas"){
			$ua = explode("?",$url_actual);
			$url_actual = $ua[0];
		}

		// LOGICA PARA PAGINACION, LUEGO REEMPLAZAR POR PAGINADOR CODEIGNITER
		$par = explode('|',urldecode($param));
		unset($par[0]);
		$par = implode('|',$par);

		$aux4 = '';
		$hay_pag = FALSE;

		if($tipo == 'empresas'){
			$corte = '_CO_r' . $id_rubro;
		}else{
			$corte = '_CO_' . 't' . $tipo . '_r' . $id_rubro;
		}

		$partes_url = explode($corte, $url_actual);

		$filtros_imploded = '';
		$pag = '';

		if(!empty($partes_url[1])) $desde_paginacion = explode('|', urldecode($partes_url[1]));
		if(!empty($desde_paginacion[0])){
			$aux3 = urldecode($partes_url[1]);
			$filtros_imploded = $aux3;
			$pag = ltrim($desde_paginacion[0], '_');
		}

		if(!empty($desde_paginacion[0])){
			if(strlen($desde_paginacion[0]) > 1){
				$hay_pag = TRUE;
				unset($desde_paginacion[0]);
				$filtros_imploded = implode('|', $desde_paginacion);
				$aux4 = urlencode((count($desde_paginacion) > 0 ? '|' : '') . $filtros_imploded);
			}
		}

		$tipo_para_url = ($tipo != 'empresas' ? ('-t' . $tipo) : '');

		$url_paginacion = $partes_url[0] . $corte . '_[pag]' . (!empty($aux3) ? ($hay_pag ? $aux4 : ltrim($aux3, '_')) : '');
		$url_paginacion_filtro = $partes_url[0] . $corte . ($hay_pag ? ('_' . $aux4) : (!empty($aux3) ? $aux3 : '_'));
		// FIN LOGICA PARA PAGINACION

		$stat_data = array(
			'id_rubro'   => $id_rubro,
		   	'id_origen'  => $id_origen,
		   	'id_user'    => ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
		   	'ip'		 => $_SERVER['REMOTE_ADDR'],
		   	'url'		 => $this->navegacion_model->getURL(),
		   	'referida'	 => !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

        $pageKey = $this->config_library->sucursal['id'] . '_proveedor_' . $id_rubro . '_' . $tipo_listado . ($pag ? ('_' . $pag) : '') . $filtros_imploded;

        $data_tipo_listado = array();

        switch ($tipo_listado) {
        	case 'empresas':
				$data_tipo_listado['view'] = 'listado';
			break;
			case 'productos':
        		$data_tipo_listado['view'] = 'listado_productos';
			break;
			case 'paquetes':
        		$data_tipo_listado['view'] = 'listado_paquetes';
			break;
			case 'promociones':
        		$data_tipo_listado['view'] = 'listado_promociones';
			break;
			case 'fotos':
        		$data_tipo_listado['view'] = 'listado_fotos';
			break;
			case 'mapa':
        		$data_tipo_listado['view'] = 'listado_mapa';
			break;
        }

		if (!$data = $this->cache->file->get($pageKey)){

				$seccion = $this->proveedores_model->get_seccion($id_rubro);
				$id_seccion = $seccion['id_seccion'] ? $seccion['id_seccion'] : '';

				$banners_primero = (array) $this->banners_model->views($id_seccion);

				$banners_segundo = (array) $this->config_library->get_segundo_banner();
				$banners = $banners_primero + $banners_segundo;

				$filtros_empresas = $this->parseo_library->filtros_empresas($tipo, $param); // ESTA FUNCION CONTIENE REDIRECCIONES
				$filtros = $filtros_empresas["filtros"];

				$cantidad = (!empty($_GET['cantidad']))?(int)$_GET['cantidad']:($tipo ? 21 : 25);
				$pagina = $filtros_empresas["pagina"] ? $filtros_empresas["pagina"] : 1;

				$cantidad_solapa_fotos = 27;
				$show_paquete_detalle = 0;
				$cantidad_galeria = 5;

				$rand = 0;

				$id_sucursal = $rubro['id_sucursal'];

				$data_tipo_listado = $this->config_library->get_tipo_listado($tipo_listado, $params = array(
					"id_rubro" 			  => $id_rubro,
					"filtros"  			  => $filtros,
					"cantidad" 			  => $cantidad,
					"pagina"   			  => $pagina,
					"rand"	   		      => $rand,
					"cantidad_galeria"    => $cantidad_galeria
				));
				$ordenar = $this->parseo_library->criterios_ordenar($tipo_listado);

				$resultados = $data_tipo_listado["resultados"];

				$item_desde = ($pagina-1) * $cantidad + 1;
				$item_hasta = ($pagina*$cantidad < $resultados)? $pagina*$cantidad : $resultados;
				$paginas = ceil($resultados/$cantidad);

				$rubros_guia = $this->rubros_model->get_listado_rubros_guia($rubro['id_sucursal']);

				$rubros = array();
				$total_r = 0;
				foreach($rubros_guia as $rd){
					$total_r += $rd['total'];
					$rubros[$rd['id_padre']]['total'] = $total_r;
					$rubros[$rd['id_padre']]['titulo'] = $rd['padre'];
					if(!isset($rubros[$rd['id_padre']]['rubros'])||!is_array($rubros[$rd['id_padre']]['rubros'])){
						$rubros[$rd['id_padre']]['rubros'] = array();
					}
					array_push($rubros[$rd['id_padre']]['rubros'],$rd);
				}

				$url_solapa = '/planea-tu-casamiento/'.$rubro['id_sucursal'].'/'.$this->parseo_library->clean_url($rubro['sucursal']).'/guia-de-servicios/'.$rubro['id'].'/'.$this->parseo_library->clean_url($rubro['rubro']).'/';
				$url_base = $url_solapa.(($tipo_listado == 'empresas')? '' : $tipo_listado.'/').'1';
				$url_solapa .= '[tipo]/1';
				$url_filtro_paq = '';

				if (isset($filtros_primarios) && $filtros_primarios){
					foreach ($filtros_primarios as $k => $v)
					$url_filtro_paq .= '&'.$k.'='.$v;
				}

				$filtros = ($filtros)? ((substr($filtros,0,1) != '|')?'|'.$filtros : $filtros) : '';
				$url_paging = substr($url_base,0,-1).'[pag]&orden='.(isset($data_tipo_listado["orden"])&&$data_tipo_listado["orden"]?$data_tipo_listado["orden"]:"").'&cantidad='.$cantidad.'&'.$url_filtro_paq.'&'.$filtros;

				$filtros_title = "";
				if($data_tipo_listado["filtros_aplicados"]){
					foreach($data_tipo_listado["filtros_aplicados"] as $f){
						$filtros_title[] = $f["opcion"];
					}
					$filtros_title = " - ".implode($filtros_title," - ");
				}

				if((isset($_GET['tipo']) && $_GET['tipo'] != 'mapa') || !isset($_GET['tipo'])){
					if($pagina != $paginas){
						$paginas_title = " - ".$pagina." de ".$paginas;
					}
				}

				if(isset($data_tipo_listado["fotos"])) $fotos_parseado = $this->parseo_library->foto_para_listado($data_tipo_listado["fotos"]);

				$paginas_margen = $this->config->item('paginas_margen');
				$paginador_da = $this->parseo_library->paginador_desde_hasta($pagina, $paginas, $paginas_margen);

		        $canonical = explode('r' . $id_rubro, $_SERVER['REQUEST_URI']);
		        if(!empty($canonical[0])) $canonical = base_url($canonical[0] . ('r' . $id_rubro));

		        $solapas = $this->rubros_model->get_rubro_solapas($id_rubro);

				$data = array(
					'banners' 		   		    => $banners,
					'cantidad'			        => $cantidad,
					'canonical'					=> $canonical,
					'desde' 				    => $paginador_da['desde'],
					'elemento_url'			    => $data_tipo_listado['elemento_url'],
					'id_tipo_formulario'		=> $data_tipo_listado['id_tipo_formulario'],
					'fotos'     			    => isset($data_tipo_listado["fotos"])&&$data_tipo_listado["fotos"]?$data_tipo_listado["fotos"]:"",
					'fotos_parseado'		    => isset($fotos_parseado)&&$fotos_parseado?$fotos_parseado:0,
					'filtros'				    => $filtros,
					'filtros_pendientes' 	    => $data_tipo_listado["filtros_pendientes"],
					'filtros_aplicados'  	    => $data_tipo_listado["filtros_aplicados"],
					'hasta'					    => $paginador_da['hasta'],
					'ic'					    => isset($data_tipo_listado['iconos_rubros'])&&$data_tipo_listado['iconos_rubros']?$data_tipo_listado['iconos_rubros']:array(),
					'id_seccion'	   		    => $id_seccion,
					'item_desde'			    => $item_desde,
					'item_hasta'			    => $item_hasta,
					'mostrar_cantidad_filtros'  => isset($data_tipo_listado["mostrar_cantidad_filtros"])&&$data_tipo_listado["mostrar_cantidad_filtros"]?$data_tipo_listado["mostrar_cantidad_filtros"]:"",
					'meta_descripcion' 		    => $data_tipo_listado["descripcion"] . ' ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . $data_tipo_listado['descripcion_2'],
					'meta_keywords'             => $rubro['rubro'].', '.$rubro['sucursal'].', casamientos, novios, bodas, Argentina'.$data_tipo_listado["meta_keywords"],
					'meta_title'	   		    => $data_tipo_listado["title"] . ' de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . ' | Casamientos Online',
					'listado_rubros'			=> TRUE,
					'orden'    				    => isset($data_tipo_listado["orden"])&&$data_tipo_listado["orden"]?$data_tipo_listado["orden"]:"",
					'ordenar'  		   		    => $ordenar,
					'pagina'			        => $pagina,
					'paginar'				    => isset($data_tipo_listado["paginar"])&&$data_tipo_listado["paginar"]?$data_tipo_listado["paginar"]:"",
					'paginas'			        => $paginas,
					'paginas_margen' 		    => $paginas_margen,
					'pres_origen_grupal' 	    => isset($data_tipo_listado["pres_origen_grupal"])&&$data_tipo_listado["pres_origen_grupal"]?$data_tipo_listado["pres_origen_grupal"]:"",
					'productos'    		   	    => isset($data_tipo_listado["productos"])&&$data_tipo_listado["productos"]?$data_tipo_listado["productos"]:"",
					'proveedores'    		    => isset($data_tipo_listado["proveedores"])&&$data_tipo_listado["proveedores"]?$data_tipo_listado["proveedores"]:"",
					'rand'					    => $rand,
					'resultados'     		    => $resultados,
					'resultados_fotos'		    => isset($data_tipo_listado["resultados_fotos"])&&$data_tipo_listado["resultados_fotos"]?$data_tipo_listado["resultados_fotos"]:"",
					'rubros_guia'	   		    => $rubros,
					'show_paquete_detalle'	    => $show_paquete_detalle,
					'solapas'				    => $solapas,
					'sucursal'		   		    => $this->navegacion_model->getSucursal($rubro['id_sucursal']),
					'sucursal_seo'				=> TRUE,
					'sucursal_redirect'			=> '/fiestas-de-casamiento',
					'tipo_listado'  		    => $tipo_listado,
					'url_base'				    => $url_base,
					'url_paging'			    => $url_paging,
					'url_solapa'			    => $url_solapa
				);

		        $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['param_get'] 		  	= isset($ua[1])&&$ua[1] ? $ua[1] : "";
		$data['request_uri_base'] 	= base_url($url_actual).(!$param?"/":"");
		$data['rubro'] 				= $rubro;
		$data['rubros_derivados']	= $rubros_derivados;
		$data['session_form']		= $session_form;
		$data['tipo_para_url']		= $tipo_para_url;
		$data['uri']				= $new_url;
		$data['url_filtros']		= base_url($url_paginacion_filtro);
		$data['url_paginacion']		= $url_paginacion;
		$data['vistas']				= isset($vistas)&&$vistas?$vistas:0;


		// --
		$this->load->view($data_tipo_listado["view"], $this->config_library->send_data_to_view($data));

	}

	public function minisitio($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');

		if($id_minisitio && !$subdominio){
			$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);

			if(!$proveedor) {
				$proveedor = $this->proveedores_model->get_proveedor_vencido($id_minisitio);
			}

			if(!$proveedor['subdominio']){
				show_404();
			}else{
				redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $proveedor['seo_url'] . (!empty($_GET['hash']) ? '?hash=' . $_GET['hash'] : '') );
			}
		}

		if(!$id_minisitio && !$subdominio && strpos($_SERVER['HTTP_HOST'], 'casamientos') > 0){
			$tmp = explode('.', $_SERVER['HTTP_HOST']);
			if(!empty($tmp[0])){
				$subdominio = $tmp[0];
			}
		}
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);

			if(!$res) {
				$res = $this->proveedores_model->get_minisitio_subdominio_vencido($subdominio);
			}

			$id_minisitio = $res['id_minisitio'];
		}

		$this->load->driver('cache');

		$pageKey = 'empresa_' . $id_minisitio;

		$this->load->model('rubros_model');
		$this->load->model('paquetes_model');
		$this->load->model('productos_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$this->load->helper('common_helper');

		$prov_vencido = 0;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);

		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		if(empty($_GET['hash'])){
			$this->visitas_model->visitas(isset($proveedor['id_rubro'])?$proveedor['id_rubro']:'', 0, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		if($prov_vencido){
			$proveedor = $this->config_library->proveedor_vencido;
		}

		if (!empty($_GET['hash'])){
			$proveedor = $this->proveedores_model->get_proveedor_hash($id_minisitio, $_GET['hash']); // piso la variable $proveedor con la info de vista previa
		}

		$proveedor['telefonos'] = (isset($proveedor['telefonos'])) ? $this->parseo_library->procesar_telefonos($proveedor['telefonos']) : null;
		
		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor, NULL, NULL, $prov_vencido);

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		$extra_canonical = '';
		$meta_title = $proveedor['subdominio'] . ': ' . $proveedor['rubro'] . ' para tu casamiento! | Casamientos Online';
		if(strlen($meta_title) > 65){
			$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
		}
		$meta_descripcion = rtrim(trim($proveedor['breve']), '.') . '. Entrá aquí!';

		if($this->session->userdata('gracias')){
			$meta_title = 'Tu presupuesto fue enviado a ' . $proveedor['subdominio'] . ' | Casamientos Online';
			$extra_canonical = '#gracias';
			$meta_descripcion = 'Listo! Tu solicitud de presupuesto fue enviada a ' . $proveedor['subdominio'] . '. ¿Qué más podemos hacer para ayudarte?';
		}


		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){
			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,50,1,'',4);
				$tipo = 4;
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,50,1,'',2);
				$tipo = 2;
			}

			$hash_valido = FALSE;
			if(!empty($_GET['hash']) && !empty($proveedor)) $hash_valido = TRUE;

			$cantidad_consultados = 5;

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);

			$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

			$videos = $this->proveedores_model->get_proveedor_media($id_minisitio, 3);

			$fotos_videos = array_merge($videos, $fotos);

			$fotos_parseado = $this->parseo_library->agrupar_fotos($fotos_videos, 5); // Solo para slider

			$resultados 	   = $this->config_library->resultados;
			$imagenes 		   = $this->proveedores_model->get_proveedor_media($id_minisitio, 1);
			$audios 		   = $this->proveedores_model->get_proveedor_media($id_minisitio, 2);
			$asociados  	   = $this->proveedores_model->get_proveedor_asociados($id_minisitio,-1,1);
			$asociados_trabaja = $this->proveedores_model->get_proveedor_asociados_ms($id_minisitio);
			$asociados_trabaja = $this->parseo_library->asociados_trabaja($asociados_trabaja);
			$comentarios 	   = $this->proveedores_model->get_proveedor_comentarios($id_minisitio,5);
			$puntajes 		   = $this->parseo_library->promedios_puntajes_comentarios($comentarios);

			$cupones 		   = $this->proveedores_model->get_cupones(array('id_minisitio' => $id_minisitio));

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$ms_paq = array();
			if (isset($proveedor['paquetes'])&&$proveedor['paquetes']){
				$ms_paq = $this->paquetes_model->get_paquetes(0, 'nivel', 10, 1, '', $id_minisitio, '');
				$solapa = (!empty($solapa))? $solapa : 'paquetes';
			}

			//-- (inicio) PAQUETES DE OTROS MINISITIOS AL Q FORMA PARTE --//
			$solo_paq_integrados = FALSE;

			if($this->paquetes_model->resIntegrados > 0){
				if(count($ms_paq) > 0){
					$ms_paq = array_merge($ms_paq, $ms_paq2); //$ms_paq2 + $ms_paq;
					$solapa = (!empty($solapa))? $solapa : 'paquetes';
				}else{
					$ms_paq = $ms_paq2;
					$solo_paq_integrados = TRUE;
				}

				$proveedor['paquetes'] = $this->paquetes_model->resIntegrados;
			}
			//-- (fin) PAQUETES DE OTROS MINISITIOS AL Q FORMA PARTE --//

			$ms_prom = array();

			if (isset($proveedor['promociones']) && $proveedor['promociones']){
				if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vistaprevia = TRUE;
				$ms_prom = $this->productos_model->get_pyp(0, 1, 'p.posicion,p.id_producto', 0, 1, '', 0, $id_minisitio, '');
				$arrImgPromo = $this->productos_model->get_productos_images((int)$id_minisitio,1);//-- Array de imagenes de la Promocion --//
				$solapa = (!empty($solapa))? $solapa : 'promociones';
			}

			$ms_prod = array();
			if (isset($proveedor['productos']) && $proveedor['productos']){
				if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vistaprevia = TRUE;
				$ms_prod = $this->productos_model->get_pyp(0, 0, 'p.posicion,p.id_producto', 0, 1, '', 0, $id_minisitio, '');

				$arrImgProducto = $this->productos_model->get_productos_images((int)$id_minisitio,0);//-- Array de imagenes de la Promocion --//
				$solapa = (!empty($solapa))? $solapa : 'productos';
			}

			if(!$solapa && $this->paquetes_model->resIntegrados > 0){
				$solapa = 'paquetes';
			}

			$arrImg = NULL;
			$items = NULL;
			$show_paquete_detalle = NULL;
			$show_dm_detalle = NULL;
			switch ($solapa){
				case 'paquetes':
					$show_paquete_detalle = 1;
					$show_dm_detalle = 0;
					break;
				case 'productos':
					$arrImg = $arrImgProducto;
					$show_paquete_detalle = 0;
					$show_dm_detalle = 1;
					break;
				case 'promociones':
					$arrImg = $arrImgPromo;
					$show_paquete_detalle = 0;
					$show_dm_detalle = 1;
					break;
			}

			$items = $this->parseo_library->minisitio_ordenar_pyp(array(
				'promociones' => $ms_prom,
				'productos'   => $ms_prod,
				'paquetes'    => $ms_paq
			));

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			$ic = $this->parseo_library->equivalencia_iconos();

			$faqs 			 = $this->proveedores_model->get_faqs($proveedor['id_proveedor'], $proveedor['id_rubro']);
			$faqs_respuestas = $this->proveedores_model->get_faqs_respuestas($proveedor['id_rubro']);
			$faqs_rangos 	 = $this->proveedores_model->get_faqs_rangos();
			$faqs_parseado 	 = $this->parseo_library->parsear_faqs($faqs, $faqs_respuestas, $faqs_rangos);

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        if($elementos_random) asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'arrImg'					=> $arrImg,
				'asociados'		   	  		=> $asociados,
				'asociados_trabaja'  		=> $asociados_trabaja,
				'audios'		   	  		=> $audios,
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . $extra_canonical,
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'columnas_audios'  	  		=> 3,
				'comentarios'	   	  		=> $comentarios,
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'cupones'		   	  		=> $cupones,
				'faqs'						=> $faqs_parseado,
				'fotos'			   	  		=> $fotos_videos,
				'fotos_parseado'			=> $fotos_parseado,
				'ic'						=> $ic,
				'hash_valido'				=> $hash_valido,
				'id_seccion' 	   	  		=> $id_seccion,
				'imagenes'   	   	  		=> $imagenes,
				'items'   	   	  			=> $items,
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq,
				'proveedores'				=> $proveedores,
				'puntajes' 				    => $puntajes,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'show_dm_detalle'	   	  	=> $show_dm_detalle,
				'show_paquete_detalle' 		=> $show_paquete_detalle,
				'solapa' 				    => $solapa,
				'solo_paq_integrados' 		=> $solo_paq_integrados,
				'ubicacion'		      		=> 'minisitio',
				'tipo'						=> $tipo,
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde']))),
				'videos'		      		=> $videos
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['meta_descripcion'] 	= $meta_descripcion;
		$data['meta_keywords'] 		= 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'];
		$data['meta_title'] 		= $meta_title;
		$data['prov_vencido'] 		= $prov_vencido;
		$data['proveedor'] 			= $proveedor;
		$data['data_form'] 			= $form['data_form'];
		$data['datos_viaje'] 		= $form['datos_viaje'];
		$data['fecha_variable'] 	= $form['fecha_variable'];
		$data['hidden'] 			= $form['hidden'];
		$data['localidades']		= $form['localidades'];
		$data['provincias']			= $form['provincias'];
		$data['salon_elegido']		= $form['salon_elegido'];

		$this->load->view('minisitio', $this->config_library->send_data_to_view($data));
	}

	public function minisitio_promociones_productos($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);
			$id_minisitio = $res['id_minisitio'];
		}
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->driver('cache');

		$this->load->model('rubros_model');
		$this->load->model('productos_model');
		$this->load->model('paquetes_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$pageKey = 'promociones_productos_' . $id_minisitio;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);
		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$prov_vencido = 0;
		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if(empty($_GET['hash'])){ //-- Sumo Visitas para MS vencidos y online. --//
			$this->visitas_model->visitas($proveedor['id_rubro'], 200, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			$cantidad_consultados = 5;

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);
			$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$resultados = $this->config_library->resultados;

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$ms_prom = array();
			if ($proveedor['promociones']){
				if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vista_previa = TRUE;
				$ms_prom = $this->productos_model->get_pyp(0, 1, 'p.posicion,p.id_producto', 0, 1, '', 0, $id_minisitio, '');
				$solapa = (!empty($solapa))? $solapa : 'promociones';
			}

			$ms_prod = array();
			if ($proveedor['productos']){
				if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vista_previa = TRUE;
				$ms_prod = $this->productos_model->get_pyp(0, 0, 'p.posicion,p.id_producto', 0, 1, '', 0, $id_minisitio, '');
				$solapa = (!empty($solapa))? $solapa : 'productos';
			}

			$items = NULL;
			$show_paquete_detalle = NULL;
			$show_dm_detalle = NULL;
			switch ($solapa){
				case 'productos':
					$show_paquete_detalle = 0;
					$show_dm_detalle = 1;
					break;
				case 'promociones':
					$show_paquete_detalle = 0;
					$show_dm_detalle = 1;
					break;
			}

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',4);
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',2);
			}

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$meta_title = $proveedor['subdominio'] . ': Promociones de ' . $proveedor['rubro'] . ' | Casamientos Online';
			if(strlen($meta_title) > 65){
				$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
			}

			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/productos-y-promos',
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'fotos'   	  				=> $fotos,
				'id_seccion' 	   	  		=> $id_seccion,
				'items'   	   	  			=> $items,
				'meta_descripcion' 	  		=> 'Aprovecha las promociones de ' . $proveedor['rubro'] . ' que ' . $proveedor['subdominio'] . ' tiene para vos! Todas las opciones encontralas aquí!',
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
				'meta_title'	   	  		=> $meta_title,
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq2,
				'productos'					=> $ms_prod,
				'promociones'				=> $ms_prom,
				'proveedores'				=> $proveedores,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'show_dm_detalle'	   	  	=> $show_dm_detalle,
				'show_paquete_detalle' 		=> $show_paquete_detalle,
				'solapa' 				    => $solapa,
				'ubicacion'		      		=> 'minisitio_promociones_productos',
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['data_form'] 			= $form['data_form'];
		$data['datos_viaje']		= $form['datos_viaje'];
		$data['fecha_variable']		= $form['fecha_variable'];
		$data['hidden']				= $form['hidden'];
		$data['localidades']		= $form['localidades'];
		$data['proveedor']			= $proveedor;
		$data['provincias']			= $form['provincias'];
		$data['salon_elegido']		= $form['salon_elegido'];

		$this->load->view('minisitio_promociones_productos', $this->config_library->send_data_to_view($data));
	}

	public function producto($id = NULL, $tipo = 'producto'){ // TAMBIEN SE USA PARA PROMOCION Y PAQUETE
		if(!$id) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->model('proveedores_model');
		$this->load->model('productos_model');
		$this->load->model('rubros_model');
		$this->load->model('paquetes_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');
		$dias = $this->parseo_library->get_dias();
		$turnos = $this->parseo_library->get_turnos();
		$this->load->helper('common_helper');

		$ubicacion = '';
		$id_origen = NULL;
		$cantidad_consultados = 5;
		$mas_consultados = array();
		$caracteristicas = array();
		if($tipo == 'paquete'){
			$ubicacion = 'minisitio_paquetes';
			$elemento = 'Paquete';
			$item = $this->paquetes_model->get_paquete($id);

			$caracteristicas = $this->paquetes_model->get_caracteristicas_paquetes_rubros($id); // FALTA TRAER EL PROVEEDOR AL QUE PERTENECE

			if ($item){
				$item['id_producto'] = $item['id_paquete'];
				$item['tipo'] = 'paquetes';
				$id_origen_stats = 32;
				$id_origen = 32;
			}
			$cod = 'pa';
		}else{
			$ubicacion = 'minisitio_promociones_productos';
			$item = $this->productos_model->get_producto($id);
			$itemVarios = $this->productos_model->get_producto_varios($id);

 			$elemento = 'Producto';
			$cod = 'pd';
			if($tipo == 'promocion'){
				$elemento = 'Promoción';
				$cod = 'pr';
			}

			if($item){
				$dm_pagos = doubleExplodeFull(';', ',', $item['dm_pagos']);
				$item['cuotas'] = $dm_pagos[0][count($dm_pagos[0])-1];
				$id_origen_stats = 33;
				if($tipo == 'promocion') $id_origen_stats = 34;

				$id_origen = 33;
				if($tipo == 'promocion' && $item['id_minisitio']){ // PROMOCION
					$id_origen = 34;
					$mas_consultados = $this->generales_model->get_mas_consultados(4,$this->session->userdata('id_sucursal'),$cantidad_consultados, 0, ' LEFT JOIN site_productos_activos spa ON spa.id_producto = smc.id_padre INNER JOIN prov_proveedores pp ON pp.id = spa.id_proveedor ', ' , IF(spa.promocion,"promociones","productos") tipo ', ' AND spa.id_rubro = ' . $item['id_rubro']);

					if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vista_previa = TRUE;
					$ms_prom = $this->productos_model->get_pyp(0, 1, 'p.posicion,p.id_producto', 4, 1, '', 0, $item['id_minisitio'], $item['id_producto'], 0, FALSE, FALSE, TRUE);
					$solapa = (!empty($solapa))? $solapa : 'promociones';

				}else{
					$mas_consultados = $this->generales_model->get_mas_consultados(3,$this->session->userdata('id_sucursal'),$cantidad_consultados, 0, ' LEFT JOIN site_productos_activos spa ON spa.id_producto = smc.id_padre INNER JOIN prov_proveedores pp ON pp.id = spa.id_proveedor ', ' , IF(spa.promocion,"promociones","productos") tipo ', ' AND spa.id_rubro = ' . $item['id_rubro']);

					if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vista_previa = TRUE;
					$ms_prod = $this->productos_model->get_pyp(0, 0, 'p.posicion,p.id_producto', 4, 1, '', 0, $item['id_minisitio'], $item['id_producto'], 0, FALSE, FALSE, TRUE);
					$solapa = (!empty($solapa))? $solapa : 'productos';

				}
			}
		}

		if(!$item || empty($item['id_proveedor'])) {
			if($tipo == 'paquete'){
				$item2 = $this->paquetes_model->get_paquete_vencido($id);
				if ($item2){
					$item2['id_producto'] = $item2['id_paquete'];
					$item2['tipo'] = 'paquetes';
				}
			}else{
				$item2 = $this->productos_model->get_producto_vencido($id);				
			}
		}

		$m = explode('.', $_SERVER['HTTP_HOST']);
		if($m[0] == 'casamientos' || $m[0] == 'casamientosonline' || $m[0] == 'www'){
			$url = null;

			if(!empty($item) && !empty($item['id_proveedor'])) {
				$id_producto_paquete = null;

				if(!empty($item['id_producto'])) {
					$id_producto_paquete = $item['id_producto'];
				} else if(!empty($item['id_paquete'])) {
					$id_producto_paquete = $item['id_paquete'];
				}

				$url = 'http://' . $item['subdominio'] . '.' . DOMAIN . '/' . $item['rubro_seo'] . '/' . $this->parseo_library->clean_url($item['titulo']) . '_CO_' . $cod . $id_producto_paquete;
			} else if(!empty($item2)) {
				$id_producto_paquete = null;

				if(!empty($item2['id_producto'])) {
					$id_producto_paquete = $item2['id_producto'];
				} else if(!empty($item2['id_paquete'])){
					$id_producto_paquete = $item2['id_paquete'];
				}

				if($item2['subdominio']) $url = 'http://' . $item2['subdominio'] . '.' . DOMAIN . '/' . $item2['rubro_seo'] . '/' . $this->parseo_library->clean_url($item2['titulo']) . '_CO_' . $cod . $id_producto_paquete;				
			}

			if($url) redirect($url);
		}

		$proveedor = $this->proveedores_model->get_proveedor($item['id_minisitio']);
		$prov_vencido = false;

		if(!$proveedor) {
			$proveedor = $this->proveedores_model->get_proveedor($item2['id_minisitio']);
			$prov_vencido = false;
		}

		if(!$proveedor) {
			$proveedor = $this->proveedores_model->get_proveedor_vencido($item['id_minisitio']);
			$prov_vencido = true;
		}

		if(!$proveedor) {
			$proveedor = $this->proveedores_model->get_proveedor_vencido($item2['id_minisitio']);
			$prov_vencido = true;
		}

		$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);
		$proveedor['telefonos'] = (isset($proveedor['telefonos'])) ? $this->parseo_library->procesar_telefonos($proveedor['telefonos']) : null;
		$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $item['id_minisitio']);

		$item_vencido = FALSE;

		$rubro = $this->rubros_model->get_rubro($proveedor['id_rubro']);
		if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
		$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);


		if (!$item){
			$proveedor_tmp = $this->generales_model->get_como_mostrar($id, ($tipo=='promocion'?'Promocion':($tipo=='producto'?'Producto':'Paquete')));

			if(!$proveedor_tmp['mostrar_inactivo']){
				redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));
			}

			$data_item = $this->config_library->item_vencido($id, $tipo);
			if(isset($data_item['item']) && $data_item['item']) $item = $data_item['item'];

			if(isset($item['id_minisitio']) && $item['id_minisitio']){
				$proveedor_tmp = $this->proveedores_model->get_proveedor($item['id_minisitio']);

				if($proveedor_tmp) redirect('http://' . $proveedor_tmp['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
				$item_vencido = TRUE;

			}else{
				redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));
			}

		}

		if(empty($item['id_proveedor']) && !empty($item2['id_proveedor'])) $item = $item2;

		$this->config_library->cambiar_sucursal_comercial($item['id_sucursal']);

		$form = $this->parseo_library->minisitio_form_data($item['id_minisitio'], $proveedor, NULL, $id, FALSE, $item_vencido);

		if($id_origen){
			$this->visitas_model->visitas($proveedor['id_rubro'],$id_origen_stats,$proveedor['id_proveedor']);
			$this->visitas_model->sumar_visita_proveedor();
		}

		$this->caracteristicas_model->caracteristicas($item['id_minisitio']);
		$cant_caracs = $this->caracteristicas_model->tiene_caracs();
		$caracs = $this->caracteristicas_model->get_caracs_proveedor();

		$fotos = $this->config_library->get_fotos($item['id_minisitio'],1,1,'',4);

		$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
        asort($elementos_random);
        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

		$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
		$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

		$meta_title = ($proveedor['subdominio'] ? $proveedor['subdominio'] : $proveedor['proveedor']) . ': ' . $item['titulo'] . ' y ' . $proveedor['rubro'] . ' | Casamientos Online';
		if(strlen($meta_title) > 65){
			$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
		}

		switch ($tipo) {
			case 'promocion':
				$codigo = 'pr';
				$meta_descripcion = 'Enterate de la variedad de ' . $item['titulo'] . ' que ' . $proveedor['subdominio'] . ' tiene para vos! Todas las opciones en ' . $proveedor['rubro'] . ' encontralas aquí!';
				break;
			case 'producto':
				$codigo = 'pd';
				$meta_descripcion = 'Enterate de la variedad de ' . $item['titulo'] . ' que ' . $proveedor['subdominio'] . ' tiene para vos! Todas las opciones en ' . $proveedor['rubro'] . ' encontralas aquí!';
				break;
			case 'paquete':
				$codigo = 'pa';
				$meta_descripcion = 'Encontrá el paquete ' . $item['titulo'] . ' que ' . $proveedor['subdominio'] . ' diseñó especialmente para vos! Conocelo aquí.';

				$meta_title = $proveedor['rubro'] . ': ' . $item['titulo'] . ' | ' . $proveedor['subdominio'] . ' | Casamientos Online';
				if(strlen($meta_title) > 65){
					$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
				}
				break;
		}

		$data = array(
			'elementos_random'			=> $elementos_random,
			'listado_seo_random'		=> $listado_seo_random,
			'listado_seo_random_guiones'=> $listado_seo_random_guiones,
			'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/' . $this->parseo_library->clean_url($item['titulo']) . '_CO_' . $codigo . $item['id_producto'],
			'cant_caracs'	   	  		=> $cant_caracs,
			'caracs'		   	  		=> $caracs,
			'caracteristicas'			=> $caracteristicas,
			'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
			'data_form'		   	  		=> $form['data_form'],
			'datos_viaje'		   	  	=> $form['datos_viaje'],
			'dias'						=> $dias,
			'elemento'					=> $elemento,
			'fecha_variable'   	  		=> $form['fecha_variable'],
			'fotos'   	  				=> $fotos,
			'hidden'		   	  		=> $form['hidden'],
			'ic'						=> $this->parseo_library->equivalencia_iconos(),
			'id_seccion' 	   	  		=> $id_seccion,
			'item'   	   	  			=> $item,
			'itemVarios'   	   	  		=> $itemVarios,
			'item_vencido'				=> $item_vencido,
			'prov_vencido' 			    => $prov_vencido,
			'localidades'	   	  		=> $form['localidades'],
			'mas_consultados'			=> $mas_consultados,
			'meta_descripcion' 	  		=> $meta_descripcion,
			'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
			'meta_keywords'    	  		=> $item['titulo'].', '.$proveedor['proveedor'].', '.$proveedor['rubro'].', '.$proveedor['sucursal'].', casamiento, boda, novia, productos, servicios, promociones, guía para casamiento, casamientosonline',
			'meta_title'	   	  		=> $meta_title,
			'minisitio'  	   	  		=> TRUE,
			'ms_paq2'					=> $ms_paq2,
			'productos'					=> isset($ms_prod) && $ms_prod ? $ms_prod : NULL,
			'promociones'				=> isset($ms_prom) && $ms_prom ? $ms_prom : NULL,
			'proveedor'  	   	  		=> $proveedor,
			'proveedores'				=> $proveedores,
			'provincias'       	  		=> $form['provincias'],
			'rubro'						=> $rubro,
			'salon_elegido'		   	  	=> $form['salon_elegido'],
			'ubicacion'		      		=> $tipo,
			'turnos'					=> $turnos,
			'ubicacion_header_minisitio'=> $ubicacion,
			'twitter_title'				=> $proveedor['proveedor'],
			'twitter_descripcion'		=> $proveedor['breve'],
			'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
		);

		$this->load->view('detalle_promo_producto', $this->config_library->send_data_to_view($data));
	}

	public function detalle_promo_producto($id_minisitio = NULL){
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->model('rubros_model');
		$this->load->model('proveedores_model');
		$this->load->model('productos_model');
		$this->load->model('paquetes_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$cantidad_consultados = 5;
		$prov_vencido = 0;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);

		$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
		if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
		$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);
		$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);
		$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

		if(empty($_GET['hash'])){//-- Sumo Visitas para MS vencidos y online. --//
			// STATS NO ESTA ACTIVADO, ENTONCES POR AHORA CUENTO VISITAS --//
			$_GET["id_origen"] = isset($_GET["id_origen"])?(int)$_GET["id_origen"]:0;
			$_GET['id_rubro'] = isset($_GET['id_rubro'])?$_GET['id_rubro']:NULL;

			$this->visitas_model->visitas($_GET['id_rubro'],$_GET["id_origen"], $proveedor['id_proveedor']);
			$this->visitas_model->sumar_visita_proveedor();
			// --
		}

		//-- SI ES UN MINISITIO VENCIDO --//
		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		$resultados = $this->config_library->resultados;

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

		if (isset($proveedor['cupones'])&&$proveedor['cupones']){
			$solapa = (!empty($solapa))? $solapa : 'cupones';
		}


		$ms_prom = array();
		if ($proveedor['promociones']){
			if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vista_previa = TRUE;
			$ms_prom = $this->productos_model->get_pyp(0, 1, 'p.posicion,p.id_producto', 0, 1, '', 0, $id_minisitio, '');
			$solapa = (!empty($solapa))? $solapa : 'promociones';
		}

		$ms_prod = array();
		if ($proveedor['productos']){
			if(isset($_GET["hash"]) && $_GET["hash"]) $this->productos_model->vista_previa = TRUE;
			$ms_prod = $this->productos_model->get_pyp(0, 0, 'p.posicion,p.id_producto', 0, 1, '', 0, $id_minisitio, '');
			$solapa = (!empty($solapa))? $solapa : 'productos';
		}

		$item_consultar = NULL;
		$show_paquete_detalle = NULL;
		$show_dm_detalle = NULL;
		switch ($solapa){
			case 'productos':
				$show_paquete_detalle = 0;
				$show_dm_detalle = 1;
				$item_consultar = '/formulario.php?area=presupuesto&tipo=producto&id_producto=';
				break;
			case 'promociones':
				$show_paquete_detalle = 0;
				$show_dm_detalle = 1;
				$item_consultar = '/formulario.php?area=presupuesto&tipo=producto&id_producto=';
				break;
		}

		$this->caracteristicas_model->caracteristicas($id_minisitio);
		$cant_caracs = $this->caracteristicas_model->tiene_caracs();
		$caracs = $this->caracteristicas_model->get_caracs_proveedor();

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if (!empty($_GET['hash'])){
			$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',4);
		}else{
			$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',2);
		}

		$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
		$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

		$meta_title = $proveedor['subdominio'] . ': Promociones de ' . $proveedor['rubro'] . ' | Casamientos Online';
		if(strlen($meta_title) > 65){
			$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
		}

		$data = array(
			'cant_caracs'	   	  		=> $cant_caracs,
			'caracs'		   	  		=> $caracs,
			'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
			'data_form'		   	  		=> $form['data_form'],
			'datos_viaje'		   	  	=> $form['datos_viaje'],
			'fecha_variable'   	  		=> $form['fecha_variable'],
			'fotos'   	  				=> $fotos,
			'hidden'		   	  		=> $form['hidden'],
			'id_seccion' 	   	  		=> $id_seccion,
			'item_consultar'   	   	  	=> $item_consultar,
			'localidades'	   	  		=> $form['localidades'],
			'meta_descripcion' 	  		=> 'Casamientos Online, el portal de los novios. '.$proveedor['meta_descripcion'],
			'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
			'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
			'meta_title'	   	  		=> 'Casamientos Online - '.$proveedor['proveedor'].', '.$proveedor['rubro'].' en '.$proveedor['sucursal'],
			'minisitio'  	   	  		=> TRUE,
			'ms_paq2'					=> $ms_paq2,
			'productos'					=> $ms_prod,
			'promociones'				=> $ms_prom,
			'proveedor'  	   	  		=> $proveedor,
			'proveedores'				=> $proveedores,
			'provincias'       	  		=> $form['provincias'],
			'resultados'	   	  		=> $resultados,
			'rubro'						=> $rubro,
			'salon_elegido'		   	  	=> $form['salon_elegido'],
			'show_dm_detalle'	   	  	=> $show_dm_detalle,
			'show_paquete_detalle' 		=> $show_paquete_detalle,
			'solapa' 				    => $solapa,
			'ubicacion'		      		=> 'detalle_promo_producto',
			'twitter_title'				=> $proveedor['proveedor'],
			'twitter_descripcion'		=> $proveedor['breve'],
			'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
		);

		$this->load->view('detalle_promo_producto', $this->config_library->send_data_to_view($data));
	}

	public function minisitio_paquetes($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);
			$id_minisitio = $res['id_minisitio'];
		}
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->driver('cache');

		$this->load->model('paquetes_model');
		$this->load->model('rubros_model');

		$this->load->model('productos_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$pageKey = 'paquetes_' . $id_minisitio;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);

		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

		if((!$proveedor['paquetes']) && (count($ms_paq2)<1)) redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);

		$prov_vencido = 0;
		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		//-- SI ES UN MINISITIO VENCIDO --//
		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		if(empty($_GET['hash'])){//-- Sumo Visitas para MS vencidos y online. --//
			$this->visitas_model->visitas($proveedor['id_rubro'], 201, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			$rubro = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$cantidad_consultados = 5;

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);

			$resultados = $this->config_library->resultados;

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$ms_paq = array();
			if (isset($proveedor['paquetes'])&&$proveedor['paquetes']){
				$ms_paq = $this->paquetes_model->get_paquetes(0, 'nivel', 10, 1, '', $id_minisitio, '');
				$solapa = (!empty($solapa))? $solapa : 'paquetes';
			}

			//-- (inicio) PAQUETES DE OTROS MINISITIOS AL Q FORMA PARTE --//
			$solo_paq_integrados = FALSE;

			if($this->paquetes_model->resIntegrados > 0){
				if(count($ms_paq) > 0){
					$ms_paq = array_merge($ms_paq, $ms_paq2); //$ms_paq2 + $ms_paq;
					$solapa = (!empty($solapa))? $solapa : 'paquetes';
				}else{
					$ms_paq = $ms_paq2;
					$solo_paq_integrados = TRUE;
				}

				$proveedor['paquetes'] = $this->paquetes_model->resIntegrados;
			}
			//-- (fin) PAQUETES DE OTROS MINISITIOS AL Q FORMA PARTE --//

			$item_consultar = NULL;
			$items = NULL;
			$show_paquete_detalle = NULL;
			$show_dm_detalle = NULL;
			switch ($solapa){
				case 'paquetes':
					$show_paquete_detalle = 1;
					$show_dm_detalle = 0;
					$item_consultar = '';
					break;
			}

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',4);
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',2);
			}

			$ic = $this->parseo_library->equivalencia_iconos();

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$meta_title = $proveedor['subdominio'] . ': Paquetes de ' . $proveedor['rubro'] . ' | Casamientos Online';
			if(strlen($meta_title) > 65){
				$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
			}

			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/paquetes',
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'fotos'   	  				=> $fotos,
				'ic'						=> $ic,
				'id_seccion' 	   	  		=> $id_seccion,
				'item_consultar'   	   	  	=> $item_consultar,
				'items'   	   	  			=> $items,
				'meta_descripcion' 	  		=> 'Aprovechá los paquetes de '.$proveedor['rubro'].' que '.$proveedor['subdominio'].' tiene para vos! Todas las opciones en '.$proveedor['rubro'].' las tenés aquí!',
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
				'meta_title'	   	  		=> $meta_title,
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq2,
				'paquetes'					=> $ms_paq,
				'proveedores'				=> $proveedores,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'show_dm_detalle'	   	  	=> $show_dm_detalle,
				'show_paquete_detalle' 		=> $show_paquete_detalle,
				'solapa' 				    => $solapa,
				'ubicacion'		      		=> 'minisitio_paquetes',
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['data_form'] 		= $form['data_form'];
		$data['datos_viaje'] 	= $form['datos_viaje'];
		$data['fecha_variable'] = $form['fecha_variable'];
		$data['hidden'] 		= $form['hidden'];
		$data['localidades'] 	= $form['localidades'];
		$data['provincias'] 	= $form['provincias'];
		$data['salon_elegido'] 	= $form['salon_elegido'];
		$data['proveedor']		= $proveedor;

		$this->load->view('minisitio_paquetes', $this->config_library->send_data_to_view($data));
	}

	public function minisitio_foto_video($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);
			$id_minisitio = $res['id_minisitio'];
		}
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->driver('cache');

		$this->load->model('rubros_model');
		$this->load->model('proveedores_model');
		$this->load->model('paquetes_model');
		$this->load->model('productos_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$pageKey = 'foto_video_' . $id_minisitio;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);

		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$prov_vencido = 0;
		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		if(empty($_GET['hash'])){//-- Sumo Visitas para MS vencidos y online. --//
			$this->visitas_model->visitas($proveedor['id_rubro'], 202, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			$cantidad_consultados = 5;

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);
			$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$resultados = $this->config_library->resultados;
			$videos     = $this->proveedores_model->get_proveedor_media($id_minisitio, 3);

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			//-- DATOS DE VIAJE para los rubros --//
			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,50,1,'',4);
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,50,1,'',2);
			}

			$ic = $this->parseo_library->equivalencia_iconos();

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$meta_title = $proveedor['subdominio'] . ': Fotos y Video de ' . $proveedor['rubro'] . ' | Casamientos Online';
			if(strlen($meta_title) > 65){
				$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
			}

			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/fotos-y-videos',
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'fotos'   	  				=> $fotos,
				'ic'						=> $ic,
				'id_seccion' 	   	  		=> $id_seccion,
				'meta_descripcion' 	  		=> 'Las mejores fotos y videos de ' . $proveedor['subdominio'] . ' están aquí! Mirá todo lo que tiene para ofrecerte en ' . $proveedor['rubro'] . ' para tu casamiento.',
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
				'meta_title'	   	  		=> $meta_title,
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq2,
				'proveedores'				=> $proveedores,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'solapa' 				    => $solapa,
				'ubicacion'		      		=> 'minisitio_foto_video',
				'videos'					=> $videos,
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['data_form'] 		= $form['data_form'];
		$data['datos_viaje'] 	= $form['datos_viaje'];
		$data['fecha_variable'] = $form['fecha_variable'];
		$data['hidden'] 		= $form['hidden'];
		$data['localidades'] 	= $form['localidades'];
		$data['provincias'] 	= $form['provincias'];
		$data['salon_elegido'] 	= $form['salon_elegido'];
		$data['proveedor'] 		= $proveedor;

		$this->load->view('minisitio_foto_y_video', $this->config_library->send_data_to_view($data));
	}

	public function minisitio_mapa($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);
			$id_minisitio = $res['id_minisitio'];
		}
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->driver('cache');

		$this->load->model('rubros_model');
		$this->load->model('paquetes_model');
		$this->load->model('productos_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$pageKey = 'mapa_' . $id_minisitio;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);

		if(!$proveedor['latitud'] || !$proveedor['longitud']){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$prov_vencido = 0;
		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		if(empty($_GET['hash'])){//-- Sumo Visitas para MS vencidos y online. --//
			$this->visitas_model->visitas($proveedor['id_rubro'], 203, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			$cantidad_consultados = 5;

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);

			$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

			$resultados = $this->config_library->resultados;

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,50,1,'',4);
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,50,1,'',2);
			}

			$dir = $this->parseo_library->dir_html_strong($proveedor['direccion']);

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$meta_title = 'Mapa de ubicación de  ' . $proveedor['subdominio'] . ' | ' . $proveedor['rubro'] . ' | Casamientos Online';
			if(strlen($meta_title) > 65){
				$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
			}

			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/ubicacion',
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'dir'						=> $dir,
				'fotos'   	  				=> $fotos,
				'id_seccion' 	   	  		=> $id_seccion,
				'meta_descripcion' 	  		=> 'Mapa de ubicación de ' . $proveedor['subdominio'] . '. Te decimos donde queda y todo lo que te ofrece en ' . $proveedor['rubro'] . '!',
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
				'meta_title'	   	  		=> $meta_title,
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq2,
				'proveedores'				=> $proveedores,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'solapa' 				    => $solapa,
				'ubicacion'		      		=> 'minisitio_mapa',
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['data_form'] 		= $form['data_form'];
		$data['datos_viaje'] 	= $form['datos_viaje'];
		$data['fecha_variable'] = $form['fecha_variable'];
		$data['hidden'] 		= $form['hidden'];
		$data['localidades'] 	= $form['localidades'];
		$data['provincias'] 	= $form['provincias'];
		$data['salon_elegido'] 	= $form['salon_elegido'];
		$data['proveedor'] 		= $proveedor;

		$this->load->view('minisitio_mapa', $this->config_library->send_data_to_view($data));
	}

	public function minisitio_testimonios($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);
			$id_minisitio = $res['id_minisitio'];
		}
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->driver('cache');

		$this->load->model('rubros_model');
		$this->load->model('paquetes_model');
		$this->load->model('productos_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$pageKey = 'testimonios_' . $id_minisitio;

		$prov_vencido = 0;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);

		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		if(empty($_GET['hash'])){//-- Sumo Visitas para MS vencidos y online. --//
			$this->visitas_model->visitas($proveedor['id_rubro'], 204, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			$cantidad_consultados = 5;

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);
			$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$resultados 	= $this->config_library->resultados;
			$comentarios 	= $this->proveedores_model->get_proveedor_comentarios($id_minisitio);
			$puntajes 		= $this->parseo_library->promedios_puntajes_comentarios($comentarios);

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',4);
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',2);
			}

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$meta_title = 'Testimonios de ' . $proveedor['subdominio'] . ' | ' . $proveedor['rubro'] . ' | Casamientos Online';
			if(strlen($meta_title) > 65){
				$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
			}

			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/testimonios',
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'comentarios'				=> $comentarios,
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'fotos'   	  				=> $fotos,
				'id_seccion' 	   	  		=> $id_seccion,
				'meta_descripcion' 	  		=> 'Enterate lo que opinan otros usuarios sobre ' . $proveedor['subdominio'] . '. Encontrá aquí los testimonios de cómo les fue.',
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
				'meta_title'	   	  		=> $meta_title,
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq2,
				'proveedores'				=> $proveedores,
				'puntajes'					=> $puntajes,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'solapa' 				    => $solapa,
				'ubicacion'		      		=> 'minisitio_testimonios',
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['data_form'] 		= $form['data_form'];
		$data['datos_viaje'] 	= $form['datos_viaje'];
		$data['fecha_variable'] = $form['fecha_variable'];
		$data['hidden'] 		= $form['hidden'];
		$data['localidades'] 	= $form['localidades'];
		$data['provincias'] 	= $form['provincias'];
		$data['salon_elegido'] 	= $form['salon_elegido'];
		$data['proveedor'] 		= $proveedor;

		$this->load->view('minisitio_testimonios', $this->config_library->send_data_to_view($data));
	}

	public function minisitio_preguntas_frecuentes($id_minisitio = NULL, $subdominio = ''){
		$this->load->model('proveedores_model');
		if($subdominio){
			$res = $this->proveedores_model->get_minisitio_subdominio($subdominio);
			$id_minisitio = $res['id_minisitio'];
		}
		if(!$id_minisitio) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$this->load->driver('cache');

		$this->load->model('rubros_model');
		$this->load->model('proveedores_model');
		$this->load->model('paquetes_model');
		$this->load->model('productos_model');
		$this->load->model('navegacion_model');
		$this->load->model('visitas_model');
		$this->load->model('usuarios_model');
		$this->load->model('generales_model');
		$this->load->model('caracteristicas_model');
		$this->load->model('stats_model');

		$this->load->library('parseo_library');

		$pageKey = 'preguntas_frecuentes_' . $id_minisitio;

		$prov_vencido = 0;

		$proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
		$proveedor['telefonos'] = $this->parseo_library->procesar_telefonos($proveedor['telefonos']);

		$this->config_library->cambiar_sucursal_comercial($proveedor['id_sucursal']);

		$prov_vencido = $this->config_library->filtro_de_url_minisitio($id_minisitio, $proveedor, $prov_vencido);

		if($prov_vencido){
			redirect('http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url']);
		}

		if(empty($_GET['hash'])){//-- Sumo Visitas para MS vencidos y online. --//
			$this->visitas_model->visitas($proveedor['id_rubro'], 205, isset($proveedor['id_proveedor'])?$proveedor['id_proveedor']:'');
			$this->visitas_model->sumar_visita_proveedor();
		}

		$form = $this->parseo_library->minisitio_form_data($id_minisitio, $proveedor);

		$stat_data = array(
			'id_proveedor'	=> $proveedor['id_proveedor'],
			'id_user' 		=> ((isset($_SESSION['id_user'])&&$_SESSION['id_user'])?$_SESSION['id_user'] : 'NULL'),
			'ip'			=> $_SERVER['REMOTE_ADDR'],
			'url'			=> $this->navegacion_model->getURL(),
			'referida'		=> !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
		);
		$this->stats_model->add($stat_data);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			$cantidad_consultados = 5;

			$id_seccion = $this->navegacion_model->get_seccion($proveedor['id_rubro']);
			$ms_paq2 = $this->paquetes_model->get_paquetes_integrados('nivel', 10, $id_minisitio);

			$resultados 	= $this->config_library->resultados;
			$comentarios 	= $this->proveedores_model->get_proveedor_comentarios($id_minisitio);
			$puntajes 		= $this->parseo_library->promedios_puntajes_comentarios($comentarios);

			$solapa = (isset($_GET['solapa'])&&$_GET['solapa'])?$_GET['solapa']:'';

			if (isset($proveedor['cupones'])&&$proveedor['cupones']){
				$solapa = (!empty($solapa))? $solapa : 'cupones';
			}

			$this->caracteristicas_model->caracteristicas($id_minisitio);
			$cant_caracs = $this->caracteristicas_model->tiene_caracs();
			$caracs = $this->caracteristicas_model->get_caracs_proveedor();

			if (!empty($_GET['hash'])){
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',4);
			}else{
				$fotos = $this->config_library->get_fotos($id_minisitio,1,1,'',2);
			}

			$faqs 			 = $this->proveedores_model->get_faqs($proveedor['id_proveedor'], $proveedor['id_rubro']);
			$faqs_respuestas = $this->proveedores_model->get_faqs_respuestas($proveedor['id_rubro']);
			$faqs_rangos 	 = $this->proveedores_model->get_faqs_rangos();
			$faqs_parseado 	 = $this->parseo_library->parsear_faqs($faqs, $faqs_respuestas, $faqs_rangos);

			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
	        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
	        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
	        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
	        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
	        asort($elementos_random);
	        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
	        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
	        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			$proveedores = $this->proveedores_model->get_proveedores($proveedor['id_rubro'], 'nivel', 0, '1', '', 0, 0, $proveedor['id_proveedor']);
			$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores);

			$rubro   = $this->rubros_model->get_rubro($proveedor['id_rubro']);
			if(isset($rubro[0])&&$rubro[0]) $rubro = $rubro[0];
			$rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$meta_title = $proveedor['subdominio'] . ': preguntas frecuentes y respuestas útiles | Casamientos Online';
			if(strlen($meta_title) > 65){
				$meta_title = str_replace(' | Casamientos Online', '', $meta_title);
			}


			$data = array(
				'elementos_random'			=> $elementos_random,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'sucursal_redirect'			=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento',
				'canonical'					=> 'http://' . $proveedor['subdominio'] . '.' . DOMAIN . '/' . $rubro['url'] . '/preguntas-frecuentes',
				'cant_caracs'	   	  		=> $cant_caracs,
				'caracs'		   	  		=> $caracs,
				'comentarios'				=> $comentarios,
				'config'					=> array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
				'faqs'						=> $faqs_parseado,
				'fotos'   	  				=> $fotos,
				'id_seccion' 	   	  		=> $id_seccion,
				'meta_descripcion' 	  		=> 'Todo lo que necesitas saber sobre ' . $proveedor['subdominio'] . '. Cómo trabaja, qué servicios brinda y mucho más sobre '.$proveedor['rubro'].' encontralo aquí!',
				'meta_img'					=> !empty($fotos[0]) ? 'http://media.casamientosonline.com/' . ($fotos[0]['imagen'] ? 'images/' . $fotos[0]['imagen'] : 'logos/' . $proveedor["logo"] ) : '',
				'meta_keywords'    	  		=> 'Casamientos Online, casamientos, bodas, novios, novias, '.$proveedor['rubro'].', '.$proveedor['sucursal'].', Argentina,'.$proveedor['proveedor'].', '.$proveedor['meta_keywords'],
				'meta_title'	   	  		=> $meta_title,
				'minisitio'  	   	  		=> TRUE,
				'ms_paq2'					=> $ms_paq2,
				'proveedores'				=> $proveedores,
				'puntajes'					=> $puntajes,
				'resultados'	   	  		=> $resultados,
				'rubro'						=> $rubro,
				'solapa' 				    => $solapa,
				'ubicacion'		      		=> 'minisitio_preguntas_frecuentes',
				'twitter_title'				=> $proveedor['proveedor'],
				'twitter_descripcion'		=> $proveedor['breve'],
				'twitter_image'         	=> 'http://media.casamientosonline.com/' . ($proveedor['portada'] ? ('logos/' . $proveedor['portada']) : ('images/' . str_replace('@', '.', $proveedor['imagen_gde'])))
			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['data_form'] 		= $form['data_form'];
		$data['datos_viaje'] 	= $form['datos_viaje'];
		$data['fecha_variable'] = $form['fecha_variable'];
		$data['hidden'] 		= $form['hidden'];
		$data['localidades'] 	= $form['localidades'];
		$data['provincias'] 	= $form['provincias'];
		$data['salon_elegido'] 	= $form['salon_elegido'];
		$data['proveedor'] 		= $proveedor;

		$this->load->view('minisitio_preguntas_frecuentes', $this->config_library->send_data_to_view($data));
	}

	public function pyp($tipo = 'productos', $id_grupo = NULL, $id_rub = 0, $pag = 1, $filtros = '', $rubro_nombre = ''){
		$this->load->driver('cache');

		$this->load->model('banners_model');
		$this->load->model('rubros_model');
		$this->load->model('proveedores_model');
		$this->load->library('parseo_library');

		$pageKey = 'todos_los_' . $tipo . ($id_grupo ? ('_grupo' . $id_grupo) : '') . ($id_rub ? ('_rubro' . $id_rub) : '') . ($pag ? ('_pag' . $pag) : '') . ($filtros ? ('_filtros' . $filtros) : '');

		$grupo = $this->rubros_model->get_grupo($id_grupo);
		if(isset($grupo['id_sucursal'])) $this->config_library->cambiar_sucursal_comercial($grupo['id_sucursal']);

		if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

			if ($id_rub){
				$id_rubro = $id_rub;
				$pyp_id_sucursal = 0;
			} else {
				$id_rubro = 0;
				$pyp_id_sucursal = $this->session->userdata('id_sucursal');
			}

			$secciones = array(
				1  => 168,
				2  => 240,
				3  => 292,
				4  => 416,
				5  => 544,
				6  => 610,
				7  => 723,
				8  => 851,
				10 => 905,
				11 => 958,
				12 => 1010
			);

			$banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);

			if (!$filtros){
				$pos = strpos(urldecode($_SERVER['REQUEST_URI']),'|');
				$filtros = ($pos === false)? '' : substr(urldecode($_SERVER['REQUEST_URI']),$pos);
			}

			$cantidad = 25;
			$pagina   = $pag;

			$rubro = $this->rubros_model->get_rubro($id_rubro);
			$rubro = isset($rubro[0]) && $rubro[0]?$rubro[0]:'';
			if($rubro) $rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);

			$data_pyp = $this->config_library->get_tipo_pyp($tipo, array(
				'id_rubro' 		  => $id_rubro,
				'filtros' 		  => ltrim(urldecode($filtros), '|'),
				'pyp_id_sucursal' => $pyp_id_sucursal,
				'cantidad'		  => $cantidad,
				'pagina'		  => $pagina,
				'id_grupo'		  => $id_grupo
			), TRUE);

			$paginas_title = '';
			$meta_keywords = '';
			if($id_rubro){
				$meta_provs = $this->proveedores_model->get_proveedores($id_rubro, 'nivel', 0, 1, $filtros, 1);
				if(is_array($meta_provs)) foreach($meta_provs as $mp){
					$meta_keywords .= ', '.$mp['proveedor'];
				}
			}
			if ($id_rubro && !$id_grupo){
				$title = ucfirst($tipo) . ' de '.$rubro['rubro'].' para tu casamiento en '.$rubro['sucursal'].' | Casamientos Online';
				$descripcion = 'Descubrí toda la variedad de productos de '.$rubro['rubro'].' pensados para tu casamiento en '.$rubro['sucursal'].'. Organizá tu casamiento fácil y rápido!';
				if($tipo == 'promociones') $descripcion = 'Aprovechá las promociones de '.$rubro['rubro'].' seleccionadas especialmente para casamientos en '.$rubro['sucursal'].'. Conocé todo lo que tenemos para vos!';
				if($tipo == 'paquetes')    $descripcion = 'Aprovechá los paquetes de '.$rubro['rubro'].' seleccionados especialmente para casamientos en '.$rubro['sucursal'].'. Conocé todo lo que tenemos para vos!';
				$keywords = $rubro['rubro'].' en '.$rubro['sucursal'].', Productos, Servicios, Promociones, organización Casamientos, Bodas, Civiles, Fiestas, Eventos, guia de casamientos, novios, novias, Casamientosonline'.(isset($meta_keywords)&&$meta_keywords?$meta_keywords:'');
			} else {
				$title = ucfirst($tipo) . ' para tu casamiento en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
				$descripcion = 'Descubrí toda la variedad de productos pensados para tu casamiento en ' . $this->config_library->sucursal['sucursal'] . '. En Casamientos Online los filtramos y ordenamos para vos.';
				if($tipo == 'promociones') $descripcion = 'Descubrí las promociones especiales que tenemos para tu casamiento en ' . $this->config_library->sucursal['sucursal'] . '. No te las pierdas!';
				if($tipo == 'paquetes')    $descripcion = 'Descubrí los paquetes especiales que tenemos para tu casamiento en ' . $this->config_library->sucursal['sucursal'] . '. No te los pierdas!';
				$keywords = 'Productos, Servicios, Promociones, organización de Casamientos, Bodas, Civiles, Fiestas, Eventos, guia de casamientos, novios, novias, salones de fiesta, catering, participaciones, fotógrafos, disc jockeys para bodas, vestidos de novia, vestidos de fiesta, '.$this->config_library->sucursal['sucursal'];
			}
			if($id_grupo){
				if($id_rubro){
					$title = $rubro['rubro'].' en '.$rubro['sucursal'].' | Casamientos Online';
					if($tipo == 'promociones') $title = 'Promociones de ' . $grupo['nombre'] . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
					if($tipo == 'paquetes')    $title = 'Paquetes de ' . $grupo['nombre'] . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
					$descripcion = 'Encontrá todas las opciones de ' . $rubro['rubro'].' que tenemos para vos en '.$rubro['sucursal'].'. Organizár tu casamiento nunca fue tan fácil!';
					if($tipo == 'promociones') $descripcion = 'Las mejores promociones en ' . $rubro['rubro'] . ' están aquí! Si estás en '.$rubro['sucursal'].' esta es tu oportunidad!';
					if($tipo == 'paquetes')    $descripcion = 'Los mejores paquetes en ' . $rubro['rubro'] . ' están aquí! Si estás en '.$rubro['sucursal'].' esta es tu oportunidad!';
					$keywords = $rubro['rubro'].' en '.$rubro['sucursal'].', Productos, Servicios, Promociones, organización Casamientos, Bodas, Civiles, Fiestas, Eventos, guia de casamientos, novios, novias, Casamientosonline'.(isset($meta_keywords)&&$meta_keywords?$meta_keywords:'');
				}else{
					$title = $grupo['nombre'] . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
					if($tipo == 'promociones') $title = 'Promociones de ' . $grupo['nombre'] . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
					if($tipo == 'paquetes')    $title = 'Paquetes de ' . $grupo['nombre'] . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
					$descripcion = 'Encontrá ' . $grupo['nombre'] . ' en ' . $this->config_library->sucursal['sucursal'] . ' de la manera más fácil y rápida! Qué placer organizar tu boda así!';
					if($tipo == 'promociones') $descripcion = 'Las mejores promociones en ' . $grupo['nombre'] . ' están aquí! Si estás en ' . $this->config_library->sucursal['sucursal'] . ' esta es tu oportunidad!';
					if($tipo == 'paquetes')    $descripcion = 'Los mejores paquetes en ' . $grupo['nombre'] . ' están aquí! Si estás en ' . $this->config_library->sucursal['sucursal'] . ' esta es tu oportunidad!';
					$keywords = 'Productos, Servicios, Promociones, organización de Casamientos, Bodas, Civiles, Fiestas, Eventos, guia de casamientos, novios, novias, salones de fiesta, catering, participaciones, fotógrafos, disc jockeys para bodas, vestidos de novia, vestidos de fiesta, '.$this->config_library->sucursal['sucursal'];
				}
			}

			$filtros_aplicados = $this->config_library->get_filtros_aplicados($filtros, $tipo=='paquetes'?3:2, $id_rubro);
            ##-- Filtros por Zonas --##
            $filtros_aplicados_zona = $this->config_library->get_filtros_aplicados($filtros,1,$id_rubro);
            if($filtros_aplicados_zona){
                foreach($filtros_aplicados_zona as $item){
                    array_push($filtros_aplicados,$item);
                }
            }
            ##-- --##


			if($id_grupo){
				$url_base = $this->config_library->sucursal['nombre_seo'] . '/' . $tipo . '/_?_' . '_CO_a' . $id_grupo;
				$grupo_nombre_seo = $this->parseo_library->clean_url($grupo['nombre']);
				if(!$id_rub){
					$url_base_p = $this->config_library->sucursal['nombre_seo'] . '/' . $tipo . '/' . $grupo_nombre_seo . '_CO_a' . $id_grupo;
				}else{
					$url_base_p = $url_base;
					$url_base = $this->config_library->sucursal['nombre_seo'] . '/' . $tipo . '/' . $grupo_nombre_seo . '/_?_' . '_CO_a' . $id_grupo;
				}
				$url_paginacion = $url_base_p . '/[pag]' . ($id_rubro ? ('-r' . $id_rubro) : '') . ($filtros ? ('-' . $filtros) : '');



				if(!$id_rub){
					switch ($tipo) {
						case 'promociones':
							$canonical = base_url(str_replace('_?_', $grupo_nombre_seo, $url_base));
							break;
						case 'productos':
							$canonical = base_url(str_replace('_?_', $grupo_nombre_seo, $url_base));

							break;
						case 'paquetes':
							$canonical = base_url(str_replace('_?_', $grupo_nombre_seo, $url_base));
							break;
					}
				}else{
					switch ($tipo) {
						case 'promociones':
							$canonical = base_url(str_replace('/_?_', '', $url_base));
							break;
						case 'productos':
							$canonical = base_url(str_replace('/_?_', '', $url_base));
							break;
						case 'paquetes':
							$canonical = base_url(str_replace('/_?_', '', $url_base));
							break;
					}
				}

			}else{
				switch ($tipo) {
					case 'promociones':
						$codigo = 'pr';
						break;
					case 'productos':
						$codigo = 'pd';
						break;
					case 'paquetes':
						$codigo = 'pa';
						break;
				}

				$url_base = $this->config_library->sucursal['nombre_seo'] . '/' . $tipo . '/_?_' . '_CO_' . $codigo;
				if(!$id_rub){
					$url_base_p = $this->config_library->sucursal['nombre_seo'] . '/' . $tipo . '_CO_' . $codigo;
				}else{
					$url_base_p = $url_base;
				}
				$url_paginacion = $url_base_p . '/[pag]' . ($id_rubro ? ('-r' . $id_rubro) : '') . ($filtros ? ('-' . $filtros) : '');


				switch ($tipo) {
					case 'promociones':
						$canonical = explode('_pr', $_SERVER['REQUEST_URI']);
						if(!empty($canonical[0])) $canonical = base_url($canonical[0] . '_pr/1-r'.$id_rubro);
						break;
					case 'productos':
						$canonical = explode('_pd', $_SERVER['REQUEST_URI']);
						if(!empty($canonical[0])) $canonical = base_url($canonical[0] . '_pd/1-r'.$id_rubro);
						break;
					case 'paquetes':
						$canonical = explode('_pa', $_SERVER['REQUEST_URI']);
						if(!empty($canonical[0])) $canonical = base_url($canonical[0] . '_pa/1-r'.$id_rubro);
						break;
				}
			}


			$item_desde = ($pagina-1) * $cantidad + 1;
			$item_hasta = ($pagina*$cantidad < $data_pyp['resultados'])? $pagina*$cantidad : $data_pyp['resultados'];
			$paginas = ceil($data_pyp['resultados']/$cantidad);

			$paginas_margen = $this->config->item('paginas_margen');
			$paginador_da = $this->parseo_library->paginador_desde_hasta($pagina, $paginas, $paginas_margen);

			$url_base_ultima_parte = explode('?',$_SERVER['REQUEST_URI']);
			$url_base_ultima_parte = $url_base_ultima_parte[0];

			$url_actual = $_SERVER['REQUEST_URI'];

			if(!$id_grupo) $id_grupo = '';

			$data = array(
				'banners'					=> $banners,
				'canonical'					=> $canonical,
				'cantidad'                  => $cantidad,
				'desde'						=> $paginador_da['desde'],
				'filtros'					=> $filtros,
				'filtros_aplicados'			=> $filtros_aplicados,
				'filtros_pendientes'		=> $data_pyp['filtros_pendientes'],
				'filtros_rubros'     		=> (!empty($pyp_id_sucursal)) ? $this->rubros_model->get_pyp_rubros($pyp_id_sucursal, $data_pyp['promo'], $id_grupo, $tipo=='paquetes'? 'site_paquetes_activos' : 'site_productos_activos') : '',
				'hasta'						=> $paginador_da['hasta'],
				'item_desde'				=> $item_desde,
				'item_hasta'				=> $item_hasta,
				'ic'						=> $this->parseo_library->equivalencia_iconos(),
				'id_rubro'					=> $id_rubro,
				'meta_descripcion'			=> $descripcion,
				'meta_keywords'				=> $keywords,
				'meta_title'				=> $title,
				'mostrar_cantidad_filtros'  => $data_pyp['mostrar_cantidad_filtros'],
				'orden'						=> $data_pyp['orden'],
				'pagina'					=> $pagina,
				'paginar'					=> $data_pyp['paginar'],
				'paginas'					=> $paginas,
				'productos'					=> $data_pyp['productos'],
				'resultados'				=> $data_pyp['resultados'],
				'rubro'						=> $rubro,
				'tipo_listado'              => $tipo,
				'url_base'					=> $url_base,
				'url_filtros'				=> base_url(str_replace(('/' . $pag .'-'), '/1-',$url_base_ultima_parte)) . (!$filtros ? '-' : ''),
				'url_paginacion'			=> base_url($url_paginacion),
				'url_base_ultima_parte' 	=> $url_base_ultima_parte,
				'ordenar'                   => $this->parseo_library->criterios_ordenar($tipo),
				'paginas_margen'			=> $paginas_margen,
				'request_uri_base'		    => base_url($url_actual),
				'sucursal_seo'				=> TRUE,
				'sucursal_redirect'			=> '/fiestas-de-casamiento'

			);

			if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$data['grupo'] = $grupo;

		$this->load->view('todos_los_productos', $this->config_library->send_data_to_view($data));
	}

	public function mapa_de_guia($id_rubro = NULL){
		if(!$id_rubro) redirect(base_url('/'));

		$this->load->model('navegacion_model');
		$this->load->model('proveedores_model');
		$this->load->model('banners_model');
		$this->load->model('rubros_model');
		$this->load->model('proveedores_model');

		$ids_prehome = array(17, 30, 155, 154);

		$rubro = $this->rubros_model->get_rubro($id_rubro);
		$rubro = $rubro[0];

		$sucursal = $this->navegacion_model->getSucursal($this->session->userdata('id_sucursal'));

		$res = $this->proveedores_model->get_seccion($id_rubro);
		$id_seccion = $res['id_seccion'];

		$banners_sitio = $this->banners_model->views($id_seccion);

		switch($this->session->userdata('id_sucursal')){
			case 1:
				$banners_sitio2 = $this->banners_model->views(123);
				break;
			case 2:
				$banners_sitio2 = $this->banners_model->views(179);
				break;
			case 3:
				$banners_sitio2 = $this->banners_model->views(208);
				break;
			case 4: // 407 es el id en sys_secciones de "Guia de Empresas", que esta relacionado con la sucursal 4 en sys_rel_secciones_portales
				$banners_sitio2 = $this->banners_model->views(407);
			break;
			case 6:
				$banners_sitio2 = $this->banners_model->views(490);
			break;
			case 5:
				$banners_sitio2 = $this->banners_model->views(559);
			break;
			case 7:
				$banners_sitio2 = $this->banners_model->views(707);
			break;
			case 8:
				$banners_sitio2 = $this->banners_model->views(889);
			break;
			case 10:
				$banners_sitio2 = $this->banners_model->views(890);
			break;
			case 11:
				$banners_sitio2 = $this->banners_model->views(836);
			break;
			case 12:
				$banners_sitio2 = $this->banners_model->views(888);
			break;
		}

		$banners = $banners_sitio + $banners_sitio2;

		$filtros_pendientes = $this->rubros_model->get_rubro_filtros_pendientes($id_rubro);
        $filtros_aplicados  = $this->config_library->get_filtros_aplicados(array(),1,$id_rubro);
        $proveedores = $this->proveedores_model->get_proveedores($id_rubro, 'nivel', 25, 1);
        $meta_keywords = '';
        if(!empty($proveedores)) foreach($proveedores as $mp){
			$meta_keywords .= ', '.$mp['proveedor'];
		}

		if(!in_array($id_rubro, $ids_prehome)) redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));

		$data = array(
			'banners'			=> $banners,
			'canonical'			=> base_url($_SERVER['REQUEST_URI']),
			'id_rubro' 			=> $id_rubro,
			'meta_descripcion'	=> 'Aquí encontrarás nuestro mapa de ' . $rubro['rubro'] . ' en ' . $sucursal['sucursal'] . '. Ingresá y buscá opciones fácil y rápido.',
			'meta_keywords' 	=> $rubro['rubro'] . ', ' . $rubro['sucursal'] . ', casamientos, novios, bodas, Argentina' . $meta_keywords,
			'meta_title' 		=> 'Mapa de ' . $rubro['rubro'] . ' para tu Casamiento en ' . $sucursal['sucursal'] . ' | Casamientos Online',
			'sucursal_redirect'	=> '/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'
		);

		$this->load->view('prehome_salones_quintas.php', $this->config_library->send_data_to_view($data));
	}

	public function sucursal($id_sucursal){
		if(isset($id_sucursal) && $id_sucursal){
			$this->load->model('rubros_model');

			$this->config_library->cambiar_sucursal($id_sucursal); // Cambiamos la sesion id_sucursal

			$meta_rubros = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r2.orden, r1.rubro', 1);

			if($meta_rubros){
				$meta_keywords = "";
				foreach ($meta_rubros as $mr){
					$meta_keywords .= ', '.$mr;
				}
			}

			$data = array(
				'meta_descripcion' => 'Guia de empresas y servicios para organizar tu casamiento en '.$this->config_library->sucursal['sucursal'],
				'meta_keywords'    => 'Casamientos Online, casamientos, novios, bodas, '.$this->config_library->sucursal['sucursal'].', Argentina'.$meta_keywords,
				'meta_title'	   => 'Guía de empresas y servicios de '.$this->config_library->sucursal['sucursal'].' - Casamientos Online'
			);

			if($_GET['redirect']){
				redirect(base_url($_GET['redirect']));
			}else{
				$this->load->view('home', $this->config_library->send_data_to_view($data));
			}
		}else{ // Si se entra a /proovedores/buscar sin ningun parametro ni variables $_POST lo redireccionamos a home porque significa que estan entrando manualmente
			redirect(base_url('/'));
		}
	}

	public function como_anunciar(){
		$this->load->model('navegacion_model');

		$success = FALSE;

		if($_POST){
			$this->load->library('form_validation');

	        $error_required = array(
	                'required' => 'Debe ingresar un %s.',
	        );

	        $error_required_hidden = array(
				'required' => 'Su formulario contiene errores'
			);

	        $this->form_validation->set_rules('accion', 'accion', 'required', $error_required_hidden);
	        $this->form_validation->set_rules('id_sucursal', 'id_sucursal', 'required', $error_required_hidden);
	        $this->form_validation->set_rules('apellido', 'apellido', 'required', $error_required_hidden);
	        $this->form_validation->set_rules('infonovias', 'infonovias', 'required', $error_required_hidden);

	        $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
	        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
	                'required'      => 'Debe ingresar un %s.',
	                'valid_email'   => 'Debe ingresar un %s válido.',
	        ));
	        $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
	        $this->form_validation->set_rules('empresa', 'Nombre de empresa', 'required', $error_required);
	        $this->form_validation->set_rules('comentario', 'Comentario', 'required', $error_required);
	        $this->form_validation->set_rules('id_rubro', 'Rubro', 'required', $error_required);
	        $this->form_validation->set_rules('opcion_publicacion', 'sitio de publicación', 'required', $error_required);
	        $this->form_validation->set_rules('id_provincia', 'Provincia', 'required', $error_required);
	        $this->form_validation->set_rules('id_localidad', 'Localidad', 'required', $error_required);


			if($this->form_validation->run() == TRUE){ // success
	        	$this->load->library('procesos_library');

	        	$this->procesos_library->procesar_nuevo_proveedor($_POST);

	        	$success = TRUE;
	        }
		}

		$provincias = $this->navegacion_model->get_provincias(0);
        $localidades = array();
        if(!empty($_POST['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($_POST['id_provincia']);
        }

        $combo_rubros = array();
        if(isset($_POST['id_sucursal']) && $_POST['id_sucursal']) $combo_rubros = $this->navegacion_model->get_rubros_combo($_POST['id_sucursal']);

        if($success){
        	$extra_canonical = '#gracias_como_anunciar';
        	$meta_title = 'Gracias por comunicarte con Casamientos Online!';
        	$meta_descripcion = 'Gracias por solicitar información! Ahora navegá y comenzá a generar ideas de cómo están haciendo otros proveedores para recibir más prospectos.';
        }else{
        	$extra_canonical = '';
        	$meta_title = 'Anunciate en Casamientos Online y Vendé Más! Aquí sabrás cómo hacerlo.';
        	$meta_descripcion = 'Aumentá tus ventas anunciandote con Casamientos Online! El portal de novios más grande de Argentina! Solicitá información aquí.';
        }

		$data = array(
			'canonical'			=> base_url($_SERVER['REQUEST_URI']) . $extra_canonical,
			'tipo' 				=> 'como_anunciar',
			'meta_title' 		=> $meta_title,
			'meta_descripcion'	=> $meta_descripcion,
			'meta_keywords'		=> 'Casamientos, bodas, organizacion casamiento, guía casamiento, evento, fiesta, festejo, novia, novios, salon de fiesta, quintas eventos, vestidos novia, catering, souvenirs, participaciones, casamiento civil, centros de mesa, buenos aires, rosario, sante fe, cordoba, mendoza, argentina, fotos, videos, audios, multimedia, horoscopo, comunidad, blogs, negocios, empresas, anunciar, como anunciar',
			'provincias'		=> $provincias,
			'localidades'		=> $localidades,
			'success'			=> $success,
			'combo_rubros'		=> $combo_rubros ? $combo_rubros : NULL
		);

		$this->load->view('como_anunciar', $this->config_library->send_data_to_view($data));
	}

	public function acerca_nuestro(){
		$data = array(
			'canonical'			=> base_url($_SERVER['REQUEST_URI']),
			'meta_title' 		=> 'Casamientos Online: Te contamos nuestra historia!',
			'meta_descripcion'	=> 'A veces nos gana la emoción, por eso también queremos contarte nuestra historia. Conocé cómo hicimos para facilitar la organización de tantos casamientos!',
			'meta_keywords'		=> 'Casamientos, bodas, organizacion casamiento, guía casamiento, evento, fiesta, festejo, novia, novios, salon de fiesta, quintas eventos, vestidos novia, catering, souvenirs, participaciones, casamiento civil, centros de mesa, buenos aires, rosario, sante fe, cordoba, mendoza, argentina, fotos, videos, audios, multimedia, horoscopo, comunidad, blogs, negocios, empresas, acerca de, acerca nuestro',
			'tipo' => 'acerca_nuestro'
		);

		$this->load->view('acerca_nuestro', $this->config_library->send_data_to_view($data));
	}

	public function expo_novias_proveedor(){
		$success = FALSE;
		if($_POST){
			$this->load->library('form_validation');

			$error_required = array(
				'required' => 'Debe ingresar un %s.',
	        );

	        $error_required_hidden = array(
				'required' => 'Su formulario contiene errores'
			);

	        $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
	        $this->form_validation->set_rules('apellido', 'Apellido', 'required', $error_required);
	        $this->form_validation->set_rules('rubros', 'Rubro', 'required', $error_required);
	        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
	                'required'      => 'Debe ingresar un %s.',
	                'valid_email'   => 'Debe ingresar un %s válido.',
	        ));
	        $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
	        $this->form_validation->set_rules('comentario', 'Comentario', 'required', $error_required);

			if($this->form_validation->run() == TRUE){ // success
				if(!$this->config->item('procesos_off')){
					$this->load->library('procesos_library');

		        	$this->load->library('email');

					$this->email->from($_POST['email'], $_POST['nombre'] . " " . $_POST['apellido']);
					$this->email->to('gabriel@casamientosonline.com');
					$this->email->cc('maca@casamientosonline.com');

					$email_message = "Información del formulario.\n\n";
					$email_message .= "nombre: ". $_POST['nombre'] ."\n";
				    $email_message .= "apellido: ". $_POST['apellido'] ."\n";
				    $email_message .= "telefono: ". $_POST['telefono'] ."\n";
				    $email_message .= "rubros: ". $_POST['rubros'] ."\n";
				    $email_message .= "email: ". $_POST['email'] ."\n";
				    $email_message .= "comentarios: ". $_POST['comentario'] ."\n";

					$this->email->subject('Solicito informacion para estar en la Jornada con mi empresa');
					$this->email->message($email_message);

					$this->email->send();
				}

	        	$success = TRUE;
	        }
		}

		if($success){
			$extra_canonical = '#gracias';
			$meta_title = 'Gracias por comunicarte con Expo Novias de Casamientos Online';
			$meta_descripcion = 'Gracias por tu mensaje! Muy pronto serás contactado para participar de Expo Novias!';
		}else{
			$extra_canonical = '';
			$meta_title = 'Aprovechá Expo Novias y Vendé Más! Averiguá aquí cómo hacerlo.';
			$meta_descripcion = 'ExpoNovias: Particiá con tu empresa y generá muchos clientes. Mostrá todo lo que ofrecés en un stand dedicado a novias y casamientos. Reservá el tuyo hoy!';
		}


		$data = array(
			'canonical'			=> base_url($_SERVER['REQUEST_URI']) . $extra_canonical,
			'tipo' 				=> 'expo_novias_proveedor',
			'meta_title' 		=> $meta_title,
			'meta_descripcion' 	=> $meta_descripcion,
			'meta_keywords'		=> 'casamiento, boda, novios, organizacion del casamiento, casamientos online, empresas, Salones de Fiesta en Capital Federal',
			'success'			=> $success
		);

		$this->load->view('expo_novias_proveedor', $this->config_library->send_data_to_view($data));
	}

	public function contacto_proveedor($tipo = NULL){
		$title = 'Anunciate con Casamientos Online! Comenzá a vender tus servicios aquí';
		$descripcion = 'Contactanos y te facilitaremos toda la información para que publicites tu empresa y productos en nuestro Portal de Novios!';
		$extra_canonical = '';
		$view = 'contacto_proveedor';
		$ubicacion = '/empresas/contacto';

		$success = FALSE;
		$this->load->library('form_validation');
		$this->load->library('procesos_library');

		if($tipo == 'contacto'){
			$view = 'contacto';
			$ubicacion = '/contacto';

			if($_POST){
				$error_required = array(
					'required' => 'Debe ingresar un %s.',
		        );

		        $error_required_hidden = array(
					'required' => 'Su formulario contiene errores'
				);

		        $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
		        $this->form_validation->set_rules('apellido', 'Apellido', 'required', $error_required);
		        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
		                'required'      => 'Debe ingresar un %s.',
		                'valid_email'   => 'Debe ingresar un %s válido.',
		        ));
		        $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
		        $this->form_validation->set_rules('celular', 'celular', 'required', $error_required_hidden);
		        $this->form_validation->set_rules('comentario', 'Comentario', 'required', $error_required);

				if($this->form_validation->run() == TRUE){ // success
		        	if($tipo == 'contacto') $_POST['desde_contacto'] = TRUE;
		        	$this->procesos_library->procesar_sugerencia($_POST);
		        	$success = TRUE;
		        }
			}
			$title = 'Contactanos - Queremos Saber de Ti - Datos de Contacto de Casamientos Online ';
			$descripcion = 'Nos encantaría saber de ti! Contactate con nosotros. Aquí te decimos cómo!';
		}else{
			if($_POST){
				$error_required = array(
	                'required' => 'Debe ingresar un %s.',
		        );

		        $error_required_hidden = array(
					'required' => 'Su formulario contiene errores'
				);

		        $this->form_validation->set_rules('accion', 'accion', 'required', $error_required_hidden);
		        $this->form_validation->set_rules('id_sucursal', 'id_sucursal', 'required', $error_required_hidden);
		        $this->form_validation->set_rules('apellido', 'apellido', 'required', $error_required_hidden);
		        $this->form_validation->set_rules('infonovias', 'infonovias', 'required', $error_required_hidden);

		        $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
		        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
		                'required'      => 'Debe ingresar un %s.',
		                'valid_email'   => 'Debe ingresar un %s válido.',
		        ));
		        $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
		        $this->form_validation->set_rules('empresa', 'Nombre de empresa', 'required', $error_required);
		        $this->form_validation->set_rules('comentario', 'Comentario', 'required', $error_required);
		        $this->form_validation->set_rules('id_rubro', 'Rubro', 'required', $error_required);
		        $this->form_validation->set_rules('opcion_publicacion', 'Sitio de publicación', 'required', $error_required);
		        $this->form_validation->set_rules('id_provincia', 'Provincia', 'required', $error_required);
		        $this->form_validation->set_rules('id_localidad', 'Localidad', 'required', $error_required);
		        $this->form_validation->set_rules('sitio_web', 'Sitio web', 'required', $error_required);


				if($this->form_validation->run() == TRUE){ // success
		        	$this->procesos_library->procesar_nuevo_proveedor($_POST);

		        	$success = TRUE;
		        }
			}
		}

		$provincias = $this->navegacion_model->get_provincias(0);
        $localidades = array();
        if(!empty($_POST['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($_POST['id_provincia']);
        }

        if($success){
        	$extra_canonical = '#gracias';
        	$title = 'Gracias por comunicarte con Casamientos Online!';
        	$descripcion = 'Gracias por tu mensaje! Muy pronto serás contactado para publicitar en Casamientos Online!';
        }

		$data = array(
			'canonical'			=> base_url($_SERVER['REQUEST_URI']) . $extra_canonical,
			'tipo' 				=> 'contacto_proveedor',
			'meta_descripcion' 	=> $descripcion,
			'meta_keywords' 	=> 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina',
			'meta_title' 		=> $title,
			'localidades'		=> $localidades,
			'provincias'		=> $provincias,
			'success'			=> $success,
			'ubicacion'			=> $ubicacion
		);

		$this->load->view($view, $this->config_library->send_data_to_view($data));
	}

	public function prensa_proveedor(){
		$meta_descripcion = 'Casamientos Online es tan grande que los medios también nos aman! Aquí te dejamos un resumen de la cobertura de prensa.';
		$meta_title = 'Casamientos Online: Publicaciones de Prensa y novedades en los medios';

		$data = array(
			'canonical'			=> base_url($_SERVER['REQUEST_URI']),
			'tipo' 				=> 'prensa_proveedor',
			'meta_descripcion' 	=> $meta_descripcion,
			'meta_keywords' 	=> 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, prensa, proveedor, prensa de proveedor',
			'meta_title' 		=> $meta_title
		);
		$this->load->view('prensa_proveedor', $this->config_library->send_data_to_view($data));
	}

	public function gracias_landing(){
		$this->load->model('generales_model');
		$this->load->model('rubros_model');

		$this->load->library('parseo_library');

		$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
        asort($elementos_random);
        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

		$data = array(
			'elementos_random'			=> $elementos_random,
			'listado_seo_random'		=> $listado_seo_random,
			'listado_seo_random_guiones'=> $listado_seo_random_guiones,
			'meta_descripcion' 			=> 'Gracias Landing',
			'meta_keywords' 			=> 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, prensa, proveedor, prensa de proveedor',
			'meta_title' 				=> 'Gracias Landing'
		);
		$this->load->view('gracias_landing', $this->config_library->send_data_to_view($data));
	}
}