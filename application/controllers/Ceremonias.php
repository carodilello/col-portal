<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceremonias extends MY_Controller{
    private $banners = '';

	public function __construct(){
    	parent::__construct();
    	$this->load->library('config_library');
        $this->load->model('banners_model');

        $id_seccion = '';
        switch($this->session->userdata('id_sucursal')){
            case 1:
                $id_seccion = 376;
                break;
            case 2:
                $id_seccion = 377;
                break;
            case 3:
                $id_seccion = 378;
                break;
        }
        $this->banners = $this->banners_model->views($id_seccion);
	}

	public function index(){
        $data = array(
            'banners'           => $this->banners,
            'canonical'         => base_url($_SERVER['REQUEST_URI']),
            'meta_descripcion'  => 'Toda la información para organizar tu ceremonia y el civil, paso a paso y por categorías. Accedé a todo en un mismo lugar: Aquí!',
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil',
            'meta_title'        => 'Información y requisitos sobre ceremonias y civiles | Casamientos Online'
        );

    	$this->load->view('ceremonias_y_civil', $this->config_library->send_data_to_view($data));
	}

    public function registros_civiles($tipo = ''){
        if($tipo == 'directorio'){
            $canonical = base_url($_SERVER['REQUEST_URI']);
            $meta_descripcion = 'Accedé a nuestro listado de registros civiles en Argentina: Ubicación, trámites y tiempos que debes considerar.';
            $meta_keywords = 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil';
            $meta_title = 'Listado de Registros Civiles en Argentina | Casamientos Online';
            $view = 'registros_civiles_directorio';
        }else{
            $canonical = base_url($_SERVER['REQUEST_URI']);
            $meta_descripcion = 'Información de registros civiles en Argentina: Dónde están, trámites y tiempos que debes considerar.';
            $meta_keywords = 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil';
            $meta_title = 'Requisitos e Información del Registro Civil en Argentina | Casamientos Online';
            $view = 'registros_civiles';
        }

        $data = array(
            'banners'           => $this->banners,
            'meta_descripcion'  => $meta_descripcion,
            'meta_keywords'     => $meta_keywords,
            'meta_title'        => $meta_title
        );

        $this->load->view($view, $this->config_library->send_data_to_view($data));
    }

    public function iglesias($tipo = ''){
        $this->load->model('generales_model');

        $barrios = $this->generales_model->get_barrios();
        $directorios = $this->generales_model->get_iglesias();

        if($tipo == 'directorio'){
            $canonical = base_url($_SERVER['REQUEST_URI']);
            $meta_descripcion = 'Accedé a nuestro listado de iglesias católicas en Argentina: Dónde están, trámites y tiempos que debes considerar para tu casamiento.';
            $meta_keywords = 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil, iglesias';
            $meta_title = 'Listado de Iglesias Católicas en Argentina | Casamientos Online';
            $view = 'iglesias_directorio';
        }else{
            $canonical = base_url($_SERVER['REQUEST_URI']);
            $meta_descripcion = 'Todo lo que necesitas saber de las iglesias católicas en Argentina: Dónde están, trámites y tiempos que debes considerar para tu casamiento.';
            $meta_keywords = 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil, iglesias';
            $meta_title = 'Información de Iglesias Católicas en Argentina | Casamientos Online';
            $view = 'iglesias';
        }

        $data = array(
            'banners'           => $this->banners,
            'barrios'           => $barrios,
            'directorios'       => $directorios,
            'meta_descripcion'  => $meta_descripcion,
            'meta_keywords'     => $meta_keywords,
            'meta_title'        => $meta_title
        );

        $this->load->view($view, $this->config_library->send_data_to_view($data));
    }

    public function templos($tipo = ''){
        $this->load->model('generales_model');

        $barrios_ids = array();
        $directorios = $this->generales_model->get_templos(NULL, 'id_barrio');
        foreach ($directorios as $value) {
            $barrios_ids[] = $value['id_barrio'];
        }
        $barrios = $this->generales_model->get_barrios($barrios_ids);
        $directorios = $this->generales_model->get_templos();

        if($tipo == 'directorio'){
            $canonical = base_url($_SERVER['REQUEST_URI']);
            $meta_descripcion = 'Accedé a nuestro listado de templos judios en Argentina: Dónde están, trámites y tiempos que debes considerar para tu casamiento.';
            $meta_keywords = 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil, templos, templos judíos';
            $meta_title = 'Listado de Templos Judios en Argentina | Casamientos Online';
            $view = 'templos_judios_directorio';
        }else{
            $canonical = base_url($_SERVER['REQUEST_URI']);
            $meta_descripcion = 'Todo lo que necesitas saber de templos judios en Argentina: Dónde están, trámites y tiempos que debes considerar para tu casamiento.';
            $meta_keywords = 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil, templos, templos judíos';
            $meta_title = 'Información de Templos Judios en Argentina | Casamientos Online';
            $view = 'templos_judios';
        }

        $data = array(
            'banners'           => $this->banners,
            'barrios'           => $barrios,
            'directorios'       => $directorios,
            'meta_descripcion'  => $meta_descripcion,
            'meta_keywords'     => $meta_keywords,
            'meta_title'        => $meta_title
        );

        $this->load->view($view, $this->config_library->send_data_to_view($data));
    }

    public function no_religiosas(){
        $data = array(
            'banners'           => $this->banners,
            'canonical'         => base_url($_SERVER['REQUEST_URI']),
            'meta_descripcion'  => 'Todo lo que necesitás saber para realizar una ceremonia no religiosa lo encontrás aquí! Organizar tu casamiento nunca fue tan fácil!',
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, Ceremonia, civil',
            'meta_title'        => 'Información de Ceremonias Laicas - No Religiosas | Casamientos Online'
        );

        $this->load->view('ceremonias_no_religiosas', $this->config_library->send_data_to_view($data));
    }
}