<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editorial extends MY_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('config_library');
    }


    public function home(){
        $this->load->driver('cache');

        $this->load->model('editorial_model');
        $this->load->model('rubros_model');
        $this->load->model('navegacion_model');
        $this->load->model('banners_model');

        $this->load->library('pagination');
        $this->load->library('parseo_library');

        $pageKey = 'notas_home';
        if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

            $nota_principal_o = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 2), TRUE, 1);
            $nota_principal   = $nota_principal_o[0];
            $notas_destacadas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 3), TRUE, 3);

            $secciones_home     = array(76, 81, 93, 98);
            $secciones_home_seo = array(76 => 'vestidos', 81 => 'belleza-y-estilo', 93 => 'la-organizacion', 98 => 'luna-de-miel');
            $notas_secciones    = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 4), TRUE, 12);
            $notas_secciones    = $this->parseo_library->secciones_para_home_notas($notas_secciones, $secciones_home);

            $mas_leidas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 7), TRUE, 4);

            $cant_ml = count($mas_leidas);

            if($cant_ml < 4){
                $mas_leidas_real = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('orden' => ' edtn.visitas DESC '), TRUE, 4-$cant_ml);
                $mas_leidas = array_merge($mas_leidas, $mas_leidas_real);
            }

            $tmp = explode('@', $nota_principal['secciones']);
            $id_seccion_de_principal = $tmp[2]?$tmp[2]:$tmp[0];

            $arr_keywords = array();
            foreach ($nota_principal_o as $u){
                $sec = explode('@',$u['secciones']);
                foreach ($sec as $s){
                    if (!empty($s) && !is_numeric($s)){
                            array_push($arr_keywords,$s);
                    }
                }
            }
            $secc = $this->navegacion_model->get_seccion($id_seccion_de_principal);

            $secciones = array(
                1  => 1068,
                2  => 1070,
                3  => 1071,
                4  => 1072,
                5  => 1073,
                6  => 1074,
                7  => 1075,
                8  => 1076,
                10 => 1077,
                11 => 1078,
                12 => 1079
            );

            $banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);

            $tit_sec     = ($secc) ? ' sobre '.(($secc['padre_seccion'])? $secc['seccion'].' en '.$secc['padre_seccion']: $secc['seccion']) : '';
            $descripcion = 'Tips para Fiestas divertidas y originales. Como Organizar tu Casamiento de día o noche, sencillos o diferentes, lo más económico posible. Actualizate acá!';
            $keywords    = ', notas, articulos, editorial, '.implode(', ',array_unique($arr_keywords));
            $title       = 'Los mejores consejos para tu casamiento están aquí! | Casamientos Online';

            $elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
            $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
            $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
            $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
            $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
            asort($elementos_random);
            $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
            $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
            $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

            $data = array(
                'canonical'                 => base_url($_SERVER['REQUEST_URI']),
                'elementos_random'          => $elementos_random,
                'listado_seo_random'        => $listado_seo_random,
                'listado_seo_random_guiones'=> $listado_seo_random_guiones,
                'mas_leidas'                => $mas_leidas,
                'meta_descripcion'          => $descripcion,
                'meta_keywords'             => 'Casamientos Online, casamientos, bodas, novios, novias, Argentina' . $keywords,
                'meta_title'                => $title,
                'nota_principal'            => $nota_principal,
                'notas_destacadas'          => $notas_destacadas,
                'notas_secciones'           => $notas_secciones,
                'secciones_home'            => $secciones_home,
                'secciones_home_seo'        => $secciones_home_seo,
                'meses'                     => $this->parseo_library->get_meses(),
                'listado_seo'               => $listado_seo,
                'banners'                   => $banners
            );

            if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
        }

        $instagram_content = @file_get_contents('https://api.instagram.com/v1/users/self/media/recent/?access_token=433404923.81a3e50.52d2416717234f7b9989ed1deafee712&count=1');

        if($instagram_content){
            $instagram_data = json_decode($instagram_content);

            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $dia_insta = date('d', $instagram_data->data[0]->created_time);
            $mes_insta = $meses[date('m', $instagram_data->data[0]->created_time) - 1];
            $anio_insta = date('Y', $instagram_data->data[0]->created_time);

            $text_with_links = $strTweet = preg_replace('/(^|\s)#(\w*[a-zA-Z_]+\w*)/', '<a href="http://www.instagram.com/explore/tags/\2" target="_blank">\1#\2</a>', $instagram_data->data[0]->caption->text);

            $instagram_post = array(
                "src" => $instagram_data->data[0]->images->low_resolution->url,
                "text" => $text_with_links,
                "likes" => $instagram_data->data[0]->likes->count,
                "comments" => $instagram_data->data[0]->comments->count,
                "fecha" => $dia_insta . ' de ' . $mes_insta . ' de ' . $anio_insta,
                "user" => $instagram_data->data[0]->user,
                "link" => $instagram_data->data[0]->link
                );
        }

        if(!empty($instagram_post)){
            $data["instagram_post"] = $instagram_post;
        }


        $this->load->view('notas_home', $this->config_library->send_data_to_view($data));
    }

    public function listado($id_seccion = NULL, $id_seccion_filtro = 0, $id_subseccion_filtro = 0){
        $this->load->driver('cache');

        $this->load->library('parseo_library');

        $this->load->model('editorial_model');
        $this->load->model('navegacion_model');
        $this->load->model('banners_model');

        $pageKey = 'notas_listado' . ($id_seccion ? ('_idsec' . $id_seccion) : '') . ($id_seccion_filtro ? ('_idsecfil' . $id_seccion_filtro) : '') . ($id_subseccion_filtro ? ('_idsubfil' . $id_subseccion_filtro) : '') . (!empty($_GET['pag']) ? ('_' . $_GET['pag']) : '');

        if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){

            $mas_leidas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 7), TRUE, 4);
            $cant_ml = count($mas_leidas);
            if($cant_ml < 4){
                $mas_leidas_real = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('orden' => ' edtn.visitas DESC '), TRUE, 4-$cant_ml);
                $mas_leidas = array_merge($mas_leidas, $mas_leidas_real);
            }

            $flag_notas_empresas = 0;

            $galeria_subseccion = array();
            if(!empty($id_seccion_filtro) && !empty($id_subseccion_filtro)) $galeria_subseccion = $this->navegacion_model->get_secciones($id_seccion_filtro, '1', TRUE);

            if($id_seccion){
                $seccion_data = $this->editorial_model->get_new_seccion($id_seccion);

                if(empty($seccion_data)) show_404();

                if(isset($seccion_data[0])) $seccion_data = $seccion_data[0];

                if($id_seccion == '98' || $id_seccion == '1055'){ // LUNA DE MIEL Y CASAMIENTOS REALES NO TIENEN PADRE
                    $nombre_id = 'id_seccion';
                }else{
                    $nombre_id = 'id_seccion_padre';
                }

                $nota_principal   = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 5, $nombre_id => $id_seccion), TRUE, 1);
                if(!$nota_principal) $nota_principal = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array($nombre_id => $id_seccion), TRUE, 1);
                if(isset($nota_principal[0])) $nota_principal = $nota_principal[0];

                if(!isset($nota_principal['id']) || !$nota_principal['id']) show_404(); // Si la seccion no tiene ninguna nota cargada

                $ids_string = $nota_principal['id'];
                $ids_string_todos = $nota_principal['id'];

                $notas_destacadas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 6, $nombre_id => $id_seccion), TRUE, 4);
                $cant_notas_destacadas = count($notas_destacadas);
                if($cant_notas_destacadas < 4){
                    foreach ($notas_destacadas as $n){
                        $ids_string .= ' ,'.$n['id'];
                    }
                    $notas_destacadas_extra = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array($nombre_id => $id_seccion, 'not_ids' => $ids_string), TRUE, 4-$cant_notas_destacadas);
                    $notas_destacadas = array_merge($notas_destacadas, $notas_destacadas_extra);
                }
                foreach ($notas_destacadas as $n){
                    $ids_string_todos .= ', '.$n['id'];
                }
                $tot = count($this->editorial_model->get_ultimas_notas(NULL,2,NULL,array($nombre_id => $id_seccion, 'not_ids' => $ids_string_todos), TRUE, 100));
                $rows = 8;
                $listado_notas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array($nombre_id => $id_seccion, 'not_ids' => $ids_string_todos), TRUE, $this->db->escape(((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']-1:0)*$rows)) . ',' . $this->db->escape($rows));

                $ultimas_notas = array_merge(array(0 => $nota_principal), $notas_destacadas, $listado_notas);

                $arr_keywords = array();
                foreach ($ultimas_notas as $u){
                    $sec = explode('@',$u['secciones']);
                    foreach ($sec as $s){
                        if (!empty($s) && !is_numeric($s)){
                            array_push($arr_keywords,$s);
                        }
                    }
                }

                $tmp = explode('@', $nota_principal['secciones']);

                if($id_seccion == '98' || $id_seccion == '1055'){
                    $nombre_seccion = isset($tmp[1])&&$tmp[1]?$tmp[1]:'La Organización';
                }else{
                    $nombre_seccion = isset($tmp[3])&&$tmp[3]?$tmp[3]:'La Organización';
                }

                $descripcion = $seccion_data['texto'];
                $title = 'Novedades de ' . $nombre_seccion . ' para tu Casamiento | Casamientos Online';
                $keywords = ', notas, articulos, editorial, '.implode(', ',array_unique($arr_keywords));

                $config['base_url'] = base_url('/consejos/' . $this->parseo_library->clean_url($nombre_seccion) . '_CO_cn' . $id_seccion);
                $canonical = base_url('/consejos/' . $this->parseo_library->clean_url($nombre_seccion) . '_CO_cn' . $id_seccion);
            }else{
                $config['base_url'] = base_url('/listado-de-consejos_CO_');
                $rows = 20;

                $filtros = array();
                if(isset($id_seccion_filtro) && $id_seccion_filtro){
                    $filtros[(in_array($id_seccion_filtro, array('98', '1055')) ? 'id_seccion' : 'id_seccion_padre')] = $id_seccion_filtro;
                    if($id_seccion_filtro == '109') $flag_notas_empresas += 1;
                }
                if(isset($id_subseccion_filtro) && $id_subseccion_filtro){
                    $filtros['id_seccion'] = $id_subseccion_filtro;
                    if($id_subseccion_filtro == '402'){
                        $filtros['id_seccion_hijo'] = $id_subseccion_filtro;
                        $flag_notas_empresas += 1;
                    }
                }

                $tot = count($this->editorial_model->get_ultimas_notas(NULL, 2, NULL, $filtros, TRUE, 1000));
                $listado_notas = $this->editorial_model->get_ultimas_notas(NULL, 2, NULL, $filtros, TRUE, $this->db->escape(((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']-1:0)*$rows)) . ',' . $this->db->escape($rows));

                $descripcion = 'Consejos e ideas para novias. Leé nuestros artículos sobre la organización del casamiento, moda y tendencias en fiestas. Ingresá y no te pierdas nada.';
                $title = 'Listado de Consejos para tu casamiento | Casamientos Online';
                $keywords = ', notas, articulos, editorial';
                $nota_principal = array();
                $notas_destacadas = array();
                $seccion_data = array();
                $nombre_seccion = 'Todas las secciones';

                if($id_seccion_filtro){
                    $sec = array(
                        76 => 'Vestidos',
                        81 => 'Belleza y Estilo',
                        93 => 'La Organización',
                        98 => 'Luna de Miel',
                        109 => 'Empresas',
                        1055 => 'Casamientos reales'
                    );

                    $title = 'Listado de Consejos de ' . $sec[$id_seccion_filtro] . ' para tu casamiento | Casamientos Online';
                    if($id_subseccion_filtro){
                        $descripcion = 'Encontrá los mejores consejos y tips sobre ' . $galeria_subseccion[$id_subseccion_filtro]['text'] . ' y ' . $sec[$id_seccion_filtro] . ' para tu casamiento aquí!';
                    }
                }

                if(isset($id_seccion_filtro) && $id_seccion_filtro == 109 && isset($id_subseccion_filtro) && $id_subseccion_filtro == 402){
                    $nombre_seccion = 'Empresas';
                    $descripcion = 'Leé nuestros artículos sobre proveedores, la organización del casamiento, moda y tendencias en fiestas. Ingresá y no te pierdas nada.';
                    $keywords .= ', notas de empresas';
                }

                $tmp = array();

                if($flag_notas_empresas == 2){
                    $flag_notas_empresas = TRUE;
                }else{
                    $flag_notas_empresas = FALSE;
                }
                $canonical = base_url('/listado-de-consejos');
            }

            if((!$id_seccion && (empty($id_seccion_filtro) && empty($id_subseccion_filtro))) || ((isset($id_seccion_filtro) && $id_seccion_filtro == '109') && (isset($id_subseccion_filtro) && $id_subseccion_filtro == '402'))){
                $secciones = array(
                    1  => 1068,
                    2  => 1070,
                    3  => 1071,
                    4  => 1072,
                    5  => 1073,
                    6  => 1074,
                    7  => 1075,
                    8  => 1076,
                    10 => 1077,
                    11 => 1078,
                    12 => 1079
                );

                $banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);
            }else{
                if(!$id_seccion && isset($id_seccion_filtro) && $id_seccion_filtro) $id_seccion = $id_seccion_filtro;

                switch ($id_seccion) {
                    case '76': // VESTIDOS
                        $secciones = array( // SUCURSALES
                            1  => 76,
                            2  => 247,
                            3  => 283,
                            4  => 417,
                            5  => 573,
                            6  => 500,
                            7  => 724,
                            8  => 906,
                            10 => 959,
                            11 => 852,
                            12 => 1011
                        );
                        break;

                    case '81': // BELLEZA Y ESTILO
                        $secciones = array(
                            1  => 81,
                            2  => 242,
                            3  => 282,
                            4  => 420,
                            5  => 574,
                            6  => 503,
                            7  => 725,
                            8  => 909,
                            10 => 962,
                            11 => 855,
                            12 => 1014
                        );
                        break;

                    case '93': // LA ORGANIZACIÓN
                        $secciones = array(
                            1  => 93,
                            2  => 250,
                            3  => 284,
                            4  => 425,
                            5  => 575,
                            6  => 508,
                            7  => 725,
                            8  => 914,
                            10 => 967,
                            11 => 860,
                            12 => 1019
                        );
                        break;

                    case '98': // LUNA DE MIEL
                        $secciones = array(
                            1  => 98,
                            2  => 255,
                            3  => 285,
                            4  => 430,
                            5  => 576,
                            6  => 514,
                            7  => 727,
                            8  => 919,
                            10 => 972,
                            11 => 856,
                            12 => 1024
                        );
                        break;

                    case '1055': // CASAMIENTOS REALES
                        $secciones = array(
                            1  => 1055,
                            2  => 1080,
                            3  => 1081,
                            4  => 1082,
                            5  => 1083,
                            6  => 1084,
                            7  => 1085,
                            8  => 1086,
                            10 => 1087,
                            11 => 1088,
                            12 => 1089
                        );
                        break;
                }

                $banners = !empty($secciones[$this->session->userdata('id_sucursal')]) ? $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]) : '';
            }

            $config['total_rows'] = $tot / $rows;
            $config['per_page'] = 1;
            $config['page_query_string'] = TRUE;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
            $config['query_string_segment'] = 'pag';
            $config['first_link'] = FALSE;
            $config['last_link'] = FALSE;
            $config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
            $config['full_tag_close'] = '</div></div>';
            $config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
            $config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
            $config['cur_tag_open'] = '<a class="active">';
            $config['cur_tag_close'] = '</a>';

            $this->pagination->initialize($config);

            if(empty($canonical)) $canonical = base_url($_SERVER['REQUEST_URI']);

            if((!empty($id_seccion_filtro) && $id_seccion_filtro == 109) && (!empty($id_subseccion_filtro) && $id_subseccion_filtro == 402)){
                $canonical = base_url('/consejos/' . $this->parseo_library->clean_url($nombre_seccion) . '_CO_cn' . $id_seccion_filtro . '_s' . $id_subseccion_filtro);
            }

            $data = array(
                'canonical'             => $canonical,
                'listado_notas'         => $listado_notas,
                'mas_leidas'            => $mas_leidas,
                'meta_descripcion'      => $descripcion,
                'meta_keywords'         => 'Casamientos Online, casamientos, bodas, novios, novias, Argentina'.$keywords,
                'nombre_seccion'        => $nombre_seccion,
                'nota_principal'        => $nota_principal,
                'notas_destacadas'      => $notas_destacadas,
                'seccion_data'          => $seccion_data,
                'meta_title'            => $title,
                'tmp'                   => $tmp,
                'meses'                 => $this->parseo_library->get_meses(),
                'pag'                   => isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']:'',
                'get'                   => array(
                    'id_seccion'            => $id_seccion_filtro,
                    'id_subseccion'         => $id_subseccion_filtro
                ),
                'flag_notas_empresas'   => $flag_notas_empresas,
                'banners'               => $banners,
                'galeria_subseccion'    => $galeria_subseccion
            );

            if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
        }

        $this->load->view('notas_listado', $this->config_library->send_data_to_view($data));
    }

    public function detalle($id_nota = NULL){
        if(!$id_nota) redirect(base_url('/consejos'));

        $this->load->driver('cache');

        $this->load->library('parseo_library');

        $this->load->model('editorial_model');
        $this->load->model('media_model');
        $this->load->model('banners_model');

        $pageKey = 'nota_' . $id_nota;

        if ((!$data = $this->cache->file->get($pageKey)) || !empty($_GET['hash'])){
            $nota = $this->editorial_model->get_nota($id_nota);

            if(empty($nota)) redirect(base_url('/consejos'));

            $this->editorial_model->get_seccion($nota['id_seccion']);

            $seccion['padre'] = $this->editorial_model->seccionPadre;
            $seccion['id']    = $this->editorial_model->seccionHijo;

            $nota["metadata"] = str_replace('"','',$nota["metadata"]);
            $nota["keywords"] = str_replace('"','',$nota["keywords"]);
            $nota["copete"] = stripslashes($nota["copete"]);

            $comentarios = $this->editorial_model->get_nota_comentarios($id_nota);

            $galeria = '';
            if ($nota['id_galeria']){
                $galeria = $this->media_model->get_galeria(array('id_galeria' => $nota['id_galeria']));
            }

            $seccion_area = $seccion['padre']?$seccion['padre']:$seccion['id'];
            $notas_destacadas = $this->editorial_model->get_ultimas_notas($seccion_area);

            $mas_leidas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 7), TRUE, 4);
            $cant_ml = count($mas_leidas);
            if($cant_ml < 4){
                $mas_leidas_real = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('orden' => ' edtn.visitas DESC '), TRUE, 4-$cant_ml);
                $mas_leidas = array_merge($mas_leidas, $mas_leidas_real);
            }

            $title = '';
            foreach (explode('~',$nota['seccion']) as $nota_seccion){
                if (strpos($nota_seccion,'@')){
                    $nota_seccion = explode('@',$nota_seccion);
                    $title .= ', '.$nota_seccion[1];
                }
            }

            $secciones = array(
                1  => 1068,
                2  => 1070,
                3  => 1071,
                4  => 1072,
                5  => 1073,
                6  => 1074,
                7  => 1075,
                8  => 1076,
                10 => 1077,
                11 => 1078,
                12 => 1079
            );

            $banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);

            $this->editorial_model->save_visita($id_nota);

            $seccion_breadcrumbs = explode('~', $nota['seccion']);

            $seccion_data = '';
            if(isset($seccion_breadcrumbs[1]) && $seccion_breadcrumbs[1]){
                $seccion_data = explode('@', $seccion_breadcrumbs[1]);
            }elseif(isset($seccion_breadcrumbs[2]) && $seccion_breadcrumbs[2]){
                $seccion_data = explode('@', $seccion_breadcrumbs[2]);
            }

            if(isset($seccion_data[0]) && $seccion_data[0] && isset($seccion_data[1]) && $seccion_data[1]){
                $seccion_data['id'] = $seccion_data[0];
                $seccion_data['nombre'] = $seccion_data[1];
                $seccion_data['nombre_seo'] = $this->parseo_library->clean_url($seccion_data[1]);
            }

            if(empty($seccion_data)){
                redirect(base_url('consejos'));
            }

            $data = array(
                'banners'               => $banners,
                'canonical'             => base_url($_SERVER['REQUEST_URI']),
                'comentarios'           => $comentarios,
                'galeria'               => $galeria,
                'galerias'              => $this->media_model->get_galerias($nota['id_seccion'], array('orden' => 'RAND()', 'limit' => 3, 'ultimo_anio' => TRUE)),
                'mas_leidas'            => $mas_leidas,
                'meses'                 => $this->parseo_library->get_meses(),
                'meta_descripcion'      => $nota['copete'] ? $nota['copete'] : substr(str_replace('&nbsp;', ' ', trim(str_replace("\r\n"," ",strip_tags(stripslashes($nota['contenido']))))), 0, 150) . '...',
                'meta_keywords'         => $nota['keywords'].$title,
                'meta_title'            => $nota['titulo'] . ' | Casamientos Online',
                'nota'                  => $nota,
                'notas_destacadas'      => $notas_destacadas,
                'id_referencia'         => 12,
                'id_seccion'            => $seccion['padre'] ? $seccion['padre'] : $seccion['id'],
                'seccion_data'          => $seccion_data,
                'twitter_image'         => 'http://media.casamientosonline.com/images/' . str_replace('@', '.', $nota['pic']),
                'twitter_descripcion'   => $nota['copete'],
            );

            if(empty($_GET['hash'])) $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
        }

        $this->load->view('notas_detalle', $this->config_library->send_data_to_view($data));
    }

    public function tags($id_tag = NULL){
        if(!$id_tag) redirect(base_url('/consejos'));

        $this->load->model('editorial_model');
        $this->load->model('banners_model');

        $this->load->library('parseo_library');

        $filtros = array();

        if(isset($_GET)) $filtros = $_GET;

        // cantidad para la nube de tags
        $filtros['having'] = 'cant >= 20';
        $filtros['tipo_nota'] = 12;

        $tags = $this->editorial_model->get_tags($filtros);
        $tag = $this->editorial_model->get_tag($id_tag);
        $tag = $tag['descripcion'];

        $descripcion = 'Consejos e ideas para novias. Leé nuestros artículos sobre ' . $tag . '. Ingresá y no te pierdas nada.';
        $title = 'Novedades de ' . $tag . ' para tu Casamiento | Casamientos Online';

        $tot = count($this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('id_tag' => $id_tag), TRUE, 100));
        $rows = 10;
        $listado_notas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('id_tag' => $id_tag), TRUE, $this->db->escape(((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']-1:0)*$rows)) . ',' . $this->db->escape($rows));

        $mas_leidas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 7), TRUE, 4);
        $cant_ml = count($mas_leidas);
        if($cant_ml < 4){
            $mas_leidas_real = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('orden' => ' edtn.visitas DESC '), TRUE, 4-$cant_ml);
            $mas_leidas = array_merge($mas_leidas, $mas_leidas_real);
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url('/listado-de-consejos/' . $this->parseo_library->clean_url($tag) . '_CO_t' . $id_tag);
        $config['total_rows'] = $tot / $rows;
        $config['per_page'] = 1;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'pag';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
        $config['full_tag_close'] = '</div></div>';
        $config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
        $config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';

        $this->pagination->initialize($config);

        $breadcrumbs = array(
            0 => array(
                'nombre' => 'Notas',
                'url'   => '/consejos',
            )
        );
        $titulo = 'Notas';
        $url_tag = '/listado-de-consejos/{?}_CO_t';

        $secciones = array(
            1  => 1068,
            2  => 1070,
            3  => 1071,
            4  => 1072,
            5  => 1073,
            6  => 1074,
            7  => 1075,
            8  => 1076,
            10 => 1077,
            11 => 1078,
            12 => 1079
        );

        $banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);

        $canonical = explode('?pag', $_SERVER['REQUEST_URI']);
        if(!empty($canonical[0])) $canonical = base_url($canonical[0]);

        $data = array(
            'canonical'         => $canonical,
            'banners'           => $banners,
            'breadcrumbs'       => $breadcrumbs,
            'listado_notas'     => $listado_notas,
            'tags'              => $tags,
            'tag'               => $tag,
            'mas_leidas'        => $mas_leidas,
            'meta_descripcion'  => $descripcion,
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Argentina',
            'meta_title'        => $title,
            'titulo'            => $titulo,
            'url_tag'           => $url_tag,
            'origen'            => 'editorial'
        );

        $this->load->view('tags', $this->config_library->send_data_to_view($data));
    }

    public function comunidad(){
        $this->load->model('editorial_model');
        $this->load->model('banners_model');

        $mas_leidas = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('ubicacion' => 7), TRUE, 4);

        $cant_ml = count($mas_leidas);

        $banners = $this->banners_model->views(109);

        if($cant_ml < 4){
            $mas_leidas_real = $this->editorial_model->get_ultimas_notas(NULL,2,NULL,array('orden' => ' edtn.visitas DESC '), TRUE, 4-$cant_ml);
            $mas_leidas = array_merge($mas_leidas, $mas_leidas_real);
        }

        $data = array(
            'meta_descripcion'  => 'Casamientos Online, el portal de los novios',
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Comunidad',
            'meta_title'        => 'Comunidad - Casamientos Online',
            'banners'           => $banners,
            'mas_leidas'        => $mas_leidas
        );

        $this->load->view('comunidad.php', $this->config_library->send_data_to_view($data));
    }

    public function to_detalle(){
        $tmp = explode('.', $_SERVER['HTTP_HOST']);
        switch ($tmp[0]) {
            case 'blogdelanovia':
                redirect(base_url('consejos/10-consejos-de-una-novia-a-otra-novia-_CO_n5896'));
                break;
            case 'weddingplanner':
                redirect(base_url('consejos/como-organizar-un-casamiento-perfecto_CO_n5895'));
                break;
            case 'reciencasada':
                redirect(base_url('consejos/recien-casada-como-sigue-la-vida-despues-del-casamiento_CO_n5931'));
                break;
            case 'hacerfamilia':
                redirect(base_url('consejos/hacer-familia-una-eleccion-diaria_CO_n5894'));
                break;
        }
        redirect(base_url('/home'));
    }
}