<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends MY_Controller{
	public function __construct(){
    	parent::__construct();
	}

    public function agrupar_rubros(){
        $this->load->model('rubros_model');
        $this->load->model('navegacion_model');

        $sucursales = $this->navegacion_model->getSucursales();

        $equivalencias = array(
            1 => array( // BUENOS AIRES
                108 => 1, // La fiesta
                109 => 1, // La fiesta para LISTA DE REGALOS
                140 => 2, // Salones
                106 => 3, // Vestidos
                105 => 4, // Novias (belleza)
                107 => 5, // Novios (trajes)
                110 => 6, // Luna de miel
                112 => 1, // La fiesta para NOCHE DE BODAS
            ),
            2 => array( // ROSARIO
                124 => 13, // La fiesta
                123 => 14, // Salones
                121 => 15, // Vestidos
                120 => 16, // Novias (belleza)
                122 => 17, // Novios (trajes)
                126 => 18, // Luna de miel
            ),
            3 => array( // SANTA FE
                134 => 28, // La fiesta
                133 => 29, // Salones
                // 0 => 30, // Vestidos
                // 0 => 31, // Novias (belleza)
                // 0 => 32, // Novios (trajes)
                // 0 => 33, // Luna de miel
            ),
            4 => array( // CORDOBA
                148 => 43, // La fiesta
                153 => 44, // Salones
                146 => 45, // Vestidos
                145 => 46, // Novias (belleza)
                147 => 47, // Novios (trajes)
                150 => 48, // Luna de miel
            ),
            5 => array( // MENDOZA
                163 => 58, // La fiesta
                168 => 59, // Salones
                161 => 60, // Vestidos
                160 => 61, // Novias (belleza)
                // 0 => 62, // Novios (trajes)
                165 => 63, // Luna de miel
            ),
            6 => array( // MAR DEL PLATA
                203 => 73, // La fiesta
                208 => 74, // Salones
                // 0 => 75, // Vestidos
                // 0 => 76, // Novias (belleza)
                202 => 77, // Novios (trajes)
                // 0 => 78, // Luna de miel
            ),
            7 => array( // LA PLATA
                218 => 88, // La fiesta
                223 => 89, // Salones
                216 => 90, // Vestidos
                215 => 91, // Novias (belleza)
                // 0 => 92, // Novios (trajes)
                // 0 => 93, // Luna de miel
            ),
            8 => array( // TUCUMAN
                233 => 103, // La fiesta
                238 => 104, // Salones
                231 => 105, // Vestidos
                // 0 => 106, // Novias (belleza)
                // 0 => 107, // Novios (trajes)
                // 0 => 108, // Luna de miel
            ),
            10 => array( // ENTRE RIOS
                379 => 118, // La fiesta
                378 => 119, // Salones
                380 => 120, // Vestidos
                // 0 => 121, // Novias (belleza)
                // 0 => 122, // Novios (trajes)
                // 0 => 123, // Luna de miel
            ),
            11 => array( // SALTA
                390 => 133, // La fiesta
                389 => 134, // Salones
                391 => 135, // Vestidos
                // 0 => 136, // Novias (belleza)
                // 0 => 137, // Novios (trajes)
                // 0 => 138, // Luna de miel
            ),
            12 => array( // MISIONES
                401 => 148, // La fiesta
                400 => 149, // Salones
                402 => 150, // Vestidos
                // 0 => 151, // Novias (belleza)
                // 0 => 152, // Novios (trajes)
                // 0 => 153, // Luna de miel
            )
        );

        foreach ($sucursales as $k => $sucursal){
            $id_sucursal = $sucursal['id'];
            $rubros_listado = $this->rubros_model->get_listado_rubros_guia($id_sucursal);

            $grupos = array();
            if(is_array($rubros_listado)) foreach ($rubros_listado as $k => $rubro){
                if(isset($equivalencias[$id_sucursal][$rubro['id_padre']])) $grupos[$equivalencias[$id_sucursal][$rubro['id_padre']]][] = $rubro;
            }

            $res = $this->rubros_model->guardar_grupos($grupos, $id_sucursal, $equivalencias[$id_sucursal]);
        }
    }

    public function notificacion_rebotes(){
        $this->load->model('generales_model');

        $res = $this->generales_model->get_presupuestos_envios();

        if($res){
            foreach($res as $datos){
                $asunto = "ERROR EN EL ENVIO DE SU MAIL A " . mb_strtoupper($datos["email_novio"], 'UTF-8');
                $mensaje = "Estimado " . mb_strtoupper($datos["proveedor"], 'UTF-8') . "<br />
                            <br />
                            Nos contactamos con Ud para informarle que el email dirigido a " . mb_strtoupper($datos["nombre_novio"], 'UTF-8')." con relaci&oacute;n al pedido de presupuesto n&deg; ".$datos["id_presupuesto"]." y asunto ".mb_strtoupper($datos["asunto"], 'UTF-8').", no pudo ser entregado correctamente al usuario producto de un error relacionado con el email de este, " . mb_strtoupper($datos["email_novio"], 'UTF-8') . ".<br />
                            <br />
                            Para mas informaci&oacute;n sobre los motivos, y reenviar su email, por favor consulte en su administrador web haciendo<br />
                            <a href='http://www.plataformaeventos.com/detalle_novio.php?id_presupuesto=".$datos["id_presupuesto"]."' target='_blank'>CLICK AQUI</a><br />
                            <br />
                            <br />
                            Atte,<br />
                            CasamientosOnline.com";

                $this->db->trans_begin();

                $ins = array();
                $upd = array();

                // genero el registro en emailer_newsletters
                $ins["newsletter"] = "Informe sobre mail rebotado";
                $ins["id_tipo"] = 20;
                $ins["remitente"] = "presupuestos@casamientosonline.com";
                $ins["email"] = "presupuestos@casamientosonline.com";
                $ins["asunto"] = $asunto;
                $ins["mensaje"] = $mensaje;

                $this->db->insert('emailer_newsletters', $ins);
                unset($ins);

                // genero el registro en la tabla de envios: prov_mails_envios
                $ins["id_newsletter"] = $this->db->insert_id();
                $ins["id_proveedor"] = $datos["id_proveedor"];
                $ins["destinatario"] = $datos["proveedor"];
                $ins["email"] = $datos["email_prov"];

                $this->db->insert('prov_mails_envios', $ins);
                unset($ins);

                // marco el registro en prov_presupuestos_envios_novios como notificado
                $upd["error_notificado"] = 1;
                $this->db->where('id', $datos["id_envio"]);
                $this->db->update('prov_presupuestos_envios_novios', $upd);

                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                }else{
                    $this->db->trans_commit();
                }
            }
        }
    }

    public function procesar_presupuestos_cola(){
        $this->load->library('procesos_library');

        $db_envios = $this->load->database('envios', TRUE);

        $temp = array();

        $sql = " SELECT * FROM prov_presupuestos_cola WHERE (estado_cola = 0 OR estado_cola = 10) order by id ASC LIMIT 50 ";
        
        $query = $db_envios->query($sql);
        $res = $query->result_array();

        $emails_bloqueados = array(
            "webeventos@yahoo.com.ar",
            "hola@eventiame.com.com",
            "showypresencia@hotmail.com",
            "info@cszmarketing.com",
            "socialerosenpie@hotmail.com",
            "ocialerosenpie@hotmail.com",
            "sistemas@casamientosonline.com",
            "landertflor@gmail.com"
        );
        $dominios_bloqueados = array(
            "velourclothes.net"
        );

        foreach($res as $item){
            $idCola = $item['id'];
            $estado_cola = $item['estado_cola'];

            //-- Cambio el estado de la cola a cada registro a procesar --//
            $sql = " SELECT estado_cola FROM prov_presupuestos_cola WHERE id = " . $idCola;
            $query = $db_envios->query($sql);
            $revalid_estado = $query->row()->estado_cola;

            if($revalid_estado == 0 || $revalid_estado == 10){
                $temp['estado_cola'] = 99;
                $db_envios->where('id', $idCola);
                $db_envios->update('prov_presupuestos_cola', $temp);
            }else{
                echo "El id: " . $idCola . " ya ha sido procesada por otro proceso";
                exit;
            }

            unset($item['id']);
            unset($item['estado_cola']);
            unset($item['padre']);
            unset($item['url_referer']);
            unset($item['accion_novio']);
            $data = array();
            $data = $item;
            $this->procesos_library->proceso_status = 1; //-- PRESUPUESTO PROCESADO
            if($estado_cola > 9) $this->procesos_library->proceso_status = 11;

            list($name, $domain) = explode('@', $data["email"]);

            if(in_array($data['email'], $emails_bloqueados) || in_array($domain, $dominios_bloqueados)){
                $temp['estado_cola'] = 5; //--USUARIO BLOQUEDO
                if($estado_cola > 9) $temp['estado_cola'] = 15;
            }else{
                //-- VALIDAR PRESUPUESTOS por el sistema --//
                $sql = "SELECT count(id) id_presupuesto
                        FROM prov_presupuestos
                        WHERE id_origen = '" . $data['id_origen'] . "'
                        AND fecha_alta = '" . $data['fecha_alta'] . "'
                        AND email = '" . $data['email'] . "'
                        AND id_minisitio IN (" . $data['id_minisitio'] . ")
                        ORDER BY id DESC ";
                
                $query = $this->db->query($sql);
                $id_presupuesto = $query->row()->id_presupuesto;

                if($id_presupuesto){ //-- Hay casos en que no se completa la ejecucion del cron y por tanto no procesa todos los presupuestos de un mismo grupo, en tales casos entra en esta seccion para eliminar los minisitios procesados y se rearma el string de minisitios q aun no fueron procesados.
                    $id_minisitio = explode(',', $item['id_minisitio']);
                    if(count($id_minisitio) > $id_presupuesto){
                        $temp['estado_cola'] = 6;
                        if($estado_cola > 9) $temp['estado_cola'] = 16;
                    }else{
                        $temp['estado_cola'] = 3;
                        if($estado_cola > 9) $temp['estado_cola'] = 13;
                    }
                }

                if($id_presupuesto){
                    //$temp['estado_cola'] = 3; //-- PRESUPUESTO REPETIDO POR EL SISTEMA
                }else{

                    $fecha = date('Y-m-d H:i:s', strtotime('-10 minutes', strtotime($data['fecha_alta'])));

                    //-- VALIDAR PRESUPUESTOS por el usuario --//
                    $sql = "SELECT id, id_origen, comentario
                            FROM prov_presupuestos_cola
                            WHERE id_minisitio = '".$data['id_minisitio']."' AND id_origen = '".$data['id_origen']."' AND id_item = '".$data['id_item']."' AND id!='".$idCola."'
                            AND nombre = '".$data['nombre']."' AND apellido = '".$data['apellido']."' AND email = '".$data['email']."' AND (estado_cola = 1 OR estado_cola = 3)
                            AND fecha_evento = '".$data['fecha_evento']."' AND fecha_nacimiento = '".$data['fecha_nacimiento']."'
                            AND fecha_alta > '".$fecha."'
                            ORDER BY id DESC ";

                    $query = $db_envios->query($sql);
                    $res = $query->row_array();

                    if($res['comentario'] == $data['comentario'] && $data['comentario'] != ""){
                        $temp['estado_cola'] = 4; //-- PRESUPUESTO REPETIDO POR EL USUARIO
                        if($estado_cola > 9) $temp['estado_cola'] = 14;
                        $temp['padre'] = $res['id'];
                    }else{
                        //-- VALIDAR PRESUPUESTOS por el usuario --//
                        $id_minisitio = explode(',',$item['id_minisitio']);
                        if(count($id_minisitio) > 1){
                            $data['id_minisitio'] = $id_minisitio;
                        }

                        $id_rubro = explode(',',$item['id_rubro']);
                        if(count($id_rubro) > 1){
                            $data['id_rubro'] = $id_rubro;
                        }

                        $data['asociados'] = explode(',',$item['asociados']);

                        $this->procesos_library->prov_con_asociados = "";
                        $this->procesos_library->prov_asociados = "";
                        
                        if($estado_cola > 9){
                            $this->procesos_library->procesar_presupuesto_sistemas_vencidos($data);
                        }else{
                            $this->procesos_library->procesar_presupuesto_sistemas($data);
                        }
                        $temp['estado_cola'] = $this->procesos_library->proceso_status;
                    }
                }
            }

            $db_envios->where('id', $idCola)->update('prov_presupuestos_cola', $temp);

            unset($temp);
            unset($data);
        }

        echo '<BR><BR> -- ACTUALIZACION FINALIZADA -- ';
    }

    public function procesar_presupuestos_cola_caracteristicas(){
        $this->load->library('procesos_library');

        set_time_limit(0);

        $db_envios = $this->load->database('envios', TRUE);

        $sql = " SELECT * FROM prov_presupuestos_cola_caracteristica WHERE (estado_cola = 0 OR estado_cola = 10) order by id ASC ";
        $query = $db_envios->query($sql);
        $res = $query->result_array();
        $error = FALSE;

        foreach($res as $item){
            $idCola = $item['id'];
            $estado_cola = $item['estado_cola'];

            //-- Cambio el estado de la cola a cada registro a procesar --//
            $sql = " SELECT estado_cola FROM prov_presupuestos_cola_caracteristica WHERE id = ".$idCola;            
            $query = $db_envios->query($sql);
            $res = $query->row_array();
            $revalid_estado = $res['estado_cola'];

            $temp = array();
            if($revalid_estado == 0 || $revalid_estado == 10){
                $temp['estado_cola'] = 99;
                $db_envios->where('id', $idCola);
                $db_envios->update('prov_presupuestos_cola_caracteristica', $temp);

                unset($temp);
            }else{
                echo "El id: " . $idCola . " ya ha sido procesada por otro proceso";
                exit;   
            }

            $this->procesos_library->proceso_status = 1; //-- PRESUPUESTO PROCESADO
            
            //-- --//
            if($item['email'] == 'webeventos@yahoo.com.ar'){
                $temp['estado_cola'] = 5; //--USUARIO BLOQUEDO
            }else{
                $fecha = date('Y-m-d H:i:s', strtotime('-10 minutes', strtotime($item['fecha_alta'])));

                //-- VALIDAR PRESUPUESTOS por el usuario --//
                $sql = "SELECT id, id_origen, caracteristicas
                        FROM prov_presupuestos_cola_caracteristica
                        WHERE id_rubro = '" . $item['id_rubro'] . "' AND id_origen = '" . $item['id_origen'] . "' AND id!='" . $idCola . "'
                        AND nombre = '" . $item['nombre'] . "' AND apellido = '" . $item['apellido'] . "' AND email = '" . $item['email'] . "' AND (estado_cola = 1 OR estado_cola = 3)
                        AND fecha_evento = '" . $item['fecha_evento'] . "' AND fecha_nacimiento = '" . $item['fecha_nacimiento'] . "'
                        AND fecha_alta > '" . $fecha . "'
                        ORDER BY id DESC ";
                
                $query = $db_envios->query($sql);
                $res = $query->row_array();
                
                if($res['caracteristicas'] == $item['caracteristicas']){
                    $temp['estado_cola'] = 4; //--PRESUPUESTO REPETIDO POR EL USUARIO
                    $temp['padre'] = $res['id'];
                }else{
                    //-- PROCESAR PRESUPUESTOS --//
                    
                    ## Defino parametros para procesar Presupuesto ##
                    //-- Caracteristicas: Tipo de Evento --//
                    $sql = "SELECT et.id, CONCAT(gc2.descripcion,': ',gc1.descripcion) desc_carac
                            FROM form_gral_caracteristicas gc1
                            INNER JOIN sys_eventos_tipo et ON et.id = gc1.id_ref AND gc1.referencia = 53
                            LEFT JOIN form_gral_caracteristicas gc2 ON gc1.padre = gc2.id
                            WHERE et.id IN (" . $item['caracteristicas'] . ")
                            UNION ";
                    //-- Caracteristicas: Todas menos tipo de vento --//
                    $sql.= "SELECT c1.id, CONCAT(c2.descripcion,': ',c1.descripcion) desc_carac
                            FROM form_gral_caracteristicas c1
                            LEFT JOIN form_gral_caracteristicas c2 ON c1.padre = c2.id
                            WHERE c1.id IN (".$item['caracteristicas'].") AND ISNULL(c1.referencia)";
                    
                    $query = $this->db->query($sql);
                    $desc_caract = $query->result_array();

                    $coma = "";
                    $comentario = '';
                    foreach($desc_caract as $item_desc){
                        $comentario .= $coma." ".$item_desc['desc_carac'];
                        $coma = "<br />";
                        if($item_desc['id'] == 34){
                            $comentario.= ' ('.$item['ubicacion'].')';  
                        }
                    }

                    $pres_data['id_localidad'] = $item['id_localidad'];
                    $pres_data['id_user'] = empty($item['id_user']) ? $this->procesos_library->get_usuario_id($item['email']) : $item['id_user'];

                    if(!empty($pres_data['id_user'])){
                        $pres_data['id_referencia'] = 4;
                    }
                    $item['id_rubro'] =  implode(',',array_unique(explode(',',$item['id_rubro'])));
                    $pres_data['id_origen'] = $item['id_origen'];
                    $pres_data['id_tipo_evento'] = $item['id_tipo_evento'];
                    $pres_data['aceptado'] = 1; 
                    $pres_data['estado'] = 1; 
                    $pres_data['id_landing'] = 0; 
                    $pres_data['fecha_evento'] = $item['fecha_evento'];
                    $pres_data['fecha_evento_variable'] = $item['fecha_evento_variable'];
                    $pres_data['fecha_nacimiento'] = $item['fecha_nacimiento'];
                    $pres_data['nombre'] = $item['nombre'];
                    $pres_data['apellido'] = $item['apellido'];
                    $pres_data['email'] = $item['email'];
                    $pres_data['telefono'] = $item['telefono']; 
                    $pres_data['invitados'] = $item['invitados'];
                    $pres_data['ubicacion'] = $item['ubicacion'];
                    $pres_data['grupo'] = md5(microtime(false));
                    $pres_data['comentario'] = utf8_encode("El novio / la novia solicita información acerca de tus servicios. <br/><br/>").$comentario;

                    ## SQL Caracteristica##
                    $union = "";
                    $sql_caract = "";
                    ## ##

                    $sql = " SELECT id FROM form_gral_caracteristicas WHERE padre IN (8,24,17) ";//-- obtengo id de caracteristicas para filtrar ms --//

                    $query = $this->db->query($sql);
                    $res = $query->result_array();
                    $arrExcluir = array();
                    if(!empty($res)) foreach ($res as $k => $val) {
                        $arrExcluir[] = $val['id'];
                    }

                    
                    $caracteristicas = explode(',', $item['caracteristicas']);
                    $i = 1; 
                    $sql = " SELECT DISTINCT(ms.id_minisitio) id_minisitio ";
                    $sql.= " FROM site_proveedores_activos ms ";

                    
                    foreach($caracteristicas as $caract){
                        if(in_array($caract, $arrExcluir)){//-- No incluyo en la consulta la caracteristica de si recibo o no paquetes.
                            $sql.= " INNER JOIN form_gral_rel_prov_minisitios relms".$i." ON relms".$i.".id_minisitio = ms.id_minisitio AND relms".$i.".id_caracteristica = ".(int)$caract;
                            $i++;
                            
                            ## SQL Caracteristica##
                            $sql_caract.= $union." select ".(int)$caract." AS id_caract ";
                            $union = " UNION ";     
                            ## ##
                        }
                    }

                    $sql.= " WHERE ms.id_rubro IN (".$item['id_rubro'].") ";
                    $query = $this->db->query($sql);
                    $res = $query->result_array();
                    $data_ms = array();
                    if(!empty($res)) foreach ($res as $k => $val) {
                        $data_ms[] = $val['id_minisitio'];
                    }
                    
                    ##$data_ms -- OBTENGO LOS MINISITIOS Q COINCIDEN CON LA BUSQUEDA DEL NOVIO  
                    foreach ($data_ms as $ms){
                        $prov = $this->procesos_library->get_proveedor_info($ms);
                        $this->procesos_library->insertar_presupuesto_proveedor($prov['id_proveedor'], $item['id_origen'], 0);
                        
                        $pres_data['id_minisitio'] = $contacto['id_minisitio'] = $ms;
                        
                        $this->db->insert('prov_presupuestos', $pres_data);                        
                        $temp_data      = $contacto = $pres_data;
                        $contacto['id_provincia']   = (int)$item['id_provincia'];
                        $contacto['infonovias']     = (int)$item['infonovias'];
                        $contacto['promociones']    = (int)$item['promociones'];
                        $contacto['id_pres']        = $temp_data['id_pres'] = $this->db->insert_id();

                        ## ALTA DE CARACTERISTICAS CON RELACION A PRESUPUESTOS ##
                        $sql = "INSERT INTO form_gral_caracteristicas_rel_prov_presupuestos(id_presupuesto, id_caracteristica)
                                SELECT ".$temp_data['id_pres']." as id_presupuesto, id_caract FROM ( ";
                        $sql .= $sql_caract;
                        $sql .= " ) AS caracteristicas ";
                        
                        $query = $this->db->query($sql);

                        // contenido a enviar por mail solo si el proveedor tiene un mail cargado en el MS, no importa si esta o no online
                        if ($prov['email']){
                            if($prov['autorespuesta_activa']){
                                unset($prov['autorespuesta_activa']);
                            }                            
                            $res = $this->procesos_library->procesar_presupuesto_envio($this->procesos_library->procesar_presupuesto_newsletter($temp_data, $prov), $prov, $temp_data);                            
                        }
                        $contacto['id_portal'] = 1;
                        $contacto['id_referencia'] = 16;

                        $this->procesos_library->procesar_contacto_presupuesto($contacto);

                        $query = $this->db->query('SELECT id FROM base_contactos WHERE email = "'.$item['email'].'"');
                        $res = $query->row_array();
                        $id_contacto = $res['id'];
                        if ($item['paquetes']){
                            $susc_paquetes = '1';
                        }else{
                            $susc_paquetes = '-1';
                        }

                        $this->procesos_library->procesar_contacto_suscripcion($id_contacto, 5, $susc_paquetes);
                        
                        if($error == TRUE){
                            $temp['estado_cola'] = $this->procesos_library->proceso_status;
                            $this->db->where('id', $idCola);
                            $this->db->update('prov_presupuestos_cola_caracteristica', $temp);
                            exit;
                        }
                    }
                    
                    ## SUMAR ESTADISTICAS A LOS RUBROS ##
                    if($this->procesos_library->proceso_status == 1 && count($data_ms) > 0){
                        $sql = " SELECT DISTINCT(id_rubro) ids FROM prov_minisitios WHERE id IN (".implode(',',$data_ms).") ";
                        $query = $this->db->query($sql);
                        $res = $query->result_array();
                        $rubros = array();
                        if(!empty($res)) foreach ($res as $k => $val) {
                            $rubros[] = $val['ids'];
                        }
                        foreach ($rubros as $rb){
                            $res = $this->procesos_library->insertar_presupuesto_rubro($rb, $item['id_origen'], 0);
                        }
                    }
                    
                    $temp['estado_cola'] = $this->procesos_library->proceso_status;
                }
            }
                

            $sql = "SELECT GROUP_CONCAT(DISTINCT(id),' - ',rubro) var FROM prov_rubros WHERE id IN (".$item['id_rubro'].") AND id NOT IN (SELECT DISTINCT(id_rubro) FROM prov_minisitios WHERE id IN (".implode(',',$data_ms)."))";
            $query = $this->db->query($sql);
            $res_rubros = $query->row_array();
            $res_rubros = $res_rubros['var'];

            if(count($data_ms) ==  0 || !empty($res_rubros)){ //-- No se encontraron MS para enviar el presupuesto.
                ###--- Procesar Contacto---###
                $contacto = $pres_data;
                $contacto['id_provincia'] = (int)$item['id_provincia'];
                $contacto['infonovias'] = (int)$item['infonovias'];
                $contacto['promociones'] = (int)$item['promociones'];
                $contacto['id_pres'] = 99;
                $contacto['id_portal'] = 1;
                $contacto['id_referencia'] = 56;
                $contacto['nombre'] = 'User';
                $this->procesos_library->procesar_contacto_presupuesto($contacto);
                
                
                $query = $this->db->query('SELECT id FROM base_contactos WHERE email = "'.$item['email'].'"');
                $res = $query->row_array();
                $id_contacto = $res['var'];

                if ($item['paquetes']){
                    $susc_paquetes = '1';
                }else{
                    $susc_paquetes = '-1';
                }
                $this->procesos_library->procesar_contacto_suscripcion($id_contacto, 5, $susc_paquetes);
                ###--- ---###

                $contenido = "<p><strong>Presupuesto con Caracteristicas</strong></p><br>";
                
                if(count($data_ms) ==  0){
                    $temp['estado_cola'] = 17;
                    $contenido .= "<p>No se ha encontrado <string>ningun Minisitio Activo</sctring> para enviar el siguiente pedido de Presupuesto bajo el Origen <strong>";

                    $query = $this->db->query("SELECT origen FROM prov_presupuestos_origenes WHERE id = " . (int) $pres_data['id_origen']);
                    $res = $query->row_array();
                    $sql_part = $res['origen'];                    
                    $contenido .= $sql_part;
                    
                    $contenido .= "</strong>:</p>";
                }
                else{
                    $temp['estado_cola'] = 18;
                    $contenido .= "<p>No se ha encontrado Minisitios Activos para los siguientes Rubros: <strong>".$res_rubros." en el siguiente pedido de Presupuesto bajo el Origen <strong>";
                    
                    $query = $this->db->query("SELECT origen FROM prov_presupuestos_origenes WHERE id = " . (int) $pres_data['id_origen']);
                    $res = $query->row_array();
                    $sql_part = $res['origen'];
                    $contenido .= $sql_part;
                    
                    $contenido .= "</strong>:</p>";
                }                
                                
                $contenido .= "<p><strong>Rubros seleccionados:</strong> ";

                $query = $this->db->query("SELECT GROUP_CONCAT(CONCAT(' #',id,'-',rubro)) var FROM prov_rubros WHERE id IN (".$item['id_rubro'].")");
                $res = $query->row_array();
                $sql_part = $res['var'];
                $contenido .= $sql_part;
                
                $contenido .= "</p>";
                $contenido .= "<p><strong>Nombre:</strong> ".$pres_data['nombre']."</p>"; 
                $contenido .= "<p><strong>Apellido:</strong> ".$pres_data['apellido']."</p>";
                $contenido .= "<p><strong>Email:</strong> ".$pres_data['email']."</p>";
                $contenido .= "<p><strong>Telefono:</strong> ".$pres_data['telefono']."</p>";
                $contenido .= "<p><strong>Tipo Evento:</strong>";

                $query = $this->db->query("SELECT tipo FROM sys_eventos_tipo WHERE id = " . (int) $pres_data['id_tipo_evento']);
                $res = $query->row_array();
                $sql_part = $res['tipo'];
                $contenido .= $sql_part;
                $contenido .= "</p>";
                
                $contenido.= "<p><strong>Fecha del Evento:</strong> ".$pres_data['fecha_evento']." - <strong>Fecha Variable: </strong> ";
                if($pres_data['fecha_evento_variable']){
                    $contenido.= "SI";
                }else{
                    $contenido.= "NO";
                }

                
                $contenido .= "</p>";
                $contenido .= "<p><strong>Fecha de Nacimiento:</strong> ".$pres_data['fecha_nacimiento']."</p>";
                $contenido .= "<p><strong>Invitados:</strong> ".$pres_data['invitados']."</p>";
                $contenido .= "<p><strong>Ubicacion:</strong> ".$pres_data['ubicacion']."</p>";

                $contenido .= "<p><strong>Provincia:</strong>";

                $query = $this->db->query("SELECT provincia FROM sys_provincias WHERE id = ".(int)$item['id_provincia']);
                $res = $query->row_array();
                $sql_part = $res['provincia'];
                $contenido .= $sql_part;
                $contenido .= "</p>";

                
                $contenido .= "<p><strong>Localidad:</strong> ";

                $query = $this->db->query("SELECT localidad FROM sys_localidades WHERE id = ".(int)$pres_data['id_localidad']);
                $res = $query->row_array();
                $sql_part = $res['localidad'];
                $contenido .= $sql_part;
                $contenido .= "</p>";
                $contenido.= "<p><strong>Comentario:</strong> " . $pres_data['comentario'] . "</p><br><br>";
                $contenido.= "<p><i>Registro <strong>#" . $idCola . "</strong> de la tabla prov_presupuestos_cola_caracteristica</i></p>";

                $id_tipo = 11;
                $remitente = 'Area Sistemas - CasamientosOnline';
                $email = 'sistemas@casamientosonline.com';
                $asunto = 'Proveedores no encontrados para envio de Presupuestos con Caracteristicas';
                $news = array();
                $news['id_tipo'] = $id_tipo;
                $news['remitente'] = $remitente;
                $news['email'] = $email;
                $news['asunto'] = $asunto;
                $news['mensaje'] = $contenido;
                $news['newsletter'] = 'CRON - Presupuestos con Caracteristicas';

                $this->db->insert('emailer_newsletters', $news);
                $id_news = $this->db->insert_id();

                $env = array();
                $env['id_newsletter'] = $id_news;
                $env['destinatario'] = 'Gabriel Meoli';
                $env['email'] = 'gabriel@casamientosonline.com';                
                $this->db->insert('cron_mails_envios', $env);
            }
            
            
            $db_envios->where('id', $idCola);
            $db_envios->update('prov_presupuestos_cola_caracteristica', $temp);
            unset($temp);
            
        } // END FOREACH

        echo '<BR><BR> -- ACTUALIZACION FINALIZADA -- ';
    }

    public function vista_media_galeria(){
        $this->db->query("TRUNCATE TABLE vista_media_galeria");

        $sql = "INSERT INTO vista_media_galeria ";

        // -- MEDIA -- //
        $sql .= "SELECT
                sysm.id 
                , IF(ISNULL(sysm.titulo) OR sysm.titulo = '',provp.proveedor,sysm.titulo) as titulo 
                , sysm.descripcion 
                , sysm.name AS video_id 
                , GROUP_CONCAT(DISTINCT systg.id,'@',systg.descripcion ORDER BY sysatg.orden ASC SEPARATOR '~') AS tags 
                , GROUP_CONCAT(DISTINCT provp.id_rubro,'@',provp.rubro,'@',provp.sucursal,'@',provp.id_sucursal SEPARATOR '~') AS rubros 
                , GROUP_CONCAT(DISTINCT provp.id_proveedor,'@',provp.proveedor,'@',provp.id_minisitio,'@',provp.rubro,'@',provp.sucursal,'@',provp.id_rubro,'@',provp.id_sucursal,'@',provp.logo SEPARATOR '~') AS proveedores 
                , '' AS cantidad_comentarios 
                , '' AS secciones 
                , '1' AS video_prov 
                , sysrmr.orden 
                ,systg.id AS tagID 
                ,provp.id_sucursal AS sucursalID 
                ,provp.id_rubro AS rubroID 
                , NULL AS id_destaque 
                , sysm.titulo AS sysm_titulo 
                , NULL AS seccionID 
                , NULL AS seccionesID 
                FROM site_proveedores_activos provp 
                JOIN sys_rel_medias_referencias sysrmr ON provp.id_minisitio = sysrmr.id_padre AND sysrmr.id_referencia = 5 AND sysrmr.estado = 2 
                JOIN sys_medias sysm ON sysrmr.id_media = sysm.id AND sysm.estado IN (2,4) AND sysm.eliminado = 0 
                JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id 
                JOIN sys_tipos syst ON sysat.id_tipo = syst.id 
                LEFT JOIN sys_asociados_tags sysatg ON sysm.id = sysatg.id_padre AND sysatg.activo = 1 
                LEFT JOIN sys_tags systg ON sysatg.id_tag = systg.id AND systg.id_referencia = 15 AND systg.activo = 1 
                WHERE syst.id=3 
                GROUP BY sysm.id ";

        $sql .= " UNION ";

        // -- GALERIA -- //
        $sql .= "SELECT
                    sysg.id
                    , IF(ISNULL(sysg.titulo) OR sysg.titulo = '',sysm.titulo,sysg.titulo) as titulo 
                    , sysg.descripcion 
                    , sysm.name AS video_id 
                    , GROUP_CONCAT(DISTINCT syst.id,'@',syst.descripcion ORDER BY sysatg.orden ASC SEPARATOR '~') AS tags 
                    , GROUP_CONCAT(DISTINCT provr.id,'@',provr.rubro,'@',syssuc.sucursal,'@',syssuc.id ORDER BY sysar.orden SEPARATOR '~') AS rubros 
                    , GROUP_CONCAT(DISTINCT provp.id,'@',provp.proveedor,'@',spa.id_minisitio,'@',spa.rubro,'@',spa.sucursal,'@',spa.id_rubro,'@',spa.id_sucursal,'@',provp.logo ORDER BY RAND() SEPARATOR '~') AS proveedores 
                    , COUNT(DISTINCT sysc.id) AS cantidad_comentarios 
                    , GROUP_CONCAT(DISTINCT syss.seccion ORDER BY sysaa.orden SEPARATOR ', ') AS secciones 
                    , '0' AS video_prov 
                    , sysrmr.orden 
                    , sysatg.id_tag AS tagID 
                    ,NULL AS sucursalID 
                    ,NULL AS rubroID 
                    , sysrdg.id_destaque 
                    , sysm.titulo AS sysm_titulo 
                    ,CASE WHEN syss.padre != '99' THEN syss.padre ELSE syss.id END AS seccionID 
                    , GROUP_CONCAT(DISTINCT syss.id ORDER BY sysaa.orden SEPARATOR ', ') AS seccionesID 
                FROM sys_galerias sysg 
                LEFT JOIN sys_areas_asociadas sysaa ON sysg.id = sysaa.id_padre AND sysaa.id_referencia = 14 AND sysaa.activo = 1 
                LEFT JOIN sys_secciones syss ON sysaa.id_seccion = syss.id 
                LEFT JOIN sys_rel_medias_referencias sysrmr ON sysg.id = sysrmr.id_padre AND sysrmr.id_referencia = 14 AND sysrmr.estado = 2 AND sysrmr.orden = 1 
                LEFT JOIN sys_medias sysm ON sysrmr.id_media = sysm.id AND sysm.estado IN (2,4) AND sysm.eliminado = 0 AND sysm.col = 1 
                LEFT JOIN sys_archivos_tipo sysat ON sysm.id_tipo = sysat.id 
                LEFT JOIN sys_tipos systp ON sysat.id_tipo = systp.id 
                LEFT JOIN sys_asociados_tags sysatg ON sysg.id = sysatg.id_padre AND sysatg.activo = 1 
                LEFT JOIN sys_tags syst ON sysatg.id_tag = syst.id AND syst.id_referencia = 14 AND syst.activo = 1 
                LEFT JOIN sys_asociados_rubros sysar ON sysg.id = sysar.id_padre AND sysar.id_referencia = 14 AND sysar.activo = 1 
                LEFT JOIN prov_rubros provr ON sysar.id_rubro = provr.id 
                LEFT JOIN sys_sucursales syssuc ON provr.id_sucursal = syssuc.id 
                LEFT JOIN sys_asociados_proveedores sysap ON sysg.id = sysap.id_padre AND sysap.id_referencia = 14 AND sysap.activo = 1 
                LEFT JOIN prov_proveedores provp ON sysap.id_proveedor = provp.id 
                LEFT JOIN site_proveedores_activos spa ON provp.id = spa.id_proveedor 
                LEFT JOIN sys_comentarios sysc ON sysg.id = sysc.id_padre AND sysc.id_referencia = 14 AND sysc.id_estado = 2 AND sysc.activo = 1 
                LEFT JOIN sys_rel_destaques_galerias sysrdg ON sysrdg.id_galeria = sysg.id 
                WHERE sysg.id_estado = 4 AND systp.id = 3 
                GROUP BY sysg.id ";

        $this->db->query($sql);

        echo '<BR><BR> -- ACTUALIZACION FINALIZADA -- ';
    }
}