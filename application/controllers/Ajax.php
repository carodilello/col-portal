<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {
	public function __construct(){
		parent::__construct();
	}

	public function get_mapas_rubro($id_rubro, $filtros = NULL){
		$this->load->model('proveedores_model');

		if($filtros) $filtros = urldecode($filtros);
		$proveedores = $this->proveedores_model->get_proveedores($id_rubro,'nivel',0,1,$filtros,1);

		die($proveedores);
	}

	public function get_mapas_proveedor($id_minisitio){
		$this->load->model('proveedores_model');
		$this->load->library('config_library');
		$r = $this->proveedores_model->get_proveedor($id_minisitio);

		$ruta_local = NULL;
		$ruta_external = NULL;

		$proveedor  = $r;
		$size 	    = 480;
		$sub_size   = 300;
		$pres_param = 'mapa';

		$data = array(
			'ruta_local' 	=> $ruta_local,
			'ruta_external' => $ruta_external,
			'proveedor' 	=> $proveedor,
			'size' 			=> $size,
			'sub_size' 		=> $sub_size,
			'pres_param' 	=> $pres_param
		);

		$vista = $this->load->view('popup_mapa', $data, true);

		die($vista);
	}

	public function check_fecha_mayor_hoy(){
		foreach ($_GET as $key => $value) {
			$fecha = $value;
		}
		if($fecha){
			$tmp = explode('/', $fecha);
			$fecha_elegida = $tmp[2].$tmp[1].$tmp[0];
			$hoy = date('Ymd');

			if((strlen($fecha_elegida) == strlen($hoy)) && ((int) $fecha_elegida >= (int) $hoy)){
				http_response_code(200);
			}else{
				header("HTTP/1.0 404 Ingrese una fecha mayor a hoy");
			}
		}
	}

	public function get_localidad_from_provincia($id_provincia, $id_default = NULL){
		$this->load->model('navegacion_model');

		$localidades = $this->navegacion_model->get_localidades($id_provincia);

		$all  = '<option value="">Seleccionar una Localidad</option>';
		if(isset($_POST['texto_por_defecto']) && $_POST['texto_por_defecto']) $all = "<option value=''>" . $_POST['texto_por_defecto'] . "</option>";
		if(is_array($localidades)) foreach ($localidades as $k => $localidad){
			$all .= '<option value=' . $k . ' ' . ($id_default == $k ? 'selected="selected"' : '') . '>' . $localidad . '</option>';
		}

		echo $all;
	}

	public function get_provincias($id_default = NULL){
		$this->load->model('navegacion_model');

		$provincias = $this->navegacion_model->get_provincias(0);

		$all  = '<option value="">Seleccionar una Provincia</option>';
		if(is_array($provincias)){
			foreach ($provincias as $k => $provincia){
				$all .= '<option value=' . $k . ' ' . ($id_default == $k ? 'selected="selected"' : '') . '>' . $provincia . '</option>';
			}
		}

		echo $all;
	}

	public function obtener_mas_imagenes($id_rubro, $page, $index){
		$this->load->library('config_library');

		$fotos = $this->config_library->get_fotos($id_rubro, 21, $page, '', 1, 0);

		$puntos_suspensivos 	  = "";
		$puntos_suspensivos_zonas = "";
		$html 					  = array();
		if(isset($fotos)&&$fotos) foreach ($fotos as $k => $foto){
			$index += 1;
			$html[] = array(
		    	'title' 	=> $foto['titulo'],
		        'href'		=> 'http://media.casamientosonline.com/images/'.$foto['imagen_gde'],
		        'type'  	=> 'image/jpeg',
		        'thumbnail' => 'http://media.casamientosonline.com/images/'.str_replace("_chica", "", $foto["imagen"]),
		        'index'     => array($index => $foto['rubro_seo'] . '/presupuesto-foto_CO_r'.$id_rubro.'_fo'.$foto['id'])
			);
		}

		echo json_encode($html);
	}

	public function sugerencias_buscador_home($search, $id_sucursal, $sucursal = 'buenos-aires'){
		$this->load->model('proveedores_model');
		$this->load->library('parseo_library');

		if(is_string($search)){
			$prov = $this->proveedores_model->buscar_contenido_proveedores(urldecode($search), $id_sucursal, $sucursal);
			$res = $this->parseo_library->resultados_busqueda_home($prov);
			echo $res;
		}
	}

	public function accion_ajax_get_salones(){
		$this->load->model('generales_model');

		$salon = $_POST['valores'];

		if(strlen($salon) > 3){
			$array = $this->generales_model->get_salones($salon);
	        echo json_encode($array);
		}
	}

	public function accion_get_rubros(){
		$this->load->model('rubros_model');

		$id_zona = (int) $_POST['id_zona'];

		$array = $this->rubros_model->get_form_zonas_sucursal($id_zona);

	    echo json_encode($array);
	}

	public function accion_organiza_paquetes(){
		$this->load->model('paquetes_model');

		$rubros = implode(',',$_POST['rubros']);

		$res = $this->paquetes_model->get_organiza_paquetes($rubros);

		echo $res;
	}

	public function enviar_amigo(){
		if(!$this->config->item('procesos_off')){
			$this->load->model('generales_model');

			if($_POST['rtte_name'] && $_POST['rtte_apellido'] && ($_POST['rtte_email'] || $_SESSION) && $_POST['dest_name'] && $_POST['dest_apellido'] && $_POST['dest_email'] && (strpos($_SERVER['HTTP_REFERER'],$_SERVER['HTTP_HOST']) !== false || strpos($_POST['enviar_amigo_url'],$_SERVER['HTTP_HOST']) !== false)){
				if ($_POST['enviar_amigo_url']){
					$data['url'] = $_POST['enviar_amigo_url'];
				}else{
					$data['url'] = $_SERVER['HTTP_REFERER'];
				}

				$message = $_POST['rtte_name'].' '.$_POST['rtte_apellido'].' entró a '.$_SERVER['HTTP_HOST'].' y quiere compartir esta página con vos: '.$data['url'].'.';
				if($_POST['comentario']) $message .= ' Te deja el siguiente comentario: '.$_POST['comentario'];


				$data_new = array();
				$data_new['newsletter'] = 'Enviar a un amigo: '.$_POST['rtte_name'].' '.$_POST['rtte_apellido'].' a '.$_POST['dest_name'].' '.$_POST['dest_apellido'];
				$data_new['id_tipo'] = 18;
				$data_new['remitente'] =  $_POST['rtte_name'].' '.$_POST['rtte_apellido'];
				$data_new['email'] = $_POST['rtte_email'];
				$data_new['asunto'] = $_POST['rtte_name'].' te envía una página de '.$_SERVER['HTTP_HOST'];
				$data_new['mensaje'] = $message;

				$res = $this->generales_model->grabar_envio_amigo($data_new, $_POST);
				echo $res;
			}
		}else{
			echo true;
		}
	}

	public function get_secciones($id, $todas_las_notas = FALSE){
		$this->load->model('navegacion_model');

		$res = $this->navegacion_model->get_secciones($id);
		if($todas_las_notas) $res = $this->navegacion_model->get_secciones($id, '1', TRUE);

		$option = '<option value=""> - Seleccione - </option>';
		if($todas_las_notas){
			if(!empty($res)) foreach ($res as $k => $val){
				$name = $val['name'];
				$option .= '<option value="'.$k.'" data-name="' . $name . '">' . $val['text'] . '</option>';
			}
		}else{
			if(!empty($res)) foreach ($res as $k => $val){
				$option .= '<option value="'.$k.'">' . $val . '</option>';
			}
		}

		echo $option;
	}


	public function get_rubro($id){
		$this->load->model('navegacion_model');
		$this->load->library('parseo_library');

		$rubros = $this->navegacion_model->get_rubros_combo($id, 1, isset($_POST['activo']) && $_POST['activo'] ? $_POST['activo'] : TRUE);
		$rubros = $this->parseo_library->rubros_para_seo($rubros);

		$option = '<option value=""> - Seleccione - </option>';
		if(!empty($rubros)) foreach ($rubros as $solapa => $rubros_solapa){
			$option .= '<optgroup label="' . $solapa . '">';

			foreach ($rubros_solapa as $id_rubro => $rubro){
				$option .= '<option value="'.$id_rubro.'" data-name="' . $rubro['nombre_seo'] . '">' . $rubro['nombre'] . '</option>';
			}

			$option .= '</optgroup>';
		}

		echo $option;
	}

	public function get_menu_sugerencias($id_sucursal = NULL){
		if($id_sucursal){
			$this->load->model('navegacion_model');
			$this->load->model('generales_model');

			$sucursal = $this->navegacion_model->getSucursal($id_sucursal);

			$data = array(
				'id_sucursal'			=> $id_sucursal,
				'nav'					=> $this->navegacion_model->get_elementos($id_sucursal),
				'rubros_reordenados'	=> $this->generales_model->get_buscador_estructura($id_sucursal),
				'doble_columna' 		=> $this->generales_model->get_doble_columna($id_sucursal),
				'class'					=> $sucursal['class'],
				'sucursal'				=> $sucursal
			);

			$this->load->view('menu_sugerencias', $data);
		}
	}

	public function get_carousel_responsive($tipo = 'proveedores_destacados', $cols = 4, $id_rubro = '', $id_proveedor = ''){
		$this->load->library('config_library');
		$this->load->library('parseo_library');
		$this->load->model('proveedores_model');

		switch ($tipo) {
			case 'proveedores_destacados':
				$inf = $this->config_library->get_info_home();

				$proveedores_destacados_or = $this->proveedores_model->get_proveedores_destacados($inf['id_seccion_prov'], 8);
				shuffle($proveedores_destacados_or);
				$proveedores_destacados    = $this->parseo_library->proveedores_para_carousel($proveedores_destacados_or, $cols);

				$data = array(
					'proveedores_destacados' => $proveedores_destacados
				);

				if(!empty($proveedores_destacados)) $this->load->view('home_carousel_inner', $data);
				break;

			case 'proveedores_relacionados':
				if($id_rubro && $id_proveedor){
					$proveedores = $this->proveedores_model->get_proveedores($id_rubro, 'nivel', 0, '1', '', 0, 0, $id_proveedor); // $_GET['vars'][0] id_rubro, $_GET['vars'][1] id_proveedor
					$proveedores = $this->parseo_library->proveedores_para_carousel($proveedores, $cols);

					$data = array(
						'proveedores' => $proveedores
					);

					if(!empty($proveedores)) $this->load->view('minisitio_carousel_inner', $data);
				}

				break;
		}
	}

	public function estadistica($id_minisitio, $id_tipo = '1'){
		$this->load->model('proveedores_model');

		$this->proveedores_model->insertar_estadistica_telefono($id_minisitio, $id_tipo);
	}
}