<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generales extends MY_Controller{
	public function __construct(){
    	parent::__construct();
        $this->load->library('config_library');
	}

    public function index(){
        $this->load->driver('cache');

        if(!empty($_GET['borrar_cache'])){
            $this->cache->file->delete($_GET['borrar_cache']);
            redirect(base_url('/'));
        }else{
            show_404();
        }
    }

    public function politicas_de_privacidad(){
        $data = array(
            'canonical'                 => base_url($_SERVER['REQUEST_URI']),
            'meta_descripcion'          => 'Políticas de privacidad de Casamientos Online - El portal de los novios.',
            'meta_keywords'             => 'Portal casamientos, casamientos online, casamientosonline, casamiento, bodas, fiestas, novias, novios, ceremonia, salones, civil, luna de miel, vestidos, vestido de novia, trajes, servicios para casamiento, catering, barras de trago, fiestas de casamiento, maquillaje para novias, makeup, souvenirs, centros de mesa, ramos, tocados, cotillón, shows, peinadores, casas de regalos, reserva de salones, registro civil, fotógrafo, dj, disc jockey, foto y video, alianzas, tradiciones, noche de bodas, arreglos florales, ambientación, wedding planners, eventos, invitaciones, participaciones, textos de participaciones, listas de casamiento, organización de bodas, iglesias, proverbios del matrimonio, autos para casamientos, transporte, carruajes, spa, menús para casamientos, carnaval, preguntas, respuestas, música en la iglesia, madrinas, cortejo de niños, anécdotas de novias, peinados, lluvia de arroz, foro de novias, despedida de soltero soltera, guía del casamiento, blogs de casamientos, consejos sobre el casamiento, ideas, terminos, politica de privacidad',
            'meta_title'                => 'Política de Privacidad de Casamientos Online'
        );

    	$this->load->view('politicas_de_privacidad', $this->config_library->send_data_to_view($data));
    }

    public function terminos_y_condiciones(){
        $data = array(
            'canonical'                 => base_url($_SERVER['REQUEST_URI']),
            'meta_descripcion'          => 'Terminos y condiciones de Casamientos Online - El portal de los novios.',
            'meta_keywords'             => 'Portal casamientos, casamientos online, casamientosonline, casamiento, bodas, fiestas, novias, novios, ceremonia, salones, civil, luna de miel, vestidos, vestido de novia, trajes, servicios para casamiento, catering, barras de trago, fiestas de casamiento, maquillaje para novias, makeup, souvenirs, centros de mesa, ramos, tocados, cotillón, shows, peinadores, casas de regalos, reserva de salones, registro civil, fotógrafo, dj, disc jockey, foto y video, alianzas, tradiciones, noche de bodas, arreglos florales, ambientación, wedding planners, eventos, invitaciones, participaciones, textos de participaciones, listas de casamiento, organización de bodas, iglesias, proverbios del matrimonio, autos para casamientos, transporte, carruajes, spa, menús para casamientos, carnaval, preguntas, respuestas, música en la iglesia, madrinas, cortejo de niños, anécdotas de novias, peinados, lluvia de arroz, foro de novias, despedida de soltero soltera, guía del casamiento, blogs de casamientos, consejos sobre el casamiento, ideas, terminos, condiciones, terminos y condiciones',
            'meta_title'                => 'Terminos y Condiciones de Casamientos Online'
        );

    	$this->load->view('terminos_y_condiciones', $this->config_library->send_data_to_view($data));
    }

     public function mapa_sitio(){
        $this->load->model('banners_model');
        $this->load->model('generales_model');
        $this->load->model('rubros_model');

        $this->load->library('parseo_library');

        $elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
        asort($elementos_random);
        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

        $banners = $this->banners_model->views(1090);

        $data = array(
            'banners'                   => $banners,
            'canonical'                 => base_url($_SERVER['REQUEST_URI']),
            'elementos_random'          => $elementos_random,
            'listado_seo_random'        => $listado_seo_random,
            'listado_seo_random_guiones'=> $listado_seo_random_guiones,
            'meta_descripcion'          => 'Mapa del Sitio: Casamientos Online - El portal de los novios.',
            'meta_keywords'             => 'Portal casamientos, casamientos online, casamientosonline, casamiento, bodas, fiestas, novias, novios, ceremonia, salones, civil, luna de miel, vestidos, vestido de novia, trajes, servicios para casamiento, catering, barras de trago, fiestas de casamiento, maquillaje para novias, makeup, souvenirs, centros de mesa, ramos, tocados, cotillón, shows, peinadores, casas de regalos, reserva de salones, registro civil, fotógrafo, dj, disc jockey, foto y video, alianzas, tradiciones, noche de bodas, arreglos florales, ambientación, wedding planners, eventos, invitaciones, participaciones, textos de participaciones, listas de casamiento, organización de bodas, iglesias, proverbios del matrimonio, autos para casamientos, transporte, carruajes, spa, menús para casamientos, carnaval, preguntas, respuestas, música en la iglesia, madrinas, cortejo de niños, anécdotas de novias, peinados, lluvia de arroz, foro de novias, despedida de soltero soltera, guía del casamiento, blogs de casamientos, consejos sobre el casamiento, ideas, terminos, politica de privacidad',
            'meta_title'                => 'Mapa del Sitio Casamientos Online'
        );

    	$this->load->view('mapa_sitio', $this->config_library->send_data_to_view($data));
    }


    public function pagina_404(){
        $this->load->model('banners_model');
        $this->load->model('rubros_model');
        $this->load->library('parseo_library');

        $elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
        $rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
        $listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
        $elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
        $elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
        asort($elementos_random);
        $elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
        $listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
        $listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

        $url = 'http://api.noencontrado.org/v1/search/';

        $missing_children = array();
        $flag = TRUE;
        $cant = 0;
        $i = 0;

        while($flag == TRUE && $i < 30){
            $se_repite = FALSE;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $children = json_decode(curl_exec($curl), true);
            curl_close ($curl);

            foreach ($missing_children as $child) {
                if($children['data']['id'] == $child['data']['id']) $se_repite = TRUE;
            }
            if(!$se_repite){
                $missing_children[] = $children;
                $cant += 1;
            }

            if($cant == 4) $flag = FALSE;
            if($cant == 0) $cant += 1;

            $i += 1;
        }

        $banners = $this->banners_model->views(1090);

        $data = array(
            'banners'                   => $banners,
            'canonical'                 => base_url($_SERVER['REQUEST_URI']),
            'elementos_random'          => $elementos_random,
            'listado_seo_random'        => $listado_seo_random,
            'listado_seo_random_guiones'=> $listado_seo_random_guiones,
            'meta_descripcion'          => 'Seguí navegando…  todavía hay mucho que podemos ofrecerte!',
            'meta_keywords'             => 'Portal casamientos, casamientos online, casamientosonline, casamiento, bodas, fiestas, novias, novios, ceremonia, salones, civil, luna de miel, vestidos, vestido de novia, trajes, servicios para casamiento, catering, barras de trago, fiestas de casamiento, maquillaje para novias, makeup, souvenirs, centros de mesa, ramos, tocados, cotillón, shows, peinadores, casas de regalos, reserva de salones, registro civil, fotógrafo, dj, disc jockey, foto y video, alianzas, tradiciones, noche de bodas, arreglos florales, ambientación, wedding planners, eventos, invitaciones, participaciones, textos de participaciones, listas de casamiento, organización de bodas, iglesias, proverbios del matrimonio, autos para casamientos, transporte, carruajes, spa, menús para casamientos, carnaval, preguntas, respuestas, música en la iglesia, madrinas, cortejo de niños, anécdotas de novias, peinados, lluvia de arroz, foro de novias, despedida de soltero soltera, guía del casamiento, blogs de casamientos, consejos sobre el casamiento, ideas',
            'meta_title'                => 'Algo falló! Probá estas opciones | Error 404 | Casamientos Online',
            'missing_children'          => $missing_children,
            'meses'                     => $this->parseo_library->get_meses()
        );

        header("HTTP/1.0 404 Not Found");
        $this->load->view('pagina_404', $this->config_library->send_data_to_view($data));
    }
}