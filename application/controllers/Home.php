<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct(){
    	parent::__construct();
    	$this->load->library('config_library');
	}

	public function index(){
		$this->load->driver('cache');

		$pageKey = 'index' . $this->config_library->sucursal['id'];

		if (!$data = $this->cache->file->get($pageKey)){
			$this->load->model('rubros_model');
			$this->load->model('generales_model');
			$this->load->model('proveedores_model');
			$this->load->model('editorial_model');
			$this->load->model('banners_model');

			$this->load->library('parseo_library');

			$slides 	= $this->editorial_model->get_slides(); // REALMENTE NO SON LAS ULTIMAS NOTAS

			$meta_rubros = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r2.orden, r1.rubro', 1);

			$meta_keywords = "";
			if($meta_rubros){
				foreach ($meta_rubros as $mr){
					$meta_keywords .= ', '.$mr;
				}
			}

			$mostrar_rubros = $this->generales_model->get_no_mostrar_rubros($this->session->userdata('id_sucursal'));
			$this->config_library->set_seccion();

			$inf = $this->config_library->get_info_home();

			$id_seccion = 126;
			$proveedores_destacados_or = $this->proveedores_model->get_proveedores_destacados($inf['id_seccion_prov'], 8);
			shuffle($proveedores_destacados_or);
			$proveedores_destacados    = $this->parseo_library->proveedores_para_carousel($proveedores_destacados_or);

			$ultimas_notas 	= $this->editorial_model->get_ultimas_notas(null,2,null,array('ubicacion' => 1), TRUE, 4); // REALMENTE NO SON LAS ULTIMAS NOTAS

			$cant_notas = 4;
			$mas_notas	= array();
			if($this->editorial_model->cantidad_notas < $cant_notas){
				$ids_notas = array();
				if(is_array($ultimas_notas)) foreach ($ultimas_notas as $nota){
					$ids_notas[] = $this->db->escape($nota['id']);
				}
				if($ids_notas) $ids_notas = implode(',',$ids_notas);
				if(is_string($ids_notas)){
					$mas_notas = $this->editorial_model->get_ultimas_notas(null,2,null,array('ubicacion' => 1, 'not_ids' => $ids_notas), FALSE, $cant_notas-$this->editorial_model->cantidad_notas);
					$ultimas_notas = array_merge($ultimas_notas, $mas_notas);
				}else{
					$ultimas_notas = $this->editorial_model->get_ultimas_notas(null,2,null,array('ubicacion' => 1), FALSE, 4);
				}
			}

			$rubros_mas_consultados = $this->rubros_model->get_rubros_mas_consultados($this->session->userdata('id_sucursal'), TRUE);
			$elementos_seo  = $this->generales_model->get_nombres_rubros_seo($this->session->userdata('id_sucursal'));
			$rubros_listado = $this->rubros_model->get_listado_rubros_guia($this->session->userdata('id_sucursal'), 0, 1, 'r1.rubro');
			$listado_seo = $this->parseo_library->listado_rubros_seo_from_rubros($rubros_listado);
			$elementos_random = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'));
			$elementos_random_seo = $this->parseo_library->elementos_random_seo($listado_seo, $elementos_seo, $this->session->userdata('id_sucursal'), TRUE);
			asort($elementos_random);
			$elementos_random_seo_ordenados = $this->parseo_library->reemplazar_nombres_seo($elementos_random, $elementos_random_seo);
			$listado_seo_random = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random);
			$listado_seo_random_guiones = $this->parseo_library->listado_rubros_seo_from_rubros($elementos_random_seo_ordenados);

			if(isset($_GET['imagen'])){
				switch ($_GET['imagen']){
					case 1:
						$imagen = 'bg_home_1.jpg';
						break;
					case 2:
						$imagen = 'bg_home_2.jpg';
						break;
					case 3:
						$imagen = 'bg_home_3.jpg';
						break;
					default:
						$imagen = 'bg_home_1.jpg';
						break;
				}
			}else{
				$imagen = 'bg_home_1.jpg';
			}

			$ic = $this->parseo_library->equivalencia_iconos();

			$proveedores_destacados = $this->parseo_library->add_nombre_seo_a_listado($proveedores_destacados);

			if($this->uri->segment(1)){
				$description = 'Lo que necesitas para organizar tu casamiento en ' . $this->config_library->sucursal['sucursal'] . ': Ceremonia, fiesta, catering, vestido de novia, experiencias y consejos reales. Aquí encontrás todo!';
			}else{
				$description = 'De todo para organizar tu casamiento soñado: Ceremonia, fiesta, civil, catering, vestido de novia, experiencias y consejos reales. Aquí encontrás todo!';
			}

			$data = array(
				'banners'					=> $inf['banners'],
				'elementos_random'			=> $elementos_random,
				'ic'						=> $ic,
				'imagen_slideshow'			=> $imagen,
				'listado_seo_random'		=> $listado_seo_random,
				'listado_seo_random_guiones'=> $listado_seo_random_guiones,
				'meta_descripcion' 		    => $description,
				'meta_keywords'    		    => 'Portal casamientos, casamientos online, casamientosonline, casamiento, bodas, fiestas, novias, novios, ceremonia, salones, civil, luna de miel, vestidos, vestido de novia, trajes, servicios para casamiento, catering, barras de trago, fiestas de casamiento, maquillaje para novias, makeup, souvenirs, centros de mesa, ramos, tocados, cotillón, shows, peinadores, casas de regalos, reserva de salones, registro civil, fotógrafo, dj, disc jockey, foto y video, alianzas, tradiciones, noche de bodas, arreglos florales, ambientación, wedding planners, eventos, invitaciones, participaciones, textos de participaciones, listas de casamiento, organización de bodas, iglesias, proverbios del matrimonio, autos para casamientos, transporte, carruajes, spa, menús para casamientos, carnaval, preguntas, respuestas, música en la iglesia, madrinas, cortejo de niños, anécdotas de novias, peinados, lluvia de arroz, foro de novias, despedida de soltero soltera, guía del casamiento, blogs de casamientos, consejos sobre el casamiento, ideas',
				'meta_title'	   		    => 'Casamientos Online: Consejos y Proveedores ' . ($this->uri->segment(1) ? ('en ' . $this->config_library->sucursal['sucursal']) : 'para tu evento ideal'),
				'canonical'					=> base_url(($this->uri->segment(1) ? ('/' . $this->config_library->sucursal['nombre_seo']) : '')),
				'mostrar_rubros'			=> $mostrar_rubros,
				'proveedores_destacados'    => $proveedores_destacados,
				'proveedores_destacados_or' => $proveedores_destacados_or,
				'rubros_mas_consultados'	=> $rubros_mas_consultados,
				'twitter_image'				=> base_url('/assets/images/bg_home_1.jpg'),
				'ultimas_notas'				=> $ultimas_notas,
				'sucursal_seo'				=> TRUE,
				'sucursal_redirect'			=> '/',
				'slides'					=> $slides,
				'no_buscador_chico'			=> TRUE
			);

			$data['twitter_title'] = $data['meta_title'];
			$data['twitter_descripcion'] = $data['meta_descripcion'];

	        $this->cache->file->save($pageKey, $data, $this->config->item('cache_expiration'));
		}

		$this->load->view('home', $this->config_library->send_data_to_view($data));
	}

	public function buscar($id_sucursal = 1, $search = '', $tipo = 'todos'){
		if($search){
			$search = urldecode($search);
			$search_alt = $this->config_library->diccionario_alternativas($search);

			$this->load->model('banners_model');

			if($id_sucursal != $this->session->userdata('id_sucursal')) $this->config_library->cambiar_sucursal_comercial($id_sucursal);

			$ic = '';
			$resultados = 0;
			$elementos = array();
			$rows = 10;

			if(isset($tipo)){
				switch ($tipo){
					case 'proveedores':
						$this->load->model('proveedores_model');						
						
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos['proveedores'] = $this->proveedores_model->buscar_proveedores($search_alt ? $search_alt : $search, 'p.nivel, p.proveedor', 10, isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']+1:1, $id_sucursal);
							$elementos['proveedores_resultados'] = $this->proveedores_model->resultados;
							
							if(!empty($elementos['proveedores']) || !empty($elementos['proveedores_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$resultados = $elementos['proveedores_resultados'];
						$view = 'resultado_busqueda_proveedores';
						$meta_title = 'Lo mejor en ' . $search . ' de ' . $this->config_library->sucursal['sucursal'] . ' aquí en Casamientos Online';
						$meta_descripcion = 'Todos los proveedores de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' los encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;

					case 'promociones':
						$this->load->model('productos_model');
							
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){ 
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos['promociones'] = $this->productos_model->buscar_pyp($search_alt ? $search_alt : $search, 1, 'p.nivel, p.proveedor', 10, isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']+1:1, $id_sucursal);
							$elementos['promociones_resultados'] = $this->productos_model->resultados;
							
							if(!empty($elementos['promociones']) || !empty($elementos['promociones_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$resultados = $elementos['promociones_resultados'];
						$view = 'resultado_busqueda_promociones';
						$meta_title = 'Promociones de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
						$meta_descripcion = 'Todas las promociones de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' las encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;

					case 'productos':
						$this->load->model('productos_model');						
						
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){ 
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos['productos'] = $this->productos_model->buscar_pyp($search_alt ? $search_alt : $search, 0, 'p.nivel, p.proveedor', 10, isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']+1:1, $id_sucursal);
							$elementos['productos_resultados'] = $this->productos_model->resultados;
							
							if(!empty($elementos['productos']) || !empty($elementos['productos_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$resultados = $elementos['productos_resultados'];
						$view = 'resultado_busqueda_productos';
						$meta_title = $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
						$meta_descripcion = 'Todos los productos de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' los encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;

					case 'paquetes':
						$this->load->library('parseo_library');
						$this->load->model('paquetes_model');
						$ic = $this->parseo_library->equivalencia_iconos();						
						
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){ 
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos['paquetes'] = $this->paquetes_model->get_paquetes(0, 'nivel', 10, isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']+1:1, '', 0, '', array('term' => $search_alt ? $search_alt : $search, 'id_sucursal' => $id_sucursal));
							$elementos['paquetes_resultados'] = $this->paquetes_model->resultados;
							
							if(!empty($elementos['paquetes']) || !empty($elementos['paquetes_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$resultados = $elementos['paquetes_resultados'];
						$view = 'resultado_busqueda_paquetes';
						$meta_title = 'Paquetes de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
						$meta_descripcion = 'Todos los paquetes de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' los encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;

					case 'editorial':
						$this->load->model('editorial_model');
						
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){ 
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos['notas'] = $this->editorial_model->get_ultimas_notas(NULL, 0, NULL, array('term' => $search_alt ? $search_alt : $search), TRUE, $this->db->escape(((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']:0)*$rows)) . ',' . $this->db->escape($rows));
							$elementos['notas_resultados'] = count($this->editorial_model->get_ultimas_notas(NULL,0,NULL,array('term' => $search_alt ? $search_alt : $search),TRUE,200));
							
							if(!empty($elementos['notas']) || !empty($elementos['notas_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$resultados = $elementos['notas_resultados'];
						$view = 'resultado_busqueda_notas';
						$meta_title = 'Consejos de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
						$meta_descripcion = 'Todas la información y consejos de ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' las encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;

					case 'todos':
						$this->load->model('proveedores_model');
						$this->load->model('productos_model');
						$this->load->model('paquetes_model');
						$this->load->library('parseo_library');
						$this->load->model('editorial_model');
						
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){ 
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos = array(
								'notas'					 => $this->editorial_model->get_ultimas_notas(NULL,0,NULL,array('term' => $search_alt ? $search_alt : $search),TRUE,((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']:0)*$rows).','.$rows),
								'notas_resultados' 		 => count($this->editorial_model->get_ultimas_notas(NULL,0,NULL,array('term' => $search_alt ? $search_alt : $search),TRUE,200)),
								'proveedores' 			 => $this->proveedores_model->buscar_proveedores($search_alt ? $search_alt : $search, 'RAND(), p.nivel, p.proveedor', 5, 1, $id_sucursal),
								'proveedores_resultados' => $this->proveedores_model->resultados,
								'promociones' 			 => $this->productos_model->buscar_pyp($search_alt ? $search_alt : $search, 1, 'p.nivel, p.proveedor', 3, 1, $id_sucursal),
								'promociones_resultados' => $this->productos_model->resultados,
								'productos'   			 => $this->productos_model->buscar_pyp($search_alt ? $search_alt : $search, 0, 'p.nivel, p.proveedor', 3, 1, $id_sucursal),
								'productos_resultados'   => $this->productos_model->resultados,
								'paquetes'	  			 => $this->paquetes_model->get_paquetes(0, 'nivel', 3, 1, '', 0, '', array('term' => $search_alt ? $search_alt : $search, 'id_sucursal' => $id_sucursal)),
								'paquetes_resultados'	 => $this->paquetes_model->resultados
							);
							
							if(!empty($elementos['notas']) || !empty($elementos['notas_resultados']) || !empty($elementos['proveedor']) || !empty($elementos['proveedores_resultados']) || !empty($elementos['promociones']) || !empty($elementos['promociones_resultados']) || !empty($elementos['productos']) || !empty($elementos['productos_resultados']) || !empty($elementos['paquetes']) || !empty($elementos['paquetes_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$ic = $this->parseo_library->equivalencia_iconos();

						$view = 'resultado_busqueda';
						$meta_title = 'Encontrá ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
						$meta_descripcion = 'Todo lo que buscas sobre ' . $search . ' lo encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;

					default:
						$this->load->model('proveedores_model');
						$this->load->model('productos_model');
						$this->load->model('paquetes_model');
						$this->load->library('parseo_library');
						
						$salir = FALSE;
						$veces = 0;
						$hasta = 0;
						$word = $search_alt ? $search_alt : $search;
						$partes = array();
						while (!$salir) {
							$veces += 1;

							if($veces == 2){ 
								// No encontró resultados en la primer iteración, entonces entró a la segunda
								$partes = explode(' ', $word);
								$hasta = count($partes)+1;
							}

							if(!empty($partes)) $search_alt = $partes[$veces-2];

							$elementos = array(
								'notas'					 => $this->editorial_model->get_ultimas_notas(NULL,0,NULL,array('term' => $search_alt ? $search_alt : $search),TRUE,((isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']:0)*$rows).','.$rows),
								'notas_resultados' 		 => count($this->editorial_model->get_ultimas_notas(NULL,0,NULL,array('term' => $search_alt ? $search_alt : $search),TRUE,200)),
								'proveedores' 			 => $this->proveedores_model->buscar_proveedores($search_alt ? $search_alt : $search, 'RAND(), p.nivel, p.proveedor', 5, 1, $id_sucursal),
								'proveedores_resultados' => $this->proveedores_model->resultados,
								'promociones' 			 => $this->productos_model->buscar_pyp($search_alt ? $search_alt : $search, 1, 'p.nivel, p.proveedor', 3, 1, $id_sucursal),
								'promociones_resultados' => $this->productos_model->resultados,
								'productos'   			 => $this->productos_model->buscar_pyp($search_alt ? $search_alt : $search, 0, 'p.nivel, p.proveedor', 3, 1, $id_sucursal),
								'productos_resultados'   => $this->productos_model->resultados,
								'paquetes'	  			 => $this->paquetes_model->get_paquetes(0, 'nivel', 3, 1, '', 0, '', array('term' => $search_alt ? $search_alt : $search, 'id_sucursal' => $id_sucursal)),
								'paquetes_resultados'	 => $this->paquetes_model->resultados
							);
							
							if(!empty($elementos['notas']) || !empty($elementos['notas_resultados']) || !empty($elementos['proveedor']) || !empty($elementos['proveedores_resultados']) || !empty($elementos['promociones']) || !empty($elementos['promociones_resultados']) || !empty($elementos['productos']) || !empty($elementos['productos_resultados']) || !empty($elementos['paquetes']) || !empty($elementos['paquetes_resultados'])){
									$salir = TRUE;
							}
							if($veces == $hasta){
								$salir = TRUE;
							}
						}

						$ic = $this->parseo_library->equivalencia_iconos();

						$view = 'resultado_busqueda';
						$meta_title = 'Encontrá ' . $search . ' en ' . $this->config_library->sucursal['sucursal'] . ' | Casamientos Online';
						$meta_descripcion = 'Todo lo que buscas sobre ' . $search . ' lo encontrás aquí. Ingresá a nuestro portal de novios y aprovechá lo que tenemos para vos!';
						break;
				}
			}else{
				$view = 'resultado_busqueda';
			}

			$url_solapa  = $id_sucursal?('?id_sucursal='.$id_sucursal):'';
			$url_solapa .= $search?('&search=' . $search):'';

			$this->load->library('pagination');

			$config['base_url'] = 'http://buscador.' . DOMAIN . '/' . $this->config_library->sucursal['nombre_seo'] . '/' . $tipo . '/' . $search . '_CO_';
			$config['total_rows'] = $resultados/$rows;
			$config['per_page'] = 1;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'pag';
			$config['first_link'] = FALSE;
			$config['last_link'] = FALSE;
			$config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
			$config['full_tag_close'] = '</div></div>';
			$config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
			$config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
			$config['cur_tag_open'] = '<a class="active">';
			$config['cur_tag_close'] = '</a>';

			$this->pagination->initialize($config);

			$imagen_tw = '';

			foreach ($elementos as $el) {
				if(is_array($el)) foreach ($el as $e) {
					if(isset($e['pic'])) 		$imagen_tw = 'http://media.casamientosonline.com/images/' . str_replace('@', '.', $e['pic']);
					if(isset($e['imagen_gde'])) $imagen_tw = 'http://media.casamientosonline.com/images/' . str_replace('@', '.', $e['imagen_gde']);
					if(isset($e['imagen'])) 	$imagen_tw = 'http://media.casamientosonline.com/images/' . str_replace('@', '.', $e['imagen']);
					if($imagen_tw) break;
				}
				if($imagen_tw) break;
			}

			$secciones = array(
				'1'	=> 1057,
				'2' => 1058,
				'3' => 1059,
				'4' => 1060,
				'5' => 1061,
				'6' => 1062,
				'7' => 1063,
				'8' => 1063,
				'10'=> 1064,
				'11'=> 1065,
				'12'=> 1066
			);

			$banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);

			$canonical = explode('_CO_', base_url($_SERVER['REQUEST_URI']));
			if(!empty($canonical[0])) $canonical = $canonical[0];

			$data = array(
				'banners'			=> $banners,
				'view' 		  		=> $view,
				'url_solapa'  		=> $url_solapa,
				'search'	  		=> $search,
				'ic'				=> $ic,
				'elementos'			=> $elementos,
				'tipo'				=> $tipo,
				'meta_descripcion' 	=> $meta_descripcion,
				'meta_keywords'    	=> 'Portal casamientos, casamientos online, casamientosonline, casamiento, bodas, fiestas, novias, novios, ceremonia, salones, civil, luna de miel, vestidos, vestido de novia, trajes, servicios para casamiento, catering, barras de trago, fiestas de casamiento, maquillaje para novias, makeup, souvenirs, centros de mesa, ramos, tocados, cotillón, shows, peinadores, casas de regalos, reserva de salones, registro civil, fotógrafo, dj, disc jockey, foto y video, alianzas, tradiciones, noche de bodas, arreglos florales, ambientación, wedding planners, eventos, invitaciones, participaciones, textos de participaciones, listas de casamiento, organización de bodas, iglesias, proverbios del matrimonio, autos para casamientos, transporte, carruajes, spa, menús para casamientos, carnaval, preguntas, respuestas, música en la iglesia, madrinas, cortejo de niños, anécdotas de novias, peinados, lluvia de arroz, foro de novias, despedida de soltero soltera, guía del casamiento, blogs de casamientos, consejos sobre el casamiento, ideas, búsqueda, buscar',
				'meta_title'	   	=> $meta_title,
				'canonical'			=> $canonical,
				'twitter_image'		=> $imagen_tw
			);

			$data['twitter_title'] = $data['meta_title'];
			$data['twitter_descripcion'] = $data['meta_descripcion'];

			$this->load->view('resultado_busqueda', $this->config_library->send_data_to_view($data));
		}else{
			redirect(base_url('/home'));
		}
	}

	public function banner_hit($id_pedido = NULL, $id_banner = NULL){
		if(!$id_pedido || !$id_banner || !isset($_GET['backto']) || !$_GET['backto']) show_404();

		$url = $_GET['backto'];

		$this->load->model('banners_model');

		$url_redirect = $this->banners_model->click(array('id_pedido' => $id_pedido, 'id_banner' => $id_banner, 'url' => $url));

		redirect(str_replace('*', '://', $url_redirect), 'refresh');
	}

	public function sucursal($id){
		if(!$id) redirect(base_url('/'));

		$this->config_library->cambiar_sucursal($id);

		redirect(base_url('/'));
	}

	public function get_instagram_access_token(){
		$this->load->view('get_instagram_access_token');
	}
}