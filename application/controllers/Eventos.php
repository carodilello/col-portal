<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends MY_Controller{
	public function __construct(){
    	parent::__construct();
    	$this->load->library('config_library');
	}

	public function index(){
        $this->load->model('banners_model');

        $banners = $this->banners_model->views(117);

		$data = array(
            'banners' => $banners,
            'canonical' => base_url($_SERVER['REQUEST_URI']),
            'meta_descripcion' => 'Las más grandes Expos y Eventos para novias en Buenos Aires, Córdoba y Rosario están aquí!  Casamientos Online te lleva a ellos. Informate ahora.',
            'meta_keywords' => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, medios y eventos, Eventos',
            'meta_title' => 'Próximos Eventos y Exposiciones para Novias de Casamientos Online'
        );

        $this->load->view('medios_y_eventos', $this->config_library->send_data_to_view($data));
	}

    public function jornadas($ids_galeria = 419, $seccion = '', $id = ''){
        if(!is_numeric($ids_galeria)) redirect(base_url('/eventos/expo-novias-' . date('Y') . '/registro/novios'));

        $this->load->model('banners_model');
        $this->load->model('navegacion_model');
        $this->load->model('media_model');
        
        $this->load->library('parseo_library');

        $provincias = $this->navegacion_model->get_provincias(0);
        $localidades = NULL;
        
        $success = FALSE;
        $succes_update = FALSE;
        if($_POST){
            $this->load->library('form_validation');
            
            $error_required = array(
                'required' => 'Debe ingresar un %s.',
            );

            $error_required_hidden = array(
                'required' => 'Su formulario contiene errores'
            );
            
            $this->form_validation->set_rules('id_origen', 'id_origen', 'required', $error_required_hidden);
            $this->form_validation->set_rules('id_evento', 'id_evento', 'required', $error_required_hidden);
            
            $this->form_validation->set_rules('tipo', 'tipo', 'required', $error_required);
            $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
            $this->form_validation->set_rules('apellido', 'Apellido', 'required', $error_required);
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
                    'required'      => 'Debe ingresar un %s.',
                    'valid_email'   => 'Debe ingresar un %s válido.',
            ));
            $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
            
            if(!$seccion){
                $this->form_validation->set_rules('id_provincia', 'Provincia', 'required', $error_required);
                $this->form_validation->set_rules('id_localidad', 'Localidad', 'required', $error_required);    
            }

            if($this->form_validation->run() == TRUE){ // success
                $this->load->library('procesos_library');

                if($_POST['tipo'] == 'empresa'){

                    $_POST['id_referencia'] = 49;
                    $_POST['id_origen_contacto'] = 56;
                    $_POST['id_origen'] = 56;

                    if(empty($_POST['update_data'])){
                        $this->procesos_library->procesar_form_proveedor($_POST);
                    }else{
                        $succes_update = TRUE;
                        $this->procesos_library->actualizar_datos_proveedor($_POST);
                    }
                }else{

                    $_POST['id_referencia'] = 48;
                    $_POST['id_origen_contacto'] = 55;
                    $_POST['id_origen'] = 55;

                    if(empty($_POST['update_data'])){
                        $this->procesos_library->procesar_form_novio($_POST);
                    }else{
                        $succes_update = TRUE;
                        $this->procesos_library->actualizar_datos_novio($_POST);
                    }
                }

                $success = TRUE;
            }
        }

        $data_form = $this->parseo_library->asignar_mayoria_campos_formulario($id, $seccion);

        $combo_rubros = $this->navegacion_model->get_rubros();
        if(!empty($data_form['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        if(!$seccion){
            $banners = $this->banners_model->views(118);

            $id_evento = $this->navegacion_model->get_id_evento();
            $id_evento_empresa = $this->navegacion_model->get_id_evento(4);

            $galerias = explode(',', $ids_galeria);
            $galeria = array();
            foreach ($galerias as $key => $v){
                $galeria = $this->media_model->get_galeria(array('id_galeria' => $v));
                if(isset($galeria[0]) && $galeria[0]) $galeria = $galeria[0];
            }

            $data = array(
                'banners'               => $banners,
                'canonical'             => base_url($_SERVER['REQUEST_URI']),
                'combo_rubros'          => $combo_rubros,
                'data_form'             => $data_form,
                'galeria'               => $galeria,
                'id_evento'             => $id_evento,
                'id_evento_empresa'     => $id_evento_empresa,
                'localidades'           => $localidades,
                'meta_descripcion'      => 'ExpoNovias: el evento más grande para las novias organizadoras y los novios que desean apoyarlas. Entrada Gratuita. Cupos Limitados. Reservá tu entrada aquí!',
                'meta_keywords'         => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, eventos',
                'meta_title'            => 'Exponovias ' . date('Y') . ' - El mayor evento del año | Casamientos Online',
                'provincias'            => $provincias,
                'success'               => $success,
                'succes_update'         => $succes_update,
                'twitter_title'         => 'Participá de la 40º Jornada de Casamientos Online y organizá todo tu casamiento en una sóla tarde!',
                'twitter_image'         => base_url('/assets/images/jornadas_registro_header.jpg'),
                'twitter_descripcion'   => 'La próxima Jornada de Casamientos Online se realizará el miércoles 27 de septiembre de 2017 en el Centro Cultural Borges. Esperamos contar con tu presencia. Si te interesa participar ya podés registrarte completando el formulario. Estate atenta...te mantendremos al tanto de las novedades! Entrada gratuita - Cupos limitados'
            );
            
            $view = 'jornadas_registro';
        }elseif($seccion == 'novios' || $seccion == 'empresas'){
            $data = array(
                'success'               => $success,
                'succes_update'         => $succes_update,
                'combo_rubros'          => $combo_rubros,
                'data_form'             => $data_form,
                'seccion'               => $seccion,
                'provincias'            => $provincias,
                'localidades'           => $localidades,
                'meta_descripcion'      => 'ExpoNovias: el evento más grande para las novias organizadoras y los novios que desean apoyarlas. Entrada Gratuita. Cupos Limitados. Reservá tu entrada aquí!',
                'meta_keywords'         => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, eventos',
                'meta_title'            => 'Exponovias ' . date('Y') . ' - El mayor evento del año | Casamientos Online',
                'twitter_title'         => 'Participá de la 40º Jornada de Casamientos Online y organizá todo tu casamiento en una sóla tarde!',
                'twitter_image'         => base_url('/assets/images/jornadas_registro_header.jpg'),
                'twitter_descripcion'   => 'La próxima Jornada de Casamientos Online se realizará el miércoles 27 de septiembre de 2017 en el Centro Cultural Borges. Esperamos contar con tu presencia. Si te interesa participar ya podés registrarte completando el formulario. Estate atenta...te mantendremos al tanto de las novedades! Entrada gratuita - Cupos limitados'
            );

            $view = 'jornadas_registro_formulario';
        }

    	$this->load->view($view, $this->config_library->send_data_to_view($data));
    }

    public function jornadas_anteriores($anio = '2015'){
        $this->load->model('navegacion_model');
        $this->load->model('media_model');
        
        $this->load->library('parseo_library');

        $anios = array();
        for ($i=2000; $i <= date('Y'); $i++) {
            $anios[] = $i;
        }

        if(!in_array($anio, $anios)) redirect(base_url('/eventos/expo-novias-' . date('Y')));

        $provincias = $this->navegacion_model->get_provincias(0);
        $localidades = NULL;

        $data_form = $this->parseo_library->asignar_mayoria_campos_formulario();

        if(!empty($data_form['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        $id_evento = $this->navegacion_model->get_id_evento();
        $id_evento_empresa = $this->navegacion_model->get_id_evento(4);

        $id_seccion = '';
        switch ($this->session->userdata('id_sucursal')){
            case 1:
                $id_seccion = 118;
                break;
            case 2:
                $id_seccion = 257;
                break;
            case 3:
                $id_seccion = 304;
                break;
        }

        $content = array(
        	'2016' => array(
                array(
                    'titulo'        => 'Jornada 39º - <span class="green">28 de Septiembre de 2016</span>',
                    'texto'         => 'El 28 de septiembre se realizó la Jornada número 39º de Casamientos Online. Nuestro ya tradicional evento de una tarde que reúne a las parejas que se están por casar con las principales empresas de servicios para casamientos.<br /><br />
Hubo de todo: degustaciones, barra de tragos, cata de vinos, shows en vivo, moda, tendencias, vestidos de novia de todos los estilos y muchas otras novedades más!!!
Viniste a la Jornada? Reviví los mejores momentos de la mejor expo para novias del país!! Repasá las tendencias, buscáideas y dejate inspirar por lo mejor para tu casamiento!
<br /><br /><strong>Fotos by CUIKA + Video by Cucu Amour</strong>.',
                    'video'         => '//www.youtube.com/embed/PFMHWWr-p7c',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 419))
                    ),
                array(
                    'titulo'        => 'Jornada 38º - <span class="green">4 de Mayo de 2016</span>',
                    'texto'         => 'Una nueva exponovias tuvo lugar el 4 de mayo de 2016 en el Centro Cultural Borges. Novias y novios recorrieron los diferentes stands, probaron cosas ricas en los puestos de degustación, escucharon la mejor música de Grupo Sarapura, tomaron ricos tragos y conocieron las últimas tendencias en moda para novias!!!
Una tarde intensa, divertida, útil y sobretodo, con mucha información!!<br /><br />
Parejas que recién comenzaron a organizar su boda y otras tantas con algunos temas pendientes por resolver, encontraron en la Jornada de Casamientos Online todo lo que necesitaban!!
Ambientación, Participaciones de boda, vestidos de novia, wedding planners, luna de miel, noche de bodas, maquillaje, peinado, zapatos de novia, propuestas para el civil, ideas originales, propuestas clásicas y mucha información para que organizar el casamiento sea más fácil.
Mirá este resumen realizado por Estudio D31 y enterate de qué se tratan las exponovias que ya son un MUST de los casamientos!',
                    'video'         => '//www.youtube.com/embed/Mj-TE9azbKQ?list=PL566CE2F413F20516',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 417))
                    )
            ),
            '2015' => array(
                array(
                    'titulo'        => 'Jornada 37º - <span class="green">30 de Septiembre de 2015</span>',
                    'texto'         => 'El 30 de septiembre de 2015 fue la Jornada número 37 de Casamientos Online. Como es habitual, las parejas próximas a casarse se reunieron con las principales empresas de servicios para bodas.<br />
Hubo de todo: degustaciones, barra de tragos, cata de vinos, shows en vivo, moda, tendencias, vestidos de novia de todos los estilos y muchas otras novedades más!!!<br>(Video by: Diego Caamaño Fotografía)',
                    'video'         => '//www.youtube.com/embed/UnC7J33BzXI?list=PL566CE2F413F20516',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 395))
                    ),
                array(
                    'titulo'        => 'Jornada 36º – <span class="green">6 de Mayo de 2015</span>',
                    'texto'         => 'El 6 de mayo de 2015 fue la Jornada número 36º de Casamientos Online. Como es habitual: estuvo MUY BUENA!! Hubo de todo: propuestas de todos los rubros, galería de vestidos de novia, degustaciones, regalitos, cabinas de fotos, barra de tragos, sorteos, shows en vivo... y mucho, mucho más!! Con cada expo, buscamos superarnos, mejorar la experiencia de las parejas que vienen a recorrerla y ayudarlas a organizar su casamiento en un tarde! La Jornada es la mejor expo para los novios que están en proceso de organización de su fiesta.',
                    'video'         => '//www.youtube.com/embed/gBlaYI782Nw',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 390))
                    )
            ),
            '2014' => array(
                array(
                    'titulo'        => 'Jornada 35 de Casamientos Online - <span class="green">3 de Septiembre de 2014</span>',
                    'texto'         => 'El 3 de septiembre de 2014 fue la Jornada número 35º de Casamientos Online. Estuvo buenisíma! Propuestas de todos los rubros, galería de vestidos de novia, degustaciones, regalitos, cabinas de fotos, barra de tragos, sorteos, la ya consagrada conferencia de Martin Roig, shows en vivo, una batucada hizo bailar a todos los que estaban en el Borges... y mucho, mucho más!! En cada Jornada buscamos armar un evento único, útil y a medida de las necesidades de los novios que están en pleno proceso de organización de su casamiento y con cada expo, superamos a la anterior, así que si estás próxima a decir “si, quiero” no te pierdas esta oportunidad de armar algo especial para el gran día...
Chusmeá todo lo que pasó en la mejor expo para novias del país!!',
                    'video'         => '//www.youtube.com/embed/3-tEYacEeMo',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 382))
                    ),
                array(
                    'titulo'        => 'Jornada 34 de Casamientos Online - <span class="green">7 de Mayo de 2014</span>',
                    'texto'         => 'El 7 de mayo de 2014 realizamos la Jornada nro 34 de Casamientos Online en el Centro Cultural Borges... Desde que Novias y Acompañantes (que hubo de todo un poco: novios, mamás, amigas, hermanas, compañeras de trabajo!) llegaron, las vimos disfrutar, compartir, preguntar, buscar info y armar su fiesta con mucha alegría. Casamientos Online está presente en la vida de las parejas próximas a casarse desde 1999 y desde ese entonces amamos lo que hacemos y trabajamos para superarnos! Esperamos que la hayan pasado tan bien como nosotros!',
                    'video'         => '//www.youtube.com/embed/aWYUKTUt1Fs',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 378))
                    )
            ),
            '2013' => array(
                array(
                    'titulo'        => 'Jornada 32 - <span class="green">17 de ABRIL de 2013, CENTRO CULTURAL BORGES, BUENOS AIRES</span>',
                    'texto'         => 'El 17 de abril fue la Jornada n°32 de Casamientos Online, una expo única y la mejor!! Te propusimos pasar una tarde genial organizando tu casamiento con las últimas propuestas, las clásicas de siempre y con datos súper útiles. Martin Roig dio una conferencia en vivo con los mejores consejos para aplicar en tu casamiento. Grupo Sarapura demostró una vez más porque es el número uno!! Hubo shows de la mano de Alexia, Cabinas en vivo para sacarse fotos y dejar mensajes divertidos. Casamientos Online se sumó a la conciencia ECO-FRIENDLY y junto a Sophie & Flowers armaron una ambientación amigable con el medio ambiente!! Preparamos una súper galería de vestidos de novia con muchos estilos: Josefina Repetto, Claudio Cosano, Ximena Alfaro, Ambas Novias, Ana Bruno de Amor Bonnito, Romina Albizu, Paola Pascua, Claudia Oyhandy, Son Santas, Moira Mora, Lucila Astarloa, Hanna, Patricia Profumo, Iara, María Pryor, Las Demiero dijeron presente con sus diseños. Hubo degustaciones de cosas ricas, dulces y saludables. La Escuela Argentina de Sommeliers festejó el día del Malbec junto a las novias con degustaciones de vinos exquisitos de las mejores bodegas del país!! Además Elie Saab y Pantene estuvieron presentes en la Jornada dando regalitos a las novias!!! Y como si todo esto fuera poco, el Gobierno de la Ciudad también se sumó a la Jornada con un stand informativo con el plus de otorgar turnos a las parejas que lo solicitaran!! Un verdadero lujo que se repetirá en 30 de octubre en el Centro Cultural Borges, no te la podés perder!!<br>Mirá las fotos y el video, una cobertura impecable de Mariano Rodriguez!',
                    'video'         => '//www.youtube.com/embed/vebwWqDHS-A',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 359))
                    )
            ),
            '2012' => array(
                array(
                    'titulo'        => 'Jornada 31 - <span class="green">3 de Octubre 2012, Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'El miércoles 3 de octubre fue la Jornada de Casamientos Online donde hubo empresas de todos los rubros, variedad de propuestas, ideas y novedades! Tres pabellones con lo mejor de los casamientos… En esta oportunidad hubo de todo… Desde Puntos interactivos: Puestos de maquillaje para empezar a verte como novia, puestos de degustación, una barra de tragos riquísimos, una cabina de fotos y un TOTEM de fotos para llevarte un souvenir especial y shows en vivo. Los vestidos de novia invadieron la Jornada: Josefina Repetto, Claudio Cosano, Ana Pugliesi, Rouge Ivoire, Diego Vaz, Ana Novias, Lorea Schwartzman, Ezequiel García, Sonia Uballes, Son Santas, María Magnin, Laura Valenzuela, Casa Raphia, Hanna y Lindissima presentaron sus últimas colecciones!!.<br><br>Arcor también dijo presente con un regalito más que especial, galletitas rex y barritas Cereal Mix para picar mientras recorrían la expo. Y para completar la tarde, hubo shows que acompañaron un evento único para novias! Grupo Sarapura pasó música de principio a fin… un lujo! Y Martin Roig sorprendió y entretuvo a las novias con los mejores consejos para organizar su casamiento',
                    'video'         => '//www.youtube.com/embed/aiNSZSLrv6A',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 335))
                    ),
                array(
                    'titulo'        => 'Jornada 30 - <span class="green">9 de Mayo 2012, Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => '<strong>El miércoles 9 de mayo fue la Jornada de Casamientos Online donde hubo empresas de todos los rubros, variedad de propuestas, ideas y novedades! </strong>Tres pabellones con lo mejor de los casamientos...<br>En esta oportunidad hubo de todo… Desde Puntos interactivos: Puestos de maquillaje para empezar a verte como novia, puestos de degustación, una barra de tragos riquísimos, una cabina de fotos y un TOTEM de fotos para llevarte un souvenir especial, shows en vivo, livings de led para descansar  un ratito...<br>Se mostraron las <strong>últimas tendencias en vestidos:</strong> Hayré Egea, Constanza Sturla, Ivana Picallo y Son Santas presentaron algunos vestidos de su última temporada.<br>Las chicas de BZ Ambientaciones pasaron por el Borges y lo dejaron INCREIBLE!! Desde la acreditación hasta los rincones de los pabellones tenían una impronta de casamiento. Arcor también dijo presente con un regalito más que especial, galletitas rex, barritas Cereal Mix y Ser snacks para picar mientras recorrían la expo.<br><strong>Como siempre el Espacio de las Revistas estuvo en la Jornada.</strong> Fiancee, Propuestas & Servicios, Bridal Time, Fiestas y Eventos y, Novias Punto de Partida repartieron sus últimos ejemplares y realizaron sorteos.<br><strong>Y para completar la tarde, los shows acompañaron un evento único para novias! El Show de Geishas</strong> de Eurinome dio un toque original y oriental, en tanto que Alexia Skoufalos cantó en la acreditación cantando los temas más pedidos y Grupo Sarapura pasó música de principio a fin...un lujo!',
                    'video'         => 'http://www.youtube.com/embed/ttiiolJmtrU?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 271))
                    )
            ),
            '2011' => array(
                array(
                    'titulo'        => 'Jornada 29 - <span class="green">7 de Septiembre 2011, Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'El miércoles 7 de septiembre fue la Jornada 29 de Casamientos Online y la última del año!! Fue increíble, muchísimas novias se reunieron con más de 100 empresas de todos los rubros con el objetivo de organizar en una tarde su casamiento!<br>En esta oportunidad hubo de todo: Shows de circo y líricos, un robot láser, una cabina de fotos dónde los novios se sacaban su foto y se la llevaban de recuerdo, degustaciones a cargo de Obar, de La Abuela Anna Catering y Fly Sushi. Marcas de primer nivel como Cadbury y Nivea estuvieron presente dando regalos a los asistentes. Eidico también dijo presente con su propuesta Eidico Casas, ideal para las parejas que sueñan con vivienda propia. Además de moda, ideas, tendencias clásicas y novedosas de todos los temas que tienen que ver con la planificación de casamientos.<br>Grupo Sarapura y Martin Roig, partners incondicionales de Casamientos Online acompañaron a las novias con sus mejores propuestas e ideas para fiestas.',
                    'video'         => 'http://www.youtube.com/embed/-xlprcVRfSg?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 271))
                    ),
                array(
                    'titulo'        => 'Jornada 28 - <span class="green">4 de Mayo 2011, Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'El 4 de mayo se realizó la Jornada Nº 28 de Casamientos Online en el Centro Cultural Borges. El evento convocó más de 3000 personas y cientos de proveedores dijeron presente para asesorar a las novias en todo cuanto pudieran para la organización del casamiento. Hubieron propuestas y tendencias de todos los rubros: desde vestidos de novia, peinados, tratamientos de belleza y maquillaje, hasta Caterings, Salones de Fiesta, shows, souvenirs, listas de regalos y Participaciones entre otros.<br>La Jornada se convirtió en un punto de encuentro productivo, completo y auténtico para todas las novias que están en plena organización del casamiento. En una sola tarde reciben todo el asesoramiento necesario y clarifican todo tipo de dudas en manos de los mejores expertos y profesionales. El evento tuvo de todo: una conferencia a cargo de Martin Roig, la presencia inigualable de Grupo Sarapura, degustaciones de Obar, Chungo, Habitus Multieventos y Epicurea, exposiciones de vestidos de novia, sorteos y actividades en vivo!<br>Si no pudiste ir a esta Jornada, te invitamos a que nos acompañes en nuestra próxima Jornada en el 2012! REGISTRATE!',
                    'video'         => 'http://www.youtube.com/embed/fYcklE3-DmY?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 174))
                    )
            ),
            '2010' => array(
                array(
                    'titulo'        => 'Jornada 27 - <span class="green">7 de Septiembre de 2010, Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'El 7 de Septiembre nos volvimos a encontrar en el Centro Cultural Borges para celebrar nuestra Jornada Nº27! Más de 2200 novias dieron el presente, cientos de proveedores nos acompañaron para cerrar este 2010 con todo!! Con expectativas cumplidas y nuevas ideas para las Jornadas 2011! Hubo de todo: sorpresas, degustaciones y muchísimas tendencias! Hubieron 2 pabellones dedicados a la FIESTA con muchísimos proveedores que respondieron todo tipo de dudas y asesoraron a las novias en todo lo que necesitaban para organizar la fiesta de su casamiento! Y también hubo 1 pabellón dedicado exclusivamente a la NOVIA: un espacio en donde las novias miraron tendencias, compararon vestidos de novia, se hicieron pruebas de maquillaje, consultaron peinados y mucho más!<br>Como siempre, la fiesta de Grupo Sarapura y la Barra de Tragos de Obar fueron protagonistas de la música, los tragos, el baile y el clima de fiesta! La Conferencia de Martin Roig tuvo una convocatoria increíble: las novias no pararon de reírse, de preguntar, de anotar! Además, hubo un Espacio de Revistas (Novias Magazine, Fianceé, Punto de Partida, Brides Time, Hacer Familia, Fiestas y Eventos, Propuestas y Servicios), reportajes de Canales Latinoamericanos, Actividades de Kendras, Dermaglós, degustaciones de Quilmes, Chungo y The Chocolate Fondue, exposiciones de vestidos de novia de Benito Fernandez, Verónica de la Canal y Rouge Ivoire y muchas sorpresas más!.<br>Y como siempre, nos volvimos a encontrar con todas las novias de nuestra COMUNIDAD en el Vip de Novias! Gracias Locas por la Ropa por llevar todos los conjuntos originales para el civil, a Sebastián Correas por darle a las novias los mejores Tips en el peinado, a Sushi POP, a Royal y a Bodegas Nieto Senetiner por las degustaciones y a las Demiero por los vestidos de novia divinos!! Si querés formar parte de nuestra Comunidad te invitamos a ser parte de los Blogs, las Notas y todos nuestras redes de comunicación! Nos encantó ayudar a todas las novias y ver la gran convocatoria que tuvo el evento! Fue una tarde en donde las novias aprovecharon para organizar todo su casamiento en una sola tarde y además pasaron una tarde increíble!!<br>Si no pudiste ir a esta Jornada, te invitamos a que nos acompañes en nuestra próxima Jornada en el 2011! REGISTRATE!',
                    'video'         => 'http://www.youtube.com/embed/VdC0W1G6WnU?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 173))
                    ),
                array(
                    'titulo'        => 'Jornada 26 - <span class="green">20 de abril de 2010, Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'El 2010 empezó con todo. EL 20 de Abril, el Centro Cultural Borges superó la convocatoria de novias y proveedores en la Jornada Nº 26 de Casamientos Online. Nuevamente, tres pabellones increíbles con propuestas de todos los rubros y para todos los estilos: dos dedicados a la FIESTA y uno a la NOVIA. ¡El evento adquirió una magnitud impensable!<br>Además, no faltó la fiesta en vivo del Grupo Sarapura, la barra de tragos de Obar, un espacio de Revistas que incluyo a Fianceé, Novias Magazine, Fiestas y Eventos, Propuestas y Servicios, Brides Time, Hacer Familia, Guía Arroz con leche. Además las novias pudieron disfrutar del espacio exclusivo de Kendras pensado para que la novia llegue divina al casamiento. La cúpula del Borges se vistió de blanco con Vestidos de Novia de Verónica de la Canal, Inés Ricur, Rouge Ivoire, Marcelo Serna, Claudio Cosano, Las Infantas, Las Demiero y Fiorella Ricciardi.<br>¡EL VIP de Novias explotó y tuvo grandes innovaciones! Una ambientación con mucho glamour de Estilo Chic, actividades en vivo y exclusivas, para las novias de nuestra comunidad: limpiezas de cutis de Dermaglós, pruebas de peinados de novias by Andrea Peinados, diseños personalizados de Vestidos de Novia por Ezequiel García, Romanoff Catering y la participación exclusiva de la Bodega Navarro Correas haciendo degustaciones de sus espumantes. La infaltable Conferencia de Martin Roig deslumbró con los mejores tips para la ORGANIZACIÓN deL CASAMIENTO , con su clásica cuota de humor, originalidad y profesionalismo.<br>Hubieron degustaciones muy originales de: Bodega Callia, La Pata Express, Quilmes, Chungo Helados, The Chocolate Fondeu y las famosas barras de Obar.<br>¡Fue una tarde imperdible y muy productiva! Todos los novios y empresas tuvieron la oportunidad de encontrarse, de aprovechar la Jornada y de pasarla genial!<br>',
                    'video'         => 'http://www.youtube.com/embed/xdxGMRJN-yU?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 172))
                    )
            ),
            '2009' => array(
                array(
                    'titulo'        => 'Jornada 25 - <span class="green">3 de septiembre, 2009 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'Una vez más Casamientos Online, se supera en cada Jornada. Esta 25 edición superó a las anteriores! Más novias asistentes y más empresas que acercaron sus propuestas y novedades.<br>Tres pabellones destinados a las empresas expositoras, con propuestas de todos los rubros y para todos los estilos. La Jornada sorprendió: un espacio exclusivo de Le Tissu pensado para diseñar vestidos de novia y probarse toiles en vivo. Un living para las novias Vips donde las asistentes tuvieron la oportunidad de probar algunas de las cosas ricas de Malcolm Catering y degustar vinos de Bodegas Salentein. Un lugar destinado a las novias que interactúan en el portal, donde expertos como Laura Valenzuela, Mabby Autino, Estilo Chic y Falabella Viajes dieron consejos. Y, un auditorio destinado a la clásica Conferencia de Martin Roig quien una vez más, sorprendió a las novias con una charla divertida, amena e interesantísima acerca de la organización del casamiento.<br>Un espacio destinado a las principales revistas: Fiestas & Eventos, La 100, Hacer Familia, Fiancee, Guia Arroz con leche y Novias Magazine. Puntos de degustación a cargo de Luigi Bosca, Chungo Helados, The Chocolate Fondue, Navarro Correas y Obar que ofrecieron a los novios cosas ricas para disfrutar a lo largo de toda la tarde.<br>El espacio de LA FIESTA de Grupo Sarapura dijo presente nuevamente con la mejor música. Cientos de novias se acercaron al equipo para hacerles preguntas y sacarse todas las dudas!<br>La cúpula del Borges se vistió de blanco con vestidos de Laura Valenzuela, Pia Carregal y Le Tissú<br>Una tarde imperdible y productiva donde novios y empresas tuvieron la oportunidad de encontrarse y pasar un buen momento.',
                    'video'         => 'http://www.youtube.com/embed/I3q12DDirEM?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 171))
                    ),
                array(
                    'titulo'        => 'Jornada 24 - <span class="green">31 de marzo, 2009 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'La primera Jornada del 2009 fue, nuevamente, un éxito! Tanto por la cantidad de novios asistentes como por la calidad, cantidad y variedad de empresas presentes.<br>Dos Pabellones, El Casamiento y La Fiesta, con propuestas de todos los rubros y para todos los estilos. Y, el tercer pabellón destinado a la clásica Conferencia de Martin Roig quien una vez más, sorprendió a las novias con una charla divertida, amena e interesantísima acerca de la organización del casamiento.<br>Un espacio destinado a las principales revistas: Ohlalá, Hacer Familia, Fiancee y Novias Magazine. Puntos de degustación a cargo de Luigi Bosca, Chungo Helados, The Chocolate Fondue, Navarro Correas y Obar que ofrecieron a los novios cosas ricas para disfrutar a lo largo de toda la tarde.<br>El espacio de LA FIESTA de Grupo Sarapura dijo presente nuevamente con la mejor música. Cientos de novias se acercaron al equipo para hacerles preguntas y sacarse todas las dudas!<br>La cúpula del Borges se vistió de blanco con vestidos de Laura Valenzuela, Las Demiero, Ezequiel García, Gabriel Lage, Verónica de la Canal y Pía Carregal.<br>Esta edición de las Jornadas presentó una novedad: El VIP de Novias! Un espacio pensado a medida de las novias que más interactúan en el portal, colaborando con otras novias, ayúdandolas con sus comentarios y participando con sus opiniones. Un living ambientado por Ramiro Arzuaga, donde él, Poli Martínez, de Poli Make Up & Store; María Inés Novegil, Wedding Planner y, Andrea Bursten (Asesora de Imagen) esperaban a los novios para conversar con ellos y ayudarlos en la organización de su casamiento.<br>En el VIP los novios tuvieron la oportunidad de probar algunas de las cosas ricas de Catering Melao y tomar algo de Finca El Portillo, de Bodegas Salentein!<br>Una tarde imperdible y productiva donde novios y empresas tuvieron la oportunidad de encontrarse y pasar un buen momento.',
                    'video'         => 'http://www.youtube.com/embed/HDPdyXzkW-g?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 170))
                    )
            ),
            '2008' => array(
                array(
                    'titulo'        => 'Jornada 23 - <span class="green">2 de septiembre, 2008 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'La XXIII Jornada de Casamientos Online sorprendió más que nunca!!! Por un lado el pabellón de La Novia con sus propuestas para que seas la más linda y por otro, el pabellón de El Casamiento con empresas de todos los rubros para ayudarte en la organización de la fiesta no tuvieron desperdicio.<br>Grupo Sarapura dijo presente con una fiesta en vivo con tracks de sus mejores disc-jockeys, las versiones más originales para entrar al salón, novedades de ambientación, efectos especiales y shows en vivo. Martin Roig dio sus mejores consejos en una conferencia donde cientos de novios escucharon atentos todo lo que tenía para contar.<br>En la cúpula Claudio Cosano sorprendió con los mejores diseños de vestidos de novia y de fiesta. Más de 100 empresas, degustaciones a cargo de The Chocolate Fondue, Chungo, Obar, Luigi Bosca y cerveza Primitiva, vestidos de novia para todos los estilos aseguraron una tarde exitosa e inolvidable.<br>Fue sin dudas una tarde intensa y divertida para ayudarte y acompañarte en la organización de tu casamiento.',
                    'video'         => 'http://www.youtube.com/embed/v2NO-aUQgkY?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 169))
                    ),
                array(
                    'titulo'        => 'Jornada 22 - <span class="green">29 de Abril, 2008 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'La jornada nro. 22 de Casamientos Online fue increíble!!! Tres pabellones completos del Centro estaban destinados a los novios: uno dedicado a los rubros de BELLEZA, otro a la FIESTA y el tercero donde Grupo Sarapura mostró todo su despliegue en un espacio destinado a una fiesta súper top y donde, en otro sector, Martin Roig dio su tradicional conferencia.<br>Más de 70 empresas del mercado de diferentes rubros dijeron presente, acompañando a los novios y conversando con ellos acerca de las mejores propuestas para su casamiento.<br>Una cúpula con vestidos de los diseñadores más importantes del país dieron el toque Glam a la tarde! Estudio PHOTO sorprendió con una muestra fotográfica con la historia de un casamiento contada de manera poco convencional que recorrió todo el pabellón de Belleza...<br>En los espacios de degustaciones, Outletbar.com, The Chocolate Fondue, Chungo, MFG Catering y Luigi Bosca ofrecieron variedad de cosas ricas en todo momento.<br>Nuestro Departamento de Orientación Familiar también formó parte de la Jornada! Clara Naón y Suku Arrieta respondieron todas las consultas de las novias que se acercaron a su espacio.',
                    'video'         => 'http://www.youtube.com/embed/HDPdyXzkW-g?rel=0',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 168))
                    )
            ),
            '2007' => array(
                array(
                    'titulo'        => 'Jornada 21 - <span class="green">26 de septiembre, 2007 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'Una nueva Jornada, un nuevo éxito. Casamientos Online se supera evento tras evento!! La apuesta del portal cada vez es mayor y se demuestra con hechos...<br>Tres pabellones completos destinados por completo al portal. Uno a todo lo referido a BELLEZA, otro a LA FIESTA y uno dedicado a la conferencia de Martin Roig.<br>Una cúpula con vestidos de Verónica de la Canal, Opaloca, Iara y ajuar de Rompecorazones para enamorar… El pabellón Belleza con diferentes propuestas para llegar divinas y más de 70 vestidos de novia. En el espacio de La Fiesta, empresas de todos los rubros, puestos de degustaciones a cargo de Gauri Catering, Persicco, Cinnamon Cakes, The Chocolate Fondue, Quilmes, Obar, Luigi Bsoca y Navarro Correas.<br>En todo momento Grupo Sarapura pasó música. La mejor onda y todos los temas que no pueden faltar en ningún casamiento!! La conferencia de Martin Roig, como es habitual, fue uno de los momentos más esperados de la tarde. En una charla divertida y sin desperdicios aconsejó a las novias acerca de lo mejor para su fiesta.',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 167))
                    ),
                array(
                    'titulo'        => 'Jornada 20 - <span class="green">27 de Marzo, 2007 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'La jornada nro. 20 de Casamientos Online fue increíble!!! Tres pabellones completos del Centro estaban destinados a los novios: uno dedicado a los rubros de BELLEZA, otro a la FIESTA y el tercero donde Grupo Sarapura mostró todo su despliegue en un espacio destinado a una fiesta súper top y donde, en otro sector, Martin Roig dio su tradicional conferencia.<br>Más de 70 empresas del mercado de diferentes rubros dijeron presente, acompañando a los novios y conversando con ellos acerca de las mejores propuestas para su casamiento.<br>Una cúpula con vestidos de los diseñadores más importantes del país dieron el toque Glam a la tarde! Estudio PHOTO sorprendió con una muestra fotográfica con la historia de un casamiento contada de manera poco convencional que recorrió todo el pabellón de Belleza...<br>En los espacios de degustaciones, Outletbar.com, The Chocolate Fondue, Chungo, MFG Catering y Luigi Bosca ofrecieron variedad de cosas ricas en todo momento.<br>Nuestro Departamento de Orientación Familiar también formó parte de la Jornada! Clara Naón y Suku Arrieta respondieron todas las consultas de las novias que se acercaron a su espacio.',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 165))
                    )
            ),
            '2006' => array(
                array(
                    'titulo'        => 'Jornada 19 - <span class="green">19 de Septiembre, 2006 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'La XIX Jornada para Organizar tu Casamiento se llevó a cabo el martes 19 de septiembre y como siempre, fue un éxito!! Una tarde muy exclusiva en la cual, alrededor de 1000 novias se encontraron con las principales empresas del mercado.<br>Una vez más Luigi Bosca acompañó a Casamientos Online y a las novias con degustaciones de su línea Finca La Linda. MFG Catering y The Chocolate Fondue también estuvieron presentes con degustaciones de sus cosas más ricas.<br>Avanzada la tarde las novias asistieron a la conferencia de Martin Roig. Fue una oportunidad para escuchar tips y consejos súper útiles que hacen a la organización del casamiento'
                    ),
                array(
                    'titulo'        => 'Jornada 18 - <span class="green">28 de Marzo, 2006 Centro Cultural Borges, Buenos Aires</span>',
                    'texto'         => 'El 28 de Marzo se realizó la XVIII Jornada para Organizar tu Casamiento.<br>Un único y exclusivo evento para las novias de CasamientosOnline. Una tarde llena de Glamour con la participación de los principales proveedores del mercado de los casamientos, una conferencia espectacular a cargo de Martin Roig, Benito Fernandez y Grupo Sarapura y una enriquecedora clase de Make Up por Poly Martinez.<br>No te pierdas la cobertura y galería de fotos!',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 164))
                    )
            ),
            '2005' => array(
                array(
                    'titulo'        => 'Jornada 17 - <span class="green">22 de agosto 2005, Marriott Plaza Hotel, Buenos Aires</span>',
                    'texto'         => 'La XVII Jornada Para organizar Tu Casamiento fue un éxito!<br>En esta ocasión asistieron 900 novias, quienes pudieron recorrer los espacios de más de 50 proveedores del mercado. Los asistentes fueron recibidos con un regalo de NIVEA y a lo largo de todo del evento pudieron degustar vinos y champagne Finca La Linda de Luigi Bosca y refrescarse con agua Ser Saborizada.<br>La conferencia a cargo de Martin Roig, sorprendió y divirtió como siempre. Compartió con las novias tips y consejos para que su casamiento sea el que siempre se imaginaron.<br>En esta oportunidad acompañó a Martin, el disc jockey Pedro Sarapura del Grupo Sarapura y una invitada muy especial, Florencia Peña, quien les contó a todos cómo organiza su casamiento.<br>Al retirarse, las novias recibieron un último regalo: una remera con la inscripción “Bride” para que puedan lucirla en este momento tan especial de sus vidas.',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 163))
                    ),
                array(
                    'titulo'        => 'Jornada 16 - <span class="green">14 de Marzo 2005, Marriott Plaza Hotel</span>',
                    'texto'         => 'Una nueva edición de nuestras clásicas Jornadas para Organizar tu Casamiento.<br>Como siempre, Martín Roig desplegó todos sus conocimientos ante más de 800 personas quienes, atentas, prestaron atención a los detalles a tener en cuenta a la hora de organizar un casamiento. Junto a Martín, el experto en vestidos Benito Fernandez y el Dj Poppy Manzanedo, charlaron acerca de sus metiers, contaron anéctodas, y entre tips y consejos claves dieron cierre a una charla inolvidable para las novias.<br>En el evento participaron más de 30 empresas del mercado de los casamientos, quienes dieron detalles de sus servicios, brindaron presupuestos y asesoraron a las novias sobre los diferentes rubros a contratar.<br>Entre los presentes estuvieron Luigi Bosca, Bacardi y CocaCola, con barras de degustación de sus producto',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 162))
                    )
            ),
            '2004' => array(
                array(
                    'titulo'        => 'Jornada 15 - <span class="green">1 de Junio 2004, Hotel Four Seassons, Buenos Aires</span>',
                    'texto'         => 'Participante: Laura Valenzuela<br>Las novias fueron recibidas en la entrada de La Mansión, sin dudas, la casa más linda de nuestra Buenos Aires. Allí, pudieron ver distintos tipos de ambientaciones y recorrer cada uno de los salones. Luego, se encontraron con más de 40 vestidos y proveedores de todos los rubros.<br>Como siempre, Martín Roig, tuvo a las novias atentísimas a su charla durante más de una hora. Los temas? La ambientación, el timing, los tipos de fiestas y los must de los casamientos.<br>Antes de la partida, Laura Valenzuela presentó algunos de sus modelos y habló acerca su forma de crear los vestidos más originales del momento.',
                    'galeria'       => $this->media_model->get_galeria(array('id_galeria' => 160))
                    )
            ),
            '2003' => array(
                array(
                    'titulo'        => 'Jornada 14 - <span class="green">11 de noviembre 2003, Hotel Marriott Plaza, Buenos Aires</span>',
                    'texto'         => 'Participante: Martín Roig<br>Nuevamente el Marriott Plaza abrió sus puertas para recibir a las novias que están organizando su casamiento.<br>Martín Roig dio una charla super interesante a las asistentes. Las novias tomaron nota atentamente a todos los tips, Martín les contó todo lo que necesitan saber para que un casamiento sea el que siempre imaginaron.'
                    ),
                array(
                    'titulo'        => 'Jornada 13 - <span class="green">10 de Agosto del 2003, Hotel Four Seasons, Buenos Aires</span>',
                    'texto'         => 'Participante: Inés Duggan<br>Una jornada inolvidable, envuelta de glamour. Inés Duggan vino acompañada de novias, madrinas y cortejos con el vestuario que usaron para el gran día. Trajo diseños para todos los gustos, modelos para todas las figuras, además en una charla intima con Martín Roig, habló de los secretos del éxito de su carrera.<br>El mejor momento del evento? La participación de Belèn y Verónica Mackinlay, Delfina Oliver y Cristian Bruno. Le pusieron voz y talento a los temas de los musicales más famosos de Broadway.<br>Antes de la partida, el Hotel Four Seassons nos despidió con lo mejor de su cocina, un cocktail espectacular acompañado por los vinos y champagnes de la Bodega Navarro Correas.'
                    ),
                array(
                    'titulo'        => 'Jornada 12 - <span class="green">11 de Junio 2003, Hotel Caesar Park Buenos Aires</span>',
                    'texto'         => 'Participante: María Mourín<br>Nos acompañaron más de 400 novias. El Hotel, completo de los principales proveedores de los diferentes rubros del casamiento, abrió las puertas de todos sus salones para mostrar su elegante decoración, la paquetería de sus mesas preparadas para casamientos y la simpatía de todo su personal.<br>Sin dudas, en este evento, todos los aplausos fueron para Martín Roig, que dio una charla im-per-di-ble acerca de todos los detalles y el paso a paso para Organizar un Casamiento.<br>María Mourín, joven diseñadora, dio una charla interesantísima acerca de las tendencias de los vestidos de novia y mostró lo mejor de su colección.<br>Llegando el final, maquilladores y peluqueros hicieron diferentes demostraciones de make up y peinados para llegar al altar.'
                    ),
                array(
                    'titulo'        => 'Jornada 11 - <span class="green">28 de Abril 2003, Marriott Plaza Hotel</span>',
                    'texto'         => 'Participante: Laurencio Adot<br>El primer encuentro del año fue ESPECTACULAR!<br>Vinieron más de 450 novias, participaron proveedores de todos los rubros con propuestas interesantísimas. Martín Roig dio una charla completísima.<br>Juan Diego Martínez Larrea estuvo a cargo de la musicalización y como siempre el Marriott Plaza Hotel nos trató como a los mejores invitados.<br>Los vestidos? Fueron de Laurencio Adot. Trajo unas propuestas super originales con colores y detalles en piedras. Lo más lindo del evento? El cortejo! Silvia Pavlosky vistió a seis chiquitas...'
                    ),
            ),
            '2002' => array(
                array(
                    'titulo'        => 'Jornada 10 - <span class="green">19 de Diciembre 2002, Hotel Intercontinental</span>',
                    'texto'         => 'Este encuentro fue super TOP, vinieron Grisel Perez Ponce y Jazmín de Gracia, las ganadoras de Super M, a presentar diez vestidos espectaculares.<br>El Hotel Intercontinental vistió a sus vendedoras de novias y a sus vendedores de novios y llevaron a las invitadas a recorrer todos los salones y a conocer las habitaciones preparadas para la Noche de Bodas.<br>La gente de Cáritas organizó una colecta mediante la cual diez chicos de un comedor de Rincón de Milberg pudieron conocer el mar...A todos los que colaboraron, les damos las Gracias!'
                    ),
                array(
                    'titulo'        => 'Jornada 9 - <span class="green">4 de Junio 2002, Hotel Intercontinental</span>',
                    'texto'         => 'Casamientosonline.com no se mantiene al margen de la crisis por la cual atraviesa nuestro país. Siendo nuestro objetivo acercarles a las novias las mejores oportunidades que ofrece el mercado de casamientos, esta vez invitamos a nuestro evento al staff de Novias al Garage, con una propuesta íntegra para las novias y con precios muy accesibles. Presentaron en el evento vestidos usados de grandes diseñadores totalmente reciclados a nuevo.<br>Los tocados fueron de Susy Braithwaite y el maquillaje de Mercedes Belaustegui.<br>Nuestro toque de distinción? Como siempre recibir a las novias de la mejor manera. Además de la charla, los consejos de Martín Roig y la participación de todos lo proveedores, contamos con un cocktail espectacular a cargo de Sancor, Navarro Correas y Quilmes Fiesta.'
                    ),
                array(
                    'titulo'        => 'Jornada 8 - <span class="green">6 de Marzo 2002, Hotel Intercontinental</span>',
                    'texto'         => 'La VIII Jornada para Organizar tu Casamiento se realizó en el salón Monserrat del hotel Intercontinental.<br>El encuentro fue un éxito!<br>Martín Roig contó de todo!<br>Tito Scuticchio estuvo a cargo de la musicalización e iluminación y contó todos los detalles de uno de los puntos más importantes de la fista: la música!<br>En vestidos estuvo la talentosa Inés Duggan, trajo muchísimos vestidos, y todo su conocimiento en géneros, tendencias, estilos y modelos para cada tipo de fiesta.<br>En Make Up y Peinado, Anita Philips y Joaquín Persson... hicieron tres maquillajes y peinados distintos, además contaron cómo trabajan, cuáles son sus recomendaciones, cómo se tienen que preparar las novias y qué es lo que se viene.<br>'
                    )
            ),
            '2001' => array(
                array(
                    'titulo'        => 'Jornada 7 - <span class="green">19 de Octubre 2001, Marriott Plaza Hotel</span>',
                    'texto'         => 'El Salón de Fiestas del Marriott Plaza Hotel estuvo colmado de cientos de novias que atentas escucharon la charla de Benito Fernández.<br>Esta vez, además de las producciones de maquillaje por Revlon y los peinados de Joaquín Pearson, participó Alejandro Massey con todos los tips para elegir la mejor música.<br>Los infaltables!<br>Navarro Correas con sus blancos y tintos. El espectacular cocktail con sus quesos y delis de Sancor y como siempre los espectaculares sorteos.'
                    ),
                array(
                    'titulo'        => 'Jornada 6 - <span class="green">25 de Julio 2001 Hotel Alvear</span>',
                    'texto'         => 'En uno de los Hoteles más exclusivos de Buenos Aires organizamos nuestro ya clásico evento para novias. La "VI Jornada para Organizar tu Casamiento" fue un éxito.<br>Laurencio Adot, contó todos sus secretos y trajo tres vestidos divinos!<br>Como siempre el maquillaje y el peinado estuvieron a cargo de los TOPS! Sebastián correa trajo las tendencias en maquillajes, colores y estilos y Joaquín Pearson nos mostró dos peinados con diferentes accesorios y nos sorprendió con sus soluciones para todos los tipos de pelo!<br>Cuáles fueron las sorpresas?<br>Una espectacular comida servida por los clásicos Chefs del Hotel, acompañada por los vinos y el champagne de Navarro Correas.<br>Más de 15 espectaculares sorteos.<br>Un show con los mejores cantantes de comedia musical. Vimos en vivo un número de Evita y uno del Fantasma de la Opera.'
                    ),
                array(
                    'titulo'        => 'Jornada 5 - <span class="green">21 de Mayo 2001, Marriott Plaza Hotel</span>',
                    'texto'         => 'Nuestra V Jornada para Organizar tu Casamiento se realizó en el Salón de Fiestas del Marriott Plaza Hotel.'
                    ),
                array(
                    'titulo'        => 'Jornada 4 - <span class="green">28 de Marzo 2001, Hotel Emperador</span>',
                    'texto'         => 'La VIII Jornada para Organizar tu Casamiento se realizó en el salón Monserrat del hotel Intercontinental.<br>El encuentro fue un éxito!<br>Martín Roig contó de todo!<br>Tito Scuticchio estuvo a cargo de la musicalización e iluminación y contó todos los detalles de uno de los puntos más importantes de la fista: la música!<br>En vestidos estuvo la talentosa Inés Duggan, trajo muchísimos vestidos, y todo su conocimiento en géneros, tendencias, estilos y modelos para cada tipo de fiesta.<br>En Make Up y Peinado, Anita Philips y Joaquín Persson... hicieron tres maquillajes y peinados distintos, además contaron cómo trabajan, cuáles son sus recomendaciones, cómo se tienen que preparar las novias y qué es lo que se viene.<br>'
                    ),
                array(
                    'titulo'        => 'Jornada 3 - <span class="green">8 de Marzo 2001, Hotel Hilton</span>',
                    'texto'         => 'La VIII Jornada para Organizar tu Casamiento se realizó en el salón Monserrat del hotel Intercontinental.<br>El encuentro fue un éxito!<br>Martín Roig contó de todo!<br>Tito Scuticchio estuvo a cargo de la musicalización e iluminación y contó todos los detalles de uno de los puntos más importantes de la fista: la música!<br>En vestidos estuvo la talentosa Inés Duggan, trajo muchísimos vestidos, y todo su conocimiento en géneros, tendencias, estilos y modelos para cada tipo de fiesta.<br>En Make Up y Peinado, Anita Philips y Joaquín Persson... hicieron tres maquillajes y peinados distintos, además contaron cómo trabajan, cuáles son sus recomendaciones, cómo se tienen que preparar las novias y qué es lo que se viene.<br>'
                    ),
            ),
            '2000' => array(
                array(
                    'titulo'        => 'Jornada 2 - <span class="green">19 de Diciembre 2000, Marriott Plaza Hotel</span>',
                    'texto'         => 'Estuvieron Revlon, con su maquillador exclusivo, Caro Aubele y Joaquín de Staff Cerini y Martín Roig condujo el evento.'
                    ),
                array(
                    'titulo'        => 'Jornada 1 - <span class="green">Marzo de 2000, Hotel Sheraton</span>',
                    'texto'         => 'Presentaron sus Diseños: Benito Fernández, Laurencio Adot, Inés Ricur, Carolina Aubele, Cocó de Eugenia, Maureene Dinar, y Marcelo Senra.<br>Entre las modelos estuvieron: Andrea Burstein, Lorena Ceriscioli, Ana Paula Dutil y Dolores Trull.'
                    )
            ),
        );

        foreach ($content[$anio] as $jornada){
            break;
        }

        $combo_rubros = $this->navegacion_model->get_rubros();

        $data = array(
            'anio'                  => $anio,
            'data_form'             => $data_form,
            'canonical'             => base_url($_SERVER['REQUEST_URI']),
            'combo_rubros'          => $combo_rubros,
            'content'               => $content[$anio],
            'id_evento'             => $id_evento,
            'id_evento_empresa'     => $id_evento_empresa,
            'id_seccion'            => $id_seccion,
            'localidades'           => $localidades,
            'provincias'            => $provincias,
            'meta_descripcion'      => 'Fotos y videos de nuestras ExpoNovias anteriores: reviví el evento más grande para las novias organizadoras. Se parte de nuestra próxima edición.',
            'meta_keywords'         => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, Jornadas, anteriores, ' . $anio,
            'meta_title'            => 'Exponovias ' . $anio . ' - El mayor evento del año | Casamientos Online',
            'twitter_title'         => strip_tags($jornada['titulo']),
            'twitter_descripcion'   => strip_tags($jornada['texto'])
        );

        $this->load->view('jornadas_anteriores', $this->config_library->send_data_to_view($data));
    }

    public function te_de_novias($id_galeria = 292, $id_sucursal = 1){ // 4 CORDOBA // 2 ROSARIO
        $this->load->model('banners_model');
        $this->load->model('media_model');
        $this->load->model('navegacion_model');

        if(!in_array($id_sucursal, array(1,2,4)) || !is_numeric($id_galeria)) show_404();

        $success = FALSE;
        if($_POST){
            $this->load->library('form_validation');
            
            $error_required = array(
                'required' => 'Debe ingresar un %s.',
            );

            $error_required_hidden = array(
                'required' => 'Su formulario contiene errores'
            );
            
            $this->form_validation->set_rules('id_origen', 'id_origen', 'required', $error_required_hidden);
            $this->form_validation->set_rules('id_evento', 'id_evento', 'required', $error_required_hidden);
            $this->form_validation->set_rules('tipo', 'tipo', 'required', $error_required_hidden);
            
            $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
            $this->form_validation->set_rules('apellido', 'Apellido', 'required', $error_required);
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
                    'required'      => 'Debe ingresar un %s.',
                    'valid_email'   => 'Debe ingresar un %s válido.',
            ));
            $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
            $this->form_validation->set_rules('dni', 'DNI', 'required', $error_required);
            $this->form_validation->set_rules('id_provincia', 'Provincia', 'required', $error_required);
            $this->form_validation->set_rules('id_localidad', 'Localidad', 'required', $error_required);
            $this->form_validation->set_rules('invitados', 'Invitados', 'required', $error_required);
            $this->form_validation->set_rules('dia_evento', 'Fecha casamiento', 'required', $error_required);

            if($this->form_validation->run() == TRUE){ // success
                $this->load->library('procesos_library');

                $data = array(
                    'dia_evento'        => $_POST['dia_evento'],
                    'invitados'         => $_POST['invitados'],
                    'infonovias'        => 1,
                    'promociones'       => 1,
                    'id_referencia'     => 48,
                    'id_origen_contacto'=> 11,
                    'id_origen'         => 11,
                    'id_evento'         => $_POST['id_evento'],
                    'tipo'              => $_POST['tipo'],
                    'nombre'            => $_POST['nombre'],
                    'apellido'          => $_POST['apellido'],
                    'email'             => $_POST['email'],
                    'telefono'          => $_POST['telefono'],
                    'dni'               => $_POST['dni'],
                    'id_provincia'      => $_POST['id_provincia'],
                    'id_localidad'      => $_POST['id_localidad'],
                    'comentario'        => $_POST['comentario']
                );

                $this->procesos_library->procesar_form_novio($data);

                $success = TRUE;
            }
        }

        $banners = $this->banners_model->views(119);

        $provincias = $this->navegacion_model->get_provincias(0);
        $localidades = NULL;

        if(!empty($_POST['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($_POST['id_provincia']);
        }

        // $id_evento = $this->navegacion_model->get_id_evento(2);

        switch ($id_sucursal) {
            case '1': // BUENOS AIRES
                $id_evento = 147;
                $meta_descripcion = '¿Tu casamiento es en Buenos Aires? Vení a tomar el Té y a divertirte en un evento con panelistas llenos de tips y consejos para tí. Reservá tu entrada.';
                $title = 'Atención Buenos Aires! Vení a nuestro Té de Novias! | Casamientos Online';
                break;
            case '2': // ROSARIO
                $id_evento = 145;
                $meta_descripcion = '¿Tu casamiento es en Rosario? Vení a tomar el Té y a divertirte en un evento con panelistas llenos de tips y consejos para tí. Reservá tu entrada.';
                $title = 'Atención Rosario! Vení a nuestro Té de Novias! | Casamientos Online';
                break;
            case '4': // CORDOBA
                $id_evento = 146;
                $meta_descripcion = '¿Tu casamiento es en Córdoba? Vení a tomar el Té y a divertirte en un evento con panelistas llenos de tips y consejos para tí. Reservá tu entrada.';
                $title = 'Atención Córdoba! Vení a nuestro Té de Novias! | Casamientos Online';
                break;
        }

        $data = array(
            'banners'               => $banners,
            'canonical'             => base_url($_SERVER['REQUEST_URI']),
            'galeria'               => $this->media_model->get_galeria(array('id_galeria' => $id_galeria)),
            'id_evento'             => $id_evento,
            'id_sucursal_els'       => $id_sucursal,
            'meta_descripcion'      => $meta_descripcion,
            'meta_keywords'         => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, registro, t&eacute;, novias',
            'meta_title'            => $title,
            'provincias'            => $provincias,
            'localidades'           => $localidades,
            'success'               => $success,
            'twitter_descripcion'   => 'Vení a disfrutar de un Té de Novias único y exclusivo para Novias que están organizando su Casamiento en la Ciudad de Rosario! Te esperamos en el salón del PUERTO NORTE EVENTOS con una propuesta gastronómica dulce, salada y riquísimos tragos. Expertos de diferentes rubros nos darán consejos, tips y nos hablarán de las últimas tendencias en Casamientos. Animate a preguntarles todas tus dudas. Vas a poder compartir tu experiencia y charlar con otras novias, recorrer stands de proveedores y una increíble Galería de Vestidos de Novia.',
            'twitter_image'         => base_url('/assets/images/header_te_novias.jpg')
        );

        $this->load->view('te_de_novias', $this->config_library->send_data_to_view($data));
    }
}