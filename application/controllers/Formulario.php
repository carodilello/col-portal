<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulario extends MY_Controller {
	public function __construct(){
    	parent::__construct();
	}

    public function index($area = NULL, $tipo = NULL, $id_rubro = NULL, $id_origen = NULL, $id_item = NULL, $tipo_multiple = ''){
        if(!$area || !$tipo || !$id_rubro) redirect(base_url('/home'));

        $id_origen = $id_origen?$id_origen:'0';
        if($tipo == 'empresa') $id_minisitio = $id_item;
        if($tipo == 'producto' || $tipo == 'promocion' || $tipo == 'paquete') $id_producto = $id_item;
        if($tipo == 'foto') $id_foto = $id_item;

        $this->load->library('config_library');

        $pos = strpos(urldecode($_SERVER['REQUEST_URI']), '|');
        $filtros = ($pos === false) ? '' : substr(urldecode($_SERVER['REQUEST_URI']), $pos); // '|36-4358|17-10';

        $this->load->model('proveedores_model');
        $this->load->model('productos_model');
        $this->load->model('rubros_model');
        $this->load->model('navegacion_model');
        $this->load->model('usuarios_model');

        $this->load->library('parseo_library');

        $this->load->helper('cookie');

        $es_grupal = 'no';
        switch ($area){
            case 'presupuesto':

                $meta_keywords = '';
                $foto = '';
                $rubro = $this->rubros_model->get_rubro($id_rubro);
                $rubro = $rubro[0];
                $rubro['url'] = $this->parseo_library->clean_url($rubro['rubro']);
                $canonical = explode('_t', $_SERVER['REQUEST_URI']);
                if(!empty($canonical[0])) $canonical = base_url($canonical[0] . '_t11');

                switch ($tipo){
                    case 'resultados':

                        //Agregado Roman 10-05-2017. para que cuando es presupuesto grupal vuelva a la lista y no al minisitio
                        $es_grupal = 'si';
                        $por_zona = array(17, 30);

                        if (!$rubro) {
                            show_404();
                        }

                        //si no hay filtros y es un rubro por zonas
                        if (empty($filtros) && in_array($id_rubro, $por_zona)){
                            $opciones = $this->proveedores_model->get_presupuesto_filtro($id_rubro, 115);
                            $meta_keywords .= ', Zonas';
                            foreach ($opciones as $opc) {
                                $meta_keywords .= ', ' . $opc['opcion'];
                            }
                        } else {
                            $solo_mapa = (!empty($id_origen) && $id_origen == 24) ? 2 : 0;
                            $proveedores = $this->proveedores_model->get_proveedores($id_rubro, 'nivel', 0, 1, $filtros, $solo_mapa);

                            foreach ($proveedores as $pv) {
                                $meta_keywords .= ', ' . $pv['proveedor'];
                            }

                            $filtros_aplicados = $this->config_library->get_filtros_aplicados($filtros, 1, $id_rubro);
                            $empresas_visibles = 12;
                        }

                        // $referer = '/planea-tu-casamiento/' . $rubro['id_sucursal'] . '/' . $this->parseo_library->clean_url($rubro['sucursal']) . '/guia-de-servicios/' . $id_rubro . '/' . $this->parseo_library->clean_url($rubro['rubro']) . '/1';

                        $referer = '/proveedores/listado_empresas/'.$id_rubro;
                        $referer = $this->config_library->sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_r' . $id_rubro;
                        $id_sucursal = $rubro['id_sucursal'];

                        $meta_descripcion = '¿Necesitás presupuestos de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . '? Solicitalo aquí fácil, rápido y sin compromiso de compra!';
                        switch ($tipo_multiple) {
                            case 'promocion':
                                $meta_title = 'Presupuesto de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . ' | Casamientos Online';
                                break;
                            case 'producto':
                                $meta_title = 'Presupuesto de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . ' | Casamientos Online';
                                break;
                            case 'paquete':
                                $meta_title = 'Presupuesto de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . ' | Casamientos Online';
                                break;
                            case 'foto':
                                $meta_title = 'Presupuesto de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . ' | Casamientos Online';
                                break;
                            case 'mapa':
                                $meta_title = 'Presupuesto de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . ' | Casamientos Online';
                                break;
                            default:
                                $meta_title = 'Solicitud de Presupuestos Múltiples de ' . $rubro['rubro'] . ' | Casamientos Online';
                                break;
                        }
                    break;

                    case 'empresa':
                        $empresa = $this->proveedores_model->get_proveedor($id_minisitio);
                        /*06062017 le agregue un 1 como parametro al get_proveedor_asociados
                        para que tome solamente los recibe = 1*/ 
                        $asociados_trabaja = $this->proveedores_model->   get_proveedor_asociados_ms($id_minisitio,1);
                        /*
                        $asociados_trabaja = $this->proveedores_model->get_proveedor_asociados_ms($id_minisitio);
                        */
                        $asociados_trabaja = $this->parseo_library->asociados_trabaja($asociados_trabaja);

                        if (!$empresa) { //-- En caso que el Minisitio este vencido --//
                            $empresa = $this->proveedores_model->get_proveedor_vencido($id_minisitio);

                            if (!$empresa){
                                show_404();
                            }else{
                                $cantProdInfoNovia = $this->productos_model->tiene_producto_activo($id_minisitio, 4);

                                if ($cantProdInfoNovia > 0 && $tipo == 'infonovia') {
                                    //-- Tiene MS vencido pero tiene Infonovias Activo! --//
                                }else{
                                    redirect(base_url('/'));
                                }
                            }
                        }

                        $tmp = explode('.', $_SERVER['HTTP_HOST']);
                        if($tmp[0] != strtolower($empresa['subdominio'])) redirect('http://' .  $empresa['subdominio'] . '.' . DOMAIN . '/' . $empresa['seo_url'] . '/presupuesto-empresa_CO_r' . $empresa["id_rubro"] . '_m' . $empresa["id_minisitio"]);



                        $proveedores = $this->proveedores_model->get_proveedor_asociados($id_minisitio, -1, 1);

                        $meta_keywords .= ', ' . $empresa['proveedor'];

                        if($proveedores) foreach ($proveedores as $pv) {
                            $meta_keywords .= ', ' . $pv['proveedor'];
                        }
                        $hidden[] = array('name' => 'id_minisitio', 'value' => $id_minisitio);

                        // $referer = '/planea-tu-casamiento/' . $empresa['id_sucursal'] . '/' . $comercial->clean_url_3($empresa['sucursal']) . '/guia-de-servicios/' . $empresa['id_rubro'] . '/' . $comercial->clean_url_3($empresa['rubro']) . '/' . $empresa['id_minisitio'] . '/' . $comercial->clean_url_3($empresa['proveedor']) . '/';

                        $id_rubro = $empresa['id_rubro'];
                        $id_sucursal = $rubro['id_sucursal'];

                        if($_SERVER['HTTP_HOST'] == BASE){ // Sin subdominio
                            $meta_descripcion = '¿Necesitás presupuestos de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . '? Solicitalo aquí fácil, rápido y sin compromiso de compra!';
                            $meta_title = 'Solicitud de Presupuestos Múltiples de ' . $rubro['rubro'] . ' | Casamientos Online';
                        }else{ // Con subdominio
                            $meta_descripcion = 'Pedile aquí tu presupuesto de ' . $rubro['rubro'] . ' a ' . $empresa['subdominio'] . '. Organizar tu casamiento nunca fue tan fácil!';
                            $meta_title = 'Presupuesto de ' . $rubro['rubro'] . ' en ' . $empresa['subdominio'] . ' | Casamientos Online';
                        }
                    break;

                    case 'producto': case 'promocion':
                        $producto = $this->productos_model->get_producto($id_producto);

                        if (!$producto) { //-- En caso que el producto/promo este vencido
                            $producto = $this->productos_model->get_producto_vencido($id_producto);

                            if (!$producto || empty($producto['titulo'])) {
                                show_404();
                            }

                            redirect(base_url('/'));
                        }

                        $codigo_tipo = 'pd';
                        if($producto['promocion']) $codigo_tipo = 'pr';
                        $tmp = explode('.', $_SERVER['HTTP_HOST']);
                        if($tmp[0] != $producto['subdominio']) redirect('http://' .  $producto['subdominio'] . '.' . DOMAIN . '/' . $producto['rubro_seo'] . '/presupuesto-producto_CO_r' . $producto['id_rubro'] . '_' . $codigo_tipo . $producto['id_producto']);

                        $empresa = $this->proveedores_model->get_proveedor($producto['id_minisitio']);
                        $meta_keywords .= ', ' . $empresa['proveedor'];

                        $hidden[] = array('name' => 'id_item', 'value' => $id_producto);
                        $hidden[] = array('name' => 'id_minisitio', 'value' => $producto['id_minisitio']);

                        $id_rubro = $producto['id_rubro'];

                        if($_SERVER['HTTP_HOST'] == BASE){ // Sin subdominio
                            $meta_descripcion = '¿Necesitás presupuestos de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . '? Solicitalo aquí fácil, rápido y sin compromiso de compra!';
                            $meta_title = 'Presupuestos de ' . $rubro['rubro'] . ' para ' . $rubro['sucursal'] . ' | Casamientos Online';
                        }else{ // Con subdominio
                            $meta_descripcion = 'Pedile aquí tu presupuesto de ' . $rubro['rubro'] . ' a '. $producto['subdominio'] . '. Solicitalo fácil y rápido.';
                            $meta_title = $producto['subdominio'] . ': Pedí tu presupuesto de ' . $rubro['rubro'] . ' en Casamientos Online';
                        }
                    break;

                    case 'paquete':
                        $this->load->model('paquetes_model');

                        $producto = $this->paquetes_model->get_paquete($id_producto);

                        if (!$producto || empty($producto['id_proveedor'])) { //-- En caso que elpaquete este vencido
                            $producto = $this->paquetes_model->get_paquete_vencido($id_producto);

                            if (!$producto || empty($producto['titulo'])) {
                                show_404();
                            }

                            if($producto['id_minisitio']){
                                redirect(base_url('proveedores/minisitio/' . $producto['id_minisitio']));
                            }else{
                                show_404();   
                            }
                        }

                        $empresa = $this->proveedores_model->get_proveedor($producto['id_minisitio']);
                        $meta_keywords .= ', ' . $empresa['proveedor'];

                        $hidden[] = array('name' => 'id_item', 'value' => $id_producto);
                        $hidden[] = array('name' => 'id_minisitio', 'value' => $producto['id_minisitio']);
                        $id_rubro = $producto['id_rubro'];

                        if($_SERVER['HTTP_HOST'] == BASE){ // Sin subdominio
                            $meta_descripcion = '¿Necesitás presupuestos de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . '? Solicitalo aquí fácil, rápido y sin compromiso de compra!';
                            $meta_title = 'Presupuestos de ' . $rubro['rubro'] . ' para ' . $rubro['sucursal'] . ' | Casamientos Online';
                        }else{ // Con subdominio
                            $meta_descripcion = 'Pedí aquí tu presupuesto del paquete especial que ' . $producto['subdominio'] . ' diseñó especialmente para vos! Solicitalo ahora!';
                            $meta_title = 'Presupuesto de ' . $producto['titulo'] . ' | ' . $producto['subdominio'] . ' | Casamientos Online';
                        }
                    break;

                    case 'foto':
                        $foto = $this->proveedores_model->get_foto($id_foto);

                        if (!$foto) {
                            show_404();
                        }

                        $producto = $foto;
                        $empresa = $this->proveedores_model->get_proveedor($foto['id_minisitio']);
                        $meta_keywords .= ', ' . $empresa['proveedor'];

                        $hidden[] = array('name' => 'id_item', 'value' => $id_foto);
                        $hidden[] = array('name' => 'id_minisitio', 'value' => $foto['id_minisitio']);

                        $id_rubro = $foto['id_rubro'];

                        if($_SERVER['HTTP_HOST'] == BASE){ // Sin subdominio
                            $meta_descripcion = '¿Necesitás presupuestos de ' . $rubro['rubro'] . ' en ' . $rubro['sucursal'] . '? Solicitalo aquí fácil, rápido y sin compromiso de compra!';
                            $meta_title = 'Presupuestos de ' . $rubro['rubro'] . ' para ' . $rubro['sucursal'] . ' | Casamientos Online';
                        }else{ // Con subdominio
                            $meta_descripcion = 'Pedile aquí tu presupuesto de ' . $rubro['rubro'] . ' a '. $foto['subdominio'] . '. Solicitalo fácil y rápido.';
                            $meta_title = $foto['subdominio'] . ': Pedí tu presupuesto de ' . $rubro['rubro'] . ' en Casamientos Online';
                        }
                    break;
                }
            break;
        }

        $item_title = '';
        if($tipo == 'producto' || $tipo == 'paquete'){
            $item_title = ' para el ' . $tipo . ' ' . $producto['titulo'];
        }

        if($tipo == 'promocion'){
            $item_title = ' para la promo ' . $producto['titulo'];
        }

        if($tipo == 'foto'){
            $item_title = ' para la foto ' . $foto['titulo'];
        }

        if($tipo == 'video'){
            $item_title = ' para el video ' . $video['titulo'];
        }

        // $referer = (substr($_SERVER['HTTP_REFERER'], 0, 33) == 'http://www.casamientosonline.com') ? $_SERVER['HTTP_REFERER'] : $referer;
        $referer = isset($referer) && $referer ? $referer : $_SERVER['REQUEST_URI'];

        $hidden[] = array('name' => 'id_rubro', 'value' => $id_rubro);
        $hidden[] = array('name' => 'id_tipo_evento', 'value' => 1);

        //$hidden[] = array('name' => 'id_user', 'value' => $_SESSION['id_user']);
        $hidden[] = array('name' => 'id_user', 'value' => NULL); // PARA ANALIZAR LUEGO POR EL TEMA DE LAS SESIONES

        // $hidden[] = array('name' => 'id_landing', 'value' => ($_SESSION['id_landing']) ? $_SESSION['id_landing'] : 0);
        $hidden[] = array('name' => 'id_landing', 'value' => 0); // PARA ANALIZAR LUEGO POR EL TEMA DE LAS SESIONES

        $hidden[] = array('name' => 'id_origen', 'value' => $id_origen);
        $hidden[] = array('name' => 'referer', 'value' => $referer);


        if (!empty($_SESSION['id_user'])) {
            $data_form = $this->usuarios_model->get_novio_info($_SESSION['id_user']);
        }else{
            $data_form = $this->parseo_library->asignar_mayoria_campos_formulario();
        }

        if (!isset($data_form['id_pais']) || !$data_form['id_pais']) $data_form['id_pais'] = 10; // 10 = argentina

        $data_form['verificador_captcha'] = get_cookie('verificador_captcha');

        $paises         = $this->navegacion_model->get_paises();
        $provincias     = $this->navegacion_model->get_provincias();
        $fecha_variable = $this->rubros_model->opcion_fecha_variable($id_rubro);

        if(isset($data_form['id_provincia']) && $data_form['id_provincia']){
            $localidades = $this->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        if(isset($empresa) && $empresa){
            $id_seccion = $this->navegacion_model->get_seccion($empresa['id_rubro']);
        }

        //-- DATOS DE VIAJE para los rubros --//
        if(!empty($id_minisitio)){ //-- pedidos de presupuestos individuales --//
            $proveedor = $this->proveedores_model->get_proveedor($id_minisitio);
        }else{//-- pedido de presupuestos grupales --//
            if(isset($producto['id_minisitio']) && $producto['id_minisitio']) $proveedor = $this->proveedores_model->get_proveedor($producto['id_minisitio']);
            $proveedor['id_rubro'] = $id_rubro;
        }

        $datos_viaje   = $this->parseo_library->datos_viaje($proveedor);
        $salon_elegido = $this->parseo_library->salon_elegido($proveedor);

        $titulo = $this->parseo_library->titulo_pedido_presupuesto($area, $tipo, $rubro, (isset($empresa)&&$empresa?$empresa:''), isset($opciones)&&$opciones?$opciones:'', (isset($filtros_aplicados)&&$filtros_aplicados?$filtros_aplicados:''));

        if(empty($canonical)) $canonical = base_url($_SERVER['REQUEST_URI']);

        $data_view = array(
            'area'              => $area,
            'canonical'         => $canonical,
            'rubro'             => $rubro,
            'opciones'          => isset($opciones)&&$opciones?$opciones:'',
            'proveedores'       => isset($proveedores)&&$proveedores?$proveedores:'',
            'proveedor'         => $proveedor,
            'asociados'         => isset($proveedores)&&$proveedores?$proveedores:'',
            'asociados_trabaja' => $asociados_trabaja,
            'filtros_aplicados' => isset($filtros_aplicados)&&$filtros_aplicados?$filtros_aplicados:'',
            'empresas_visibles' => isset($empresas_visibles)&&$empresas_visibles?$empresas_visibles:'',
            'meta_descripcion'  => $meta_descripcion,
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, ' . $rubro['sucursal'] . ', Argentina, pedi presupuesto, consulta, cotizacion, ' . $rubro['rubro'] . $meta_keywords,
            'meta_title'        => $meta_title,
            'hidden'            => $hidden,
            'data_form'         => $data_form,
            'tipo'              => $tipo,
            'paises'            => $paises,
            'provincias'        => $provincias,
            'fecha_variable'    => $fecha_variable,
            'localidades'       => isset($localidades)&&$localidades?$localidades:'',
            'id_seccion'        => isset($id_seccion)&&$id_seccion?$id_seccion:'',
            'titulo'            => $titulo,
            'datos_viaje'       => $datos_viaje,
            'salon_elegido'     => $salon_elegido,
            'id_rubro'          => $id_rubro,
            'id_origen'         => $id_origen,
            'config'            => array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
            'er_fecha'          => $this->config_library->er_fecha,
            'empresa'           => isset($empresa)&&$empresa?$empresa:'',
            'foto'              => $foto,
            'id_minisitio'      => isset($id_minisitio)&&$id_minisitio?$id_minisitio:'',
            'producto'          => isset($producto)&&$producto?$producto:'',
            'ubicacion'         => 'presupuesto',
            'es_grupal'         => $es_grupal
        );


        $this->load->view('presupuesto', $this->config_library->basic_data_to_view($data_view));
    }

	public function procesar_pedido(){
        $post = !empty($_POST) ? $_POST : array();

        $es_grupal = $post['es_grupal'];
        unset($post['es_grupal']);

        /* -- VALIDACION DE CAMPOS OBLIGATORIOS */
        $required_fields = array(
            'nombre',
            'apellido',
            'dia_evento',
            'email'
        );

        if(!empty($post['id_origen'])){
            if($post['id_origen'] != '46' && $post['id_origen'] != '47')  $required_fields[] = 'telefono';

            if($post['id_origen'] != '45' && $post['id_origen'] != '51' && $post['id_origen'] != '46' && $post['id_origen'] != '47'){

                $required_fields[] = 'id_provincia';
                $required_fields[] = 'id_localidad';
            }
        }

        $error_required_fields = FALSE;
        
        if(!empty($post)){
            foreach ($required_fields as $required_field) {
                if(!array_key_exists($required_field, $post)) $error_required_fields = TRUE;
            }    
        }else{
            $error_required_fields = TRUE;
        }
        /* -- FIN VALIDACION CAMPOS OBLIGATORIOS */



        if (!$error_required_fields){
            $this->load->library('procesos_library');
            $this->load->library('parseo_library');

            $this->load->helper('cookie');

            $dia_evento     = $post['dia_evento'];
            $dia_nacimiento = isset($post['dia_nacimiento'])?$post['dia_nacimiento']:'';

            if(isset($post['dia_evento']) && strpos($post['dia_evento'], '/')){
                $tmp = explode('/', $post['dia_evento']);
                $post['dia_evento'] = $tmp[0];
                $post['mes_evento'] = $tmp[1];
                $post['anio_evento'] = $tmp[2];
            }

            if(isset($post['dia_nacimiento']) && strpos($post['dia_nacimiento'], '/')){
                $tmp = explode('/', $post['dia_nacimiento']);
                $post['dia_nacimiento'] = $tmp[0];
                $post['mes_nacimiento'] = $tmp[1];
                $post['anio_nacimiento'] = $tmp[2];
            }

            $tunnel_proforma_facturacion = isset($post["tunnel_proforma_facturacion"])?$post["tunnel_proforma_facturacion"]:NULL;
            $from_espacioeventos = isset($post["from_espacioeventos"])?$post["from_espacioeventos"]:NULL;
            $from_produccion_modas = isset($post["from_produccion_modas"])?$post["from_produccion_modas"]:NULL;
            unset($post["from_espacioeventos"], $post["from_produccion_modas"],$post["tunnel_proforma_facturacion"]);
            $referer = $post['referer'];
            unset($post['referer']);
            $time = time() + 60 * 60 * 24 * 365;

            set_cookie("dia_evento"           , $dia_evento                                           , $time);
            set_cookie("ubicacion"            , isset($post['ubicacion'])?$post['ubicacion']:''       , $time);
            set_cookie("invitados"            , isset($post['invitados'])?$post['invitados']:''       , $time);
            set_cookie("nombre"               , $post['nombre']                                       , $time);
            set_cookie("apellido"             , $post['apellido']                                     , $time);
            set_cookie("dia_nacimiento"       , $dia_nacimiento                                       , $time);
            set_cookie("email"                , $post['email']                                        , $time);
            set_cookie("telefono"             , isset($post['telefono'])?$post['telefono']:''         , $time);
            set_cookie("id_provincia"         , isset($post['id_provincia'])?$post['id_provincia']:'' , $time);
            set_cookie("id_localidad"         , isset($post['id_localidad'])?$post['id_localidad']:'' , $time);

            set_cookie("fecha_evento_variable", isset($post["fecha_evento_variable"])?$post["fecha_evento_variable"]:NULL, $time);
            set_cookie("verificador_captcha"  , isset($post["verificador_captcha"])  ?$post["verificador_captcha"]  :NULL, $time);

            $session_data = array(
                "session_form" => array(
                    "dia_evento"      => $dia_evento,
                    "ubicacion"       => isset($post['ubicacion'])?$post['ubicacion']:'',
                    "invitados"       => isset($post['invitados'])?$post['invitados']:'',
                    "nombre"          => $post['nombre'],
                    "apellido"        => $post['apellido'],
                    "dia_nacimiento"  => $dia_nacimiento,
                    "email"           => $post['email'],
                    "telefono"        => isset($post['telefono'])?$post['telefono']:'',
                    "id_provincia"    => isset($post['id_provincia'])?$post['id_provincia']:'',
                    "id_localidad"    => isset($post['id_localidad'])?$post['id_localidad']:''
                )
            );

            $this->session->set_userdata($session_data);

            // para los check de las zonas minisitios[]
            if(isset($post['minisitios'])&&$post['minisitios']){
                $post['id_minisitio'] = explode(',', implode(',', $post['minisitios']));
                $post['id_minisitio'] = array_unique($post['id_minisitio']);
                unset($post['minisitios']);
            }

            if (isset($post['infonovias'])&&$post['infonovias']){
                $post['promociones'] = 1;
            }

            //SI NO ESTA ACTIVO emp_asociadas elimino el array de asociados
            if (isset($post['formulario_lateral']) && !isset($post['emp_asociadas'])){
                unset($post['asociados']);
            }
            unset($post['emp_asociadas']);
            unset($post['formulario_lateral']);

            if (isset($post['datos_viaje'])&&$post['datos_viaje']){ // Campos agregados para rubro LUNA DE MIEL
                $datos_viaje = '';
                if (!empty($post['dia_viaje']) && !empty($post['mes_viaje']) && !empty($post['anio_viaje'])) {
                    $datos_viaje.= '  ** Fecha Estimada del Viaje: ' . $post['dia_viaje'] . '/' . $post['mes_viaje'] . '/' . $post['anio_viaje'];
                }
                if (!empty($post['presupuesto'])) {
                    $datos_viaje.= '  ** Presupuesto Estimado : $' . $post['presupuesto'];
                }
                if (!empty($post['cant_noches'])) {
                    $datos_viaje.= '  ** Cantidad de noches : ' . $post['cant_noches'];
                }

                if (!empty($datos_viaje)) {
                    $post['comentario'] .= '
                        Datos del Viaje: ' . $datos_viaje;
                }

                unset($post['datos_viaje']);
                unset($post['dia_viaje']);
                unset($post['mes_viaje']);
                unset($post['anio_viaje']);
                unset($post['presupuesto']);
                unset($post['cant_noches']);
            }

            //para las landings dobles id_rubro@proveedores_separados_por_comas

            if (isset($post['proveedores_landing']) && $post['proveedores_landing']){
                $data = $post;
                unset($post['proveedores_landing']);
                foreach ($data['proveedores_landing'] as $pl) {
                    $tmp = explode('@', $pl);
                    $post['id_rubro'] = $tmp[0];
                    $post['id_minisitio'] = explode(',', $tmp[1]);

                    $this->procesos_library->procesar_presupuesto($post);
                }
            }else{
                if (isset($post['verificador_captcha'])) {
                    if ((int) $post['verificador_captcha'] == 5) {
                        $this->procesos_library->procesar_presupuesto($post);
                    }
                } else {
                    $this->procesos_library->procesar_presupuesto($post);
                }
            }


            if (substr($referer, ((int) strlen($referer)) - 1, 1) == '/') {
                $referer = substr($referer, 0, ((int) strlen($referer)) - 1);
            }

            //-- limpio URL porq todos los "-" de la sucursal y nombre del proveedor xq en algunos casos vienen con espacios en blanco.
            $redirect = explode('/', $referer);
            $ultima_pos = array_pop($redirect);
            $goto = '';
            $separador = '';
            foreach ($redirect as $item) {
                $goto .= $separador . $this->parseo_library->clean_url($item);
                $separador = '/';
            }

            $goto .= '/' . $ultima_pos;

            if ($from_espacioeventos)
                die('ok');
            if ($from_produccion_modas)
                die('ok');
            if ($tunnel_proforma_facturacion)
                die('ok');

            if(!empty($post['id_minisitio']) && count($post['id_minisitio']) < 2 && !empty($redirect[0]) && $es_grupal <> 'si'){
                $id_minisitio_tmp = isset($post['id_minisitio'][0]) ? $post['id_minisitio'][0] : $post['id_minisitio'];
                $goto = $redirect[1] . '_CO_m' . $id_minisitio_tmp;
            }

            if(!empty($post['subdominio']) && !empty($redirect[1])){
                $goto = 'http://' . $post['subdominio'] . '.' . DOMAIN . '/' . $redirect[1] . '#gracias';
            }

            $this->session->set_flashdata('gracias', 1);
            $this->session->set_flashdata('id_rubro', $post['id_rubro']);

            if(!empty($post['subdominio']) && !empty($redirect[1])){
                redirect($goto);
            }elseif(!empty($post['subdominio'])){
                redirect('http://' . $post['subdominio'] . '.' . DOMAIN . $goto . '#gracias');
            }else{
                redirect(base_url($goto.'#gracias'));
            }
        }else{
            if($_SERVER['HTTP_REFERER']){
                $this->session->set_flashdata('error_formulario', 1);
                redirect($_SERVER['HTTP_REFERER']);
            }else{
                $this->load->library('config_library');
                redirect(base_url('/' . $this->config_library->sucursal['nombre_seo'] . '/fiestas-de-casamiento'));
            }
            
        }
	}

    public function registro_infonovias(){
        if (!empty($_POST)){
            if (!empty($_GET['id_news'])){
                // Esperando logica
            } else {
                if($_POST['registro_corto']){
                    $this->load->library('procesos_library');
                    $this->load->library('form_validation');

                    $error_required_hidden = array(
                        'required' => 'Su formulario contiene errores'
                    );

                    $this->form_validation->set_rules('redirect', 'redirect', 'required', $error_required_hidden);
                    $this->form_validation->set_rules('id_sucursal', 'id_sucursal', 'required|numeric', $error_required_hidden);
                    $this->form_validation->set_rules('email', 'E-mail', 'required', array(
                            'required'      => 'Debe ingresar un %s.',
                            'valid_email'   => 'Debe ingresar un %s válido.'
                    ));

                    if($this->form_validation->run() == TRUE){ // success
                        $this->procesos_library->procesar_nuevo_infonovias_corto($_POST);
                        $this->session->set_flashdata('registro_infonovia_corto', 1);
                    }

                    redirect($_POST['redirect']);
                }
            }
        }
    }

    public function presupuesto_general(){
        $this->load->library('config_library');
        $this->load->library('parseo_library');
        $this->load->library('procesos_library');

        $this->load->model('generales_model');

        $ok = '';
        if($_POST){
            $ok = $this->config_library->validate_formulario($_POST, FALSE);

            if($ok){

                $this->load->helper('cookie');

                $pres['url_referer'] = $_SERVER['HTTP_REFERER'];
                $pres['id_origen'] = (int)$_POST['id_origen'];
                $pres['id_user'] = isset($_SESSION['id_user'])?$_SESSION['id_user']:NULL;
                $pres['nombre'] = stripslashes(trim($_POST['nombre']));
                $pres['apellido'] = stripslashes(trim($_POST['apellido']));
                $pres['email'] = stripslashes(trim($_POST['email']));
                $pres['telefono'] = stripslashes(trim($_POST['telefono']));
                $pres['id_localidad'] = (int)$_POST['id_localidad'];
                $pres['id_provincia'] = (int)$_POST['id_provincia'];
                $pres['infonovias'] = stripslashes(trim($_POST['infonovias']));
                if($_POST['infonovias']){
                    $pres['promociones'] = 1;
                }

                if(isset($_POST['dia_evento']) && strpos($_POST['dia_evento'], '/')){
                    $tmp = explode('/', $_POST['dia_evento']);
                    $pres['fecha_evento'] = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
                }else{
                    $pres['fecha_evento'] = '0000-00-00';
                }

                if(isset($_POST['dia_nacimiento']) && strpos($_POST['dia_nacimiento'], '/')){
                    $tmp = explode('/', $_POST['dia_nacimiento']);
                    $pres['fecha_nacimiento'] = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];
                }else{
                    $pres['fecha_nacimiento'] = '0000-00-00';
                }

                if(isset($_POST['fecha_evento_variable']) && $_POST['fecha_evento_variable']){
                    $pres['fecha_evento_variable'] = $_POST['fecha_evento_variable'];
                }

                ### ID Tipo Evento ###
                $pres['id_tipo_evento'] = (int)$_POST['resp_1'];

                $invitados = $this->generales_model->get_invitados_form_general($_POST['resp_24']);
                $invitados = str_replace('-','#',$invitados);
                $invitados = str_replace('+','#',$invitados);
                $cant_invitados = explode('#',$invitados);
                $pres['invitados'] = (int) $cant_invitados[1];

                ### Salon - Para los casos en q no tiene uno.
                if(!empty($_POST['resp_39'])){
                    $pres['ubicacion'] = trim($_POST['resp_39']);
                }

                $coma = '';
                $pres['caracteristicas'] = '';

                ### Suscripcion a Paquetes ###
                if(isset($_POST['resp_36']) && (int)$_POST['resp_36']){
                    $pres['paquetes'] = 1;
                    $pres['caracteristicas'] = '37';
                    $coma = ",";
                }elseif(isset($_POST['resp_36'])){
                    $pres['caracteristicas'] = '38';
                    $coma = ",";
                }

                ### Caracteristicas Seleccionadas ###
                if(isset($_POST['resp_8']) && (int)$_POST['resp_8']){
                    $pres['caracteristicas'] .= $coma.(int)$_POST['resp_8'];
                    $coma = ",";
                }
                if(isset($_POST['resp_1']) && (int)$_POST['resp_1']){//-- Tipo de evento
                    $pres['caracteristicas'] .= $coma.(int)$_POST['resp_1'];
                    $coma = ",";
                }
                if(isset($_POST['resp_24']) && (int)$_POST['resp_24']){//-- Cant. de Invitados
                    $pres['caracteristicas'] .= $coma.(int)$_POST['resp_24'];
                    $coma = ",";
                }
                if(isset($_POST['resp_33']) && (int)$_POST['resp_33']){//-- Salon Elejido
                    $pres['caracteristicas'] .= $coma.(int)$_POST['resp_33'];
                    $coma = ",";
                }
                if(isset($_POST['resp_17']) && (int)$_POST['resp_17']){
                    $pres['caracteristicas'] .= $coma.(int)$_POST['resp_17'];
                    $coma = ",";
                }

                ### Rubros Seleccionadas ###
                $pres['id_rubro'] =  implode(',',array_unique(explode(',',trim($_POST['rubrosID']))));

                $this->procesos_library->procesar_presupuesto_general($pres);

                $time = time()+60*60*24*365;


                set_cookie("dia_evento"           , $_POST['dia_evento']                                    , $time);
                set_cookie("ubicacion"            , isset($_POST['ubicacion'])?$_POST['ubicacion']:''       , $time);
                set_cookie("invitados"            , isset($_POST['invitados'])?$_POST['invitados']:''       , $time);
                set_cookie("nombre"               , $_POST['nombre']                                        , $time);
                set_cookie("apellido"             , $_POST['apellido']                                      , $time);
                set_cookie("dia_nacimiento"       , $_POST['dia_nacimiento']                                , $time);
                set_cookie("email"                , $_POST['email']                                         , $time);
                set_cookie("telefono"             , $_POST['telefono']                                      , $time);
                set_cookie("id_provincia"         , isset($_POST['id_provincia'])?$_POST['id_provincia']:'' , $time);
                set_cookie("id_localidad"         , isset($_POST['id_localidad'])?$_POST['id_localidad']:'' , $time);

                $this->session->set_flashdata('registro_presupuesto_general', 1);
            }
        }

        $this->load->model('generales_model');
        $this->load->model('navegacion_model');
        $this->load->model('generales_model');

        $zonas_items = $this->generales_model->get_zonas_e_items();
        $hidden[] = array('name' => 'id_origen', 'value' => 93);


        if (!empty($_SESSION['id_user']) && empty($_COOKIE['email'])){
            $data_form = $this->usuarios_model->get_novio_info($_SESSION['id_user']);
        }else{
            $data_form = $this->parseo_library->asignar_mayoria_campos_formulario();
        }

        $provincias  = $this->navegacion_model->get_provincias(0);
        $localidades = array();
        if(!empty($data_form['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        $id_zona = $this->generales_model->get_id_zona();

        $registro_presupuesto_general = $this->session->userdata('registro_presupuesto_general');

        if($ok){
            $meta_descripcion   = 'Listo! Tu solicitud de presupuesto fue enviada. ¿Qué más podemos hacer para ayudarte?';
            $meta_title         = 'Gracias por Solicitar tu Presupuesto! | Casamientos Online';
            $extra_seo          = '#gracias';
        }else{
            $meta_descripcion   = 'Pedí tu presupuesto aquí y recibí la respuesta rápidamente. Organizar tu casamiento nunca fue tan fácil!';
            $meta_title         = 'Solicitá un Presupuesto para tu Casamiento | Casamientos Online';
            $extra_seo          = '';
        }

        $data_view = array(
            'config'                       => array('mantenimiento_site' => $this->config->item('mantenimiento_site'), 'mantenimiento_presupuesto' => $this->config->item('mantenimiento_presupuesto')),
            'items'                        => $zonas_items['items'],
            'zonas'                        => $zonas_items['zonas'],
            'meta_descripcion'             => $meta_descripcion,
            'meta_keywords'                => 'Casamientos Online, casamientos, bodas, novios, novias,  Argentina, pedi presupuesto, consulta, cotizacion,',
            'meta_title'                   => $meta_title,
            'hidden'                       => $hidden,
            'data_form'                    => $data_form,
            'provincias'                   => $provincias,
            'localidades'                  => $localidades,
            'id_zona'                      => $id_zona,
            'er_fecha'                     => $this->config_library->er_fecha,
            'registro_presupuesto_general' => $registro_presupuesto_general,
            'canonical'                    => base_url('solicitar-presupuesto-general') . $extra_seo
        );

        $this->load->view('presupuesto_general', $this->config_library->basic_data_to_view($data_view));
    }
}