<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infonovias extends MY_Controller{
	public function __construct(){
    	parent::__construct();
    	$this->load->library('config_library');
	}

	public function index($anio = 2017){
        if($anio < 2012) show_404();

        $this->load->model('navegacion_model');
        $this->load->model('banners_model');

        $this->load->library('parseo_library');

        $provincias = $this->navegacion_model->get_provincias(0);
        $localidades = NULL;

        $data_form = $this->parseo_library->asignar_mayoria_campos_formulario();

        if(!empty($data_form['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        $banners = $this->banners_model->views(120);

        $success = FALSE;
        if($_POST){
            $this->load->library('form_validation');
            
            $error_required = array(
                'required' => 'Debe ingresar un %s.',
            );

            $error_required_hidden = array(
                'required' => 'Su formulario contiene errores'
            );
            
            $this->form_validation->set_rules('id_origen', 'id_origen', 'required', $error_required_hidden);
            
            $this->form_validation->set_rules('nombre', 'Nombre', 'required', $error_required);
            $this->form_validation->set_rules('apellido', 'Apellido', 'required', $error_required);
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email', array(
                    'required'      => 'Debe ingresar un %s.',
                    'valid_email'   => 'Debe ingresar un %s válido.',
            ));
            $this->form_validation->set_rules('telefono', 'Teléfono', 'required', $error_required);
            $this->form_validation->set_rules('id_sucursal', 'Provincia', 'required', $error_required);
            $this->form_validation->set_rules('id_provincia', 'Provincia', 'required', $error_required);
            $this->form_validation->set_rules('id_localidad', 'Localidad', 'required', $error_required);

            if($this->form_validation->run() == TRUE){ // success
                $this->load->library('procesos_library');

                $this->procesos_library->procesar_nuevo_infonovias($_POST);

                $success = TRUE;
            }
        }

        $listado_infonovias = $this->navegacion_model->listado_infonovias($anio);
        $meses = $this->parseo_library->get_meses();

        $data_form['id_sucursal'] = $this->session->userdata('id_sucursal');

        $meta_title = 'Info Novias: Enterate de todo lo que una novia preparada debe saber';
        $meta_descripcion = 'Las novias bien informadas son mejores organizadoras. No te pierdas ni un detalle con Infonovias!';

        if($anio){
            $meta_title = 'Info Novias ' . $anio . ': Enterate de todo lo que una novia preparada debe saber';
            $meta_descripcion = 'Infonovias ' . $anio . ': La mejor información de novedades y tendencias de cada año!';
        }

		$data = array(
            'anio'              => $anio,
            'banners'           => $banners,
            'canonical'         => base_url($_SERVER['REQUEST_URI']),
            'data_form'         => $data_form,
            'listado_infonovias'=> $listado_infonovias,
            'localidades'       => $localidades,
            'meses'             => $meses,
            'meta_descripcion'  => $meta_descripcion,
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, infonovias, '.((isset($_GET['anio']) && $_GET['anio'])? $_GET['anio'] : date("Y")),
            'meta_title'        => $meta_title,
            'provincias'        => $provincias,
            'success'           => $success
        );

    	$this->load->view('infonovias', $this->config_library->send_data_to_view($data));
	}

    public function registro(){
        $this->load->model('banners_model');

        $provincias = $this->navegacion_model->get_provincias(0);
        $localidades = NULL;

        $banners = $this->banners_model->views(120);

        $data_form = $this->parseo_library->asignar_mayoria_campos_formulario();

        if(!empty($data_form['id_provincia'])){
            $localidades = $this->navegacion_model->get_localidades($data_form['id_provincia']);
        }

        $data_form['id_sucursal'] = $this->session->userdata('id_sucursal');

        $data = array(
            'banners'           => $banners,
            'data_form'         => $data_form,
            'localidades'       => $localidades,
            'meta_descripcion'  => 'Casamientos Online, el portal de los novios',
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, registro, infonovias',
            'meta_title'        => 'Registro Infonovias - Casamientos Online',
            'sucursal_redirect' => '/infonovias/registro',
            'provincias'        => $provincias
        );

        $this->load->view('infonovias_registro', $this->config_library->send_data_to_view($data));
    }
}