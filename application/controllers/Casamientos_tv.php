<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Casamientos_tv extends MY_Controller {
	private $secciones = array(
        1  => array(
			101 => 101, // VESTIDOS
			100 => 100, // BELLEZA Y ESTILO
			104 => 104, // LA ORGANIZACIÓN / LA FIESTA
			106 => 106  // LUNA DE MIEL
		),
        2  => array(
			101 => 273,
			100 => 272,
			104 => 275,
			106 => 279	
		),
        3  => array(
			101 => 308,
			100 => 307,
			104 => 310,
			106 => 314
		),
        4  => array(
			101 => 664,
			100 => 662,
			104 => 661,
			106 => 658
		),
        5  => array(
			101 => 613,
			100 => 615,
			104 => 616,
			106 => 619
		),
        6  => array(
			101 => 521,
			100 => 523,
			104 => 524,
			106 => 527
		),
        7  => array(
			101 => 748,
			100 => 754,
			104 => 762,
			106 => 782
		),
        8  => array(
			101 => 926,
			100 => 929,
			104 => 925,
			106 => 924
		),
        10 => array(
			101 => 979,
			100 => 982,
			104 => 978,
			106 => 977
		),
        11 => array(
			101 => 871,
			100 => 873,
			104 => 874,
			106 => 877
		),
        12 => array(
			101 => 1031,
			100 => 1034,
			104 => 1030,
			106 => 1029
		)
    );

    public function __construct(){
        parent::__construct();
        $this->load->library('config_library');
        $this->load->model('media_model');
    }

 	public function index($id_seccion = 99){
 		$this->load->model('navegacion_model');
 		$this->load->model('banners_model');

 		$this->load->library('parseo_library');

 		$ubicaciones = $this->navegacion_model->get_ubicaciones_col_tv(1,1,1);
 		$videos = $this->media_model->get_col_tvs($id_seccion, array('ubicacion' => 4, 'limit' => 5, 'orden' => 'id DESC'));

 		if (!$videos){
 			// SOLO MOSTRAMOS LOS RUBROS DE FOTO Y VIDEO DE TODAS LAS SUCURSALES
			$videos = $this->media_model->get_col_tvs($id_seccion, array('orden'=>'RAND()', 'limit' => 5, 'show_prov' => 1, 'rubro' => array(8,50,66,157,173,247,286,373,384,395,406,417,428,439,450,461,472,483,494,505,516,527,538,549,582,619)));
		}

		$videos_json = $this->parseo_library->videos_to_json($videos);

 		$keywords = '';
		$description = 'Encontrá aquí nuestros videos de recomendaciones para tu casamiento y llenate de ideas! Enterate de eventos, promociones y muchas cosas más.';
		
		$secciones = array(
            1  => 99,
            2  => 260,
            3  => 287,
            4  => 555,
            5  => 578,
            6  => 519,
            7  => 729,
            8  => 923,
            10 => 976,
            11 => 869,
            12 => 1028
        );

        $banners = $this->banners_model->views($secciones[$this->session->userdata('id_sucursal')]);

		$data = array(
			'banners'			=> $banners,
			'categoria'			=> isset($_GET['id_categoria']) && $_GET['id_categoria'] ? $_GET['id_categoria'] : '',
			'canonical'			=> base_url($_SERVER['REQUEST_URI']),
			'meta_descripcion'	=> $description,
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, CasamientosTV, TV, video, videos, empresas' . $keywords,
            'meta_title'        => 'Casamientos TV - Los Mejores Videos de Casamientos Online',
            // 'meta_image'		=> str_replace('{_id_}',$video['pic_name'], $this->config->item('youtube_thumb'))
            'seccion'			=> $this->navegacion_model->get_seccion($id_seccion),
            'secciones_hijos'   => $this->navegacion_model->get_secciones($id_seccion),
			'ubicaciones' 		=> $ubicaciones,
			'videos'	  		=> $videos,
			'videos_json'		=> $videos_json
		);	

        $this->load->view('casamientos_tv', $this->config_library->send_data_to_view($data));
 	}

 	public function listado($id_sec = 0, $id_sec_hijo = 0){
 		$this->load->library('parseo_library');
 		$this->load->model('banners_model');

 		$id_seccion = $id_sec ? $id_sec : 99;

 		$secciones_hijos = $this->navegacion_model->get_secciones($id_seccion);

 		if ($id_sec_hijo){
			 $id_seccion = $id_sec_hijo;
		}

		$results = 15;
		$page_id = isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']:1;
		$filtros = array('rubro' => $this->parseo_library->get_rubro_coltv($id_seccion), 'sucursalID' => isset($_GET['id_sucursal'])&&$_GET['id_sucursal']?$_GET['id_sucursal']:'');

		$videos = $this->media_model->get_col_tvs($id_seccion, array('ubicacion' => 2, 'limit' => 5, 'orden' => 'id DESC'));
 		if (!$videos){
 			$videos = $this->media_model->get_col_tvs($id_seccion, array('orden'=>'RAND()', 'limit' => 5, 'rubro' => $this->parseo_library->get_rubro_coltv($id_seccion)));
		}

		if(empty($videos)) redirect(base_url('/casamientos-tv'));

		$videos_json = $this->parseo_library->videos_to_json($videos);

		$videos_all = $this->media_model->get_col_tvs($id_seccion, $filtros, ($page_id - 1) * $results, $results);

		$videos_sin_limit = $this->media_model->get_col_tvs($id_seccion, $filtros);
        $tot = count($videos_sin_limit);

		$nombre_seccion = $this->media_model->get_nombre_seccion($id_seccion);

		$keywords = ', canal, '.$nombre_seccion;
		$description = 'Entrá a nuestros videos de recomendaciones para tu ' . $nombre_seccion . ' y llenate de ideas! Enterate de eventos, promociones y muchas cosas más.';
		$id_seccion = $id_sec ? $id_sec : 99;

		$this->load->library('pagination');

        $config['total_rows'] = $tot / $results;
        $config['per_page'] = 1;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = 'pag';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
        $config['full_tag_close'] = '</div></div>';
        $config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
        $config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';

        $this->pagination->initialize($config);

        $base_javascript = '';
        if(isset($_GET)) $base_javascript = base_url('/casamientos-tv');

        $banners = '';
		if($id_sec && in_array($id_sec, array(100,101,104,106))){
			$banners = $this->banners_model->views($this->secciones[$this->session->userdata('id_sucursal')][$id_sec]);
		}

        $ubicaciones = $this->navegacion_model->get_ubicaciones_col_tv(1,1,1);

        $canonical = explode('?pag', $_SERVER['REQUEST_URI']);
        if(!empty($canonical[0])) $canonical = base_url($canonical[0]);

		$data = array(
			'banners'				=> $banners,
			'base_javascript'	  	=> $base_javascript,
			'canonical'				=> $canonical,
			'get'					=> array(
				'id_seccion' 			=> $id_sec,
				'id_seccion_hijo' 		=> $id_sec_hijo,
			),
			'meta_descripcion'    	=> $description,
            'meta_keywords'       	=> 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, CasamientosTV, TV, video, videos, empresas' . $keywords,
            'meta_title'          	=> 'Casamientos TV - Los Mejores Videos de ' . $nombre_seccion . ' en Casamientos Online',
            'nombre_seccion'	  	=> $nombre_seccion,
            'secciones_hijos'		=> $secciones_hijos,
            'ubicaciones'			=> $ubicaciones,
			'videos'	  		  	=> $videos,
			'videos_all'		  	=> $videos_all,
			'videos_json'		  	=> $videos_json
		);

 		$this->load->view('casamientos_tv_listado', $this->config_library->send_data_to_view($data));
 	}

 	public function detalle($id, $tipo = 'empresas'){
 		$this->load->library('parseo_library');

 		$this->load->model('banners_model');

 		if($tipo == 'empresas'){
 			$video = $this->media_model->get_video_proveedor($id, TRUE);

 			$video['titulo_seo'] = $this->parseo_library->clean_url($video['titulo_galeria']);

 			if(!$video['id']){
 				redirect(base_url('/casamientos-tv/' . $video['titulo_seo'] . '_CO_v' . $id . '_c'));
 			}

			/* ESTO ES PARA URLS AMIGABLES
			if(!$_GET["nombre_empresa"]){
				$nom = explode('@',$video['proveedores']);
				$url = "/videos-de-casamientos/empresas-".$comercial->clean_url_3($nom[1])."/".$_GET["id_galeria"];
				header("HTTP/1.1 301 Moved Permanently");
				die(header("Location: ".$url));
			} */

			$rubros_id = explode('@',$video['rubros']);
			
			switch ($rubros_id[4]){
				case 140:
				case 123:
				case 133:
				case 153:
				case 168:
				case 208:
				case 223:
				case 238:
					$id_seccion = 103;
					break;
				case 106:
				case 121:
				case 131:
				case 146:
				case 161:
				case 201:
				case 216:
				case 231:
					$id_seccion = 101;
					break;
				case 107:
				case 122:
				case 132:
				case 147:
				case 162:
				case 202:
				case 217:
				case 232:
					$id_seccion = 102;
					break;
				case 105:
				case 120:
				case 130:
				case 145:
				case 160:
				case 200:
				case 215:
				case 230:
					$id_seccion = 100;
					break;
				case 108:
				case 124:
				case 134:
				case 148:
				case 163:
				case 203:
				case 218:
				case 233:
					$id_seccion = 104;
					break;
				case 109:
				case 125:
				case 135:
				case 149:
				case 164:
				case 204:
				case 219:
				case 234:
					$id_seccion = 105;
					break;
				case 112:
				case 128:
				case 138:
				case 152:
				case 167:
				case 207:
				case 222:
				case 237:
					$id_seccion = 360;
					break;
				case 110:
				case 126:
				case 136:
				case 150:
				case 165:
				case 205:
				case 220:
				case 235:
					$id_seccion = 106;
					break;
				case 111:
				case 127:
				case 137:
				case 151:
				case 166:
				case 206:
				case 221:
				case 236:
					$id_seccion = 107;
					break;
			}

			//-- Si el video no esta activo, me redirecciona a la seccion --//
			if(!$video['activo_media']){
				redirect(base_url('/casamientos-tv/' . $video['titulo_galeria'] . '_CO_ct' . $id_seccion));
			}
			//-- --//
 		}elseif($tipo == 'casamientos'){
 			$video = $this->media_model->get_galeria(array('id_galeria' => $id));
 			if(isset($video[0]) && $video[0]){
 				$video = $video[0];
 				$video['titulo_seo'] = $this->parseo_library->clean_url($video['titulo_galeria']);
 			}else{
 				show_404();
 			}

			if($video['seccion'] == ""){
				//$_GET['id_seccion'] = 99;
			}else{
				$seccion_1 = explode('~', $video['seccion']);
				$seccion_2 = explode('@', $seccion_1[1]);
				$id_seccion = $seccion_2[0];
				$nombre_seccion = $this->parseo_library->clean_url(!empty($seccion_2[1]) ? $seccion_2[1] : '');
			}	
 		}else{
 			show_404();
 		}

 		$keywords = ','.str_replace(' ',',',$video['titulo_galeria']);
		$description = $video['titulo_galeria'] . ': El video que debes ver si estás organizando tu casamiento!';
		$title = '- Video: '.$video['titulo_galeria'].' ';

		$filtros = array('rubro' => $this->parseo_library->get_rubro_coltv($id_seccion), 'sucursalID' => $this->session->userdata('id_sucursal'));
		$videos_relacionados = $this->media_model->get_col_tvs($id_seccion, $filtros, 0, 6);

		$banners = '';
		if(isset($_GET['id_seccion']) && in_array($_GET['id_seccion'], array(100,101,104,106))){
			$banners = $this->banners_model->views($this->secciones[$this->session->userdata('id_sucursal')][$_GET['id_seccion']]);
		}

 		$data = array(
 			'banners'				=> $banners,
 			'canonical'				=> base_url($_SERVER['REQUEST_URI']),
 			'id_seccion'		  	=> $id_seccion,
 			'meta_descripcion'    	=> $description,
            'meta_keywords'       	=> 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, CasamientosTV, TV, video, videos, empresas' . $keywords,
            'meta_title'          	=> $video['titulo_galeria'] . ' | Casamientos Online',
            'nombre_seccion'		=> !empty($nombre_seccion) ? $nombre_seccion : 'casamientos-tv',
            'tipo'				  	=> $tipo,
			'video' 			  	=> $video,
			'videos_relacionados' 	=> $videos_relacionados
		);

 		$this->load->view('casamientos_tv_detalle', $this->config_library->send_data_to_view($data));
 	}

 	public function tags($id = NULL){
 		if(!$id) redirect(base_url('/casamientos-tv'));

 		$id_seccion = '';
 		$page_id = isset($_GET['pag'])&&$_GET['pag']?$_GET['pag']:1;

 		$this->load->model('editorial_model');
 		
 		$results = 15;

 		$filtros['show_prov'] = 1;
		$filtros['id_tag'] = $id;

 		$videos_all = $this->media_model->get_col_tvs($id_seccion, $filtros, ($page_id - 1) * $results, $results);

 		if(empty($videos_all)) show_404();

 		$videos_sin_limit = $this->media_model->get_col_tvs($id_seccion, $filtros);
        $tot = count($videos_sin_limit);

		$this->load->library('pagination');

        $config['total_rows'] = $tot / $results;
        $config['per_page'] = 1;
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;
        $config['query_string_segment'] = 'pag';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['full_tag_open'] = '<div class="paginator"><div class="pull-right">';
        $config['full_tag_close'] = '</div></div>';
        $config['next_link'] = '<span><i class="fa fa-caret-right"></i></span>';
        $config['prev_link'] = '<span><i class="fa fa-caret-left"></i></span>';
        $config['cur_tag_open'] = '<a class="active">';
        $config['cur_tag_close'] = '</a>';

        $this->pagination->initialize($config);

        $nombre_seccion = 'Tags';

        $keywords = ', canal, '.$nombre_seccion;
		$description = 'Casamientos Online, el portal de los novios. Casamientos TV, Canal de videos de '.$nombre_seccion;
		$title = '- Canal: ' . $nombre_seccion . ' ';

		$breadcrumbs = array(
            0 => array(
                'nombre' => 'Casamientos TV',
                'url'   => '/casamientos-tv',
            )
        );
        $titulo = 'Videos';
        $url_tag = '/casamientos-tv/{?}_CO_t';

        $canonical = explode('?pag', $_SERVER['REQUEST_URI']);
        if(!empty($canonical[0])) $canonical = base_url($canonical[0]);

		$data = array(
			'breadcrumbs'       => $breadcrumbs,
			'canonical'			=> $canonical,
			'meta_descripcion'  => $description,
            'meta_keywords'     => 'Casamientos Online, casamientos, bodas, novios, novias, Buenos Aires, Argentina, CasamientosTV, TV, video, videos, empresas' . $keywords,
            'meta_title'        => 'CASAMIENTOS TV ' . $title . '- Casamientos Online',
            'nombre_seccion'	=> $nombre_seccion,
			'casamientos_tv'	=> TRUE,
			'tag' 				=> $this->editorial_model->get_tag($id)['descripcion'],
			'tags' 				=> $this->editorial_model->get_tags(array('tipo_nota'=>1415,'tipo'=>3)),
			'videos_all'		=> $videos_all,
            'titulo'            => $titulo,
            'url_tag'           => $url_tag
		);

		$this->load->view('tags', $this->config_library->send_data_to_view($data));
 	}
}