<?php
$bodyclass = 'page ceremonias_detalle iglesias';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>
<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li>Templos Judíos</li>
	</ul>
	<h1 class="title_sep">Requisitos para Casarse en un Templo Judío en Argentina</h1>
	<img class="big_pic border_templ" src="<?php echo base_url('/assets/images/big_pic_templos_judios.jpg'); ?>" />
	<div id="split">
		<section id="primary">
			<div id="filtro" class="relative">
				<h2>Datos útiles</h2>
				
			</div>
			<div class="caba datos_utiles_cont">
				<div class="datos_utiles_wrapper no_margin">
					<div>
						<span class="icon ic_7 valign"></span>
						<h4>¿Donde casarse?</h4>
						<p>Los casamientos judíos se pueden realizar en las sinagogas o en el lugar que los novios indiquen, solicitando la presencia del rabino, luego de haberse realizado el casamiento por civil como condición indispensable.</p>
					</div>
					
					<div>
						<span class="icon ic_9 valign"></span>
						<h4>Requisitos</h4>
						<p>- Ambos contrayentes deben ser judíos, de madre judía. Deben presentar el certificado de judeidad que se tramita en el rabinato de la AMIA. Para demostrar que pertenecen a una familia judía deben presentar el ketubá de sus padres.</p>
						<p>- Si son divorciados y quieren casarse nuevamente por la religión judía, el divorcio debe estar realizado de acuerdo a la ley Judía, al Guet .Es decir que si existiese otro matrimonio religioso anterior, éste deberá estar anulado por la Comisión de Divorcios del Seminario Rabínico para que se pueda celebrar el nuevo matrimonio.</p>
						<p>- Si bien no hay un "curso prematrimonial obligatorio" los novios estudian las leyes de la cosmovisión de la Torá acerca del hombre, la mujer y sus diferencias. En la mayoría de los templos se dictan charlas para los novios acerca de la pareja, la religión judía en el matrimonio, el futuro, etc.</p>
					</div>
					
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>Padrinos</h4>
						<p>No son obligatorios. El novio y la novia pueden llevar sus acompañantes.</p>
					</div>
					
					<div>
						<span class="icon ic_3 valign"></span>
						<h4>Testigos</h4>
						<p>No son obligatorios. Los testigos pueden ser 2, mayores de 13 años, observantes de los preceptos.</p>
					</div>
					
					<div>
						<span class="icon ic_8 valign"></span>
						<h4>Distintas religiones</h4>
						<p>El judaísmo celebra casamientos mixtos. Si bien acepta la convivencia y hasta la procreación de un judío con un no judío, no considera que estas parejas sean un "matrimonio". En el caso de que uno de los novios no sea judío pero desee serlo, puede realizar un curso anual de conversión religiosa. Una vez realizado el curso, se extiende un certificado que deberá ser presentado en el templo donde se celebre el matrimonio.</p>
					</div>
										
				</div><!-- .datos_utiles_wrapper -->
			</div><!-- .caba.datos_utiles_cont -->
									
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php $templos = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->	
	</div><!-- #split -->
</div><!-- .container -->

<?php include('footer.php'); ?>