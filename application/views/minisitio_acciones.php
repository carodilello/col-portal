<div id="minisitio_acciones" class="box dropdown">
	<a href="javascript:;" data-toggle="modal" data-id-minisitio="<?php echo isset($proveedor['id_minisitio'])&&$proveedor['id_minisitio']?$proveedor['id_minisitio']:''; ?>" data-idtipo="1" data-target="#modal-telefono">
		<div data-toggle="tooltip" data-placement="top" title="Ver teléfono"><img alt="Teléfono" src="<?php echo base_url('/assets/images/accion_phone.png'); ?>" /></div>
	</a>
	
	<a href="javascript:;" data-toggle="modal" data-id-minisitio="<?php echo isset($proveedor['id_minisitio'])&&$proveedor['id_minisitio']?$proveedor['id_minisitio']:''; ?>" data-idtipo="2" data-target="#modal-whatsapp" class="whatsapp <?=empty($proveedor['whatsapp'])?'disabled':'';?>">
		<div <?=!empty($proveedor['whatsapp'])?'data-toggle="tooltip"':'';?> data-placement="top" title="Ver Whatsapp"><img alt="Whatsapp" src="<?php echo base_url('/assets/images/accion_whatsapp.png'); ?>" /></div>
	</a>

	<a data-id-minisitio="<?php echo isset($proveedor['id_minisitio'])&&$proveedor['id_minisitio']?$proveedor['id_minisitio']:''; ?>" href="<?=!empty($proveedor['usuario_facebook']) ? ('https://m.me/' . $proveedor['usuario_facebook']) : 'javascript:;';?>" data-idtipo="3" class="<?=empty($proveedor['usuario_facebook']) ? 'disabled' : '';?>">
		<div <?=!empty($proveedor['usuario_facebook']) ? 'data-toggle="tooltip"':'';?> data-placement="top" title="Ver Messenger"><img alt="Messenger" src="<?php echo base_url('/assets/images/accion_messenger.png'); ?>" /></div>
	</a>
		
	<a data-id-minisitio="<?php echo isset($proveedor['id_minisitio'])&&$proveedor['id_minisitio']?$proveedor['id_minisitio']:''; ?>" id="accion_qr" class="no_border" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-idtipo="4">
		<div data-toggle="tooltip" data-placement="top" title="Ver código QR">
			<img alt="QR" src="<?php echo base_url('/assets/images/accion_qr.png'); ?>" />
			<ul class="dropdown-menu" aria-labelledby="accion_qr">
				<li><img alt="QR" src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=<?php echo $vcard; ?>&choe=UTF-8" title="Añadir datos del proveedor" /></li>
			</ul>
		</div>	
	</a><!-- #accion_qr -->
	<div class="clear"></div>
</div><!-- #minisitio_acciones -->

