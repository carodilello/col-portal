<?php
$bodyclass = 'casamientos_tv videos_listado';
include('header.php');

$cant_videos = count($videos);

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="'.base_url('/assets/js/funciones_redes.js').'"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_video.js') . '"></script>',
	'<script type="text/javascript" src="'.base_url('/assets/js/funciones_casamientostv.js').'"></script>'
);

$primer_video = array();

$tmp = json_decode($videos_json, TRUE);
if(!empty($tmp[0])) $primer_video = $tmp[0]; ?>
<div id="header_casamientos_tv">
	<div class="container">
		<ul id="breadcrumbs">
			<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
			<li><a href="<?php echo base_url('/casamientos-tv'); ?>">Casamientos TV</a></li>
			<li><?php echo $nombre_seccion; ?></li>
		</ul>
		<h1 class="title_sep">Casamientos TV</h1>
		<?php include('video_slider.php'); ?>	
	</div> <!-- .container -->	
</div><!-- #casamientos_tv -->

<div class="container">
	<input type="hidden" class="base_javascript" value="<?php echo $base_javascript; ?>" />
	<div id="split">
		
		<section id="primary">
			<div id="grid_galeria">
				<div id="header_grid_galeria">
					<h3>Más videos de <span class="green"><?php echo $nombre_seccion; ?></span></h3>
					<select class="select_secciones">
						<option value="">- Secciones -</option>
						<?php if($ubicaciones) foreach ($ubicaciones as $suc){ 
							$tmp_ubicacion = explode('@', $suc['hijos']); ?>
							<option value="<?php echo $tmp_ubicacion[0]; ?>" <?=isset($get['id_seccion']) && $get['id_seccion'] == $tmp_ubicacion[0] ? 'selected="selected"' : '';?> data-url="<?php echo $tmp_ubicacion[4]; ?>"><?php echo $tmp_ubicacion[1]; ?></option>
						<?php } ?>
					</select>
					<select class="select_canales">
						<option value="">- Canales -</option>
						<?php if(isset($secciones_hijos) && $secciones_hijos) foreach ($secciones_hijos as $k => $canal){ ?>
							<option value="<?=$k;?>" <?=isset($get['id_seccion_hijo']) && $get['id_seccion_hijo'] == $k ? 'selected="selected"' : '';?>><?=$canal;?></option>
						<?php } ?>
					</select>
				</div><!-- #header_grid_videos -->
				<?php if($videos_all){ ?>
					<div class="row">
						<?php include('video.php'); ?>
					</div>
					<?php echo $this->pagination->create_links(); ?>
				<?php } ?>
			</div><!-- #grid_galeria -->
		</section><!-- #primary -->
		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
			
			<?php if(isset($banners[275]) && $banners[275] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[275]) && $banners[275] ? $banners[275] : '<p>COL TV listado 300x250<span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[276]) && $banners[276] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[276]) && $banners[276] ? $banners[276] : '<p>COL TV listado 300x250<span>Posicion 2</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #sidebar -->
	</div><!-- #split -->
	<?php include('popup_amigo.php'); ?>
</div><!-- .container --> 
<script type="text/javascript">
var videos = <?php echo $videos_json; ?>;
</script>
<?php include('footer_info.php'); ?>