<?php
$bodyclass = 'contacto';
include('header.php'); ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Contacto</li>
	</ul>
	<h1 class="title_sep">Contacto</h1>
	<div class="contacto_intro box">
		<p class="big">¿Algún comentario o sugerencia que quieras compartir con el equipo de Casamientos Online? </p>
		<p>En este espacio dejá tus observaciones o inquietudes que serán derivadas al área correspondiente.</p>
	</div>
	<?php include('formulario_sugerencia.php'); ?>
</div><!-- container -->
<?php include('footer.php'); ?>