<?php if(strlen(strip_tags($producto["contenido"])) >= 160) $puntos_suspensivos = "...";
if(strlen(strip_tags($producto["aclaracion"])) >= 40) $puntos_suspensivos_aclaracion = "...";
$fv_tmp = explode("-",$producto["fecha_vencimiento"]);
if(isset($fv_tmp[2])) $fecha_vencimiento = $fv_tmp[2]."/".$fv_tmp[1]."/".$fv_tmp[0]; ?>
<div class="box_listado">
	<div class="precio">
		<?php if($producto["precio"] != '0.00' && $producto["precio"] != '$ 0,00' && $producto["precio"] != 'U$S 0,00'){
				if(isset($producto["precio_tipo"])&&$producto["precio_tipo"]){ ?>
					<strong>$<?php echo $producto["precio"]; ?></strong>
					<strong><?php echo $producto["precio_tipo"]; ?></strong>
				<?php }else{ ?>
					<strong>
						<?php
							if($producto["moneda"] == 'P') echo '$'; 
							if($producto["moneda"] == 'D') echo 'U$S';
							if($producto["moneda"] == 'E') echo '&euro;';
							echo number_format($producto["precio"], 2, ",", "."); ?>
					</strong>
				<?php }
			}
		if($producto["porcentaje_descuento"] && (!$producto["precio"] || $producto["precio"] == '0.00')){ ?>
			<span><span><?php echo $producto["porcentaje_descuento"]; ?></span>%OFF</span>
		<?php } ?>
	</div>
	<div class="img_wrapper">
		<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . '_CO_pr' . $producto["id_producto"] . '#pp_minisitio'; ?>">
			<?php
			if(in_array(get_headers('http://media.casamientosonline.com/images/'.$producto["imagen"])[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
				<img alt="<?php echo $producto["titulo"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/images/'.$producto["imagen"].'"';?> alt="<?php echo $producto["titulo"]; ?>" />
			<?php }else{ ?>
				<img alt="<?php echo $producto["titulo"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/logos/'.$producto["logo"].'"';?> alt="<?php echo $producto["titulo"]; ?>" />
			<?php } ?>
		</a>
	</div>
	<div class="info_wrapper">
		<?php if($this->router->class.'_'.$this->router->method == 'proveedores_listado_empresas' || $this->router->class.'_'.$this->router->method == 'home_buscar'){ ?>
			<h2><a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . '_CO_pr' . $producto["id_producto"] . '#pp_minisitio'; ?>"><?php echo $producto["titulo"]; ?></a></h2>
		<?php } else { ?>	
			<h3><a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . '_CO_pr' . $producto["id_producto"] . '#pp_minisitio'; ?>"><?php echo $producto["titulo"]; ?></a></h3>
		<?php }
		
		if($producto["subtitulo"]){ ?><p class="subtitulo"><?php echo $producto["subtitulo"]; ?></p><?php } ?>

		<p class="proveedor">
			<?php if(!isset($no_empresa) || !$no_empresa || (isset($no_empresa) && $no_empresa == FALSE)){ ?>
				<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro']; ?>"><i class="fa fa-suitcase"></i><?php echo $producto["proveedor"]; ?></a>
			<?php }
			if($producto["porcentaje_descuento"] && (!$producto["precio"] || $producto["precio"] == '0.00')){ ?>
				<span><i class="fa fa-percent"></i>Descuento</span>
			<?php }
			if(isset($producto["regalo"]) && $producto["regalo"]){ ?>
				<span><i class="fa fa-gift"></i>Regalo</span>
			<?php }
			if(isset($producto["oferta"]) && $producto["oferta"]){ ?>
				<span><i class="fa fa-tag"></i>Oferta</span>
			<?php } ?>
		</p>

		<?php if($this->router->class.'_'.$this->router->method == 'proveedores_listado_empresas' || $this->router->class.'_'.$this->router->method == 'home_buscar'){ ?>
			<h3 class="descripcion"><?php echo substr(strip_tags($producto["contenido"]),0,160).$puntos_suspensivos; ?></h3>
		<?php } else { ?>
			<p class="descripcion"><?php echo substr(strip_tags($producto["contenido"]),0,160).$puntos_suspensivos; ?></p>
		<?php }

		if(!empty($fecha_vencimiento)){ ?>
			<p class="fecha"><i class="fa fa-calendar"></i>Valido hasta: <strong><?php echo $fecha_vencimiento; ?></strong></p>
		<?php } ?>
	
		<div class="bottom_actions">
			<?php if(($producto["precio"] == '0.00' || $producto["precio"] == '$ 0,00' || $producto["precio"] == 'U$S 0,00')){ ?>
				<?php if($producto["precio_viejo"]){ ?>
					<p class="data_adicional"><?php echo $producto["precio_viejo"]; ?></p>
				<?php } ?>
			<?php }

			if(isset($producto["aclaracion"])){ ?>
				<p class="data_adicional"><?php if(isset($producto["aclaracion"]) && (isset($producto["precio_viejo"]))){ ?>&nbsp;- <?php } ?> <?php echo substr(strip_tags($producto["aclaracion"]),0,40).$puntos_suspensivos_aclaracion; ?></p>
			<?php }

			if(isset($promocion_en_minisitio)){ ?>
				<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/presupuesto-promocion_CO_r'.$producto['id_rubro'].'_pr'.$producto["id_producto"]; ?>" class="btn btn-default">Consultar</a>
			<?php }else{ ?>
				<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/presupuesto-promociones_CO_r'.$producto['id_rubro'].'_pr'.$producto["id_producto"]; ?>" class="btn btn-default">Pedir Presupuesto</a>
			<?php } ?>
		</div><!-- .bottom_actions -->
	</div><!-- .info_wrapper -->
</div><!-- .box_listado -->