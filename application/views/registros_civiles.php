<?php
$bodyclass = 'page ceremonias_detalle registro_civiles';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>

<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li>Registros Civiles</li>
	</ul>
	<h1 class="title_sep">Requisitos para Casarse por Civil en Argentina</h1>
	<img class="big_pic border_reg" src="<?php echo base_url('/assets/images/big_pic_registros_civiles.jpg'); ?>" alt="Requisitos para Casarse por Civil en Argentina" />
	<div id="split">
		<section id="primary">
			<div id="filtro" class="relative">
				<h2 class="pull-left">Datos útiles</h2>
				<div class="pull-right">
					<label>Seleccionar ubicación:</label>
					<select class="ubicacion_datos_utiles">
						<option value="caba">CABA</option>
						<option value="bsas">Prov. Buenos Aires</option>
						<option value="sf">Santa Fe</option>
						<option value="ros">Rosario</option>
					</select>
				</div>
			</div>
			<div class="caba datos_utiles_cont">
				<div class="datos_utiles_wrapper">
					<div>
						<span class="icon valign"></span>
						<h4>¿Cuando hay que reservar Fecha?</h4>
						<p>Se deberá concurrir a la Circunscripción que elijan a fin de fijar día y hora; recibir los formularios e instrucciones necesarias, en un plazo no mayor a 29 días ni menor a 10 (días hábiles).</p>
						<p><strong>Si lo piden a través de Internet en: <a href="http://www.buenosaires.gov.ar/registrocivil">www.buenosaires.gov.ar/registrocivil</a></strong> y hay disponibilidad de turnos, pueden reservarla como máximo 15 días antes.</p>
						<p>En ambos casos la documentación necesaria para la celebración y labrado del acta, debe ser entregada a los 3 días hábiles anteriores a la ceremonia.</p>
					</div>
					
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>¿Quién puede solicitar el turno?</h4>
						<p>Uno o ambos contrayentes.</p>
					</div>
					
					<div>
						<span class="icon ic_3 valign"></span>
						<h4>¿Cuántos testigos son necesarios y obligatorios?</h4>
						<p>Dos testigos mayores de 18 años, con domicilio en la Ciudad de Buenos Aires. Testigos innecesarios: Hasta cuatro adicionales. Sin necesidad de acreditar domicilio en la Ciudad de Bs. As.</p>
					</div>
					
					<div>
						<span class="icon ic_4 valign"></span>
						<h4>¿Cuánto sale el trámite del matrimonio por civil?</h4>
						<p>Libreta Matrimonial: $ 30.-<br />Costo por cada testigo innecesario: $ 80.-<br />Los testigos necesarios no tienen costo.</p>
					</div>
					
					<div>
						<span class="icon ic_5 valign"></span>
						<h4>¿Cómo se solicita turno a través de la web?</h4>
						<p>Hay que ingresar a a la página de guía de trámites para solicitar requisitos y solicitar turno www.buenosaires.gob.ar/registrocivil. Se selecciona turno, reservando sala y seleccionado sede, fecha y hora para la realización del trámite. Se debe completar los formularios: prenupcial, matrimonios, testigos, fotógrafo y estadístico y confirmar.<br />Paso siguiente se completa el formulario de datos personales e imprime el comprobante Turnos Matrimonio y 3 días antes de celebrarse el casamiento se presenta la documentación.</p>
					</div>
					
					<div>
						<span class="icon ic_6 valign"></span>
						<h4>¿Qué registro civil les corresponde?</h4>
						<p>Los novios pueden elegir cualquier Registro Civil del CGP que quieran. Ver directorio haciendo <a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-registros-civiles-en-argentina'); ?>">click aquí.</a></p>
					</div>
				</div><!-- .datos_utiles_wrapper -->
			
				<div class="requisitos_wrapper">
					<!-- Nav tabs -->
					<ul class="solapas_datos_utiles" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span>1</span>Requisitos</a></li>
						<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span>2</span>Pasos para realizar el trámite</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<p>Los turnos para matrimonios, se solicitarán entre los 29 y 14 días hábiles, antes del día de la 
							ceremonia.</p>
							<p>Si ambos contrayentes son solteros, mayores de edad y tienen DNI argentino vigente, el turno se solicita “on line”, mediante la página respectiva del Gob. De la Ciudad.- Uno de los dos contrayentes debe tener en su DNI un domicilio registrado en la CABA.-</p>
							<p>En casos que los contrayentes no se encuentren encuadrados en lo establecido en el punto 
							anterior, el pedido de turno es presencial.</p>
							<h5>Solteros y mayores de edad con DNI vigente:</h5>
							<p>- Documentos de ambos contrayentes.</p>
							<h5>Solteros y mayores de edad Argentino/a con DNI y Extranjero/a con Pasaporte:</h5>
							<p>- DNI vigente<br />
							- Pasaporte vigente y en buen estado.</p>
							<h5>Con matrimonio y divorcio vincular en Argentina.</h5>
							<p>- Se prueba tal extremo con la partida original de su matrimonio anterior con nota marginal del divorcio, inscripta.</p>
							<h5>Con matrimonio en el extranjero y divorcio vincular en Argentina</h5>
							<p>- Presentará la partida de matrimonio con la marginal del divorcio y testimonio de la sentencia. 
							En su caso, la documentación deberá ser traducida y legalizada.</p>
							<h5>Con matrimonio celebrado en Argentina y divorcio en el extranjero:</h5>
							<p>- Deberá presentar la partida de matrimonio original con la correspondiente inscripción del divorcio vincular.</p>
							<h5>Con matrimonio y divorcio en el extranjero:</h5>
							<p>- Se deberá presentar la partida de su matrimonio y sentencia de su divorcio, con las legalizaciones internacionales correspondientes (Apostilla de La Haya o firmas legalizadas por el Ministerio de Relaciones Exteriores, de los Diplomáticos firmantes que a su vez legalizan las firmas extranjeras). En este caso particular, se necesitará tramitar por ante la oficina de Resoluciones y Dictámenes, la Disposición, que deberá firmar la Dir. Gral., que autorice al interesado a contraer nupcias en nuestro país. Mientras tanto, se otorgará turno condicional, a la espera de la Disposición.</p>
							<h5>Contrayente viudo/a:</h5>
							<p>- Se prueba tal extremo con las respectivas partidas originales, de matrimonio y defunción.</p>
							<p>De edades inferiores a 18 años:</p>
							<p>- Podrán contraer matrimonio los menores a partir de los 16 años, con la autorización de sus 
							progenitores o mediante Dispensa Judicial.
							Los padres darán autorización encontrándose presentes durante la ceremonia firmando el respectivo libro de actas. Si no estuvieren presentes, antes de la celebración, podrán dar su consentimiento a través de una minuta debidamente firmada y legalizada por ante Oficial Público, quien suscribirá el documento junto con el o los firmantes, que se archivará con los demás documentos.</p>
							<p>También, la autorización, podrá ser otorgada ante Escribano Público, se archivará la copia 
							certificada de la Escritura.</p>
							<p>Los vínculos filiales se probarán con partida de nacimiento del contrayente menor, que deberá 
							presentar en original actualizado.</p>
							<p>- Testimonio de la dispensa judicial (autorización del Juez competente)<br />
							- Padre o madre fallecido, partida de defunción.</p>
							<p>Si hubiere filiación única, bastará que el autorizante sea el progenitor reconociente.</p>
							<p>En todos los casos en que no hubiere consentimiento expreso de los padres del menor, se 
							requerirá indefectiblemente, la Dispensa Judicial otorgada por Juez competente.</p>
							<h5>Si ambos padres están fallecidos, tutor.</h5>
							<p>Venia judicial (cuando los progenitores no la otorgan)</p>
							<h5>Matrimonio a domicilio por peligro inminente de muerte.</h5>
							<p>- Certificado médico que acredite que el/la contrayente se encuentra en pleno uso de sus facultades mentales y en peligro inminente de muerte.<br />
							- Cuatro testigos con domicilio en la república Argentina.<br />
							- Documentación que acredite el estado civil.</p>
							<h5>Idioma Nacional</h5>
							<p>Si alguno de los contrayentes no conociera el idioma nacional, deberá ser asistido por un traductor público nacional de su idioma de origen, matriculado en el Colegio de Traductores Públicos de la Ciudad de Buenos Aires, quien estará presente en el momento de la ceremonia de matrimonio. En este caso, se deberá traer 3 días antes, junto con el resto de la documentación, fotocopia del DNI y de la Credencial de la matrícula del traductor público elegido. Si con la debida certificación del 
							colegio de traductores de la CABA no hubiere inscripto traductor público del idioma que se trate, se suplirá con un intérprete, deberá tener DNI argentino vigente, que bajo juramento de ley declare que conoce el idioma del contrayente y el idioma nacional.</p>
							<h5>Prenupciales</h5>
							<p>- Se realizan siete (7) días corridos antes de la fecha de celebración, en un hospital público del G.C.B.A o en clínicas y sanatorios privados de la Ciudad de Buenos Aires.<br />De hacerse en otra jurisdicción, deberán ser legalizados por la autoridad sanitaria del lugar.</p>
							<p>Los contrayentes deben manifestar la elección del régimen patrimonial del matrimonio, al momento de presentar la solicitud para contraer matrimonio.</p>
							<p>Los contrayentes, también podrán presentar Convención Matrimonial. Esta se otorga siempre ante Escribano Público.</p>
						</div>
						<div role="tabpanel" class="tab-pane pasos_a_seguir" id="profile">
							<h3>1) Tener en cuenta</h3>
							<p>En alguna de las siguientes <strong>Circunscripciones del Registro Civil</strong> que elijas. Tené en cuenta que deberás solicitar el turno 14 días corridos con antelación a la fecha del acto y que, 7 días corridos antes, deberás acercarte a la sede elegida para entregar la documentación completa:</p>
							<p>- Circunscripción 1º: Uruguay 753</p>
							<p>- Circunscripción 2º: Vicente López 2050</p>
							<p>- Circunscripción 3º: Sarandí 1273 (circunstancialmente, para agregar testigos no obligatorios, se deberá abonar la tarifa en otra sede)</p>
							<p>- Circunscripción 4º: Suárez 2032</p>
							<p>- Circunscripción 5º: Carlos Calvo 3325</p>
							<p>- Circunscripción 6º: Patricias Argentinas 277</p>
							<p>- Circunscripción 7º: Avenida Rivadavia 7202</p>
							<p>- Circunscripción 11º: Ricardo Gutiérrez 3254</p>
							<p>- Circunscripción 13º: Cabildo 3067</p>
							<p>- Circunscripción 14º: Beruti 3325</p>
							<p class="gum_40">Circunscripción 15º: Av. Córdoba 5690</p>
							
							<h3>2) Saber quién puede efectuarlo</h3>
							<p class="gum_40">Uno o ambos contrayentes en condiciones legales de casarse.</p>
							
							<h3>3) Realizar los estudios prenupciales y presentarse en el Registro Civil con esa documentación antes del día de la boda</h3>
						</div>
					</div><!-- .tab-content -->
				</div><!-- .requisitos_wrapper -->
			
				<div class="mas_info_ceremonias">
					<h2 class="title_sep">Más información</h2>
					<p>Si estás fuera de Capital Federal comunicate al <strong>0800-999-2727.</strong></p>
					<p class="gum_40">Podés encontrar más información en <a href="http://www.buenosaires.gob.ar/registrocivil" target="_blank" class="pink"><strong>www.buenosaires.gob.ar/registrocivil</strong></a><br />Para el servicio de ceremonia de entrega de libreta: Solicitar turno en forma presencial hasta 10 días hábiles antes de la ceremonia. </p>
					
					<h3>Organismo responsable</h3>
					<ul class="gum_40">
						<li><i class="fa fa-map-marker"></i><strong>Dirección:</strong> Uruguay 753</li>
						<li><i class="fa fa-clock-o"></i><strong>Horarios de atención:</strong> Lunes a viernes de 7.30 a 19 hs / Horario habilitado de las cajas de tesorería  para pago de trámites): lunes a viernes de 8.30 a 14.30 hs</li>
						<li><i class="fa fa-phone"></i><strong>Tel:</strong> 4373-8441/45</li>
						<li><i class="fa fa-envelope"></i><strong>Mail:</strong> consultas_dgrc@buenosaires.gov.ar</li>
						<li><i class="fa fa-link"></i><strong>Web:</strong> buenosaires.gob.ar/areas/registrocivil/</li>
					</ul>
					<a href="http://www.buenosaires.gob.ar/pedir-nuevo-turno?idPrestacion=1029" target="_blank" class="btn btn-default" id="tramite_online"><i class="fa fa-desktop white"></i> Iniciar Trámite online</a>
				</div><!-- #mas_info -->
			</div><!-- .caba.datos_utiles_cont -->
			
			<div class="bsas datos_utiles_cont">
				<div class="datos_utiles_wrapper">
					<div>
						<span class="icon valign"></span>
						<h4>¿Cuando hay que reservar Fecha?</h4>
						<p>Se deberá concurrir a la Circunscripción que elijan a fin de fijar día y hora; recibir los formularios e instrucciones necesarias, en un plazo no mayor a 29 días ni menor a 10 (días hábiles).</p>
						<p><strong>Si lo piden a través de Internet en: <a href="http://www.buenosaires.gov.ar/registrocivil">www.buenosaires.gov.ar/registrocivil</a></strong> y hay disponibilidad de turnos, pueden reservarla como máximo 15 días antes.</p>
						<p>En ambos casos la documentación necesaria para la celebración y labrado del acta, debe ser entregada a los 3 días hábiles anteriores a la ceremonia.</p>
					</div>
					
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>¿Quién puede solicitar el turno?</h4>
						<p>Uno o ambos contrayentes.</p>
					</div>
					
					<div>
						<span class="icon ic_3 valign"></span>
						<h4>¿Cuántos testigos son necesarios y obligatorios?</h4>
						<p>Dos testigos mayores de 18 años, con domicilio en la Ciudad de Buenos Aires. Testigos innecesarios: Hasta cuatro adicionales. Sin necesidad de acreditar domicilio en la Ciudad de Bs. As.</p>
					</div>
					
					<div>
						<span class="icon ic_4 valign"></span>
						<h4>¿Cuánto sale el trámite del matrimonio por civil?</h4>
						<p>Libreta Matrimonial: $ 30.-<br />Costo por cada testigo innecesario: $ 80.-<br />Los testigos necesarios no tienen costo.</p>
					</div>
					
					<div>
						<span class="icon ic_5 valign"></span>
						<h4>¿Cómo se solicita turno a través de la web?</h4>
						<p>Hay que ingresar a a la página de guía de trámites para solicitar requisitos y solicitar turno www.buenosaires.gob.ar/registrocivil. Se selecciona turno, reservando sala y seleccionado sede, fecha y hora para la realización del trámite. Se debe completar los formularios: prenupcial, matrimonios, testigos, fotógrafo y estadístico y confirmar.<br />Paso siguiente se completa el formulario de datos personales e imprime el comprobante Turnos Matrimonio y 3 días antes de celebrarse el casamiento se presenta la documentación.</p>
					</div>
					
					<div>
						<span class="icon ic_6 valign"></span>
						<h4>¿Qué registro civil les corresponde?</h4>
						<p>Los novios pueden elegir cualquier Registro Civil del CGP que quieran. Ver directorio haciendo <a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-registros-civiles-en-argentina'); ?>">click aquí.</a></p>
					</div>
				</div><!-- .datos_utiles_wrapper -->
				
				<div class="requisitos_wrapper">
					<!-- Nav tabs -->
					<ul class="solapas_datos_utiles" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span>1</span>Requisitos</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<h5>Edad de los contrayentes</h5>
							<p>Entre 16 y 18 años, con autorización de sus representantes legales o en su defecto Dispensa Judicial.</p>
							<p>Si el contrayente fuera menor de 16 años deberá requerirse la autorización de sus representantes legales (Padres, tutores, etc.) más la dispensa judicial.</p>
							<h5>Observaciones</h5>
							<p>En los dos casos mencionados con antelación si los representantes legales no brindan su consentimiento (por cualquier causa por ejemplo conflicto de intereses, ausencia, etc.) podrá suplirse el mismo con Dispensa Judicial.</p>
							<p>Deben presentarse los futuros contrayentes con DNI o documento que acredite identidad.</p>
							<p>Se deberá preguntar a los futuros contrayentes si optan por separación de bienes o convención matrimonial.</p>
							<p>Dos testigos mayores de 18 años. Deben presentar cualquier documento que acredite identidad según Disposición 598/16.</p>
							<p>Cuatro testigos en caso de peligro de muerte.</p>
							<p>Solicitar el turno con 20 días hábiles con antelación a la fecha que desean contraer matrimonio pudiéndose acortar este plazo por motivo debidamente justificado.</p>
							<p>Presentar prenupciales con no más de 7 días emitidos.</p>
							<p>Para el supuesto de que alguno de los contrayentes estuviere divorciado, deberá justificar su estado civil con el acta correspondiente de matrimonio anterior con la debida nota marginal de la disolución del vínculo anterior.</p>
							<p>En caso de viudez o ausencia con presunción de fallecimiento, deberá presentar el acta de matrimonio anterior y el acta de defunción de su cónyuge.</p>
							<p>El día de la ceremonia se entregará la libreta de familia en forma Gratuita.</p>
							<p>Deberán declarar si han celebrado o no convención matrimonial, en caso afirmativo, presentando en la delegación original y copia certificada por escribano.</p>
							<p>Podrán optar por régimen de separación de bienes o comunidad de ganancias, de no efectuar la opción rige este último.</p>
							<h5>Lugar de trámite</h5>
							<p>En la delegación correspondiente al domicilio de cualquiera de los contrayentes, con domicilio en la Provincia de Buenos Aires.</p>
							<p>En caso de peligro de muerte se trasladarán los Protocolos al lugar indicado.</p>
							<h5>Plazo</h5>
							<p>En el momento.</p>
							<h5>Tasa</h5>
							<p>Libreta familiar: primera vez gratis, duplicado: $ 78, triplicado y siguientes: $ 117</p>
							<p>Matrimonio $ 58,00</p>
							<h5>Aclaraciones</h5>
							<p>No hay inconvenientes con la presentación de testigos que tengan domicilio en otra provincia. No pueden tener domicilio en otro país</p>
							<p>"Entrega de libreta a domicilio" En caso de requerir la presencia del funcionario público en otro lugar distinto a la delegación, se deberá abonar la tasa provincial que corresponda. El pedido se formulará en el mismo sitio donde se solicita el turno y se hará entrega en el domicilio indicado por los solicitantes.

</p>
							<p>En el caso arriba mencionado, el acto es simbólico. Los novios gestionarán en la delegación correspondiente, toda la estructura legal para celebrar el matrimonio.</p>
						</div>
						<div role="tabpanel" class="tab-pane" id="profile">
							ACA VA LO OTRO JAVI
						</div>
					</div>
										
				</div><!-- .requisitos_wrapper -->
				<div class="mas_info_ceremonias">
					<div class="gum_40">
						<h2 class="title_sep gum_30">Atención al Público</h2>
						<p><i class="fa fa-calendar"></i>Lunes a Viernes de 8 a 16hs</p>
						<p><i class="fa fa-phone"></i>0800-999-6304</p>
						<p><i class="fa fa-envelope"></i><a href="">cqr@jg.gba.gov.ar</a></p>
						<span class="block clear"></span>
					</div>
					<p>Encontrá la <strong>delegación para matrimonios</strong> según tu municipio haciendo click en el siguiente botón.</p>
					<a href="http://www.gob.gba.gov.ar/portal/registro/busca_deleg.php" target="_blank" class="btn btn-default gum_50">Ir a buscador</a>
				</div><!-- .mas_info_bsas -->
				
			</div><!-- .bsas.datos_utiles_cont -->
			
			<div class="santa_fe datos_utiles_cont">
				<div class="datos_utiles_wrapper">
					<div>
						<span class="icon valign"></span>
						<h4>¿Cuando hay que reservar Fecha?</h4>
						<p>Los futuros contrayentes deberán presentarse en el Registro Civil con un plazo prudencial de entre 15 y 30 días.</p>
						
					</div>
					
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>¿Quién puede solicitar el turno?</h4>
						<p>Uno o ambos contrayentes.</p>
					</div>
					
					<div>
						<span class="icon ic_3 valign"></span>
						<h4>¿Cuántos testigos son necesarios y obligatorios?</h4>
						<p>Sólo podrán ser testigos de matrimonio, las personas mayores de 21 años; ó, siendo menores de edad, los casados.</p>
						<p>Necesarios: Dos (2)<br />Innecesarios Hasta cuatro (4) adicionales, debiendo abonar por cada uno de ellos $100.</p>
						<p>Los impedimentos para ser testigos y/ o para contraer matrimonio surgen del Código Civil Argentino.</p>
					</div>
					
					<div>
						<span class="icon ic_4 valign"></span>
						<h4>¿Cuánto sale el trámite del matrimonio por civil?</h4>
						<p>Libreta de Matrimonio: $10,00<br />Celebración de matrimonio en oficina en hora y día hábil: $10,00<br />Celebración de matrimonio en oficina en hora y día inhábil: $333,20<br />Celebración de matrimonio en oficina en hora y día inhábil: $333,20<br />Primer Acta de Matrimonio sin costo. Otras copias: $7,50.</p>
					</div>
					
					<div>
						<span class="icon ic_6 valign"></span>
						<h4>¿Qué registro civil les corresponde?</h4>
						<p>Es el Registro Civil correspondiente al domicilio que figura en el DNI de cualquiera de los dos contrayentes.</p>
					</div>
				</div><!-- .datos_utiles_wrapper -->
				
				<div class="requisitos_wrapper">
					<!-- Nav tabs -->
					<ul class="solapas_datos_utiles" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span>1</span>Requisitos</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<h5>Si son solteros y mayores de edad</h5>
							<p>Deberán presentarse en el Registro Civil que le corresponda por domicilio o localidad, con un plazo prudencial de 15 quince días antes del fijado para el matrimonio.<br />
Deberán firmar una declaración jurada donde consten sus datos personales, el domicilio, lugar y fecha del nacimientos. Los nombres y apellidos, nacionalidad, profesión y domicilio y sus documentos de identidad.<br />Se les hará entrega de la orden pertinente para que obtengan los Certificados Prenupciales.</p>
							<h5>Divorciados</h5>
							<p>Deberán presentar el Acta de Matrimonio anterior con la marginal de Divorcio Vincular</p>
							<h5>Viudos</h5>
							<p>Deberán presentar las Actas de Matrimonio anterior y de Defunción del cónyuge fallecido.</p>
							<h5>Matrimonio de Menores </h5>
							<p><strong>Mujer mayor de 16 años y varón mayor de 18 años: </strong>Los Requisitos son iguales que para los adultos, pero deberán presentar las Actas de Nacimientos. Deberán estar acompañados de sus progenitores a los efectos de que presten su CONSENTIMIENTO para la celebración del matrimonio en el momento del acto. En el caso de que uno o ambos progenitores del/ la menor hubiere fallecido, deberá/ n acompañar el/ las Acta/ s de Defunción/ es y tramitar la Venia Supletoria de la Justicia.</p>
							<p><strong>Cuando uno ó ambos sean menores de 16 y 18 años: </strong>Los requisitos son iguales a los mencionados en el punto anterior, pero deberán tramitar la Dispensa Judicial por ante el Juez de 1era. Instancia que corresponda. Obtenida la misma, deberán presentarse con los padres para el debido CONSENTIMIENTO para la celebración del acto del matrimonio. </p>
							<h5>Matrimonio en el que uno o ambos de los futuros contrayentes se encuentra imposibilitado por cuestión de salud, de trasladarse a la Oficina que le corresponda</h5>
							<p>Los requisitos son los mismos a los puntos antes descriptos, debiendo acompañar el Certificado Médico que exprese la imposibilidad de que se trate.</p>
							<h5>Matrimonio en el que uno de los futuros contrayentes esté en peligro de muerte:</h5>
							<p>Deberá acreditarse esta situación con Certificado de Médico tratante.</p>
							<p>Los dos últimos casos son los ÚNICOS legalmente permitidos para realizar la celebración del Matrimonio fuera de la Oficina. En los demás, sólo se podrán realizar en la Oficina del Registro Civil.</p>
	
							<p>Los impedimentos para ser testigos y/ o para contraer matrimonio surgen del Código Civil Argentino.</p>
							<h5>Análisis prenupciales</h5>
							<p>Los Certificados Prenupciales deben ser extendidos siempre por Médicos Oficiales y su validez es de siete(7) días desde la expedición de los mismos.</p>
							<h5>La publicación del acto de matrimonio</h5>
							<p>Una vez completada la declaración jurada el Registro Civil deberá dar cumplimiento con la publicación previa, a los fines de la oposición que pudiere deducirse</p>
							<h5>Días y horarios para la celebración del matrimonio civil</h5>
							<p>Los matrimonios se celebran todos los días hábiles de 7:30 a 12:30 horas.
El arancel de $333,20 se abona cuando la celebración se efectúa los jueves y viernes hábiles de 17 a 20 horas en la Oficina.</p>
							<h5>Celebración del matrimonio civil fuera del registro</h5>
							<p>Si los novios quisieran casarse fuera del registro civil, los días para hacerlo son los viernes y sábados en dos turnos: Mañana de 10 a 14 hs y Tarde de 18 a 22 hs. El costo para lo mismo es de $1500.-<br />Los requisitos son los anteriormente descriptos.</p>
						</div>									
					</div><!-- .tab-content -->
				</div><!-- .requisitos_wrapper -->		
				
			</div><!-- .santa_fe.datos_utiles_cont -->
			
			<div class="rosario datos_utiles_cont">
				<div class="datos_utiles_wrapper">
					<div>
						<span class="icon valign"></span>
						<h4>¿Cuando hay que reservar Fecha?</h4>
						<p>Los turnos se deben solicitar una semana antes a la semana de enlace. Concurrir los días Lunes.</p>
						<p>Si el enlace se realiza en horarios vespertinos (Jueves y Viernes de 17 a 20 hs.), se debe solicitar el turno 15 días antes de la fecha de enlace excepto para la oficina de "Villa Hortensia".</p>
						<p>Para la Oficina de "Villa Hortensia", los turnos deben solicitarse 20 días antes a la semana de enlace.</p>
						<p>Si el enlace se realiza fuera de la Oficina Seccional se debe solicitar el turno 30 días antes de la fecha de enlace. Turnos sujetos a disponbilidad.</p>
					</div>
					
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>¿Quién puede solicitar el turno?</h4>
						<p>Uno o ambos contrayentes, salvo cuando el enlace se realiza fuera de la oficina seccional donde deberán concurrir ambos contrayentes.</p>
					</div>
					
					<div>
						<span class="icon ic_3 valign"></span>
						<h4>¿Cuántos testigos son necesarios y obligatorios?</h4>
						<p>Necesarios: Dos (2) mayores de 21 años. Deben ser "Testigos de conocimiento", es decir, que conozcan a los novios que puedan constatar que los novios gozan de aptitud nupcial.</p>
						<p>Innecesarios Hasta cuatro (4) adicionales, sin necesidad de acreditar domicilio en la Ciudad de Rosario (abona $30 cada uno).</p>
					</div>
					
					<div>
						<span class="icon ic_4 valign"></span>
						<h4>¿Cuánto sale el trámite del matrimonio por civil?</h4>
						<p>La celebración del matrimonio en horario matutino con dos testigos necesarios tiene un costo de $10 en cualquier Registro Civil de Rosario. El costo por cada testigo adicional es de $100.</p>
						<p>La celebración del matrimonio en horario vespertino se realiza los días Jueves y Viernes y se deberá abonar un adicional de $ 333,20. Los horarios de atención vespertinos varían de acuerdo al Registro Civil:</p>
						<p>- Villa Hortensia: Horario de 17:00 a 19:45 hs.<br />
- Calle Brown 2261: Horario de 17:00 a 19:45 hs.<br />
- Felipe Moré: Horario de 17:00 a 18:00 hs.<br />
- Calle Juan Manuel de Rosas 847: Horario de 17:00 a 18:00 hs.<br />
- Rosa Ziperovich: Horario de 17:00 a 19:00 hs.</p>
				
					</div>
					
					<div>
						<span class="icon ic_6 valign"></span>
						<h4>¿Qué registro civil les corresponde?</h4>
						<p>El Registro Civil correspondiente al domicilio que figura en el DNI de cualquiera de los dos contrayentes.</p>
					</div>
				</div><!-- .datos_utiles_wrapper -->
				
				<div class="requisitos_wrapper">
					<!-- Nav tabs -->
					<ul class="solapas_datos_utiles" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span>1</span>Requisitos</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<h5>Si son solteros y mayores de edad</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia.</p>
							
							<h5>Si son menores de 21 años de edad (solteros)</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia. Partida de nacimiento de los menores contrayentes. Documentos de los padres.</p>
							
							<h5>Si son mayores de 21 años de edad (solteros)</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia.</p>
														
							
							<h5>Divorciados</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia. Partida de matrimonio anterior con marginal de divorcio vincular.</p>
							<h5>Viudos</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia. Partida de defunción del anterior cónyuge y libreta de matrimonio anterior</p>
													
							<h5>Análisis prenupciales</h5>
							<p>Deben realizarse en Hospital Público y tienen una vigencia de 7 días.<br />El hospital es asignado por el Registro Civil. Los novios deben hacerse análisis de sangre. Requisitos: llevar documento.<br /> Se recomienda llevar jeringas descartables y a las 48 hs pueden retirarse.</p>
							
							<h4>Matrimonio a domicilio</h4>
							<p>Este trámite le permite solicitar turno ante el Registro Civil al solo efecto de celebrar matrimonios dentro y fuera del asiento fijo de la Oficina Seccional que por domicilio corresponda, en horarios y días hábiles e inhábiles.</p>
							
							<h5>¿Quién puede realizarlo?</h5>
							<p>Uno de los contrayentes.</p>
							
							<h5>Si son menores de 21 años de edad (solteros)</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia. Partida de nacimiento de los menores contrayentes. Documentos de los padres.</p>
							
							<h5>Si son mayores de 21 años de edad (solteros)</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia.</p>
														
							
							<h5>Divorciados</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia. Partida de matrimonio anterior con marginal de divorcio vincular.</p>
							<h5>Viudos</h5>
							<p>DNI (original y fotocopia de 1º y 2º página) de ambos contrayentes y último cambio de domicilio de la novia. Partida de defunción del anterior cónyuge y libreta de matrimonio anterior</p>
							
							<h5>¿Cuándo pueden celebrarse estos matrimonios?</h5>
							<p>Los horarios son de lunes a Viernes de 14:00 a 21:00 hs y los sábados de 10:00 a 14:00 y de 17:00 a 22:00 hs.
No se podrán celebrar matrimonios, de acuerdo a la Ley 12.229, los días domingos, feriados nacionales y fiestas provinciales.</p>	

							<h5>¿Cuál es el costo por este trámite?</h5>
							<p>El costo es de $ 1500.</p>			
						</div>	
														
					</div><!-- .tab-content -->
				</div><!-- .requisitos_wrapper -->
			</div><!-- .rosario.datos_utiles_cont -->
			
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php $registros = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->	
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>