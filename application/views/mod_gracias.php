<?php if(isset($gracias) && $gracias){ ?>
	<div id="gracias" class="alert alert-success alert-dismissible" role="alert">
		<div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h2><strong>Gracias</strong>, tu consulta ya ha sido enviada!</h2>
			<p class="bajada">El formulario se completó correctamente y tus datos ya fueron enviados <?=empty($no_empresas)?'a la/s empresa/as seleccionadas':'';?>. En breve recibirás novedades.</p>			
			<?php if(empty($no_empresas)){ ?>
				<div class="listado">
					<h3 id="segui_navegando">¡Seguí Navegando!</h3>
					<?php if(isset($rubros_gracias) && $rubros_gracias) foreach($rubros_gracias as $key => $rubro_gracias){ ?>
						<a class="box" href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro_gracias['seo_rubro'] . '_CO_r' . $rubro_gracias["id_rubro"]); ?>">
							<img <?php echo 'src="' . base_url('/assets/images/guia/' . $rubro_gracias["imagen"]) .'"';?> />
							<h3><strong class="valign"><?php echo $rubro_gracias['rubro']; ?></strong></h3>
							<span>Ver empresas</span>
						</a>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>	
<?php } ?>
<script type="text/javascript">
dataLayer.push({
'event':'VirtualPageview',
'virtualPageURL':'<?php echo $_SERVER["REQUEST_URI"]; ?>',
'virtualPageTitle' : 'Pantalla Gracias'
});
</script>

<?php /*
<!-- Google Code for pedir presupuesto Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */

/*
var google_conversion_id = 1064078652;
var google_conversion_language = "es";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "qIgLCJCwYxC8mrL7Aw";
var google_conversion_value = 1.000000;
var google_remarketing_only = false;
*/

/* ]]>
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1064078652/?value=1.000000&amp;label=qIgLCJCwYxC8mrL7Aw&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

{if $nro_trackeo == 3}
<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6005519541275';
fb_param.value = '0.00';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = (location.protocol=='http:'?'http':'https')+'://connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6005519541275&amp;value=0" /></noscript>
{/if} */ ?>