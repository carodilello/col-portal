<?php
$bodyclass = 'page mapa_sitio';
include('header.php');

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>
<div class="container">
	<?php if(isset($banners[325]) && $banners[325] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[325]) && $banners[325] ? $banners[325] : '<p>Banner Accesorias Top 970x90</p>'; ?></div>
	<?php } ?>
	<h1 class="title_sep gum_60">Mapa del Sitio de
		<span class="dropdown">
			<a class="green suc_selector" id="sucursales-nav-label-empresas-destacadas" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
			<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label-empresas-destacadas">
				<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
					<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>"><?=$suc["sucursal"];?></a>
				<?php } ?>
			</ul>
		</span>
	</h1>
		
	<div id="split">
		<section id="primary">
			<table class="tabla_la_fiesta">
				<?php
				$i = 0;
				foreach ($nav as $k => $n) { 
					if(!in_array($n['padre']['nombre'], array('EVENTOS', 'NOTAS'))){ ?>
						<tr>
							<?php if($i == 0){ ?>
								<td rowspan="7" class="uno"><h2>Guia de Empresas</h2></td> 
							<?php } ?>
							<td class="dos"><h3><?php echo $n['padre']['nombre']; ?></h3></td>
							<td class="tres">
								<?php if(isset($n['hijos']) && $n['hijos']) foreach ($n['hijos'] as $k => $h) { ?>
									<a href="<?php echo $h['nombre_url'] ? base_url('/' . $sucursal['nombre_seo'] . '/proveedor/' . $h['nombre_url'] . '_CO_r' . $h['id_rubro']) : base_url('/proveedores/listado_empresas/' . $h["id_rubro"]); ?>"><?php echo $h['nombre']; ?></a>
								<?php } ?>
							</td>
						</tr>
					<?php }
					$i += 1;
				} ?>
				<tr>
					<td class="dos"><h3>LISTADO</h3></td>
					<td class="tres">
						<a href="<?php echo base_url('/' . $sucursal['nombre_seo'] . '/promociones_CO_pr'); ?>">Promociones</a>
						<a href="<?php echo base_url('/' . $sucursal['nombre_seo'] . '/productos_CO_pd'); ?>">Productos</a>
						<a href="<?php echo base_url('/' . $sucursal['nombre_seo'] . '/paquetes_CO_pa'); ?>">Paquetes</a>
					</td>
				</tr>
			</table>	
		 	
		 	<table>	
				<tr>
					<td rowspan="2" class="uno"><h2>Notas</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('/consejos'); ?>">Notas y Actualidad</a>
						<a href="<?php echo base_url('/consejos/vestidos_CO_cn76'); ?>">Vestidos</a>
						<a href="<?php echo base_url('/consejos/belleza-y-estilo_CO_cn81'); ?>">Belleza y Estilo</a>
						<a href="<?php echo base_url('/consejos/la-organizacion_CO_cn93'); ?>">La Organización</a>
						
					</td>
					<td class="tres">
						<a href="<?php echo base_url('/consejos/luna-de-miel_CO_cn98'); ?>">Luna de Miel</a>
						<a href="<?php echo base_url('/consejos/casamientos-reales_CO_cn1055'); ?>">Casamientos Reales</a>
						<a href="<?php echo base_url('/listado-de-consejos'); ?>">Todas las Notas</a>
					</td>
				</tr>
			
			</table>
		 	
		 	
		 	<table>	
				<tr>
					<td rowspan="2" class="uno"><h2>Casamientos Tv</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('/casamientos-tv/vestidos_CO_ct101'); ?>">Vestidos</a>
						<a href="<?php echo base_url('/casamientos-tv/trajes_CO_ct102'); ?>">Trajes</a>
						<a href="<?php echo base_url('/casamientos-tv/belleza_CO_ct100'); ?>">Belleza</a>
						<a href="<?php echo base_url('/casamientos-tv/la-fiesta_CO_ct104'); ?>">La Fiesta</a>
						<a href="<?php echo base_url('/casamientos-tv/salones_CO_ct103'); ?>">Salones</a>
					</td>
					<td class="tres">
						<a href="<?php echo base_url('/casamientos-tv/lista-de-regalos_CO_ct105'); ?>">Lista de Regalos</a>
						<a href="<?php echo base_url('/casamientos-tv/noche-de-bodas_CO_ct360'); ?>">Noche de Bodas</a>
						<a href="<?php echo base_url('/casamientos-tv/luna-de-miel_CO_ct106'); ?>">Luna de Miel</a>
						<a href="<?php echo base_url('/casamientos-tv/civiles_CO_ct107'); ?>">Civiles</a>
						<a href="<?php echo base_url('/casamientos-tv/moda-y-actualidad_CO_ct108'); ?>">Moda y Actualidad</a>
					</td>
				</tr>
			
			</table>
			
			<table>	
				<tr>
					<td rowspan="2" class="uno"><h2>Galerías</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('fotos-casamiento'); ?>">Galerías de Empresas</a>
					</td>
					<td class="tres">
						<a href="<?php echo base_url('fotos-casamiento'); ?>">Galerías Notas</a>
					</td>
				</tr>
			
			</table>

			<table>	
				<tr>	
					<td rowspan="2" class="uno"><h2>Ceremonia y Civil</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a>
						<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina'); ?>">Registros Civiles</a>
						<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-iglesias-catolicas-en-argentina'); ?>">Iglesias Católicas</a>
						<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-templos-judios-en-argentina'); ?>">Templos Judíos</a>
						
						
					</td>
					<td class="tres">
						<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-ceremonias-no-religiosas'); ?>">Ceremonias no Religiosas</a>
						<a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-registros-civiles-en-argentina'); ?>">Directorio de Registros Civiles</a>
						<a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-iglesias-catolicas-en-argentina'); ?>">Directorio de Iglesias Católicas</a>
						<a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-templos-judios-en-argentina'); ?>">Directorio de Templos Judíos</a>
					</td>
					
				</tr>
			</table>
			
			<table>
				<tr>
					<td rowspan="2" class="uno"><h2>Medios y Eventos</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('/eventos'); ?>">Medios y Eventos</a>
						<a href="<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>">Jornadas</a>
						<a href="<?php echo base_url('eventos/te-de-novias-en-buenos-aires'); ?>">Té de Novias BsAs</a>
					</td>
					<td class="tres">
						<a href="<?php echo base_url('/eventos/te-de-novias-en-cordoba'); ?>">Té de Novias Córdoba</a>
						<a href="<?php echo base_url('/eventos/te-de-novias-en-rosario'); ?>">Té de Novias rosario</a>
						<a href="<?php echo base_url('/empresas/expo-novias'); ?>">Participar de la Expo con tu empresa</a>
					</td>
				</tr>
								
			</table>
			
			<table>	
				<tr>
					<td rowspan="2" class="uno"><h2>Infonovias</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('/infonovias'); ?>">Infonovias</a>
					</td>
					<td class="tres">
						<a href="<?php echo base_url('/infonovias/registro'); ?>">Infonovias Registro</a>
					</td>
				</tr>
			</table>
			
			<table>
				<tr>
					<td rowspan="2" class="uno"><h2>Institucional</h2></td>
					<td class="dos">
						<a href="<?php echo base_url('/empresas/nuestra-historia'); ?>">Quienes Somos</a>
						<a href="<?php echo base_url('/empresas/como-anunciar'); ?>">Cómo publicar</a>
						<a href="<?php echo base_url('/terminos-y-condiciones'); ?>">Términos y condiciones</a>
					</td>
					<td class="tres">
						<a href="<?php echo base_url('/politicas-de-privacidad'); ?>">Políticas de privacidad</a>
						<a href="<?php echo base_url('/contacto'); ?>">Contacto</a>
					</td>
				</tr>
			</table>				
		</section><!-- #primary -->
		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
			<?php if(isset($banners[326]) && $banners[326] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[326]) && $banners[326] ? $banners[326] : '<p>Banner Accesorias 300x250<span>Posición 1</span></p>'; ?></div>
			<?php }
			if(isset($banners[327]) && $banners[327] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[327]) && $banners[327] ? $banners[327] : '<p>Banner Accesorias 300x250<span>Posición 2</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #secondary -->	
	</div><!-- #split -->
</div><!-- .container -->
<div id="wrapper_listado_proveedores_interna">
	<?php include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php include('footer.php'); ?>