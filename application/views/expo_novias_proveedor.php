<?php
$bodyclass = 'expo_novias_proveedor site_proveedor';

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
);

include('header_site_proveedor.php'); ?>
<div id="cont_video">
	<video id="backgroundvid" autoplay loop="true">
		<source src="<?php echo base_url('/assets/images/expo_proveedor/video.mp4'); ?>" type='video/mp4; codecs="avc1.4D401E, mp4a.40.2"'>
	</video>
	
	<div id="logo_jornadas"></div>
</div><!-- #cont_video -->

<div id="banda_fecha">
	<h2><strong>27 DE SEPTIEMBRE 2017</strong>JORNADA 41º</h2>
	<span id="anchor_top"></span>
</div>


<div class="container">
	<div class="row">
		<div id="intro" class="col-md-8 col-xs-7">
			
	    	<h1>Reservá tu stand</h1>
			<p class="bajada">Y participá de la exclusiva y única expo de Novias del Año</p>
			
			<p class="big_txt">Dejanos tus datos y una ejecutiva de cuentas se contactará a la brevedad para brindarte toda la info necesaria para participar de la expo con tu espacio.</p>
			<p><strong>Con 40 exitosas Jornadas organizadas,</strong> Casamientos Online nuevamente pone en práctica su alto poder de convocatoria y su experiencia en el desarrollo de eventos, con el objeto de citar en un solo lugar, a novios y empresas del mercado de los casamientos.</p>
			<h2>¿Qúe es la Jornada?</h2>
			<p>Es la <strong>expo para Novias más importante del país</strong> y en la que participan las empresas líderes del mercado del evento, lo que jerarquizará su empresa.</p>
			
			<img src="<?php echo base_url('/assets/images/expo_proveedor/novios.jpg'); ?>" />
			
		</div><!-- #intro -->
	
		<div id="form_wrapper" class="col-md-4 col-xs-5	">
			<?php echo form_open(base_url('/empresas/expo-novias#gracias_exponovia'), 'method="POST" role="form" class="form"'); ?>
		    	<div id="gracias_exponovia" class="dialog_corner"></div>

		    	<?php if($success){ ?>
					<div id="gracias_como_anunciar">
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<p><i class="fa fa-check-circle-o"></i><span><strong>Gracias por contactarte!</strong> Nos estaremos comunicando a la brevedad.</span></p>
						</div>
					</div><!-- #gracias -->	
				<?php }elseif(validation_errors()){ ?>
					<div class="errores-generales">	
						<div><i class="fa fa-exclamation-circle"></i> 
							<?php echo validation_errors(); ?>
						</div>	
					</div> <!-- .errores-generales -->	
				<?php }else{ ?>
					<h3>SOLICITAR INFORMACIÓN DE LA JORNADA</h3>
				<?php } ?>
		        
		        <div class="form-group has-feedback">
					<input type="text" name="nombre" required placeholder="Nombre" value="" class="form-control" />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
				
				 <div class="form-group has-feedback">
					<input type="text" name="apellido" required placeholder="Apellido" value="" class="form-control" />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
				
				 <div class="form-group has-feedback">
					<input type="text" name="telefono" required placeholder="Teléfono" value="" class="form-control" pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
				
				<div class="form-group has-feedback">
					<input type="email" name="email" required placeholder="Email" value="" class="form-control" />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
				
				<div class="form-group has-feedback">
					<input type="text" name="empresa" required placeholder="Empresa" value="" class="form-control" />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
				
				<div class="form-group has-feedback">
					<select name="rubros" required class="form-control">
						<option value="">Rubro</option>
						<option value="Agencias de Viaje y Turismo">Agencias de Viaje y Turismo</option>
						<option value="Ajuar">Ajuar</option>
						<option value="Alianzas">Alianzas</option>
						<option value="Alquiler de carpas">Alquiler de carpas</option>
						<option value="Alquiler de Livings y Equipamientos">Alquiler de Livings y Equipamientos</option>
						<option value="Ambientación y Centros de Mesa">Ambientación y Centros de Mesa</option>
						<option value="Bebidas y Barras de Tragos">Bebidas y Barras de Tragos</option>
						<option value="Catering">Catering</option>
						<option value="Ceremonias no tradicionales">Ceremonias no tradicionales</option>
						<option value=" Civil"> Civil</option>
						<option value="Cotillón">Cotillón</option>
						<option value="Despedida de Soltera">Despedida de Soltera</option>
						<option value="Disc Jockey">Disc Jockey</option>
						<option value="Foto y Video">Foto y Video</option>
						<option value="Listas de Regalos"> Listas de Regalos</option>
						<option value="Maquillaje">Maquillaje</option>
						<option value="Mesas Dulces y Cosas Ricas">Mesas Dulces y Cosas Ricas</option>
						<option value="Noche de Bodas">Noche de Bodas</option>
						<option value="Participaciones">Participaciones</option>
						<option value="Peinados">Peinados</option>
						<option value="Propuestas Originales">Propuestas Originales</option>
						<option value="Ramos, Tocados y Accesorios">Ramos, Tocados y Accesorios</option>
						<option value="Salones de Fiesta">Salones de Fiesta</option>
						<option value="Salones de Fiesta en Uruguay">Salones de Fiesta en Uruguay</option>
						<option value="Salones de Hoteles">Salones de Hoteles</option>
						<option value="Shows de Entretenimiento">Shows de Entretenimiento</option>
						<option value="Shows Musicales">Shows Musicales</option>
						<option value="Souvenirs">Souvenirs</option>
						<option value="Trajes de Etiqueta">Trajes de Etiqueta</option>
						<option value="Tratamientos de Belleza">Tratamientos de Belleza</option>
						<option value="Vestidos de Fiesta y Madrina">Vestidos de Fiesta y Madrina</option>
						<option value="Vestidos de Novia">Vestidos de Novia</option>
						<option value="Vestidos Usados y Terminados">Vestidos Usados y Terminados</option>
						<option value="Wedding Planners">Wedding Planners</option>
						<option value="Zapatos de Novias">Zapatos de Novias</option>
						<option value="Otros">Otros</option>
					</select>
					<div class="help-block with-errors"></div>
				</div>
				
				<div class="form-group has-feedback">
					<textarea required placeholder="Comentarios" name="comentario" class="form-control"></textarea>
					<div class="help-block with-errors"></div>
				</div>
				
				<input class="btn btn-default" type="submit" />
			</form>
		</div>
		<div class="clear"></div>
	</div><!-- .row -->	
</div><!-- .cointaer -->

<section id="foto_y_video">
	<div class="inner">
		<h2 class="title_sep">La jornada en fotos y video</h2>
		<div class="thumbs">
			<div class="row">
				<div class="col-md-8 col-xs-8 iframe_wrapper">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/PFMHWWr-p7c" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="col-md-4 col-xs-4">
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic4.jpg'); ?>" class="thumb_4" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_4.jpg'); ?>" /></a>
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic5.jpg'); ?>" class="thumb_5" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_5.jpg'); ?>" /></a>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-5 col-xs-5">
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic1.jpg'); ?>" class="thumb_1" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_1.jpg'); ?>" /></a>
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic2.jpg'); ?>" class="thumb_2" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_2.jpg'); ?>" /></a>
				</div>
				<div class="col-md-3 col-xs-3">
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic3.jpg'); ?>" class="thumb_3" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_3.jpg'); ?>" /></a>
				</div>
				<div class="col-md-4 col-xs-4">
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic6.jpg'); ?>" class="thumb_6" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_6.jpg'); ?>" /></a>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-8 col-xs-8">
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic7.jpg'); ?>" class="thumb_7" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_7.jpg'); ?>" /></a>	
				</div>
				<div class="col-md-4 col-xs-4">
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic8.jpg'); ?>" class="thumb_8" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_8.jpg'); ?>" /></a>
					<a href="<?php echo base_url('/assets/images/expo_proveedor/pic9.jpg'); ?>" class="thumb_9" data-gallery title="Expo novias proveedor"><img src="<?php echo base_url('/assets/images/expo_proveedor/thumb_9.jpg'); ?>" /></a>
				</div>
			</div>
		</div><!-- .thumbs -->

	</div><!-- .inner -->
</section><!-- #foto_y_video -->	


<section id="participar">
	<div class="inner">
		<h2 class="title_sep">¿Por qué participar en la Jornada?</h2>
		
		<div id="iconos_participar">
		
				<div class="row">
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_contacto_directo.png'); ?>" />
						</div>
						<div class="data">
							<p>Permite entrar en contacto directo con un número alto de potenciales clientes en un lapso muy corto</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_armado.png'); ?>" />
						</div>
						<div class="data">
							<p>El armado de los espacios es simple e implica una baja inversión con grandes posiblidades de repago. Consiste en un espacio con formáto predefinido. Le brindamos la estructura para el armado.</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_bar_chart.png'); ?>" />
						</div>
						<div class="data">
							<p>Hay cupos limitados por rubro, lo que eficientiza los resultados para cada empresa.</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_novios_gustos.png'); ?>" />
						</div>
						<div class="data">
							<p>Es el escenario ideal para conocer más a fondo la idiosincrasia de los novios, sus gustos y preferencias.</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_agenda.png'); ?>" />
						</div>
						<div class="data">
							<p>Porque puede armar su base de datos actualizada de cientos de novios reales, seleccionados por nuestro Contact Center.</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_productos.png'); ?>" />
						</div>
						<div class="data">
							<p>Es el ámbito ideal para exhibir y realizar lanzamientos de sus nuevos servicios y productos, ante un público específico de novias próximas a casarse.</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_marketing.png'); ?>" />
						</div>
						<div class="data">
							<p>Es una excelente oportunidad para realizar acciones de branding, fidelización y networking.</p>
						</div>
					</div><!-- .col-md-6 -->
					
					<div class="col-md-6 col-xs-6">
						<div class="img_wrapper">
							<img src="<?php echo base_url('/assets/images/icon_mercado.png'); ?>" />
						</div>
						<div class="data">
							<p>Es una forma directa de conocer los productos, servicios y tendencias del mercado en general (condiciones de oferta y demanda, etc).</p>
						</div>
					</div><!-- .col-md-6 -->
				</div>
		
		</div><!-- #iconos_participar -->
		<!-- <div id="slider_empresas" class="carousel fade box" data-ride="carousel" data-interval="2250"> -->
		<div id="slider_empresas" class="box">
			<p class="title_sep"><strong>Estas son algunas de las empresas que participaron de las Jornadas de Casamientos Online</strong></p>
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="<?php echo base_url('/assets/images/expo_proveedor/logos_empresas_1.jpg'); ?>">
				</div>
				<div class="item active">
					<img src="<?php echo base_url('/assets/images/expo_proveedor/logos_empresas_2.jpg'); ?>">
				</div>	
				<div class="item active">
					<img src="<?php echo base_url('/assets/images/expo_proveedor/logos_empresas_3.jpg'); ?>">
				</div>					
			</div>
		</div><!-- #slider_emrpesas -->
	</div><!-- .inner -->
</section><!-- #participar -->



<section id="testimonios_jornada">
	<div class="inner testimonios_container">
		<h2>Testimonios de Expositores</h2>
		<ul class="col-md-6">
		
			<li><strong>Rip Consulting/Secontur - Agencia de Viajes:</strong> Gracias por el apoyo! Salió todo perfecto! El primer paso,  era tener un stand atractivo, lo cumplimos! Estuvo lleno de gente y con consultas desde el primer momento! El segundo paso ya comenzó para el martes tengo  entrevistas agendadas, y el lunes termino de cargar los datos  para escribirle a los novios, hacer el sorteo y  enviarte los datos. Seguimos en contacto, un beso, Majo.</li>
			<li><strong>Flopi Diaz- Mesa Dulce:</strong> Hola Jose como estas!! Nos encanto participar! Muchas novias pasaron por nuestro stand, sacaron fotos y demas asi que muy contentas, esperamos ver resultados!<br />Ya unas 5 novias que nos conocieron en la expo nos contactaron para pedir cotizaciones puntuales e ir cerrando el pedido :D. Nos pareció super bien la organización en todo momento. La convocatoria fue grande, era lo que al menos yo imaginaba, no hay dudas! Un beso enorme y buen finde!</li>
			<li><strong> Silvia Giorgio - Vestidos de Novia:</strong> Hola Jose, me encanto participar en la expo y la cupula me parece que es mi lugar en la expo, para otras jornadas me gustaría exponer 2 vestidos. Tuve varias consultas, asi que muchas Gracias!!! Te mando un beso, Silvina</li>
			<li><strong>Strauss Musik:</strong> Fue muy buena la recepción de nuestras propuestas en la jornada, estoy recibiendo muchas consultas y ya cerré varias contrataciones. Y eso que todavía no comencé con el seguimiento. Me quedo una importante base de datos. </li>
			<li><strong>Laura Serra Novias:</strong> La verdad es fue una muy buena experiencia!!!! Muchísimas novias se acercaron, miraron, llevaron mis datos... y muchas de ellas ya están en contacto conmigo!<br />Gracias a uds por la buena onda!!!!!!</li>
			<li><strong>Dinucci Paisajismo:</strong> Quería agradecerles por haberme convocado para participar en la Jornada de ayer. La experiencia fue desde todos los puntos de vista, absolutamente positiva.<br />El trato por parte de ustedes, y de todo el personal de CO, fue excelente, y  me hicieron sentir super cómoda y bienvenida. Todas las chicas, en particular Caro, me ayudaron, y siempre estuvieron atentas a cualquier necesidad que tuviese. La organización tanto del armado, como de la jornada misma, fue impecable y sumamente recomendable. Realmente, fue una muy linda experiencia.</li>
			<li><strong>Outletbar:</strong> El resultado de la Jornada fue excelente. Muy parecido a la última, que fue la mejor de los últimos 3 años, así que impecable. En lo personal además sentí que en esta ocasión la dinámica y puesta de la Jornada estuvo muy por encima de las últimas ediciones, cosa que percibí fundamentalmente en la circulación, que fue mucho mas constante, ordenada y se notaba</li>
			<li><strong>Garbarino:</strong> Esta expo fue mucho más convocante que la expo del mes de Abril y la verdad que nos quedamos cortos con el material. Nos gustó mucho como quedo el stand..! y la verdad que tuvimos una muy buena recepción por parte de las parejas que se acercaron a consultar sobre el servicio.</li>
	        <li><strong>Cristovao & Martin:</strong> Muchas novias y novios pasaron por nuestro stand, así que estamos muy contentas.</li>
	        <li><strong>Divertite:</strong> Nos quedamos súper contentos con la respuesta que tuvimos de la gente. Esperemos que salga todo bien! Muy buena la organización por parte de Uds. y de la gente del centro cultural.Mil gracias por todo.</li>
	        <li><strong>Maria Grebol:</strong> Estuvo genial la jornada! Cada año mejor!! Es un placer ser parte!</li>
	        <li><strong>Te alquilo el freezer:</strong> Bueno de nada, la verdad que apostamos a que saliera todo bien y que viniera gente, pero quedamos sorprendidos de la convocatoria, mucha gente, buen publico, asi que para nosotros fue muy satisfactoria la participación. </li>
	        <li><strong>Date el Gusto:</strong>: Les quería agradecer nuevamente por el voto de confianza para que les hiciéramos la ambientación!! Como saben dejamos todo para que fuera un éxito y me dio una alegaría enorme saber que les gustó. Fue un placer trabajar con todo su equipo. La verdad que las felicito por tan lindo grupo de trabajo. Eso se refleja en todo lo que hacen.</li>
	        <li><strong>Baf:</strong> Sofi, primero gracias a vos porque estuviste en todo y nos ubicaste en un lugar ideal! Sos una genia y la verdad que la expo fue un golazooooo!!!!</li>
	        <li><strong>Bousquet Jacket:</strong>La verdad nos encanto como se dio todo, y estamos muy contentos. Esta vez nos organizamos mejor y lo notamos en el resultado (comprando con la jornada anterior). Realmente nos sentimos privilegiados, siendo uno de los pocos stands exclusivos para novios/padrinos/cortejos . La gente se acercaba justamente por eso y fascinada con el modelo para niños.Tenemos que destacar su atencion y colaboracion para con nosotros. Estuvieron en todo y se lo agradecemos, sobretodo a vos.Resumiendo: Estamos muy conformes y no queda nada mas que agradecerles, y sera hasta, la proxima.</li>
	        <li><strong>Gobierno de la Ciudad:</strong> Sofi muchísimas gracias por como nos atendieron todas ayer!</li>
	        <li><strong>BELLA DI BIANCO:</strong> : Salió todo divino y las chicas que pasaban por ahi quedaron encantadas. Gracias por tu asesoramiento, nos estamos viendo seguramente en otras jornadas!!</li>
	        <li><strong>PAULA DE LUCA:</strong> Quería agradecerte x la atención de ayer, la expo estuvo genial y ya empezó a dar sus frutos!!</li>
	        <li><strong>HIGOS Y BREVAS:</strong> Nosotros más que contentos con las jornadas, y más pensando que estuvimos a muy poco de no participar... Yo no me imaginaba que fuera tan multitudinario. La gente por suerte se acercó mucho al stand, al punto que en cierto momento nos desbordaban.</li>
	
	        <li><strong>LILIANA DI TOMMASO:</strong> La experiencia fue muy positiva, sobrepasó mis expectativas.... La afluencia de gente a mi espacio fue numerosísima.</li>
	        <li class="no_border"><span>PAOLA PASCUA:</strong> Ayer publiqué en la Fan Page, un agradecimiento para todo el equipo de Casamientos Online! Las jornadas estuvieron muy lindas</li>
		</ul>
	
		<ul class="col-md-6">
			
			<li><strong>Joyería Big Ben: </strong>Hola Lucia! Como te comentamos notamos la cantidad de gente que fue. Nosotras no paramos un minuto y si en algun momento salimos un segundo del stand no se podia caminar casi. Fue un flujo constante de gente desde las 18 hasta las 21.30 calculo. Y nos llamo mucho la atención que además de que la gente pasaba y chusmeaba, la mayoría se acercaba especialmente al stand a pedir presupuestos a averiguar y asesorarse.  Gracias por recibirnos y nos vemos la próxima. </li>
			<li><strong>Tiempo de Waltz: </strong>Hola Lucía, acá todo bien, por suerte, bueno, un poco hablamos antes de terminar la expo, primero, gracias por convocarnos a participar, fue nuestra primera experiencia y la verdad salimos muy contentos. Muchas novias y parejas, en nuestro caso las parejas preguntaban mas y mas decididos en su consulta, las novias por ahí eran mas de llevar tarjetas, pero en lineas generales fue todo muy bueno. Te mando un beso y gracias por todo.
	Vero y Diego</li>
			<li><strong>Gabris Chocolates: </strong>Hola Delfi!!! Primero ante todo quería agradecerte por el día de la jornada y agradecerles también a todo el Staff de Casamientos Online, muy atentos todos. La verdad que nos fuimos muy contentos, de diez todo... muchas novias interesadas por nuestro servicio, hoy justamente estuve llamando y hay varias interesadas. Muchas Gracias por todo! Te mando Beso!</li>
			<li><strong>Atelier Quintana: </strong>La expo fue una muy linda experiencia, nos sirvió para ir consolidando la marca.... ustedes son amorosas, me encanto el trato y la buena predisposicion.”</li>
			
			<li><strong>Mi regalito: </strong>El evento nos sorprendió, no imaginamos la cantidad de novias que vinieron al mismo tiempo, hizo que tuviéramos que cambiar la estrategia de atención ya que no dimos abasto con la toma de datos. En lo personal siento que fue un éxito, que dimos a conocer la marca y que el trato directo con las novias les dio confianza en el servicio qué brindamos.<br />Ahora viene lo más crítico, el trabajo pos evento... El tener que cargar y chequear los datos para lograr un contacto lo antes posible</li>
			<li><strong>Caro Alvarez:</strong> Debo agradecerles porque de verdad es para mí una mano grande. La jornada me encantó, creo que la que más me gusto desde que voy, muy ordenada, mucha gente, el pabellón que me tocó también estuvo buenísimo, no sé si porque es el primero después del ingreso pero tuve muchas mas visitas que en ocasiones anteriores. Así que gracias nuevamente.</li>
	        <li><strong>Fravega:</strong> Excelente el evento de ayer, y buen caudal de gente. Las quiero Felicitar por la organización!!</li>
			<li><strong>Llelle Argentina:</strong> Fue muy bien, saque buenos contactos y pude empezar a tener un buena base de datos.</li>
	        <li><strong>Flis eventos:</strong> Gracias por la buena onda. Ya me contactaron un par de personas y empresas, por lo que creemos que va a haber buena respuesta. Estamos contentos por los resultados!</li>
	        <li><strong>Lorena Toledo:</strong> Sólo tengo palabras de agradecimiento para vos y todo el equipo, siempre presentes, atentos y súper solidarios.GRACIAS!!!! y Felicitaciones, por todos los logros alcanzados, más que merecidos.</li>
	        <li><strong>MG ambientaciones:</strong> nos encanto participar nos sentimos súper cómodas.</li>
	        <li><strong>OMG PHOTOBOOTH-CABINAS:</strong> : la verdad que exelente!! La gente muy contenta!! Estos días no paramos de recibir mails. Como te comente en la jornada nos gustaría participar en la próxima!!</li>
	        <li><strong>YOLI MORALES-VESTIDOS DE NOVIAS:</strong> El evento nos gustó, como siempre. La verdad que la organización de Uds es impecable. En cuanto a las novias se mostraron interesadas en nuestro trabajo y eso es genial, veremos cuantas de todas podemos cerrar, ojalá sean muchas!!</li>
	        <li><strong>BA Booth:</strong> Cada vez más gente se acerca a la jornada.El año que viene estaremos con mas servicios y más cosas para ofrecerles a las novias.</li>
	        <li><strong>Festa:</strong> Por ahora recibí seis pedidos de presupuesto, y he subido likes en facebook! Tuvimos la posibilidad a través del sorteo de generar 500 contactos. Así que tenemos para trabajar!!!!</li>
	        <li><strong>Lorena Schwartzman:</strong> estuvo muy buena la jornada, muy buena convocatoria!!! esperemos que salgan varias clientas!!!</li>
	        <li><strong>DON GOURMET CATERING:</strong> La verdad que una muy linda y buena experiencia para contactarnos con clientes y colegas o proveedores. Pudimos trabajar con muchos datos y la gente parecía bastante interesada.</li>
	        <li><strong>ORKNEY JOYERIA:</strong> Gracias por ocuparte y preocuparte por nuestra experiencia! Queremos agradecerte toda la disposición que nos brindaste para que la Jornada fuera genial! Tuvimos consultas después de la Jornada y bueno, todo suma.</li>
	        <li><strong>BATUCADA ENTRERRIANA CAMBACUA:</strong> Gracias por estar en todos los detalles, la verdad que nos fuimos contentos esta vez y con muchas expectativas, por supuesto que el show sumó muchísimo, es nuestro fuerte, que lo vean, lo sientan, lo palpen. Seguimos en contacto y mil gracias de parte de todo el staff de Batucada Cambacuá!!</li>
	        <li class="no_border"><strong>MIN AGOSTINI:</strong> CHICAS MUCHISIMAS GRACIAS A USTEDES! ESTABA TODO MUY MUY LINDO!! FELCITACIONES POR EL EXITO!</li>
		</ul>
	</div><!-- .inner -->

	<a href="javascript:;" class="more ver_mas"><i class="fa fa-plus"></i> Ver más testimonios</a>

</section><!-- #testimonios -->
   		   		   		
<section id="numeros_jornada">
	<div class="inner">
		<div class="col-md-5">
			<strong>17 AÑOS JUNTOS</strong>
			<p>Llevamos 17 años de eventos exitosos. Durante el 2017 organizaremos nuestras Jornadas 40 y 41. Más de 120 empresas de distintos rubros.</p>
		</div>		
		
		<div class="col-md-2">
			<img src="<?php echo base_url('/assets/images/expo_proveedor/novia.png'); ?>" />
		</div>
		
		<div class="col-md-5">
			<strong>+3000 NOVIAS</strong>
			<p>Participan en cada jornada, y son seleccionadas entre unas 10.000 novias  de acuerdo a su fecha de casamiento, zona de referencia y cantidad de invitados.</p>
		</div>
		
		<div class="col-md-12">
			<a href="#anchor_top" class="btn btn-default">- CONSULTAR POR TU STAND<span class="mobile_none"> EN LA JORNADA</span> -</a>
		</div>
	</div><!-- .inner -->
</section><!-- #numeros_jornada -->

<section id="mapa_jornada">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.172318275371!2d-58.37641404886243!3d-34.59980388036458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8d53a5cf4f399e9c!2zR2FsZXLDrWFzIFBhY8OtZmljbw!5e0!3m2!1ses-419!2sar!4v1475863978866" frameborder="0" style="border:0" allowfullscreen></iframe>
</section><!-- #numeros_jornada -->

<div id="organiza">
	<div class="inner">
		<span>Organiza:</span>
		<img class="logo_col" src="<?php echo base_url('/assets/images/logo_col_2.png'); ?>" />
		<span>Sponsor:</span>
		<img src="<?php echo base_url('/assets/images/logo_grupo_sarapura.jpg'); ?>" />
	</div>
</div>

<?php
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
$flechas = TRUE;
include('popup_fotos.php');
include('footer_info.php'); ?>