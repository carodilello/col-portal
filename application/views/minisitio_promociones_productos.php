<?php
$bodyclass = 'page minisitio';
include('header.php');

$no_empresa = TRUE;

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		include('mod_header_minisitio.php'); ?>

		<section id="wrapper_minisitio" class="row">

			<div id="content_minisitio" class="col-md-8">
			
				<?php if($promociones){ ?>
					<div id="promociones_minisitio" class="box">
						<h2>Promociones de <?php echo $proveedor['proveedor']; ?></h2>
						<div class="box_inner">
							<?php
							$puntos_suspensivos = "";
							$puntos_suspensivos_aclaracion = "";
							$promocion_en_minisitio = TRUE;
							foreach ($promociones as $k => $producto){ 
								include('promocion.php');
								$puntos_suspensivos = "";
								$puntos_suspensivos_aclaracion = "";
							} ?>
						</div><!-- .box_inner -->
					</div><!-- #pp_minisitio -->
				<?php } ?>
				
				<?php if($productos){ ?>
					<div id="productos_minisitio" class="box">
						<h2>Productos de <?php echo $proveedor['proveedor']; ?></h2>
						<div class="box_inner">
							<?php
							$puntos_suspensivos = "";
							$puntos_suspensivos_aclaracion = "";
							$producto_en_minisitio = TRUE;
							foreach ($productos as $k => $producto){ 
								include('producto.php');
								$puntos_suspensivos = ""; 
							} ?>
						</div><!-- .box_inner -->	
					</div><!-- #pp_minisitio -->
				<?php } ?>

			</div><!-- #content_minisitio -->	

			<aside id="sidebar_minisitio" class="col-md-4">
				<?php 
				include('minisitio_acciones.php');
				include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->

			<div class="col-md-12">
				<?php include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->

		</section><!-- #wrapper_minisitio -->
		
	</div><!-- .container -->
	<?php 
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php include('footer.php'); ?>