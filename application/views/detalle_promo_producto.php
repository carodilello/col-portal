<?php
$bodyclass = 'page minisitio';
include('header.php');

if(isset($item['id_paquete']) && $item['id_paquete']){
	$tipo_prod = 'Paquete';
	$tipo_prod_para_url = 'paquete';
	$codigo = 'pa';
}elseif($item['promocion']){
	$tipo_prod = 'Promoción';
	$tipo_prod_para_url = 'promocion';
	$codigo = 'pr';
}else{
	$tipo_prod = 'Producto';
	$tipo_prod_para_url = 'producto';
	$codigo = 'pd';
}

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		if($item_vencido) include('mod_publicacion_no_disponible.php');
		
		$ubicacion_aux = $ubicacion;
		$ubicacion     = $ubicacion_header_minisitio;
		
		include('mod_header_minisitio.php');

		$ubicacion = $ubicacion_aux; ?>
		
		<section id="wrapper_minisitio" class="row">
		
			<div id="content_minisitio" class="col-md-8">
			
				<div id="pp_minisitio" class="box <?=isset($item['id_paquete'])&&$item['id_paquete']?'listado_paquetes':'';?>">
					<h2><?php echo $tipo_prod; ?> de <?php echo $item['proveedor']; ?></h2>
					<div class="box_inner">
						<?php
						$fecha_vencimiento = '';
						if(isset($item["fecha_vencimiento"]) && $item["fecha_vencimiento"]){
							$fv_tmp = explode("-",$item["fecha_vencimiento"]);
							$fecha_vencimiento = $fv_tmp[2]."/".$fv_tmp[1]."/".$fv_tmp[0];	
						} ?>
						<div class="box_listado no_padding">
							<?php if(!$item_vencido) : ?>
								<div class="precio">
									<?php
									if(!isset($item['id_paquete'])){
										if($item["precio"] != '0.00' && $item["precio"] != '$ 0,00' && $item["precio"] != 'U$S 0,00'){
											if(isset($item["precio_tipo"])&&$item["precio_tipo"]){ ?>
												<strong>$<?php echo $item["precio"]; ?></strong>
												<strong><?php echo $item["precio_tipo"]; ?></strong>
											<?php }else{ ?>
												<strong>
													<?php
														if(isset($item['moneda']) && $item["moneda"] == 'P') echo '$'; 
														if(isset($item['moneda']) && $item["moneda"] == 'D') echo 'U$S';
														if(isset($item['moneda']) && $item["moneda"] == 'E') echo '&euro;';
														echo number_format($item["precio"], 2, ",", "."); ?>
												</strong>
											<?php }
										}
									}else{ ?>
										<strong><?php echo $item["precio"]; ?></strong><span><?php echo $item["precio_tipo"]; ?></span>
									<?php }
									if(isset($item["porcentaje_descuento"]) && $item["porcentaje_descuento"] && (!$item["precio"] || $item["precio"] == '0.00')){ ?>
										<span><span><?php echo $item["porcentaje_descuento"]; ?></span>%OFF</span>
									<?php } ?>
								</div>
							<?php endif; ?>

							<div class="img_wrapper">

								<?php
								/* CAMBIADO EL 18-05 X ROMAN. Que de tener varias fotos el producto, en el popup puedan verse con als flechas. Cambie 
								$items por $itemsVarios que carga todas las fotos del producto y las recorre con el foreach
								if(in_array(get_headers('http://media.casamientosonline.com/images/'.$item["imagen"])[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>

									<a href="<?php echo 'http://media.casamientosonline.com/images/'.(str_replace('_chica', '_grande', $item["imagen"])); ?>" title='<?php echo $item["titulo"]; ?>' data-gallery class="image-link item"
										data-urlminisitio="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $item['rubro_seo']; ?>"
										data-url="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $item['rubro_seo'] . '/presupuesto-' . $tipo_prod_para_url . '_CO_r' . $item['id_rubro'] . '_' . $codigo . $item["id_producto"]; ?>">
		 

										<img <?php echo 'src="http://media.casamientosonline.com/images/'.(str_replace('_chica', '_grande', $item["imagen"])).'"';?> alt="<?php echo $item["titulo"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" />
									</a>

								<?php }else{ ?>
									<img <?php echo 'src="http://media.casamientosonline.com/logos/'.$item["logo"].'"';?> alt="<?php echo $item["titulo"]; ?>" />
								<?php } */?>
								<?php
								if(in_array(get_headers('http://media.casamientosonline.com/images/'.$item["imagen"])[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
								    	<?php foreach ($itemVarios as $k => $foto){?>
											<a href="<?php echo 'http://media.casamientosonline.com/images/'.(str_replace('_chica', '_grande', $foto["imagen"])); ?>" <?php if($k > 0){?>hidden<?php }?> 
												title='<?php echo $foto["titulo"]; ?>' data-gallery class="image-link item"
												data-urlminisitio="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $item['rubro_seo']; ?>"
												data-url="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $item['rubro_seo'] . '/presupuesto-' . $tipo_prod_para_url . '_CO_r' . $foto['id_rubro'] . '_' . $codigo . $foto["id_producto"]; ?>">
				 

												<img <?php echo 'src="http://media.casamientosonline.com/images/'.(str_replace('_chica', '_grande', $foto["imagen"])).'"';?> alt="<?php echo $foto["titulo"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" />
											</a>

						    	 	<?php } ?>
						    	 	<?php 
										if (COUNT($itemVarios)>1){
											$flechas = TRUE;
										}
										?>

								<?php }else{ ?>
									<img <?php echo 'src="http://media.casamientosonline.com/logos/'.$item["logo"].'"';?> alt="<?php echo $item["titulo"]; ?>" />
								<?php } ?>
							</div>
							<div class="info_wrapper detalle">
								<h3 class="detalle"><?php echo $item["titulo"]; ?></h3>

								<?php if(isset($item['id_paquete']) && $item['id_paquete']){ ?>
									<div class="caract_paquete">
										<?php if($item["invitados"]){ ?>
											<span><i class="fa fa-user"></i>Base <?php echo $item["invitados"]; ?> inv.</span>
										<?php } ?>
										<!--<span><i class="fa fa-calendar"></i>Viernes.</span>-->
										<?php if($item["zonas"]){ ?>
											<span><i class="fa fa-map-marker"></i><?php echo $item["zonas"]; ?></span>
										<?php } ?>
										<!--<span><i class="fa fa-clock-o"></i>Noche</span>-->
									</div><!-- .caract_paquete -->
								<?php } ?>

								<?php if(isset($item["subtitulo"]) && $item["subtitulo"]){ ?><p class="subtitulo"><?php echo $item["subtitulo"]; ?></p><?php } ?>

								<p class="proveedor">
									<?php if(!empty($item["porcentaje_descuento"]) && (!$item["precio"] || $item["precio"] = '0.00')){ ?>
										<span><i class="fa fa-percent"></i>Descuento</a></span>
										<?php }
										if(isset($item["regalo"]) && $item["regalo"]){ ?>
											<span><i class="fa fa-gift"></i>Regalo</span>
										<?php }
										if(isset($item["oferta"]) && $item["oferta"]){ ?>
											<span><i class="fa fa-tag"></i>Oferta</span>
									<?php } ?>
								</p>

								<div class="descripcion no_margin_b"><?php echo $item["contenido"]; ?></div>

								<?php if(isset($item['legal']) && $item['legal']){ ?>
									<p class="legales">Legales: <?php echo $item['legal']; ?></p>
								<?php } ?>

								<?php if(isset($fecha_vencimiento) && $fecha_vencimiento){ ?>
									<p class="fecha clear"><i class="fa fa-calendar"></i>Valido hasta: <strong><?php echo $fecha_vencimiento; ?></strong></p>
								<?php } ?>
								<?php if(($item["precio"] == '0.00' || $item["precio"] == '$ 0,00' || $item["precio"] == 'U$S 0,00') && !$item["aclaracion"]){ ?>
									<?php if($item["precio_viejo"]){ ?>
										<p class="data_adicional"><?php echo $item["precio_viejo"]; ?></p>
									<?php } ?>
								<?php } ?>
								
								<div class="bottom_actions detalle">
									<?php if(isset($item['rubros']) && $item['rubros']){
										$paquetes_rubros = explode('@',$item['rubros']); ?>
										<div class="inc_paquete">
											<strong>Rubros incluidos:</strong>
											<?php foreach ($paquetes_rubros as $rub) {
												$elem_rub = explode('*',$rub); ?>
												<span data-toggle="tooltip" data-placement="top" class="icon ic_<?php echo $ic[$elem_rub[0]]; ?>" title="<?php echo $elem_rub[1]; ?>"></span>
											<?php } ?>
										</div><!-- .inc_paquete -->
									<?php } ?>

									<?php if(isset($item["aclaracion"]) && $item["aclaracion"]){ ?>
										<p class="data_adicional"><?php echo $item["aclaracion"]; ?></p>
									<?php }
									
									if(!$item_vencido){ ?>
										<a href="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $item['rubro_seo'] . '/presupuesto-' . $tipo_prod_para_url . '_CO_r' . $item['id_rubro'] . '_' . $codigo . $item["id_producto"]; ?>" class="btn btn-default">Consultar</a>
									<?php } ?>
								</div><!-- .bottom_actions -->
							</div>
							
							<?php if(isset($item['id_paquete']) && $item['id_paquete']){ ?>
								<div class="caracteristicas_paquetes">
									<div class="gum_30">
										<?php if($item['valido_dias']){
											$arr_dias = explode(',', $item['valido_dias']);
											if(!empty($arr_dias)){
												if(isset($arr_dias[7]) && $arr_dias[7]){ ?>
													<p><i class="fa fa-calendar"></i><strong>Válido para todos los días</strong></p>
												<?php }else{
													$dias_string = ''; ?>
													<p><i class="fa fa-calendar"></i><strong>Válido para los días</strong>
													<?php foreach ($arr_dias as $key => $dia){
														if(!empty($dias[$dia])) $dias_string .= ucfirst($dias[$dia]) . ', ';
													}
													echo rtrim($dias_string, ', '); ?>
													</p>
												<?php }
											}
										}
										if($item['valido_turnos']){
											$arr_turnos = explode(',', $item['valido_turnos']);
											if(!empty($arr_turnos)){ 
												$turnos_string = ''; ?>
												<p><i class="fa fa-clock-o"></i><strong>Válido para los turnos</strong>
												<?php foreach ($arr_turnos as $key => $turno){
													$turnos_string .= ucfirst($turnos[$turno]) . ', ';
												}
												echo rtrim($turnos_string, ', '); ?>
												</p>
											<?php }
										} ?>
									</div>
									
									<?php if(!empty($caracteristicas)){
										if($item['rubro']){ ?>
											<div class="info_caracteristicas">
												<h3><span class="<?php echo $ic[$item['id_rubro']] ? ('icon ic_' . $ic[$item['id_rubro']]) : ''; ?>"></span><?php echo $item['rubro']; ?> (<?php echo trim($item['proveedor']); ?>)</h3>
												<?php $tmp_car = explode('`', $item['caracteristicas']);
												if(!empty($tmp_car)){ ?>
													<ul style="overflow:hidden;">
														<?php foreach ($tmp_car as $key => $value){ 
															$tmp_lv = explode('*', $value);
															if(isset($tmp_lv[0]) && isset($tmp_lv[1])){
																$label = $tmp_lv[0];
																$value = $tmp_lv[1]; ?>
																	<li><?php echo $label; ?>: <span><?php echo $value; ?></span></li>
															<?php }
															} ?>
													</ul>
												<?php } ?>
											</div>
										<?php }
										foreach ($caracteristicas as $key => $caracteristica){
											$tmp_cars = array();
											if($caracteristica['caracteristicas']) $tmp_cars = explode('`', $caracteristica['caracteristicas']); ?>
											<?php if($caracteristica['rubro'] || !empty($tmp_cars)){ ?>
												<div class="info_caracteristicas">
													<?php if($caracteristica['rubro']){ ?>
														<h3><span data-toggle="tooltip" data-placement="top" class="<?php echo isset($ic[$caracteristica['id_rubro']]) && $ic[$caracteristica['id_rubro']] ? ('icon ic_' . $ic[$caracteristica['id_rubro']]) : ''; ?>"></span><?php echo $caracteristica['rubro']; ?> (<?php echo trim($caracteristica['proveedor']); ?>)</h3>
													<?php }
													if(!empty($tmp_cars)){ ?>
														<ul style="overflow:hidden;">
															<?php foreach ($tmp_cars as $key => $val){
																$tmp_lv = explode('*', $val);
																$label = $tmp_lv[0];
																$value = $tmp_lv[1]; ?>
																	<li><?php echo $label; ?>: <span><?php echo $value; ?></span></li>
															<?php } ?>
														</ul>
													<?php } ?>
												</div>
											<?php }
										}
									} ?>
								</div><!-- .caracteristicas_banners -->
							<?php } ?>
						</div><!-- .box_listado -->
					</div>
				</div><!-- #pp_minisitio -->

				<?php
				if(count($promociones)){
					$no_empresa = TRUE; ?>
					<div id="mas_consultados">
						<?php if($ubicacion == 'promocion'){ ?>
							<div id="promociones_minisitio" class="box">
								<h2>Otros productos y promociones del minisitio<a href="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $rubro['url'] . '/productos-y-promos'; ?>" class="pull-right ver_todos"><i class="fa fa-plus-circle"></i>Ver todas las promos y productos</a></h2>
								<div class="box_inner">
									<?php
									$puntos_suspensivos = "";
									$puntos_suspensivos_aclaracion = "";
									$promocion_en_minisitio = TRUE;
									foreach ($promociones as $k => $producto){
										include('promocion.php');
										$puntos_suspensivos = "";
										$puntos_suspensivos_aclaracion = "";
									} ?>
								</div><!-- .box_inner -->
							</div><!-- #pp_minisitio -->
						<?php } ?>
					</div>
				<?php }elseif($ubicacion == 'promocion' && !$item_vencido){ ?>
					<a href="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $rubro['url'] . '/productos-y-promos'; ?>" class="btn btn-default pull-right ver_todos">Ver todas las promos y productos</a>
				<?php }

				if(count($productos)){
					$no_empresa = TRUE; ?>
					<div id="mas_consultados">
						<?php if($ubicacion == 'producto'){ ?>
							<div id="productos_minisitio" class="box">
								<h2>Otros productos y promociones del minisitio<a href="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $rubro['url'] . '/productos-y-promos'; ?>" class="mobile_none  pull-right ver_todos"><i class="fa fa-plus-circle"></i>Ver todas las promos y prductos</a></h2>
								<div class="box_inner">
									<?php
									$puntos_suspensivos = "";
									$puntos_suspensivos_aclaracion = "";
									$producto_en_minisitio = TRUE;
									foreach ($productos as $k => $producto){
										include('producto.php');
										$puntos_suspensivos = ""; 
									} ?>
								</div><!-- .box_inner -->	
							</div><!-- #pp_minisitio -->
						<?php } ?>
					</div>
				<?php }elseif($ubicacion == 'producto' && !$item_vencido){ ?>
					<a href="<?php echo str_replace('{?}', $item['subdominio'], $base_url_subdomain) . $rubro['url'] . '/productos-y-promos'; ?>" class="btn btn-default pull-right ver_todos">Ver todas las promos y productos</a>
				<?php } ?>
			</div><!-- #content_minisitio -->
			
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php if(!$prov_vencido) include('minisitio_acciones.php');
				
				if($item_vencido) $prov_vencido = TRUE;
				include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->
			
			<?php if(!$item_vencido) : ?>
				<div class="col-md-12">
					<?php include('mod_resumen_minisitio.php'); ?>
				</div><!-- .col-md-12 -->
			<?php endif; ?>
		</section><!-- #wrapper_minisitio -->
	</div><!-- .container -->
	<?php 
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>

<?php $no_presupuesto_foto = TRUE;

include('popup_fotos.php');
include('footer.php'); ?>