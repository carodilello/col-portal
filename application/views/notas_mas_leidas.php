<?php 
$puntos_suspensivos = '...';
if(!empty($mas_leidas)){ ?>
	<div id="listado_notas_sidebar">
		<h2>Las <span class="pink">+</span> leidas</h2>
		<?php foreach ($mas_leidas as $k => $nota_v){
			$tmp = explode('@', $nota_v['secciones']); ?>
			<a href="<?php echo base_url('/consejos/' . $nota_v['titulo_seo'] . '_CO_n' . $nota_v['id']); ?>">
				<span><?php echo $tmp[3]?$tmp[3]:$tmp[1]; ?></span>
				<h3><?php echo $nota_v['titulo']; ?></h3>
				<p><?php echo strlen($nota_v['copete'])<=120?$nota_v['copete']:(mb_substr($nota_v['copete'], 0, 120).$puntos_suspensivos); ?></p>
			</a>
		<?php } ?>
	</div><!-- #listado_notas_sidebar -->
<?php } ?>