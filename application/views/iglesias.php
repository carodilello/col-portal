<?php
$bodyclass = 'page ceremonias_detalle iglesias';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>

<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li>Iglesias</li>
	</ul>
	<h1 class="title_sep">Requisitos para Casarse por Iglesia en Argentina</h1>
	<img class="big_pic border_igle" src="<?php echo base_url('/assets/images/big_pic_iglesias_catolicas.jpg'); ?>" alt="Requisitos para Casarse por Iglesia en Argentina" />
	<div id="split">
		<section id="primary">
			<div id="filtro" class="relative">
				<h2>Datos útiles</h2>
				
			</div>
			<div class="caba datos_utiles_cont">
				<div class="datos_utiles_wrapper no_margin">
					<div>
						<span class="icon ic_7 valign"></span>
						<h4>¿Donde casarse?</h4>
						<p>Los casamientos cristianos se realizan únicamente en templos parroquiales. La ceremonia debe realizarse en la parroquia que le corresponde a la novia, es decir la más cercana a su domicilio.</p>
						<p><strong>Pase a otra parroquia:</strong> Si quieren cambiar de parroquia, tienen que solicitar el pase o permiso en la parroquia de origen.</p>
					</div>
					
					<div>
						<span class="icon ic_9 valign"></span>
						<h4>Requisitos</h4>
						<p>- Ser solteros para la iglesia. Ninguno de los novios puede tener otro matrimonio religioso anterior (salvo con nulidad del mismo).</p>
						<p>- Estar bautizados en la iglesia católica. Como comprobante deberán presentar la Fé de bautismo (si no la encuentran la pueden pedir en la parroquia que fueron bautizados).</p>
						<p>- Completar el expediente matrimonial y presentarse con los testigos para su firma.</p>
						<p>- Cumplir con el curso prematrimonial, que consiste en una serie de charlas grupales acerca del matrimonio cristiano y al finalizar el mismo la parroquia les extenderá un certificado que deberán presentar el día en que se celebre el casamiento.</p>
					</div>
					
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>Padrinos</h4>
						<p>Cualquier persona, independientemente de su relación con los novios y la religión que practique, puede ser padrino / madrina del casamiento.</p>
					</div>
					
					<div>
						<span class="icon ic_3 valign"></span>
						<h4>Testigos</h4>
						<p>Se necesitan dos testigos. Uno para la novia y uno para el novio. Los testigos tienen que ser mayores de 21 años, preferentemente no familiares. No deben cumplir con ningún requisito en particular, ni ser obligatoriamente de la misma religión.</p>
					</div>
					
					<div>
						<span class="icon ic_8 valign"></span>
						<h4>Distintas religiones</h4>
						<p>En el caso de que uno de los contrayentes no sea católico, la iglesia ofrece una ceremonia que se denomina "matrimonio mixto".</p>
					</div>
										
				</div><!-- .datos_utiles_wrapper -->
			</div><!-- .caba.datos_utiles_cont -->
									
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php $iglesias = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->	
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>