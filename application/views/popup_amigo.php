<!-- Modal -->
<div class="modal fade" id="enviar-amigo" tabindex="-1" role="dialog" aria-labelledby="enviar-amigo-title">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url('/ajax/enviar_amigo'); ?>" role="form" class="form ajax">
				<div class="modal-body">
						<h2>Enviar a un amigo</h2>
						<input type="hidden" name="enviar_amigo_url" id="enviar_amigo_url" value="<?php echo base_url($_SERVER['REQUEST_URI']); ?>" />
						<fieldset class="row">
							<div class="form-group col-md-12">
								<h3>Remitente</h3>
								<div class="form-group has-feedback">
									<input type="text" name="rtte_name" id="rtte_name" value="<?php echo isset($data_form)&&$data_form?$data_form['nombre']:''; ?>" class="form-control" required placeholder="Nombre" />    
								    <span class="fa form-control-feedback" aria-hidden="true"></span>
								    <div class="help-block with-errors"></div>
								</div>

								<div class="form-group has-feedback">
									<input type="hidden" name="rtte_apellido" id="rtte_apellido" value="<?php echo isset($data_form)&&$data_form['apellido']?$data_form['apellido']:'-'; ?>" />
								    <span class="fa form-control-feedback" aria-hidden="true"></span>
								    <div class="help-block with-errors"></div>
								</div>

								<div class="form-group has-feedback">
									<input type="email" name="rtte_email" id="rtte_email" value="<?php echo isset($data_form)&&$data_form?$data_form['email']:''; ?>" class="form-control" required placeholder="Email" />
								    <span class="fa form-control-feedback" aria-hidden="true"></span>
								    <div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="form-group col-md-12 no_margin">
								<h3>Destinatario</h3>
								<div class="form-group has-feedback">
									<input type="text" name="dest_name" class="form-control" id="dest_name" required placeholder="Nombre" value="" />
								    <span class="fa form-control-feedback" aria-hidden="true"></span>
								    <div class="help-block with-errors"></div>
								</div>

								<div class="form-group has-feedback">
									<input type="hidden" name="dest_apellido" id="dest_apellido" value="-" />
								    <span class="fa form-control-feedback" aria-hidden="true"></span>
								    <div class="help-block with-errors"></div>
								</div>

								<div class="form-group has-feedback">
									<input type="email" required name="dest_email" id="dest_email" placeholder="Email" class="form-control" value="" />
								    <span class="fa form-control-feedback" aria-hidden="true"></span>
								    <div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<textarea name="comentario" placeholder="Comentarios" class="form-control"></textarea>
							</div>
						</fieldset>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn-default submit" value="Enviar" />
				</div>
			</form>
		</div>
	</div>
</div>