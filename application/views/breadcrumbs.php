<ul id="breadcrumbs">
	<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
	<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo'] . '/fiestas-de-casamiento'); ?>">Guia de Empresas</a></li>
	<?php if((isset($minisitio)&&$minisitio) || (isset($ubicacion)&&$ubicacion)){ ?>
		<li><a href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url']. '_CO_r' . $proveedor["id_rubro"]); ?>"><?php echo isset($proveedor['rubro'])&&$proveedor['rubro']?$proveedor['rubro']:(isset($rubro['rubro'])&&$rubro['rubro']?$rubro['rubro']:''); ?></a></li>
		<?php if(isset($elemento)){ ?>
			<li><a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url']; ?>"><?php echo isset($proveedor['proveedor'])&&$proveedor['proveedor']?$proveedor['proveedor']:''; ?></a></li>
			<li><?php echo $item['titulo']; ?></li>
		<?php }else{ ?>
			<li><?php echo isset($proveedor['proveedor'])&&$proveedor['proveedor']?$proveedor['proveedor']:'Pedido presupuesto'; ?></li>
		<?php } ?>
	<?php }elseif(isset($rubro["rubro"])&&$rubro["rubro"]){ ?>
		<li><?=$rubro["rubro"];?></li>
	<?php } ?>
</ul>