<?php
$bodyclass = 'page split prehome';
include('header.php'); ?>
<div class="container">
	<?php if($id_rubro == 17 || $id_rubro == 154){ ?>
		<h1 class="h_sep">Salones de Fiesta</h1>
	<?php } elseif($id_rubro == 30 || $id_rubro == 155) { ?>
		<h1 class="h_sep">Quintas y Estancias</h1>
	<?php }	?>
	
	<?php if(isset($banners[114]) && $banners[114] || $mostrar_banners){ ?>
		<div class="banner banner_970_90 <?=isset($banners[114]) && $banners[114] ? 'no_bg' : '';?>"><?php echo isset($banners[114]) && $banners[114] ? $banners[114] : '<p>Banner Rubro Pre-home Top 728x90<span>Tamaño real 970x90</span></p>';  ?></div>
	<?php } ?>
		
	<div id="split">
		
		<section id="primary">
			<?php if($id_rubro == 17){ ?>
				<div id="mapa_prehome">
					<p>Hacé <strong>click en tu zona</strong> y encontrá todos los Salones de Fiesta</p>
					<div id="mapa_wrapper">
						<img class="bg" src="<?php echo base_url('/assets/images/mapa_salones_prehome.jpg'); ?>" />
						
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4480'); ?>" class="norte">Zona <strong>Norte</strong>
							<div><span><span>Salones en Zona Norte</span><p>Beccar, Bella Vista, Martinez, Olivos, Pilar, San Isidro, Tigre, Vicente Lopez...</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4482'); ?>" class="oeste">Zona <strong>Oeste</strong>
							<div><span> <span>Salones en Zona Oeste</span><p>Castelar, Ituzaingó, Lujan, San Justo, Lomas del Mirador, San Martín...</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4481'); ?>" class="sur">Zona <strong>Sur</strong>
							<div><span><span>Salones en Zona Sur</span><p>Avellaneda, Banfield, Berazategui, Ezeiza, Monte Grande, La Plata, Lanús, Quilmes...</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4479'); ?>" class="capital">Capital <strong>Federal</strong>
							<div><span><span>Salones en Capital Federal</span><p>Barrio Norte, Belgrano, Caballito, Palermo, Puerto Madero, Recoleta, San Telmo...</p></span></div>
						</a>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta-en-uruguay_CO_r333'); ?>" class="uruguay"><strong>Uruguay</strong>
							<div><span><span>Salones en Uruguay</span><p>Salones en Punta del Este</p></span></div>
						</a>
					</div><!-- #mapa_wrapper -->		
					<h2 class="title_sep">Elegí la Zona que quieras ver:</h2>
					<div id="mapa_links">
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4479'); ?>">Salones de Fiesta en Capital Federal</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4480'); ?>">Salones de Fiesta en Zona Norte</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4482'); ?>">Salones de Fiesta en Zona Oeste</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4481'); ?>">Salones de Fiesta en Zona Sur</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17_%7C115-4500'); ?> ">Salones de Fiesta en Uruguay</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17'); ?>">Ver todos los Salones de Fiesta</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-hoteles_CO_r81'); ?>">Salones de Hoteles</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas_y_estancias_CO_r30'); ?>">Quintas y Estancias</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta-en-uruguay_CO_r333'); ?>">Uruguay</a>
					</div>
				</div><!-- #mapa_prehome -->
				
			<?php }elseif($id_rubro == 30){ ?>
				<div id="mapa_prehome">
					<p>Hacé <strong>click en tu zona</strong> y encontrá todas las quintas y Estancias</p>
					<div id="mapa_wrapper">
						<img class="bg" src="<?php echo base_url('/assets/images/mapa_quintas_prehome.jpg'); ?>" />
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30_%7C115-4480'); ?>" class="norte">Zona <strong>Norte</strong>
							<div><span><span>Quintas y Estancias en Zona Norte</span><p>Beccar, Bella Vista, Martinez, Olivos, Pilar, San Isidro, Tigre, Vicente Lopez...</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30_%7C115-4482'); ?>" class="oeste">Zona <strong>Oeste</strong>
							<div><span> <span>Quintas y Estancias en Zona Oeste</span><p>Castelar, Ituzaingó, Lujan, San Justo, Lomas del Mirador, San Martín...</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30_%7C115-4481'); ?>" class="sur">Zona <strong>Sur</strong>
							<div><span><span>Quintas y Estancias en Zona Sur</span><p>Avellaneda, Banfield, Berazategui, Ezeiza, Monte Grande, La Plata, Lanús, Quilmes...</p></span></div>
						</a>
					</div><!-- #mapa_wrapper -->		
					<h2 class="title_sep">Elegí la Zona que quieras ver:</h2>
					<div id="mapa_links">
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30_%7C115-4480'); ?>">Quintas y Estancias en Fiesta en Zona Norte</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30_%7C115-4482'); ?>">Quintas y Estancias en Fiesta en Zona Oeste</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30_%7C115-4481'); ?>">Quintas y Estancias en Fiesta en Zona Sur</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/quintas-y-estancias_CO_r30'); ?>">Ver todas las Quintas y Estancias</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-hoteles_CO_r81'); ?>">Salones de Hoteles</a><span> |</span>
						<a href="<?php echo base_url('/buenos-aires/proveedor/salones-de-fiesta_CO_r17'); ?>">Salones de Fiesta</a>
					</div>
				</div><!-- #mapa_prehome -->

			<?php }elseif($id_rubro == 155){ ?>
				<div id="mapa_prehome" class="cordoba">
					<p>Hacé <strong>click en tu zona</strong> y encontrá todas las quintas y Estancias</p>
					<div id="mapa_wrapper">
						<img class="bg" src="<?php echo base_url('/assets/images/mapa_quintas_prehome_cordoba.jpg'); ?>" />
						
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4494'); ?>" class="norte">Zona<strong>Norte</strong>
							<div><span>Ver quintas y estancias en <span>Zona Norte</span><p>V. del Totoral, Deán Funes, Villa Tulumba, Villa de María, San Fco. del Chañar.</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4495'); ?>" class="oeste">Zona<strong>Oeste</strong>
							<div><span>Ver quintas y estancias en <span>Zona Oeste</span><p>Valle de Traslasierra, Cruz del Eje y Villa Dolores.</p></span></div>
						</a>
				
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4499'); ?>" class="centro"><strong>Centro</strong>
							<div><span>Ver quintas y estancias en <span>Centro</span><p>Cba. Capital, Alta Gracia, Cosquín, C. Paz, La Falda, Valle de Punilla, Sierras Chicas.</p></span></div>
						</a>
				
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4498'); ?>" class="sur">Zona<strong>Sur</strong>
							<div><span>Ver quintas y estancias en <span>Zona Sur</span><p>Valle de Calamuchita,  La Carlota, Laboulaye, Río Cuarto y Villa Huidobro.</p></span>
							</div>
						</a>
						
						<a href="javascript:;" class="este">Zona<strong>Este</strong>
							<div><span>Ver quintas y estancias en <span>Zona Este</span><p>San Francisco, Santa Rosa del Río Primero.</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4497'); ?>" class="sudeste">Zona<strong>Sudeste</strong>
							<div><span>Ver quintas y estancias en <span>Zona Sudeste</span><p>Villa del Rosario, Villa María, Bell Ville, Marcos Juárez.</p></span></div>
						</a>
					</div><!-- #mapa_wrapper -->		
					<h2 class="title_sep">Elegí la Zona que quieras ver:</h2>
					<div id="mapa_links">
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4494'); ?>">Zona Norte de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4495'); ?>">Zona Oeste de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4499'); ?>">Centro de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4497'); ?>">Zona Sudeste de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/quintas-y-estancias_CO_r155_%7C121-4498'); ?>">Zona Sur de Córdoba</a><span> |</span>
					</div>
				</div><!-- #mapa_prehome -->
			
			<?php }elseif($id_rubro == 154){ ?>	
			
			
				<div id="mapa_prehome" class="cordoba">
					<p>Hacé <strong>click en tu zona</strong> y encontrá todos los Salones de Fiesta</p>
					<div id="mapa_wrapper">
						<img class="bg" src="<?php echo base_url('/assets/images/mapa_quintas_prehome_cordoba.jpg'); ?>" />
						
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4494'); ?>" class="norte">Zona<strong>Norte</strong>
							<div><span>Ver Salones en <span>Zona Norte</span><p>V. del Totoral, Deán Funes, Villa Tulumba, Villa de María, San Fco. del Chañar.</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4495'); ?>" class="oeste">Zona<strong>Oeste</strong>
							<div><span>Ver Salones en <span>Zona Oeste</span><p>Valle de Traslasierra, Cruz del Eje y Villa Dolores.</p></span></div>
						</a>
				
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4499'); ?>" class="centro"><strong>Centro</strong>
							<div><span>Ver Salones en <span>Centro</span><p>Cba. Capital, Alta Gracia, Cosquín, C. Paz, La Falda, Valle de Punilla, Sierras Chicas.</p></span></div>
						</a>
				
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4498'); ?>" class="sur">Zona<strong>Sur</strong>
							<div><span>Ver Salones en <span>Zona Sur</span><p>Valle de Calamuchita,  La Carlota, Laboulaye, Río Cuarto y Villa Huidobro.</p></span>
							</div>
						</a>
						
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4496'); ?>" class="este">Zona<strong>Este</strong>
							<div><span>Ver Salones en <span>Zona Este</span><p>San Francisco, Santa Rosa del Río Primero.</p></span></div>
						</a>
						
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4497'); ?>" class="sudeste">Zona<strong>Sudeste</strong>
							<div><span>Ver Salones en <span>Zona Sudeste</span><p>Villa del Rosario, Villa María, Bell Ville, Marcos Juárez.</p></span></div>
						</a>
						
					</div><!-- #mapa_wrapper -->		
					<h2 class="title_sep">Elegí la Zona que quieras ver:</h2>
					<div id="mapa_links">
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4494'); ?>">Zona Norte de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4495'); ?>">Zona Oeste de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4499'); ?>">Centro de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4496'); ?>">Zona Este de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4497'); ?>">Zona Sudeste de Córdoba</a><span> |</span>
						<a href="<?php echo base_url('/cordoba/proveedor/salones-de-fiesta_CO_r154_%7C121-4498'); ?>">Zona Sur de Córdoba</a><span> |</span>
					</div>
				</div><!-- #mapa_prehome -->
			<?php } ?>
		</section>
		
		<aside id="secondary">
			<div class="fb-page gum_40" data-href="https://www.facebook.com/casamientosonline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/casamientosonline" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/casamientosonline">CasamientosOnline Portal de Novios</a></blockquote></div>
			
			<div class="banners clear">
				<?php if(isset($banners[150]) && $banners[150] || $mostrar_banners){ ?>
					<div class="banner banner_300_250 <?=isset($banners[150]) && $banners[150] ? 'no_bg' : '';?>"><?php echo isset($banners[150]) && $banners[150] ? $banners[150] : '<p>Banner Rubro Pre-home 300x250<span>Posición 1</span></p>'; ?></div>
				<?php }
				if(isset($banners[237]) && $banners[237] || $mostrar_banners){ ?>
					<div class="banner banner_300_250 <?=isset($banners[237]) && $banners[237] ? 'no_bg' : '';?>"><?php echo isset($banners[237]) && $banners[237] ? $banners[237] : '<p>Banner Rubro Pre-home 300x250<span>Posición 2</span></p>'; ?></div>
				<?php } ?>
			</div>
		</aside> <!-- #sidebar -->
		
	</div><!-- #split -->		
</div><!-- .container -->
<?php include('footer.php'); ?>