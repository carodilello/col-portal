<?php
$bodyclass = 'page split notas notas_home';
$tmp = explode('@',$nota_principal['secciones']);
$puntos_suspensivos = '...';

include('header.php'); ?>
<div class="container">
	<?php if(isset($banners[281]) && $banners[281] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[281]) && $banners[281] ? $banners[281] : '<p>Banner Editorial 970 x 90</p>'; ?></div>
	<?php } ?>
	
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Notas</li>
	</ul>
	
	<h1>Ideas y Consejos para Organizar tu <span class="green">Casamiento</span></h1>
	<p class="bajada">Tips para Fiestas divertidas y originales. Como Organizar tu Casamiento de día o noche,<br> sencillos o diferentes, lo más económico posible. Actualizate acá!</p>
	
	<div id="split">
		<section id="primary">
			<?php if($nota_principal){
				$id_tipo_editorial = $tmp[0]; ?>
				<div id="nota_principal">
					<a href="<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>"><img src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota_principal['pic']); ?>" alt="<?php echo $nota_principal['titulo_pic']; ?>" /></a>
					<h2><a href="<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>"><?php echo $nota_principal['titulo']; ?></a></h2>
					<p><?php echo $nota_principal['copete']; ?></p>
					<a href="<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>" class="btn btn-default">Ver Nota</a>
					<div class="ribbon_cat"><span>Nota del Mes</span></div>
				</div><!-- #nota_principal -->
			<?php }
			
			if($notas_destacadas){ ?>
				<div id="notas_segundo_nivel" class="row">
					<?php
					foreach ($notas_destacadas as $nota){
						$tmp = explode('@',$nota['secciones']);
						$secciones = $tmp[1];
						$id_tipo_editorial = $tmp[0]; ?>
						<div class="col-md-4">
							<a href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>" class="img_wrapper block">
								<img src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" alt="<?php echo $nota['titulo_pic']; ?>" />
								<?php if($secciones){ ?> <div class="ribbon_cat"><span><?php echo $secciones; ?></span></div> <?php } ?>
							</a>
							<h3><a href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>"><?php echo strlen($nota['titulo'])<=50?$nota['titulo']:(mb_substr($nota['titulo'], 0, 50).$puntos_suspensivos); ?></a></h3>
							<p><?php echo strlen($nota['copete'])<=68?$nota['copete']:(mb_substr($nota['copete'], 0, 68).$puntos_suspensivos); ?></p>
						</div>	
					<?php } ?>
				</div><!-- #notas_segundo_nivel -->
			<?php } ?>
			
			<?php if(isset($banners[282]) && $banners[282] || $mostrar_banners){ ?>
				<div class="banner_wrapper">
					<div class="banner banner_728_90"><?php echo isset($banners[282]) && $banners[282] ? $banners[282] : '<p>Banner Editorial 728 x 90</p>'; ?></div>
				</div>
			<?php } ?>
			
			<?php if($secciones_home && count($notas_secciones) > 0){ ?>
				<div id="cat_notas_wrapper">
					<?php foreach ($secciones_home as $id_seccion){
						if(isset($notas_secciones[$id_seccion]) && $notas_secciones[$id_seccion]){
							$tmp = explode('@',$notas_secciones[$id_seccion][0]['secciones']);
							$seccion    = $tmp[3]?$tmp[3]:$tmp[1];
							$id_seccion = $tmp[2]?$tmp[2]:$tmp[0];
							
							if(!$id_seccion){
								$tmp2 = explode('@',$notas_secciones[$id_seccion][1]['secciones']);
								$id_seccion = $tmp2[2]; 
							} ?>
							<div class="bloque_cat_notas row">
								<div class="col-md-12">
									<h2 class="pull-left"><?php echo $seccion; ?></h2>
									<a class="pull-right ver_todos" href="<?php echo base_url('/consejos/' . $secciones_home_seo[$id_seccion] . '_CO_cn' . $id_seccion); ?>"><i class="fa fa-plus-circle"></i>Ver todas las notas de <?php echo $seccion; ?></a>
								</div>
								<div class="clear"></div>
								<?php foreach ($notas_secciones[$id_seccion] as $nota){ 
									if(isset($nota['secciones'])){
										$tmp = explode('@',$nota['secciones']);
										$id_tipo_editorial = $tmp[0]; ?>
										<div class="col-md-6 col-xs-6 col-xxs-12">
											<a href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>" class="box">
												<img src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" alt="<?php echo $nota['titulo_pic']; ?>" />
												<h3 data-toggle="tooltip" data-placement="bottom" title="<?php echo strlen($nota['titulo'])>40?$nota['titulo']:''; ?>"><?php echo strlen($nota['titulo'])<=40?$nota['titulo']:(mb_substr($nota['titulo'], 0, 40).$puntos_suspensivos); ?></h3>
											</a>
										</div>		
									<?php }
								} ?>								
								
								<div class="line"></div>
							</div><!-- .bloque_cat_notas -->		
					<?php }
					} ?>
				</div><!-- #cat_notas_wrapper -->
			<?php } ?>
			
			<div id="notas_boxes_bottom" class="row">
				<div class="col-md-4 col-xs-4 col-xxs-12">
					<a href="<?php echo base_url('/editorial/comunidad'); ?>" class="img_wrapper block">
						<img src="<?php echo base_url('/assets/images/pic_notas_blogs.jpg'); ?>" alt="Comunidad" />
						<h3>Comunidad</h3>
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
				<div class="col-md-4 col-xs-4 col-xxs-12">
					<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina'); ?>" class="img_wrapper block">
						<img src="<?php echo base_url('/assets/images/pic_notas_civil.jpg'); ?>" alt="Registros Civiles" />
						<h3>Registros<br />Civiles</h3>
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
				<div class="col-md-4 col-xs-4 col-xxs-12">
					<a href="<?php echo base_url('/consejos/casamientos-reales_CO_cn1055'); ?>" class="img_wrapper block">
						<img src="<?php echo base_url('/assets/images/pic_notas_casamientos_reales.jpg'); ?>" alt="Casamientos Reales" />
						<h3>Casamientos<br />Reales</h3>
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
			</div><!-- #notas_boxes_bottom -->
		</section>
		
		<aside id="secondary">
			<?php include('notas_categorias_sidebar.php');
			include('notas_facebook.php'); ?>
			
			<?php include('registro_info_novias_sidebar.php'); ?>
			
			<?php if(isset($banners[283]) && $banners[283] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[283]) && $banners[283] ? $banners[283] : '<p>Banner Editorial 300x250 <span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			
			<?php include('notas_instagram.php'); ?>
			
			<?php include('notas_mas_leidas.php'); ?>
			
			<?php include('casamientos_tv_sidebar.php'); ?>
			
			<?php if(isset($banners[284]) && $banners[284] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[284]) && $banners[284] ? $banners[284] : '<p>Banner Editorial 300x250 <span>Posicion 2</span></p>'; ?></div>
			<?php } ?>

			<?php if(isset($banners[285]) && $banners[285] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[285]) && $banners[285] ? $banners[285] : '<p>Banner Editorial 300x250 <span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #sidebar -->
	</div><!-- #split -->
			
</div><!-- .container -->
<div id="wrapper_listado_proveedores_interna">
<?php 
	$en_notas = TRUE;
	$class = ''; 
	include('mod_listado_proveedores_footer.php'); ?>
	<div class="clear"></div>
</div><!-- #wrapper_listado_proveedores_interna -->
<?php include('footer.php'); ?>