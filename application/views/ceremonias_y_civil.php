<?php
$bodyclass = 'page ceremonias';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>

<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Ceremonias y Civil</li>
	</ul>
	<h1 class="title_sep">Registros Civiles, Iglesias y Ceremonias</h1>
	
	<div id="split">
		<section id="primary">
			<p class="box padding_box gum_50">Toda la información necesaria y el paso a paso para organizar tu Ceremonia y el Civil. Información de Registros Civiles en Argentina: dónde están, trámites y tiempos que debes considerar. Además Iglesias Católicas, Templos Judíos y Ceremonias no tradicionales.</p>
			
			<h2>Datos útiles, Trámites y Requisitos</h2>
			<p class="gum_30">Conocé todo lo que debes saber para iniciar el trámite de civil y llevar a cabo tu ceremonia.</p>
			
			<div id="boxes_ceremonias" class="row">
				<div class="col-md-6">
					<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina'); ?>" class="border_reg">
						<img src="<?php echo base_url('/assets/images/registros_civiles_box.jpg'); ?>" alt="Registros Civiles" />
						<span class="valign">Registros Civiles</span>
					</a>
				</div>
				
				<div class="col-md-6">
					<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-iglesias-catolicas-en-argentina'); ?>" class="border_igle">
						<img src="<?php echo base_url('/assets/images/iglesias_catolicas_box.jpg'); ?>" alt="Iglesias Católicas" />
						<span class="valign">Iglesias Católicas</span>
					</a>
				</div>
				
				<div class="col-md-6">
					<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-templos-judios-en-argentina'); ?>" class="border_templ">
						<img src="<?php echo base_url('/assets/images/templos_judios_box.jpg'); ?>" alt="Templos Judíos" />
						<span class="valign">Templos Judíos</span>
					</a>
				</div>
				
				<div class="col-md-6">
					<a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-ceremonias-no-religiosas'); ?>" class="border_no_reli">
						<img src="<?php echo base_url('/assets/images/ceremonias_no_religiosas_box.jpg'); ?>" alt="Ceremonias no Religiosas" />
						<span class="valign">Ceremonias no Religiosas</span>
					</a>
				</div>
			
			</div><!-- .row -->
			
		</section><!-- #primary -->
		
		<aside id="secondary">
			
			<div class="mod_aside_ceremonias">
				<?php $no_religiosas = TRUE;
				include('mod_aside_ceremonias.php');
				include('ceremonias_banners_aside.php'); ?>
			</div><!-- #aside_directorios -->
			
		</aside> <!-- #sidebar -->
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>