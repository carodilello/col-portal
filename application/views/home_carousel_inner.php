<?php
$puntos_suspensivos = '...';
$str_mas = '';
$zonas = '';
foreach ($proveedores_destacados as $key => $provs){ ?>
	<div class="item <?=$key==0?'active':'';?>">
		<?php foreach ($provs as $k => $prov){
			if(!empty($prov["nombre_seo"])){
				if(strlen($prov["proveedor"]) >= 22) $str_mas = "...";
				$tmp = explode(',', $prov['zonas']);
				if(isset($tmp[0])&&$tmp[0]){
					$zona = $tmp[0];
					$zonas = $zona;
					unset($tmp[0]);
					if(count($tmp) > 0){
						$zonas .= '<span> + '.count($tmp).'</span>';
					}
				} ?>
				<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="box">
						<span class="rubro"><?php echo $prov['rubro']; ?></span>
						<a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['nombre_seo']; ?>"><img <?php echo 'src="http://media.casamientosonline.com/images/'.$prov['imagen'].'"'; ?> alt="<?php echo mb_substr($prov['proveedor'], 0, 22) . $str_mas; ?>" /></a>
						<div class="info">
							<h3><a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['nombre_seo']; ?>" <?=strlen($prov["proveedor"]) >= 22 ? ('data-toggle="tooltip" data-placement="top" title="' . $prov['proveedor'] . '"'):'';?>><?php echo mb_substr($prov['proveedor'], 0, 22) . $str_mas; ?></a></h3>
							<p <?=count($tmp)>0?('data-toggle="tooltip" data-placement="top" title="' . str_replace(',', ', ', implode(',', $tmp)) . '"'):('');?> class="zona"><?php echo $zonas; ?></p>
							<p title="<?php echo $prov['breve']; ?>" class="descripcion"><?php echo strlen($prov['breve'])>52?(mb_substr($prov['breve'], 0, 52).$puntos_suspensivos):$prov['breve']; ?></p>
						</div><!-- .info -->
						<a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['nombre_seo']; ?>" class="btn-box-carrusel">Más información</a>
					</div><!-- .box -->
				</div><!-- .col-md-3 -->
			<?php 
			$str_mas = '';	
			}
		} ?>
	</div>
<?php } ?>