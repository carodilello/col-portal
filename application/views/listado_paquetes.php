<?php
$bodyclass = 'page listado listado_promociones listado_paquetes';
include('header.php'); ?>
<div class="container">
	<?php include('listado_header.php'); ?>
	<div id="listado_wrapper">
		<aside>
			<?php
			$filtros_paquetes = TRUE;
			include('listado_filtros.php');
			include('listado_banners.php'); ?>
		</aside>
		<section id="content_listado" class="listado_paquetes">
			<?php include('listado_solapas.php');
			$puntos_suspensivos = "";
			if(isset($productos)&&$productos) foreach ($productos as $k => $producto){
				if(strlen(strip_tags($producto["contenido"])) >= 160) $puntos_suspensivos = "..."; ?>
				<div class="box_listado no_padding">
					<div class="precio">
						<strong><?php echo $producto["precio"]; ?></strong><span><?php echo $producto["precio_tipo"]; ?></span>
					</div>
					<div class="img_wrapper">
						<a href='<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . '_CO_pa' . $producto["id_producto"] . '#pp_minisitio'; ?>'>
							<?php if(in_array(get_headers('http://media.casamientosonline.com/images/'.$producto["imagen"])[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
								<img alt="<?php echo $producto["titulo"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/images/'.str_replace("_chica", "_grande", $producto["imagen"]).'"';?> alt="<?php echo $producto["titulo"]; ?>" />
							<?php }else{ ?>
								<img alt="<?php echo $producto["titulo"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/logos/'.$producto["logo"].'"';?> alt="<?php echo $producto["titulo"]; ?>" />
							<?php } ?>
						</a>
					</div>
					<div class="info_wrapper">
						<h2><a href='<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . '_CO_pa' . $producto["id_producto"] . '#pp_minisitio'; ?>'><?php echo $producto["titulo"]; ?></a></h2>
						<p class="organiza">Organiza: <a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro']; ?>"><?php echo $producto["proveedor"]; ?></a></p>
						
						<div class="caract_paquete">
							<?php if($producto["invitados"]){ ?>
								<span><i class="fa fa-user"></i>Base <?php echo $producto["invitados"]; ?> inv.</span>
							<?php } ?>
							<!--<span><i class="fa fa-calendar"></i>Viernes.</span>-->
							<?php if($producto["zonas"]){ ?>
								<span><i class="fa fa-map-marker"></i><?php echo $producto["zonas"]; ?></span>
							<?php } ?>
							<!--<span><i class="fa fa-clock-o"></i>Noche</span>-->
						</div><!-- .caract_paquete -->
						
						<div class="bottom_actions">
							<?php if($producto['rubros']){
								$paquetes_rubros = explode('@',$producto['rubros']); ?>
								<div class="inc_paquete">
									<strong>Rubros incluidos:</strong>
									<?php foreach ($paquetes_rubros as $rub) {
										$elem_rub = explode('*',$rub); ?>
										<span data-toggle="tooltip" data-placement="top" title="<?php echo $elem_rub[1]; ?>" class="icon ic_<?php echo $ic[$elem_rub[0]]; ?>"></span>
								<?php } ?>
								</div><!-- .inc_minisitio -->
							<?php } ?>
							<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $rubro['url'] . '/presupuesto-paquetes_CO_r' . $rubro["id"] . '_pa' . $producto['id_producto']; ?>" class="btn btn-default">Pedir Presupuesto</a>
						</div><!-- .bottom_actions -->
					</div>
				</div><!-- .box_listado -->
			<?php 
			$puntos_suspensivos = "";
			} 
			include('paginacion.php'); ?>
		</section><!-- #content_listado -->
	</div><!-- #listado_wrapper -->
</div><!-- .container -->
<?php include('footer.php'); ?>