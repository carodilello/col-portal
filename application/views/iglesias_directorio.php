<?php
$bodyclass = 'page ceremonias_detalle directorio';
include('header.php'); ?>
<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-iglesias-catolicas-en-argentina'); ?>">Iglesias</a></li>
		<li>Directorio</li>
	</ul>
	<h1 class="title_sep">Iglesias Católicas en Capital Federal<span>Sedes y directorios</span></h1>
	<img class="big_pic border_reg" src="<?php echo base_url('/assets/images/big_pic_iglesias_catolicas2.jpg'); ?>" alt="Iglesias Católicas en Capital Federal, Sedes y directorios" />
	<div id="split">
		<section id="primary">
			<div class="filtro">
				<h2 class="gum">Directorio</h2>
				<div class="pull-right">
					<label>Seleccioná tu barrio: </label>
					<?php if(!empty($barrios)){ ?>
						<select class="ubicacion_iglesias">
							<option value="">Todos</option>
							<?php foreach ($barrios as $val){ ?>
								<option value="<?php echo $val['id']; ?>"><?php echo $val['nombre']; ?></option>
							<?php } ?>
						</select>
					<?php } ?>
				</div>	
			</div><!-- .filtro -->
			<div id="listado_directorio">
				<?php if(!empty($directorios)){ ?>
					<table class="caba directorio_cont no_margin">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Dirección</th>
								<th>Teléfono</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($directorios as $directorio){ ?>
								<tr class="tr_<?php echo $directorio['id_barrio']; ?>">
									<td class="head_mobile"><?php echo $directorio['nombre']; ?></td>
									<td><?php echo $directorio['direccion']; ?></td>
									<td><?php echo $directorio['telefono'] ? $directorio['telefono'] : '---'; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php }else{ ?>
					<p>No se han encontrado resultados</p>
				<?php } ?>
			</div><!-- #listado_directorio -->
		</section><!-- #primary -->
		<aside id="secondary">
			<?php $iglesias_dir = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>