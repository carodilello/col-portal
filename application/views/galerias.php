<?php
$bodyclass = 'galerias';
include('header.php');

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
); ?>
<div class="container">
	<?php if(isset($banners[294]) && $banners[294] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[294]) && $banners[294] ? $banners[294] : '<p>Galerias top 970 x 90</p>'; ?></div>
	<?php } ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Galerías</li>
	</ul>
	<h1 class="title_sep">Galerías de Casamientos destacadas</h1>
	
	<div id="split">
		<section id="primary">
			<?php if(!empty($galerias_destacadas)){ ?>
				<div class="galeria_standard">
					<div id="carousel-minisitio" class="big_pic carousel slide" data-ride="carousel" data-pause="hover" data-interval="false">
						<div class="carousel-inner" role="listbox">
							<?php foreach ($galerias_destacadas as $k => $galeria){
								$tmp = explode("@", $galeria['secciones']);
								if(isset($tmp[3])&&$tmp[3]){
									$nombre_seccion = $tmp[3];
								}elseif(isset($tmp[1])&&$tmp[1]){
									$nombre_seccion = $tmp[1];
								} ?>
								<div class="align_c item <?=$k==0?'active':'';?>">
							 		<a href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria['id']); ?>" class="item_minis" title="<?php echo $galeria['titulo']; ?>">
								    	<img src="http://media.casamientosonline.com/images/<?php echo $galeria['pic_name'].'.'.$galeria['pic_extension']; ?>" alt="<?php echo $galeria['titulo']; ?>">
								    </a>
								    <div class="epigrafe">
										<h2 class="col-md-10"><?php if(isset($nombre_seccion)&&$nombre_seccion){ ?>
											<span class="rubro"><?php echo $nombre_seccion; ?></span>
										<?php } ?><a href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria['id']); ?>"><?php echo $galeria['titulo']; ?></a></h2>
									</div>
								</div>
							<?php } ?>
						</div><!-- #carousel-inner -->
						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-minisitio" role="button" data-slide="prev">
							<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
							<span class="sr-only valign">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-minisitio" role="button" data-slide="next">
							<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div><!-- #carousel-galerias -->
				</div><!-- #galeria_standard -->
			<?php } ?>
			
			<?php if(!empty($galerias)){ ?>
				<div id="grid_galeria">
					<div id="header_grid_galeria">
						<h3>Todas las galerías</h3>
						<div class="pull-right">
							<select class="galeria_tipo">
								<option value=""> - Filtros - </option>
								<option value="1" <?=isset($get['id_tipo']) && $get['id_tipo'] == '1' ? 'selected' : '';?> data-name="notas">Notas</option>
								<option value="2" <?=isset($get['id_tipo']) && $get['id_tipo'] == '2' ? 'selected' : '';?> data-name="empresas">Empresas</option>
							</select>
							
							<select class="galeria_seccion" style="display:none;">
								<option value=""> - Seleccione - </option>
								<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '76' ? 'selected' : '';?> value="76" data-name="vestidos">Vestidos</option>
								<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '81' ? 'selected' : '';?> value="81" data-name="belleza-y-estilo">Belleza y Estilo</option>
								<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '93' ? 'selected' : '';?> value="93" data-name="la-organizacion">La Organización</option>
								<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '98' ? 'selected' : '';?> value="98" data-name="luna-de-miel">Luna de Miel</option>
							</select>
							
							<select id="galeria_subseccion" class="galeria_subseccion" style="display:none;">
								<option value=""> - Seleccione - </option>
								<?php foreach ($galeria_subseccion as $key => $value){ ?>
									<option <?=isset($get['id_subseccion']) && $get['id_subseccion'] == $key ? 'selected' : '';?> value="<?php echo $key; ?>" data-name="<?php echo $value['nombre_seo']; ?>"><?php echo $value['nombre']; ?></option>
								<?php } ?>
							</select>
							
							
							<select class="galeria_sucursal" style="display:none;">
								<option value=""> - Seleccione - </option>
								<?php foreach ($sucursales as $k => $sucursal){ ?>
									<option <?=isset($get['id_sucursal']) && $get['id_sucursal'] == $sucursal['id'] ? 'selected' : '';?> value="<?php echo $sucursal['id']; ?>" data-name="<?php echo $sucursal['nombre_seo']; ?>"><?php echo $sucursal['sucursal']; ?></option>
								<?php } ?>
							</select>
							
							<select id="galeria_rubro" class="galeria_rubro" style="display:none;">
								<option value=""> - Seleccione - </option>
								<?php foreach ($rubros as $solapa => $rubros_solapa){ ?>
									<optgroup label="<?php echo $solapa; ?>">
										<?php foreach ($rubros_solapa as $id_rubro => $rubro){ ?>
											<option <?=isset($get['id_rubro']) && $get['id_rubro'] == $id_rubro ? 'selected' : '';?> value="<?php echo $id_rubro; ?>" data-name="<?php echo $rubro['nombre_seo']; ?>"><?php echo $rubro['nombre']; ?></option>
										<?php } ?>
									</optgroup>
								<?php } ?>
							</select>
						</div>
						
						
					</div><!-- #header_grid_videos -->
				
					<div class="row">
						<?php foreach ($galerias as $k => $galeria){
							include('galeria.php');
						} ?>
					</div>
				</div><!-- #grid_videos -->
			<?php } ?>
			<?php echo $this->pagination->create_links(); ?>
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
			
			<?php if(isset($banners[172]) && $banners[172] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[172]) && $banners[172] ? $banners[172] : '<p>Galeria 300x250 <span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[173]) && $banners[173] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[173]) && $banners[173] ? $banners[173] : '<p>Galeria 300x250 <span>Posicion 2</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[174]) && $banners[174] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[174]) && $banners[174] ? $banners[174] : '<p>Galeria 300x250 <span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[295]) && $banners[295] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[295]) && $banners[295] ? $banners[295] : '<p>Galeria 300x250 <span>Posicion 4</span></p>'; ?></div>
			<?php } ?>
			
		</aside> <!-- #sidebar -->
		
		<?php echo $this->pagination->create_links(); ?>
	</div><!-- #split -->
</div><!-- .container -->
<?php
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
include('popup_fotos.php');
include('footer_info.php'); ?>