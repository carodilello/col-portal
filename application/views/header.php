<?php include('head.php'); ?>
	<?php if(!empty($no_buscador_chico)){
	}else{ ?>
		<div id="header_search" class="<?php echo isset($sucursal['class']) && $sucursal['class'] ? $sucursal['class'] : ''; ?> buscador_global" >
			<div class="inner relative">
				<div class="row">
					<p class="col-md-4">Encontrá lo que necesitás para tu Casamiento</p>
					<form class="col-md-8 form" action="<?php echo 'http://buscador.' . DOMAIN . '/'; ?>" method="GET" role="form">
						<fieldset>
							<div class="dropdown">
								<a id="sucursales-menu-sugerencias" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="block">
									<i class="fa fa-map-marker"></i><span class="green name-sucursal-ms"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?></span>
								</a>
								<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-menu-sugerencias">
									<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
										<li><a class="block change-sucursal" href="javascript:;" data-id="<?=$suc["nombre_seo"];?>"><?=$suc["sucursal"];?></a></li>
									<?php } ?>
								</ul>
							</div>
							<select name="id_sucursal" class="select-sucursal" style="display:none;">
								<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
									<option value="<?php echo $suc["nombre_seo"]; ?>" <?=$sucursal['nombre_seo']==$suc["nombre_seo"]?'selected':'';?>><?=$suc["sucursal"];?></option>
								<?php } ?>
							</select>
							<input class="input-buscar" type="text" name="search" placeholder="Qué estás buscando?" autocomplete="off" value="" required />
							<input class="tipo" type="hidden" name="tipo" value="todos" />
						</fieldset>
						<input type="button" value="Buscar" class="button-search" />
					</form>
					<div class="clear"></div>
					<div class="sugerencias_container"><?php include('menu_sugerencias.php'); ?></div>
					<div id="menu_busqueda_wrapper" class="menu-busqueda row col-md-3">
						<div class="relative">
							<span class="arrow_dialog_buscador"></span>
						</div>
						<div id="menu_busqueda_inner" class="menu_busqueda_inner"></div>
					</div><!-- #menu_busqueda_wrapper -->
				</div><!-- .row -->
			</div><!-- .inner -->
		</div><!-- #inner_search -->
	<?php } ?>
</header>