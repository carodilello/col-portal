<div class="mod_aside_ceremonias first">
	<h3 class="title_sep">Ceremonias y Civil</h3>
	<p>Datos útiles, trámites y requisitos.</p>
	<ul>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina'); ?>" <?=isset($registros)&&$registros ? 'class="active"' : '';?>><span class="icon_ceremonia"></span>Registros Civiles</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-iglesias-catolicas-en-argentina'); ?>" <?=isset($iglesias)&&$iglesias ? 'class="active"' : '';?>><span class="icon_ceremonia iglesia"></span>Iglesias Católicas</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-templos-judios-en-argentina'); ?>" <?=isset($templos)&&$templos ? 'class="active"' : '';?>><span class="icon_ceremonia templo"></span>Templos Judíos</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-ceremonias-no-religiosas'); ?>" <?=isset($no_religiosas)&&$no_religiosas ? 'class="active"' : '';?>><span class="icon_ceremonia no_reli"></span>Ceremonias no religiosas</a></li>
	</ul>
</div><!-- #aside_ceremonias -->

<div class="mod_aside_ceremonias">
	<h3 class="title_sep">Directorios</h3>
	<p>Un completo directorio divididas por barrio y con datos de contacto.</p>
	
	<div id="categorias_notas_sidebar">
		<a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-registros-civiles-en-argentina'); ?>" class="box <?=isset($registros_dir)&&$registros_dir ? 'active' : '';?>">
			<img src="<?php echo base_url('/assets/images/icon_registros_civiles.png'); ?>" alt="Registros Civiles" />
			<p>Ver directorio de </p>
			<h4>Registros Civiles</h4>
		</a>
		
		<a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-iglesias-catolicas-en-argentina'); ?>" class="box <?=isset($iglesias_dir)&&$iglesias_dir ? 'active' : '';?>">
			<img src="<?php echo base_url('/assets/images/icon_iglesias_catolicas.png'); ?>" alt="Iglesias Católicas" />
			<p>Ver directorio de </p>
			<h4>Iglesias Católicas</h4>
		</a>
	
		<a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-templos-judios-en-argentina'); ?>" class="box <?=isset($templos_dir)&&$templos_dir ? 'active' : '';?>">
			<img src="<?php echo base_url('/assets/images/icon_templos_judios.png'); ?>" alt="Templos Judíos" />
			<p>Ver directorio de </p>
			<h4>Templos Judíos</h4>
		</a>
	</div><!-- #aside_directorios -->
</div><!-- #aside_directorios -->