<footer>
	<div class="inner">
		<div class="row">
			<div class="col-md-6">
				<div class="gum_30">
					<h4><span>GUIA DE EMPRESAS POR PROVINCIA</span></h4>
					<div class="col-md-6 no_padding listado ">
						<h3><a href="<?php echo base_url('/buenos-aires'); ?>">Casamientos en Buenos Aires</a></h3>
						<h3><a href="<?php echo base_url('/cordoba'); ?>">Casamientos en Córdoba</a></h3>
						<h3><a href="<?php echo base_url('/entre-rios'); ?>">Casamientos en Entre Ríos</a></h3>
						<h3><a href="<?php echo base_url('/la-plata'); ?>">Casamientos en La Plata</a></h3>
						<h3><a href="<?php echo base_url('/mar-del-plata'); ?>">Casamientos en Mar del Plata</a></h3>	
					</div>
					
					<div class="col-md-6 listado">
						<h3><a href="<?php echo base_url('/misiones'); ?>">Casamientos en Misiones</a></h3>
						<h3><a href="<?php echo base_url('/rosario'); ?>">Casamientos en Rosario</a></h3>
						<h3><a href="<?php echo base_url('/salta'); ?>">Casamientos en Salta</a></h3>
						<h3><a href="<?php echo base_url('/santa-fe'); ?>">Casamientos en Santa Fe</a></h3>
						<h3><a href="<?php echo base_url('/tucuman'); ?>">Casamientos en Tucumán</a></h3>
					</div>
					
				</div>
			</div> <!-- .col-md-6  -->
			
			<div class="col-md-3">
				<div class="gum_30">
					<h4><span>Trámites de Casamientos</span></h4>
					<div class="listado">
						<h3><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina'); ?>">Requisitos para Casamiento por Civil</a></h3>
						<h3><a href="<?php echo base_url('/ceremonias-y-civiles/listado-de-registros-civiles-en-argentina'); ?>">Sedes Registro Civil Casamiento</a></h3>
						<h3><a href="http://www.buenosaires.gob.ar/pedir-nuevo-turno?idPrestacion=1029">Matrimonio Reserva de Fecha</a></h3>
						<h3><a href="<?php echo base_url('/proveedores/listado_empresas/300'); ?>">Ceremonias para Casamientos</a></h3>
						<h3><a href="<?php echo base_url('/consejos/asignacion-por-matrimonio-el-tope-sube-a-60-mil-pesos_CO_n5713'); ?>">Asignación por Matrimonio ANSES</a></h3>
						<h3><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-iglesias-catolicas-en-argentina'); ?>">Casamientos por iglesia </a></h3>
					</div>
				</div>	
			</div> <!-- .col-md-3  -->
			
			<div class="col-md-3">
				<div class="gum_30">
					<h4>Acerca de Casamientos Online</h4>
					<div class="listado">
						<h3><a href="<?php echo base_url('/empresas/nuestra-historia'); ?>">¿Quienes somos?</a></h3>
						<h3><a href="<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>">Expo para Novias</a></h3>
						<h3><a href="<?php echo base_url('/contacto'); ?>">Contacto</a></h3>
					</div>
					<a href="<?php echo base_url('/empresas/como-anunciar'); ?>" class="como_anunciar"><i class="fa fa-chevron-right"></i>Como anunciar en el portal</a>
				</div>	
			</div> <!-- .col-md-3  -->
		
		</div> <!-- .row  -->
	</div><!-- #inner  -->
	
	<section id="social_footer">
		<p>¡Seguinos!</p>
		<a href="http://www.facebook.com/casamientos" target="_blank"><i class="fa fa-facebook"></i></a>
		<a href="http://www.twitter.com/casamientos_ol" target="_blank"><i class="fa fa-twitter"></i></a>
		<a href="https://www.instagram.com/casamientosonline/" target="_blank"><i class="fa fa-instagram"></i></a>
		<a href="http://pinterest.com/casamientos/" target="_blank"><i class="fa fa-pinterest"></i></a>
		<a href="http://www.youtube.com/casamientosonline" target="_blank"><i class="fa fa-youtube"></i></a>
		<a href="https://plus.google.com/+casamientosonline" target="_blank"><i class="fa fa-google-plus"></i></a>
		
		
	</section><!-- #social-footer  -->
	<div id="footer_bottom">
		<div class="inner">
			<div class="row">
				<p class="col-md-6">
					<a href="http://qr.afip.gob.ar/?qr=JkijmKUbSNwatkzv6p8DNQ,," target="_F960AFIPInfo" class="afip"><img alt="Data Fiscal" src="<?php echo base_url('/assets/images/data_fiscal.jpg'); ?>" ></a>
					<span>Copyright © 2016  |  Casamientosonline.com</span>
						
					</p>
				<ul class="col-md-6 pull-right">
					<li><a href="<?php echo base_url('/terminos-y-condiciones'); ?>">Términos y condiciones</a></li>
					<li><a href="<?php echo base_url('/politicas-de-privacidad'); ?>">Políticas de privacidad</a></li>
					<li><a href="<?php echo base_url('/mapa-de-sitio'); ?>">Mapa del sitio</a></li>
				</ul>
			</div>
		</div><!-- .inner -->
		
	</div><!-- #footer_bottom -->
</footer>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/plugins/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/plugins/bootstrap-datepicker.es.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/plugins/validator.js'); ?>"></script>
<?php if(isset($scripts_javascript)&&$scripts_javascript) foreach ($scripts_javascript as $script){ echo $script."\n"; } ?>
<script type="text/javascript" src="<?php echo base_url('/assets/js/funciones_form.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/funciones.js'); ?>"></script>
</body>
</html>