<?php $str_mas = '';
foreach ($proveedores as $k => $group) { ?>
	<div class="item <?=$k == 0 ? 'active' : '';?>">
		<?php foreach ($group as $prov) {
			if(strlen($prov["proveedor"]) >= 22) $str_mas = "...";
			$tmp = explode(',', $prov['zonas']);
			if(!empty($tmp)){
				$zona = $tmp[0];
				$zonas = $zona;
				unset($tmp[0]);
				if(count($tmp) > 0){
					$zonas .= '<span> + '.count($tmp).'</span>';
				}
			} ?>
			<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
				<div class="box">
					<span class="rubro"><?php echo $prov['rubro']; ?></span>
					<a class="img_wrapper" href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro_seo']; ?>"><img alt="Imagen de <?php echo mb_substr($prov['proveedor'], 0, 22) . $str_mas; ?>" <?php
					if(isset($prov["imagen_gde"]) && $prov["imagen_gde"]){
						echo str_replace('_grande', '_mediana', 'src="http://media.casamientosonline.com/images/'.$prov["imagen_gde"].'"');
					}elseif(isset($prov["imagen_chica"]) && $prov["imagen_chica"]){
						echo 'src="http://media.casamientosonline.com/images/'.$prov["imagen_chica"].'"';
					}else{
						echo 'src="http://media.casamientosonline.com/logos/'.$prov["imagen"].'"';
					} ?> /></a>
					<div class="info">
						<h3><a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro_seo']; ?>" <?=strlen($prov["proveedor"]) >= 22 ? ('data-toggle="tooltip" data-placement="top" title="' . $prov['proveedor'] . '"'):'';?>><?php echo mb_substr($prov['proveedor'], 0, 22) . $str_mas; ?></a></h3>
						<p <?=count($tmp)>0?('data-toggle="tooltip" data-placement="top" title="' . str_replace(',', ', ', implode(',', $tmp)) . '"'):('');?> class="zona"><?php echo $zonas; ?></p>
						<p title="<?php echo $prov['breve']; ?>" class="descripcion"><?php echo strlen($prov['breve'])>52?(mb_substr($prov['breve'], 0, 52).$str_mas):$prov['breve']; ?></p>
					</div><!-- .info -->
					<a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro_seo']; ?>" class="btn-box-carrusel">Más información</a>
				</div><!-- .box -->
			</div><!-- .col-md-3 -->
		<?php } ?>
	</div>
<?php } ?>