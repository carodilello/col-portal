<?php 
$tmp = explode("@", $galeria['secciones']);
if(isset($tmp[3])&&$tmp[3]){
	$nombre_seccion = $tmp[3];
}elseif(isset($tmp[1])&&$tmp[1]){
	$nombre_seccion = $tmp[1];
} ?>
<div class="col-md-4 col-xs-6 col-xxs-12 col">
	<a href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria['id']); ?>" class="img_wrapper">
		<img src="http://media.casamientosonline.com/images/<?php echo $galeria['pic_name'].'.'.$galeria['pic_extension']; ?>" alt="<?php echo $galeria['titulo']; ?>" /></a>
	<h3><a href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria['id']); ?>"><?php echo $galeria['titulo']; ?></a></h3>
	<span><i class="fa fa-camera"></i> <?php echo $galeria['cantidad_fotos']; ?> Fotos</span>
	<?php if(isset($nombre_seccion)&&$nombre_seccion){ ?>
		<span class="rubro"><?php echo rtrim($nombre_seccion, '~'); ?></span>
	<?php } ?>
</div><!-- .col -->
<?php 
unset($nombre_seccion);