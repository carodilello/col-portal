<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
<title><?=isset($meta_title)&&$meta_title?$meta_title:"Casamientos Online ". date('Y');?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo !empty($meta_descripcion) ? str_replace('"', '', $meta_descripcion) : "Casamientos Online 2017"; ?>" />
<meta name="language" content="es" />
<meta name="author" content="Casamientosonline.com" />
<!--<meta name="keywords" content="<?=isset($meta_keywords)&&$meta_keywords?$meta_keywords:"";?>">-->

<link rel="shortcut icon" href="<?php echo base_url('/favicon.png'); ?>" type="image/png" />
<link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:700i,900i" rel="stylesheet">
<link href="<?php echo base_url('/assets/css/bootstrap.css'); ?>" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url('/assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/gallery/blueimp-gallery.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/gallery/bootstrap-image-gallery.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/datepicker/bootstrap-datepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/datepicker/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/style.css?nocache=' . filemtime(FCPATH . '/assets/css/style.css')); ?>" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url('/assets/css/responsive.css'); ?>" type="text/css" rel="stylesheet" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P3MV');</script>
<!-- End Google Tag Manager -->

<!-- Tweet Cards -->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@Casamientos_OL" />
<meta name="twitter:title" content="<?=isset($twitter_title)&&$twitter_title ? $twitter_title : (isset($meta_title)&&$meta_title?$meta_title:"Casamientos Online 2016");?>" />
<meta name="twitter:description" content="<?=isset($twitter_descripcion)&&$twitter_descripcion ? str_replace('"', '', $twitter_descripcion): (!empty($meta_descripcion) ? str_replace('"', '', $meta_descripcion) : "Casamientos Online 2016") ;?>" />
<meta name="twitter:image" content="<?=isset($twitter_image)&&$twitter_image ? $twitter_image: base_url("/assets/images/logo_twitter.png"); ?>" />
<!-- End Tweet Cards -->

<!-- Open Graph data -->
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo !empty($canonical) ? $canonical : ''; ?>" />
<meta property="og:title" content="<?php echo !empty($meta_title) ? $meta_title : 'Casamientos Online 2017'; ?>" />
<meta property="og:image" content="<?php echo !empty($meta_img) ? $meta_img : base_url('/assets/images/casamientos_online.png'); ?>" />
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="300" />
<meta property="og:site_name" content="Casamientos Online" />
<meta property="og:description" name="description" content="<?php echo !empty($meta_descripcion) ? str_replace('"', '', $meta_descripcion) : "Casamientos Online 2016"; ?>" />
<meta property="fb:app_id" content="230987006941395" />
<meta property="fb:admins" content="100000469127452" />
<!-- End Open Graph data -->

<!-- Google Rich Snippet -->
<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "WebSite",
	  "url": "http://www.casamientosonline.com/",
	  "potentialAction": {
	    "@type": "SearchAction",
	    "target": "http://www.casamientosonline.com/busqueda.php?tipo=1&term={search_term}",
	    "query-input": "required name=search_term"
	  }
	}
</script>

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "casamientosonline",
    "url": "http://www.casamientosonline.com",
    "sameAs": [
      "https://www.facebook.com/Casamientos/"
    ]
  }
</script>
<!-- End Google Rich Snippet -->


<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s){ if(f.fbq)return;n=f.fbq=function(){ n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '294684074060325');
	fbq('track', 'PageView');
</script>

<noscript>
	<img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=294684074060325&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="<?php echo $bodyclass;?> <?php echo isset($sucursal['class']) && $sucursal['class'] ? $sucursal['class'] : ''; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3MV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Pluggin Facebook -->
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7&appId=211692522224170";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<!-- End Pluggin Facebook -->
<link rel="canonical" href="<?php echo !empty($canonical) ? $canonical : ''; ?>">
<header>
	<section id="top_bar">
		<div class="inner">
			<div class="dropdown">
				<a id="sucursales-header-label" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="pull-left">
					<i class="fa fa-map-marker"></i><span class="mobile_none">Estás viendo: </span><span class="green"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?></span>
				</a>
				<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-header-label">
					<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
						<li><a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') . (isset($sucursal_redirect)&&$sucursal_redirect?$sucursal_redirect:'')); ?>"><?=$suc["sucursal"];?></a></li>
					<?php } ?>
				</ul>
			</div>

			<div id="login" class="pull-right">
				<a href="<?php echo base_url('/empresas/como-anunciar'); ?>" class="pull-left"><i class="fa fa-sign-in"></i>Área <span>Empresas</span></a>
			</div><!-- #login -->
		</div><!-- .inner -->
	</section>

	<div class="inner relative fix_height_header">
		<div>
			<div id="logo_col" class="col-md-3 clear no_padding">
				<?php if($this->router->class.'_'.$this->router->method == 'home_index'){ ?>
					<h1>Casamientos Online - Encontrá todo para tu casamiento</h1>
				<?php } ?>
				<a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">
					<img src="<?php echo base_url('/assets/images/logo_col_2.png'); ?>" alt="Casamientos Online el Portal de los Novios" />
				</a>
			</div><!-- #logo_col -->

			<nav role="navigation" class="navbar navbar-default col-md-9 no_padding">
				<div class="navbar-header">
		            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		        </div>
				<?php include('nav.php'); ?>
			</nav>
		</div><!-- .row -->
		<div class="clear"></div>
	</div><!-- .inner -->
	<?php if(isset($rubros_derivados) && $rubros_derivados){
		include('popup_derivador.php');
	} ?>