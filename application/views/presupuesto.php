<?php
$bodyclass = 'page presupuesto';

include('header.php'); ?>
<div class="container">
	<?php 
	include("breadcrumbs.php");
	include("mod_gracias.php"); ?>
	<h1 class="h_sep gum_40"><?php echo $titulo['titulo']; ?><span class="green"><?php echo $titulo['titulo_color']; ?></span></h1>
	<?php if(!isset($gracias)){ ?>
		<?php if($config['mantenimiento_site'] || $config['mantenimiento_presupuesto']){ ?>
			<p>Estamos realizando tareas de mantenimiento. Intente nuevamente en unos minutos. Muchas gracias.</p>
		<?php }else{ ?>
			<form action="<?php echo base_url('/formulario/procesar_pedido'); ?>" id="form_presupuesto_wrapper" method="POST" role="form" class="form<?php echo ($datos_viaje ? ' form_presupuesto_luna' : ''); ?>">
				<input type="hidden" name="randomID" value="<?php echo rand(99, 999999); ?>">
				<input type="hidden" name="id_pais"  value="<?php echo $data_form['id_pais']; ?>">
				<input type="hidden" name="es_grupal"  value="<?php echo $es_grupal; ?>">

				<?php if(!empty($proveedor['subdominio'])){ ?>
					<input type="hidden" name="subdominio"  value="<?php echo $proveedor['subdominio']; ?>">
				<?php }
				if($hidden) foreach ($hidden as $hid){ ?>
					<input type="hidden" name="<?php echo $hid['name']; ?>" value="<?php echo $hid['value']; ?>" />
				<?php } ?>
				<aside class="col-md-3">
					<?php
					if(empty($area)){ ?>
						<h2>Contacto a Casamientos Online</h2>
						<p>¿Algún comentario o sugerencia que quieras compartir con el equipo de Casamientos Online? En este espacio dejá tus observaciones o inquietudes que serán derivadas al área correspondiente.</p>
						<p>Muchas gracias!</p>
					<?php }elseif($tipo == 'evento'){ ?>						
					<?php }elseif(($tipo == 'empresa' || $tipo == 'producto' || $tipo == 'promocion' || $tipo == 'paquete' || $tipo == 'foto' || $tipo == 'video' || empty($tipo)) && $area == 'presupuesto'){ ?>
						<div id="detalle_presupuesto_empresa">
							<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['seo_url']; ?>">
								<img <?php echo 'src="http://media.casamientosonline.com/logos/'.$empresa["imagen"].'"'; ?> />
							</a>
							<h2><a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['seo_url']; ?>"><?php echo $empresa['proveedor']; ?></a></h2>
							<?php if($empresa['zonas']){ ?>
								<p class="zona"><span class="fa fa-map-marker"></span><?php echo $empresa['zonas']; ?></p>
							<?php }
							if($empresa['direccion']){ ?>
								<p class="direccion"><?php echo $empresa['direccion']; ?></p>
							<?php } ?>
						</div><!-- #listado_presupuesto_grupal -->
					<?php }
					if($filtros_aplicados){ ?>
						<div id="filtros_wrapper">
							<div id="filtros_aplicados">
								<h3>Filtros Aplicados</h3>
								<?php foreach ($filtros_aplicados as $filtro_a){ ?>
									<span><span><strong><?php echo $filtro_a['filtro']; ?></strong>: <?php echo $filtro_a["opcion"];?></span> <a href="<?php
									$filt = str_replace("|".$filtro_a["id_filtro"]."-".$filtro_a["id_opcion"], "", urldecode($_SERVER['REQUEST_URI']));

									if(count($filtros_aplicados) == 1 && !$filt){
										echo str_replace('?','',$filt);
									}else{
										echo $filt;
									} ?>"><i class="fa fa-close"></i></a></span>
								<?php } ?>
							</div>
						</div>
					<?php }
					if($area == 'presupuesto'){ ?>
						<?php if($tipo == 'producto' || $tipo == 'promocion' || $tipo == 'paquete' || $tipo == 'foto' || $tipo == 'video'){ ?>
							<?php 
							$articulo = 'la';
							if(in_array($tipo,array('producto', 'paquete', 'video'))){
								$articulo = 'el';
							} ?>
						<div id="detalle_presupuesto_pp">
							<h3><?php echo strtoupper($tipo); ?></h3>
							<p>Estás consultando a la empresa por <?php echo $articulo; ?> siguiente <?php echo $tipo; ?>:</p>
							<?php if($tipo == 'video'){ ?>
								<img src="<?php echo $producto['image']; ?>" width="220">
							<?php }else{ ?>
								<div class="img_wrapper">
									<?php
									if(in_array(get_headers('http://media.casamientosonline.com/images/'.str_replace('_chica', '_grande', $producto["imagen"]))[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
										<img style="width:100%;" <?php echo 'src="http://media.casamientosonline.com/images/'.str_replace('_chica', '_grande', $producto["imagen"]).'"';?> alt="<?php echo $producto["titulo"]; ?>" />
									<?php }else{ ?>
										<img style="width:100%;" <?php echo 'src="http://media.casamientosonline.com/logos/'.$producto["logo"].'"';?> alt="<?php echo $producto["titulo"]; ?>" />
									<?php } ?>
								</div>
							<?php } ?>
							
							<h4><?php echo $producto['titulo']; ?></h4>
							<?php if(isset($producto['subtitulo']) && $producto['subtitulo']){ ?>
								<span class="subtitulo"><?php echo $producto['subtitulo']; ?></span>
							<?php }
							
							if($tipo != 'foto' && $tipo != 'video'){ ?>
								<div class="precio">
									<?php if($producto["precio"] != '0.00' && $producto["precio"] != '$ 0,00' && $producto["precio"] != 'U$S 0,00'){
											if(isset($producto["precio_tipo"])&&$producto["precio_tipo"]){ ?>
												<strong><?=$tipo!="paquete"?"$":"";?><?php echo $producto["precio"]; ?></strong>
												<strong><?php echo $producto["precio_tipo"]; ?></strong>
											<?php }else{ ?>
												<strong>
													<?php
														if($producto["moneda"] == 'P') echo '$'; 
														if($producto["moneda"] == 'D') echo 'U$S';
														if($producto["moneda"] == 'E') echo '&euro;';
														echo number_format($producto["precio"], 2, ",", "."); ?>
												</strong>
											<?php }
										}
									if(isset($producto["porcentaje_descuento"]) && $producto["porcentaje_descuento"]){ ?>
										<span><span><?php echo $producto["porcentaje_descuento"]; ?></span>%OFF</span>
									<?php } ?>
								</div>
							<?php } ?>
							<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['seo_url'] . '/presupuesto-empresa_CO_r' . $rubro["id"] . '_m' . $proveedor["id_minisitio"]; ?>" class="eliminar_pp">
								<i class="fa fa-times"></i>
								<span>Eliminar <?php echo $tipo; ?> del presupuesto y sólo consultar a esta empresa.</span>
							</a>
						</div>
						<?php }elseif($tipo == 'resultados'){
							if($proveedores){ ?>
								<h3>Empresas</h3>
								<div id="listado_presupuesto_grupal" class="gum_40">
									<input  type="hidden" name="accion_novio" id="accion_novio" value="0">
									<?php foreach ($proveedores as $k => $proveedor){ ?>
										<div class="empresa-container checkbox <?=$k>=$empresas_visibles?'empresa-hidden':'';?>" <?=$k>=$empresas_visibles?'style="display:none;"':'';?>>
											<label><input class="empresas_checkbox" name="id_minisitio[]" type="checkbox" value="<?php echo $proveedor['id_minisitio']; ?>" checked><?php echo $proveedor['proveedor']; ?></label>
										</div>
									<?php } ?>
								</div><!-- #listado_presupuesto_grupal -->

								<?php $count_prov = count($proveedores);
								if($count_prov > $empresas_visibles){ ?>
									<div><a href="javascript:;" class="ver-mas-empresas"><i class="fa fa-plus-circle"></i><span class='text-container'>Ver las</span><span class='empresas-restantes'> <?php echo $count_prov-$empresas_visibles;?></span> <span class='text-container-restantes'>empresas restantes</span></a></div>
								<?php } ?>
							<?php }elseif($opciones){ ?>
								<h3>Zonas</h3>
								<div id="listado_presupuesto_grupal" class="gum_40 has-error has-danger has-feedback">
									<?php foreach ($opciones as $k => $opcion){ ?>
										<div class="checkbox">
											<label><input class="minisitios" type="checkbox" value="<?php echo $opcion['minisitios']; ?>" name="minisitios[]"><?php echo $opcion['opcion']; ?></label>
										</div>
									<?php } ?>
								</div><!-- #listado_presupuesto_grupal -->
							<?php }
						} ?>
					<?php } ?>

					<?php if(isset($asociados_trabaja)&&$asociados_trabaja){ ?>
						<fieldset id="empresas_asociadas">
								<p class="Tit">Empresas Relacionadas</p>
							<p>El Presupuesto también se enviará a las siguientes empresas con las que trabajamos:</p>
							<?php foreach($asociados_trabaja as $asociados){ 
								foreach($asociados as $asociado){ ?>
								<div class="checkbox col-md-7 check_form no_padding has-feedback">
									<label><input type="checkbox" value="<?php echo $asociado['id_asociado']; ?>" name="asociados[]" checked="checked" /><?php echo $asociado['proveedor']; ?></label>
								</div>
							<?php } } ?>
						</fieldset>
					<?php } ?>

				</aside>

				<div id="form_presupuesto" class="col-md-9">
					<?php
					if(validation_errors()){ ?>
						<div class="errores-generales"><?php echo validation_errors(); ?></div>
					<?php } ?>
					<input type="hidden" name="dia_nacimiento" value="<?php echo $data_form['dia_nacimiento']?$data_form['dia_nacimiento']:set_value('dia_nacimiento'); ?>" class="date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" />

					<fieldset class="row no_margin_b">
						<label class="col-md-12"><?=$datos_viaje?'Cuándo te Casas?':'Datos del Casamiento';?></label>
						<div class="col-md-4 input_icon form-group has-feedback no_margin_b">
							<span class="fa fa-calendar"></span>
							<input name="dia_evento" type="text" value="<?php echo $data_form['dia_evento'];?>" placeholder="Fecha de Casamiento" class="date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
							
							<div class="help-block with-errors"></div>
						</div>
						
						<?php if($salon_elegido){ ?>
							<div class="col-md-4 input_icon form-group has-feedback no_margin_b">
								<span class="fa fa-building"></span>
								<input name="ubicacion" type="text" value="<?php echo $data_form['ubicacion'];?>" placeholder="Salón elegido" />
								<div class="help-block with-errors"></div>
							</div>
						<?php }else{ ?>
							<input type="hidden" name="ubicacion" value="" />
						<?php } ?>
						
						<?php if(!$datos_viaje){ ?>
							<div class="col-md-4 input_icon form-group has-feedback no_margin_b">
								<span class="fa fa-user-plus"></span>
								<input name="invitados" type="text" value="<?php echo $data_form['invitados'];?>" placeholder="Cantidad de Invitados" required />
								<div class="help-block with-errors"></div>
							</div>
						<?php } ?>
					</fieldset>

					<?php if($fecha_variable){ ?>
						<fieldset class="row no_margin_b">
							<div class="col-md-4 input_icon form-group has-feedback">
								<div class="checkbox check_form no_padding has-feedback">
									<label for="fecha_evento_variable"><input name="fecha_evento_variable" type="checkbox" id="fecha_evento_variable" value="1" />La fecha del evento puede variar</label>
								</div>
							</div>
						</fieldset>
					<?php } ?>
					
					
					<?php /*
					{if $smarty.get.estimado}
						<label for="estimado">Presupuesto disponible para este rubro</label>
						<input type="text" name="estimado"  class="float_l input_number" id="estimado" value="{$smarty.get.estimado}" />
					{/if}
					 */ ?>

					<?php 
						/*if(isset($asociados_trabaja)&&$asociados_trabaja){ ?>
						<fieldset>
							<label class="col-md-12">Empresas Relacionadas</label>
							<p>El Presupuesto también se enviará a las siguientes empresas con las que trabajamos:</p>
							<?php foreach($asociados_trabaja as $asociados){ 
									foreach($asociados as $asociado){ ?>
								<div class="checkbox col-md-7 check_form no_padding has-feedback">
									<label><input type="checkbox" value="<?php echo $asociado['id_asociado']; ?>" name="asociados[]" checked="checked" /><?php echo $asociado['proveedor']; ?></label>
								</div>
							<?php } } ?>
						</fieldset>
					<?php } */?>

					<?php if($datos_viaje){ ?>
						<input type="hidden" name="datos_viaje" value="1" />

						<fieldset class="row">
							<label class="col-md-12">Datos del Viaje</label>
							<div class="col-md-4 input_icon form-group has-feedback">
								<span class="fa fa-calendar"></span>
								<input name="dia_viaje" type="text" placeholder="Fecha estimada del viaje" class="date" data-remote="/mayor_hoy" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" required />
								<div class="help-block with-errors"></div>
							</div>
							
							<div class="col-md-4 input_icon form-group has-feedback">
								<span class="fa fa-usd"></span>
								<input name="presupuesto" type="text" placeholder="Presupuesto Estimado" required />
								<div class="help-block with-errors"></div>
							</div>
							
							<div class="col-md-4 input_icon form-group has-feedback">
								<span class="fa fa-moon-o"></span>
								<input name="cant_noches" type="text" placeholder="Cantidad de Noches"  required />
								<div class="help-block with-errors"></div>
							</div>
						</fieldset>
					<?php } ?>
					
					<fieldset class="row no_margin_b">
						<label class="col-md-12">Datos de Contacto</label>
						<div class="col-md-4 input_icon form-group has-feedback">
							<span class="fa fa-user"></span>
							<input name="nombre" type="text" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" required />
							<div class="help-block with-errors"></div>
						</div>
						
						<div class="col-md-4 input_icon form-group has-feedback">
							<span class="fa fa-user"></span>
							<input name="apellido" type="text" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" placeholder="Apellido" required />
							<div class="help-block with-errors"></div>
						</div>
						
						<div class="col-md-4 input_icon form-group has-feedback">
							<span class="fa fa-phone"></span>
							<input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
							<div class="help-block with-errors"></div>
						</div>
					</fieldset>
					
					<fieldset class="row no_margin_b">
						<div class="col-md-4 input_icon form-group has-feedback">
							<span class="fa fa-envelope"></span>
							<input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" required />
							<div class="help-block with-errors"></div>
						</div>
						
						<div class="col-md-4 input_icon form-group has-feedback">
							<span class="fa fa-map-marker"></span>
							<select name="id_provincia" class="provincias" required>
								<option value="">Seleccionar una Provincia</option>
								<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
									<option value="<?php echo $k; ?>"  <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
								<?php } ?>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						
						<div class="col-md-4 input_icon form-group has-feedback">
							<span class="fa fa-location-arrow"></span>
							<select name="id_localidad" class="localidades" required><!-- CAMBIE localidad X id_localidad. EN class.comercial.php HACE REFENCIA A id_localidad-->
								<option value="">Seleccionar una Localidad</option>
								<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
									<option value="<?php echo $k; ?>"  <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
								<?php } ?>
							</select>
							<div class="help-block with-errors"></div>
						</div>
					</fieldset>
					
					<div class="form-group has-feedback">
						<textarea placeholder="Comentarios" required name="comentario"></textarea>
						<div class="help-block with-errors"></div>
						<a href="<?php echo base_url('/politicas-de-privacidad'); ?>" target="_blank" class="politicas">Ver políticas de privacidad</a>
					</div>
					
					

					<div class="clear">
						<div class="checkbox col-md-7 check_form no_padding has-feedback">
						  <label for="infonovias"><input id="infonovias" name="infonovias" type="checkbox" value="1" <?php if($data_form['infonovias']){ echo 'checked="checked"'; } ?>>Deseo recibir el Infonovias y promociones de casamientos</label>
						</div>
						
						<input type="submit" class="btn-default submit_presupuesto" value="Enviar consulta" />
						<div class="has-error has-danger has-feedback">
							<div style="display:none; float:right;" class="error-zonas help-block with-errors"><ul class="list-unstyled"><li>Debe seleccionar al menos una zona</li></ul></div>
							<div style="display:none; float:right;" class="error-empresas help-block with-errors"><ul class="list-unstyled"><li>Debe seleccionar al menos una empresa</li></ul></div>
						</div><!-- #listado_presupuesto_grupal -->
					</div><!-- .row -->
				</div>
			</form>
		<?php } ?>
	<? } ?>
</div><!-- .container -->
<?php include('footer.php'); ?>