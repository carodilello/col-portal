<?php
$bodyclass = 'page medios_eventos';
include('header.php'); ?>
<div class="container">
	<?php if(isset($banners[303]) && $banners[303] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[303]) && $banners[303] ? $banners[303] : '<p>Comunidad top 970 x 90</p>'; ?></div>
	<?php } ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Eventos</li>
	</ul>
	<h1 class="title_sep">Exposiciones, Medios y Eventos de Casamientos</h1>
	<a href="<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>"><img class="header_eventos_pic" src="<?php echo base_url('/assets/images/jornadas_registro_header.jpg'); ?>" alt="Exposiciones, Medios y Eventos de Casamientos" /></a>
	<div id="bottom_header_eventos">
		<div class="row">
			<div class="col-md-8 col-xs-8">
				<h2>Próxima Jornada: <span class="pink">27 de Septiembre de 2017</span></h2>
				<p>
					<span class="direccion"><i class="fa fa-map-marker"></i>Centro Cultural Borges, Galerías Pacífico</span>
					<span><i class="fa fa-ticket"></i>Entrada gratuita, cupos limitados</span>
				</p>
			</div>	
			<a href="<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>" class="btn btn-default pull-right col-xs-3"  >¡Reservá tu Lugar!</a>
		</div>
	</div><!-- #bottom_header_eventos -->
	
	<div id="iconos_jornada_eventos">
			<div class="row">
				<div class="col col-md-3 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_numeros_empresas.png'); ?>" alt="150 Stands" />
					</div>
					<div class="data">
						<h3>150 Stands</h3>
						
					</div>
				</div><!-- .col-md-6 -->
				
				<div class="col col-md-3 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_organiza_jornada.png'); ?>" alt="Organizá tu casamiento" />
					</div>
					<div class="data">
						<h3>Organizá tu casamiento</h3>
						
					</div>
				</div><!-- .col-md-6 -->
				
				<div class="col col-md-3 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_publicacion_jornadas.png'); ?>" alt="Galería de Vestidos de Novia" />
					</div>
					<div class="data">
						<h3>Galería de Vestidos<br/>de Novia</h3>
						
					</div>
				</div><!-- .col-md-6 -->
				
				<div class="col col-md-3 col-xs-6 no_border">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_degustacion_jornadas.png'); ?>" alt="Shows, Degustaciones y Sorteos" />
					</div>
					<div class="data">
						<h3>Shows, Degustaciones<br/>y Sorteos</h3>
						
					</div>
				</div><!-- .col-md-6 -->
			</div><!-- .row -->
	
	
		</div><!-- #iconos_jornada_eventos -->
		
</div><!-- .container -->

<section id="te_de_novias_eventos">
	<div class="inner">
		<img class="logo_te" src="<?php echo base_url('/assets/images/logo_te_de_novias.png'); ?>" alt="Te de novias" />
		<p class="big_txt">Los <strong>Té de Novias son únicos</strong>, especiales, intensos y muy divertidos, ideales para <strong>pasar una tarde genial! Solicitá tu lugar!</strong></p>
		
		<h3>PRÓXIMOS TÉ DE NOVIAS</h3>
		<p>Expertos de diferentes rubros nos darán <strong>consejos, tips</strong> y nos hablarán de las <strong>últimas tendencias en Casamientos.</strong></p>
		
		<div class="row">
			<div class="col col-md-4 col-xs-4">
				<div class="img_wrapper">
					<a href="<?php echo base_url('eventos/te-de-novias-en-buenos-aires'); ?>"><img src="<?php echo base_url('/assets/images/te_de_novias_buenos_aires.jpg'); ?>" alt="Té de novias en Buenos Aires" /></a>
					<h4>Buenos Aires</h4>
				</div>	
				
				<p>16 de agosto de 2017</p>
				<!--<h6>(Proximamente)</h6> -->
				<a href="<?php echo base_url('eventos/te-de-novias-en-buenos-aires'); ?>" class="btn btn-default">Más información</a>
			</div>
			
			<div class="col col-md-4 col-xs-4">
				<div class="img_wrapper">
					<a href="<?php echo base_url('eventos/te-de-novias-en-rosario'); ?>"><img src="<?php echo base_url('/assets/images/te_de_novias_rosario.jpg'); ?>" alt="Té de novias en Rosario" /></a>
					<h4>Rosario</h4>
				</div>	
				
				<!-- <h5>13 de Noviembre</h5> -->
				<h6>(Proximamente)</h6>
				<a href="<?php echo base_url('eventos/te-de-novias-en-rosario'); ?>" class="btn btn-default">Más información</a>
			</div>
			
			<div class="col col-md-4 col-xs-4">
				<div class="img_wrapper">
					<a href="<?php echo base_url('eventos/te-de-novias-en-cordoba'); ?>"><img src="<?php echo base_url('/assets/images/te_de_novias_cordoba.jpg'); ?>" alt="Té de novias en Córdoba" /></a>
					<h4>Córdoba</h4>
				</div>	
				
				<!--<p>(Proximamente)</p> -->
				<p>5 de Julio del 2017</p>
				<a href="<?php echo base_url('eventos/te-de-novias-en-cordoba'); ?>" class="btn btn-default">Más información</a>
			</div>
			
		</div> <!-- .row -->
		
		
		
	</div><!-- .inner -->
</section>

<div id="otros_medios" class="container">
	<div id="split">
	
		<div id="primary">
			<h2 class="title_sep">Revistas</h2>
			<div class="row gum_60 revista_single">
				<div class="col-md-4">
					<img src="<?php echo base_url('/assets/images/revista_fiancee.jpg'); ?>" alt="Revista Fiancee" />
					<ul>
						<li><a href="http://www.fiancee.com.ar" target="_blank"><i class="fa fa-car"></i>www.fiancee.com.ar</a></li>
						<li><a href="mailto:info@fiancee.com.ar"><i class="fa fa-envelope"></i>info@fiancee.com.ar</a></li>
						<li><a href="http://www.facebook.com/revistadenovias" target="_blank"><i class="fa fa-facebook"></i>/revistadenovias</a></li>
						<li><a href="http:///www.instagram.com/Fianceearg" target="_blank"><i class="fa fa-instagram"></i>/fianceearg</a></li>
					</ul>
				</div>
				
				<div class="col-md-8">
					<h4>Fiancee</h4>
					<p><strong>Ya salió la nueva edición de Fiancée, colección anual de novias!</strong></p>
					
					<p>Este número, con una nueva sección: Colección 15 años! Todo lo que necesitas para estar diosa en tu fiesta!<br />
					Además y como siempre, las producciones con los más destacados diseñadores nacionales , cortejo para los más chicos y lo más exclusivo de las pasarelas europeas . Cobertura del Bridal week de Barcelona adelanto exclusivo 2017!</p>	
					<p><strong>Producción de fotos:</strong><br />
						Especial, íntimo dedicado a lencería, trajes de baño. Anímate!</p>

					<p><strong>Notas:</strong><br />- Luna de Miel en Porto de Galhinas y Ushuaia. Enamórate!.<br />
- Lo último en mesa dulce, ambientación , salones para civiles y más ¡<br />
- Entrevista exclusiva a Gabriel Lage por sus 25 años años en la moda!<br />
- Looks de Novias que enamoran.
					</p>

					<p>No te la pierdas, regálate Fiancée, la mejor revista de novias!<br /></p>
					<p><strong>¡Encontrala en todos los kioscos!</strong></p>
				</div>
				
			</div><!-- .revista -->
			
			<div class="row gum_40 revista_single">
				<div class="col-md-4">
					<img src="<?php echo base_url('/assets/images/revista_novias_magazine.jpg'); ?>" alt="Revista Novias Magazine" />
					<ul>
						<li><a href="http://www.noviasmagazine.com" target="_blank"><i class="fa fa-car"></i>www.noviasmagazine.com</a></li>
						<li><a href="mailto:info@noviasmagazine.com"><i class="fa fa-envelope"></i>info@noviasmagazine.com</a></li>
						<li><a href="http://www.facebook.com/revistanoviasmagazine" target="_blank"><i class="fa fa-facebook"></i>/revistanoviasmagazine</a></li>
						<li><a href="http:///www.instagram.com/noviasmagazine" target="_blank"><i class="fa fa-instagram"></i>/noviasmagazine</a></li>
					</ul>
				</div>
				
				<div class="col-md-8">
					<h4>Novias Magazine</h4>
					<p>Para estas fechas en las que el año se va apagando, los días se escabullen y sentimos que no nos alcanzan, entre compromisos sociales y tantas cosas por terminar, en Novias Magazine siempre nos hacemos un tiempo para hacer una pausa, frenar, conectarnos y reflexionar. Mirar atrás es una linda forma de motivarnos para seguir siempre para adelante, detectar aquello en lo que nos superemos, ese detalle que nos hizo crecer y sentir alegría, y quizás, también, lo que aún tenemos para seguir mejorando. Qué placer que siempre haya cosas por mejorar y cambiar, es éste el motor perfecto para nunca quedarse en un mismo lugar. Avanzar, siempre avanzar.
</p>
					<p>En este último número del año, con el calorcito que se cuela en cada rincón, les mostramos ambientaciones al aire libre, frescas y cerca de la naturaleza, como debe ser por estas épocas.</p>
					<p>Moda, las últimas tendencias, vestidos de novia y de fiesta, para que se empapen de diseño y buen gusto y pasen un verano a puro glamour. Además, Real Weddings cerca del verde, entrevistas a jóvenes profesionales que, desde lugar dentro del mundo casamientos, pueden aportar mucho para el suyo; y una charla exclusiva con el fotógrafo del momento, José Pereyra Lucena.</p>
					<p>Disfruten nuestras páginas dedicadas a todas ustedes, mujeres enamoradas e 
ilusionadas cerca de dar el gran paso de sus vidas. Para nosotros es un honor acompañarlas en este recorrido.</p>
					<p><strong>¡Feliz año y nos reencontramos pronto!</strong></p>
				</div>
				
			</div><!-- .revista -->
			
			<div id="fiancee_tv">
				<h2 class="title_sep">Fiancee TV</h2>
				<div class="row">
					<div class="col-md-4">
						<img src="<?php echo base_url('/assets/images/fiancee_tv.jpg'); ?>" alt="Fiancee TV" />
						<ul>
							<li><a href="http://www.fianceetv.com.ar/" target="_blank"><i class="fa fa-car"></i>www.fianceetv.com.ar</a></li>
							<li><a href="mailto:info@fianceetv.com.ar"><i class="fa fa-envelope"></i>info@fianceetv.com.ar</a></li>
							<li><a href="javascript:;" class="no_link" target="_blank"><i class="fa fa-phone"></i>4765-5445</a></li>
							
						</ul>
					</div>
					
					<div class="col-md-8">
						<h4>El único programa pensado para las novias.</h4>
						<p>El más alto nivel en diseño y las últimas tendencias de la moda.<br />Desfiles, destinos y notas a los mejores proveedores del rubro.</p>
						<p>Te esperamos todos los viernes pasada la medianoche.<br /><strong>Sábados 0.30 hs. Por Canal Metro.</strong></p>
						<p>Señales: 3 Cablevisión I 13 CV Telered I Digital 413 y 644 y Repetidoras en todo el País.</p>
					</div>
				</div>		
			</div><!-- fiancee_tv -->
			
		</div><!-- #primary -->
		
		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
			<?php include('notas_instagram.php'); ?>
			<?php if(isset($banners[184]) && $banners[184] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[184]) && $banners[184] ? $banners[184] : '<p>Medios y Eventos 300x250<span>Posicion 1</span></p>'; ?></div>
			<?php }
			if(isset($banners[185]) && $banners[185] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[185]) && $banners[185] ? $banners[185] : '<p>Medios y Eventos 300x250<span>Posicion 2</span></p>'; ?></div>
			<?php }
			if(isset($banners[191]) && $banners[191] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[191]) && $banners[191] ? $banners[191] : '<p>Medios y Eventos 300x250<span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
			
			
		</aside><!-- #primary -->
		
	</div><!-- #split -->
</div><!-- #otros_medios -->
<?php include('footer.php'); ?>