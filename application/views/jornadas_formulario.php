<?php echo form_open(base_url('/eventos/expo-novias-' . date('Y') . '/gracias#gracias'), 'method="POST" role="form" class="form" id="form_jornadas_registro"'); ?>
	<input type="hidden" name="id_origen" value="0" />
	<input type="hidden" name="id_evento" value="<?php echo $id_evento; ?>" class="id_evento" />
	<input type="hidden" value="<?php echo $id_evento; ?>" class="id_evento_novios" />
	<input type="hidden" value="<?php echo $id_evento_empresa; ?>" class="id_evento_empresa" />
	
	<div class="dialog_corner"></div>

	<h3>Solicitá tu entrada</h3>
	<?php if(validation_errors()){ ?>
		<div class="errores-generales">	
			<div><i class="fa fa-exclamation-circle"></i> 
				<?php echo validation_errors(); ?>
			</div>	
		</div> <!-- .errores-generales -->	
	<?php }else{ ?>
		<p>Completá tus datos para reservar un lugar. La entrada es <span class="pink">gratuita</span> y el <span class="pink">cupo limitado</span>.</p>
	<?php } ?>

	<div class="form-group has-feedback">
		<select name="tipo" required class="form-control jornada_tipo">
			<option value="novios" <?=set_value('tipo') == 'novios' ? 'selected="selected"' : '';?>>Soy novia</option>
			<option value="empresa" <?=set_value('tipo') == 'empresa' ? 'selected="selected"' : '';?>>Soy una empresa</option>
		</select>
		<div class="help-block with-errors"></div>
	</div>

	<h5 class="datos_casamiento">Datos del casamiento</h5>

	<div class="form-group has-feedback datos_casamiento">
		<input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']; ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
		<span class="fa form-control-feedback" aria-hidden="true"></span>
		<div class="help-block with-errors"></div>
	</div>

	<div class="form-group has-feedback">
		<input type="text" name="invitados" placeholder="Cantidad de Invitados"  value="<?php echo $data_form['invitados']; ?>" class="form-control invitados" required />
		<span class="fa form-control-feedback" aria-hidden="true"></span>
    	<div class="help-block with-errors"></div>
	</div>

	<div class="form-group has-feedback gum_30 datos_empresa">
		<input type="text" name="empresa" placeholder="Empresa" value="<?php echo set_value('empresa'); ?>" class="form-control" required />
		<span class="fa form-control-feedback" aria-hidden="true"></span>
		<div class="help-block with-errors"></div>
	</div>

	<h5>Datos de contacto</h5>

    <div class="form-group has-feedback">
		<input type="text" name="nombre" required placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control">
		<span class="fa form-control-feedback" aria-hidden="true"></span>
    	<div class="help-block with-errors"></div>
	</div>
	
	 <div class="form-group has-feedback">
		<input type="text" name="apellido" required placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control">
		<span class="fa form-control-feedback" aria-hidden="true"></span>
    	<div class="help-block with-errors"></div>
	</div>
	
	 <div class="form-group has-feedback">
		<input type="text" name="telefono" required placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido">
		<span class="fa form-control-feedback" aria-hidden="true"></span>
    	<div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback">
		<input type="email" name="email" required placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control">
		<span class="fa form-control-feedback" aria-hidden="true"></span>
    	<div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback datos_casamiento">
		<input type="text" name="dni" required placeholder="DNI" value="<?php echo set_value('dni'); ?>" class="form-control">
		<span class="fa form-control-feedback" aria-hidden="true"></span>
    	<div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback datos_empresa">
		<select name="id_rubro" required class="form-control">
			<option value="">Rubro</option>
			<?php if($combo_rubros) foreach ($combo_rubros as $key => $rubro){ ?>
				<option value="<?php echo $rubro['id']; ?>" <?= set_value('id_rubro') == $rubro['id'] ? 'selected="selected"' : '';?>><?php echo $rubro['rubro']; ?></option>
			<?php } ?>
		</select>
		<div class="help-block with-errors"></div>
	</div>

	<div class="form-group has-feedback">
		<select name="id_provincia" required class="form-control provincias">
			<option value="">Provincia</option>
			<?php if($provincias) foreach ($provincias as $k => $provincia) { ?>
				<option value="<?php echo $k; ?>" <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
			<?php } ?>
		</select>
		<div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback">
		<select name="id_localidad" required class="form-control localidades">
			<option value="">Localidad</option>
			<?php if($localidades) foreach ($localidades as $k => $localidad) { ?>
				<option value="<?php echo $k; ?>" <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
			<?php } ?>
		</select>
		<div class="help-block with-errors"></div>
	</div>

	<div class="form-group has-feedback">
		<div class="checkbox check_form">
			  <label for="infonovias"><input type="checkbox" id="infonovias" name="infonovias" value="1" <?php if($data_form['infonovias']){ echo 'checked="checked"'; } ?> />Deseo recibir infonovias</label>
		</div>
		<span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
	    <div class="clear"></div>
	</div>
	
	<div class="form-group has-feedback gum">
		<textarea placeholder="Comentarios" name="comentario" class="form-control"></textarea>
		<div class="help-block with-errors"></div>
	</div>
	
	<input class="btn btn-default disabled submit" type="submit">
</form>