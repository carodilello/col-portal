<?php if($proveedores){ ?>
	<section id="destacados_home">
		<div id="carousel-destacados-home" class="inner carousel slide carrusel_proveedores carousel_responsive" data-ride="carousel" data-pause="hover" data-interval="false" data-content="proveedores_relacionados" data-vars="<?php echo $proveedor['id_rubro']; ?>,<?php echo $proveedor['id_proveedor']; ?>">
			<h2 class="align_c gum_30">
				Otros proveedores del rubro <?php echo $proveedor['rubro']; ?> en
				<span class="dropdown">
					<a class="green suc_selector" id="sucursales-nav-label-empresas-destacadas" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label-empresas-destacadas">
						<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
							<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>"><?=$suc["sucursal"];?></a>
						<?php } ?>
					</ul>
				</span>
			</h2>
			<div class="carousel-inner" role="listbox">
				<?php $str_mas = '';
				include('minisitio_carousel_inner.php'); ?>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-destacados-home" role="button" data-slide="prev">
				<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-destacados-home" role="button" data-slide="next">
				<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div><!-- #carousel-destacados-home -->
	</section><!-- #destacados_home -->
<?php }