<?php
$bodyclass = 'page minisitio';
include('header.php');
$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php
		include('breadcrumbs.php');
		include('mod_header_minisitio.php'); ?>
		<section id="wrapper_minisitio" class="row">
		
			<div id="content_minisitio" class="col-md-8">
			
				<div id="preguntas_frecuentes_minisitio" class="box clear big">
					<h2>Preguntas Frecuentes</h2>
					
					<div class="box_inner">
						<table id="tabla_preguntas_frecuentes">
							<?php include('minisitio_preguntas_frecuentes_tabla.php'); ?>
						</table><!-- #tabla_servicios_proveedor -->
					</div>
				</div><!-- #mapa_minisitio -->
					
			</div><!-- #content_minisitio -->	
			
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php include('minisitio_acciones.php'); ?>
			
				<?php include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->
			
			<div class="col-md-12">
				<?php include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->

		</section><!-- #wrapper_minisitio -->
		
	</div><!-- .container -->
	<?php 
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php include('footer.php'); ?>