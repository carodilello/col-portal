<div id="modal-derivador" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="telefono-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
           
            <div class="modal-body">
                <?php if(validation_errors()){ ?>
                    <div class="errores-generales"> 
                        <div><i class="fa fa-exclamation-circle"></i> 
                            <?php echo validation_errors(); ?>
                        </div>  
                    </div> <!-- .errores-generales -->  
                <?php } ?>

                <?php echo form_open(base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_r' . $rubro["id"]), 'id="form-derivador" method="POST" role="form" class="form"'); ?>
                    <input type="hidden" name="accion" value="procesar" />
                    <input type="hidden" name="dia_evento" value="<?php echo $session_form['dia_evento']; ?>" />
                    <input type="hidden" name="ubicacion" value="<?php echo $session_form['ubicacion']; ?>" />
                    <input type="hidden" name="invitados" value="<?php echo $session_form['invitados']; ?>" />
                    <input type="hidden" name="nombre" value="<?php echo $session_form['nombre']; ?>" />
                    <input type="hidden" name="apellido" value="<?php echo $session_form['apellido']; ?>" />
                    <input type="hidden" name="dia_nacimiento" value="<?php echo $session_form['dia_nacimiento']; ?>" />
                    <input type="hidden" name="email" value="<?php echo $session_form['email']; ?>" />
                    <input type="hidden" name="telefono" value="<?php echo $session_form['telefono']; ?>" />
                    <input type="hidden" name="id_provincia" value="<?php echo $session_form['id_provincia']; ?>" />
                    <input type="hidden" name="id_localidad" value="<?php echo $session_form['id_localidad']; ?>" />
                    <input type="hidden" name="comentario" value="Solicito me envíen información acerca de sus servicios para mi evento. Muchas gracias." />
					
					<div id="derivador_txt">
						<img src="<?php echo base_url('/assets/images/icon_derivador.png'); ?>" />
						<h2>Las mejores opciones<span>para tu Casamiento</span></h2>
	                    
	
	                    <p class="gum">Solicitá y compará diferentes presupuestos para conseguir el más adecuado para tu casamiento.</p>
	                    <div id="summary" style="display:none">Por favor ingresar al menos un rubro</div>
					</div><!-- #derivador_txt -->
					
					<div id="derivador_items">
						<div class="row">
						<p class="col-md-12">Los novios interesados en <strong><?=isset($rubro["rubro"])&&$rubro["rubro"]?$rubro["rubro"]:''; ?></strong> solicitaron presupuesto a los siguientes rubros.</p>
	                    <?php 
	                    $tiene_empresas = FALSE;
	                    foreach ($rubros_derivados as $key => $derivado){
	                        if(!$derivado['proveedor']){ ?>
	                            <div class="item col-md-4">
	                            	<div class="box">
	                                	<img src="<?php echo 'http://www.casamientosonline.com/images/cont/derivador/'.$derivado['id_rubro_recibe']; ?>.jpg" />
	                                	<h3><?php echo $derivado['rubro']; ?></h3>
	                                	<input type="checkbox" name="id_rubro[]" value="<?php echo $derivado['id_rubro_recibe']; ?>" class="require-one" checked="checked" id="check_<?php echo $key; ?>" />
	                                </div>		
	                            </div>
	                        <?php }else{
	                            $tiene_empresas = TRUE;
	                        }
	                    }
	
	                    if($tiene_empresas){ ?>
	                        <h3 class="gum">Empresas</h3>
	
	                        <?php
	                        foreach ($derivado['proveedor'] as $key => $derivado){
	                            if($derivado['proveedor']){ ?>
	                                <div class="item empresa col-md-4">
	                                	<div class="box">
	                                    	<img src="http://media.casamientosonline.com/logos/<?php echo $derivado['id_prov_recibe']; ?>.jpg" />
	                                    	<h3><?php echo $derivado['proveedor']; ?></h3>
	                                    	<input type="checkbox" name="id_proveedor[]" value="<?php echo $derivado['id_prov_recibe']; ?>" class="require-one" checked="checked" />
	                                    </div>
	                                </div>
	                            <?php }
	                        }
	                    } ?>
						</div>
						<div class="bottom">
	                    	<a href="javascript:;" class="volver">No gracias, volver al sitio</a>
	                    	<input type="submit" class="btn-default submit_derivador btn-green" value="Solicitar presupuesto a estos rubros" />
	                    </div>	
	                    
                    </div><!-- #derivador_items -->
                </form>
            </div>
            </form>
        </div> <!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div><!-- #modal-sucursal -->