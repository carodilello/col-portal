<div class="row">
	<div class="form-group has-feedback col-md-6">
	    <input type="text" name="nombre" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control" required>
	    <span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback col-md-6">
	    <input type="text" name="apellido" placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control" required>
	    <span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback col-md-6">
	    <input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control" required />
	    <span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback col-md-6">
	    <input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
	    <span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
	</div>
	
	<div class="form-group has-feedback col-md-6">
	    <input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']; ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
	    <span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
	</div>
</div>	
	
<div class="clear form-group has-feedback checkbox check_form">
	    <label for="deseo_recibir_popup_<?php echo $var_form_telefono; ?>"><input type="checkbox" id="deseo_recibir_popup_<?php echo $var_form_telefono; ?>" name="infonovias" value="1" <?php if($data_form['infonovias']){ echo 'checked="checked"'; } ?> />Deseo recibir infonovias y promos</label>
	    
	    <span class="fa form-control-feedback" aria-hidden="true"></span>
	    <div class="help-block with-errors"></div>
</div>
<input type="submit" class="btn-default submit" value="Enviar Consulta" />




