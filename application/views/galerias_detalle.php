<?php
$bodyclass = 'galerias galeria_detalle';
include('header.php');

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>',
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
);

$secciones = '';
if(!empty($galeria['seccion'])){
	$tmp_secciones = explode('~', $galeria['seccion']);
	if(is_array($tmp_secciones)) foreach ($tmp_secciones as $seccion_str){
		if($seccion_str){
			$tmp_seccion = explode('@', $seccion_str);
			if(isset($tmp_seccion[1])) $secciones .= ' | '.$tmp_seccion[1];
		}
	}
}

$proveedores = array();
if(!empty($galeria['proveedores'])){
	$tmp_proveedores = explode('~', $galeria['proveedores']);
	if(is_array($tmp_proveedores)) foreach ($tmp_proveedores as $proveedor_str){
		if($proveedor_str){
			$tmp_proveedor = explode('@', $proveedor_str);
			if(isset($tmp_proveedor[1])) $proveedores[] = $tmp_proveedor[1];
		}
	}
}

$rubros = array();
if(!empty($galeria['rubros'])){
	$tmp_rubros = explode('~', $galeria['rubros']);
	if(is_array($tmp_rubros)) foreach ($tmp_rubros as $proveedor_str){
		if($proveedor_str){
			$tmp_rubro = explode('@', $proveedor_str);
			if(isset($tmp_rubro[1])) $rubros[] = $tmp_rubro;
		}
	}
}

$tags = array();
if(!empty($galeria['tags'])){
	$tmp_tags = explode('~', $galeria['tags']);
	if(is_array($tmp_tags)) foreach ($tmp_tags as $proveedor_str){
		if($proveedor_str){
			$tmp_tag = explode('@', $proveedor_str);
			if(isset($tmp_tag[1])) $tags[] = $tmp_tag;
		}
	}	
}

$fotos = array();
if(!empty($galeria['thumbs'])){
	$tmp_fotos = explode('~', $galeria['thumbs']);
	if(is_array($tmp_fotos)) foreach ($tmp_fotos as $foto_str){
		if($foto_str){
			$tmp_foto = explode('@', $foto_str);
			if(!empty($tmp_foto[1]) && !empty($tmp_foto[2])) $fotos[] = array('imagen' => $tmp_foto[0].'_chica.'.$tmp_foto[1], 'titulo' => $tmp_foto[2]);
		}
	}	
} ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/fotos-casamiento'); ?>">Galerias</a></li>
		<li><?php echo $galeria['titulo_galeria']; ?></li>
	</ul>
	<h1><?php echo $galeria['titulo_galeria']; ?></h1>
	<span class="proveedor_rubro title_sep"><strong class="pink"><?php echo implode(', ', $proveedores); ?></strong><?php echo $secciones ? ($proveedores ? ' - ' : '' ) . ltrim($secciones, ' | ') : ''; ?></span>
	
	<div id="split">
		
		<section id="primary">
			<div class="single galeria_standard" id="galeria_container">
				<div id="carousel-galeria" class="big_pic carousel slide" data-ride="carousel" data-pause="hover" data-interval="false">
					<div class="carousel-inner" role="listbox">
						<?php foreach ($fotos as $k => $foto){ ?>
							<div class="align_c item <?=$k==0 ? 'active' : '';?>">	
						 		<a href="http://media.casamientosonline.com/images/<?php echo str_replace('_chica', '_grande', $foto['imagen']); ?>" class="item_minis" data-gallery title="<?php echo $foto['titulo']; ?>">
							    	<img src="http://media.casamientosonline.com/images/<?php echo str_replace('_chica', '_grande', $foto['imagen']); ?>" alt="<?php echo $foto['titulo']; ?>">
							    </a>
							    <div class="epigrafe">
									<span class="col-md-10"><?php echo $foto['titulo']; ?></span>
									<div class="social col-md-2">
										<input type="hidden" class="id_redes" value="galerias/detalle/<?php echo $id_galeria; ?>" />
										<a class="compartir_facebook no_margin" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
										<a class="twitter" href="https://twitter.com/intent/tweet?url=<?php echo base_url('/galerias/detalle/'.$id_galeria); ?>&text=<?php echo urlencode($galeria["titulo_galeria"]);?>"><i class="fa fa-twitter"></i></a>
										<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/galerias/detalle/'.$id_galeria); ?>&description=<?php echo $galeria["titulo_galeria"];?>&media=http://media.casamientosonline.com/images/<?php echo str_replace('_chica', '_grande', $foto['imagen']); ?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
									</div>
								</div>
							</div>
						<?php } ?>																														    	 		
					</div><!-- #carousel-inner -->
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-galeria" role="button" data-slide="prev">
						<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
						<span class="sr-only valign">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-galeria" role="button" data-slide="next">
						<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div><!-- #carousel-galerias -->
				<div class="thumbs">
					<?php foreach ($fotos as $k => $foto){ ?>
						<a href="javascript:;" class="thumb_c"><img class="col-md-2" data-target="#carousel-galeria" data-slide-to="<?php echo $k; ?>" src="http://media.casamientosonline.com/images/<?php echo $foto['imagen']; ?>" alt="<?php echo $foto['titulo']; ?>"></a>
					<?php } ?>
				</div>
			</div><!-- #galeria_standard -->
			
			<p class="gum_40"><?php echo $galeria['descripcion']; ?></p>

			<?php if($rubros){ ?>
				<div id="rubros_galeria">
					<h3>Rubros asociados</h3>
					<?php foreach ($rubros as $k => $rubro){ ?>
						<a href="<?php echo base_url('/' . $rubro[5] . '/proveedor/' . $rubro[4] . '_CO_r' . $rubro[0]); ?>" class="btn-default"><?php echo $rubro[1] . ' en ' . $rubro[2]; ?></a>
					<?php } ?>
				</div>
			<?php } ?>
			
			<?php if($tags){ ?>
				<div id="tags_galeria">
					<h3><i class="fa fa-tag"></i>Tags:</h3>
					<?php foreach ($tags as $k => $tag) { ?>
						<a href="<?php echo base_url('/fotos-casamiento/' . $tag[2] . '_CO_t' . $tag[0]); ?>"><?php echo $tag[1]; ?></a>
					<?php } ?>
				</div>
			<?php }
			
			if(isset($tmp_proveedor[7]) && $tmp_proveedor[7] && isset($tmp_proveedor[2]) && $tmp_proveedor[2]){ ?>
			<div id="galeria_empresa_relacionada">
				<h3>Empresas relacionadas</h3>
				<div class="empresa">
					<a class="img_wrapper" href="<?php echo str_replace('{?}', $tmp_proveedor[8], $base_url_subdomain) . $tmp_proveedor[9]; ?>"><img src="http://media.casamientosonline.com/logos/<?php echo $tmp_proveedor[7]; ?>" /></a>
					<div class="data">
						<h4><a href="<?php echo str_replace('{?}', $tmp_proveedor[8], $base_url_subdomain) . $tmp_proveedor[9]; ?>"><?php echo implode(', ', $proveedores); ?></a></h4>
						<span class="zona"><?php echo $secciones ? ($proveedores ? ' ' : '' ) . ltrim($secciones, ' | ') : ''; ?></span>
						<a class="btn-default btn" href="<?php echo str_replace('{?}', $tmp_proveedor[8], $base_url_subdomain) . $tmp_proveedor[9]; ?>">Ver Minisitio</a>
					</div>
				</div>	

			</div><!-- #galeria_empresa_relacionada -->
			<?php } ?>		
			
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
									
			<?php if(isset($banners[176]) && $banners[176] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[176]) && $banners[176] ? $banners[176] : '<p>Galeria Detalle 300x250<span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[177]) && $banners[177] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[177]) && $banners[177] ? $banners[177] : '<p>Galeria Detalle 300x250<span>Posicion 2</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[178]) && $banners[178] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[178]) && $banners[178] ? $banners[178] : '<p>Galeria Detalle 300x250<span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #sidebar -->
		
	</div><!-- #split -->
</div><!-- .container -->
<?php 
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
$flechas = TRUE;
include('popup_fotos.php');
include('footer_info.php'); ?>