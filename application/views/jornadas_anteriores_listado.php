<h3>Jornadas Anteriores</h3>
<ul>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2016'); ?>"><strong class="pink">2016</strong> - Jornadas 38º y 39º </a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2015'); ?>"><strong class="pink">2015</strong> - Jornadas 36º y 37º </a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2014'); ?>"><strong class="pink">2014</strong> - Jornadas 34º y 35º </a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2013'); ?>"><strong class="pink">2013</strong> - Jornadas 32º y 33º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2012'); ?>"><strong class="pink">2012</strong> - Jornadas 30º y 31º </a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2011'); ?>"><strong class="pink">2011</strong> - Jornadas 28º y 29º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2010'); ?>"><strong class="pink">2010</strong> - Jornadas 26º y 27º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2009'); ?>"><strong class="pink">2009</strong> - Jornadas 24º y 25º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2008'); ?>"><strong class="pink">2008</strong> - Jornadas 22º y 23º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2007'); ?>"><strong class="pink">2007</strong> - Jornadas 20º y 21º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2006'); ?>"><strong class="pink">2006</strong> - Jornadas 18º y 19º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2005'); ?>"><strong class="pink">2005</strong> - Jornadas 16º y 17º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2004'); ?>"><strong class="pink">2004</strong> - Jornada 15º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2003'); ?>"><strong class="pink">2003</strong> - Jornadas 11º, 12º, 13º y 14º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2002'); ?>"><strong class="pink">2002</strong> - Jornadas 8º, 9º y 10º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2001'); ?>"><strong class="pink">2001</strong> - Jornadas 3º , 4º, 5º, 6º y 7º</a></li>
	<li><a href="<?php echo base_url('/eventos/expo-novias-2000'); ?>"><strong class="pink">2000</strong> - Jornadas 1º y 2º </a></li>
</ul>