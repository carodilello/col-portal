<?php
$bodyclass = 'page ceremonias_detalle directorio';
include('header.php');  ?>
<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-templos-judios-en-argentina'); ?>">Templos Judíos</a></li>
		<li>Directorio</li>
	</ul>
	<h1 class="title_sep">Templos Judíos<span>Sedes y directorios</span></h1>
	<img class="big_pic border_templ" src="<?php echo base_url('/assets/images/big_pic_templos_judios2.jpg'); ?>" alt="Templos Judíos, Sedes y directorios" />
	<div id="split">
		<section id="primary">
			<?php if($barrios){ ?>
				<div class="filtro">
					<h2 class="gum">Directorio</h2>
					<div class="pull-right">
						<label>Seleccioná tu barrio: </label>
						<select class="ubicacion_iglesias">
							<option value="">Todos</option>
							<?php foreach ($barrios as $barrio) { ?>
								<option value="<?php echo $barrio['id']; ?>"><?php echo $barrio['nombre']; ?></option>
							<?php } ?>
						</select>
					</div>	
				</div><!-- .filtro -->	
			<?php } ?>
			
			<div id="listado_directorio">
				<?php if(!empty($directorios)){ ?>
					<table class="caba directorio_cont">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Dirección</th>
								<th>Teléfono</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($directorios as $directorio){ ?>
								<tr class="tr_<?php echo $directorio['id_barrio']; ?>">
									<td class="head_mobile"><?php echo $directorio['nombre']; ?></td>
									<td><?php echo $directorio['direccion']; ?></td>
									<td><?php echo $directorio['telefono'] ? $directorio['telefono'] : '---'; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php }else{ ?>
					<p>No se han encontrado resultados</p>
				<?php } ?>	
			</div><!-- #listado_directorio -->
		</section><!-- #primary -->
		<aside id="secondary">
			<?php $templos_dir = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>