<?php
$bodyclass = 'acerca_nuestro site_proveedor';
include('header_site_proveedor.php');

?>



<section id="header_acerca_nuestro">
	<div class="container">
		<div class="row">
			<div class="col-md-6 txt">
				<h1 class="title_sep">Quienes somos</h1>
				<p class="big_txt">Casamientos Online es el <strong>primer y único portal de la Argentina dedicado a la temática de los casamientos de manera integral.</strong> Nació hacia fines del 1999 con grandes proyectos y acompañada por una ambiciosa campaña de comunicación.</p>
				<p><strong class="green">Información:</strong> Más de 1500 proveedores en 45 rubros con descripción de sus servicios, fotos, videos, testimonios, productos, promociones y paquetes.</p>
				<p><strong class="green">Tendencias:</strong> Contenido original generado por un departamento editorial propio con las últimas tendencias, informes y especiales.</p>
				<p><strong class="green">Cobertura:</strong> Una empresa 100% argentina con presencia en todo el país. Además contamos con estructura propia en Cordóba, Santa Fe, Rosario, Mendoza, Mar del Plata y Salta.</p>
			</div>
			
			
		</div>
		
	</div><!-- .container --> 
</section> <!-- #header_como_anunciar --> 

<div id="title_banda">
	<div class="inner relative">
		<img class="logos_viejos" src="<?php echo base_url('/assets/images/logos_viejos.jpg'); ?>" />
		<h2><i class="fa fa-play"></i>La Historia de Casamientos Online</h2>
	</div>
</div><!-- #title_banda --> 

<div id="historia_mobile" class="inner mobile_block">
	<p><strong>Nació hacia fines del 99</strong> con grandes proyectos y acompañada por una ambiciosa campaña de comunicación.</p>

<p>En el 2001, con la crisis económica y la ruptura de la burbuja de Internet, Casamientos Online logró sobrevivir <strong>demostrando ser una empresa sólida y con iniciativa que entiende a los proveedores y novios</strong>. El portal ofreció un espacio que permitió concentrar a los proveedores y a generar oportunidades de negocio a las empresas en medio de una fuerte crisis económica.</p>
<p>Desde un principio, Casamientos Online apostó al <strong>desarrollo de tecnología y su innovación</strong> le permitió lanzar aplicaciones de avanzada para novios y proveedores.</p>
<p>Desde el año 2007 se enfocó en el desarrollo y capacitación de sus clientes en el uso de herramientas web.</p>
<p>El diálogo y el respeto por el cliente y el usuario son dos de sus virtudes más destacadas.</p>

</div>

<div id="timeline">
	<div class="inner">
		<div id="line">
			<div class="anio bottom primero">
				<strong>1999</strong>
				<img src="<?php echo base_url('/assets/images/timeline-nacimiento-casamientosonline.jpg'); ?>" />
			</div>
			
			<div class="anio top segundo">
				<strong>2001</strong>
				<img src="<?php echo base_url('/assets/images/timeline-crecimiento-casamientosonline.jpg'); ?>" />
			</div>
			
			<div class="anio bottom tercero">
				<strong>2007</strong>
				<img src="<?php echo base_url('/assets/images/timeline-capacitacion-casamientosonline.jpg'); ?>" />
			</div>
			
			<div class="anio top cuarto">
				<strong>2017</strong>
				<img src="<?php echo base_url('/assets/images/timeline-lanzamiento-casamientosonline.jpg'); ?>" />
			</div>
			
			<div class="txt top primero">
				<h4>Nacimiento</h4>				
				<p>Nació hacia fines del 99 con grandes proyectos y acompañada por una ambiciosa campaña de comunicación.</p>
			</div>
			
			<div class="txt bottom segundo">
				<h4>Crecimiento</h4>				
				<p>En el 2001, con la crisis económica y la ruptura de la burbuja de Internet, Casamientos Online logró sobrevivir demostrando ser una empresa sólida y con iniciativa que entiende a los proveedores y novios. </p>
			</div>
			
			<div class="txt top tercero">
				<h4>Desarrollo y Capacitación</h4>				
				<p>Desde el año 2007 se enfocó en el desarrollo y capacitación de sus clientes en el uso de herramientas web.</p>
			</div>
			
			<div class="txt bottom cuarto">
				<h4>A la vanguardia</h4>				
				<p>Desde un principio, Casamientos Online apostó al desarrollo de tecnología y su innovación le permitió lanzar aplicaciones de avanzada para novios y proveedores. Año a año nos vamos renovando y el 2017 no es la excepción, lanzamos una nuevo portal con una imagen totalmente renovada.</p>
			</div>
				
		</div><!-- #line -->
	</div><!-- .inner -->
</div><!-- #timeline --> 


<div class="container">
	<section id="personalidad_filosofia">
		<div class="row">
			
			<h2 class="title_sep">Personalidad y Filosofía</h2>
			<p class="bajada">Casamientos Online es, por naturaleza, una empresa joven y como tal dinámica, en pleno proceso de crecimiento y expansión. Hoy define a la marca su identidad de éxito, basada en la superación constante.</p>
			
			<p class="bajada">Hay cuatro principios en los que se basa nuestro espíritu de trabajo.</p>
			
			<div class="col-md-6 col-xs-6">
				<p><strong class="green">1) Excelencia:</strong> La búsqueda constante de excelencia es parte de la naturaleza de Casamientos Online. Ser líderes es lo que motiva a superarse constantemente, en la innovación de tecnologías y en la generación propia de contenidos.</p>
			</div>
			
			<div class="col-md-6 col-xs-6">
				<p><strong class="green">2) Integridad:</strong> La transparencia ante todo. Casamientos Online abre el juego a todos sus competidores convencido del valor del portal y que de esta manera también se evoluciona. El cuidado de la confidencialidad de novios registrados e información publicada es respetar a todos los que intervienen en la comunidad.</p>
			</div>
			
			<div class="col-md-6 col-xs-6">
				<p><strong class="green">3) Pro Usuario:</strong> Casamientos Online responde a parámetros de usabilidad pensados a medida de sus usuarios. Se construye un espacio donde los novios encuentren en forma fácil y sencilla toda la información necesaria para organizar su casamiento. Un lugar donde las parejas encuentran contención, datos, novedades y consejos. Donde además pueden hacer uso útil de aplicaciones para la organización y comunicación de su casamiento. </p>
			</div>
			
			<div class="col-md-6 col-xs-6">
				<p><strong class="green">4) Pro Cliente:</strong> Las empresas encuentran en Casamientos Online un aliado. Una plataforma donde administrar su negocio en la web. A través del portal los proveedores hacen uso de un medio donde responder a las consultas por parte de los novios, ofrecer información acerca de su 
servicio, modificar, dar de alta y baja información acerca de su empresa. Desarrollar acciones de promoción y hacer uso de aplicaciones 
multimedia que enriquecen la comunicación de producto.</p>
			</div>
		</div>
	</section><!-- #personalidad_filosofia -->

</div><!-- .container -->

<section id="iconos_numeros">
	<div class="inner">
					
		<h3 class="clear title_sep_center">Los números de Casamientos Online</h3>
				
		<div class="row">
			<div class="col-md-4 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_visitas_diarias.png'); ?>" />
				</div>
				<div class="data">
					<strong>7.000</strong>
					<p>Visitas promedio diarias interactúan con el Portal.</p>
				</div>
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_visitas_mensuales.png'); ?>" />
				</div>
				<div class="data">
					<strong>180.000</strong>
					<p>Visitas mensuales al portal</p>
				</div>	
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_paginas.png'); ?>" />
				</div>
				<div class="data">
					<strong>600.000</strong>
					<p>Páginas vistas por mes</p>
				</div>	
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12 clear no_margin">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_novios.png'); ?>" />
				</div>
				<div class="data">
					<strong>45.000</strong>
					<p>Novios registrados anualmente</p>
				</div>
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12 no_margin">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_empresas.png'); ?>" />
				</div>
				<div class="data">
					<strong>1.500</strong>
					<p>Empresas publicando sus productos y Servicios</p>
				</div>
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12 no_margin">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_pedidos_anuales.png'); ?>" />
				</div>
				<div class="data">
					<strong>650.000</strong>
					<p>Pedidos de presupuestos anuales a las empresas</p>
				</div>	
			</div><!-- .col-md-4 -->
			
			
		</div>
		
		
			<p class="bajada target clear">Orientado a un target ABC1/C2, los usuarios primarios son <strong>Mujeres jóvenes de 25 a 35 años, Profesionales y de formación universitaria</strong>, próximas a casarse en un período no menor a los 6 meses. <strong>El 35% de los casamientos de Buenos Aires pasan por Casamientos Online.</strong></p>
			
		

		
		<div id="mercado_casamientos">
			<h2 class="title_sep">El mercado del Casamiento</h2>
			<div id="txt_mercado_casamientos" class="col-md-8 col-xs-6">
				<p class="bajada">Anualmente se realizan <strong>11.000 casamientos en la Ciudad de Buenos Aires y 48.000 en la provincia de Buenos Aires</strong>, mientras que la <strong>inversión promedio</strong> en la fiesta es de <strong>$80.000.-</strong></p>
				<p class="bajada gum_60">El mercado de los casamientos representa transacciones superiores a los <strong>1200 millones de pesos anuales,</strong> fuertemente concentradas en 3 rubros: <strong>La Fiesta</strong> (480 Millones de pesos), <strong>Luna de Miel</strong> (256 Millones de pesos) y <strong>Casas de Regalos</strong> (480 Millones de pesos).</p>
			</div>
			
			<div id="pic_mercado_casamientos" class="col-md-4 col-xs-6">
				<img src="<?php echo base_url('/assets/images/mercado_casamientos.png'); ?>" />
			</div>	
		</div><!-- #mercado_casamientos -->
		
		
	</div><!-- .inner -->
</section><!-- #iconos_numeros -->


<?php include('footer_info.php'); ?>