<?php
$bodyclass = 'contacto contacto_proveedor site_proveedor';
include('header_site_proveedor.php'); ?>
<div class="container">
	<h1 class="title_sep">Contacto para publicar en CasamientosOnline</h1>
	<div class="contacto_intro box gum_40">
		<p class="big">En Casamientos Online encontrarás el socio ideal para hacer crecer tu negocio y conseguir más y nuevos clientes ¡Se parte de nuestra comunidad y promocioná tu empresa!</p>
		<p>Para mayor información acerca del Portal y opciones de publicación, completá el siguiente formulario. A la brevedad una Ejecutiva de Cuentas se contactará con vos. Muchas gracias.</p>
	</div>
	<?php
	echo form_open(base_url('/empresas/contacto#gracias'),'id="form_presupuesto_wrapper" method="POST" role="form" class="form"'); ?>
		<input type="hidden" name="accion" value="proveedor" />
		<input type="hidden" name="apellido" value="-" />
		<input type="hidden" name="infonovias" value="1" />
		<input type="hidden" name="opcion_publicacion" value="Otro" />
		<input type="hidden" class="texto_por_defecto" value="Localidad" />
		<input type="hidden" class="rubro_activo" value="0" />
		
		<aside class="col-md-3">
			<h3>Telefónicamente</h3>
			<p class="no_margin">Lunes a Viernes de 9:30 a 18 hrs.</p>
			<ul class="gum_30">
				<li><strong>Sucursal Buenos Aires</strong></li>
				<li><i class="fa fa-phone"></i>11 5197.5525</li>
				<li class="no_border gum"><i class="fa fa-whatsapp"></i>+54 9 11 3196.1390</li>	
				
				<li><strong>Suc. Rosario / Santa Fe</strong></li>
				<li class="gum"><i class="fa fa-phone"></i><i class="fa fa-plus"></i><i class="fa fa-whatsapp"></i>+54 9 341 394.2961</li>	
				
				<li><strong>Suc. Córdoba y Resto del país</strong></li>
				<li><i class="fa fa-phone"></i><i class="fa fa-plus"></i><i class="fa fa-whatsapp"></i>+54 9 351 632.8367​</li>
			</ul>
			<h3>Vía Email</h3>
			<a href="mailto:info@casamientosonline.com"><i class="fa fa-envelope"></i>info@casamientosonline.com</a>
		</aside>

		<div id="form_presupuesto" class="col-md-9">
			<?php if($success){ ?>
				<div id="gracias">
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<p><i class="fa fa-check-circle-o"></i><span><strong>Gracias por contactarte!</strong> En CasamientosOnline valoramos mucho tu opinión. En breve serás contactado por un Ejecutivo de Cuentas.</span></p>
					</div>
				</div><!-- #gracias -->	
			<?php }elseif(validation_errors()){ ?>
				<div class="errores-generales">	
					<div><i class="fa fa-exclamation-circle"></i> 
						<?php echo validation_errors(); ?>	
					</div>	
				</div> <!-- .errores-generales -->	
			<?php }else{ ?>
				<p class="txt_form_sugerencia">Para mayor información acerca del portal y opciones de publicación, contactate con nosotros completando el siguiente formulario:</p>
			<?php } ?>
			<fieldset class="row no_margin_b">
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-building"></span>
					<input type="text" name="empresa" placeholder="Nombre Empresa"  value="<?php echo set_value('empresa'); ?>" class="form-control" required />
					<div class="help-block with-errors"></div>
				</div>
				
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-map-marker"></span>
					<select class="form-control sel_sucursal" required name="id_sucursal">
						<option value="">Publicar en...</option>
						<?php foreach ($sucursales as $suc){ ?>
							<option value="<?php echo $suc['id']; ?>" <?=set_value('id_sucursal') == $suc['id'] ? "selected='selected'" : "";?>><?php echo $suc['sucursal']; ?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"></div>
				</div>
				
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-gift"></span>
					<select class="form-control sel_rubro" required name="id_rubro">
						<option value="">Rubro</option>
						<?php foreach ($combo_rubros as $k => $grupo){ ?>
							<optgroup label="<?php echo $k; ?>">
								<?php foreach ($grupo as $j => $rubro) { ?>
									<option value="<?php echo $j; ?>" <?=set_value('id_rubro') == $j ? "selected='selected'" : "";?>><?php echo $rubro; ?></option>
								<?php } ?>
							</optgroup>
						<?php } ?>
					</select>
					<div class="help-block with-errors"></div>
				</div>
			</fieldset>
			
			<fieldset class="row no_margin_b">
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-user"></span>
					<input type="text" name="nombre" placeholder="Nombre y Apellido"  value="<?php echo set_value('nombre'); ?>" class="form-control" required />
					<div class="help-block with-errors"></div>
				</div>
				
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-envelope"></span>
					<input type="email" name="email" placeholder="Email"  value="<?php echo set_value('email'); ?>" class="form-control" required />
					<div class="help-block with-errors"></div>
				</div>

				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-phone"></span>
					<input type="text" name="telefono" placeholder="Teléfono"  value="<?php echo set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
					<div class="help-block with-errors"></div>
				</div>
			</fieldset>

			<fieldset class="row no_margin_b">
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-globe"></span>
					<input type="text" name="sitio_web" placeholder="Sitio web" value="<?php echo set_value('sitio_web'); ?>" class="form-control" required />
					<div class="help-block with-errors"></div>
				</div>

				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-map-marker"></span>
					<select name="id_provincia" class="provincias" required>
						<option value="">Seleccionar una Provincia</option>
						<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
							<option value="<?php echo $k; ?>" <?=$k==set_value('id_provincia') ? 'selected' : '';?>><?php echo $provincia; ?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"></div>
				</div>
				
				<div class="col-md-4 input_icon form-group has-feedback has-success">
					<span class="fa fa-location-arrow"></span>
					<select name="id_localidad" class="localidades" required>
						<option value="">Seleccionar una Localidad</option>
						<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
							<option value="<?php echo $k; ?>" <?=$k==set_value('id_localidad') ? 'selected' : '';?>><?php echo $localidad; ?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"></div>
				</div>
			</fieldset>
					
			<div class="form-group has-feedback">
				<textarea placeholder="Comentarios" name="comentario" required><?php echo set_value('comentario'); ?></textarea>
				<div class="help-block with-errors"></div>
			</div>

			<div class="clear">
				<div class="checkbox col-md-7 check_form no_padding has-feedback">
				  <label for="infonovias"><input id="infonovias" name="infonovias" type="checkbox" value="1" checked="checked">Deseo recibir el Infonovias y promociones de casamientos</label>
				</div>
				
				<input type="submit" class="btn-default submit_presupuesto disabled" value="Enviar consulta">
				<div class="has-error has-danger has-feedback">
					<div style="display:none; float:right;" class="error-zonas help-block with-errors">
						<ul class="list-unstyled">
							<li>Debe seleccionar al menos una zona</li>
						</ul>
					</div>
				</div>
			</div><!-- .clear -->
		</div>
	</form>
</div><!-- container -->
<?php include('footer_info.php'); ?>