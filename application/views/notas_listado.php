<?php
$bodyclass = 'page split notas notas_listado';
include('header.php');

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="container">

	<?php if(isset($banners[286]) && $banners[286] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[286]) && $banners[286] ? $banners[286] : '<p>Banner Editorial listado 970 x 90</p>'; ?></div>
	<?php } ?>
	
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/consejos'); ?>">Notas</a></li>
		<li><?php echo $nombre_seccion; ?></li>
	</ul>
	<h1 class="h_sep">Notas de <span class="green"><?php echo !empty($seccion_data['titulo']) ? $seccion_data['titulo'] : $nombre_seccion; ?></span></h1>
	
	<div id="split">
				
		<section id="primary">
			<?php if(isset($seccion_data['texto']) && $seccion_data['texto']){ ?>
				<p id="cat_nota_bajada" class="box"><?php echo $seccion_data['texto']; ?></p>
			<?php }elseif(!$flag_notas_empresas){ ?>
				<div class="filtros">
					<label>Filtrar Notas: </label>
					<select class="galeria_seccion">
						<option value=""> - Categoría - </option>
						<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '76' ? 'selected' : '';?> value="76" data-name="vestidos">Vestidos</option>
						<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '81' ? 'selected' : '';?> value="81" data-name="belleza-y-estilo">Belleza y Estilo</option>
						<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '93' ? 'selected' : '';?> value="93" data-name="la-organizacion">La Organización</option>
						<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '98' ? 'selected' : '';?> value="98" data-name="luna-de-miel">Luna de Miel</option>
						<option <?=isset($get['id_seccion']) && $get['id_seccion'] == '1055' ? 'selected' : '';?> value="1055" data-name="casamientos-reales">Casamientos reales</option>
					</select>
					<select class="galeria_subseccion">
						<option value=""> - Subcategoría - </option>
						<?php if(!empty($galeria_subseccion)) foreach ($galeria_subseccion as $k => $sub) { ?>
							<option value="<?php echo $k; ?>" <?=$get['id_subseccion'] == $k ? 'selected' : ''; ?> data-name="<?php echo $sub['name']; ?>"><?php echo $sub['text']; ?></option> 
						<?php } ?>
					</select>
				</div>
			<?php } ?>
			
			<?php if(!empty($nota_principal) && $pag < 2){ // SOLO SE MUESTRA EN LA PRIMERA PAGINA ?>
				<div id="nota_principal">
					<a href="<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>"><img src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota_principal['pic']); ?>" alt="<?php echo $nota_principal['titulo_pic']; ?>" /></a>
					<?php
					$nombre_subseccion = '';
					if($nota_principal['secciones']){
						$tmp_secciones = explode('~', $nota_principal['secciones']);
						foreach ($tmp_secciones as $key => $secc){
							$tmp_secc = explode('@', $secc);
							$nombre_subseccion .= $tmp_secc[1].', ';
						}
					}
					if($nombre_subseccion){ ?>
						<p class="cat_segundo_nivel"><?php echo rtrim($nombre_subseccion, ', '); ?></p>
					<?php } ?>
					<h2><a href="<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>"><?php echo $nota_principal['titulo']; ?></a></h2>
					<p><?php echo $nota_principal['copete']; ?></p>
					<a href="<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>" class="btn btn-default">Ver Nota</a>
					
					<div class="social">
						<input type="hidden" class="id_redes" value="consejos/<?php echo $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']; ?>" />
						<a class="compartir_facebook" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
						<a class="twitter" href="https://twitter.com/intent/tweet?url=<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>&text=<?php echo urlencode($nota_principal['titulo']);?>"><i class="fa fa-twitter"></i></a>
						<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/consejos/' . $nota_principal['titulo_seo'] . '_CO_n' . $nota_principal['id']); ?>&description=<?php echo $nota_principal['titulo'];?>&media=http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota_principal['pic']); ?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
					</div><!-- .social -->

					
				</div><!-- #nota_principal -->
			<?php } ?>

			<div id="notas_listado_segundo_nivel" class="row">
				<?php if(!empty($notas_destacadas) && $pag < 2){ ?>
					<h2 class="col-md-12 title_sep"><?php echo $nombre_seccion; ?></h2>
					<?php 
					$str_mas = '';
					$str_mas_titulo = '';
					foreach ($notas_destacadas as $k => $nota){ // SOLO SE MUESTRA EN LA PRIMERA PAGINA
						$tmp_secciones = explode('~', $nota['secciones']);
						$nombre_subseccion = '';
						foreach ($tmp_secciones as $key => $secc){
							$tmp_secc = explode('@', $secc);
							$nombre_subseccion .= $tmp_secc[1].', ';
						}

						if(strlen($nota['copete']) >= 135) $str_mas        = '...';
						if(strlen($nota['titulo']) >= 32)  $str_mas_titulo = '...'; ?>
						<div class="col-md-6">
							<div class="box">
								<?php if($nombre_subseccion){ ?>
									<p class="cat_segundo_nivel"><?php echo rtrim($nombre_subseccion, ', '); ?></p>
								<?php } ?>
								<a href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>" class="img_wrapper"><img src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" alt="<?php echo $nota['titulo_pic']; ?>" /></a>
								<h3><a <?php if(strlen($nota['titulo']) >= 32){ echo 'data-toggle="tooltip" data-placement="top"'; } ?> title="<?php echo $nota['titulo']; ?>" href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>"><?php echo mb_substr($nota['titulo'], 0, 32).$str_mas_titulo; ?></a></h3>
								<?php if($nota['autor']){ ?>
									<p class="autor">Por <strong><?php echo $nota['autor']; ?></strong></p>
								<?php } ?>
								<p class="descripcion"><?php echo mb_substr($nota['copete'], 0, 135).$str_mas; ?></p>
								<div class="actions">
									<div class="social">
										<input type="hidden" class="id_redes" value="consejos/<?php echo $nota['titulo_seo'] . '_CO_n' . $nota['id']; ?>" />
										<a class="compartir_facebook" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
										<a href="https://twitter.com/intent/tweet?url=<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>&text=<?php echo urlencode($nota['titulo']);?>"><i class="fa fa-twitter"></i></a>
										<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>&description=<?php echo $nota['titulo'];?>&media=http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
									</div>
									<a href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>">Seguir Leyendo <i class="fa fa-chevron-right"></i></a>
								</div><!-- .actions -->
							</div><!-- .box -->
						</div><!-- .col-md-6 -->	
					<?php 
						$str_mas = '';
						$str_mas_titulo = '';
					} ?>

					<div class="clear col-md-12">
						<div class="sep_line"></div>
					</div>
				<?php } ?>
				
				<?php 
				$str_mas = '';
				$str_mas_titulo = '';
				if($flag_notas_empresas) $show_image = FALSE;
				if(!empty($listado_notas)) foreach ($listado_notas as $k => $nota){
					$com = substr(strip_tags($nota['contenido']), 0, 140);
					$tmp_secciones = explode('~', $nota['secciones']);
					$nombre_subseccion = '';
					foreach ($tmp_secciones as $key => $secc){
						$tmp_secc = explode('@', $secc);
						$nombre_subseccion .= $tmp_secc[1].', ';
					}
					include('nota.php');
					$str_mas = '';
					$str_mas_titulo = '';
				} ?>
			</div><!-- #notas_listado_segundo_nivel -->
			<?php echo $this->pagination->create_links(); ?>
		</section>
		
		<aside id="secondary">
			<?php include('notas_categorias_sidebar.php');
			include('notas_facebook.php'); ?>
			
			<?php include('registro_info_novias_sidebar.php'); ?>
						
			<?php if(isset($banners[287]) && $banners[287] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[287]) && $banners[287] ? $banners[287] : '<p>Editorial listado 300x250 <span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			
			<?php include('notas_instagram.php'); ?>
			
			<?php include('notas_mas_leidas.php'); ?>
			
			<?php include('casamientos_tv_sidebar.php'); ?>
			
			<?php if(isset($banners[288]) && $banners[288] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[288]) && $banners[288] ? $banners[288] : '<p>Editorial listado 300x250 <span>Posicion 2</span></p>'; ?></div>
			<?php } ?>

			<?php if(isset($banners[289]) && $banners[289] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[289]) && $banners[289] ? $banners[289] : '<p>Editorial listado 300x250 <span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #sidebar -->
	</div><!-- #split -->		
</div><!-- .container -->
<?php include('footer.php'); ?>