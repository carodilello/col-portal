<div id="form_minisitio" class="box">
	<h2>Solicitar Información <i class="fa fa-comment pull-right"></i></h2>

	<h3><?php echo $proveedor['proveedor']; ?></h3>
	<p class="rubro"><?php echo $proveedor['rubro']; ?></p>

	<?php if(!empty($_SESSION['error_formulario'])){ ?>
		<div class="errores-generales">
			<div><i class="fa fa-exclamation-circle"></i>
				A ocurrido un error inesperado, por favor reintente en unos instantes
			</div>
		</div> <!-- .errores-generales -->
	<?php } ?>

	<?php if($config['mantenimiento_site'] || $config['mantenimiento_presupuesto']){ ?>
		<p>Estamos realizando tareas de mantenimiento. Intente nuevamente en unos minutos. Muchas gracias.</p>
	<?php }else{ ?>
	
	<form action="<?php echo base_url('/formulario/procesar_pedido'); ?>" id="form_presupuesto" method="POST" role="form" class="form">
		<?php if($prov_vencido){ ?>
			<p id="mensaje_vencido_formulario"><strong class="block"><i class="fa fa-warning"></i><?php echo isset($elemento)&&$elemento?$elemento:'Minisitio'; ?> vencid<?=isset($elemento)&&$elemento=='Promoción'?'a':'o';?></strong><span>Dejá tus datos para enviarte info relacionada a tu busqueda.</span></p>
		<?php } ?>

		<h5><?=$datos_viaje?'Cuándo te Casas?':'Datos del Casamiento';?></h5>

		<div class="form-group has-feedback">
			<input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']; ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
			<span class="fa form-control-feedback" aria-hidden="true"></span>
			<div class="help-block with-errors"></div>
		</div>

		<?php if(!$prov_vencido){
			if(isset($asociados_trabaja)&&$asociados_trabaja) foreach($asociados_trabaja as $asociados_l){ foreach($asociados_l as $asociado){ ?>
				<input type="hidden" name="asociados[]" value="<?php echo $asociado['id_asociado']; ?>" />
			<?php } }
		} ?>

		<?php if($prov_vencido){ ?>
			<input type="hidden" name="estado_cola" value="10" />
		<?php }else{ ?>
			<input type="hidden" name="randomID" value="<?php echo rand(99, 999999); ?>">
			<input type="hidden" name="microtime" value="<?php echo microtime(); ?>">
		<?php } ?>
		<input type="hidden" name="formulario_lateral" value="1" />
		<input type="hidden" name="subdominio"  value="<?php echo $proveedor['subdominio']; ?>">

		<?php if($hidden) foreach ($hidden as $hid){ ?>
			<input type="hidden" name="<?php echo $hid['name']; ?>" value="<?php echo $hid['value']; ?>" />
		<?php } ?>

		<?php if($fecha_variable && !$prov_vencido){ ?>
			<div id="fecha_variable_input" class="form-group has-feedback">
				<div class="checkbox check_form">
					<label for="fecha_evento_variable"><input name="fecha_evento_variable" type="checkbox" id="fecha_evento_variable" value="1" class="form-control" />La fecha del evento puede variar</label>
				</div>
				<span class="fa form-control-feedback" aria-hidden="true"></span>
			    <div class="help-block with-errors"></div>
			    <div class="clear"></div>
			</div>
		<?php } ?>

		<?php if(!$prov_vencido){
			if($salon_elegido){ ?>
				<div class="form-group has-feedback">
					<input type="text" name="ubicacion" placeholder="Salón elegido" value="<?php echo $data_form['ubicacion']; ?>" class="form-control" />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
			<?php }else{ ?>
				<input type="hidden" name="ubicacion" value="" />
			<?php }
		}

		if(!$prov_vencido){
			if(!$datos_viaje){ ?>
				<div class="form-group has-feedback">
					<input type="text" name="invitados" placeholder="Cantidad de Invitados"  value="<?php echo $data_form['invitados']; ?>" class="form-control" required />
					<span class="fa form-control-feedback" aria-hidden="true"></span>
			    	<div class="help-block with-errors"></div>
				</div>
			<?php }
		}


		if($datos_viaje && !$prov_vencido){ ?>
			<input type="hidden" name="datos_viaje" value="1" />

			<h5>Datos del viaje</h5>

			<div class="form-group has-feedback">
				<input type="text" name="dia_viaje" placeholder="Fecha viaje" class="form-control date" data-remote="/mayor_hoy" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" required />
				<span class="fa form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
			</div>

			<div class="form-group has-feedback">
				<input type="text" name="presupuesto" placeholder="Presupuesto Estimado" class="form-control" required />
				<span class="fa form-control-feedback" aria-hidden="true"></span>
		    	<div class="help-block with-errors"></div>
			</div>

			<div class="form-group has-feedback">
				<input type="text" name="cant_noches" placeholder="Cantidad de Noches"  class="form-control" required />
				<span class="fa form-control-feedback" aria-hidden="true"></span>
		    	<div class="help-block with-errors"></div>
			</div>
		<?php } ?>

		<h5>Datos de contacto</h5>

		<div class="form-group has-feedback">
		    <input type="text" name="nombre" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control" required>
		    <span class="fa form-control-feedback" aria-hidden="true"></span>
		    <div class="help-block with-errors"></div>
		</div>

		<div class="form-group has-feedback">
		    <input type="text" name="apellido" placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control" required>
		    <span class="fa form-control-feedback" aria-hidden="true"></span>
		    <div class="help-block with-errors"></div>
		</div>

		<div class="form-group has-feedback">
			<input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control" required />
			<span class="fa form-control-feedback" aria-hidden="true"></span>
		    <div class="help-block with-errors"></div>
		</div>

		<?php if(!$prov_vencido){ ?>
			<div class="form-group has-feedback">
				<input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
				<span class="fa form-control-feedback" aria-hidden="true"></span>
			    <div class="help-block with-errors"></div>
			</div>

			<div class="form-group has-feedback">
				<input type="hidden" name="dia_nacimiento" value="<?php echo $data_form['dia_nacimiento']?$data_form['dia_nacimiento']:set_value('dia_nacimiento'); ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" />
				<span class="fa form-control-feedback" aria-hidden="true"></span>
			    <div class="help-block with-errors"></div>
			</div>

			<div class="form-group has-feedback">
				<select name="id_provincia" class="form-control provincias" required>
					<option value="">Seleccionar una Provincia</option>
					<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
						<option value="<?php echo $k; ?>"  <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>

			<div class="form-group has-feedback">
				<select name="id_localidad" class="form-control localidades" required><!-- CAMBIE localidad X id_localidad. EN class.comercial.php HACE REFENCIA A id_localidad-->
					<option value="">Seleccionar una Localidad</option>
					<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
						<option value="<?php echo $k; ?>"  <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>
		<?php } ?>


		<div class="form-group has-feedback">
			<textarea placeholder="Comentarios" name="comentario" required class="form-control"></textarea>
			<div class="help-block with-errors"></div>
		</div>

		<div class="form-group has-feedback">
			<div class="checkbox check_form">
				  <label for="infonovias"><input type="checkbox" id="infonovias" name="infonovias" value="1" <?php if($data_form['infonovias']){ echo 'checked="checked"'; } ?> />Deseo recibir infonovias</label>
			</div>
			<span class="fa form-control-feedback" aria-hidden="true"></span>
		    <div class="help-block with-errors"></div>
		    <div class="clear"></div>
		</div>

		<?php
		if(isset($asociados) && $asociados && !$prov_vencido){ ?>
			<div id="check_empresas_asociadas" class="form-group has-feedback">
				<div class="checkbox check_form">
					<label for="emp_asociadas"><input id="emp_asociadas" type="checkbox" name="emp_asociadas" value="1" checked="checked" />Enviar a empresas asociadas</label>
				</div>
		    	<div class="help-block with-errors"></div>
			</div>
		<?php } ?>

		<?php if(!empty($hash)){ ?>
			<input type="button" class="btn-default disabled" value="Enviar Consulta" title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom" />
		<?php }else{ ?>
			<input type="submit" class="btn-default submit" value="Enviar Consulta" />
		<?php } ?>

		<a href="<?php echo base_url('/politicas-de-privacidad'); ?>" target="_blank" class="politicas">Ver políticas de privacidad</a>
	</form>
	<?php } ?>
</div><!-- #form_minisitio -->