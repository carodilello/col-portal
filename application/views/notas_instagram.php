<div id="instagram_box">
	<?php if(!empty($instagram_post)): ?>
	<div class="contenedor">
		<div class="encabezado">
			<a href="<?php echo $instagram_post["link"]; ?>" target="_blank">
				<div class="avatar"><img src="<?php echo $instagram_post["user"]->profile_picture; ?>"></div>
				<div class="text-noavatar"><?php echo $instagram_post["user"]->username; ?></div>
			</a>
		</div>
		<a href="<?php echo $instagram_post["link"]; ?>" target="_blank"><img src="<?php echo $instagram_post["src"]; ?>"></a>
		<div class="pie">
			<div class="social">
				<a href="<?php echo $instagram_post["link"]; ?>" target="_blank"><i class="fa fa-heart"></i><?php echo $instagram_post["likes"]; ?> Me gusta</a>
				<a href="<?php echo $instagram_post["link"]; ?>" target="_blank"><i class="fa fa-comment"></i><?php echo $instagram_post["comments"]; ?> Comentarios</a>
			</div>

			<div class="texto"><?php echo $instagram_post["text"]; ?></div>
			<div class="fecha"><?php echo $instagram_post["fecha"]; ?></div>
		</div>

		<div class="insta-logo"><i class="fa fa-instagram"></i></div>
	</div>
	<?php endif; ?>

</div><!--- #instagram_box  -->