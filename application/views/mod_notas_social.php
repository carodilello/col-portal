<div class="social">
	<input type="hidden" class="id_redes" value="consejos/<?php echo $nota['titulo_seo'] . '_CO_n' . $nota['id']; ?>" />
	<a class="compartir_facebook no_margin" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
	<a href="https://twitter.com/intent/tweet?url=<?php echo base_url('/consejos/'.$nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>&text=<?php echo urlencode($nota["titulo"]);?>"><i class="fa fa-twitter"></i></a>
	<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/consejos/'.$nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>&description=<?php echo urlencode($nota["titulo"]);?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
	<a class="mobile_block" href="whatsapp://send?text=http://webdevelopmentscripts.com"><i class="fa fa-whatsapp"></i></a>
	<a class="compartir_imprimir"><i class="fa fa-print"></i></a>
	<a href="#" data-toggle="modal" data-target="#enviar-amigo"><i class="fa fa-envelope"></i></a>
</div>