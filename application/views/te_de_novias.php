<?php
$bodyclass = 'page te_de_novias';
include('header.php'); 

if(isset($galeria[0])){
	$galeria = $galeria[0];

	$slides = array();
	if(isset($galeria['thumbs']) && $galeria['thumbs']){
		$tmp_slides = explode('~', $galeria['thumbs']);

		if(!empty($tmp_slides)) foreach ($tmp_slides as $k => $slide){
			$tmp_data_slide = explode('@', $slide);
			$slides[] = $tmp_data_slide;
		}
	}	
}


switch ($id_sucursal_els) {
	case '1': // BUENOS AIRES
		$suc_te = 'buenos-aires';
		break;
	case '2': // ROSARIO
		$suc_te = 'rosario';
		break;
	case '4': // CORDOBA
		$suc_te = 'cordoba';
		break;
}

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>',
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>'
); ?> 
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/eventos'); ?>">Eventos</a></li>
		<li>Té de novias en <?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											echo 'Buenos Aires';
											break;
										case '2': // ROSARIO
											echo 'Rosario';
											break;
										case '4': // CORDOBA
											echo 'Córdoba';
											break;
									} ?></li>
	</ul>
	
	<?php if(isset($banners[320]) && $banners[320] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[320]) && $banners[320] ? $banners[320] : '<p>Te de novias top 970 x 90</p>'; ?></div>
	<?php } ?>
	<?php if(isset($banners[321]) && $banners[321] || $mostrar_banners){ ?>
		<div class="banner banner_1140_115 push"><?php echo isset($banners[321]) && $banners[321] ? $banners[321] : '<p>Te de novias Push 1140x115</p>'; ?></div>
	<?php } ?>

	<?php if($success){ ?>
		<div id="gracias" class="alert alert-success alert-dismissible" role="alert">
			<div>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h2>Muchas gracias, tu Registro al Té de novias fue enviado con éxito.</h2>
				<p class="bajada">En breve recibirás un <strong>email confirmando tu participación y pasos a seguir</strong>. No olvides revisar tu correo no deseado.</p>
			</div>
		</div><!-- #gracias -->
	<?php } ?>

	<h1 class="title_sep">Te de Novias en <?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											echo 'Capital Federal';
											break;
										case '2': // ROSARIO
											echo 'Rosario';
											break;
										case '4': // CORDOBA
											echo 'Córdoba';
											break;
									} ?></h1>
	
	<div id="split">

			<div id="primary">
			
				<div id="intro_te_de_novias">
					<div id="header_te_de_novias">
						<div class="relative">
							<?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											echo '<img class="big_pic mobile_none" alt="Buenos Aires | Té de novias" src="' . base_url('/assets/images/header_te_novias.png') . '" /><img class="big_pic mobile_block" src="' . base_url('/assets/images/header_te_novias_mobile.jpg') . '" alt="Buenos Aires | Té de novias" />';
											break;
										case '2': // ROSARIO
											echo '<img class="big_pic mobile_none" alt="Rosario | Té de novias" src="' . base_url('/assets/images/header_te_novias_rosario.jpg') . '" /><img class="big_pic mobile_block" src="' . base_url('/assets/images/header_te_novias_rosario_mobile.jpg') . '" alt="Rosario | Té de novias" />';
											break;
										case '4': // CORDOBA
											echo '<img class="big_pic mobile_none" alt="Córdoba | Té de novias" src="' . base_url('/assets/images/header_te_novias_cordoba.jpg') . '" /><img class="big_pic mobile_block" src="' . base_url('/assets/images/header_te_novias_cordoba_mobile.jpg') . '" alt="Córdoba | Té de novias" />';
											break;
									} ?>
						<div id="logo_circulo_te_de_novias" class="align_total">
								<h3><?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											echo '- Buenos Aires -';
											break;
										case '2': // ROSARIO
											echo '- Rosario -';
											break;
										case '4': // CORDOBA
											echo '- Córdoba -';
											break;
									} ?></h3>
								
								<span class="green"><?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											echo '16 de agosto de 2017';
											break;
										case '2': // ROSARIO
											echo 'Proximamente';
											break;
										case '4': // CORDOBA
											echo '5 de Julio de 2017';
											break;
									} ?></span>
						
							</div><!-- #logo_circulo_te_de_novias -->
						</div><!-- .relative -->
						
						<div class="col col-md-4 col-xs-4">
							<div class="icon">
								<h4>Donde</h4>
							</div>
							<p><strong><?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											/*echo 'Proximamente';*/
											echo 'Argenta Tower Hotel.
											<br><span style="font-style: italic; font-size: 15px; color: #999";>Juncal 868, Ciudad Autónoma de Buenos Aires</span>';
											break;
										case '2': // ROSARIO
											echo 'Proximamente';
											break;
										case '4': // CORDOBA
											echo 'Holiday Inn.
											<br><span style="font-style: italic; font-size: 15px; color: #999";>Fray Luis Beltrán y Cardeñosa, Córdoba</span>';
											break;
									} ?></strong></p>
						</div>
						
						<div class="col col-md-4 col-xs-4">
							<div class="icon fecha">
								<h4>Cuando</h4>
							</div>
							<p><strong><?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											/*echo 'Proximamente';*/
											echo 'Miércoles 16 de agosto 2017
											<br><span style="font-style: italic; font-size: 15px; color: #999";>Horario: 17 a 20 hs.</span>';
											break;
										case '2': // ROSARIO
											echo 'Proximamente';
											break;
										case '4': // CORDOBA
											echo '5 de Julio 2017
											<br><span style="font-style: italic; font-size: 15px; color: #999";>Horario: 17 a 20 hs.</span>';
											break;
									} ?></strong></p>
						</div>
						
						<div class="col col-md-4 col-xs-4 no_border">
							<div class="icon precio">
								<h4>Valor</h4>
							</div>
							<p><strong><?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											/*echo 'Proximamente';*/
											echo '$ 250.-
											<br><span style="font-style: italic; font-size: 15px; color: #999";>¡PROMO 30% off pagando antes
											<br> del 25 de julio de 2017!</span>';
											break;
										case '2': // ROSARIO
											echo 'Proximamente';
											break;
										case '4': // CORDOBA
											echo '$ 250.-
											<br><span style="font-style: italic; font-size: 15px; color: #999";>20% off pagando antes del
											<br> 15 de junio de 2017</span>';
											break;
									} ?></strong></p>
						</div>
						
					</div><!-- #header_te_de_novias -->
				
					<h2 class="title_sep">¿Qué es el Te de Novias?</h2>
					<p><strong>El Té de Novias es un evento íntimo, útil y divertido, ideal para organizar tu Casamiento.</strong> El objetivo es que VOS junto a otras novias se encuentren en un espacio exclusivo a tomar el Té, probar cosas ricas y escuchar una mesa de <strong>panelistas expertos y referentes de los casamientos</strong> hablar acerca de la organización de la fiesta. Ellos darán consejos, tips y hablarán de las últimas tendencias en Casamientos.</p>
					
					<p>Animate a preguntarles todas tus dudas. Vas a poder compartir tu experiencia y charlar con otras novias, recorrer stands de proveedores y una increíble Galería de Vestidos de Novia. Te vas a llevar mucha información útil, regalos, sorpresas y participarás de importantes sorteos!</p>
					
					<p>Te invitamos a pasar una tarde genial con los que más saben. ¡No te podés perder!</p>
					<p class="gum_40">Para Consultas o Sugerencias podés escribirnos a: <a href="mailto:tedenovias@casamientosonline.com"><i class="famail">tedenovias@casamientosonline.com</i></a></p>
			<a 


					<?php switch ($id_sucursal_els) {
										case '1': // BUENOS AIRES
											echo /*'<h2>Próximamente</h2>
											<p>Nuevas fechas, lugar y panelistas invitados a nuestro "Té de Novias 2017".<br />
											Si estás interesada en participar, dejá tus datos y te mantendremos informada. </p>	*/
											'<p><strong class="block green">Recordá que los lugares son limitados. Reservá el tuyo</strong></p>';
											break;
										case '2': // ROSARIO
											echo '<h2>Próximamente</h2>
											<p>Nuevas fechas, lugar y panelistas invitados a nuestro "Té de Novias 2017".<br />
											Si estás interesada en participar, dejá tus datos y te mantendremos informada. </p>	
											<p><strong class="block green">Recordá que los lugares son limitados. Reservá el tuyo</strong></p>';
											break;											break;
										case '4': // CORDOBA
											echo '<p><strong class="block green" style="font-size: 25px";>Recordá que los lugares son limitados. Reservá el tuyo</strong></p>
											';
											break;
									} ?>					
				</div><!-- #intro_te_de_novias -->
				
				
				
				<div id="sponsors_panelistas">
<!--					<h3>Panelistas de nuestro último Té de Novias</h3>-->
					<?php switch ($id_sucursal_els) {
						case '1': // BUENOS AIRES
							/*echo "<h3>Panelistas de nuestro último Té de Novias</h3>";*/
							echo "<h3>Panelistas Invitados</h3>";

/*
							echo '<ul class="gum_60">
								<li><img src="http://media.casamientosonline.com/logos/104.jpg" alt="Grupo Sarapuba" /></li>
								<li><img src="' . base_url('/assets/images/logo_cuika_fotos.jpg') . '" alt="Cuika Foto" /></li>
								<li><img src="' . base_url('/assets/images/logo_estetica_bsas.jpg') . '" alt="Estética Buenos Aires" /></li>
								<li><img src="http://media.casamientosonline.com/logos/2852.jpg" alt="María Magnin" /></li>
							</ul>';*/
							echo '<ul class="gum_60">
								<li><img src="' . base_url('/assets/images/Logo-Florencia-Pierro.png') . '" alt="Cuika Foto" /></li>
								<li><img src="' . base_url('/assets/images/Maria_Grebol.jpg') . '" alt="Estética Buenos Aires" /></li>
								<li><img src="' . base_url('/assets/images/Maria-Ines-Novegil.png') . '" alt="Estética Buenos Aires" /></li>
								<li><img src="' . base_url('/assets/images/Tommy-Munoz.png') . '" alt="Tommy Munoz" /></li>
							</ul>';							
							break;
						case '2': // ROSARIO
							echo "<h3>Panelistas de nuestro último Té de Novias</h3>";
							echo '<ul class="gum_60 rosario">
								<li><img src="http://media.casamientosonline.com/logos/24748.jpg" alt="C" /></li>
								<li><img src="' . base_url('/assets/images/logo_ab_makeup.jpg') . '" alt="ABI Make Up" /></li>
								<li><img src="http://media.casamientosonline.com/logos/24981.jpg" alt="POP" /></li>
								<li><img src="http://media.casamientosonline.com/logos/8573.jpg" alt="Josefina Reppeto" /></li>
								<li><img src="http://media.casamientosonline.com/logos/17131.jpeg" alt="Evangelina Fissore" /></li>
							</ul>';
							break;
						case '4': // CORDOBA
							echo "<h3>PANELISTAS INVITADOS</h3>";

							echo '<ul class="gum_0 tres">
									<li><img src="' . base_url('/assets/images/logo_arias_bazan.jpg') . '" alt="Ariasbazan" /></li>
									<li><img src="' . base_url('/assets/images/logo_verde_flor.jpg') . '" alt="Verdeflor" /></li>
									<li><img src="' . base_url('/assets/images/logo_claudio_barzabal.jpg') . '" alt="Claudio Barzábal" /></li>
									</ul>
								<ul class="gum_60 dos">									
									<li><img src="' . base_url('/assets/images/logo_holidayinn_cordoba.jpg') . '" alt="Holiday inn Cordoba" /></li>
									<li><img src="' . base_url('/assets/images/logo_grupo_sarapura2.jpg') . '" alt="Grupo Sara Pura" /></li>
								</ul>';
							break;
					} ?>
	
				</div><!-- #ubicacion_te_de_novias -->
				
				<?php if(!empty($slides)){ ?>
					<h2 class="title_sep">Último Té de Novias</h2>
					<div class="galeria_standard">
						<div class="big_pic carousel slide" id="carousel-jornadas" data-ride="carousel" data-pause="hover" data-interval="false">
							<div class="carousel-inner" role="listbox">
								<?php foreach ($slides as $k => $slide){
									if(isset($slide[0]) || isset($slide[1]) || isset($slide[2]) || isset($slide[3])){ ?>
										<div class="align_c item <?php echo $k == 0 ? 'active' : ''; ?>">
									 		<a href="http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>" class="item_minis" data-gallery title="<?php echo $slide[2]; ?>">
										    	<img src="http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>" alt="<?php echo $slide[2]; ?>">
										    </a>
										</div>
									<?php }
								} ?>
							</div><!-- .carousel-inner -->		
						</div><!--  .big_pic -->	
						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-jornadas" role="button" data-slide="prev">
							<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
							<span class="sr-only valign">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-jornadas" role="button" data-slide="next">
							<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div><!-- #galeria_standard -->
				<?php } ?>
			</div><!-- .content -->
			
			<aside id="secondary">
				<?php echo form_open(base_url('/eventos/te-de-novias-en-' . $suc_te . '#gracias'), 'method="POST" role="form" class="form" id="form_te_de_novias"'); ?>
					<input type="hidden" name="id_origen" value="0" />
					<input type="hidden" name="id_evento" value="<?php echo $id_evento; ?>" />
					<input type="hidden" name="tipo" value="novios" />

			    	<div class="dialog_corner"></div>
	
					<h3>Registro Té de Novias</h3>
					<?php if(validation_errors()){ ?>
						<div class="errores-generales">	
							<div><i class="fa fa-exclamation-circle"></i> 
								<?php echo validation_errors(); ?>	
							</div>	
						</div> <!-- .errores-generales -->	
					<?php }else{ ?>
						<p>Completá tus datos para reservar un lugar.</p>
					<?php } ?>
					
					
					
					<h5>Datos del Casamiento</h5>

					<div class="form-group has-feedback has-success">
						<input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo set_value('dia_evento'); ?>" class="form-control date" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required="">
						<div class="help-block with-errors"></div>
					</div>
		
					<input type="hidden" name="ubicacion" value="">
					<div class="form-group has-feedback has-success gum_30">
						<input type="text" name="invitados" placeholder="Cantidad de Invitados"  value="<?php echo set_value('invitados'); ?>" class="form-control" required="">
						<div class="help-block with-errors"></div>
					</div>
					
					<h5>Datos del Contacto</h5>
							        
			        <div class="form-group has-feedback">
						<input type="text" name="nombre" required placeholder="Nombre" value="<?php echo set_value('nombre'); ?>" class="form-control">
						<div class="help-block with-errors"></div>
					</div>
					
					 <div class="form-group has-feedback">
						<input type="text" name="apellido" required placeholder="Apellido" value="<?php echo set_value('apellido'); ?>" class="form-control">
						<div class="help-block with-errors"></div>
					</div>
					
					 <div class="form-group has-feedback">
						<input type="text" name="telefono" required placeholder="Teléfono" value="<?php echo set_value('telefono'); ?>" class="form-control" pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido">
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group has-feedback">
						<input type="email" name="email" required placeholder="Email" value="<?php echo set_value('email'); ?>" class="form-control">
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group has-feedback">
						<input type="text" name="dni" required placeholder="DNI" value="<?php echo set_value('dni'); ?>" class="form-control">
						<div class="help-block with-errors"></div>
					</div>
					
					<?php if(!empty($provincias)){ ?>
						<div class="form-group has-feedback">
							<select name="id_provincia" required class="form-control provincias">
								<option value="">Provincia</option>
								<?php foreach ($provincias as $k => $provincia) { ?>
									<option value="<?php echo $k; ?>" <?=set_value('id_provincia') == $k ? "selected='selected'" : "";?>><?php echo $provincia; ?></option>
								<?php } ?>
							</select>
							<div class="help-block with-errors"></div>
						</div>
						
						<div class="form-group has-feedback">
							<select name="id_localidad" required class="form-control localidades">
								<option value="">Localidad</option>
								<?php if(!empty($localidades)) foreach ($localidades as $k => $localidad) { ?>
									<option value="<?php echo $k; ?>" <?=set_value('id_localidad') == $k ? "selected='selected'" : "";?>><?php echo $localidad; ?></option>
								<?php } ?>
							</select>
							<div class="help-block with-errors"></div>
						</div>
					<?php } ?>
					
					<div class="form-group has-feedback gum">
						<textarea placeholder="Comentarios" name="comentario" class="form-control"><?php echo set_value('comentario'); ?></textarea>
						<div class="help-block with-errors"></div>
					</div>
					
					<input class="btn btn-default disabled" type="submit">
				</form>
				
				<div id="ubicacion_te_de_novias">

					<h2 class="title_sep">Ubicación del Evento</h2>
					
					<?php switch ($id_sucursal_els) {
						case '1': // BUENOS AIRES
							echo '<a href="https://www.google.com/maps/place/Argenta+Tower+Hotel"><img style="border:1px solid #ddd;" src="' . base_url('/assets/images/mapa_te_bsas.jpg') . '" alt="Mapa de té proximamente" /></a>';

	/*							echo '<img style="border:1px solid #ddd;" src="' . base_url('/assets/images/									mapa_te_proximamente.jpg') . '" alt="Mapa de té proximamente" />';*/
							break;
						case '2': // ROSARIO
							echo '<img style="border:1px solid #ddd;" src="' . base_url('/assets/images/mapa_te_proximamente.jpg') . '" alt="Mapa de té proximamente" />';
							break;
						case '4': // CORDOBA
							echo '<a href="https://www.google.com/maps?daddr=-31.363367,-64.219615&saddr"><img style="border:1px solid #ddd;" src="' . base_url('/assets/images/mapa_te_cordoba.jpg') . '" alt="Mapa de té proximamente" /></a>';
							break;
					} ?>
					
				</div><!-- #ubicacion_te_de_novias -->
				<?php if(isset($banners[322]) && $banners[322] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[322]) && $banners[322] ? $banners[322] : '<p>Te de novias 300x250 <span>Posicion 1</span></p>'; ?></div>
				<?php } ?>
				<?php if(isset($banners[323]) && $banners[323] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[323]) && $banners[323] ? $banners[323] : '<p>Te de novias 300x250 <span>Posicion 2</span></p>'; ?></div>
				<?php } ?>
				<?php if(isset($banners[324]) && $banners[324] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[324]) && $banners[324] ? $banners[324] : '<p>Te de novias 300x250 <span>Posicion 3</span></p>'; ?></div>
				<?php } ?>
			</aside>
	</div><!-- #split -->
</div><!-- .container -->
<?php
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
$flechas = TRUE;
include('popup_fotos.php');
include('footer.php'); ?>