<?php
$bodyclass = 'home';
$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url("/assets/js/slidehome.js") . '"></script>',
	'<script type="text/javascript" src="' . base_url("/assets/js/preload.js") . '"></script>'
);

include('header.php'); ?>
<section id="buscador" class="buscador_global <?php echo isset($sucursal['class']) && $sucursal['class'] ? $sucursal['class'] : ''; ?>">
	
	<div id="slides" style="visibility:hidden;">
		<?php 

		foreach ($slides as $slid){ 
				if ($slid === reset($slides)) { ?>
				<div class="slide preload" style="background: url(<?php echo 'http://media.casamientosonline.com/slides/principal/'.$slid['id']; ?>) no-repeat center bottom;">

			<?php } else { ?>
				<div class="slide" style="background: url(<?php echo 'http://media.casamientosonline.com/slides/principal/'.$slid['id']; ?>) no-repeat center bottom;">

			<?php } ?>

			<?php 
				$file = 'http://media.casamientosonline.com/slides/logos/'.$slid['id'];
				$file_headers = @get_headers($file);
				if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') { ?>
				</div>  
				<?php }
				else { ?>
				<a href="<?php echo $slid['linklogo']; ?>" target="_blank" class=<?php $slid['id'];?>><img src="<?php echo $file; ?>" alt=<?php $slid['id'];?> /></a>
				</div>  
			<?php }	?>
		<?php } ?>


		<!-- manejo anterior de los slides -->
		<?php
		/*
		<div class="slide" style="background: url(<?php echo base_url('/assets/images/bg_home_7.jpg'); ?>) no-repeat center bottom;"></div>
				
		<div class="slide preload" style="background: url(<?php echo base_url('/assets/images/bg_home_1.jpg'); ?>) no-repeat center bottom;">
			<a href="<?php echo base_url('/proveedores/minisitio/13088'); ?>" class="sofia_cabrera"><img src="<?php echo base_url('/assets/images/logo_slides_sofia_cabrera.png'); ?>" alt="Sofia Cabrera" /></a>
		</div>
		
		<!-- SLIDE MADERO TANGO -->
		 
		<div class="slide" style="background: url(<?php echo base_url('/assets/images/bg_home_11.jpg'); ?>) no-repeat center bottom;">
			<a href="http://maderotango.com/landings/2017/Mega-Promo-Evento-Demo-Junio2-2017/" class="sofia_cabrera"><img src="<?php echo base_url('/assets/images/logo_slides_madero.png'); ?>" alt="Madero Tango" /></a>
		</div>
		
		<div class="slide" style="background: url(<?php echo base_url('/assets/images/bg_home_3.jpg'); ?>) no-repeat center bottom;">
			<a href="http://quintalosdiezeventos.casamientosonline.com/salones-de-fiesta?utm_source=web_col&utm_campaign=machado&utm_medium=foto_home" class="machado"><img src="<?php echo base_url('/assets/images/logo_slides_machado.png'); ?>" alt="Machado Catering" /></a>
		</div>
		*/ ?>

		
	</div><!-- #slides -->
	
	<img class="preload_back" src="<?php echo base_url('/assets/images/bg_home_1.jpg'); ?>" style="display: none" alt="Todo para tu Casamiento" />
	<div class="vh-align">	
		<div class="inner_buscador">
			<div>
				<h2>Encontrá todo para tu casamiento</h2>
				<form action="<?php echo 'http://buscador.' . DOMAIN . '/'; ?>" method="GET" role="form" class="form">
					<fieldset>
						<div class="dropdown">
							<a id="sucursales-menu-sugerencias" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="pull-left">
								<i class="fa fa-map-marker"></i><span class="green name-sucursal-ms"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?></span>
							</a>
							<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-menu-sugerencias">
								<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
									<li><a class="block change-sucursal" href="javascript:;" data-id="<?=$suc["nombre_seo"];?>"><?=$suc["sucursal"];?></a></li>
								<?php } ?>
							</ul>
						</div>
						<select class="col-md-5 select-sucursal selectpicker" name="id_sucursal" style="display:none;">
							<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
								<option value="<?php echo $suc["nombre_seo"]; ?>" <?=$sucursal['nombre_seo']==$suc["nombre_seo"]?'selected':'';?>><?=$suc["sucursal"];?></option>
							<?php } ?>
						</select>
						<input class="col-md-7 input-buscar" type="text" name="search" placeholder="Qué estás buscando?" autocomplete="off" required />
						<input class="tipo" type="hidden" name="tipo" value="todos" />
					</fieldset>
					<input type="button" value="Buscar" class="submit btn-default button-search" />
				</form>
				<div class="clear"></div>
				<div class="sugerencias_container"><?php include('menu_sugerencias.php'); ?></div>
				<div id="menu_busqueda_wrapper" class="menu-busqueda row col-md-3">
					<div class="relative">
						<span class="arrow_dialog_buscador"></span>
					</div>
					<div id="menu_busqueda_inner" class="menu_busqueda_inner"></div>
				</div>
			</div>
		</div><!-- .inner_buscador -->
	</div><!-- #vh-align -->	
</section><!-- #buscador -->
<div class="container">
	<section id="grid_home">
		<div class="gum_40 mobile_none">
			<?php if(isset($banners[300]) && $banners[300] || $mostrar_banners){ // HOME TOP 1140 115 ?>
				<div class="banner banner_1140_115 push"><?php echo isset($banners[300]) && $banners[300] ? $banners[300] : '<p>Banner Home TOP 1140 x 115</p>'; ?></div>
			<?php }
			if(isset($banners[234]) && $banners[234] || $mostrar_banners){ // HOME TOP PUSH 1140 115 ?>
				<div class="banner banner_1140_115 push"><?php echo isset($banners[234]) && $banners[234] ? $banners[234] : '<p>Banner Home Push 980x84<span>Tamaño real (1140x115)</span></p>'; ?></div>
			<?php } ?>
		</div>
		<?php if(!empty($rubros_mas_consultados)){ ?>
			<h2>
				Los rubros más consultados de 
				<strong class="dropdown">
					<a class="green suc_selector" id="sucursales-nav-label-top" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label-top">
						<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
							<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') . (isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>#grid_home"><?=$suc["sucursal"];?></a>
						<?php } ?>
					</ul>
				</strong>
			</h2>
			<p class="bajada">Todas las propuestas de las mejores empresas del Mercado</p>
			<div class="row">
				<?php foreach ($rubros_mas_consultados as $key => $columna){ ?>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<?php foreach ($columna as $k => $rubro){ ?>
							<a href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro["nombre_seo"] . '_CO_r' . $rubro["id"]); ?>" class="box">
								<img src="<?php echo base_url('/assets/images/grid_home/' . $rubro['imagen']) ; ?>" alt="<?php echo $rubro['alias'] ? $rubro['alias'] : $rubro['rubro']; ?>" />
								<div class="heading">
									<div class="solapa"><span class="ic_<?php echo $rubro['icon'] ? $rubro['icon'] : (isset($ic[$rubro['id']]) && $ic[$rubro['id']] ? $ic[$rubro['id']] : ''); ?>"></span></div>
									<h3><?php echo $rubro['alias'] ? $rubro['alias'] : $rubro['rubro']; ?></h3>
									<span>Ver Rubro</span>
								</div> 
							</a><!-- .box -->	
						<?php } ?>
					</div><!-- .col-md-3 -->
				<?php } ?>
			</div><!-- .row -->
		<?php } ?>

		<a href="<?php echo base_url('/' . $sucursal['nombre_seo'] . '/fiestas-de-casamiento'); ?>" class="btn-default col-md-12 btn_guia">Ver Guía completa de Rubros ></a>
	</section> <!-- #grid_home -->
	<div class="clear"></div>
	
	<div class="fixed_width_sidebar">
		<?php if($ultimas_notas){ ?>
			<section id="notas_home" class="pull-left">
				<h2 class="title_sep">Ideas y Consejos para Casamientos</h2>
				<a href="<?php echo base_url('/consejos'); ?>" class="ver_todas btn box"><i class="fa fa-plus-circle"></i>Ver todas las Notas</a>
				<div class="row">
					<?php foreach ($ultimas_notas as $nota){
						$tmp = explode('@',$nota['secciones']);
						$secciones = $tmp[1];
						$id_tipo_editorial = $tmp[2];
						$categoria_seo = !empty($tmp[7]) ? $tmp[7] : 'nota'; ?>
						<div class="col-md-6 gum_30">
							<div class="nota box">
								<a href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>" class="img_wrapper"><img src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" alt="<?php echo $nota['titulo']; ?>" /></a>
								<div class="heading">
									<a class="rubro" href="<?php echo base_url('/consejos/' . $categoria_seo . '_CO_cn' . $id_tipo_editorial); ?>"><?php echo $secciones; ?></a>
									<h3><a data-toggle="tooltip" data-placement="top" title="<?php echo $nota['copete']; ?>" href="<?php echo base_url('/consejos/' . $nota['titulo_seo'] . '_CO_n' . $nota['id']); ?>"><?php echo $nota['titulo']; ?></a></h3>
								</div>
							</div><!-- .nota .box -->
						</div><!-- .col-md-6 -->
					<?php } ?>
				</div><!-- .row -->
			</section> <!-- #notas_home -->
		<?php } ?>
		
		<section id="sidebar_home_1" class="pull-right">
			<?php if(isset($banners['302'])){ ?>
				<div class="banner banner_300_600"><?php echo isset($banners['302']) && $banners['302'] ? $banners['302'] : '<p>Home 300 x 600</p>'; ?></div>
			<?php }
			if(isset($banners['138']) || isset($banners['139']) || $mostrar_banners){ ?>
				<div class="banner banner_300_250 banner_doble"><?php echo isset($banners['138']) && $banners['138'] ? $banners['138'] : ''; ?><?php echo isset($banners['139']) && $banners['139'] ? $banners['139'] : '<p>Home 300 x 600</p>'; ?></div>
			<?php }
			if(isset($banners['236']) || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners['236']) && $banners['236'] ? $banners['236'] : '<p>Home 300x250 posicion 2</p>'; ?></div>
			<?php } ?>
		</section><!-- #sidebar_home_1 -->
	</div>

	<div id="banner_presupuesto_general_home" class="col-md-12">
		<?php if(isset($banners[301]) && $banners[301] || $mostrar_banners){ // HOME MEDIO, SI NO HAY BANNER MOSTRAMOS BANNER DE PRESUPUESTO ?>
			<div class="banner banner_1140_115"><?php echo isset($banners[301]) && $banners[301] ? $banners[301] : '<p>Home Banner medio 1140x115</p>'; ?></div>
		<?php } ?>
	</div>
</div><!-- .container -->

<?php if($proveedores_destacados){ ?>
	<section id="destacados_home" class="full_bg_gris">
		<div id="carousel-destacados-home" class="inner carousel slide carrusel_proveedores carousel_responsive" data-ride="carousel" data-pause="hover" data-interval="false" data-content="proveedores_destacados">
			<h2 class="align_c reference_position gum_30">
				Empresas destacadas en 
				<span class="dropdown">
					<a class="green suc_selector" id="sucursales-nav-label-empresas-destacadas" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
					<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label-empresas-destacadas">
						<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
							<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>"><?=$suc["sucursal"];?></a>
						<?php } ?>
					</ul>
				</span>
			</h2>
			<div class="carousel-inner" role="listbox">
				<?php include('home_carousel_inner.php'); ?>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-destacados-home" role="button" data-slide="prev">
				<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-destacados-home" role="button" data-slide="next">
				<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div><!-- #carousel-destacados-home -->
	</section><!-- #destacados_home -->
<?php }
include('mod_listado_proveedores_footer.php'); ?>
<div class="overlay-full" style="position:absolute; width:100%; opacity:0.5; background:#000; top:0; left:0; z-index:1000; display:none;"></div>
<?php include('footer.php'); ?>