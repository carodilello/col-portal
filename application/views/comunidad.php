<?php
$bodyclass = 'page comunidad';
include('header.php'); ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Comunidad</li>
	</ul>
	<?php if(isset($banners[280]) && $banners[280] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[280]) && $banners[280] ? $banners[280] : '<p>Banner Comunidad 970 x 90</p>'; ?></div>
	<?php } ?>	
	<div id="split">
		<div id="primary">
			<h1 class="title_sep">Comunidad de Casamientos Online</h1>
			<div class="box padding_30 gum_40">
				<p>Mujeres. Novias. Futuras madres. Formamos una gran comunidad. <strong>Entendimos tu necesidad de compartir, de charlar con otras novias y con expertos en casamientos para sacarte tus dudas</strong>, para relativizar tus miedos, y para canalizar tu ansiedad.</p>
				<p>Un espacio para conversar. Un espacio para compartir. Valoramos tu aporte, tu punto de vista, tus ganas de ayudar a otras. <strong>Bienvenida a nuestra Comunidad.</strong></p>
			</div><!-- .box --> 
		 
			<div class="row">
				<div class="col-md-6 clear blog">
					<div class="box">
						<div class="img_wrapper">
							<a href="http://blogdelanovia.casamientosonline.com/"><img src="<?php echo base_url('/assets/images/logo_blog_novia.jpg'); ?>" /></a>
						</div>
						<p>Anécdotas, vivencias, miedos y emociones de una novia que está a punto de dar el sí. Compartí su historia y contanos la tuya.</p>
						<a href="http://blogdelanovia.casamientosonline.com/" class="btn btn-default">Visitar Blog</a>
					</div>
				</div>
				
				<div class="col-md-6 blog">
					<div class="box">
						<div class="img_wrapper">
							<a href="http://weddingplanner.casamientosonline.com/"><img src="<?php echo base_url('/assets/images/logo_blog_wedding_planner.jpg'); ?>" /></a>
						</div>	
						<p>Tips, consejos y tendencias para entender bien como organizar tu casamiento sin morir en el intento. Palabras de una experta para hacerte las cosas más fáciles!</p>
						<a href="http://weddingplanner.casamientosonline.com/" class="btn btn-default">Visitar Blog</a>
					</div>
				</div>
				
				<div class="col-md-6 blog no_margin">
					<div class="box">
						<div class="img_wrapper">
							<a href="http://reciencasada.casamientosonline.com/"><img src="<?php echo base_url('/assets/images/logo_blog_recien_casada.jpg'); ?>" /></a>
						</div>	
						<p>Luego de que la tensión pasó... qué es lo que sigue? A no desesperar, que ahora también podremos seguir compartiendo experiencias post-casamiento!</p>
						<a href="http://reciencasada.casamientosonline.com/" class="btn btn-default">Visitar Blog</a>
					</div>
				</div>
				
				<div class="col-md-6 blog no_margin">
					<div class="box">
						<div class="img_wrapper">
							<a href="http://hacerfamilia.casamientosonline.com/"><img src="<?php echo base_url('/assets/images/logo_blog_hacer_familia.jpg'); ?>" /></a>
						</div>	
						<p>Ya te casaste y estás por formar, o ya formaste, tu nueva familia. Hacer familia cuenta con orientadores familiares, docentes, psicólogos, sociólogos, psicopedagogos y sexólogos dispuestos a ayudarte a transitar este nuevo camino!!! Animate a hacer tu pregunta!!!</p>
						<a href="http://hacerfamilia.casamientosonline.com/" class="btn btn-default">Visitar Blog</a>
					</div>
				</div>
			</div><!-- .row -->
		</div><!-- .primary -->

		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
			<?php include('notas_mas_leidas.php'); ?>
			<?php if(isset($banners[180]) && $banners[180] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[180]) && $banners[180] ? $banners[180] : '<p>Comunidad 300x250 <span>Posicion 1</span></p>'; ?></div>
			<?php } ?>

			<?php if(isset($banners[181]) && $banners[181] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[181]) && $banners[181] ? $banners[181] : '<p>Comunidad 300x250 <span>Posicion 2</span></p>'; ?></div>
			<?php } ?>
		</aside>
	</div><!-- #grid -->
	
</div><!-- .container -->		
		
<?php include('footer_newsletter.php'); ?>		
<?php include('footer_info.php'); ?>		