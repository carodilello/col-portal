<?php
$bodyclass = 'page listado listado_promociones';
$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
);
include('header.php'); ?>
<div class="container">
	<?php include('listado_header.php'); ?>
	<div id="listado_wrapper">
		<aside>
			<?php include('listado_filtros.php');
			include('listado_banners.php'); ?>
		</aside>
		<section id="content_listado">
			
			<?php include('listado_solapas.php'); ?>
					
			<div id="content_listado_fotos" class="row">
				<div class="col-md-12"><p class="bg_expandir_fotos"><i class="fa fa-arrows-alt"></i>Click en la imagen para agrandar</p></div>
				<div class="grid">
					<?php 
					$puntos_suspensivos 	  = "";
					$puntos_suspensivos_zonas = "";
					if(isset($fotos)&&$fotos) foreach ($fotos as $k => $foto){ 
							if(strlen($foto['titulo']) >= 25) $puntos_suspensivos = "...";
							if(strlen($foto['zonas'])  >= 33) $puntos_suspensivos_zonas = "..."; ?>
							<div class="foto col-md-4 col-xs-6 col-xxs-12">
								<input type='hidden' class="id_rubro" value="<?=$rubro['id'];?>" />
								<input type='hidden' class="pag-galeria" value="<?=$pagina;?>" />
								<div class="foto-gallery">
									<div class="img_wrapper">
										<a href="<?php echo 'http://media.casamientosonline.com/images/'.$foto["imagen_gde"]; ?>" title='<?php echo $foto['titulo']; ?>' data-gallery 
										data-urlminisitio="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto['rubro_seo']; ?>"
										data-url="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto["rubro_seo"] . '/presupuesto-foto_CO_r'.$rubro["id"].'_fo'.$foto['id']; ?>" class="image-link">
											<img alt="<?php echo $foto['titulo']; ?>" <?php echo 'src="http://media.casamientosonline.com/images/'.str_replace("_chica", "", $foto["imagen"]).'"'; ?> />
										</a>
									</div>
									<h3><a href="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto['rubro_seo']; ?>" <?=$puntos_suspensivos?'data-toggle="tooltip" data-placement="top" data-container="body" title="'.strip_tags($foto['titulo']).'"':'';?>><?php echo substr($foto['titulo'], 0, 25).$puntos_suspensivos; ?></a></h3>
									<?php if($foto['zonas']){ ?>
										<p class="zona" title="<?php echo $foto['zonas']; ?>" <?=$puntos_suspensivos_zonas?'data-toggle="tooltip" data-placement="top" data-container="body" title="'.strip_tags($foto['zonas']).'"':'';?>><i class="fa fa-map-marker"></i><?php echo substr($foto['zonas'], 0, 33).$puntos_suspensivos_zonas; ?></p>
									<?php } ?>
								</div>	
							</div>
					<?php 
					$puntos_suspensivos       = "";
					$puntos_suspensivos_zonas = "";
					} ?>
				</div> <!-- .grid -->
			</div><!-- #content_listado_fotos -->
			<?php include('paginacion.php'); ?>
		</section><!-- #content_listado -->
	</div><!-- #listado_wrapper -->
</div><!-- .container -->
<?php 
include('popup_fotos.php');
include('footer.php'); ?>