<?php
$bodyclass = 'contacto registro_jornadas';
include('header.php'); ?>
<div class="container">
<ul id="breadcrumbs">
        <li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
        <li><a href="<?php echo base_url('/eventos'); ?>">Eventos</a></li>
        <li><a href="<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>">Jornadas</a></li>
        <li>Jornadas Registro</li>
</ul>
<h1 class="title_sep">Registrarse a la 41º Jornada de Casamientos Online</h1>
<?php echo form_open(base_url('/eventos/expo-novias-' . date('Y') . '/registro/' . $seccion . (!empty($data_form['id_novio']) ? '/' . $data_form['id_novio'] : '') . '#gracias'), 'method="POST" role="form" class="form" id="form_presupuesto_wrapper"'); ?>
        <input type="hidden" name="id_origen" value="0">
        <input type="hidden" name="id_evento" value="<?php echo $seccion == 'novios' ? '143' : '144'; ?>">
        <input type="hidden" name="tipo" value="<?php echo $seccion == 'novios' ? 'novio' : 'empresa'; ?>">

        <?php if(!empty($data_form['id_novio'])){ ?>
            <input type="hidden" name="id_novio" value="<?php echo $data_form['id_novio']; ?>" />
        <?php }
        
        if(!empty($data_form['update_data'])){ ?>
            <input type="hidden" name="update_data" value="<?php echo $data_form['update_data']; ?>" />
        <?php } ?>

        <aside class="col-md-3">
                <div class="datos gum">
                        <p><i class="fa fa-calendar"></i>27 de Septiembre de 2017</p>
                        <p><i class="fa fa-map-marker"></i>Centro Cultural Borges, Galerías Pacífico.</strong></p>
                        <p><i class="fa fa-tag"></i>Entrada gratuita, cupos limitados.</strong></p>
                </div>
                <p>Si te interesa participar ya <strong>podés solicitar tu cupo completando el formulario</strong>. Te mantendremos al tanto de las novedades. Muchas gracias!</p>
                <p><strong>Recordá que el cupo es limitado.</strong> Una vez cerrada la inscripción te informaremos si tu solicitud quedó dentro del cupo y te enviaremos la invitación por e-mail para que confirmes tu asistencia. Si no entraste en el cupo te tendremos al tanto de las próximas Jornadas</p>
        </aside>

        <div id="form_presupuesto" class="col-md-9">
                <?php if(!empty($succes_update)){ ?>
                        <div id="gracias" class="alert alert-success alert-dismissible" role="alert">
                                <div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h2>Muchas gracias, tu Registro a la Jornada de Casamientos Online fue enviado con éxito.</h2>
                                    <p class="bajada">El formulario se completó correctamente y tus datos ya fueron enviados. En breve recibirás novedades. No olvides revisar tu correo no deseado. Cualquier inconveniente podés escribirnos a <a href="mailto:jornadas@casamientosonline.com" class="green"><strong>jornadas@casamientosonline.com</strong></a></p>
                                </div>
                        </div><!-- #gracias -->
                <?php }elseif($success){ ?>
                        <div id="gracias" class="alert alert-success alert-dismissible" role="alert">
                                <div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h2>Muchas gracias, tu Registro a la Jornada de Casamientos Online fue enviado con éxito.</h2>
                                    <p class="bajada">En breve recibirás un <strong>email confirmando tu participación y el cupón de acceso a la expo</strong>. No olvides revisar tu correo no deseado. Cualquier inconveniente podés escribirnos a <a href="mailto:jornadas@casamientosonline.com" class="green"><strong>jornadas@casamientosonline.com</strong></a></p>
                                </div>
                        </div><!-- #gracias -->
                <?php }

                if(validation_errors()){ ?>
                        <div class="errores-generales"> 
                                <div><i class="fa fa-exclamation-circle"></i> 
                                        <?php echo validation_errors(); ?>
                                </div>  
                        </div><!-- .errores-generales -->
                <?php } ?>

                <div class="row">       
                    <div class="col-md-4">
                        <fieldset class="row no_margin_b">
                            <label class="col-md-12">Asistir a la jornada como...</label>
                            <div class="col-md-12 input_icon form-group has-feedback">
                                <span class="fa fa-check"></span>
                                <select class="cambiar_tipo_jornada form-control">
                                    <option value="1" <?php echo $seccion == 'novios' ? 'selected' : ''; ?>>Novio</option>
                                    <option value="2" <?php echo $seccion == 'empresas' ? 'selected' : ''; ?>>Empresa</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </fieldset>
                    </div>
                    
                    <?php if($seccion == 'novios'){ ?>
                        <div class="col-md-8">  
                            <fieldset class="row no_margin_b">
                                <label class="col-md-12">Datos del Casamiento</label>
                                <div class="col-md-6 input_icon form-group has-feedback">
                                    <span class="fa fa-user"></span>
                                    <input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']?$data_form['dia_evento']:set_value('dia_evento'); ?>" class="form-control date"  required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                
                                <div class="col-md-6 input_icon form-group has-feedback">
                                    <span class="fa fa-user"></span>
                                    <input type="text" name="invitados" placeholder="Cantidad de Invitados"  value="<?php echo $data_form['invitados']?$data_form['invitados']:set_value('invitados'); ?>" class="form-control" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </fieldset>
                        </div>
            		<?php } ?>
                </div>
                                        
                <fieldset class="row no_margin_b">
                        <label class="col-md-12">Datos de contacto</label>
                        <div class="col-md-4 input_icon form-group has-feedback">
                                <span class="fa fa-user"></span>
                                <input type="text" name="nombre" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control" required>
                                <div class="help-block with-errors"></div>
                        </div>
                        
                        <div class="col-md-4 input_icon form-group has-feedback">
                                <span class="fa fa-user"></span>
                                <input type="text" name="apellido" placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control" required>
                                <div class="help-block with-errors"></div>
                        </div>
                        
                        <div class="col-md-4 input_icon form-group has-feedback">
                                <span class="fa fa-envelope"></span>
                                <input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control" required>
                                <div class="help-block with-errors"></div>
                        </div>
                </fieldset>
                
                <fieldset class="row no_margin_b">
                        <div class="col-md-4 input_icon form-group has-feedback">
                                <span class="fa fa-phone"></span>
                                <input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido">
                                <div class="help-block with-errors"></div>
                        </div>

                        <?php if($seccion == 'empresas'){ ?>
                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-building"></span>
                                        <input type="text" name="empresa" placeholder="Empresa" value="<?php echo !empty($data_form['empresa'])?$data_form['empresa']:set_value('empresa'); ?>" class="form-control" required />
                                        <div class="help-block with-errors"></div>
                                </div>

                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-building"></span>
                                        <select name="id_rubro" required class="form-control">
                                                <option value="">Rubro</option>
                                                <?php if($combo_rubros) foreach ($combo_rubros as $key => $rubro){ ?>
                                                        <option value="<?php echo $rubro['id']; ?>" <?=(set_value('id_rubro')==$rubro['id'])||!set_value('id_rubro')&&($rubro['id']==$data_form['id_rubro']) ? 'selected="selected"' : '';?>><?php echo $rubro['rubro']; ?></option>
                                                <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                </div>
                        <?php } ?>

                        <?php if($seccion == 'novios'){ ?>
                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-user"></span>
                                        <input type="text" name="dni" required placeholder="DNI" value="<?php echo !empty($data_form['dni'])?$data_form['dni']:set_value('dni'); ?>" class="form-control">
                                        <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-map-marker"></span>
                                        <select name="id_provincia" required class="form-control provincias">
                                                <option value="">Provincia</option>
                                                <?php if($provincias) foreach ($provincias as $k => $provincia) { ?>
                                                        <option value="<?php echo $k; ?>" <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
                                                <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                </div>
                        <?php } ?>
                </fieldset>

                <fieldset class="row no_margin_b">
                        <?php if($seccion == 'novios'){ ?>
                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-location-arrow"></span>
                                        <select name="id_localidad" required class="form-control localidades">
                                                <option value="">Localidad</option>
                                                <?php if($localidades) foreach ($localidades as $k => $localidad) { ?>
                                                        <option value="<?php echo $k; ?>" <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
                                                <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                </div>
                        <?php }
                        if($seccion == 'empresas'){ ?>
                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-mobile-phone"></span>
                                        <input type="text" name="celular" placeholder="Celular" value="<?php echo !empty($data_form['celular'])?$data_form['celular']:set_value('celular'); ?>" class="form-control" required />
                                        <div class="help-block with-errors"></div>
                                </div>
                                <div class="col-md-4 input_icon form-group has-feedback">
                                        <span class="fa fa-users"></span>
                                        <input type="text" name="invitados" placeholder="Cantidad de Acompañantes"  value="<?php echo $data_form['invitados']?$data_form['invitados']:set_value('invitados'); ?>" class="form-control" required>
                                        <div class="help-block with-errors"></div>
                                </div>
                        <?php } ?>
                </fieldset>

                <div class="clear" style="border-top:1px solid #ddd; padding-top: 20px;">
                        <div class="checkbox col-md-7 check_form no_padding has-feedback">
                          <label for="infonovias"><input id="infonovias" name="infonovias" type="checkbox" value="1" checked="checked">Deseo recibir el Infonovias y promociones de casamientos</label>
                        </div>
                        <input type="submit" class="btn-default submit_presupuesto disabled" value="Registrarse a la Jornada">
                        <div class="has-error has-danger has-feedback">
                                <div style="display:none; float:right;" class="error-zonas help-block with-errors">
                                        <ul class="list-unstyled">
                                                <li>Debe seleccionar al menos una zona</li>
                                        </ul>
                                </div>
                        </div>
                </div><!-- .clear -->
        </div>
</form>
</div><!-- container -->
<?php include('footer.php'); ?>