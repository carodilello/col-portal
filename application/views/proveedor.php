<div class="box_listado <?php if($proveedor["nivel"] <= 4){ echo 'destacado'; } ?>">
	<div class="img_wrapper">
		<a href='<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['seo_url']; ?>'>
			<img <?php
				if(isset($proveedor["imagen_gde"]) && $proveedor["imagen_gde"]){
					echo 'src="http://media.casamientosonline.com/images/'.$proveedor["imagen_gde"].'"';
				}elseif(isset($proveedor["imagen_chica"]) && $proveedor["imagen_chica"]){
					echo 'src="http://media.casamientosonline.com/images/'.$proveedor["imagen_chica"].'"';
				}else{
					echo 'src="http://media.casamientosonline.com/logos/'.$proveedor["imagen"].'"';
				} ?> />
		</a>
	</div>
	<div class="info_wrapper">
		<h2 style="margin-bottom: 10px;"><a href='<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['seo_url']; ?>'><?php echo $proveedor["proveedor"]; ?></a></h2>
		<?php if($proveedor["zonas"]){ ?>
			<span class="zona"><i class="fa fa-map-marker"></i><?php echo str_replace(",",", ",$proveedor["zonas"]); ?></span>
		<?php } ?>
		<h3 class="descripcion"><?php echo $proveedor["breve"]; ?></h3>
		<div class="bottom_actions">
			<div class="inc_minisitio">
				<?php if($proveedor["fotos"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["fotos"]; ?> fotos" ><i class="fa fa-camera"></i></span><?php } ?>
				<?php if($proveedor["videos"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["videos"]; ?> videos" ><i class="fa fa-video-camera"></i></span><?php } ?>
				<?php if($proveedor["audios"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["audios"]; ?> audios" ><i class="fa fa-music"></i></span><?php } ?>
				<?php if($proveedor["comentarios"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["comentarios"]; ?> comentarios" ><i class="fa fa-comments-o"></i></span><?php } ?>
				<?php if($proveedor["productos"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["productos"]; ?> productos" ><i class="fa fa-star"></i></span><?php } ?>
				<?php if($proveedor["promociones"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["promociones"]; ?> promociones" ><i class="fa fa-tag"></i></span><?php } ?>
				<?php if($proveedor["paquetes"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["paquetes"]; ?> paquetes" ><i class="fa fa-gift"></i></span><?php } ?>
			</div><!-- .inc_minisitio -->
			<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['seo_url'] . '/presupuesto-empresa_CO_r' . $proveedor["id_rubro"] . '_m' . $proveedor["id_minisitio"]; ?>" class="btn btn-default">Pedir Presupuesto</a>
		</div><!-- .bottom_actions -->
	</div>
</div><!-- .box_listado -->	