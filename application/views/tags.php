<?php
$bodyclass = 'page tags split notas notas_listado';

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
);

include('header.php'); ?>
<div class="container">
	<?php if(isset($origen) && $origen == 'galerias'){
		if(isset($banners[294]) && $banners[294] || $mostrar_banners){ ?>
			<div class="banner banner_970_90"><?php echo isset($banners[294]) && $banners[294] ? $banners[294] : '<p>Galerias top 970 x 90</p>'; ?></div>
		<?php }
	} ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<?php if(!empty($breadcrumbs)) foreach ($breadcrumbs as $breadcrumb){ ?>
			<li><a href="<?php echo base_url($breadcrumb['url']); ?>"><?php echo $breadcrumb['nombre'];?></a></li>
		<?php } ?>
		<li>Tags</li>
	</ul>
	<h1 class="h_sep"><?php echo $titulo; ?> del tag: <span class="green"><?php echo ucfirst($tag); ?></span></h1>
	<div id="split">
		<section id="primary">
			<?php if(!empty($listado_notas)){ ?>
				<div id="notas_listado_segundo_nivel" class="row">
					<?php foreach ($listado_notas as $key => $nota){
						$str_mas        = '...';
						$str_mas_titulo = '...';
						$tmp_secciones = explode('~', $nota['secciones']);
						$nombre_subseccion = '';
						foreach ($tmp_secciones as $key => $secc){
							$tmp_secc = explode('@', $secc);
							$nombre_subseccion .= $tmp_secc[1].', ';
						}
						include('nota.php');
					} ?>
				</div><!-- #notas_listado_segundo_nivel -->
			<?php }
			
			if(!empty($videos_all)){ ?>
				<div id="grid_galeria">
					<div class="row">
						<?php include('video.php'); ?>
					</div>
				</div>
			<?php }

			if(!empty($galerias)){ ?>
			<div id="grid_galeria">
				<div class="row">
					<?php foreach ($galerias as $k => $galeria){
						include('galeria.php');
					} ?>
				</div>
			</div>
			<?php }

			echo $this->pagination->create_links(); ?>
		</section>
		
		<aside id="secondary">
			<?php if(!empty($tags['data'])){ ?>
				<div id="tags_sidebar">
					<h3 class="title_sep">Búsqueda por Tags</h3>
					<?php foreach ($tags['data'] as $k => $tag_e){ ?>
						<a class="<?php if($k > 9) echo "can_hide hidden";?>" <?php if($k > 9) echo "style='display:none;'";?> href="<?php echo base_url(str_replace('{?}', $tag_e['data_seo'], $url_tag) . $tag_e["id"]); ?>"><?php echo $tag_e['data']; ?> (<?php echo $tag_e['cant']; ?>)</a>
					<?php } ?>
					<a href="javascript:;" class="mostrar_tags" style="background-color:#222;">Ver más</a>
				</div>
			<?php } ?>
			
			<?php include('notas_facebook.php'); ?>
			
			<?php if(isset($origen) && $origen == 'editorial'){
				if(isset($banners[283]) && $banners[283] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[283]) && $banners[283] ? $banners[283] : '<p>Banner Editorial 300x250 <span>Posicion 1</span></p>'; ?></div>
				<?php }
			} ?>
			<?php if(isset($origen) && $origen == 'galerias'){
				if(isset($banners[172]) && $banners[172] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[172]) && $banners[172] ? $banners[172] : '<p>Banner Galerias 300x250 <span>Posicion 1</span></p>'; ?></div>
				<?php }
			} ?>
			
			<?php include('notas_instagram.php'); ?>
			
			<?php include('notas_mas_leidas.php'); ?>
			
			<a href="" id="casamientos_tv_sidebar">
				<img src="<?php echo base_url('/assets/images/casamientos_tv_sidebar.jpg'); ?>" />
				<div class="banda valign">
					<i class="fa fa-television"></i>
					<h4>Casamientos TV<span>¡Los mejores videos!</span></h4>
				</div>
			</a><!-- #casamientos_tv_sidebar -->
			
			<?php if(isset($origen) && $origen == 'editorial'){
				if(isset($banners[284]) && $banners[284] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[284]) && $banners[284] ? $banners[284] : '<p>Banner Editorial 300x250 <span>Posicion 2</span></p>'; ?></div>
				<?php }

				if(isset($banners[285]) && $banners[285] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[285]) && $banners[285] ? $banners[285] : '<p>Banner Editorial 300x250 <span>Posicion 3</span></p>'; ?></div>
				<?php }
			} ?>
			<?php if(isset($origen) && $origen == 'galerias'){
				if(isset($banners[173]) && $banners[173] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[173]) && $banners[173] ? $banners[173] : '<p>Banner Galerias 300x250 <span>Posicion 2</span></p>'; ?></div>
				<?php }
				if(isset($banners[174]) && $banners[174] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[174]) && $banners[174] ? $banners[174] : '<p>Banner Galerias 300x250 <span>Posicion 3</span></p>'; ?></div>
				<?php }
				if(isset($banners[295]) && $banners[295] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[295]) && $banners[295] ? $banners[295] : '<p>Banner Galerias 300x250 <span>Posicion 4</span></p>'; ?></div>
				<?php }
			} ?>
		</aside> <!-- #sidebar -->
		
	</div><!-- #split -->		
</div><!-- .container -->
<?php include('footer.php'); ?>