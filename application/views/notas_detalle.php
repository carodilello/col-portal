<?php
$bodyclass = 'page split notas nota_detalle';
include('header.php');

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
);

$tmp_tags = explode('~', $nota['tags']);
$tags = array();
if(is_array($tmp_tags)) foreach ($tmp_tags as $tag_v){
	$tmp_v = explode('@', $tag_v);
	if(isset($tmp_v[0]) && isset($tmp_v[1])) $tags[] = array('id' => $tmp_v[0], 'nombre' => $tmp_v[1], 'seo_url' => $tmp_v[2]);
}
$rubros = array();
if($nota['rubros']){
	$rubros_str = explode('~',$nota['rubros']);
	if(count($rubros_str) > 0) foreach ($rubros_str as $rubro_v){
		$tmp_v = explode('@', $rubro_v);
		$rubros[] = array('id' => $tmp_v[0], 'nombre' => $tmp_v[1], 'zona' => $tmp_v[2], 'id_sucursal' => $tmp_v[3], 'rubro' => $tmp_v[4], 'sucursal' => $tmp_v[5]);
	}
}
$otras = array();
if($nota['urls']){
	$otras_str = explode('~',$nota['urls']);
	if(count($otras_str) > 0) foreach ($otras_str as $otra_v){
		$tmp_v = explode('@', $otra_v);
		$otras[] = array('id' => $tmp_v[0], 'nombre' => $tmp_v[2], 'url' => $tmp_v[1]);
	}
}
$proveedores_n = array();
if($nota['proveedores']){
	$proveedores_n_str = explode('~',$nota['proveedores']);
	if(count($proveedores_n_str) > 0) foreach ($proveedores_n_str as $proveedor_v){
		$tmp_v = explode('@', $proveedor_v);
		$proveedores_n[] = array('id' => $tmp_v[2], 'nombre' => $tmp_v[1], 'img' => $tmp_v[7], 'breve' => $tmp_v[8], 'zonas' => $tmp_v[9], 'subdominio' => $tmp_v[10], 'rubro_seo' => $tmp_v[11]);
	}
}

$nota_anterior  = '';
$nota_siguiente = '';
if($nota['nota_anterior'])  $nota_anterior = explode('@&$', $nota['nota_anterior']);
if($nota['nota_siguiente']) $nota_siguiente = explode('@&$', $nota['nota_siguiente']); ?>
<div class="container">
	
	<?php if(isset($banners[290]) && $banners[290] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[290]) && $banners[290] ? $banners[290] : '<p>Banner Editorial notas 970 x 90</p>'; ?></div>
	<?php } ?>
	
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/consejos'); ?>">Notas</a></li>
		<?php if(!in_array($nota['id_seccion'], array(116,264,319,402))){ // ESTO ES PORQUE EN LA CONSULTA ESTAMOS RESTRINGIENDO ESTOS IDS ?>
			<li><a href="<?php echo base_url('/consejos/' . $seccion_data['nombre_seo'] . '_CO_cn' . $id_seccion); ?>"><?php echo $seccion_data['nombre']; ?></a></li>
		<?php } ?>
		<li><?php echo $nota['titulo']; ?></li>
	</ul>
	<div class="ribbon_cat"><span><?php echo $seccion_data['nombre']; ?></span></div>
	<h1><?php echo $nota['titulo']; ?></h1>
	<h2 class="bajada mobile_none"><?php echo $nota['copete']; ?></h2>
	
	<div id="split">
		<section id="primary">
			<div id="nota_header_box" class="box">
				<div class="autor">
					<?php if($nota['nombre'] || $nota['apellido']){
						$autor = $nota['nombre'].' '.$nota['apellido'];
					}else{
						$autor = 'Redacción';
					} ?>
					<p><img src="<?php echo base_url('/assets/images/' . ($nota['avatar'] ? $nota['avatar'] : 'avatar_notas_generica.jpg' )); ?>" />Por <strong><?php echo $autor; ?></strong></p>
				</div>
				<div class="fecha">
					<p><i class="fa fa-calendar"></i><?php
						$tmp = explode('-', $nota['fecha_online']);
						$fecha = $tmp[2].' de '. (ucfirst($meses[(int) $tmp[1] - 1])) .' de '.$tmp[0]; ?><?php echo $fecha; ?></p>
				</div>
				<?php include('mod_notas_social.php'); ?>
			</div><!-- #nota_header_box -->
			
			<?php if(count($tags) > 0){ ?>
				<div id="tags_nota" class="box">
					<p><i class="fa fa-tag"></i><span>Tags:</span> <?php foreach ($tags as $k => $tag){ ?>
						<a href="<?php echo base_url('/listado-de-consejos/' . $tag["seo_url"] . '_CO_t' . $tag["id"]); ?>"><?php echo $tag['nombre']; ?></a><?php if($k != count($tags)-1) echo ', ';
					} ?></p>
				</div><!-- #tags_nota -->
			<?php } ?>
			
			<h2 class="bajada mobile_block"><?php echo $nota['copete']; ?></h2>
			
			<?php 
			if($galeria) foreach ($galeria as $k => $gal){
				$thumbs = explode('~', $gal['thumbs']); ?>
				<div class="galerias">
					<div class="galeria_standard no_margin">
						<div id="carousel-nota" class="big_pic carousel slide no_margin" data-ride="carousel" data-pause="hover" data-interval="false">
							<div class="carousel-inner" role="listbox">
								<?php if(!empty($thumbs)) foreach ($thumbs as $j => $thumb){
									$part = explode('@', $thumb);
									if(count($part) >= 2){ ?>
										<div class="align_c item <?=$j == 0 ? 'active' : '';?>">
									 		<a href="http://media.casamientosonline.com/images/<?php echo $part[0] . '.' . $part[1]; ?>" class="item_minis" title="<?php echo isset($part[2]) && $part[2] ? $part[2] : ''; ?>" data-gallery>
										    	<img src="http://media.casamientosonline.com/images/<?php echo $part[0] . '.' . $part[1]; ?>">
										    </a>
										    <div class="epigrafe">
												<h2 class="col-md-10"><!--<span class="rubro">La Organización</span>-->
												<?php if(isset($part[2]) && $part[2]){ ?>
													<a href="<?php echo base_url('editorial/listado/' . $part[1]); ?>"><?php echo $part[2]; ?></a>
												<?php } ?>
												</h2>
											</div>
										</div>
									<?php }
									} ?>
							</div><!-- #carousel-inner -->
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-nota" role="button" data-slide="prev">
								<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
								<span class="sr-only valign">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-nota" role="button" data-slide="next">
								<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div><!-- #carousel-galerias -->
					</div><!-- .galeria_standard -->
				</div><!-- .galerias -->
			<?php } ?>
			
			<div id="content_nota">			
				<?php 
				if(isset($nota['contenido']) && $nota['contenido']){
					echo stripslashes($nota['contenido']);
				} ?>
			</div><!-- #content_nota -->
			
			
			<?php if(!empty($proveedores_n) > 0){ 
				$str_mas = ''; ?>
				<div id="empresas_relacionadas_nota">
					<h2>Empresas relacionadas</h2>
					<div class="relative">
						<?php foreach ($proveedores_n as $k => $proveedor){
							if(strlen($proveedor['breve']) >= 60) $str_mas = '...';
							$tmp = explode(',', $proveedor['zonas']);
							if(isset($tmp[0])&&$tmp[0]){
								$zona = $tmp[0];
								$zonas = $zona;
								unset($tmp[0]);
								if(count($tmp) > 0){
									$zonas .= '<span> + '.count($tmp).'</span>';
								}
							} ?>
							<div class="empresa row">
								<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo']; ?>" class="col-md-2 col-xs-2 logo"><img src="http://media.casamientosonline.com/logos/<?php echo $proveedor['img']; ?>" /></a>
								<div class="data col-md-7 col-xs-12">
									<h4><a href="<?php str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo']; ?>"><?php echo $proveedor['nombre']; ?></a></h4>
									<?php if(isset($zonas) && $zonas){ ?>
										<p <?=count($tmp)>0?('data-toggle="tooltip" data-placement="top" title="' . str_replace(',', ', ', implode(',', $tmp)) . '"'):('');?> class="zona"><i class="fa fa-map-marker"></i><?php echo $zonas; ?></p>
									<?php } ?>
									<p class="descripcion"><?php echo substr($proveedor['breve'], 0, 65).$str_mas; ?></p>
								</div>
								<div class="col-md-3 col-xs-3">
									<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo']; ?>" class="btn btn-default">Ver minisitio</a>
								</div>
								<div class="clear"></div>
							</div><!-- .empresa -->
						<?php } ?>
						
					</div>
				</div><!-- #empresas_relacionadas_nota -->		
			<?php } ?>
			
			<?php if(count($rubros) > 0){ ?>
				<p id="rubros_incluidos_nota">Rubros incluidos:  <?php foreach ($rubros as $k => $rubro){ ?> 
					<a href="<?php echo base_url($rubro['sucursal'] . '/proveedor/' . $rubro['rubro'] . '_CO_r' . $rubro["id"]); ?>"><?php echo $rubro['nombre'].($rubro['zona']?(' en '.$rubro['zona']):''); ?></a><?php if($k != count($rubros)-1) echo ', ';
				} ?></p>
			<?php } ?>
						
			<?php if(!empty($otras) > 0){ ?>
				<div id="notas_asociadas_nota">
					<h2>Tal vez te pueda interesar...</h2>
					<div class="relative">
						<?php foreach ($otras as $k => $otra){ ?>
							<div class="nota">
								<div class="data">
									<h4><a href="<?php echo $otra["url"]; ?>"><?php echo $otra['nombre']; ?></a></h4>
								</div>
							</div><!-- .nota -->
						<?php } ?>
					</div>
				</div><!-- #notas_asociadas_nota -->
			<?php } ?>
			
			
			<div id="notas_navigation">
				<?php if($nota_anterior){ ?>
					<a href="<?php echo base_url('/consejos/' . $nota_anterior[2] . '_CO_n' . $nota_anterior[0]); ?>" class="box"><i class="fa fa-chevron-left"></i>Nota anterior<span><?php echo $nota_anterior[1]; ?></span></a>
				<?php }
				if($nota_siguiente){ ?>
					<a href="<?php echo base_url('/consejos/' . $nota_siguiente[2] . '_CO_n' . $nota_siguiente[0]); ?>" class="box">
						<i class="fa fa-chevron-right"></i>Siguiente Nota<span><?php echo $nota_siguiente[1]; ?></span>
					</a>	
				<?php } ?>
			</div><!-- #notas_navigation -->
			
			<?php if(!empty($comentarios) > 0){ ?>
				<div id="comentarios">
					<h3 class="title_sep">Comentarios</h3>
					<p class="big_txt"><?php echo count($comentarios); ?> comentarios en "<?php echo $nota['titulo']; ?>"</p>
					<div id="comentarios_wrapper">
						<?php foreach ($comentarios as $key => $comentario){
							$i = 0;
							$tmp_com = explode('#', $comentario);
							if(count($tmp_com) == 10) $i = 1;
							$contenido = $tmp_com[1+$i];
							$nombre = $tmp_com[2+$i].' '.$tmp_com[3+$i];
							$fecha = $tmp_com[5+$i];
							$tmp_fecha = explode(' ', $fecha);
							$tmp_fecha = explode('-', $tmp_fecha[0]);
							$fecha = $tmp_fecha[2] . '/' . $tmp_fecha[1] . '/' . $tmp_fecha[0];
							$bg_color = $tmp_com[8+$i]; ?>
							<div class="comentario">
								<div class="img_wrapper">
									<div style="font-weight: bold; font-size: 60px; text-align: center; background: #<?php echo $bg_color; ?>; color: #FFF; border-radius: 48%; height: 90px; width: 90px; vertical-align: middle; padding-top: 5px;"><?php echo strtoupper(substr(trim($nombre), 0,1)); ?></div>
								</div>	
								<div class="data">
									<h5><?php echo stripslashes($nombre); ?></h5>
									<p class="zona"><?php echo $fecha; ?></p>
									<p class="txt"><?php echo stripslashes($contenido); ?></p>
								</div>
							</div><!-- .comentario -->
						<?php } ?>
					</div><!-- #comentarios_wrapper -->
				</div><!-- #comentarios -->
			<?php } ?>
		</section>
		
		<aside id="secondary">
			<?php include('notas_categorias_sidebar.php'); ?>
			
			<?php include('notas_facebook.php'); ?>
			
			<?php include('registro_info_novias_sidebar.php'); ?>
			
			<?php if(isset($banners[291]) && $banners[291] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[291]) && $banners[291] ? $banners[291] : '<p>Editorial notas 300x250 <span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			
			<?php include('notas_instagram.php'); ?>
			
			<?php include('notas_mas_leidas.php'); ?>
			
			<?php if(!empty($galerias) > 0){ ?>
				<div id="galerias_asociadas_nota">
					<h2>Galerias de Fotos</h2>
					<div class="relative">
						<?php foreach ($galerias as $k => $galeria){ ?>
							<div class="galeria">
								<a class="img_wrapper" href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria["id"]); ?>"><img src="http://media.casamientosonline.com/images/<?php echo $galeria['pic_name'].'.'.$galeria['pic_extension']; ?>" alt="<?php echo $galeria['titulo']; ?>" /></a>
								<div class="data col-md-8">
									<h4><a href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria["id"]); ?>"><?php echo $galeria['titulo']; ?></a></h4>
									<p class="zona"><i class="fa fa-camera"></i><?php echo $galeria['cantidad_fotos']; ?> fotos</p>
								</div>
								<div class="col-md-4 boton">
									<a href="<?php echo base_url('/fotos-casamiento/' . $galeria['seo_titulo'] . '_CO_g' . $galeria["id"]); ?>" class="btn btn-default">Ver galería</a>
								</div>
							</div><!-- .empresa -->
						<?php } ?>
					</div>
				</div><!-- #galerias_asociadas_nota -->
			<?php } ?>
			
			<?php include('casamientos_tv_sidebar.php'); ?>
			
			<?php if(isset($banners[292]) && $banners[292] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[292]) && $banners[292] ? $banners[292] : '<p>Editorial notas 300x250 <span>Posicion 2</span></p>'; ?></div>
			<?php } ?>

			<?php if(isset($banners[293]) && $banners[293] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[293]) && $banners[293] ? $banners[293] : '<p>Editorial notas 300x250 <span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #sidebar -->
		
		<?php include('popup_amigo.php'); ?>
	</div><!-- #split -->		
</div><!-- .container -->

	
<div id="print_content">
	<h2><?php echo $nota['titulo']; ?></h2>
	<p class="copete"><?php echo $nota['copete']; ?></p>
	<div class="bg_nota_print">
		<div class="autor">
			<?php if($nota['nombre'] || $nota['apellido']){
				$autor = $nota['nombre'].' '.$nota['apellido'];
			}else{
				$autor = 'Redacción';
			} ?>
			<p><img src="<?php echo base_url('/assets/images/' . ($nota['avatar'] ? $nota['avatar'] : 'avatar_notas_generica.jpg' )); ?>" />Por <strong><?php echo $autor; ?></strong></p>
		</div><!-- .autor -->
	
		<div class="fecha">
			<p><i class="fa fa-calendar"></i><?php
				$tmp = explode('-', $nota['fecha_online']);
				$fecha = $tmp[2].' de '. (ucfirst($meses[(int) $tmp[1] - 1])) .' de '.$tmp[0]; ?><?php echo $fecha; ?></p>
		</div><!-- .fecha -->
		<div class="cat">
			<p>Categoría: <strong><?php echo $seccion_data['nombre']; ?></strong></p>
		</div><!-- .cat -->
	</div><!-- .bg_nota_print -->
	
		
	<div id="contenido_nota_print">
		<?php echo stripslashes($nota['contenido']); ?>
	</div>
</div>			
<?php
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
include('popup_fotos.php');
include('footer.php'); ?>