<?php
include('popup_minisitio_telefono.php');
include('popup_amigo.php');
include('mod_gracias.php'); ?>
<div id="header_minisitio">
	<div id="wrapper_header_minisitio" <?php echo 'style="background: url(\'http://media.casamientosonline.com/'.($proveedor['portada']?('logos/'.$proveedor['portada']):('images/'.$proveedor['imagen_gde'])).'\') no-repeat center; background-size: cover;"'; ?>>
		<div id="overlay_minisitio">		
		</div><!-- #overlay_minisitio -->
		<div id="inner_header_minisitio">
			<a id="logo_minisitio" href="<?php echo base_url('/proveedores/minisitio/'.$proveedor['id_minisitio']); ?>"><img alt="Logo <?php echo isset($proveedor['proveedor'])&&$proveedor['proveedor']?substr($proveedor['proveedor'], 0, 30).(strlen($proveedor['proveedor'])>=30?'...':''):''; ?>" <?php echo 'src="http://media.casamientosonline.com/logos/'.(isset($proveedor["logo"])&&$proveedor["logo"]?$proveedor["logo"]:'').'"'; ?> /></a>

			<div id="proveedor_header_minisitio">
				<div class="valign">
					<a <?=(strlen($proveedor['proveedor'])>=27?'data-toggle="tooltip" data-placement="top" title="'. $proveedor["proveedor"] .'"':'');?> href="<?php echo base_url('/proveedores/minisitio/'.$proveedor['id_minisitio']); ?>"><h1><?php echo isset($proveedor['proveedor'])&&$proveedor['proveedor']?substr($proveedor['proveedor'], 0, 30).(strlen($proveedor['proveedor'])>=30?'...':''):''; ?></h1></a>
					
					<p class="slogan_header_minisitio"><?php echo $proveedor['slogan']; ?></p>
					
					<?php if(!$prov_vencido){ ?>
						<p class="contacto_header_minisitio">
							<?php if(!empty($proveedor['direccion']) || !empty($proveedor['zonas'])){ ?>
								<span class="direccion">
									<?php if(!empty($proveedor['zonas'])){ ?>
									<i class="fa fa-map-marker"></i><strong><?php echo str_replace(',', ', ', $proveedor['zonas']); ?> <?= !empty($proveedor['direccion']) ? '-' : '';?> </strong>
									<?php }
									if(!empty($proveedor['direccion'])){
										$dir_sin_arg = str_replace('- Argentina', '', $proveedor['direccion']);
										$dir = str_replace('Ciudad Autonoma de Buenos Aires', 'CABA', $dir_sin_arg);
										$dir = str_replace('Ciudad Autónoma de Buenos Aires', 'CABA', $dir);
										$dir = str_replace('sin calle -', '', $dir);
										echo $dir;	
									} ?>
								</span>
							<?php } ?>
						</p>
					<?php } ?>
					<?php if($proveedor['id_minisitio'] && $proveedor['id_rubro'] && !$prov_vencido && empty($hash)){ ?>
						<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/presupuesto-empresa_CO_r' . $rubro["id"] . '_m' . $proveedor["id_minisitio"]; ?>" class="btn-default" style="display:none;">Pedir presupuesto</a>

					<?php } ?>
				</div><!-- .valign -->		
			</div><!-- #proveedor_header_minisitio -->	
			
			<?php if($proveedor['id_minisitio'] && $proveedor['id_rubro'] && !$prov_vencido && empty($hash)){ ?>
				<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/presupuesto-empresa_CO_r' . $rubro["id"] . '_m' . $proveedor["id_minisitio"]; ?>" class="btn-default">Pedir presupuesto</a>
			<?php } ?>
		</div><!-- #inner_header_minisitio -->
		
	</div><!-- #wrapper_header_minisitio -->
	
	<div id="mod_contacto_mobile" style="display:none;">
		<?php include('minisitio_acciones.php'); ?>
	</div>
	
	<ul class="solapas box">
		<?php $str_mas = '';
		if($prov_vencido || (!empty($hash))){ ?>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-suitcase"></i>La Empresa</a></li>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-tag"></i>Promociones y productos</a></li>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-gift"></i>Paquetes</a></li>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-photo"></i>Fotos y Videos</a></li>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-map-marker"></i>Mapa</a></li>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-comment"></i>Testimonios</a></li>
			<li><a href="#" class="disabled" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><i class="fa fa-question-circle"></i>Preguntas frecuentes</a></li>
		<?php }else{ ?>
			<li><a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url']; ?>" <?=$ubicacion=='minisitio'?'class="active"':'';?>><i class="fa fa-suitcase"></i>La Empresa</a></li>
			<?php if((!empty($proveedor['productos']) && $proveedor['productos']>0) || !empty($proveedor['promociones']) && $proveedor['promociones']>0){ ?>
				<li>
					<a href="<?php if($proveedor['productos']<1 && $proveedor['promociones']<1){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/productos-y-promos'; } ?>" <?=$ubicacion=='minisitio_promociones_productos'?'class="active"':'';?>><i class="fa fa-tag"></i>Promociones y productos</a>
				</li>
			<?php }
			if(!empty($proveedor['paquetes']) && $proveedor['paquetes']>0){ ?>
				<li>
					<a href="<?php if($proveedor['paquetes']<1&&count($ms_paq2)<1){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/paquetes';} ?>" <?=$ubicacion=='minisitio_paquetes'?'class="active"':'';?>><i class="fa fa-gift"></i>Paquetes</a>
				</li>
			<?php }
			if(!empty($proveedor['fotos']) && $proveedor['fotos']>0){ ?>
				<li>
					<a href="<?php if($proveedor['fotos']<1){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/fotos-y-videos'; } ?>" <?=$ubicacion=='minisitio_foto_video'?'class="active"':'';?>><i class="fa fa-photo"></i>Fotos y Videos</a>
				</li>
			<?php }
			if(!empty($proveedor['latitud']) || !empty($proveedor['longitud'])){ ?>
				<li>
					<a href="<?php if(!$proveedor['latitud']||!$proveedor['longitud']){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/ubicacion'; } ?>" <?=$ubicacion=='minisitio_mapa'?'class="active"':'';?>><i class="fa fa-map-marker"></i>Mapa</a>
				</li>
			<?php }
			if(!empty($proveedor['comentarios']) && $proveedor['comentarios']>0){ ?>
				<li>
					<a href="<?php if($proveedor['comentarios']<1){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/testimonios';} ?>" <?=$ubicacion=='minisitio_testimonios'?'class="active"':'';?>><i class="fa fa-comment"></i>Testimonios</a>
				</li>
			<?php }
			if(!empty($proveedor['faqs']) && $proveedor['faqs']>0){ ?>
				<li>
					<a href="<?php if($proveedor['faqs']<1){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/preguntas-frecuentes';} ?>" class="no_border <?=$ubicacion=='minisitio_preguntas_frecuentes'?'active':'';?>"><i class="fa fa-question-circle"></i>Preguntas frecuentes</a>
				</li>
			<?php } ?>
		<?php } ?>
	</ul>
</div><!-- .header_minisitio -->

<div id="print_content">
	<div class="header_print">
		<div class="logo">
			<img alt="Logo <?php echo $proveedor['proveedor']; ?>" <?php echo 'src="http://media.casamientosonline.com/logos/'.(isset($proveedor["logo"])&&$proveedor["logo"]?$proveedor["logo"]:'').'"'; ?> />
		</div>
		<div class="datos_contacto_print">
			<h2><?php echo $proveedor['proveedor']; ?></h2>
			<?php if($proveedor['direccion'] || $proveedor['zonas']){ ?>
				<span class="direccion">
					<?php if($proveedor['zonas']){ ?>
					<i class="fa fa-map-marker"></i><strong><?php echo str_replace(',', ', ', $proveedor['zonas']); ?> <?=$proveedor['direccion'] ? '-' : '';?> </strong>
					<?php }
					if($proveedor['direccion']){
						$dir_sin_arg = str_replace('- Argentina', '', $proveedor['direccion']);
						$dir = str_replace('Ciudad Autonoma de Buenos Aires', 'CABA', $dir_sin_arg);
						$dir = str_replace('Ciudad Autónoma de Buenos Aires', 'CABA', $dir);
						echo $dir;	
					} ?>
				</span>
			<?php } ?>
			
			<?php if(isset($proveedor['tels']) && is_array($proveedor['tels'])){ ?>
	            <p class="telefono"><i class="fa fa-phone"></i><?php
	            foreach ($proveedor['tels'] as $k => $t){
	                if($t[0] != '7' && $t[0] != '8') echo '<span>' . $t[1] . '</span>';
	            } ?></p>
	        <?php } ?>
	        
	        <?php
	            if(isset($proveedor['tels']) && is_array($proveedor['tels'])){ ?>
	            <p class="telefono"><i class="fa fa-whatsapp"></i><?php 
	                foreach ($proveedor['tels'] as $k => $t){
	                    if($t[0] == '7') echo '<span>' . trim($t[1],'@') . '</span>';
	                }
	                ?></p>
	        <?php } ?>
	        
		</div><!-- .datos_contacto_print -->
	</div><!-- .header_print -->
	<div id="fotos_minisitio_print">
	<?php $i = 1;
	    if(!empty($fotos)) foreach ($fotos as $k => $fot) {
	    	if(empty($fot['item']) && $i <= 4){ ?>
	    		<img alt="Imagen de <?php echo $proveedor['proveedor']; ?>" src="http://media.casamientosonline.com/images/<?php echo $fot['imagen']; ?>" />
	    	<?php 
	    	$i += 1;
	    	}
	    } ?>
	</div>        
	<?php if(isset($proveedor['latitud']) && $proveedor['latitud'] && isset($proveedor['longitud']) && $proveedor['longitud']){ ?>
	<div id="mapa_print">
		<h3>Ubicación</h3>
		<div class="map">
			<img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $proveedor['latitud']; ?>,<?php echo $proveedor['longitud']; ?>&zoom=16&size=640x400&markers=color:red%7Clabel:%7C<?php echo $proveedor['latitud']; ?>,<?php echo $proveedor['longitud']; ?>&path=weight:3%7Ccolor:blue%7Cenc:{coaHnetiVjM??_SkM??~R&key=AIzaSyCAoHQCQIUQM7IjMchESBI4jqxB2aOfxr4" />
		</div>
	</div>
	<?php } ?>
	<?php echo stripslashes($proveedor['contenido']); ?>
</div>