<?php
$bodyclass = 'page jornadas_registro';
include('header.php');

$slides = array();
if(isset($galeria['thumbs']) && $galeria['thumbs']){
	$tmp_slides = explode('~', $galeria['thumbs']);

	if(!empty($tmp_slides)) foreach ($tmp_slides as $k => $slide){
		$tmp_data_slide = explode('@', $slide);
		$slides[] = $tmp_data_slide;
	}
}

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>',
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>'
); ?>
<div class="container">
	<?php if(isset($banners[315]) && $banners[315] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[315]) && $banners[315] ? $banners[315] : '<p>Jornadas top 970 x 90</p>'; ?></div>
	<?php } ?>
	<?php if(isset($banners[316]) && $banners[316] || $mostrar_banners){ ?>
		<div class="banner banner_1140_115"><?php echo isset($banners[316]) && $banners[316] ? $banners[316] : '<p>Jornada Push 1140x115</p>'; ?></div>
	<?php } ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/eventos'); ?>">Eventos</a></li>
		<li>Jornadas</li>
	</ul>
	<?php if($success){ ?>
		<div id="gracias" class="alert alert-success alert-dismissible" role="alert">
			<div>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h2>Muchas gracias, tu Registro a la Jornada de Casamientos Online fue enviado con éxito.</h2>
				<p class="bajada">En breve recibirás un <strong>email confirmando tu participación y el cupón de acceso a la expo</strong>. No olvides revisar tu correo no deseado. Cualquier inconveniente podés escribirnos a <a href="mailto:jornadas@casamientosonline.com" class="green"><strong>jornadas@casamientosonline.com</strong></a></p>			
				
			</div>
		</div><!-- #gracias -->
	<?php } ?>
	<h1 class="title_sep">Jornada de Casamientos Online</h1>
	<img class="gum_60 header_eventos_pic" src="<?php echo base_url('/assets/images/jornadas_registro_header.jpg'); ?>" />
	
	<div class="row">
		<div id="intro_jornada" class="content col-md-8">
			<h2>¡LA MEJOR EXPO PARA NOVIAS!</h2>
			<div id="info_jornada">
			
				<div class="col-md-5 col-xs-12 top">
					<img src="<?php echo base_url('/assets/images/icon_info_jornada_organizadas.png'); ?>" />
					<div class="data">
						<h4>Jornada Nº41</h4>
						<p>de Casamientos Online</p>
					</div>
				</div>
				
				<div class="col-md-7 col-xs-12 top">
					<img src="<?php echo base_url('/assets/images/icon_info_jornada_calendar.png'); ?>" />
					<div class="data">
						<h4>27 de Septiembre de 2017</h4>
						<p>Reservá tu lugar!</p>
					</div>
				</div>
				
				<div class="col-md-5 col-xs-12">
					<img src="<?php echo base_url('/assets/images/icon_info_jornada_entrada.png'); ?>" />
					<div class="data">
						<h4>Entrada Gratuita</h4>
						<p>Cupos limitados</p>
					</div>
				</div>
				
				<div class="col-md-7 col-xs-12">
					<img src="<?php echo base_url('/assets/images/icon_info_jornada_mapa.png'); ?>" />
					<div class="data">
						<h4>Centro Cultural Borges</h4>
						<p>Galerías Pacífico, Viamonte 750</p>
					</div>
				</div>
			</div><!-- #info_jornada -->
			<a id="btn_registro_jornada_mobile" href="<?php echo base_url('/eventos/expo-novias-' . date('Y') . '/registro/novios'); ?>" class="btn btn-default" style="display:none;">Registrarse a la Jornada</a>
			<p class="big_txt">Participá de la <strong class="pink">41º Jornada de Casamientos Online</strong> y organizá todo tu casamiento en una sóla tarde!</p>
			<p>Con 40 exitosas Jornadas organizadas, Casamientos Online nuevamente pone en práctica su alto poder de convocatoria y su experiencia en el desarrollo de eventos, desfiles y presentaciones, con el objeto de citar en un solo lugar, a novios y empresas del mercado de los casamientos.</p>
			
			<p><strong>La próxima Jornada de Casamientos Online</strong> se realizará el <strong>miércoles 27 de septiembre de 2017</strong> en el Centro Cultural Borges. Esperamos contar con tu presencia. Si te interesa participar <strong>ya podés registrarte completando el formulario</strong>. Estate atenta...te mantendremos al tanto de las novedades!</p>
			
			<p class="gum_40"><strong>Entrada gratuita - Cupos limitados</strong></p>
			
									
			<div id="sponsors_jornada_registro">
				<span>Sponsors:</span><img src="<?php echo base_url('/assets/images/logos_jornada_registro.jpg'); ?>" />
			</div>
			
			<div id="acceso_proveedor_registro_jornada">
				<p><i class="fa fa-suitcase"></i>Sos proveedor? Te interesa participar de la jornada con tu stand?</p>
				<a href="<?php echo base_url('/empresas/expo-novias'); ?>" class="btn btn-default btn-green">Solicitar información<span class="mobile_none">, click aquí</span></a>
			</div><!-- #acceso_proveedor_registro_jornada -->
		</div><!-- .content -->
		
		<aside class="col-md-4">
			<?php include('jornadas_formulario.php'); ?>
		</aside>
	</div><!-- .row -->
		
</div><!-- .container -->

<div id="participar">
	<div class="inner">
		
		<div id="iconos_participar" class="col-md-8">
			<h2 class="title_sep">¿Por qué participar en la Jornada?</h2>
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_numeros_empresas.png'); ?>" />
					</div>
					<div class="data">
						<h5>+ de 150 Stands</h5>
						<p>Charlá en persona con los mejores proveedores para casamientos en un solo lugar.</p>
					</div>
				</div><!-- .col-md-6 -->
				
				<div class="col-md-6 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_organiza_jornada.png'); ?>" />
					</div>
					<div class="data">
						<h5>Organizá tu casamiento</h5>
						<p>Encontrá las últimas tendencias e ideas para tu fiesta.</p>
					</div>
				</div><!-- .col-md-6 -->
				
				<div class="col-md-6 col-xs-6 no_margin_b clear">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_publicacion_jornadas.png'); ?>" />
					</div>
					<div class="data">
						<h5>Belleza</h5>
						<p>Armá tu look de novia en una sola tarde. Vestidos de Novia, MakeUp, Peinados, Zapatos, Ramos y Accesorios</p>
					</div>
				</div><!-- .col-md-6 -->
				
				<div class="col-md-6 col-xs-6 no_margin_b">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_degustacion_jornadas.png'); ?>" />
					</div>
					<div class="data">
						<h5>Diversión</h5>
						<p>Viví un momento increíble con Shows, Degustaciones, Fiestas en Vivo, StandUp, Charlas, Sorteos y mucho más</p>
					</div>
				</div><!-- .col-md-6 -->
				
				
			</div>
		</div><!-- #iconos_participar -->
		
		<div id="mapa_jornada_registro" class="col-md-4">
			
			<img class="como_llegar" src="<?php echo base_url('/assets/images/como_llegar_jornada_registro.png'); ?>" />
			<img src="<?php echo base_url('/assets/images/test_images/mapa_jornada_registro.jpg'); ?>" />
		</div><!-- #mapa_jornada_registro -->
		
	</div><!-- .inner -->
</div><!-- #participar -->

<div class="container">
	<div class="row">
		<div id="ultima_jornada" class="content col-md-8">
			<h2 class="title_sep gum_30">Reviví nuestra última Jornada del<br/>28 de Septiembre del 2016</h2>
			
			<iframe width="560" height="315" src="https://www.youtube.com/embed/PFMHWWr-p7c" frameborder="0" allowfullscreen></iframe>
			<p class="epigrafe_media gum_30">Video by <a href="http://www.casamientosonline.com/planea-tu-casamiento/1/buenos-aires/guia-de-servicios/8/foto-y-video/18955/cucu-amour">Cucu Amour</a></p>
			
			<p>El 28 de septiembre se realizó la <strong>Jornada número 39º de Casamientos Online</strong>. Nuestro ya tradicional evento de una tarde que reúne a las parejas que se están por casar con las principales empresas de servicios para casamientos. <strong>Hubo de todo: degustaciones, barra de tragos, cata de vinos, shows en vivo, moda, tendencias, vestidos de novia de todos los estilos</strong> y muchas otras novedades más!!!</p>
			<p>Viniste a la Jornada? Reviví los mejores momentos de <strong>la mejor expo para novias del país!!</strong> Repasá las tendencias, buscá ideas y dejate inspirar por lo mejor para tu casamiento!</p>
			<p class="gum_50">Y, si te estás por casar, preparate porque el <strong>10 de mayo de 2017 será la próxima Jornada.</strong> No te la pierdas!! <strong>Registrate!</strong></p>
			
			<div class="relative">
			<?php if(!empty($slides)){ ?>
				<h3 class="title_sep">Galería de Imágenes</h3>
				<div class="galeria_standard gum_10">
					<div class="big_pic carousel slide" id="carousel-jornadas" data-ride="carousel" data-pause="hover" data-interval="false">
						<div class="carousel-inner" role="listbox">
							<?php foreach ($slides as $k => $slide){
								if(isset($slide[0]) || isset($slide[1]) || isset($slide[2]) || isset($slide[3])){ ?>
									<div class="align_c item <?php echo $k == 0 ? 'active' : ''; ?>">
								 		<a href="http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>" class="item_minis" data-gallery title="<?php echo $slide[2]; ?>">
									    	<img src="http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>">
									    </a>
									    <div class="epigrafe">
											<span class="col-md-9"><?php echo $slide[3]; ?></span>
											<div class="social col-md-3">
												<input type="hidden" class="id_redes" value="<?php echo 'eventos/expo-novias-' . date('Y'); ?>" />
												<a class="compartir_facebook no_margin" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
												<a class="twitter" href="https://twitter.com/intent/tweet?url=<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>&text=<?php echo urlencode($slide[2]);?>"><i class="fa fa-twitter"></i></a>
												<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>&description=<?php echo urlencode($slide[2]);?>&media=http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
											</div>
										</div>
									</div>
								<?php }
							} ?>
						</div><!-- .carousel-inner -->		
					</div><!--  .big_pic -->	
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-jornadas" role="button" data-slide="prev">
						<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
						<span class="sr-only valign">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-jornadas" role="button" data-slide="next">
						<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div><!-- #galeria_standard -->
			<?php } ?>
				<p class="epigrafe_media">Fotos by <a href="http://cuika.com.ar/wp/">CUIKA</a></p>
				<div id="bottom_jornada_entrada">
					<p>No te quedes afuera! Reservá tu lugar hoy mismo!</p>
					<a href="<?php echo base_url('/eventos/expo-novias-' . date('Y') . '/registro/novios'); ?>" class="btn btn-default">Solicitar entrada</a>
				</div>	
			</div><!-- .relative -->
		</div><!-- #ultima_jornada -->
		
		<aside id="jornadas_anteriores" class="content col-md-4">
			
			<?php include('jornadas_anteriores_listado.php'); ?>

			<?php if(isset($banners[317]) && $banners[317] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[317]) && $banners[317] ? $banners[317] : '<p>Jornadas 300x250<span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[318]) && $banners[318] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[318]) && $banners[318] ? $banners[318] : '<p>Jornadas 300x250<span>Posicion 2</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[319]) && $banners[319] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[319]) && $banners[319] ? $banners[319] : '<p>Jornadas 300x250<span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
		</aside>
		
	</div>
</div><!-- #participar -->
<?php 
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
$flechas = TRUE;
include('popup_fotos.php');
include('footer.php'); ?>