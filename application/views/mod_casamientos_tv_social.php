<div class="social pull-right">
	<span>Compartir</span>
	<?php switch ($tipo) {
		case 'empresas':
			$codigo = 'e';
			break;
		case 'casamientos':
			$codigo = 'c';
			break;
	} ?>
	<input type="hidden" class="id_redes" value="casamientos-tv/<?php echo $video['titulo_seo'] . '_CO_v' . $video['id'] . '_' . $codigo; ?>" />
	<a class="compartir_facebook no_margin" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
	<a class="twitter" href="https://twitter.com/intent/tweet?url=<?php echo base_url('/casamientos-tv/' . $video['titulo_seo'] . '_CO_v' . $video['id'] . '_' . $codigo); ?>&text=<?php echo urlencode($video["titulo_galeria"]);?>"><i class="fa fa-twitter"></i></a>
	<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/casamientos-tv/' . $video['titulo_seo'] . '_CO_v' . $video['id'] . '_' . $codigo); ?>&description=<?php echo urlencode($video["titulo_galeria"]);?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
	<a class="mobile_block" href="whatsapp://send?text=http://webdevelopmentscripts.com"><i class="fa fa-whatsapp"></i></a>
	
	<a href="#" data-toggle="modal" data-target="#enviar-amigo"><i class="fa fa-envelope"></i></a>
</div>