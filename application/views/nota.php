<?php
if(strlen($nota['copete']) >= 135) $str_mas        = '...';
if(strlen($nota['titulo']) >= 47)  $str_mas_titulo = '...'; ?>
<div class="col-md-12">	
	<div class="box horizontal<?php if(isset($show_image)) { ?> todas_las_secciones<?php } ?>">
		<?php if(!isset($show_image) || $show_image){ ?>
			<a href="<?php echo base_url('/consejos/' . $nota["titulo_seo"] . '_CO_n' . $nota["id"]); ?>" class="img_wrapper <?php if(!$nota['pic']) echo 'pic_generica'; ?>">
				<?php if($nota['pic']){ ?>
					<img onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" src="http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" alt="<?php echo $nota['titulo_pic']; ?>" />
				<?php } ?>
			</a>
		<?php } ?>
		<div class="data">
			<?php if(isset($nombre_subseccion) && $nombre_subseccion){ ?>
				<p class="cat_segundo_nivel"><?php echo rtrim($nombre_subseccion, ', '); ?></p>
			<?php } ?>
			<h3><a <?php if(strlen($nota['titulo']) >= 47){ echo 'data-toggle="tooltip" data-placement="bottom"'; } ?> title="<?php echo $nota['titulo']; ?>" href="<?php echo base_url('/consejos/' . $nota["titulo_seo"] . '_CO_n' . $nota["id"]); ?>"><?php echo mb_substr(stripslashes($nota['titulo']), 0, 47).$str_mas_titulo; ?></a></h3>
			<?php if(isset($com) && $com){ ?>
				<p class="descripcion"><?php echo $com . '...'; ?></p>
			<?php }else{ ?>
				<p class="descripcion"><?php echo mb_substr(stripslashes($nota['copete']), 0, 135).$str_mas; ?></p>
			<?php } ?>
			<div class="actions">
				<div class="social">
					<input type="hidden" class="id_redes" value="consejos/<?php echo $nota['titulo_seo'] . '_CO_n' . $nota['id']; ?>" />
					<?php if(!isset($no_social) || (isset($no_social) && !$no_social)){ ?>
						<a class="compartir_facebook" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/intent/tweet?url=<?php echo base_url('/consejos/' . $nota["titulo_seo"] . '_CO_n' . $nota["id"]); ?>&text=<?php echo urlencode($nota['titulo']);?>"><i class="fa fa-twitter"></i></a>
						<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/consejos/' . $nota["titulo_seo"] . '_CO_n' . $nota["id"]); ?>&description=<?php echo $nota['titulo'];?>&media=http://media.casamientosonline.com/images/<?php echo str_replace('@', '.', $nota['pic']); ?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
					<?php } ?>
				</div>
				<a href="<?php echo base_url('/consejos/' . $nota["titulo_seo"] . '_CO_n' . $nota["id"]); ?>">Seguir Leyendo <i class="fa fa-chevron-right"></i></a>
			</div><!-- .actions -->
		</div><!-- .data -->
	</div><!-- .box -->	
</div><!-- .col-md-12 -->