<?php
$bodyclass = 'page listado listado_promociones';
include('header.php'); ?>
<div class="container">
	
	<?php include('listado_header.php'); ?>
	
	<div id="listado_wrapper">
		
		<aside>
			<?php include('listado_filtros.php');
			include('listado_banners.php'); ?>
		</aside>
		
		<section id="content_listado">
			
			<?php include('listado_solapas.php');

			$puntos_suspensivos = "";
			$puntos_suspensivos_aclaracion = "";
			if(isset($productos)&&$productos) foreach ($productos as $k => $producto){
				include('producto.php');
				$puntos_suspensivos = "";
			}
			include('paginacion.php'); ?>
		</section><!-- #content_listado -->
	</div><!-- #listado_wrapper -->
</div><!-- .container -->
<?php include('footer.php'); ?>