<?php if(is_array($elementos_random)){ ?>
	<section id="listado_proveedores" class="inner <?php echo isset($class)&&$class?$class:''; ?>">
			<h2 class="title_sep">Todo para tu Casamiento en
			<span class="dropdown">
				<a class="green suc_selector" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label-empresas-destacadas">
					<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
						<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') . (isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>#listado_proveedores"><?=$suc["sucursal"];?></a>
					<?php } ?>
				</ul>
			</span>
			</h2>
			<div class="gutters">
				<?php 
				foreach ($listado_seo_random as $k => $col){ ?>
					<div class="col-md-3 col-xs-6 col-xxs-12">
						<?php $str_mas = '';
						foreach ($col as $j => $elem){
							if(strlen($elem) > 30) $str_mas = '...'; ?>
							<h3><a <?php if(strlen($elem) > 30){ echo 'data-toggle="tooltip" data-placement="bottom"'; } ?> title="<?php echo $elem; ?>" href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $listado_seo_random_guiones[$k][$j] . '_CO_r' . $j); ?>"><span class="seo_s"><?php echo substr($elem, 0, 30).$str_mas; ?></span><span class="seo_n" style="display:none;"><?php echo $elem; ?></span></a></h3>
						<?php $str_mas = '';
						} ?>
					</div>
				<?php } ?>
			</div><!-- .gutters -->
		<div class="clear"></div>
	</section><!-- #listado_proveedores -->
<?php } ?>