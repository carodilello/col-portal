<?php
if($faqs){
	$i = 0;
	foreach ($faqs as $label => $values){
		if(!empty($values['values']) and $values['values'] <> 'Entre $0 y $0' and $values['values'] <> 'Desde 0 hasta 0'){
			$i += 1;
			if( (is_array($values['values']) && !empty($values['values'][0])) || (!is_array($values['values'])) && !empty($values['values'])){ ?>
				<tr>
					<td><i class="fa <?php echo $values['icono'] ? ($values['icono']) : 'fa-car'; ?>"></i><strong><?php echo $label; ?></strong></td>
					<td>
						<?php if(is_array($values['values'])){ ?>
							<ul>
								<?php foreach ($values['values'] as $key => $value){ ?>
									<li><?php echo $value; ?></li>
								<?php } ?>
							<ul>
						<?php }else{ ?>
							<p><?php echo $values['values']; ?></p>
						<?php } ?>
					</td>
				</tr>
			<?php }
		}
		if(isset($minisitio_home) && $minisitio_home && $i == 5) break;
	}
}