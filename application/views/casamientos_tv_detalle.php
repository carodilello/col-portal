<?php
$bodyclass = 'casamientos_tv video_detalle';
include('header.php');

$proveedores = '';
$provs_info = array();
$provs = explode('~', $video['proveedores']);
if(!empty($provs)) foreach ($provs as $key => $value) {
	$provs_data = explode('@', $value);
	if($provs_data[0]){
		if(count($provs_data) > 10){
			$provs_info[$key] = array(
				'id_proveedor' 		=> $provs_data[2],
				'nombre_proveedor'  => $provs_data[1],
				'nombre_rubro' 		=> $provs_data[3],
				'logo_proveedor' 	=> $provs_data[7],
				'breve' 			=> $provs_data[8],
				'subdominio'		=> $provs_data[9],
				'rubro'				=> $provs_data[10]
			);
		}else{
			$provs_info[$key] = array(
				'id_proveedor' 		=> $provs_data[2],
				'nombre_proveedor'  => $provs_data[1],
				'nombre_rubro' 		=> $provs_data[3],
				'logo_proveedor' 	=> $provs_data[7],
				'breve' 			=> '',
				'subdominio'		=> $provs_data[8],
				'rubro'				=> $provs_data[9]
			);
		}
	}
}

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="'.base_url('/assets/js/funciones_redes.js').'"></script>',
	'<script type="text/javascript" src="'.base_url('/assets/js/funciones_casamientostv.js').'"></script>'
); ?>
<div id="header_casamientos_tv">
	<div id="print_content">
		<img src="http://img.youtube.com/vi/<?php echo $video['pic_name'];?>/0.jpg">
		<h1><?php echo $video["titulo_galeria"]; ?></h1>
		<p><?php echo $video['descripcion_galeria']; ?></p>
		<p>Proveedor: <?php echo $nombre_proveedor; ?> (<?php echo $nombre_rubro; ?>)</p>
	</div>
	<div class="container">
		<ul id='breadcrumbs'>
			<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
			<li><a href="<?php echo base_url('/casamientos-tv'); ?>">Casamientos TV</a></li>
			<?php if($id_seccion){ ?>
				<li><a href="<?php echo base_url('/casamientos-tv/' . $nombre_seccion . '_CO_ct' . $id_seccion); ?>">Listado</a></li>
			<?php } ?>
			<li><?php echo $video['titulo_galeria']; ?></li>
		</ul>
		<h1 class="title_sep">Casamientos TV</h1>

		<div id="big_video_slider">
			<div id="video">
				<iframe src="http://www.youtube.com/embed/<?php echo $video['pic_name'];?>?wmode=opaque&amp;rel=0" class="iframe_styled" frameborder="0" allowfullscreen=""></iframe>
			</div>
			<div class="box">
				<h2><?php echo $video['titulo_galeria']; ?></h2>
				<div class="row">
					<p class="descripcion col-md-9"><?php echo $video['descripcion_galeria']; ?></p>
					<div class="col-md-3">
					<?php 
					$detalle = TRUE;
					include('mod_casamientos_tv_social.php'); ?>
					</div>	
				</div>
					
				<?php if(!empty($provs_info)) foreach ($provs_info as $key => $prov){ ?>
					<div class="bottom clear">
						<div class="proveedor">
							<div class="img_wrapper"><a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro']; ?>"><img src="http://media.casamientosonline.com/logos/<?php echo $prov['logo_proveedor']; ?>" /></a></div>
							<div class="data">
								<h3><a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro']; ?>"><?php echo $prov['nombre_proveedor']; ?></a></h3>
								<p class="zona"><?php echo $prov['nombre_rubro']; ?></p>
								<?php if($prov['breve']){ ?>
									<p class="descripcion_proveedor"><?php echo $prov['breve']; ?></p>
								<?php } ?>
							</div><!-- .data -->
						</div><!-- .proveedor -->
						<a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro']; ?>" class="btn btn-default">Visitar Minisitio</a>
					</div><!-- .bottom -->
				<?php } ?>
			</div><!-- .box -->
		</div><!-- .big_video_slider -->		
	</div> <!-- .container -->	
</div><!-- #casamientos_tv -->

<div class="container">
	<div id="split">
		<?php if($videos_relacionados){ ?>
			<section id="primary">
				<div id="grid_galeria">
					<div id="header_grid_galeria">
						<h3>Videos Relacionados</h3>
					</div><!-- #header_grid_videos -->
					<div class="row">
						<?php
						$str_mas = '';
						$str_mas_prov = '';
						foreach ($videos_relacionados as $k => $video){
							if($video['video_prov']){
									$tipo = 'e';
								}else{
									$tipo = 'c';
								}
								$tmp_proveedor = explode('@', $video['proveedores']);
								$nombre_proveedor = $tmp_proveedor[1];
								$id_proveedor = $tmp_proveedor[2];
								if(strlen($video['titulo']) >= 25)       $str_mas = '...';
								if(strlen($nombre_proveedor) >= 30) $str_mas_prov = '...'; ?>
							
								<div class="col-md-4 col">
									<a href="<?php echo base_url('/casamientos-tv/' . $video["titulo_seo"] . '_CO_v' . $video["id"] . '_' . $tipo); ?>"><img src="http://img.youtube.com/vi/<?php echo $video['video_id']; ?>/0.jpg" /></a>
									<h3><a <?php echo strlen($video['titulo']) >= 25 ? 'data-toggle="tooltip" data-placement="top"' : ''; ?> title="<?php echo $video['titulo']; ?>" href="<?php echo base_url('/casamientos-tv/' . $video["titulo_seo"] . '_CO_v' . $video["id"] . '_' . $tipo); ?>"><?php echo substr($video['titulo'], 0, 25).$str_mas; ?></a></h3>
									<?php if($video['proveedores']){
										$tmp = explode('@', $video['proveedores']);
										if(!empty($tmp[9])){ ?>
											<a href="<?php echo str_replace('{?}', $tmp[9], $base_url_subdomain) . $tmp[8]; ?>"><span class="proveedor_rubro"><strong><?php echo substr($nombre_proveedor, 0, 30).$str_mas_prov; ?></strong></span></a>
										<?php }
									} ?>
								</div>
						<?php 
						$str_mas = '';
						$str_mas_prov = '';
						} ?>
					</div>
				</div><!-- #grid_galeria -->
			</section><!-- #primary -->
			<aside id="secondary">
				<?php include('notas_facebook.php'); ?>
				
				<?php if(isset($banners[277]) && $banners[277] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[277]) && $banners[277] ? $banners[277] : '<p>COL TV detalle video 300 x 250<span>Posicion 1</span></p>'; ?></div>
				<?php } ?>
				<?php if(isset($banners[278]) && $banners[278] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[278]) && $banners[278] ? $banners[278] : '<p>COL TV detalle video 300 x 250<span>Posicion 2</span></p>'; ?></div>
				<?php } ?>
				<?php if(isset($banners[279]) && $banners[279] || $mostrar_banners){ ?>
					<div class="banner banner_300_250"><?php echo isset($banners[279]) && $banners[279] ? $banners[279] : '<p>COL TV detalle video 300 x 250<span>Posicion 3</span></p>'; ?></div>
				<?php } ?>
			</aside> <!-- #sidebar -->
		<?php } ?>
		<?php include('popup_amigo.php'); ?>
	</div><!-- #split -->	
</div><!-- .container --> 
<?php include('footer_info.php'); ?>