<?php
$bodyclass = 'page infonovias';
include('header.php'); ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Infonovias</li>
	</ul>
		
	<h1 class="title_sep">Infonovias</h1>
	<?php if($success){ ?>
		<div id="gracias" class="alert alert-success alert-dismissible" role="alert">
			<div>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h2>Muchas gracias, registro exitoso.</h2>
				<p class="bajada">Comenzarás a recibir el infonovias con todas la novedades del mercado de casamientos</p>
			</div>
		</div><!-- #gracias -->
	<?php } ?>
	<div id="split">
		<div id="primary">
			<img class="gum_40" src="<?php echo base_url('/assets/images/pic_infonovias.jpg'); ?>" alt="Infonovias" />
			
			<p class="intro_box box">Las infonovias son nuestro medio de contacto! A través de nuestros newsletters vas a recibir novedades, notas, promociones, invitaciones, a eventos, y muchos más...<br /><strong>Registrate para recibirla! <a href="<?php echo base_url('/infonovias/registro'); ?>"><span class="pink">Click aqui!</span></a></strong></p>
			
			<div id="filtro_infonovias">
				<h2 class="pull-left">Infonovias Anteriores</h2>
				<form class="pull-right">
					<label>Seleccionar año:</label>
					<select class="anio_infonovias">
						<?php for ($i = date('Y'); $i >= 2012; $i--){ ?>
							<option value="<?php echo $i; ?>" <?=$anio == $i? 'selected="selected"' : '';?>><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</form>
			</div>
			
			<?php if(!empty($listado_infonovias)) foreach ($listado_infonovias as $k => $infonovia){
				$tmp_fecha = explode('-', $infonovia['fecha_visual']);
				$mes = ucfirst($meses[(int) $tmp_fecha[1] - 1]); ?>
				<div class="infonovia">
					<h3 class="title_sep"><?php echo $mes; ?></h3>
					<h4><?php echo $infonovia['titulo']; ?></h4>
					<span><?php echo $tmp_fecha[2]; ?> de <?php echo $mes; ?> <?php echo $tmp_fecha[0]; ?></span>
					<p><?php echo stripslashes($infonovia['contenido']); ?></p>
					<a href="http://www.casamientosonline.com/newsletters/newsletter_<?php echo $infonovia['id']; ?>.html" class="btn btn-default">Ver Infonovia</a>
				</div><!-- .infonovia -->
			<?php } ?>
		</div><!-- .primary -->
		
		<aside id="secondary">
			<div id="form_infonovias_wrapper" class="box">
				<h2>Registro Infonovias<i class="fa fa-envelope pull-right"></i></h2>
				<?php if(validation_errors()){ ?>
					<div class="errores-generales">	
						<div><i class="fa fa-exclamation-circle"></i> 
							<?php echo validation_errors(); ?>
						</div>	
					</div> <!-- .errores-generales -->	
				<?php }
				
				echo form_open(base_url('/infonovias/gracias#gracias'), 'method="POST" role="form" class="form" id="form_infonovias"'); ?>
					<input type="hidden" name="id_origen" value="0">

			    	<div class="dialog_corner"></div>
					
					<h5>Datos del Casamiento</h5>

					<div class="form-group has-feedback has-success">
						<input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']; ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
						<div class="help-block with-errors"></div>
					</div>
		
					<input type="hidden" name="ubicacion" value="">
					<div class="form-group has-feedback has-success gum_30">
						<input type="text" name="invitados" placeholder="Cantidad de Invitados"  value="<?php echo $data_form['invitados']?$data_form['invitados']:set_value('invitados'); ?>" class="form-control" required>
						<div class="help-block with-errors"></div>
					</div>
					
					<h5>Datos del Contacto</h5>
							        
			        <div class="form-group has-feedback">
					    <input type="text" name="nombre" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control" required>
					    <div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group has-feedback">
					    <input type="text" name="apellido" placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control" required>
					    <div class="help-block with-errors"></div>
					</div>
					
					 <div class="form-group has-feedback">
						<input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group has-feedback">
						<input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control" required />
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback has-success">
						<select name="id_sucursal" required class="form-control">
							<option value="">Sucursales</option>
							<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
								<option value="<?php echo $suc["id"]; ?>"  <?=set_value('id_sucursal')==$suc["id"]||(!set_value('id_provincia')&&$suc["id"]==$data_form['id_sucursal'])?'selected':'';?>><?php echo $suc["sucursal"]; ?></option>
							<?php } ?>
						</select>
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback has-success">
						<select name="id_provincia" required class="form-control provincias">
							<option value="">Provincia</option>
							<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
								<option value="<?php echo $k; ?>"  <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
							<?php } ?>
						</select>
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group has-feedback gum">
						<select name="id_localidad" required class="form-control localidades">
							<option value="">Localidad</option>
							<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
								<option value="<?php echo $k; ?>"  <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
							<?php } ?>
						</select>
						<div class="help-block with-errors"></div>
					</div>
					
					<input class="btn btn-default submit" type="submit">
				</form>
			</div><!-- #form_infonovias_wrapper -->	
			<div class="banner banner_300_250"><?php echo isset($banners['326']) && $banners['326'] ? $banners['326'] : '<p>Home 300x250 posicion 1</p>'; ?></div>
			<div class="banner banner_300_250"><?php echo isset($banners['327']) && $banners['327'] ? $banners['327'] : '<p>Home 300x250 posicion 2</p>'; ?></div>
							
			<div id="categorias_notas_sidebar">
				<a id="notas_proveedores_btn" href="<?php echo base_url('/consejos/empresas_CO_cn109_s402'); ?>" class="box">
					<img src="<?php echo base_url('/assets/images/icon_notas_proveedores.png'); ?>" alt="Notas de empresas | Todas las novedades" />
					<h4>Notas de empresas</h4>
					<p>Todas las novedades</p>
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
			
		</aside>
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>