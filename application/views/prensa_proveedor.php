<?php
$bodyclass = 'prensa site_proveedor';
include('header_site_proveedor.php'); ?>

<div class="container">
	<h1 class="title_sep">Prensa</h1>
	<div class="row">
	<div id="content_prensa" class="col-md-8">
		<div class="anchor" id="anchor_diarios"></div>
		<h2><i class="fa fa-newspaper-o"></i>Diarios</h2>
		<div class="articulo clear">
			<span class="fecha">29.05.2009</span>
			<h3>Diario La Nación</h3>
			<span class="titulo">Las bodas 2.0, tan cerca del altar como de la Web</span>
			<p>Los novios generan un nuevo negocio en torno a sitios de Internet personalizados</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-nacion-05-12.jpg'); ?>">Ver artículo</a>
		</div>
		
		<div class="articulo clear">
			<span class="fecha">25.04.2009</span>
			<h3>Diario Clarin - Suplemento Mujer</h3>
			<span class="titulo">La hora de la Fiesta</span>
			<p>Estrategias para abaratar costos y los nuevos hábitos de los casamientos . Secretos y opiniones de especialistas para que la celebración sea un éxito</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-clarin-25-04-2009.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">22.11.2008</span>
			<h3>El Litoral</h3>
			<span class="titulo">Toda tu boda en un click</span>
			<p>Casamientosonline.com es un portal de internet que permite contactar a parejas próximas a casarse con empresas de todos los rubros, para facilitar la organización de la fiesta de boda.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-el-litoral-22112008.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">04.05.2007</span>
			<h3>La Razón</h3>
			<span class="titulo">Los casamientos temáticos, una moda sofisticada y muy cara</span>
			<p>Una fiesta de categoría puede costar más de $100 mil, como en París o Venecia. Los wedding planner, muy requeridos.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-razon-032008.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">04.05.2007</span>
			<h3>El Cronista</h3>
			<span class="titulo">Casamientos: cada vez hay menos, pero son más caros y sofisticados</span>
			<p>Los especialistas dicen que el mercado más que duplicó su facturación en los últimos cinco años. No sólo porque se invita a un 30% más de personas y se incluyen bebidas y se incluyen bebidas más caras, "wedding planners" y ambientadores de salón, sino porque se sumaron extras tan insólitos como peceras como centros de mesa o artistas que pintan óleos de la fiesta.	</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-el-cronista-04052007.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">10.2005</span>
			<h3>La Nación</h3>
			<span class="titulo">Metronovios, un fenómeno que crece</span>
			<p>Cada vez son más tenidos en cuenta.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-nacion-09102005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">10.2005</span>
			<h3>Mañana Profesional</h3>
			<span class="titulo">Un portal temático sólo para novios</span>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-manana-profesional-102005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">11.2005</span>
			<h3>La Nación</h3>
			<span class="titulo">Emprendedores: cómo evitar fracasos</span>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-nacion-112005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">07.2005</span>
			<h3>Clarín</h3>
			<span class="titulo">El mercado de las novias</span>
			<p>$800 millones para decir "sí, quiero".</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-clarin-072005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">08.2005</span>
			<h3>Infobae</h3>
			<span class="titulo">Las claves de la boda perfecta</span>
			<p>Costos y consejos para organizarla.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-infobae-082005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">08.2005</span>
			<h3>La Nación</h3>
			<span class="titulo">Bodas en línea</span>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-nacion-informatica.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">03.2005</span>
			<h3>Infobae</h3>
			<span class="titulo">La organización de bodas, un nuevo mercado para PyMEs</span>
			<p>Una actividad que genera $700m por año en 25 rubros. La organización de una boda se ha convertido en una verdadera fuente para los negocios. El monto que se destina a una fiesta de proporciones "medianas", con 170 invitados promedio es de unos 10.000 pesos.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-infobae-032005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>La Capital</h3>
			<span class="titulo">La fiesta inolvidable</span>
			<p>Las parejas que conviven desde hace tiempo por lo general financian el festejo y adoptan un perfil más informal.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-capital.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>La Razón</h3>
			<span class="titulo">Casarse ya no es un dolor de cabeza</span>
			<p>Desde la elección del lugar para celebrar la ceremonia religiosa y la reserva del salón de fiesta, hasta la posibilidad de saber en tiempo real los regalso recibidos. Todo, con el mínimo trámite de encender la computadora.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-razon-consumo.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>Clarín</h3>
			<span class="titulo">Romper el molde</span>
			<p>Desde casamientos en Las Vegas hasta libretas a domicilio por internet. Todo para dar el sí de una manera diferente.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-clarin-sumplemento-mujer.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>El Día</h3>
			<span class="titulo">Las dos caras del amor que navegan por Internet</span>
			<p>Se calcula que son más de 5.000 los platenses que cotidianamente chatean por la red en busca de nuevos amores. Algunos se concretan y terminan en casamientos organizados a través de la misma red. Y otros naufragan en el desengaño, como el caso de una profesional panameña que creyó encontrar en nuestra ciudad al amor de su vida.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-el-dia-informacion.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>La Nación</h3>
			<span class="titulo">Novias por Internet</span>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-la-nacion-economia-y-negocios.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear no_border gum">
			<span class="fecha">2005</span>
			<h3>El Cronista Comercial</h3>
			<span class="titulo">Con la mira en los matrimonios ajenos</span>
			<p>El flamante "site" de productos y servicios para bodas.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/diario-el-cronista-comercial.jpg'); ?>">Ver artículo</a>

		</div>
			
		<div class="anchor" id="anchor_revistas"></div>			
		<h2><i class="fa fa-file-text-o"></i>Revistas</h2>
		
		<div class="articulo clear">
			<span class="fecha">01.2009</span>
			<h3>Ohlalá!</h3>
			<span class="titulo">La vida online</span>
			<p>Web 2.0 se transpormó en una gigantesca conversación. Te sugerimos diez sitios que no deberías dejar de visitar para vivir este fenómeno social.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-ohlala-2009-01.pdf'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">11.2008</span>
			<h3>Puerto Negocios</h3>
			<span class="titulo">Casamientos Online: un negocio con el que todos se benefician</span>
			<p>Surgió hace casi una década en Capital Federal y Gran Buenos Aires pero en 2005 recaló en la ciudad de Santa Fe. La idea original pretende unir dos necesidades: la de una pareja que necesita información para organizar su casamiento, y la de los proveedores que quieren difundir productos y servicios. Aristas de un negocio que crece por minuto.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-puerto-negocios-112008.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">22.11.2008</span>
			<h3>Nosotros (Diario El Litoral)</h3>
			<span class="titulo">Toda tu boda en un click</span>
			<p>Casamientosonline.com es un portal de internet que permite contactar a parejas próximas a casarse con empresas de todos los rubros, para facilitar la organización de la fiesta de boda.</p>
			<a class="btn btn-default" target="_blank" href="">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">08.2008</span>
			<h3>InfoBrand</h3>
			<span class="titulo">Casémonos vía Web</span>
			<p>Con la meta de brindar todo tipo de ayuda para la organización de una boda, casamientosonline.com se convirtió en el sitio líder de un mercado muy particular, que posee un tipo de cliente efímero y una cartera de proveedores muy atomizada.</p>
			<a class="float_r" href="<?php echo base_url('/assets/images/prensa/revista-infobrand-082008.pdf'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">06.2008</span>
			<h3>Hacer Familia</h3>
			<span class="titulo">S.O.S. me caso</span>
			<p>Organizar la fiesta de casamiento, paradójicamente, puede ser un factor de estrés, de conflicto entre los novios y las familias,. ¿Cómo hacer para que prime lo esencial sin que el nuevo matrimonio quede ahogado en gastos, medios y planes?</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-hacer-familia-062008.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">09.2006</span>
			<h3>Miradas</h3>
			<span class="titulo">Sí, quiero</span>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-miradas-092006.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">08.2006</span>
			<h3>Quién</h3>
			<span class="titulo">La celebración</span>
			<p>Desde la fiesta hasta el vestido pasando por el ajuar y el catering, elaboramos una guía completa para planificar tu casamiento. Presupuestos, opciones y tendencias para organizar una boda inolvidable.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-quien-082006.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">01.2006</span>
			<h3>Nosotros</h3>
			<span class="titulo">Planear "el" día</span>
			<p>Organización del casamiento. El reconocido wedding planner porteño Martín Roig estuvo en nuestra ciudad para participar de "Síquiero, Expoboda 2006". En una charla con novias santafesinas, compartió su experiencia y brindó consejos para tener en cuenta a la hora de organizar el mejor de los casamientos.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-nosotros-072006.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">01.2006</span>
			<h3>Quién</h3>
			<span class="titulo">Donde cae el sol</span>
			<p>Festejar a cielo abierto es última moda en eventos. Y supera la tendencia de alquilar salones cerrados. En campos y estancias, de día o de noche, se ambienta con livings, almohadones velas, flores y carpas para un estilo que combina el lujo con el relax.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-quien-012006.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">10.2005</span>
			<h3>Nosotros</h3>
			<span class="titulo">Bodas en línea</span>
			<p>Con la llegada de la primavera se lanzó en Santa Fe la versión local de casamientosonline.com, un portal de Internet que hace furor en Capital Federal y Rosario. Dedicada a las parejas que están preparando su boda, ofrece servicios, provedores y asesoramiento especializado al alcance de un click.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-nosotros-102005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">09.2005</span>
			<h3>Noticias</h3>
			<span class="titulo">Noticias</span>
			<p>Se impone la moda de casarse para celebrar uniones afianzadas. Fiestas transgresoras, pero con vestido blanco. Hijos, invitados de honor.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-noticias-092005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>Nueva</h3>
			<span class="titulo">El negocio de las bodas</span>
			<p>En la era de la imagen, las bodas se suman a la movida y hoy se organizan más como un evento que como una celebración familiar. El mercado se amplía y se profesionaliza.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-nueva.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">2005</span>
			<h3>NE</h3>
			<span class="titulo">Yo me quiero casar...</span>
			<p>La decisión, los parientes, los preparativos y los problemas. Una ayuda online para novios al borde de un ataque de nervios.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-ne-notiexpress.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear no_border gum">
			<span class="fecha">2005</span>
			<h3>Elle Novias</h3>
			<span class="titulo">Online</span>
			<p>Resolver casi todo virtualmente. La modernidad se impone.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/revista-elle-novias-internet.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="anchor" id="anchor_internet"></div>
		<h2><i class="fa fa-desktop"></i>Internet</h2>
		
		<div class="articulo clear">
			<span class="fecha">17.12.2008</span>
			<h3>Cronista</h3>
			<span class="titulo">"Sí, quiero": cada vez con más los novios que eligen cash como regalo de bodas</span>
			<p>Dar el "sí" no es sólo cuestión de amor, sino que también hay matices financieros que los enamorados deben tener en cuenta a la hora de casarse. Elegir dinero como regalo de bodas es una nueva tendencia que ayuda a los novios a planificar gastos y ahorrar para el futuro. Empresas, agencias de turismo y bancos se lanzaron a atender a este nicho, que ya no es un negocio que deja millones.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/web-cronista-17012008.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">13.05.2008</span>
			<h3>Cronista</h3>
			<span class="titulo">Inflación: la última excusa de los argentinos para no casarse</span>
			<p>Con el alza del costo de vida, los proveedores no aceptan cancelar con anticipación el valor de la fiesta y ajustan sus productos con la suba del combustible y el dólar.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/web-el-cronista-13052008.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear">
			<span class="fecha">19.12.2005</span>
			<h3>Punto Biz</h3>
			<span class="titulo">Casamientos Online compartió su experiencia con otras pymes</span>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/web-punto-biz-19122005.jpg'); ?>">Ver artículo</a>

		</div>
		
		<div class="articulo clear no_border">
			<span class="fecha">2005</span>
			<h3>Punto Biz</h3>
			<span class="titulo">Casamientos Online llegó a Rosario</span>
			<p>Es el principal portal del país que abarca la temática de los casamientos de manera integral. Cuenta con lso servicios y soluciones necesarias para la organización de bodas.</p>
			<a class="btn btn-default" target="_blank" href="<?php echo base_url('/assets/images/prensa/web-punto-biz.jpg'); ?>">Ver artículo</a>

		</div>
		
	</div><!-- .col-md-8 -->
	
	<aside class="col-md-4">
		<div class="tipos_de_medio">
			<a href="#anchor_diarios" class="box">
				<i class="fa fa-newspaper-o"></i>
				<h4>Diarios</h4>
				<p>Notas y artículos en diarios</p>
				<i class="fa fa-angle-right"></i>
			</a>
			<a href="#anchor_revistas" class="box">
				<i class="fa fa-file-text-o"></i>
				<h4>Revistas</h4>
				<p>Notas y artículos en Revistas</p>
				<i class="fa fa-angle-right"></i>
			</a>
			<a href="#anchor_internet" class="box">
				<i class="fa fa-desktop"></i>
				<h4>Internet</h4>
				<p>Notas y artículos en la web</p>
				<i class="fa fa-angle-right"></i>
			</a>
			
			
		</div>
	</aside>
	
	
	
	</div>
</div><!-- container -->

<?php include('footer_info.php'); ?>