<?php
$bodyclass = 'page presupuesto_general';
include('header.php');

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_presupuesto_general.js') . '"></script>'
); ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo'] . '/fiestas-de-casamiento'); ?>">Guia de Empresas</a></li>
		<li>Presupuesto general</li>
	</ul>
	<?php 
	include("mod_gracias.php");
	if($registro_presupuesto_general){ ?>
		<div id="gracias" class="gracias-home">
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3><strong>Gracias</strong>, su solicitud se ha enviado exitosamente! En breve recibirá novedades</h3>
			</div>
		</div><!-- #gracias -->	
	<?php } ?>
	<h1 class="title_sep">Qué estas buscando para tu Casamiento?</h1>
	<p class="box">Contanos que estas buscando, cuales son tus preferencias y necesidades... Nosotros <strong>te ayudamos</strong> a armar <strong>tu casamiento</strong> con las mejores propuestas! Enviaremos tu consulta a las empresas que concuerden con tu búsqueda para que puedas generar entrevistas personalizadas y exclusivas sin perder tiempo.</p>
	<?php if(!isset($gracias)){ ?>
		<?php if($config['mantenimiento_site'] || $config['mantenimiento_presupuesto']){ ?>
			<p>Estamos realizando tareas de mantenimiento. Intente nuevamente en unos minutos. Muchas gracias.</p>
		<?php }else{
			if(validation_errors()){ ?>
				<div class="errores-generales"><?php echo validation_errors(); ?></div>
			<?php }
			echo form_open(base_url('/solicitar-presupuesto-general#gracias'),' id="form_presupuesto_general" method="POST" role="form" class="form row"'); ?>
					<?php if(isset($id_zona) && $id_zona){ ?>
						<input type="hidden" value="<?php echo $id_zona; ?>" class="id_zona" />
					<?php } ?>
					<div class="col-md-12">
						<h3><i class="fa fa-map-marker"></i>Selecciona tu zona / provincia</h3>
					</div>
					<div class="form-group col-md-6 gum_40">
						<?php 
						if(is_string($zonas['hijo'])){ ?>
							<select name="resp_<?php echo $zonas['id']; ?>" class="hijo_<?php echo $zonas['id']; ?> form-control" <?=$zonas['requerido']?'required':'';?> onchange="javascript:activarRubros(this.value)">
								<option value="">Seleccione</option>
								<?php $zonas = explode('@', $zonas['hijo']);
								foreach ($zonas as $k => $zona){
									$z = explode('#', $zona); ?>
									<option value="<?php echo $z[0];?>"><?php echo $z[1];?></option>
								<?php  } ?>
							</select>
							<div class="help-block with-errors"></div>
						<?php } ?>
					</div>
					<div class="col-md-12">
						<h3><i class="fa fa-list"></i>Selecciona los rubros de tu interes<span id="txt-rubros" style="display:none; position:relative; top:0; left:20px; color:red; font-size:14px;">Debes seleccionar al menos un rubro</span></h3>
						<input type="hidden" name="rubrosID" id="rubrosID" value="">
						<?php if($hidden) foreach ($hidden as $key => $hid){ ?>
							<input type="hidden" name="<?php echo $hid['name']; ?>" value="<?php echo $hid['value']; ?>" />
						<?php } ?>
						<fieldset class="caracteristicas_inner gum_40 rubrosBA clear"></fieldset>
					</div>
						
					<div class="col-md-12"><h3><i class="fa fa-star"></i>Detalles de tu evento</h3></div>
					<?php foreach ($items as $k => $padre){ ?>
						<div id="padre_<?php echo $padre['id']; ?>" class="form-group col-md-6">
							<?php $hijo = explode('@', $padre['hijo']); ?>
							<?php if($padre['objeto'] == 'select'){ ?>
								<select name="resp_<?php echo $padre['id']; ?>" class="hijo_<?php echo $padre['id']; ?> form-control" <?=$padre['requerido']?'required':'';?>>
									<option value="">Tipo de Evento</option>
									<?php foreach ($hijo as $k => $opcion){
										$item_opcion = explode('#', $opcion); ?>
										<option value="<?php echo $item_opcion[0]; ?>"><?php echo $item_opcion[1]; ?></option>
									<?php } ?>
								</select>
							<?php }else{
								foreach ($hijo as $k => $opcion){
									$item_opcion = explode('#', $opcion);
									if($padre['objeto'] == 'checkbox'){ ?>
										<input type="checkbox" name="resp_<?php echo $padre['id']; ?>[]" value="<?php echo $item_opcion[0]; ?>" class="hijo_<?php echo $item_opcion[0]; ?>" <?=$item_opcion[2];?> id="check_<?php echo $item_opcion[0]; ?>" />
										<label for="check_<?php echo $item_opcion[0]; ?>"><?php echo $item_opcion[1]; ?></label>
									<?php }elseif($padre['objeto'] == 'radio'){ ?>
										<input type="radio" name="resp_<?php echo $padre['id']; ?>" value="<?php echo $item_opcion[0]; ?>" class="hijo_<?php echo $item_opcion[0]; ?>" <?=$item_opcion[2];?> id="radio_<?php echo $item_opcion[0]; ?>" />
										<label for="radio_<?php echo $item_opcion[0]; ?>"><?php echo $item_opcion[1]; ?></label>
									<?php }
								}
							}
							if($padre['hijo2'] != ''){
								$hijo2 = explode('@', $padre['hijo2']);
								foreach ($hijo2 as $k => $opcion2){
									$item_hijo2 = explode('**', $opcion2);

									/*
									$item_hijo2[0]<!-- id -->
									$item_hijo2[1]<!-- descripcion -->
									$item_hijo2[2]<!-- objeto -->
									$item_hijo2[3]<!-- requerido -->
									$item_hijo2[4]<!-- accion -->
									*/
									
									if($item_hijo2[2] == 'text'){
										if($item_hijo2[1] != ""){ ?>
											<label><?php echo $item_hijo2[1]; ?></label>
										<?php } ?>
										<input type="text" name="resp_<?php echo $item_hijo2[0]; ?>" id="hijo2_<?php echo $item_hijo2[0]; ?>" class="hijo2_<?php echo $item_hijo2[0]; ?> salon_elegido" <?=$item_hijo2[3]?'required':'';?> <?=$item_hijo2[4];?> />
									<?php }
								}
							} ?>
							<div class="help-block with-errors"></div>
						</div>
					<?php } ?>

					<div class="col-md-12" style="margin-top: 40px;">
						<h3><i class="fa fa-heart"></i>Datos del casamiento</h3>
					</div>
					<div class="form-group has-feedback col-md-6 gum_40">
						<input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']; ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
						<span class="fa form-control-feedback" aria-hidden="true"></span>
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback col-md-6">
						<div class="checkbox check_form">
							<label for="fecha_evento_variable"><input name="fecha_evento_variable" type="checkbox" id="fecha_evento_variable" value="1" />La fecha del evento puede variar</label>
						</div>
						<span class="fa form-control-feedback" aria-hidden="true"></span>
					    <div class="help-block with-errors"></div>
					    <div class="clear"></div>
					</div>

					<div class="col-md-12">
						<h3><i class="fa fa-user"></i>Datos de contacto</h3>
					</div>

					<div class="form-group has-feedback col-md-6">
						
						<input type="text" name="nombre" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control" required>
					    <span class="fa form-control-feedback" aria-hidden="true"></span>
					    <div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback col-md-6">
						
						<input type="text" name="apellido" placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control" required>
					    <span class="fa form-control-feedback" aria-hidden="true"></span>
					    <div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback col-md-6">
						
						<input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control" required>
						<span class="fa form-control-feedback" aria-hidden="true"></span>
					    <div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback col-md-6">
						<input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
						<span class="fa form-control-feedback" aria-hidden="true"></span>
					    <div class="help-block with-errors"></div>
					</div>
						
					<div class="form-group has-feedback col-md-6">
						
						<select name="id_provincia" class="form-control provincias" required>
							<option value="">Seleccionar una Provincia</option>
							<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
								<option value="<?php echo $k; ?>"  <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
							<?php } ?>
						</select>
						<div class="help-block with-errors"></div>
					</div>
						
					<div class="form-group has-feedback col-md-6">
						
						<select name="id_localidad" class="form-control localidades" required>
							<option value="">Seleccionar una Localidad</option>
							<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
								<option value="<?php echo $k; ?>"  <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
							<?php } ?>
						</select>
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group has-feedback col-md-6">
						<div class="checkbox check_form">
							<label for="infonovias"><input type="checkbox" id="infonovias" name="infonovias" value="1" <?php if($data_form['infonovias']){ echo 'checked="checked"'; } ?> />Deseo recibir infonovias y promos</label>
						</div>
						<span class="fa form-control-feedback" aria-hidden="true"></span>
					    <div class="help-block with-errors"></div>
					    <div class="clear"></div>
					</div>

					<input type="hidden" name="dia_nacimiento" value="<?php echo $data_form['dia_nacimiento']?$data_form['dia_nacimiento']:set_value('dia_nacimiento'); ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" />

					<div class="form-group has-feedback col-md-12">
						<textarea placeholder="Comentarios" name="comentario" class="form-control" rows="3"></textarea>
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group col-md-12 gum_40">
						<input type="submit" class="btn-default pull-right submit" onclick="javascript:validarpresupuestoRubros()" value="Enviar Consulta" />
					</div>
				</div>
			</form>
		<?php } ?>
	<? } ?>
</div><!-- .container -->
<?php include('footer.php'); ?>