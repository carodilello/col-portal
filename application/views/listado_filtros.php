<?php if(!empty($listado_rubros)){ ?>
	<div id="filtros_wrapper">
        <?php
        if(isset($filtros_aplicados)&&$filtros_aplicados){ ?>
                <div id="filtros_aplicados" class="mobile_none">
                        <h3>Filtros Aplicados</h3>
                        <?php foreach ($filtros_aplicados as $filtro_a){ ?>
                                <span><span><strong><?php echo $filtro_a['filtro']; ?></strong>: <?php echo $filtro_a["opcion"];?></span> <a href="
                                <?php
                                $var = str_replace("|" . $filtro_a["id_filtro"]."-".$filtro_a["id_opcion"], "", urldecode($request_uri_base));
                                if(count($filtros_aplicados) == 1) $var = rtrim($var, '_');
                                echo $var;
                                if(isset($param_get)&&$param_get){ echo "?".$param_get; } ?>"><i class="fa fa-close"></i></a></span>
                        <?php } ?>
                </div> <!-- #filtros_aplicados -->
        <?php }
        if(isset($filtros_pendientes)&&$filtros_pendientes){
                foreach ($filtros_pendientes as $filtro_p){ ?>
                <ul>
                        <h3><?php echo $filtro_p["filtro"];?></h3>
                        <?php if(isset($filtro_p["opciones"])&&$filtro_p["opciones"]){
                                foreach ($filtro_p["opciones"] as $opc){ ?>
                                        <li><a href="<?=$url_filtros;?>|<?php echo $filtro_p["id"]; ?>-<?php echo $opc["id"]; ?><?php if(isset($param_get)&&$param_get){ echo "?".$param_get; } ?>"><?php echo $opc["opcion"]; ?> <span>(<?php echo $opc["total"]; ?>)</span></a></li>
                                <?php }
                        } ?>
                </ul>
                <?php }
        } ?>
	</div><!-- #filtros_wrapper -->
	<?php if(isset($banners)&&$banners){ ?>
        <div class="banners">
            <?php foreach ($banners as $banner){
                if($banner){ ?>
                    <div class="hidden"><?php echo $banner; ?></div>
                <?php } 
            } ?>
        </div>
	<?php } ?>
<?php }else{ ?>
	<div id="filtros_wrapper">
		<?php
		if(isset($filtros_aplicados)&&$filtros_aplicados){ ?>
			<div id="filtros_aplicados" class="mobile_none">
				<h3>Filtros Aplicados</h3>
				<?php foreach ($filtros_aplicados as $filtro_a){ ?>
					<span><span><strong><?php echo $filtro_a['filtro']; ?></strong>: <?php echo $filtro_a["opcion"];?></span> <a href="
					<?php
					$var = str_replace("|" . $filtro_a["id_filtro"]."-".$filtro_a["id_opcion"], "", urldecode($request_uri_base));
					if(count($filtros_aplicados) == 1){
						$var = rtrim($var, '_');
						$var = rtrim($var, '-');	
					} 
					echo $var;
					if(isset($param_get)&&$param_get){ echo "?".$param_get; } ?>"><i class="fa fa-close"></i></a></span>
				<?php } ?>
			</div> <!-- #filtros_aplicados -->
		<?php }
		if(isset($filtros_pendientes)&&$filtros_pendientes){
			foreach ($filtros_pendientes as $filtro_p){ ?>
			<ul>
				<li><h3><?php echo $filtro_p["filtro"];?></h3><li>
				<?php if(isset($filtro_p["opciones"])&&$filtro_p["opciones"]){
					foreach ($filtro_p["opciones"] as $opc){ ?>
						<li><a href="<?=$url_filtros;?>|<?php echo $filtro_p["id"]; ?>-<?php echo $opc["id"]; ?>"><?php echo $opc["opcion"]; ?> <span>(<?php echo $opc["total"]; ?>)</span></a></li>
					<?php }
				} ?>
			</ul>
			<?php }
		} ?>
	</div><!-- #filtros_wrapper -->
	<?php if(isset($banners)&&$banners){ ?>
		<div class="banners">
			<?php foreach ($banners as $banner){
				if($banner){ ?>
					<div class="hidden"><?php echo $banner; ?></div>
				<?php } 
			} ?>
		</div>
	<?php } ?>
<?php } ?>