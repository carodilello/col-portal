<div id="box_vencido" class="box">
	<h2><i class="fa fa-warning"></i>Lo sentimos, esta publicación ya no se encuentra disponible.</h2>
	<a class="btn btn-default btn-green" href="<?php echo base_url('/proveedores/listado_empresas/'.$proveedor["id_rubro"]); ?>" title="Ver empresas similares">Ver empresas similares</a>
</div>