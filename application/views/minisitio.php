<?php
$bodyclass = 'page minisitio';
include('header.php');

$scripts_javascript = array(
	'<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAbSuwVNqfo1GeWu1Q7r-MHAUs9gb88O_w"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/markerclusterer.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/plugins/audio/audio.min.js') . '"></script>',
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_audio.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_video.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		if($prov_vencido) include('mod_publicacion_no_disponible.php');
		include('mod_header_minisitio.php'); // Aqui estan los popups de acciones  ?>
		<section id="wrapper_minisitio" class="row">
			<div id="content_minisitio" class="col-md-8">
				<div id="resumen_proveedor" class="box">
					<h2>Información de <?php echo $proveedor['proveedor']; ?></h2>
					<div class="box_inner">						
						<?php
						if(!empty($faqs) && (isset($proveedor['faqs']) && $proveedor['faqs'] > 0)){ ?>
							<table id="tabla_servicios_proveedor">
								<?php 
								$minisitio_home = TRUE;
								include('minisitio_preguntas_frecuentes_tabla.php'); ?>
							</table><!-- #tabla_servicios_proveedor -->
						<?php }
						if($fotos){ ?>
							<div class="galeria_standard">
								<div class="big_pic carousel slide" id="carousel-minisitio" data-ride="carousel" data-pause="hover" data-interval="false">
									<input type="hidden" value="5" class="cant_thumbs" />
									<div class="carousel-inner" role="listbox">
								    	<?php foreach ($fotos as $k => $foto){
								    	 	if(isset($foto['item']) && $foto['item']){ ?>
								    	 		<div class="align_c item video <?=$k==0?'active':'';?>">
								    	 			<iframe src="http://www.youtube.com/embed/<?php echo $foto['item'];?>?wmode=opaque&amp;rel=0&amp;enablejsapi=1" class="iframe_styled ytplayer" allowfullscreen="" id="player<?php echo $k; ?>"></iframe>
								    	 		</div>
								    	 	<?php }else{ ?>
								    	 		<a href="http://media.casamientosonline.com/images/<?php echo $foto['imagen_gde']; ?>" class="align_c item item_minis <?=$k==0?'active':'';?>" data-gallery title='<?php echo $foto['titulo']; ?>'
													data-urlminisitio="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto['rubro_seo']; ?>"
													data-url="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto["rubro_seo"] . '/presupuesto-foto_CO_r'.$rubro["id"].'_fo'.$foto['id']; ?>">
											      		<?php if($foto['titulo']){ ?>
											      			<img alt="<?php echo $foto['titulo']; ?> | Casamientos Online" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" src="<?php echo "http://media.casamientosonline.com/images/" . str_replace(' ', '%20', $foto['imagen_gde']); ?>" />
											      		<?php }else{ ?>
											      			<img alt="Imagen de <?php echo $proveedor['proveedor']; ?> sobre <?php echo $proveedor['rubro']; ?> | Casamientos Online" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" src="<?php echo "http://media.casamientosonline.com/images/" . str_replace(' ', '%20', $foto['imagen_gde']); ?>" />
											      		<?php } ?>
											    	</a>
								    	 	<?php } ?>
										<?php } ?>
								  	</div>
								  	
									<!-- Controls -->
									<a class="left carousel-control" href="#carousel-minisitio" role="button" data-slide="prev">
										<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
										<span class="sr-only valign">Previous</span>
									</a>
									<a class="right carousel-control" href="#carousel-minisitio" role="button" data-slide="next">
										<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div><!-- .big_pic -->
								
								<div class="thumbs carousel slide" id="carousel-thumbs" data-ride="carousel" data-interval="false">
									<div class="carousel-inner" role="listbox">
										<?php
										foreach ($fotos_parseado as $key => $fotos_grupo){ ?>
											<div class="item item-carousel-thumb <?=$key==0?'active':'';?>">
												<?php foreach ($fotos_grupo as $k => $foto){ ?>
													<a class="thumb_c <?php echo isset($foto['imagen'])&&$foto['imagen']?'':'thumb_video'; ?>" href="javascript:;"><img data-target="#carousel-minisitio" data-slide-to="<?php echo $k; ?>" class="col-md-2" src="<?php echo isset($foto['imagen'])&&$foto['imagen']?('http://media.casamientosonline.com/images/'.$foto['imagen']):($foto['thumb']?$foto['thumb']:('http://img.youtube.com/vi/'.$foto['item'].'/0.jpg')); ?>" alt="thumb" /></a>
												<?php } ?>
											</div>
										<?php } ?>
									</div>
									<!-- Controls -->
									<a class="left carousel-control carousel-control-thumb-left" href="#carousel-thumbs" role="button" data-slide="prev">
										<span class="fa fa-chevron-left fa-thumb-left valign" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control carousel-control-thumb-right" href="#carousel-thumbs" role="button" data-slide="next">
										<span class="fa fa-chevron-right fa-thumb-right valign" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div><!-- .thumbs -->
								
							</div><!-- #galeria_standard -->
						<?php } ?>
						
						<div id="descripcion_proveedor" style="height: 100%; overflow: hidden;">
							<?php echo stripslashes($proveedor['contenido']); ?>
						</div><!-- #descripcion_proveedor -->

						
					</div><!-- .box_inner -->
					<div id="box_ver_mas" style="display:none;">
						<div class="relative">
							<div id="gradient"></div>
							<a class="seguir_leyendo" href="javascript:;"><span class="seguir_leyendo_texto">Ver más</span> <i class="seguir_leyendo_icon fa fa-chevron-down"></i></a>
						</div><!-- .relative --> 
					</div><!-- #box_ver_mas --> 
				</div><!-- #resumen_proveedor -->
				
				
				
				<?php if($items){ ?>
					<div id="pp_minisitio" class="box">
						<h2>Productos y Promociones<div class="actions pull-right" <?php echo !empty($hash) ? 'title="No se puede acceder debido a que es una vista previa" data-toggle="tooltip" data-placement="bottom"' : ''; ?>><a href="<?php echo !empty($hash) ? 'javascript:;' : str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/productos-y-promos'; ?>">Ver todos los Productos y Promos</a></div></h2>
						<div class="box_inner">
							<?php 
							$puntos_suspensivos = "";
							$puntos_suspensivos_aclaracion = "";
							$fecha_vencimiento = "";
							foreach ($items as $k => $item){
								if(strlen(strip_tags($item["contenido"])) >= 160) $puntos_suspensivos = "...";
								if(isset($item["fecha_vencimiento"])&&$item["fecha_vencimiento"]){
									$fv_tmp = explode("-",$item["fecha_vencimiento"]);
									$fecha_vencimiento = $fv_tmp[2]."/".$fv_tmp[1]."/".$fv_tmp[0];
								}
								if(isset($item["aclaracion"]) && strlen(strip_tags($item["aclaracion"])) >= 40) $puntos_suspensivos_aclaracion = "...";
								if($item['tipo']) switch ($item['tipo']){
									case 'productos':
										$url_detalle = 'pd';
										$id_item = $item["id_producto"];
										$tipo_url = 'producto';
										$nombre_el = 'Producto';
										$icon = 'fa-star';
										$codigo_tipo = '_pd';
										break;
									case 'promociones':
										$url_detalle = 'pr';
										$id_item = $item["id_producto"];
										$tipo_url = 'promocion';
										$nombre_el = 'Promoción';
										$icon = 'fa-tag';
										$codigo_tipo = '_pr';
										break;
									case 'paquetes':
										$url_detalle = 'pa';
										$id_item = $item["id_paquete"];
										$tipo_url = 'paquete';
										$nombre_el = 'Paquete';
										$icon = 'fa-gift';
										$codigo_tipo = '_pr';
										break;
								} ?>
								<div class="box_listado no_padding">
									<div class="precio">
										<?php if($item["precio"] != '0.00' && $item["precio"] != '$ 0,00' && $item["precio"] != 'U$S 0,00'){
												if(isset($item["precio_tipo"])&&$item["precio_tipo"]){ ?>
													<strong><?=$item['tipo']!='paquetes'?'$':'';?><?php echo $item["precio"]; ?></strong>
													<strong><?php echo $item["precio_tipo"]; ?></strong>
												<?php }else{ ?>
													<strong>
														<?php if($item['tipo'] != 'paquetes'){
																if($item["moneda"] == 'P') echo '$'; 
																if($item["moneda"] == 'D') echo 'U$S';
																if($item["moneda"] == 'E') echo '&euro;';
															}
															echo number_format($item["precio"], 2, ",", "."); ?>
													</strong>
												<?php }
											}
										if(isset($item["porcentaje_descuento"])&&$item["porcentaje_descuento"] && (!isset($item["precio"]) || !$item["precio"] || $item["precio"] == '0.00')){ ?>
											<span><span><?php echo $item["porcentaje_descuento"]; ?></span>%OFF</span>
										<?php } ?>
									</div>
									<div class="img_wrapper">
										<a href="<?php echo !empty($hash) ? 'javascript:;' : str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $item['seo_rubro'] . '/' . $item["seo_url"] . '_CO_' . $url_detalle . $item["id_producto"] . '#pp_minisitio'; ?>">
											<?php
											if(in_array(get_headers('http://media.casamientosonline.com/images/'.$item["imagen"])[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
												<img onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/images/'.$item["imagen"].'"';?> alt="<?php echo $item["titulo"]; ?>" />
											<?php }else{ ?>
												<img onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/logos/'.$item["logo"].'"';?> alt="<?php echo $item["titulo"]; ?>" />
											<?php } ?>
										</a>
									</div>
									<div class="info_wrapper">
										<h3><a href="<?php echo !empty($hash) ? 'javascript:;' : str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $item['seo_rubro'] . '/' . $item["seo_url"] . '_CO_' . $url_detalle . $item["id_producto"] . '#pp_minisitio'; ?>"><?php echo $item["titulo"]; ?></a></h3>
										<p class="proveedor">
										<span class="tipo_de_producto"><i class="fa <?php echo $icon; ?>"></i><?php echo $nombre_el; ?></span>
										<?php if(isset($item["porcentaje_descuento"])&&$item["porcentaje_descuento"] && (!isset($item["precio"]) || !$item["precio"] || $item["precio"] == '0.00')){ ?>
											<span><i class="fa fa-percent"></i>Descuento</span>
										<?php }
										if(isset($item["regalo"]) && $item["regalo"]){ ?>
											<span><i class="fa fa-gift"></i>Regalo</a></span>
										<?php }
										if(isset($item["oferta"]) && $item["oferta"]){ ?>
											<span><i class="fa fa-tag"></i>Oferta</a></span>
										<?php } ?>
										</p>
										
										<?php if(isset($item["subtitulo"])&&$item["subtitulo"]){ ?><p class="subtitulo"><?php echo $item["subtitulo"]; ?></p><?php } ?>
										
										<p class="descripcion"><?php echo substr(strip_tags($item["contenido"]),0,160).$puntos_suspensivos; ?></p>
										
										<?php if($fecha_vencimiento){ ?>
											<p class="fecha"><i class="fa fa-calendar"></i>Valido hasta: <strong><?php echo $fecha_vencimiento; ?></strong></p>
										<?php } ?>
										
										<?php if(($item["precio"] == '0.00' || $item["precio"] == '$ 0,00' || $item["precio"] == 'U$S 0,00') && !$item["aclaracion"]){ ?>
											<?php if($item["precio_viejo"]){ ?>
												<p class="data_adicional"><?php echo $item["precio_viejo"]; ?></p>
											<?php } ?>
										<?php } ?>

										<div class="bottom_actions">
											<?php if(isset($item["aclaracion"])&&$item["aclaracion"]){ ?>
												<p class="data_adicional"><?php echo substr(strip_tags($item["aclaracion"]),0,40).$puntos_suspensivos_aclaracion; ?></p>
											<?php }
											if($item['id_minisitio'] && $item['id_rubro']){ ?>
												<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $item['seo_rubro'] . '/presupuesto-' . $tipo_url . '_CO_r' . $item['id_rubro'] . $codigo_tipo . $id_item; ?>" class="btn btn-default">Pedir presupuesto</a>
											<?php } ?>
										</div><!-- .bottom_actions -->
									</div>
								</div><!-- .box_listado -->	
							<?php 
							$fecha_vencimiento = '';
							} ?>
						</div><!-- .box_inner -->	
					</div><!-- #pp_minisitio -->	
				<?php } ?>
				
				<?php if($audios){ 
					$puntos_suspensivos = '';
					$estilo_audio = '';
					$cant = count($audios); ?>
					<div id="audios_minisitio" class="box">
						<h2>Audios</h2>
						<div class="box_inner">
							<ul>
								<?php foreach ($audios as $k => $audio){
									if(strlen(strip_tags($audio["titulo"])) >= 20) $puntos_suspensivos = '...'; ?>
										<li class="col-md-4 <?=(($cant-$k)<4)?'no_border':'';?>"><audio class="audio-player" src="http://media.casamientosonline.com/audios/<?php echo $audio['item']; ?>" preload="none"></audio><span class="span-audioplayer"><?php echo substr(strip_tags($audio['titulo']),0,20).$puntos_suspensivos; ?></span></li>
								<?php 
									$puntos_suspensivos = '';
								} ?>
							</ul>
						</div><!-- .box_inner -->
					</div><!-- #audios_minisitio -->
				<?php } ?>
			</div><!-- #content_minisitio -->	
			
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php				
				if(!$prov_vencido) include('minisitio_acciones.php');
				
				include('minisitio_formulario.php');
				
				if($comentarios && !$prov_vencido){ ?>
					<div id="testimonios_minisitio" class="box">
						<h2>Testimonios
							<?php if($puntajes['puntajes_total']){ ?>
								<div class="rating">
									<p>Rating Total</p>
									<?php 
									for ($i=0; $i < (int) $puntajes['puntajes_total']; $i++){  ?>
										<i class="fa fa-star"></i>
									<?php }
									if(!is_int($puntajes['puntajes_total'])){ ?>
										<i class="fa fa-star-half"></i>
									<?php } ?>
									
								</div>
							<?php } ?>
						</h2>
						<div class="box_inner">
							<?php 
							$puntos_suspensivos = "";
							foreach ($comentarios as $k => $comentario){
								$com = explode('#',$comentario);
								$body = $com[1];
								$nombre = $com[2];
								
								$nom = mb_strtoupper(mb_substr(trim($nombre), 0,1));

								if(strlen($nom) == 3){
									$nom = mb_strtoupper(mb_substr($nombre, 1,1));
								}

								$bg_color = $com[13];
								if(strlen(strip_tags($body)) >= 80) $puntos_suspensivos = "..."; ?>
								<div class="testimonio">
									<div class="img col-md-3">
										<div style="font-weight: bold; font-size: 36px; text-align: center; background: #<?php echo $bg_color; ?>; color: #FFF; border-radius: 48%; height: 60px; width: 60px; vertical-align: middle; padding-top: 5px;"><?php echo $nom; ?></div>
									</div>
									<div class="col-md-9 info">
										<h4><?php echo $nombre; ?></h4>
										<?php if(isset($puntajes['puntajes'][$k]) && $puntajes['puntajes'][$k]){ ?>
											<div class="rating">
												<?php
												for ($i=0; $i < (int) $puntajes['puntajes'][$k]; $i++){  ?>
													<i class="fa fa-star"></i>
												<?php }
												if(!is_int($puntajes['puntajes'][$k])){ ?>
													<i class="fa fa-star-half"></i>
												<?php } ?>
											</div>
										<?php } ?>
										<p title="<?php echo $body; ?>"><?php echo substr(strip_tags($body),0,80).$puntos_suspensivos; ?></p>
									</div>
								</div><!-- .testimonio  -->	
							<?php } ?>
						</div><!-- .box_inner -->
						<a class="ver_todos" href="<?php if($proveedor['comentarios']<1){echo 'javascript:;';}else{echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/testimonios';} ?>"><i class="fa fa-comment"></i>Leer todos los testimonios</a>
					</div><!-- #testimonios_minisitio -->
				<?php } ?>
				
				<?php if($asociados_trabaja && !$prov_vencido){ ?>
					<div id="empresas_asociadas_minisitio" class="box">
						<h2>Empresas Asociadas</h2>
						<div class="box_inner">
							<?php 
							$icon_ea = '';
							foreach ($asociados_trabaja as $k => $asociado){
								if(isset($ic[$asociado[key($asociado)]['id_rubro']])) $icon_ea = $ic[$asociado[key($asociado)]['id_rubro']]; ?>
								<div class="rubro">
									<h3><span class="icon <?=$icon_ea?('ic_'.$icon_ea):'ic_catering';?>"></span><?php echo $k; ?></h3>
									<ul>
										<?php foreach ($asociado as $val){
											if($val['id_minisitio']){ ?>
												<li><a href="<?php echo str_replace('{?}', $val['subdominio'], $base_url_subdomain) . $val['url_seo']; ?>"><?php echo $val['proveedor']; ?></a></li>
											<?php }else{ ?>
												<li><?php echo $val['proveedor']; ?></li>
											<?php }
										} ?>
									</ul>
								</div><!-- .rubro -->	
							<?php } ?>
						</div>
					</div><!-- #empresas_asociadas_minisitio -->
				<?php } ?>
			</aside> <!-- #sidebar_minisitio -->
			
			<div class="col-md-12">
				<?php if(isset($proveedor['latitud']) && $proveedor['latitud'] && isset($proveedor['longitud']) && $proveedor['longitud']){ ?>
					<div id="mapa_minisitio" class="box clear">
						<span style="display:none;" id="id_minisitio"><?php echo $proveedor['id_minisitio']; ?></span>
						<span style="display:none;" id="minisitio_title"><?php echo $proveedor['titulo']; ?></span>
						<h2>Ubicación <div class="actions"><span>Cambiar vista: </span>
							<a href="javascript:;" class="vista-roadmap"><i class="fa fa-map-marker"></i></a>
							<a href="javascript:;" class="vista-street_view"><i class="fa fa-male"></i></a>
						</div></h2>
						<div class="box_inner">
							<div class="roadmap" id="content_listado_mapa_map" data-gmap data-latitude='<?php echo $proveedor['latitud']; ?>' data-longitude='<?php echo $proveedor['longitud']; ?>'></div>
							<div style="display:none;" class="street_view" data-gmap data-latitude='<?php echo $proveedor['latitud']; ?>' data-longitude='<?php echo $proveedor['longitud']; ?>'></div>
						</div>
					</div><!-- #mapa_minisitio -->
				<?php } ?>		
				<?php if(!$prov_vencido) include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->
		</section><!-- #wrapper_minisitio -->
	</div><!-- .container -->
	<?php
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div><!-- .bg_grey -->
<?php 
include('popup_fotos.php');
include('footer.php'); ?>