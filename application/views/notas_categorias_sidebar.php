<div id="categorias_notas_sidebar">
	<a href="<?php echo base_url('/consejos/vestidos_CO_cn76'); ?>" class="box">
		<img src="<?php echo base_url('/assets/images/icon_seccion_notas_vestidos.png'); ?>" />
		<h4>Vestidos</h4>
		<p>Mirá los últimos modelos</p>
		<i class="fa fa-angle-right"></i>
	</a>
	
	<a href="<?php echo base_url('/consejos/belleza-y-estilo_CO_cn81'); ?>" class="box">
		<img src="<?php echo base_url('/assets/images/icon_seccion_notas_belleza_estilo.png'); ?>" />
		<h4>Belleza y Estilo</h4>
		<p>Los mejores tips</p>
		<i class="fa fa-angle-right"></i>
	</a>

	<a href="<?php echo base_url('/consejos/la-organizacion_CO_cn93'); ?>" class="box">
		<img src="<?php echo base_url('/assets/images/icon_seccion_notas_la_organizacion.png'); ?>" />
		<h4>La Organización</h4>
		<p>Organizá tu Casamiento</p>
		<i class="fa fa-angle-right"></i>
	</a>
	
	<a href="<?php echo base_url('/consejos/luna-de-miel_CO_cn98'); ?>" class="box">
		<img src="<?php echo base_url('/assets/images/icon_seccion_notas_luna_de_miel.png'); ?>" />
		<h4>Luna de Miel</h4>
		<p>Las mejores opciones</p>
		<i class="fa fa-angle-right"></i>
	</a>
	
	<a href="<?php echo base_url('/consejos/casamientos-reales_CO_cn1055'); ?>" class="box">
		<img src="<?php echo base_url('/assets/images/icon_seccion_notas_casamientos_reales.png'); ?>" />
		<h4>Casamientos Reales</h4>
		<p>Conocé otros casamientos</p>
		<i class="fa fa-angle-right"></i>
	</a>
</div><!-- #categorias_notas_sidebar -->