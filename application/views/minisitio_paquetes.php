<?php
$bodyclass = 'page minisitio';
include('header.php');

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		include('mod_header_minisitio.php'); ?>
		
		<section id="wrapper_minisitio" class="row">
		
			<div id="content_minisitio" class="col-md-8">

				<?php if($paquetes){ ?>
					<div id="paquetes_minisitio" class="box listado_paquetes">
						<h2>Paquetes de <?php echo $proveedor['proveedor']; ?></h2>
						<div class="box_inner">
							<?php 
								$puntos_suspensivos = "";
								foreach ($paquetes as $k => $paquete){ 
									if(strlen(strip_tags($paquete["contenido"])) >= 160) $puntos_suspensivos = "..."; ?>
									<div class="box_listado no_padding">
										<div class="precio">
											<strong><?php echo $paquete["precio"]; ?></strong><span><?php echo $paquete["precio_tipo"]; ?></span>
										</div>
										<div class="img_wrapper">
											<a href='<?php echo str_replace('{?}', $paquete['subdominio'], $base_url_subdomain) . $paquete['seo_rubro'] . '/' . $paquete["seo_url"] . '_CO_pa' . $paquete["id_producto"] . '#pp_minisitio'; ?>'>
												<?php if(in_array(get_headers('http://media.casamientosonline.com/images/'.$paquete["imagen"])[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
													<img <?php echo 'src="http://media.casamientosonline.com/images/'.str_replace("_chica", "_grande", $paquete["imagen"]).'"';?> alt="<?php echo $paquete["titulo"]; ?>" />
												<?php }else{ ?>
													<img <?php echo 'src="http://media.casamientosonline.com/logos/'.$paquete["logo"].'"';?> alt="<?php echo $paquete["titulo"]; ?>" />
												<?php } ?>
											</a>
										</div>
										<div class="info_wrapper">
											<h3><a href='<?php echo str_replace('{?}', $paquete['subdominio'], $base_url_subdomain) . $paquete['seo_rubro'] . '/' . $paquete["seo_url"] . '_CO_pa' . $paquete["id_producto"] . '#pp_minisitio'; ?>'><?php echo $paquete["titulo"]; ?></a></h3>
											<p class="organiza">Organiza: <a href="<?php echo str_replace('{?}', $paquete['subdominio'], $base_url_subdomain) . $paquete['seo_rubro']; ?>"><?php echo $paquete["proveedor"];?></a></p>
											<div class="caract_paquete">
												<?php if($paquete["invitados"]){ ?>
													<span><i class="fa fa-user"></i>Base <?php echo $paquete["invitados"]; ?> inv.</span>
												<?php } ?>
												<!--<span><i class="fa fa-calendar"></i>Viernes.</span>-->
												<?php if($paquete["zonas"]){ ?>
													<span><i class="fa fa-map-marker"></i><?php echo $paquete["zonas"]; ?></span>
												<?php } ?>
												<!--<span><i class="fa fa-clock-o"></i>Noche</span>-->
											</div><!-- .caract_paquete -->
											
											<div class="bottom_actions">
												<?php if($paquete['rubros']){
													$paquetes_rubros = explode('@',$paquete['rubros']); ?>
													<div class="inc_paquete">
														<strong>Rubros incluidos:</strong>
														<?php foreach ($paquetes_rubros as $rub) {
															$elem_rub = explode('*',$rub); ?>
															<span data-toggle="tooltip" data-placement="top" class="icon ic_<?php echo $ic[$elem_rub[0]]; ?>" title="<?php echo $elem_rub[1]; ?>"></span>
														<?php } ?>
													</div><!-- .inc_minisitio -->
												<?php }
												if($paquete['id_minisitio'] && $paquete['id_rubro']){ ?>
													<a href="<?php echo str_replace('{?}', $paquete['subdominio'], $base_url_subdomain) . $rubro['url'] . '/presupuesto-paquete_CO_r' . $rubro["id"] . '_pa' . $paquete['id_producto']; ?>" class="btn btn-default">Consultar</a>
												<?php } ?>
											</div><!-- .bottom_actions -->
										</div>
									</div><!-- .box_listado -->
								<?php 
								$puntos_suspensivos = "";
								} ?>
						</div><!-- .box_inner -->	
					</div><!-- #pp_minisitio -->
				<?php } ?>

			</div><!-- #content_minisitio -->	
			
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php include('minisitio_acciones.php'); ?>
			
				<?php include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->
			
			<div class="col-md-12">
				<?php include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->
		</section><!-- #wrapper_minisitio -->
	</div><!-- .container -->
	<?php
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php include('footer.php'); ?>