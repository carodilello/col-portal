<?php
$bodyclass = 'page listado';
include('header.php'); ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><?=$tipo_listado=='productos'?'Todos los productos':($tipo_listado=='promociones'?'Todas las promociones':'Todos los paquetes');?></li>
	</ul>
	<div id="listado_header" class="row">
		<h1 class="col-md-9 h_sep">
				<?=$tipo_listado=='productos'?'Todos los productos':($tipo_listado=='promociones'?'Todas las promociones':'Todos los paquetes');?> <?=$rubro?('de '.$rubro['rubro']):($grupo['nombre']?'de '.ucfirst(strtolower($grupo['nombre'])):''); ?> en <span class="dropdown"><a class="green suc_selector" id="sucursales-nav-label" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label">
					<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
						<?php
						/* cambio 27062017
							<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.$suc["nombre_seo"].(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>#listado_header"><?=$suc["sucursal"];?></a>
						*/
						$redic = base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.$suc["nombre_seo"].(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); 

						// lo primero que hace es controlar si $grupo['nombre'] es la fiesta, 
						// salones, vestidos, novias, novios, luna de miel
						if( ! $rubro['rubro'] and (
							$grupo['nombre'] == 'LA FIESTA' OR 
							$grupo['nombre'] == 'SALONES' OR 
							$grupo['nombre'] == 'VESTIDOS' OR 
							$grupo['nombre'] == 'NOVIAS' OR 
							$grupo['nombre'] == 'NOVIOS' OR 
							$grupo['nombre'] == 'LUNA DE MIEL' )){
							//BUSCA EN new_nav EL GRUPO SEGUN LA SUCURSAL
							$idtipo = $this->rubros_model->get_grupoid($grupo['nombre'],$suc["id"],$tipo_listado);								

							if(isset($idtipo[0])&&$idtipo[0]) $idtipo = $idtipo[0];	
							$grupolower = strtolower($grupo['nombre']);
							if($idtipo){
								$redic = base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . $suc["nombre_seo"] . '/'.$tipo_listado.'/'.str_replace(' ','-',$grupolower).'_CO_a'.$idtipo["id_ref"]);
								}

						}else{
							//si esta en un rubro, busca el id de ese rubro por el nombre y para la sucursal y con ese dato arma el redirect
							$idRubro = $this->rubros_model->get_pyp_rubroid($rubro["rubro"],$suc["id"],$tipo_listado);
							if(isset($idRubro[0])&&$idRubro[0]) $idRubro = $idRubro[0];	

							if($idRubro){
								switch ($tipo_listado) {
									case 'promociones':
										$codigo = 'pr';
										break;
									case 'productos':
										$codigo = 'pd';
										break;
									case 'paquetes':
										$codigo = 'pa';
										break;
								}
								$redic = base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . $suc["nombre_seo"] . '/'.$tipo_listado.'/'.str_replace(' ','-',$rubro["rubro"]).'_CO_'.$codigo.'/1-r'.$idRubro["id"]);				
							}
						}?>
						<a class="block" href="<?php echo $redic; ?>"><?=$suc["sucursal"];?></a>
					<?php } ?>
				</ul>
			</span>
		</h1>
		<div class="col-md-12">
			<?php if(isset($banners[306]) && $banners[306] || $mostrar_banners){ ?>
				<div class="banner banner_970_90"><?php echo isset($banners[306]) && $banners[306] ? $banners[306] : '<p>Banner PyP todos los rubros Top 970x90</p>' ; ?></div>
			<?php } ?>
		</div>
		
		<div id="filtros_mobile_wrapper" class="mobile_block col-md-12">
			<h4>Refinar busqueda</h4>
			<a href="javascript:;" onClick="$('#filtros_wrapper').toggle();" id="filtros_listado_mobile" class="mobile_block"><i class="fa fa-filter"></i>Filtrar</a>
		</div>
	</div> <!-- #listado_header -->
	<div id="listado_wrapper">
		<aside>
			<?php if(isset($rubro['rubro']) || $filtros_rubros){ ?>
				<?php if(isset($rubro['rubro'])){ ?>
					<div id="filtros_aplicados_rubro">
						<span><?php echo $tipo_listado=='paquetes'?'Paquetes que incluyan':'Rubro'; ?>: <?php echo $rubro['rubro']; ?><a href="<?php echo base_url(str_replace('/_?_', '', $url_base)); ?>"><i class="fa fa-close"></i></a></span>
					</div>
				<?php }
				if($filtros_rubros){ ?>
					<div id="filtros_wrapper">
						<h3><?php echo $tipo_listado=='paquetes'?'Paquetes que incluyan':'Rubro'; ?></h3>
						<ul>
							<?php foreach ($filtros_rubros as $k => $f_rubro){ ?>
								<li><a href="<?php echo base_url(str_replace('_?_', $f_rubro['rubro_seo'], $url_base) . '/1' . '-r' . $f_rubro['id_rubro']); ?>"><?php echo $f_rubro['rubro']; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
			<?php }
			include('listado_filtros.php');
			if(isset($banners[307]) && $banners[307] || $mostrar_banners){ ?>
				<div class="banner banner_250_250"><?php echo isset($banners[307]) && $banners[307] ? $banners[307] : '<p>Banner PyP todos los rubros 250x250 Posicion 1</p>' ; ?></div>
			<?php }
			if(isset($banners[308]) && $banners[308] || $mostrar_banners){ ?>
				<div class="banner banner_250_250"><?php echo isset($banners[308]) && $banners[308] ? $banners[308] : '<p>Banner PyP todos los rubros 250x250 Posicion 2</p>' ; ?></div>
			<?php }
			if(isset($banners[309]) && $banners[309] || $mostrar_banners){ ?>
				<div class="banner banner_250_250"><?php echo isset($banners[309]) && $banners[309] ? $banners[309] : '<p>Banner PyP todos los rubros 250x250 Posicion 3</p>' ; ?></div>
			<?php } ?>
		</aside>
		<section id="content_listado" class="<?=$tipo_listado=='paquetes'?'listado_paquetes':''; ?>">
			<?php 
			$puntos_suspensivos = "";
			$puntos_suspensivos_aclaracion = "";
			if(isset($productos)&&$productos) foreach ($productos as $k => $producto){

				switch ($producto['tipo']) {
					case 'productos':
						$tipo_pedido_presupuesto = 'producto';
						$tipo_presupuesto = 'productos';
						$codigo_tipo = '_pd';
						$codigo = '_CO' . $codigo_tipo;
						break;
					case 'promociones':
						$tipo_pedido_presupuesto = 'promocion';
						$tipo_presupuesto = 'promociones';
						$codigo_tipo = '_pr';
						$codigo = '_CO' . $codigo_tipo;
						break;
					case 'paquetes':
						$tipo_pedido_presupuesto = 'paquete';
						$tipo_presupuesto = 'paquetes';
						$codigo_tipo = '_pa';
						$codigo = '_CO' . $codigo_tipo;
						break;
				}

				if(strlen(strip_tags($producto["contenido"])) >= 160) $puntos_suspensivos = "...";
				if(isset($producto["fecha_vencimiento"])) $fv_tmp = explode("-",$producto["fecha_vencimiento"]);
				if(isset($fv_tmp[2])) $fecha_vencimiento = $fv_tmp[2]."/".$fv_tmp[1]."/".$fv_tmp[0]; ?>
				<div class="box_listado no_padding">
					<div class="precio">
						<?php if(!isset($producto['id_paquete'])){
							if($producto["precio"] != '0.00' && $producto["precio"] != '$ 0,00' && $producto["precio"] != 'U$S 0,00'){
								if(isset($producto["precio_tipo"])&&$producto["precio_tipo"]){ ?>
									<strong>$<?php echo $producto["precio"]; ?></strong>
									<strong><?php echo $producto["precio_tipo"]; ?></strong>
								<?php }else{ ?>
									<strong>
										<?php
											if($producto["moneda"] == 'P') echo '$';
											if($producto["moneda"] == 'D') echo 'U$S';
											if($producto["moneda"] == 'E') echo "&euro;"; 
											echo number_format($producto["precio"], 2, ",", "."); ?>
									</strong>
								<?php }
							}
							if(isset($producto["porcentaje_descuento"])&&$producto["porcentaje_descuento"] && (!$producto["precio"] || $producto["precio"] == '0.00')){ ?>
								<span><span><?php echo $producto["porcentaje_descuento"]; ?></span>%OFF</span>
							<?php }
						}else{ ?>
							<strong><?php echo $producto["precio"]; ?></strong><span><?php echo $producto["precio_tipo"]; ?></span>
						<?php } ?>
					</div>
					<div class="img_wrapper">
						<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . $codigo . $producto["id_producto"] . '#pp_minisitio'; ?>">
							<?php
							if(in_array(get_headers('http://media.casamientosonline.com/images/' . str_replace('_chica', '_grande', $producto["imagen"]))[0],array('HTTP/1.1 302 Found', 'HTTP/1.1 200 OK'))){ ?>
								<img onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/images/'.str_replace('_chica', '_grande', $producto["imagen"]).'"';?> alt="<?php echo $producto["titulo"]; ?>" />
							<?php }else{ ?>
								<img onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php echo 'src="http://media.casamientosonline.com/logos/'.$producto["logo"].'"';?> alt="<?php echo $producto["titulo"]; ?>" />
							<?php } ?>
						</a>
					</div>
					<div class="info_wrapper">
						<h2><a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/' . $producto["seo_url"] . $codigo . $producto["id_producto"] . '#pp_minisitio'; ?>"><?php echo $producto["titulo"]; ?></a></h2>
						<p class="proveedor">
							<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro']; ?>"><i class="fa fa-suitcase"></i><?php echo $producto["proveedor"]; ?></a>
							<?php if(isset($producto["porcentaje_descuento"])&&$producto["porcentaje_descuento"] && (!isset($producto["precio"]) || !$producto["precio"] || $producto["precio"] == '0.00')){ ?>
								<span><i class="fa fa-percent"></i>Descuento</span>
							<?php }
							if(isset($producto["regalo"]) && $producto["regalo"]){ ?>
								<span><i class="fa fa-gift"></i>Regalo</a></span>
							<?php }
							if(isset($producto["oferta"]) && $producto["oferta"]){ ?>
								<span><i class="fa fa-tag"></i>Oferta</a></span>
							<?php } ?>
						</p>
						
						<?php if($tipo_listado == 'paquetes'){ ?>
							<div class="caract_paquete">
								<?php if($producto["invitados"]){ ?>
									<span><i class="fa fa-user"></i>Base <?php echo $producto["invitados"]; ?> inv.</span>
								<?php } ?>
								<!--<span><i class="fa fa-calendar"></i>Viernes.</span>-->
								<?php if($producto["zonas"]){ ?>
									<span><i class="fa fa-map-marker"></i><?php echo $producto["zonas"]; ?></span>
								<?php } ?>
								<!--<span><i class="fa fa-clock-o"></i>Noche</span>-->
							</div><!-- .caract_paquete -->	
						<?php } ?>

						<h3 class="descripcion"><?php echo substr(strip_tags($producto["contenido"]),0,160).$puntos_suspensivos; ?></h3>
						
						<?php if(isset($fecha_vencimiento) && $fecha_vencimiento){ ?>
							<p class="fecha"><i class="fa fa-calendar"></i>Valido hasta: <strong><?php echo $fecha_vencimiento; ?></strong></p>
						<?php } ?>

						<?php if(($producto["precio"] == '0.00' || $producto["precio"] == '$ 0,00' || $producto["precio"] == 'U$S 0,00')){ ?>
							<?php if(isset($producto["precio_viejo"]) && $producto["precio_viejo"]){ ?>
								<p class="data_adicional"><?php echo $producto["precio_viejo"]; ?></p>
							<?php } ?>
						<?php } ?>
						
						<div class="bottom_actions">
							<?php if(isset($producto["aclaracion"])){ ?>
								<p class="data_adicional"><?php if(isset($producto["aclaracion"]) && (isset($producto["precio_viejo"]))){ ?>&nbsp;- <?php } ?> <?php echo substr(strip_tags($producto["aclaracion"]),0,40).$puntos_suspensivos_aclaracion; ?></p>
							<?php } ?>
							<?php if(isset($producto['rubros']) && $producto['rubros']){
								$paquetes_rubros = explode('@',$producto['rubros']); ?>
								<div class="inc_paquete">
									<strong>Rubros incluidos:</strong>
									<?php foreach ($paquetes_rubros as $rub) {
										$elem_rub = explode('*',$rub); ?>
										<span data-toggle="tooltip" data-placement="top" title="<?php echo $elem_rub[1]; ?>" class="icon ic_<?php echo $ic[$elem_rub[0]]; ?>"></span>
								<?php } ?>
								</div><!-- .inc_minisitio -->
							<?php } ?>
							<a href="<?php echo str_replace('{?}', $producto['subdominio'], $base_url_subdomain) . $producto['seo_rubro'] . '/presupuesto-' . $tipo_presupuesto . '_CO_r'.$producto['id_rubro'] . $codigo_tipo . $producto["id_producto"]; ?>" class="btn btn-default">Pedir Presupuesto</a>
						</div><!-- .bottom_actions -->
					</div>
				</div><!-- .box_listado -->
			<?php } 
			include('paginacion.php'); ?>
		</section><!-- #content_listado -->
	</div><!-- #listado_wrapper -->
</div><!-- .container -->
<?php include('footer.php'); ?>