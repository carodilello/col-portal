<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- A PARTIR DE ACA PODES TOCAR LO QUE QUIERAS MANTENIENDO EN LO POSIBLE LAS CLASES EXISTENTES -->
    <div class="modal fade">
        <div id="popup_foto_wrapper" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body next">
                    <?php if(!isset($no_presupuesto_foto) || isset($flechas)){ ?>
    	                <button type="button" class="btn pull-left prev valign"><i class="fa fa-angle-left"></i></button>
                        <button type="button" class="btn next valign"><i class="fa fa-angle-right"></i></button>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <div id="data_proveedor">
                        <h3 class="modal-title"></h3>
                        <?php if(!isset($no_presupuesto_foto)){ ?>
                            <?php if(isset($prov_vencido) && !$prov_vencido){ ?>
                                <a href="javascript:;" class="link_minisitio_popup">Ver minisitio</a>
                            <?php } ?>
                        <?php } ?>
                        <!--<p><span class="zona"><i class="fa fa-map-marker"></i>Zona Norte</span></p>-->
                    </div>
                    
                    <!--<a id="guardar" class="col-md-2 hvr-icon-wobble-vertical" href="javascript:;">Guardar</a>
                    
                    <div id="social">
                        <a class="compartir_facebook no_margin" href="javascript:;"><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a class="boton-pinterest" data-pin-do="buttonPin" href=""><i class="fa fa-pinterest no_margin"></i></a>
                    </div>-->
                    <?php if(!isset($no_presupuesto_foto)){ ?>
                        <?php if(isset($prov_vencido) && !$prov_vencido){ ?>
                            <a href="<?php echo base_url('/'); ?>" class="btn btn-default pedir-presupuesto">Pedir presupuesto</a>
                        <?php } ?>
                    <?php }elseif(!isset($no_presupuesto)){
                        if(!$item_vencido){ ?>
                            <a href="<?php echo base_url('/'); ?>" class="btn btn-default pedir-presupuesto">Consultar</a>
                        <?php }
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>