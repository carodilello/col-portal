<?php
$bodyclass = 'page ceremonias_detalle iglesias';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>

<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li>Ceremonias no religiosas</li>
	</ul>
	<h1 class="title_sep">Ceremonias no religiosas en Argentina</h1>
	<img class="big_pic border_no_reli" src="<?php echo base_url('/assets/images/big_pic_ceremonias_no_religiosas.jpg'); ?>" alt="Ceremonias no religiosas" />
	<div id="split">
		<section id="primary">
			<div id="filtro" class="relative">
				<h2>Datos útiles</h2>				
			</div>
			<div class="caba datos_utiles_cont">
				<div class="datos_utiles_wrapper no_margin">
					<div>
						<span class="icon ic_2 valign"></span>
						<h4>¿Qué son las ceremonias no religiosas?</h4>
						<p>Las Ceremonias No Religiosas son celebraciones para aquellas personas que no pueden o no quieren realizar una ceremonia religiosa.</p>
						<p>Algunos ejemplos de las personas que realizan una ceremonia no religiosa son las parejas que tienen distintos credos o las divorciadas que no pueden casarse por Iglesia dos veces.</p>
						<p>Existen las ceremonias laicas en las que no hay ningún elemento religioso, como las ceremonias de religiones mixtas. También se realizan ceremonias para los Aniversarios y Compromisos.</p>
					</div>
					
					<div>
						<span class="icon ic_10 valign"></span>
						<h4>¿Cómo se realizan?</h4>
						<p>Las Ceremonias No Religiosas son únicas y especiales. Es decir, al ser personales, la pareja es la que decide hasta el último detalle.</p>
						<p>Proporcionando las características propias de la pareja, familia y amigos. Se eligen los símbolos, los rituales, los detalles, la vestimenta. Es original porque cada ceremonia es diferente, cada pareja escribe su propio libreto.</p>
						
					</div>
					
					<div>
						<span class="icon ic_9 valign"></span>
						<h4>¿Qué requisitos necesitan?</h4>
						<p>Hay distintos tipos de ceremonias. El único requisito es querer celebrar la unión entre dos personas, un compromiso, el aniversario, entre otras.</p>
					</div>
										
				</div><!-- .datos_utiles_wrapper -->
			</div><!-- .caba.datos_utiles_cont -->
									
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php $no_religiosas = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->	
	</div><!-- #split -->
</div><!-- .container -->

<?php
include('footer_newsletter.php');
include('footer_info.php'); ?>