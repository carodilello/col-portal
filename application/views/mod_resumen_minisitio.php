<div id="resumen_minisitio" class="box">
	<div class="col col-md-5 direccion">
		<?php 
		$str_mas = "";
		if(mb_strlen($proveedor['proveedor']) >= 40) $str_mas = "..."; ?>
		<h4><?php echo mb_substr($proveedor['proveedor'], 0, 40).$str_mas; ?></h4>
		<?php $puntos_suspensivos = "";
		$dir = (!empty($proveedor['direccion'])) ? str_replace('Ciudad Autonoma de Buenos Aires', 'CABA', $proveedor['direccion']) : null;
		$dir = str_replace('Ciudad Autónoma de Buenos Aires', 'CABA', $dir);
		if(mb_strlen($dir) >= 52) $puntos_suspensivos = "..."; ?>
		<p data-toggle="tooltip" title="<?php echo (!empty($proveedor['direccion'])) ? $proveedor['direccion'] : null; ?>"><?php echo mb_substr($dir, 0, 52).$puntos_suspensivos; ?></p>
	</div>
	<div class="col col-md-4 social">
		<div class="social_inner">
			<span>Compartir:</span>
			<input type="hidden" class="id_redes" value="proveedores/minisitio/<?php echo $proveedor['id_minisitio']; ?>" />
			<a class="compartir_facebook" disabled href="javascript:;"><i class="fa fa-facebook"></i></a>
			<a href="https://twitter.com/intent/tweet?url=<?php echo base_url('/proveedores/minisitio/'.$proveedor["id_minisitio"]); ?>&text=<?php echo urlencode($meta_title);?>"><i class="fa fa-twitter"></i></a>
			<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('/proveedores/minisitio/'.$proveedor["id_minisitio"]); ?>&description=<?php echo urlencode($meta_title);?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
			<a class="mobile_block" href="whatsapp://send?text=http://webdevelopmentscripts.com"><i class="fa fa-whatsapp"></i></a>
			<a class="compartir_imprimir"><i class="fa fa-print"></i></a>
			<a href="#" data-toggle="modal" data-target="#enviar-amigo"><i class="fa fa-envelope"></i></a>
		</div>		
	</div>
	<div class="col col-md-3 no_border button">

		<?php if($proveedor['id_minisitio'] && $proveedor['id_rubro']){ ?> <a href="<?php echo !empty($hash) ? 'javascript:;' : str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $rubro['url'] . '/presupuesto-empresa_CO_r' . $rubro["id"] . '_m' . $proveedor['id_minisitio']; ?>" class="btn-default <?php echo !empty($hash) ? 'disabled' : ''; ?>" >Pedir presupuesto</a> <?php } ?>

	</div>
</div><!-- #resumen_minisitio -->