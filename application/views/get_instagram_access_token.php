<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Casamientos Online - Instagram Access Token</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="header clearfix">
				<h3 class="text-muted">Casamientos Online</h3>
			</div>

			<div class="jumbotron">
				<h1>Access Token para Instagram</h1>
				<p class="lead">Haga click en el siguiente enlace para generar un access token que podrá ser utilizado en la instancia de API de Instagram para Casamientos Online.</p>
				<p><a class="btn btn-lg btn-success" href="https://api.instagram.com/oauth/authorize/?client_id=81a3e5066a894604ae9058e541691d2e&redirect_uri=<?php echo urlencode('http://www.casamientosonline.com/get_instagram_access_token'); ?>&response_type=token" role="button">Generar Access Token</a></p>
			</div>

			<footer class="footer">
				<p>&copy; 2017 Casamientos Online</p>
			</footer>

		</div> <!-- /container -->


		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

		<script>
		var access_token = location.hash.split("#access_token=")[1];

		if(access_token){
			$('.jumbotron').after('<div class="alert alert-info"><strong>Access token generado:</strong> ' + access_token + '</div>');
		}

		</script>

	</body>
</html>