<?php
$bodyclass = 'page gracias_landing';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
); ?>

<div class="container">

	<h1 class="title_sep">Gracias</h1>
	
	<div id="gracias" class="alert alert-success" role="alert">
		<div>
			<button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>
			<h2><strong>Gracias</strong>, tu solicitud ya ha sido enviada!</h2>
			<p class="bajada">En los próximos días serás contactado, recordá revisar tu casilla de Spam.</p>			
		</div>					
	</div>
	
	<div id="split">
		<section id="primary">
			<p class="txt">Los novios que consultaron por <strong>salones en capital federal</strong> también solicitaron presupuesto a los siguientes rubros:</p>
			<div class="listado row">
				<div class="col-md-4">
					<div class="box">
						<a href=""><img src="<?php echo base_url('/assets/images/guia/salones_de_hoteles.jpg'); ?>"></a>
						<h3><a href="">Salones de Hoteles</a></h3>
						
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="box">
						<a href=""><img src="<?php echo base_url('/assets/images/guia/bares_y_restaurantes.jpg'); ?>"></a>
						<h3><a href="">Bares y Restaurantes </a></h3>
						
					</div>
				</div>
				
				<div class="col-md-4">	
					<div class="box">
						<a href=""><img src="<?php echo base_url('/assets/images/guia/quintas_y_estancias.jpg'); ?>"></a>
						<h3><a href="">Quintas y Estancias</a></h3>
						
					</div>
				</div>
				
				<div class="col-md-4">		
					<div class="box no_margin">
						<a href=""><img src="<?php echo base_url('/assets/images/guia/quintas_y_estancias.jpg'); ?>"></a>
						<h3><a href="">Quintas y Estancias</a></h3>
						
					</div>
				</div>
				
				<div class="col-md-4">	
					<div class="box no_margin">
						<a href=""><img src="<?php echo base_url('/assets/images/guia/bebidas_y_barras.jpg'); ?>"></a>
						<h3><a href="">Bebidas y Barras</a></h3>
						
					</div>
				</div>
				
				<div class="col-md-4">		
					<div class="box no_margin">
						<a href=""><img src="<?php echo base_url('/assets/images/guia/quintas_y_estancias.jpg'); ?>"></a>
						<h3><a href="">Quintas y Estancias</a></h3>
						
					</div>
				</div>
			</div>
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php include('notas_facebook.php'); ?>
			<div class="banner banner_300_250"><p>Banner 300x250 <span>Posicion 1</span></p></div>
		</aside> <!-- #secondary -->	
	</div><!-- #split -->
</div><!-- .container -->
<div id="wrapper_listado_proveedores_interna">
	<?php include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php
include('footer_newsletter.php');
include('footer_info.php'); ?>