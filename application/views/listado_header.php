<?php include("breadcrumbs.php");
include("mod_gracias.php"); ?>

<div class="row">
	<div class="col-md-12">
		<?php if(isset($banners[235]) && $banners[235] || $mostrar_banners){ ?>
			<div class="banner push <?=isset($banners[235]) && $banners[235] ? 'no_bg' : '';?>"><?php echo isset($banners[235]) && $banners[235] ? $banners[235] : '<p>Banner Rubro Push 980x84<span>Tamaño real 1140x115</span></p>' ; ?></div>
		<?php }
		if(isset($banners[111]) && $banners[111] || $mostrar_banners){ ?>
			<div class="banner banner_970_90 <?=isset($banners[111]) && $banners[111] ? 'no_bg' : '';?>"><?php echo isset($banners[111]) && $banners[111] ? $banners[111] : '<p>Banner Rubro Top 728x90<span>Tamaño real 970x90</span></p>' ; ?></div>
		<?php } ?>
	</div>
</div>

<div id="listado_header" class="row">
	<h1 class="col-md-8 col-xs-12 h_sep">
		<span>
			<?php if(isset($rubro["rubro"])&&$rubro["rubro"]){ echo $rubro["rubro"]; } ?> en 
			<span class="dropdown">
				<a class="green suc_selector" id="sucursales-nav-label" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label">

					<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
						<?php
							/* cambio 27062017*/
							$redic = base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') .(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):''));

							$idRubro = $this->rubros_model->get_rubroid($rubro["rubro"],$suc["id"]);
							if(isset($idRubro[0])&&$idRubro[0]) $idRubro = $idRubro[0];	

							if($idRubro){
								$redic = base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') . '/proveedor/'.str_replace(' ','-',$rubro["rubro"]).'_CO_r'.$idRubro["id"]);				
							}
						?>
						<a class="block" href="<?php echo $redic; ?>"><?=$suc["sucursal"];?></a>
					<?php 
					/* 27062017 como estaba antes. 
						<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>"><?=$suc["sucursal"];?></a>

					lo cambie primero para que tome la sucursal ya que sino tiraba un 404.
					Quedo
					<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') .(isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>"><?=$suc["sucursal"];?></a>

					Luego hice que busque el rubro de la sucursal para que redireccione al mismo rubro y no a la fiestas de casamiento que tiene $sucursal_redirect.
					ej: http://www.casamientosonline.com/cordoba/fiestas-de-casamiento
						*/ 
					?>
					<?php } ?>
				</ul>
			</span>
		</span>
	</h1>
	<div class="col-md-4 col-xs-12"><a href="<?php echo base_url($sucursal['nombre_seo'] . '/' . $rubro['url'] . '/solicitar-presupuesto-multiple_CO_r' . $rubro['id'] . '_t' . $id_tipo_formulario . ($elemento_url ? ('_' . $elemento_url) : '') . ($filtros?('?'.urlencode($filtros)):'')); ?>" class="btn btn-default btn-default"><i class="fa fa-calendar"></i>Pedir presupuesto a todas las empresas</a></div>
	
	<div id="filtros_mobile_wrapper" class="mobile_block col-md-12">
		<h4>Refinar busqueda</h4>
		<a href="javascript:;" onClick="$('#filtros_wrapper').toggle();" id="filtros_listado_mobile" class="mobile_block"><i class="fa fa-filter"></i>Filtrar</a>
	</div>
	<?php 
		if(isset($filtros_aplicados)&&$filtros_aplicados){ ?>
		<div id="filtros_aplicados" class="mobile_block col-md-12 filtros_aplicados_mobile">
			<h3 style="font-size: 14px; margin-top: 14px;">Filtros Aplicados</h3>
			<?php foreach ($filtros_aplicados as $filtro_a){ ?>
				<span><span><strong><?php echo $filtro_a['filtro']; ?></strong>: <?php echo $filtro_a["opcion"];?></span> <a href="
				<?php
				$var = str_replace("|" . $filtro_a["id_filtro"]."-".$filtro_a["id_opcion"], "", urldecode($request_uri_base));
				if(count($filtros_aplicados) == 1) $var = rtrim($var, '_');
				echo $var;
				if(isset($param_get)&&$param_get){ echo "?".$param_get; } ?>"><i class="fa fa-close"></i></a></span>
			<?php } ?>
		</div> <!-- #filtros_aplicados -->
	<?php } ?>
</div><!-- #listado_header -->