<?php
$bodyclass = 'casamientos_tv';
include('header.php');

$cant_videos = count($videos);

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_video.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_casamientostv.js') . '"></script>'
);

$primer_video = json_decode($videos_json, TRUE)[0]; ?>
<div id="header_casamientos_tv">
	<div class="container">
		<ul id="breadcrumbs">
			<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
			<li>Casamientos TV</li>
		</ul>
		<h1 class="title_sep">Casamientos TV</h1>
		<?php include('video_slider.php'); ?>
	</div> <!-- .container -->	
</div><!-- #casamientos_tv -->

<div class="container">
	<div id="split">
		<section id="primary">
			<div id="grid_galeria">
				<div id="header_grid_galeria">
					<h3>Todos los videos por Rubro</h3>
					<select class="change_seccion pull-right">
						<option value="">Todas las categorias</option>
						<?php foreach ($ubicaciones as $k => $ubicacion){
							$tmp_ubicacion = explode('@', $ubicacion['hijos']); ?>
							<option value="<?php echo $tmp_ubicacion[0]; ?>" <?=$categoria == $tmp_ubicacion[0] ? 'selected="selected"' : '' ;?>><?php echo $tmp_ubicacion[1]; ?></option>
						<?php } ?>
					</select>
				</div><!-- #header_grid_videos -->
			
				<div class="row">
					<?php foreach ($ubicaciones as $k => $ubicacion){
						$tmp_ubicacion = explode('@', $ubicacion['hijos']);
						if(isset($tmp_ubicacion[3]) && $tmp_ubicacion[3]){
							$tmp_listado = explode('$', $tmp_ubicacion[3]);
							if(!empty($tmp_listado)){
								$str_mas = '';
								foreach ($tmp_listado as $key => $listado){
									$data_rubro = explode('#', $listado);
									if(strlen($data_rubro[1]) >= 25) $str_mas = '...'; ?>
									<div class="col-md-4 col-xs-6 col-xxs-12 col col-<?php echo $tmp_ubicacion[0]; ?>">
										<a href="<?php echo base_url('/casamientos-tv/' . $data_rubro[4] . '_CO_ct' . $tmp_ubicacion[0] . '_ch' . $data_rubro[0]); ?>"><img src="<?php echo base_url('/assets/images/guia/' . ($data_rubro[3] ? $data_rubro[3] : 'default_guia.jpg') ); ?>" alt="<?php echo $data_rubro[1]; ?>" /></a>
										<h3><a <?php if(strlen($data_rubro[1]) >= 25){ echo 'data-toggle="tooltip" data-placement="top"'; } ?> title="<?php echo $data_rubro[1]; ?>" href="<?php echo base_url('/casamientos-tv/' . $data_rubro[4] . '_CO_ct' . $tmp_ubicacion[0] . '_ch' . $data_rubro[0]); ?>"><?php echo substr($data_rubro[1], 0, 25).$str_mas; ?></a></h3>
										<span><i class="fa fa-play-circle"></i> Ver videos del rubro</span>
									</div>	
								<?php $str_mas = '';
								}
							}
						} 
					} ?>
				</div>
			</div><!-- #grid_videos -->
		</section><!-- #primary -->
		
		<aside id="secondary">
			<div id="categorias_notas_sidebar">
				<a href="<?php echo base_url('/casamientos-tv/vestidos_CO_ct101'); ?>" class="box">
					<img src="<?php echo base_url('/assets/images/icon_seccion_notas_vestidos.png'); ?>" alt="Vestidos" />
					<p>Ver videos de</p>
					<h4>Vestidos</h4>
					<i class="fa fa-angle-right"></i>
				</a>
				
				<a href="<?php echo base_url('/casamientos-tv/belleza-y-estilo_CO_ct100'); ?>" class="box">
					<img src="<?php echo base_url('/assets/images/icon_seccion_notas_belleza_estilo.png'); ?>" alt="Belleza y Estilo" />
					<p>Ver videos de</p>
					<h4>Belleza y Estilo</h4>
					<i class="fa fa-angle-right"></i>
				</a>

				<a href="<?php echo base_url('/casamientos-tv/la-organizacion_CO_ct104'); ?>" class="box">
					<img src="<?php echo base_url('/assets/images/icon_seccion_notas_la_organizacion.png'); ?>" alt="La Organización" />
					<p>Ver videos de</p>
					<h4>La Organización</h4>
					<i class="fa fa-angle-right"></i>
				</a>
				
				<a href="<?php echo base_url('/casamientos-tv/luna-de-miel_CO_ct106'); ?>" class="box">
					<img src="<?php echo base_url('/assets/images/icon_seccion_notas_luna_de_miel.png'); ?>" alt="Luna de Miel" />
					<p>Ver videos de</p>
					<h4>Luna de Miel</h4>
					<i class="fa fa-angle-right"></i>
				</a>
			</div><!-- #categorias_notas_sidebar -->

			<?php include('notas_facebook.php'); ?>
			
			<?php if(isset($banners[168]) && $banners[168] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[168]) && $banners[168] ? $banners[168] : '<p>COL TV 300x250<span>Posicion 1</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[169]) && $banners[169] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[169]) && $banners[169] ? $banners[169] : '<p>COL TV 300x250<span>Posicion 2</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[273]) && $banners[273] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[273]) && $banners[273] ? $banners[273] : '<p>COL TV 300x250<span>Posicion 3</span></p>'; ?></div>
			<?php } ?>
			<?php if(isset($banners[274]) && $banners[274] || $mostrar_banners){ ?>
				<div class="banner banner_300_250"><?php echo isset($banners[274]) && $banners[274] ? $banners[274] : '<p>COL TV 300x250<span>Posicion 4</span></p>'; ?></div>
			<?php } ?>
		</aside> <!-- #sidebar -->

	</div><!-- #split -->
	<?php include('popup_amigo.php'); ?>
</div><!-- .container --> 
<script type="text/javascript">
var videos = <?php echo $videos_json; ?>;
</script>
<?php include('footer_info.php'); ?>