<?php
$bodyclass = 'page ceremonias_detalle directorio';
include('header.php'); 

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>'
); ?>

<div class="container">
	<?php include('ceremonias_banners_top.php'); ?>
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles'); ?>">Ceremonias y Civil</a></li>
		<li><a href="<?php echo base_url('/ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina'); ?>">Registros Civiles</a></li>
		<li>Directorio</li>
	</ul>
	<h1 class="title_sep">Registros Civiles<span>Sedes y directorios</span></h1>
	<img class="big_pic border_reg" src="<?php echo base_url('/assets/images/big_pic_registros_civiles.jpg'); ?>" alt="Registros Civiles, Sedes y directorios" />
	<div id="split">
		<section id="primary">
			
			<div class="filtro">
				<h2 class="gum">Directorio</h2>
				<div class="pull-right">
					<label>Seleccioná tu ubicación: </label>
					<select class="ubicacion_registro_civil">
						<option value="caba">CABA</option>
						<option value="bsas">Provincia de Buenos Aires</option>
						<option value="pais">Resto del País</option>
					</select>
				</div>	
			</div><!-- .filtro -->
			
			<div id="listado_directorio">
				<table class="caba directorio_cont" style="display:none;">
					<thead>
						<tr>
							<th>Circunscripción</th>
							<th>Dirección</th>
							<th>Teléfono</th>
							<th>Horario</th>
							<th>Trámites</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="head_mobile">1º (Central)</td>
							<td><strong><i class="fa fa-map-marker"></i>Uruguay 753</strong></td>
							<td>4373-8441/5</td>
							<td>Ver cuadro trámites</td>
							<td>Entrega de Partidas de 8:30 a 13:30hs.<br />Solicitud de Partidas de 8:30 a 14:30hs.<br />Turnos Previos: Nacimiento, Reconocimiento, Matrimonio, Unión Civil, Unión Convivencial, Matrimonio, 7.30 a 18.00 hs. Informaciones Sumarias: 9:30 a 14:30 hs.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">2º</td>
							<td><strong><i class="fa fa-map-marker"></i>Vicente López 2050. Nivel 4. Recoleta Mall. Ingreso por 3º piso.</strong></td>
							<td>4808-7300</td>
							<td>8.30 a 19hs</td>
							<td>Turnos Previos: Nacimiento de 8.30 a 19 hs. Reconocimiento (sin turno) de 8.30 a 19 hs. Matrimonios de 9 a 13 hs.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">3º</td>
							<td><strong><i class="fa fa-map-marker"></i>Sarandí 1273</strong></td>
							<td>- </td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Nacimiento, Reconocimiento, Matrimonio, Unión Civil, Unión Convivencial.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">4ºsubsede</td>
							<td><strong><i class="fa fa-map-marker"></i>Suárez 2032</strong></td>
							<td>4301-6544 / 4628 / 6679 / 6536 / 3867</td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Nacimiento, Reconocimiento, Matrimonio, Unión Civil, Unión Convivencial, Informaciones Sumarias, Solicitud y Entrega de Partidas.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">5º</td>
							<td><strong><i class="fa fa-map-marker"></i>Carlos Calvo 3325</strong></td>
							<td>4931-6699 / 4932-5471</td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Nacimiento, Reconocimiento Matrimonio, Unión Civil, Unión Convivencial.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">6º</td>
							<td><strong><i class="fa fa-map-marker"></i>Patricias Argentinas 227</strong></td>
							<td>4981-5291</td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Nacimiento, Reconocimiento Matrimonio, Unión Civil, Unión Convivencial, Informaciones Sumarias, Solicitud y Entrega de Partidas.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">7º</td>
							<td><strong><i class="fa fa-map-marker"></i>Av. Rivadavia 7202</strong></td>
							<td></td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Nacimiento, Reconocimiento Matrimonio, Unión Civil, Unión Convivencial, Informaciones Sumarias, Solicitud y Entrega de Partidas.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">11º</td>
							<td><strong><i class="fa fa-map-marker"></i>R. Gutiérrez 3254</strong></td>
							<td>4504-6044</td>
							<td>9:30 a 14:30 hs.</td>
							<td>Turnos Previos: Nacimiento, Reconocimiento Matrimonio, Unión Civil, Unión Convivencial. Informaciones Sumarias, Solicitud y Entrega de Partidas.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">12º</td>
							<td><strong><i class="fa fa-map-marker"></i>Miller 2751</strong></td>
							<td>CERRADO TEMPORALMENTE</td>
							<td></td>
							<td></td>
						</tr>
						
						<tr>
							<td class="head_mobile">13º</td>
							<td><strong><i class="fa fa-map-marker"></i>Cabildo 3067</strong></td>
							<td>4702-3748</td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Nacimiento, Reconocimientos, Matrimonio, Unión Civil, Unión Convivencial Solicitud y Entrega de Partidas.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">14º</td>
							<td><strong><i class="fa fa-map-marker"></i>Beruti 3325</strong></td>
							<td>4827-5957</td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Matrimonio, Unión Civil, Unión Convivencial, Informaciones Sumarias, Solicitud y Entrega de Partidas.</td>
						</tr>
						
						<tr>
							<td class="head_mobile">15º</td>
							<td><strong><i class="fa fa-map-marker"></i>Av. Córdoba 5690</strong></td>
							<td>4771-1960 / 4771-0750</td>
							<td>9.30 a 14.30hs</td>
							<td>Turnos Previos: Informaciones Sumarias, Matrimonio, Unión Civil, Unión Convivencial.</td>
						</tr>
					
					</tbody>
				</table>
				
				<div class="bsas directorio_cont" style="display:none;">
					<div class="gum">
					<h3>Atención al Público</h3>
						<p><i class="fa fa-calendar"></i>Lunes a Viernes de 8 a 16hs</p>
						<p><i class="fa fa-phone"></i>0800-999-6304</p>
						<p><i class="fa fa-envelope"></i><a href="">cqr@jg.gba.gov.ar</a></p>
						<span class="block clear"></span>
					</div>
					<p class="gum">Encontrá la <strong>delegación para matrimonios</strong> según tu municipio haciendo click en el siguiente botón.</p>
					<a href="http://www.gob.gba.gov.ar/portal/registro/busca_deleg.php" target="_blank" class="btn btn-default gum_50">Ir a buscador</a>
				</div>
				
				<table class="pais directorio_cont" style="display:none;">
					<thead>
						<tr>
							<th>Provincias</th>
							<th>Dirección</th>
							<th>Localidad</th>
							<th>Teléfono</th>
							<th>Página Web</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="head_mobile">Buenos Aires</td>
							<td><strong><i class="fa fa-map-marker"></i>Calle 1 y 60</strong></td>
							<td>La Plata <br />CP 1900</td>
							<td>Tel: 0221 4 29-6200</td>
							<td><a href="http://www.gob.gba.gov.ar/portal/subsecretarias/gabinete/registro/registro.php" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Catamarca</td>
							<td><strong><i class="fa fa-map-marker"></i>San Martín 382</strong></td>
							<td>San Fernando del Valle de Catamarca<br />CP 4700</td>
							<td>Tel: 03833 4 37525</td>
							<td><a href="https://portal.catamarca.gob.ar/organismo/direccion-de-registro-civil-y-capacidad-de-las-personas-189/" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Córdoba (Capital)</td>
							<td><strong><i class="fa fa-map-marker"></i>Av. Colon 1775 Barrio Alberdi</strong></td>
							<td>Ciudad de Córdoba<br />CP 5000</td>
							<td>Tel: 051-802163
Fax: 051-807346</td>
							<td><a href="http://www.cba.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Córdoba (Interior)</td>
							<td><strong><i class="fa fa-map-marker"></i>Caseros 356</strong></td>
							<td>Ciudad de Córdoba<br />CP 5000</td>
							<td>Tel: 0351 4 342175</td>
							<td><a href="http://www.cordoba.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Corrientes</td>
							<td><strong><i class="fa fa-map-marker"></i>San Lorenzo 837</strong></td>
							<td>Corrientes<br />CP 3400</td>
							<td>Tel: 03783 4 30422</td>
							<td><a href="http://www.corrientes.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Chaco</td>
							<td><strong><i class="fa fa-map-marker"></i>Hipolito Irigoyen 49</strong></td>
							<td>Resistencia <br />CP 3500</td>
							<td>Tel: 03722 4 34452</td>
							<td><a href="http://www.chaco.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Chubut</td>
							<td><strong><i class="fa fa-map-marker"></i>Moreno 450</strong></td>
							<td>Rawson<br />CP 9103</td>
							<td>Tel: 02965 4 81201</td>
							<td><a href="http://www.chubut.gov.ar/rc/" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Entre Rios</td>
							<td><strong><i class="fa fa-map-marker"></i>Dean J. Alvarez Nº 31/33</strong></td>
							<td>Ciudad de Paraná</td>
							<td>Tel: 0343 - 4206705</td>
							<td><a href="http://www.entrerios.gov.ar/registrocivil/" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Formosa</td>
							<td><strong><i class="fa fa-map-marker"></i>Av 25 de Mayo 162</strong></td>
							<td>Formosa <br />CP 3600</td>
							<td>Tel: 03717 4 26424</td>
							<td><a href="http://www.formosa.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Jujuy</td>
							<td><strong><i class="fa fa-map-marker"></i>Belgrano 1083</strong></td>
							<td>San Salvador de Jujuy<br />CP 4600</td>
							<td>Tel: 0388 4 221218</td>
							<td><a href="http://www.jujuy.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">La Pampa</td>
							<td><strong><i class="fa fa-map-marker"></i>Av San Martín 237</strong></td>
							<td>Santa Rosa <br />CP 6300</td>
							<td>Tel: 02954 4 56249 / 4 56249</td>
							<td><a href="http://www.lapampa.gov.ar/" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">La Rioja</td>
							<td><strong><i class="fa fa-map-marker"></i>Dorrego 37</strong></td>
							<td>La Rioja <br />CP 5300</td>
							<td>Tel: 03822 4 53666</td>
							<td><a href="http://www.larioja.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Mendoza</td>
							<td><strong><i class="fa fa-map-marker"></i>Casa de Gobierno P.Baja Cuerpo Central</strong></td>
							<td>Mendoza <br />CP 5500</td>
							<td>Tel: 0261 4 492200 / 4 492245</td>
							<td><a href="http://servicios.mendoza.gov.ar/inscripcion-de-matrimonio/" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Misiones</td>
							<td><strong><i class="fa fa-map-marker"></i>Av Andresito, Centro Comercial Chacra 32 y 33</strong></td>
							<td>Posadas <br />CP 3300</td>
							<td>Tel: 03752 4 58232/34</td>
							<td><a href="http://www.misiones.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Neuquen</td>
							<td><strong><i class="fa fa-map-marker"></i>S. del Estero 64</strong></td>
							<td>Neuquen<br />CP 8300</td>
							<td>Tel: 0229 4 425016</td>
							<td><a href="http://www.neuquen.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Rio Negro</td>
							<td><strong><i class="fa fa-map-marker"></i>Laprida 361</strong></td>
							<td>Viedma <br />CP 8500</td>
							<td>Tel: 02920 4 22066 / 4 20718 / 4 20570</td>
							<td><a href="http://www.rionegro.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Salta</td>
							<td><strong><i class="fa fa-map-marker"></i>Pedernera 273</strong></td>
							<td>Salta<br />CP 4400</td>
							<td>Tel: 0387 4 212009 / 4 221126</td>
							<td><a href="http://www.registrocivilsalta.gov.ar/ok/tramites.php" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">San Juan</td>
							<td><strong><i class="fa fa-map-marker"></i>Laprida 287 (Este)</strong></td>
							<td>San Juan <br />CP 5400 Casilla de Correo 41</td>
							<td>Tel: 0264 4 276186 / 4 272880</td>
							<td><a href="http://www.sanjuan.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">San Luis</td>
							<td><strong><i class="fa fa-map-marker"></i>Junin 766</strong></td>
							<td>San Luis<br />CP 5700</td>
							<td>Tel: 02652 4 23095 / 4 24659</td>
							<td><a href="http://www.sanluis.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Santa Cruz</td>
							<td><strong><i class="fa fa-map-marker"></i>Moreno 123</strong></td>
							<td>Rio Gallegos<br />CP 9400</td>
							<td>Tel: 02966 4 22651</td>
							<td><a href="http://www.santacruz.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Santa Fé</td>
							<td><strong><i class="fa fa-map-marker"></i>San Luis 2950</strong></td>
							<td>Santa Fé<br />CP 3000</td>
							<td>Tel: 0342 4 572864 / 4 577031</td>
							<td><a href="http://www.santafe.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Santiago del Estero</td>
							<td><strong><i class="fa fa-map-marker"></i>Entre Rios 55</strong></td>
							<td>Santiago del Estero<br />CP 4200</td>
							<td>Tel: 0385 4 219044</td>
							<td><a href="http://www.sde.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Tierra del Fuego</td>
							<td><strong><i class="fa fa-map-marker"></i>Gobernador Campos 1465 Tita 7, Casa 42</strong></td>
							<td>Ushuaia<br />CP 9410</td>
							<td>Tel: 02901 4 37049</td>
							<td><a href="http://www.tierradelfuego.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						<tr>
							<td class="head_mobile">Tucumán</td>
							<td><strong><i class="fa fa-map-marker"></i>24 de Septiembre 848</strong></td>
							<td>San Miguel de Tucuman<br />CP 4000</td>
							<td>Tel: 0381 4 218902 / 0381 4 213113</td>
							<td><a href="http://www.tucuman.gov.ar" target="_blank">Ingresá</a></td>
						</tr>
						
						
					</tbody>
				</table>
			</div><!-- #listado_directorio -->
		</section><!-- #primary -->
		
		<aside id="secondary">
			<?php $registros_dir = TRUE;
			include('mod_aside_ceremonias.php');
			include('ceremonias_banners_aside.php'); ?>
		</aside> <!-- #secondary -->
		
	</div><!-- #split -->
</div><!-- .container -->
<?php include('footer.php'); ?>