<?php
$bodyclass = 'page listado resultados_busqueda';
include('header.php');

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="container">
	<div id="listado_header" class="row">
		<h1 class="col-md-12 h_sep">Resultados de busqueda para: <span class="green"><?php echo $search; ?></span></h1>
	</div> <!-- #listado_header -->

	<div id="listado_wrapper">
				
		<div id="resultados_busqueda_wrapper">
			<?php include('busquedas_solapas.php');
			if(isset($elementos['proveedores']) && $elementos['proveedores']){ ?>
				<section class="listado_proveedores gum">
					<?php if($elementos['proveedores_resultados']){ ?>
						<h2 class="busqueda_title"><i class="fa fa-suitcase"></i>Se encontraron <span><?php echo $elementos['proveedores_resultados']; ?></span> Proveedores <?php if($elementos['proveedores_resultados'] > 3 && $tipo != 'proveedores'){ ?><a href="<?php echo 'http://buscador.' . DOMAIN . '/' . $sucursal['nombre_seo'] . '/proveedores/' . $search; ?>"><i class="fa fa-arrow-right"></i>Ver todos los resultados de Proveedores</a><?php } ?></h2>
						<?php foreach ($elementos['proveedores'] as $k => $proveedor){
							include('proveedor.php');
						}
					}else{ ?>
						<h2 class="busqueda_title"><i class="fa fa-suitcase"></i>No se encontraron resultados de proveedores.</h2>
					<?php } ?>
				</section><!-- .listado_proveedores -->
			<?php }elseif($view == 'resultado_busqueda_proveedores'){ ?>
				<h2 class="busqueda_title"><i class="fa fa-suitcase"></i>No se encontraron resultados de proveedores</h2>
			<?php }
			if(isset($elementos['promociones']) && $elementos['promociones']){ ?>
				<section class="listado_promociones gum">
					<?php if($elementos['promociones_resultados']){ ?>
						<h2 class="busqueda_title"><i class="fa fa-tag"></i>Se encontraron <span><?php echo $elementos['promociones_resultados']; ?></span> Promociones <?php if($elementos['promociones_resultados'] > 3 && $tipo != 'promociones'){ ?><a href="<?php echo 'http://buscador.' . DOMAIN . '/' . $sucursal['nombre_seo'] . '/promociones/' 	.$search; ?>"><i class="fa fa-arrow-right"></i>Ver todos los resultados de Promociones</a><?php } ?></h2>
						<?php $puntos_suspensivos = '';
						$puntos_suspensivos_aclaracion = '';
						foreach ($elementos['promociones'] as $k => $producto){
							include('promocion.php');
							$puntos_suspensivos = '';
							$puntos_suspensivos_aclaracion = '';
						}
					}else{ ?>
						<h2 class="busqueda_title"><i class="fa fa-tag"></i>No se encontraron promociones.</h2>
					<?php } ?>
				</section><!-- .listado_promociones -->
			<?php }elseif($view == 'resultado_busqueda_promociones'){ ?>
				<h2 class="busqueda_title"><i class="fa fa-tag"></i>No se encontraron resultados de promociones</h2>
			<?php }
			if(isset($elementos['productos']) && $elementos['productos']){ ?>
				<section class="listado_productos gum">
					<?php if($elementos['productos_resultados']){ ?>
						<h2 class="busqueda_title"><i class="fa fa-star"></i>Se encontraron <span><?php echo $elementos['productos_resultados']; ?></span> Productos <?php if($elementos['productos_resultados'] > 3 && $tipo != 'productos'){ ?><a href="<?php echo 'http://buscador.' . DOMAIN . '/' . $sucursal['nombre_seo'] . '/productos/' . $search; ?>"><i class="fa fa-arrow-right"></i>Ver todos los resultados de Productos</a><?php } ?></h2>
						<?php $puntos_suspensivos = "";
						$puntos_suspensivos_aclaracion = "";
						foreach ($elementos['productos'] as $k => $producto){
							include('producto.php');
							$puntos_suspensivos = "";
						}
					}else{ ?>
						<h2 class="busqueda_title"><i class="fa fa-star"></i>No se encontraron productos.</h2>
					<?php } ?>
				</section>
			<?php }elseif($view == 'resultado_busqueda_productos'){ ?>
				<h2 class="busqueda_title"><i class="fa fa-star"></i>No se encontraron resultados de productos</h2>
			<?php }
			if(isset($elementos['paquetes']) && $elementos['paquetes']){ ?>
				<section class="listado_paquetes gum">
					<?php if($elementos['paquetes_resultados']){ ?>
						<h2 class="busqueda_title"><i class="fa fa-gift"></i>Se encontraron <span><?php echo $elementos['paquetes_resultados']; ?></span> Paquetes <?php if($elementos['paquetes_resultados'] > 3 && $tipo != 'paquetes'){ ?><a href="<?php echo 'http://buscador.' . DOMAIN . '/' . $sucursal['nombre_seo'] . '/paquetes/' . $search; ?>"><i class="fa fa-arrow-right"></i>Ver todos los resultados de Paquetes</a><?php } ?></h2>
						<?php foreach ($elementos['paquetes'] as $k => $producto){
							include('paquete.php');
						}
					}else{ ?>
						<h2 class="busqueda_title"><i class="fa fa-gift"></i>No se encontraron paquetes.</h2>
					<?php } ?>
				</section><!-- .listado_paquetes -->
			<?php }elseif($view == 'resultado_busqueda_paquetes'){ ?>
				<h2 class="busqueda_title"><i class="fa fa-gift"></i>No se encontraron resultados de paquetes</h2>
			<?php }
			if(isset($elementos['notas']) && $elementos['notas']){ ?>
				<section id="notas_listado_segundo_nivel" class="gum">
					<?php if($elementos['notas_resultados']){ ?>
						<h2 class="busqueda_title"><i class="fa fa-file-text-o"></i>Se encontraron <span><?php echo $elementos['notas_resultados']; ?></span> Notas <?php if($elementos['notas_resultados'] > 3 && $tipo != 'editorial'){ ?><a href="<?php echo 'http://buscador.' . DOMAIN . '/' . $sucursal['nombre_seo'] . '/editorial/' . $search; ?>"><i class="fa fa-arrow-right"></i>Ver todos los resultados de Notas</a><?php } ?></h2>
						<?php 
							$str_mas = '';
							$str_mas_titulo = '';
							foreach ($elementos['notas'] as $k => $nota){
								$tmp_secciones = explode('~', $nota['secciones']);
								$nombre_subseccion = '';
								foreach ($tmp_secciones as $key => $secc){
									$tmp_secc = explode('@', $secc);
									$nombre_subseccion .= $tmp_secc[1].', ';
								}
								include('nota.php');
								$str_mas = '';
								$str_mas_titulo = '';
							}
						}else{ ?>
						<h2 class="busqueda_title"><i class="fa fa-file-text-o"></i>No se encontraron notas.</h2>
					<?php } ?>
				</section><!-- .listado_paquetes -->
			<?php }elseif($view == 'resultado_busqueda_notas'){ ?>
				<h2 class="busqueda_title"><i class="fa fa-file-text-o"></i>No se encontraron resultados de notas</h2>
			<?php }

			if($view == 'resultado_busqueda'){
				if(!$elementos['notas'] && !$elementos['paquetes'] && !$elementos['productos'] && !$elementos['promociones'] && !$elementos['proveedores']){ ?>
					<h2 class="busqueda_title"><i class="fa fa-suitcase"></i>No se encontraron resultados.</h2>
				<?php }
			}

			if($view != 'resultados_busqueda'){
				echo $this->pagination->create_links();	
			} ?>
		</div><!-- #resultados_busqueda_wrapper -->
		<aside>
			<?php include('notas_facebook.php'); ?>
			<?php if(isset($banners[328]) && $banners[328] || $mostrar_banners){ // HOME TOP 1140 115 ?>
				<div class="banner banner_300_250"><?php echo isset($banners[328]) && $banners[328] ? $banners[328] : '<p>Banner Resultado de Busqueda 300 x 250 <span>Posicion 1</span></p>'; ?></div>
			<?php }
			if(isset($banners[329]) && $banners[329] || $mostrar_banners){ // HOME TOP 1140 115 ?>
				<div class="banner banner_300_250"><?php echo isset($banners[329]) && $banners[329] ? $banners[329] : '<p>Banner Resultado de Busqueda 300 x 250 <span>Posicion 2</span></p>'; ?></div>
			<?php }
			if(isset($banners[330]) && $banners[330] || $mostrar_banners){ // HOME TOP 1140 115 ?>
				<div class="banner banner_300_250"><?php echo isset($banners[330]) && $banners[330] ? $banners[330] : '<p>Banner Resultado de Busqueda 300 x 250 <span>Posicion 3</span></p>'; ?></div>
			<?php }
			if(isset($banners[331]) && $banners[331] || $mostrar_banners){ // HOME TOP 1140 115 ?>
				<div class="banner banner_300_250"><?php echo isset($banners[331]) && $banners[331] ? $banners[331] : '<p>Banner Resultado de Busqueda 300 x 250 <span>Posicion 4</span></p>'; ?></div>
			<?php } ?>
		</aside>
		<div class="clear"></div>
		
	</div><!-- #listado_wrapper -->
	
</div><!-- .container -->
<?php include('footer.php'); ?>