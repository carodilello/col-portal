<?php if(isset($data_form)&&$data_form){ ?>
    <div id="modal-telefono" class="popup_contacto_minisitio modal fade" tabindex="-1" role="dialog" aria-labelledby="telefono-label">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">

                <?php // echo form_open('http://' . $proveedor["subdominio"] . '.' . DOMAIN . '/' . $proveedor["seo_url"],'id="form_telefono" method="POST" role="form" class="form"'); ?>
                <form action="<?php echo base_url('/formulario/procesar_pedido'); ?>" id="form_telefono" method="POST" role="form" class="form">
                        <?php if($hidden) foreach ($hidden as $hid){ ?>
                            <input type="hidden" name="<?php echo $hid['name']; ?>" value="<?php echo $hid['value']; ?>" />
                        <?php } ?>
                        <input type="hidden" name="id_origen" value="45" />
                        <input type="hidden" name="comentario" value='Esta es una consulta desde el formulario de contacto de su minisitio en la vista click teléfono'>
                        <input type="hidden" name="subdominio"  value="<?php echo $proveedor['subdominio']; ?>">

                        <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title" id="telefono-label">Teléfono de <?php echo $proveedor['proveedor']; ?></h2>
                            <?php if(isset($proveedor['tels']) && is_array($proveedor['tels'])){ ?>
                                <p class="telefono"><i class="fa fa-phone"></i><?php
                                foreach ($proveedor['tels'] as $k => $t){
                                    if($t[0] != '7' && $t[0] != '8') echo '<span>' . $t[1] . '</span>';
                                } ?></p>
                            <?php } ?>
                            <h3>&iquest;Querés que esta empresa te contacte?</h3>
                            <p class="txt">Dejanos tus datos y se contactaran a la brevedad.</p>

                            <?php
                            if(validation_errors() && !set_value('formulario_lateral')){ ?>
                              <div class="errores-generales">
                              <?php echo validation_errors(); ?>
                              <input type="hidden" class="trigger-telefono-popup" value="1" />
                              </div>
                            <?php } ?>


                            <?php
                                $var_form_telefono = 1;
                                include('campos_formulario_telefono.php'); ?>
                        </div>
                </form>
            </div> <!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- #modal-sucursal -->
<?php } ?>
<?php if(isset($data_form) && $data_form && isset($proveedor['whatsapp']) && $proveedor['whatsapp']){ ?>
    <div id="modal-whatsapp" class="popup_contacto_minisitio modal fade" tabindex="-1" role="dialog" aria-labelledby="telefono-label">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url('/formulario/procesar_pedido'); ?>" id="form_whatsapp" method="POST" role="form" class="form">
                        <?php if($hidden) foreach ($hidden as $hid){ ?>
                            <input type="hidden" name="<?php echo $hid['name']; ?>" value="<?php echo $hid['value']; ?>" />
                        <?php } ?>
                        <input type="hidden" name="id_origen" value="51" />
                        <input type="hidden" name="comentario" value='Esta es una consulta desde el formulario de contacto de su minisitio en la vista click Whatsapp'>
                        <input type="hidden" name="subdominio"  value="<?php echo $proveedor['subdominio']; ?>">

                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="modal-title" id="whatsapp-label">Whatsapp de <?php echo $proveedor['proveedor']; ?></h2>
                            <?php
                                if(isset($proveedor['tels']) && is_array($proveedor['tels'])){ ?>
                                <p class="telefono"><i class="fa fa-whatsapp"></i><?php
                                    foreach ($proveedor['tels'] as $k => $t){
                                        if($t[0] == '7') echo '<span>' . trim($t[1],'@') . '</span>';
                                    }
                                    ?></p><?php
                                } ?>

                            <h3>Queres que esta empresa te contacte?</h3>
                            <p class="txt">Dejanos tus datos y se contactaran a la brevedad.</p>

                            <?php
                            if(validation_errors() && !set_value('formulario_lateral')){ ?>
                              <div class="errores-generales">
                              <?php echo validation_errors(); ?>
                              <input type="hidden" class="trigger-telefono-popup" value="1" />
                              </div>
                            <?php } ?>

                            <?php
                                $var_form_telefono = 2;
                                include('campos_formulario_telefono.php'); ?>
                        </div>
                </form>
            </div> <!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- #modal-sucursal -->
<?php } ?>
<?php if(isset($data_form) && $data_form && isset($proveedor['messenger']) && $proveedor['messenger']){ ?>
    <div id="modal-messenger" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="telefono-label">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="messenger-label">Messenger de <?php echo $proveedor['proveedor']; ?></h4>
                </div>
                <form action="<?php echo base_url('/formulario/procesar_pedido'); ?>" id="form_messenger" method="POST" role="form" class="form">
                        <?php if($hidden) foreach ($hidden as $hid){ ?>
                            <input type="hidden" name="<?php echo $hid['name']; ?>" value="<?php echo $hid['value']; ?>" />
                        <?php } ?>
                        <input type="hidden" name="id_origen" value="45" />
                        <input type="hidden" name="comentario" value='Esta es una consulta desde el formulario de contacto de su minisitio en la vista click teléfono'>
                        <input type="hidden" name="subdominio"  value="<?php echo $proveedor['subdominio']; ?>">

                        <div class="modal-body">
                            <h3><?php
                            if(isset($proveedor['tels']) && is_array($proveedor['tels'])){
                                foreach ($proveedor['tels'] as $k => $t){
                                    if($t[0] == '8') echo trim($t[1],'@');
                                }
                            }
                            ?></h3>
                            <p>Queres que esta empresa te contacte?</p>
                            <h5>Dejanos tus datos y se contactaran a la brevedad.</h5>

                            <?php
                            if(validation_errors() && !set_value('formulario_lateral')){ ?>
                              <div class="errores-generales">
                              <?php echo validation_errors(); ?>
                              <input type="hidden" class="trigger-telefono-popup" value="1" />
                              </div>
                            <?php } ?>

                            <?php
                                $var_form_telefono = 3;
                                include('campos_formulario_telefono.php'); ?>
                        </div>
                </form>
            </div> <!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- #modal-sucursal -->
<?php } ?>