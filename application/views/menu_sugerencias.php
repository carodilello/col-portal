<?php 
if(is_array($nav)){ ?>
	<div id="menu_sugerencias_wrapper" class="menu-sugerencias row" data-class="<?php echo isset($class) && $class ? $class : ''; ?>">
		<div class="relative">
			<span class="arrow_dialog_buscador"></span>
		</div>
		<div class="col_buscador">
			<?php 
			$doble_columna_flag = FALSE;
			if(isset($rubros_reordenados[$id_sucursal]) && $rubros_reordenados[$id_sucursal]){
				foreach ($rubros_reordenados[$id_sucursal] as $key => $arr){
					foreach ($arr as $k => $id_rub){
						if(in_array($id_rub,$doble_columna)) $doble_columna_flag = TRUE;
					} ?>
					<div class="<?php 
						foreach ($arr as $k => $id_rub){
							if(isset($nav[$id_rub]) && $nav[$id_rub]) echo str_replace(' ', '_', strtolower($nav[$id_rub]['padre']['nombre'])).' ';
						} ?> col-md-<?=$doble_columna_flag?'6':'3'?>">
						<?php foreach ($arr as $k => $id_rub){
							if(isset($nav[$id_rub]) && $nav[$id_rub]){ ?>
								<h3 class="pink"><span class="<?php echo $k; ?>"></span><?php echo $nav[$id_rub]['padre']['nombre']; ?></h3>
								<?php if(in_array($nav[$id_rub]['padre']['id'], $doble_columna)){
									if(isset($nav[$id_rub]['hijos'])){
										$tot = count($nav[$id_rub]['hijos']);
										$half = round($tot/2, 0, PHP_ROUND_HALF_UP); ?>
										<ul>
											<?php for ($i=0; $i < $half; $i++){
												if(isset($nav[$id_rub]['hijos'][$i]['nombre'])){ ?>
													<li><a href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $nav[$id_rub]['hijos'][$i]['nombre_url'] . '_CO_r' . $nav[$id_rub]['hijos'][$i]['id_rubro']); ?>"><?php echo $nav[$id_rub]['hijos'][$i]['nombre']; ?></a></li>
												<?php }
											} ?>
										</ul>
										<ul>
											<?php for ($i=$half; $i < $half*2; $i++){
												if(isset($nav[$id_rub]['hijos'][$i]['nombre'])){ ?>
													<li><a href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $nav[$id_rub]['hijos'][$i]['nombre_url'] . '_CO_r' . $nav[$id_rub]['hijos'][$i]['id_rubro']); ?>"><?php echo $nav[$id_rub]['hijos'][$i]['nombre']; ?></a></li>
												<?php }
											} ?>
										</ul>
									<?php } ?>
								<?php }else{ ?>
									<ul class="<?php echo str_replace(' ', '_', strtolower($nav[$id_rub]['padre']['nombre']).'_inner'); ?>">
										<?php if(isset($nav[$id_rub]['hijos'])) foreach ($nav[$id_rub]['hijos'] as $j => $el){ ?>
											<?php if(isset($el['nombre'])){ ?>
												<li><a href="<?php echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $el['nombre_url'] . '_CO_r' . $el['id_rubro']); ?>"><?php echo $el['nombre']; ?></a></li>
											<?php } ?>
										<?php } ?>
									</ul>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
				<?php 
				$doble_columna_flag = FALSE;
				}
			} ?>
		</div>
		<div class="link_images row">
			<div>
				<a href="<?php echo base_url($sucursal['nombre_seo'] . '/productos_CO_pd'); ?>"><img style="width:100%;" src="<?php echo base_url('/assets/images/test_images/pic_buscador2.jpg'); ?>" alt="Todos los productos" /></a>
				<a href="<?php echo base_url($sucursal['nombre_seo'] . '/productos_CO_pd'); ?>">Todos los Productos</a>
			</div>
			
			<div>
				<a href="<?php echo base_url($sucursal['nombre_seo'] . '/promociones_CO_pr'); ?>"><img style="width:100%;" src="<?php echo base_url('/assets/images/test_images/pic_buscador3.jpg'); ?>" alt="Todas los promociones" /></a>
				<a href="<?php echo base_url($sucursal['nombre_seo'] . '/promociones_CO_pr'); ?>">Todas las Promociones</a>
			</div>
			<?php if($id_sucursal == 1){ ?>
				<div>
					<a href="<?php echo base_url($sucursal['nombre_seo'] . '/paquetes_CO_pa'); ?>"><img style="width:100%;" src="<?php echo base_url('/assets/images/test_images/pic_buscador1.jpg'); ?>" alt="Todos los paquetes"  /></a>
					<a href="<?php echo base_url($sucursal['nombre_seo'] . '/paquetes_CO_pa'); ?>">Todos los Paquetes</a>
				</div>
			<?php } ?>
		</div>
	</div><!-- #menu_sugerencias_wrapper -->
<?php } ?>