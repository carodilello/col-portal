<?php
$bodyclass = 'page listado';
include('header.php'); ?>
<div class="container">
	<?php include('listado_header.php'); ?>
	<div id="listado_wrapper">
		<aside>
			<?php include('listado_filtros.php');
			include('listado_banners.php'); ?>
		</aside>
		<section id="content_listado">
			<?php include('listado_solapas.php');
			$primer_banner = TRUE;
			$i = 0;
			$xconta = 0;
			if(isset($proveedores)&&$proveedores) foreach ($proveedores as $k => $proveedor){ ?>
				<?php /* Mod 03-07-2017
				antes ponia el primer banner luego de que nivel sea mayor q 4.
				ahora directamente va contando cada posicion y luego de la 3, pone el primer banner de estar
				if($primer_banner && $proveedor["nivel"] > 4 && (isset($banners[311]) && $banners[311] || $mostrar_banners)){ */?>
				<?php if($primer_banner && $xconta == 3 && (isset($banners[311]) && $banners[311] || $mostrar_banners)){ ?>
					<div class="banner_wrapper" style="padding-top:20px;">
						<div class="banner banner_810_115 banner_rubro_posicion_1"><?php echo isset($banners[311]) && $banners[311] ? $banners[311] : '<p>Banner Rubro 810x115 Posicion 1</p>' ; ?></div>
					</div>
				<?php $primer_banner = FALSE;
				}
				$xconta += 1;

				// 12/06/2017 lo comente el if ya que si no estaba el banner de primera posicion pero si el de 3ra x ejemplo, no lo mostraba ya que no sumana el $i
				// 03-07-2017 Antes manejaba cada banner con la variable $i que empezaba a contar luego de poner el primer banner. Como puede ya suceder que ni exista el primer banner y si el 2, 3 o 4, ahora maneja con $xconta y poniendo cada uno de haber cada 3 posiciones
				/*if(!$primer_banner){*/
				$i += 1;
				/*} */?>
				<div class="box_listado <?php if($proveedor["nivel"] <= 4){ echo 'destacado'; } ?>">
					<div class="img_wrapper">
						<a href='<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo']; ?>'>
							<img alt="<?php echo $proveedor["proveedor"]; ?>" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" <?php
								if(isset($proveedor["imagen_gde"]) && $proveedor["imagen_gde"]){
									echo 'src="http://media.casamientosonline.com/images/'.$proveedor["imagen_gde"].'"';
								}elseif(isset($proveedor["imagen_chica"]) && $proveedor["imagen_chica"]){
									echo 'src="http://media.casamientosonline.com/images/'.$proveedor["imagen_chica"].'"';
								}else{
									echo 'src="http://media.casamientosonline.com/logos/'.$proveedor["imagen"].'"';
								} ?> />
						</a>
					</div>
					<div class="info_wrapper">
						<h2><a href='<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo']; ?>'><?php echo $proveedor["proveedor"]; ?></a></h2>
						<?php if($proveedor["zonas"]){ ?>
							<span class="zona"><i class="fa fa-map-marker"></i><?php echo str_replace(",",", ",$proveedor["zonas"]); ?></span>
						<?php } ?>
						<h3 class="descripcion"><?php echo stripslashes($proveedor["breve"]); ?></h3>
						<div class="bottom_actions">
							<div class="inc_minisitio">
								<?php if($proveedor["fotos"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["fotos"]; ?> fotos" ><i class="fa fa-camera"></i></span><?php } ?>
								<?php if($proveedor["videos"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["videos"]; ?> videos" ><i class="fa fa-video-camera"></i></span><?php } ?>
								<?php if($proveedor["audios"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["audios"]; ?> audios" ><i class="fa fa-music"></i></span><?php } ?>
								<?php if($proveedor["comentarios"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["comentarios"]; ?> comentarios" ><i class="fa fa-comments-o"></i></span><?php } ?>
								<?php if($proveedor["productos"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["productos"]; ?> productos" ><i class="fa fa-star"></i></span><?php } ?>
								<?php if($proveedor["promociones"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["promociones"]; ?> promociones" ><i class="fa fa-tag"></i></span><?php } ?>
								<?php if($proveedor["paquetes"] > 0){ ?><span data-toggle="tooltip" data-placement="top" title="<?php echo $proveedor["paquetes"]; ?> paquetes" ><i class="fa fa-gift"></i></span><?php } ?>
							</div><!-- .inc_minisitio -->
							<a href="<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo'] . '/presupuesto-empresas_CO_r' . $rubro["id"] . '_m' . $proveedor["id_minisitio"]; ?>" class="btn btn-default mobile_none">Pedir Presupuesto</a>
							<a href='<?php echo str_replace('{?}', $proveedor['subdominio'], $base_url_subdomain) . $proveedor['rubro_seo']; ?>' class="btn btn-default mobile_block">Ver más info</a>
						</div><!-- .bottom_actions -->
					</div>
				</div><!-- .box_listado -->
 					
				<?php if($xconta == 6 && (isset($banners[312]) && $banners[312] || $mostrar_banners)){ ?>
					<div class="banner_wrapper">
						<div class="banner banner_810_115 banner_rubro_top"><?php echo isset($banners[312]) && $banners[312] ? $banners[312] : '<p>Banner Rubro 810x115 Posicion 2</p>' ; ?></div>
					</div>		
				<?php } ?>
				<?php if($xconta == 9 && (isset($banners[313]) && $banners[313] || $mostrar_banners)){ ?>
					<div class="banner_wrapper">
						<div class="banner banner_810_115 banner_rubro_top"><?php echo isset($banners[313]) && $banners[313] ? $banners[313] : '<p>Banner Rubro 810x115 Posicion 3</p>' ; ?></div>
					</div>
				<?php } ?>
				<?php if($xconta == 12 && (isset($banners[314]) && $banners[314] || $mostrar_banners)){ ?>
					<div class="banner_wrapper">
						<div class="banner banner_810_115 banner_rubro_top"><?php echo isset($banners[314]) && $banners[314] ? $banners[314] : '<p>Banner Rubro 810x115 Posicion 4</p>' ; ?></div>
					</div>		
				<?php } ?>
			<?php } ?>

			<?php
				// controla luego de recorder el bucle si quedaron sin mostrar los banner para ponerlos
				if($xconta <= 3 && (isset($banners[311]) && $banners[311] || $mostrar_banners)){ ?>
					<div class="banner_wrapper" style="padding-top:20px;">
						<div class="banner banner_810_115 banner_rubro_posicion_1"><?php echo isset($banners[311]) && $banners[311] ? $banners[311] : '<p>Banner Rubro 810x115 Posicion 1</p>' ; ?></div>
					</div>
				<?php } ?>
				<?php
				if($xconta <= 6 && (isset($banners[312]) && $banners[312] || $mostrar_banners)){ ?>
					<div class="banner_wrapper">
						<div class="banner banner_810_115 banner_rubro_top"><?php echo isset($banners[312]) && $banners[312] ? $banners[312] : '<p>Banner Rubro 810x115 Posicion 2</p>' ; ?></div>
					</div>		
				<?php } ?>
				<?php 
				if($xconta <= 9 && (isset($banners[313]) && $banners[313] || $mostrar_banners)){ ?>
					<div class="banner_wrapper">
						<div class="banner banner_810_115 banner_rubro_top"><?php echo isset($banners[313]) && $banners[313] ? $banners[313] : '<p>Banner Rubro 810x115 Posicion 3</p>' ; ?></div>
					</div>
				<?php } ?>
				<?php 
				if($xconta <= 12 && (isset($banners[314]) && $banners[314] || $mostrar_banners)){ ?>
					<div class="banner_wrapper">
						<div class="banner banner_810_115 banner_rubro_top"><?php echo isset($banners[314]) && $banners[314] ? $banners[314] : '<p>Banner Rubro 810x115 Posicion 4</p>' ; ?></div>
					</div>		
				<?php } ?>
			<?php include('paginacion.php'); ?>
		</section><!-- #content_listado -->
	</div><!-- #listado_wrapper -->
</div><!-- .container -->
<?php include('footer.php'); ?>