<?php
$bodyclass = 'page listado listado_promociones';
include('header.php'); 
$scripts_javascript = array(
	'<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAbSuwVNqfo1GeWu1Q7r-MHAUs9gb88O_w"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/markerclusterer.js') . '"></script>'
); ?>
<div class="container">
	<?php include('listado_header.php'); ?>
	<div id="listado_wrapper">
		<aside>
			<?php include('listado_filtros.php');
			include('listado_banners.php'); ?>
		</aside>
		<section id="content_listado">
			<?php include('listado_solapas.php');

			if($proveedores){
				$proveedores_array = json_decode($proveedores, 1);
				$proveedores_array = $proveedores_array['markers'];
			} ?>
			<div id="content_listado_mapa">
				<div class="row">
					<p class="resultados col-md-8">Se encontraron <strong><?php echo $resultados; ?></strong> empresas</p>
					<!--<div class="actions col-md-4 pull-right"><span>Cambiar vista: </span><a href="javascript:;"><i class="fa fa-map-marker"></i></a><a href="javascript:;"><i class="fa fa-car"></i></a></div>-->
				</div>
				<div id="content_listado_mapa_map" data-gmap></div>
				<span style="display:none;" id="js_id_rubro"><?php echo $rubro['id']; ?></span>
				<span style="display:none;" id="js_filtros"><?php echo $filtros; ?></span>
				<?php if(isset($proveedores_array)&&$proveedores_array){ ?>
					<div id="proveedores_mapa">
						<h2>Listado de Proveedores</h2>
						<ul class="row">
							<?php foreach ($proveedores_array as $prov){ ?>
								<li class="col-md-4"><a href="<?php echo str_replace('{?}', $prov['subdominio'], $base_url_subdomain) . $prov['rubro_seo']; ?>"><?php echo $prov['label']; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
			</div>
		</section><!-- #content_listado -->
	</div><!-- #listado_wrapper -->
</div><!-- .container -->
<?php include('footer.php'); ?>