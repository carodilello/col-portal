<?php
$bodyclass = 'page minisitio';
include('header.php');

$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		include('mod_header_minisitio.php'); ?>
		
		<section id="wrapper_minisitio" class="row">
		
			<div id="content_minisitio" class="col-md-8">
			
				<div id="testimonios_minisitio" class="box clear big">
					<h2>Testimonios para <?php echo $proveedor['proveedor']; ?></h2>
					
					<div class="box_inner">
						<?php if($puntajes['puntajes_total']){ ?>
						<div id="recomendaciones">
							<!--<p class="col-md-7"><strong><i class="fa fa-check-circle"></i>12 Novios</strong> recomiendan a este Proveedor</p>-->
								<p class="col-md-5 rating">
									<span>Valoración Promedio:</span>	
									<?php for ($i=0; $i < (int) $puntajes['puntajes_total']; $i++){  ?>
										<i class="fa fa-star"></i>
									<?php }
									if(!is_int($puntajes['puntajes_total'])){ ?>
										<i class="fa fa-star-half"></i>
									<?php } ?>
								</p>
						</div><!-- #recomendaciones -->
						<?php } ?>

						<?php foreach ($comentarios as $k => $comentario){
							$com = explode('#',$comentario);
							$body = $com[1];
							$nombre = $com[2];
							$bg_color = $com[13];

							$nom = mb_strtoupper(mb_substr(trim($nombre), 0,1));

							if(strlen($nom) == 3){
								$nom = mb_strtoupper(mb_substr($nombre, 1,1));
							} ?>
							<div class="testimonio">
								<div class="img col-md-2">
									<div class="avatar" style="background: #<?php echo $bg_color; ?>;"><?php echo $nom; ?></div>
								</div>
								<div class="col-md-10 info">
									<h4><?php echo $nombre; ?></h4>
									<?php if(isset($puntajes['puntajes'][$k]) && $puntajes['puntajes'][$k]){ ?>
										<div class="rating">
											<?php for ($i=0; $i < (int) $puntajes['puntajes'][$k]; $i++){  ?>
												<i class="fa fa-star"></i>
											<?php }
											if(!is_int($puntajes['puntajes'][$k])){ ?>
												<i class="fa fa-star-half"></i>
											<?php } ?>
										</div>
									<?php } ?>
									<p><?php echo $body; ?></p>
								</div>
							</div><!-- .testimonio  -->
						<?php } ?>
					</div><!-- .box_inner -->
				</div><!-- #testimonios_minisitio -->
					
			</div><!-- #content_minisitio -->	
			
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php 
				include('minisitio_acciones.php');
				include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->
			
			<div class="col-md-12">
				<?php include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->

		</section><!-- #wrapper_minisitio -->

	</div><!-- .container -->
	<?php 
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php include('footer.php'); ?>