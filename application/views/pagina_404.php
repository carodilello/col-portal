<?php
$bodyclass = 'page page_404';
include('header.php'); ?>
<div id="header_404">
	<div class="inner">
		<h1>Ups! <i>404</i><span>Página no encontrada</span></h1>
		<p>Por favor revisá tu URL o intentá una nueva busqueda</p>
	</div>
</div><!-- #header_404 -->
<div id="missing_children">
	<?php if(isset($banners[325]) && $banners[325] || $mostrar_banners){ ?>
		<div class="banner banner_970_90"><?php echo isset($banners[325]) && $banners[325] ? $banners[325] : '<p>Banner Accesorias Top 970x90</p>'; ?></div>
	<?php } ?>
	<div class="inner">
		<?php if(!empty($missing_children)){ ?>
			<h2>¿No encontraste lo que estabas buscando?</h2>
			<h3>Podés ayudarnos a <span class="green">encontrarlos a ellos</span></h3>
			<div id="children" class="row">
				<?php foreach ($missing_children as $key => $child){ 
					$tmp_fecha = explode('-', $child['data']['fecha_desaparicion']); ?>
					<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
						<div class="box">
							<div class="img_wrapper">
								<img src="http://static.noencontrado.org/img/<?php echo $child['data']['id']; ?>.jpg" />
							</div>
							
							<div class="box_inner">
								<h4><?php echo $child['data']['nombre'] . ' ' . $child['data']['apellido']; ?></h4>
								<p class="first">Falta de su hogar desde: <strong><?php echo $tmp_fecha[2]; ?> de <?php echo ucfirst($meses[(int) $tmp_fecha[1] - 1]); ?> del <?php echo $tmp_fecha[0]; ?></strong></p>
								<p>Localidad: <strong><?php echo $child['data']['residencia']; ?></strong></p>
							</div>
						</div><!-- .box -->	
					</div><!-- .col-md-3 -->
				<?php } ?>
			</div><!-- #children -->
			<p class="info">Si tenés información comunicate al <span class="pink">0800-333-5500</span></p>
			<img id="logo_missing_children" src="<?php echo base_url('/assets/images/logo_missingchildren.jpg'); ?>" />
		<?php } ?>
	</div><!-- .inner -->
</div><!-- #children -->
<?php 
include('mod_listado_proveedores_footer.php');
include('footer.php'); ?>