<?php if(isset($primer_video["id"]) && $primer_video["id"]){ ?>
	<div id="big_video_slider" class="carousel slide" data-ride="carousel" data-pause="hover" data-interval="false">
		<div id="nav_casamientos_tv" class="carousel-indicators">
			<?php if($cant_videos > 0) for ($i = 0; $i < $cant_videos; $i++){ ?>
				<a data-target="#big_video_slider" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?> "><?php echo $i+1; ?></a>
			<?php } ?>
		</div>
		<div id="video" class="carousel-inner" role="listbox">
			<?php foreach ($videos as $k => $video){ ?>
				<div class="item <?php echo $k == 0 ? 'active' : ''; ?>">
					<input type="hidden" class="desc_encoded_<?php echo $k; ?>" value="<?php echo urlencode($video["titulo"]); ?>" />
					<iframe src="http://www.youtube.com/embed/<?php echo $video['video_id'];?>?wmode=opaque&amp;rel=0&amp;vq=hd720&amp;enablejsapi=1" class="iframe_styled ytplayer" allowfullscreen="" id="player<?php echo $k; ?>"></iframe>
				</div>	
			<?php } ?>
		</div>
		<div class="box" data-base-url="<?php echo base_url(); ?>">
			<div class="row">
				<h2 class="titulo_box col-md-9 col-xs-7"><a href="<?php echo base_url('/casamientos-tv/' . $primer_video["titulo_seo"] . '_CO_v' . $primer_video["id"]) ;?>"><?php echo stripslashes($primer_video['titulo']); ?></a></h2>
				<p class="descripcion col-md-9 col-xs-7 clear"><?php echo stripslashes($primer_video['descripcion']); ?>&nbsp;</p>
				<div class="col-md-3">
					<?php
						$video = $primer_video;
						$video['titulo_galeria'] = $video['titulo'];
						if($video['video_prov']){
							$tipo = 'empresas';
						}else{
							$tipo = 'casamientos';
						}
					include('mod_casamientos_tv_social.php'); ?>
				</div>
			</div>
			
			<div class="bottom">
				<div class="proveedor">
					<h3><i class="fa fa-suitcase"></i>
					<?php if(isset($primer_video['proveedores'][0][0]) && $primer_video['proveedores'][0][0]){
						foreach ($primer_video['proveedores'] as $k => $proveedor){
							if(!empty($proveedor[9])){ ?>
								<a href="<?php echo str_replace('{?}', $proveedor[9], $base_url_subdomain) . $proveedor[8]; ?>"><?php echo $proveedor[1]; ?></a>
							<?php }
						}
					}else{
						echo '---';
					} ?>
					</h3>
				</div>
				
				<p class="rubros"><i class="fa fa-calendar"></i>Rubros: 
				<?php if(isset($primer_video['rubros'][0][0]) && $primer_video['rubros'][0][0]){
					foreach ($primer_video['rubros'] as $k => $rubro){ ?>
						<a href="<?php echo base_url($rubro[5] . '/proveedor/' . $rubro[4] . '_CO_r' . $rubro[0]); ?>"><?php echo $rubro[1]; ?></a>
					<?php }
				}else{
					echo '---';
				} ?>
				</p>

				<p class="tags" <?=isset($primer_video['tags'][0][0]) && $primer_video['tags'][0][0] ? "" : "style='display:none;'";?>><i class="fa fa-tag"></i>Tags: 
				<?php 
				if(isset($primer_video['tags'][0][0]) && $primer_video['tags'][0][0]){
					foreach ($primer_video['tags'] as $k => $tag){
						if($tag[1] && $tag[0] != '22898'){ ?>
							<a class="tag_a" href="<?php echo base_url('/casamientos-tv/' . $tag[2] . '_CO_t' . $tag[0]); ?>"><?php echo $tag[1].(($k!=count($primer_video['tags'])-1)?',':''); ?></a>
						<?php }else{
							echo '<span class="tag_span">---</span>';
						}
					}
				}else{
					echo '---';
				} ?>
				</p>
			</div><!-- .bottom -->
		</div><!-- .box -->
	</div>
<?php } ?>