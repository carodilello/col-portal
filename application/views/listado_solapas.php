<ul class="solapas">
	<li><a href="<?=base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_r' . $rubro["id"]); ?>" class="<?php if($tipo_listado == "empresas") echo 'active'; ?>"><i class="fa fa-suitcase"></i>Empresas</a></li>
	
	<?php if(isset($solapas['promociones']['cant'])&&$solapas['promociones']['cant']){ ?>
		<li>
			<a href="<?php if(isset($solapas['promociones']['cant'])&&!$solapas['promociones']['cant']){
				echo 'javascript:;';
			}else{
				echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_tpromociones_r' . $rubro["id"]);
			} ?>" class="<?php if($tipo_listado == "promociones") echo 'active '; ?>"><i class="fa fa-tag"></i>Promociones</a>
		</li>
	<?php } ?>

	
	<?php if(isset($solapas['productos']['cant'])&&$solapas['productos']['cant']){ ?>
		<li>
			<a href="<?php if(isset($solapas['productos']['cant'])&&!$solapas['productos']['cant']){
				echo 'javascript:;';
			}else{
				echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_tproductos_r' . $rubro["id"]);
			} ?>" class="<?php if($tipo_listado == "productos") echo 'active '; ?>"><i class="fa fa-star"></i>Productos</a>
		</li>
	<?php } ?>
	
	<?php if(isset($solapas['paquetes']['cant'])&&$solapas['paquetes']['cant']){ ?>
		<li>
			<a href="<?php if(isset($solapas['paquetes']['cant'])&&!$solapas['paquetes']['cant']){
				echo 'javascript:;';
			}else{
				echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_tpaquetes_r' . $rubro["id"]);
			} ?>" class="<?php if($tipo_listado == "paquetes") echo 'active '; ?>"><i class="fa fa-gift"></i>Paquetes</a>
		</li>
	<?php } ?>
	
	<li>
		<a href="<?=base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_tfotos_r' . $rubro["id"]); ?>" class="<?php if($tipo_listado == "fotos") echo 'active'; ?>"><i class="fa fa-camera"></i>Fotos</a>
	</li>
	
	<?php if(isset($solapas['mapas']['cant'])&&$solapas['mapas']['cant']){ ?>
		<li class="no_margin">
			<a href="<?php if(isset($solapas['mapas']['cant'])&&!$solapas['mapas']['cant']){
				echo 'javascript:;';
			}else{
				echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rubro['url'] . '_CO_tmapa_r' . $rubro["id"]);
			} ?>" class="<?php if($tipo_listado == "mapa") echo 'active '; ?>"><i class="fa fa-map-marker"></i>Mapa</a>
		</li>
	<?php } ?>
</ul>

