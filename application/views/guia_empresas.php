<?php
$bodyclass = 'page guia';
include('header.php'); ?>
<div class="container no_padding">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li>Guia de Empresas</li>
	</ul>
	<div id="guia_header">
		<h1> ¿Estás organizando tu fiesta de casamiento en <span class="dropdown">
				<a class="green suc_selector" id="sucursales-nav-label" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:;"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label">
					<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
						<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect='.(isset($sucursal_redirect)&&$sucursal_redirect?$sucursal_redirect:'')); ?>#guia_header"><?=$suc["sucursal"];?></a>
					<?php } ?>
				</ul>
			</span>?
		</h1>
		<a href="<?php echo base_url('/solicitar-presupuesto-general'); ?>" class="col-md-6 btn btn-default btn-green"><i class="fa fa-calendar"></i>Presupuesto general</a>
		<p id="txt_intro_guia">No pierdas más tiempo! Encontrá aquí todas las opciones disponibles.</p>
		<!--<div id="guia_acciones">
			<div id="vistas">
				<p>Cambiar vista:</p>
				<a href=""><i class="fa fa-th"></i></a>
				<a href=""><i class="fa fa-list-ul"></i></a>
			</div>
			
		</div>-->
	</div> <!-- #guia_header -->
	
</div><!-- .container -->

<section id="guia_grid_wrapper">
	<div class="container">
		<?php if(isset($banners[115]) && $sucursal['id'] != "1" || $mostrar_banners){ ?>
			<div class="banner banner_970_90"><?php echo isset($banners[115]) && $banners[115] ? $banners[115] : '<p>Guia Top 728x90<span>Tamaño real 970x90</span></p>'; ?></div>
		<?php } ?>
		<?php if(isset($banners[263]) || $mostrar_banners){ ?>
			<div class="banner banner_1140_115 push no_bg"><?php echo isset($banners[263]) && $banners[263] ? $banners[263] : '<p>Guía Push 980x84<span>Tamaño real 1140x115</span></p>'; ?></div>
		<?php } ?>
		<h2 id="rubros">Todo para tu Casamiento</h2>
		<div class="row">
			<?php if($solapas){ ?>
				<ul class="solapas row-centered col-md-12">
					<li><a href="javascript:;" class="active solapa solapa_todos"><span></span>Todos</a></li>
					<?php foreach($solapas as $key => $solapa){ ?>
						<li><a class="solapa solapa_<?php echo $key; ?> <?php echo str_replace(' ', '_', strtolower($solapa)); ?>" href="javascript:;" data-id='<?php echo $key; ?>'><span></span><?php echo str_replace('Trajes', 'Novios', str_replace('Belleza', 'Novias', $solapa)); ?></a></li>
					<?php } ?>
				</ul>
			<?php } ?>
		</div>
		
		<div id="boxes_rubros" class="row clear">
			<?php
			$puntos_suspensivos = "";
			if($rubros) foreach($rubros as $k => $rub){
				if(strlen($rub['rubro']) >= 27) $puntos_suspensivos = "..."; ?>
				<div class=" col-md-3 col-sm-6 col-xs-6 col-xxs-12 item-rubro padre_<?php echo $rub['id_padre']; ?>">
					<a href="<?php 
						if($rub['href']){
							echo base_url($rub['href']);
						}else{
							echo base_url($sucursal['nombre_seo'] . '/proveedor/' . $rub['url'] . '_CO_r' . $rub["id_rubro"]);
						} ?>" class="box">
						<?php if($rub['imagen']){ ?>
							<img src="<?php echo base_url('/assets/images/guia/' . $rub['imagen']); ?>"  alt="<?php echo $rub['rubro']; ?>" />
						<?php } ?>
						<h3 <?=$puntos_suspensivos?'data-toggle="tooltip" data-placement="top" title="'.$rub['rubro'].'"':'';?>><?=mb_substr($rub['rubro'], 0, 27).$puntos_suspensivos;?></h3>
					</a>
				</div>	
			<?php 
			$puntos_suspensivos = "";
			} ?>
		</div>
	</div><!-- .container -->
</section><!-- #guia_grid_wrapper -->
<?php include('footer.php'); ?>