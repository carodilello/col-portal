<?php if(count($nav) > 0 && is_array($nav)){ ?>
		<div id="navbarCollapse" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<?php foreach ($nav as $id_rub => $el){
					$cont = 1;
					if(isset($rubros_reordenados_nav[$id_sucursal][$id_rub]['columnas']) && $rubros_reordenados_nav[$id_sucursal][$id_rub]['columnas']){
						$cont = (int)$rubros_reordenados_nav[$id_sucursal][$id_rub]['columnas']; // Esta variable trae la cantidad de columnas, y a partir de esto se hace el calculo para que sean columnas parejas
					}
					$tot = 0;
					if(isset($nav[$id_rub]['hijos'])) $tot = count($nav[$id_rub]['hijos']);
					$part = round($tot/$cont, 0, PHP_ROUND_HALF_UP);
					if($part < $tot/$cont) $part += 1; ?>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle <?=!$el['padre']['href']?'no_pointer':'';?>" href="<?=$el['padre']['href']?(base_url($el['padre']['href'])):'javascript:;';?>" data-url="<?=$el['padre']['href']?(base_url($el['padre']['href'])):'javascript:;';?>"><?php echo $el['padre']['nombre']; ?><span class="arrow_dialog_buscador"></span></a>
						<?php if(isset($el['hijos']) && count($el['hijos']) > 0 && is_array($nav)){ ?>
							<div class="submenu dropdown-menu <?php echo str_replace(' ', '_',strtolower($el['padre']['nombre'])); ?>">
								<?php for($i=0; $i < $cont; $i++){ ?>
									<ul>
										<?php
										for ($j=($part*$i); $j < $part*($i+1); $j++){
											if(isset($el['hijos'][$j]['id_ref']) && $el['hijos'][$j]['id_ref']){ ?>
												<li><a href="<?php echo !empty($el['hijos'][$j]['href'])?(base_url($el['hijos'][$j]['href'])):base_url($sucursal['nombre_seo'] . '/proveedor/'. $el['hijos'][$j]['nombre_url'] . '_CO_r' . $el['hijos'][$j]['id_rubro']); ?>"><?php echo $el['hijos'][$j]['nombre']; ?></a></li>
											<?php }
										} ?>
									</ul>
								<?php }
								if(isset($rubros_reordenados_nav[$id_sucursal][$id_rub]['imagenes'])&&$rubros_reordenados_nav[$id_sucursal][$id_rub]['imagenes']){ ?>
									<div class="nav-images">
										<?php if(isset($el['imagenes']) && $el['imagenes']) foreach ($el['imagenes'] as $k => $imagen){ ?>
											<a class="a_parent" href="<?=$imagen['href']?(base_url(($imagen['url_sucursal'] ? ($sucursal['nombre_seo'] . '/') : '')  . $imagen['href'])):(base_url('/' . $sucursal['nombre_seo'] . '/fiestas-de-casamiento')); ?>"><img alt="<?php echo $imagen['nombre']; ?>" src="<?php echo base_url('/assets/images/' . $imagen['imagen']); ?>" /><span><?php echo $imagen['nombre']; ?></span></a>
										<?php } ?>
									</div>
								<?php } ?>
							</div>	
						<?php } ?>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>
<input type="hidden" class="base_url" value="<?php echo base_url(); ?>" />
<input type="hidden" class="base_url_subdomain" value="<?php echo $base_url_subdomain; ?>" />