<?php echo form_open(base_url($ubicacion),'id="form_presupuesto_wrapper" method="POST" role="form" class="form"'); ?>
	<input type="hidden" name="celular" value="-" />
	<aside class="col-md-3">
		<h3>Telefónicamente</h3>
		<p class="no_margin">Lunes a Viernes de 9:30 a 18 hrs.</p>
		<ul class="gum_30">
			<li class="no_border"><i class="fa fa-phone"></i>5197.5525</li>
		</ul>
		<h3>Vía Email</h3>
		<a href="mailto:info@casamientosonline.com"><i class="fa fa-envelope"></i>info@casamientosonline.com</a>
	</aside>

	<div id="form_presupuesto" class="col-md-9">
		<?php if($success){ ?>
			<div id="gracias_como_anunciar">
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<p><i class="fa fa-check-circle-o"></i><span><strong>Gracias por contactarte!</strong> En CasamientosOnline valoramos mucho tu opinión. En breve serás contactado por un Ejecutivo de Cuentas.</span></p>
				</div>
			</div><!-- #gracias -->	
		<?php }elseif(validation_errors()){ ?>
			<div class="errores-generales">	
				<div><i class="fa fa-exclamation-circle"></i> 
					<?php echo validation_errors(); ?>	
				</div>	
			</div> <!-- .errores-generales -->	
		<?php }else{ ?>
			<p class="txt_form_sugerencia">Para mayor información acerca del portal y opciones de publicación, contactate con nosotros completando el siguiente formulario:</p>
		<?php } ?>
		<fieldset class="row no_margin_b">
			<div class="col-md-4 input_icon form-group has-feedback has-success">
				<span class="fa fa-user"></span>
				<input name="nombre" type="text" placeholder="Nombre" value="<?php echo set_value('nombre'); ?>" required>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback has-success">
				<span class="fa fa-user"></span>
				<input name="apellido" type="text" value="<?php echo set_value('apellido'); ?>" placeholder="Apellido" required>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback has-success">
				<span class="fa fa-phone"></span>
				<input type="text" name="telefono" placeholder="Teléfono" value="<?php echo set_value('telefono'); ?>" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido">
				<div class="help-block with-errors"></div>
			</div>
		</fieldset>
		
		<fieldset class="row no_margin_b">
			<div class="col-md-4 input_icon form-group has-feedback has-success">
				<span class="fa fa-envelope"></span>
				<input type="email" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>" required>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback has-success">
				<span class="fa fa-map-marker"></span>
				<select name="id_provincia" class="provincias" required>
					<option value="">Seleccionar una Provincia</option>
					<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
						<option value="<?php echo $k; ?>" <?=$k==set_value('id_provincia') ? 'selected' : '';?>><?php echo $provincia; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback has-success">
				<span class="fa fa-location-arrow"></span>
				<select name="id_localidad" class="localidades" required>
					<option value="">Seleccionar una Localidad</option>
					<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
						<option value="<?php echo $k; ?>" <?=$k==set_value('id_localidad') ? 'selected' : '';?>><?php echo $localidad; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>
		</fieldset>
				
		<div class="form-group has-feedback">
			<textarea placeholder="Comentarios" name="comentario" required><?php echo set_value('comentario'); ?></textarea>
			<div class="help-block with-errors"></div>
		</div>

		<div class="clear">
			<div class="checkbox col-md-7 check_form no_padding has-feedback">
			  <label for="infonovias"><input id="infonovias" name="infonovias" type="checkbox" value="1" checked="checked">Deseo recibir el Infonovias y promociones de casamientos</label>
			</div>
			
			<input type="submit" class="btn-default submit_presupuesto disabled" value="Enviar consulta">
			<div class="has-error has-danger has-feedback">
				<div style="display:none; float:right;" class="error-zonas help-block with-errors">
					<ul class="list-unstyled">
						<li>Debe seleccionar al menos una zona</li>
					</ul>
				</div>
			</div>
		</div><!-- .clear -->
	</div>
</form>