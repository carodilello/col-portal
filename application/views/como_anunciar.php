<?php
$bodyclass = 'como_anunciar site_proveedor';
include('header_site_proveedor.php'); ?>
<section id="header_como_anunciar">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-6 txt">
				<h1><strong>17 años acompañando<br /> a <span class="pink">Empresas y Novios.</span></strong></h1>
				<p class="gum_30 big_txt">¡Se parte del principal <strong>Portal de Casamientos de la Argentina!</strong></p>
				
				<!-- <div id="login_proveedor_header">
					<p>¿Ya estás publicando en el portal?</p>
					<a href="" class="btn btn-default btn-green"><i class="fa fa-sign-in"></i>Ingresár al Panel de Control</a>
				</div> -->
				<ul>
					<li><i class="fa fa-check"></i>Llegá a tu público objetivo y <strong>potencia tus ventas</strong>.</li>
					<li><i class="fa fa-check"></i>Recibí solicitudes diarias de <strong>pedidos de presupuestos</strong>.</li>
					<li><i class="fa fa-check"></i>Auto-gestioná <strong>tu Minisitio y tu información</strong>.</li>
					<li><i class="fa fa-check"></i>Asesoramiento permanente de tu Ejecutiva de Cuentas.</li>
					<li><i class="fa fa-check"></i>Uso exclusivo de nuestro CRM para administrar tus eventos.</li>
					<li><i class="fa fa-check"></i>Presencia Nacional.</li>
				</ul>
				
			</div>
			
			<div id="form_como_anunciar_wrapper" class="col-md-5 col-xs-5 pull-right box">
				<h3>¡Empezá hoy mismo!</h3>
				
				<?php echo form_open(base_url('/empresas/como-anunciar#gracias'), 'method="POST" role="form" class="form"'); ?>
					<input type="hidden" name="accion" value="proveedor" />
					<input type="hidden" name="apellido" value="-" />
					<input type="hidden" name="infonovias" value="1" />
					<input type="hidden" name="opcion_publicacion" value="Otro" />
					<input type="hidden" class="texto_por_defecto" value="Localidad" />
					<input type="hidden" class="rubro_activo" value="0" />

					<?php if($success){ ?>
						<div id="gracias">
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<p><i class="fa fa-check-circle-o"></i><span><strong>Gracias por tu consulta,</strong> en breve serás contactado por una Ejecutiva de Cuentas.</span></h3>
							</div>
						</div><!-- #gracias -->	
					<?php }elseif(validation_errors()){ ?>
						<div class="errores-generales">	
							<div><i class="fa fa-exclamation-circle"></i> 
								<?php echo validation_errors(); ?>	
							</div>	
						</div> <!-- .errores-generales -->	
					<?php }else{ ?>
						<p>Completá tus datos y una Ejecutiva de Cuentas se comunicará con vos.</p>
					<?php } ?>
					
					<div class="row">
						<div class="form-group has-feedback col-md-6">
							<input type="text" name="empresa" placeholder="Nombre Empresa" value="<?php echo set_value('empresa'); ?>" class="form-control" required />
						</div>
					
						<div class="form-group has-feedback col-md-6">
							<select class="form-control sel_sucursal" required name="id_sucursal">
								<option value="">Publicar en...</option>
								<?php foreach ($sucursales as $suc){ ?>
									<option value="<?php echo $suc['id']; ?>" <?=set_value('id_sucursal') == $suc['id'] ? "selected='selected'" : "";?>><?php echo $suc['sucursal']; ?></option>
								<?php } ?>
							</select>
						</div>
						
					</div><!-- .row -->	
					
					<div class="row">
						<div class="form-group has-feedback col-md-6">
							<select class="form-control sel_rubro" required name="id_rubro">
								<option value="">Rubro</option>
								<?php foreach ($combo_rubros as $k => $grupo){ ?>
									<optgroup label="<?php echo $k; ?>">
										<?php foreach ($grupo as $j => $rubro) { ?>
											<option value="<?php echo $j; ?>" <?=set_value('id_rubro') == $j ? "selected='selected'" : "";?>><?php echo $rubro; ?></option>
										<?php } ?>
									</optgroup>
								<?php } ?>
							</select>
						</div>
					
						<div class="form-group has-feedback col-md-6">
							<input type="text" name="nombre" placeholder="Nombre y Apellido"  value="<?php echo set_value('nombre'); ?>" class="form-control" required />
						</div>
						
					</div><!-- .row -->
					
					<div class="row">
						<div class="form-group has-feedback col-md-6">
							<input type="email" name="email" placeholder="Email"  value="<?php echo set_value('email'); ?>" class="form-control" required />
						</div>
						
						<div class="form-group has-feedback col-md-6">
							<input type="text" name="telefono" placeholder="Teléfono"  value="<?php echo set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
						</div>
					</div><!-- .row -->
					
					
					
					<div class="row">
						<?php if(!empty($provincias)){ ?>
							<div class="form-group has-feedback has-success col-md-6">
								<select name="id_provincia" class="form-control provincias" required>
									<option value="">Provincia</option>
									<?php foreach ($provincias as $k => $provincia) { ?>
										<option value="<?php echo $k; ?>" <?=set_value('id_provincia') == $k ? "selected='selected'" : "";?>><?php echo $provincia; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group has-feedback has-success col-md-6">
								<select name="id_localidad" class="form-control localidades" required>
									<option value="">Localidad</option>
									<?php if(!empty($localidades)) foreach ($localidades as $k => $localidad) { ?>
										<option value="<?php echo $k; ?>" <?=set_value('id_localidad') == $k ? "selected='selected'" : "";?>><?php echo $localidad; ?></option>
									<?php } ?>
								</select>
							</div>	
						<?php } ?>
					</div><!-- .row -->
					
					<div class="row">
						<div class="form-group has-feedback col-md-12">
							<textarea class="form-control" name="comentario" placeholder="Mensaje" required><?php echo set_value('comentario'); ?></textarea>
						</div>
					</div><!-- .row -->	
					
					<div class="row">
						<div class="col-md-12">
							<input type="submit" class="btn-default submit col-md-12" value="Enviar Solicitud" />
						</div>
					</div>
				</form>
			</div><!-- #form_como_anunciar_wrapper -->
		</div>
		
	</div><!-- .container --> 
</section> <!-- #header_como_anunciar --> 

<div id="title_banda">
	<div class="inner">
		<p><i class="fa fa-play"></i>¿Por qué elegir Casamientos Online?</p>
	</div>
</div><!-- #title_banda --> 

<section id="iconos_porque_publicar">
	<div class="inner">
		<div class="row">
			<div class="col col-md-3 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<span class="valign">
						<img src="<?php echo base_url('/assets/images/icon_numeros_novios.png'); ?>" alt="Contacto directo con los novias">
					</span>
				</div>
				<div class="data">
					<h2>Contacto directo con los novios</h2>
					<p>Recibir presupuestos de novios nunca fue tán fácil.</p>
				</div>
			</div><!-- .col-md-6 -->
			
			<div class="col col-md-3 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<span class="valign">
						<img src="<?php echo base_url('/assets/images/icon_asesoramiento.png'); ?>" alt="Asesoramiento profesional">
					</span>
				</div>
				<div class="data">
					<h2>Asesoramiento profesional</h2>
					<p>Las ejecutivas te guiarán en todo momento</p>
				</div>
			</div><!-- .col-md-6 -->
			
			<div class="col col-md-3 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<span class="valign">
						<img src="<?php echo base_url('/assets/images/icon_admin.png'); ?>" alt="Es Rápido, fácil y cómodo">
					</span>
				</div>
				<div class="data">
					<h2>Es Rápido, fácil y cómodo</h2>
					<p>Manejá tus presupuestos desde tu Panel de Control</p>
				</div>
			</div><!-- .col-md-6 -->
			
			<div class="col col-md-3 col-xs-6 col-xxs-12 no_border">
				<div class="img_wrapper">
					<span class="valign">
						<img src="<?php echo base_url('/assets/images/icon_multiples.png'); ?>" alt="Multiples canales de publicación">
					</span>
				</div>
				<div class="data">
					<h2>Multiples canales de publicación</h2>
					<p>Expos para Novias, Banners, Newsletters y más</p>
				</div>
			</div><!-- .col-md-6 -->
		</div><!-- .row -->
	</div><!-- .inner -->	

</section><!-- #iconos_jornada_eventos -->

<section id="iconos_numeros">
	<div class="inner">	
		<h3 class="title_sep_center">Los números de Casamientos Online</h3>
		<div class="row">
			<div class="col-md-4 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_visitas_diarias.png'); ?>" alt="Visitas promedio diarias interactúan con el Portal" />
				</div>
				<div class="data">
					<strong>7.000</strong>
					<p>Visitas promedio diarias interactúan con el Portal.</p>
				</div>
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_visitas_mensuales.png'); ?>" alt="Visitas mensuales al portal" />
				</div>
				<div class="data">
					<strong>180.000</strong>
					<p>Visitas mensuales al portal</p>
				</div>	
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_paginas.png'); ?>" alt="Páginas vistas por mes" />
				</div>
				<div class="data">
					<strong>600.000</strong>
					<p>Páginas vistas por mes</p>
				</div>	
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12 no_margin">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_novios.png'); ?>" alt="Novios registrados anualmente" />
				</div>
				<div class="data">
					<strong>45.000</strong>
					<p>Novios registrados anualmente</p>
				</div>
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4 col-xs-6 col-xxs-12 no_margin">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_empresas.png'); ?>" alt="Empresas publicando sus productos y Servicios" />
				</div>
				<div class="data">
					<strong>1.500</strong>
					<p>Empresas publicando sus productos y Servicios</p>
				</div>
			</div><!-- .col-md-4 -->
			
			<div class="col-md-4  col-xs-6 col-xxs-12 no_margin">
				<div class="img_wrapper">
					<img src="<?php echo base_url('/assets/images/icon_numeros_pedidos_anuales.png'); ?>" alt="Pedidos de presupuestos anuales a las empresas" />
				</div>
				<div class="data">
					<strong>650.000</strong>
					<p>Pedidos de presupuestos anuales a las empresas</p>
				</div>	
			</div><!-- .col-md-4 -->
			
		</div>
	</div><!-- .inner -->
</section><!-- #iconos_numeros -->

<section id="capturas_portal_panel">
	<div class="container no_padding">
		<div class="row">
			<div id="el_portal" class="col-md-6 col-xs-6">
				<h3>El portal</h3>
				<img src="<?php echo base_url('/assets/images/captura_portal.jpg'); ?>" alt="Captura portal" />
				<p><i class="fa fa-play"></i><span><strong> Totalmente dinámico:</strong>  Actualización diaria con información de distintos proveedores del mercado, servicios y productos.</span></p>
				<p><i class="fa fa-play"></i><span><strong> Notas de interés y contenido editorial:</strong> Un equipo de expertos al servicio de los intereses de las novias y las últimas tendencias en la organización del casamiento.</span></p>
				<p><i class="fa fa-play"></i><span><strong> Alcance Nacional:</strong>  Un sitio 100% argentino con llegada a todas las provincias. Se convierte así en una herramienta online estés donde estés.</span></p>
			</div><!-- #el_portal --> 
			
			<div id="el_panel" class="col-md-6 col-xs-6">
				<h3>Panel de Control / CRM</h3>
				<img src="<?php echo base_url('/assets/images/captura_admin.jpg'); ?>" alt="Captura administrador" />
				<p><i class="fa fa-play"></i><span><strong> Gestor de Contactos:</strong> Administrá tus pedidos de presupuesto, agendá alarmas, agregá notas, contestá y recibí emails de parte de los novios en todos tus dispositivos.</span></p>
				<p><i class="fa fa-play"></i><span><strong> Auto administración de los contenidos que se publican en tu minisitio:</strong>  fotos, videos, audios, productos, promos, paquetes, empresas asociados y testimonios.</span></p>
				<p><i class="fa fa-play"></i><span><strong>Reportes:</strong> Dale seguimiento a todas tus publicaciones. Un completo tablero que muestra de forma consolidada, la cantidad de visitas y como estas se traducen en consultas directas a tu empresa.</span></p>
			</div><!-- #el_panel --> 
		</div>
	</div>
</section><!-- #split_screen -->

<div class="container">
	<section id="opciones_publicacion">
		
			<div class="intro">
				<div class="row">
					<div class="col-md-6 col-xs-6">
						<h2 class="title_sep">Opciones de publicación</h2>
						<p><strong class="green">MINISITIO:</strong> Un espacio multimedial propio con información personalizada de tu empresa donde mostrar fotos, videos, audios, servicios, productos, promociones, paquetes de casamiento, oportunidades y testimonios de tus clientes.</p>
						<p>Gestioná tu información con un completo sistema de gestión para manejar el dialogo y cierres con tus clientes, tu Minisitio, y mucho más!</p>
						<a href="<?php echo base_url('/empresas/contacto'); ?>" class="btn btn-default">¡Obtené tu Minisitio Hoy!</a>
					</div>
					<div class="col-md-6 col-xs-6">
						<img src="<?php echo base_url('/assets/images/captura_minisitio.jpg'); ?>" alt="Obtené tu Minisitio Hoy" />
					</div>
				</div>
			</div><!-- .intro -->	
			
			<div class="row">
				<h3 class="col-md-12">Otros productos para potenciar tu presencia</h3>
				
				<div class="col-md-6 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_publicacion_banner.png'); ?>" alt="Banners y destacados" />
					</div>
					<p class="data"><strong>Banners y destacados:</strong> Destacá tu empresa por sobre el resto. Herramientas comerciales ideales para comunicar promociones y obtener mayores contactos personalizados. Además podrás generar branding y awareness de tu marca.</p>
				</div><!-- .col-md-6 -->
				
				<div class="col-md-6 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_publicacion_infonovias.png'); ?>" alt="Infonovias y Flyers" />
					</div>
					<p class="data"><strong>Infonovias y Flyers</strong> Newsletter semanal dirigido a las 18 mil novias registradas con productos, promociones, consejos y las últimas tendencias para organizar el casamiento.</p>
				</div><!-- .col-md-6 -->
				
				<div class="col-md-6 clear col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_publicacion_jornadas.png'); ?>" alt="Eventos Exclusivos" />
					</div>
					<p class="data"><strong>Eventos Exclusivos</strong> Participá de nuestros exclusivos eventos y exposiciones anuales para novios. Son ideales para que la novia logre un contacto personal con tu empresa. Durante el 2017 realizaremos nuestras Jornadas número 40 y 41. Además se llevarán a cabo nuestros tradicionales Té de Novias en Buenos Aires, Rosario y Córdoba.</p>
				</div><!-- .col-md-6 -->
				
				<div class="col-md-6 col-xs-6">
					<div class="img_wrapper">
						<img src="<?php echo base_url('/assets/images/icon_publicacion_crm.png'); ?>" alt="Presencia en Redes Sociales" />
					</div>
					<p class="data"><strong>Presencia en Redes Sociales: </strong>Tener presencia activa en Internet hoy, es fundamental, y en Redes Sociales: “indispensable”. Las Redes Sociales se han convertido en una gran herramienta para tener una interacción con tus clientes en cualquier momento, dar una mejor calidad de servicio, y lograr viralizar y llegar a nuevos usuarios con la información específica.</p>
				</div><!-- .col-md-6 -->
				
			</div>
	</section><!-- #opciones_publicacion -->
	
	<!-- <div id="mediakit">
		<h2>Media Kit 2016</h2>	
		<p>Conocé los diferentes espacios, y opciones para publicitar tu empresa.</p>
		<a href="" class="btn btn-default"><i class="fa fa-download"></i>Descargar Media Kit en PDF</a>
	</div><!-- #mediakit -->
	
	<?php /* <div id="presencia_redes_sociales">
		<h2 class="title_sep">Presencia en Redes Sociales</h2>
		<div class="data">
			<p>Tener una presencia activa es fundamental en Internet pero sobre todo en las Redes Sociales hoy en día para cualquier tipo de negocio. Las Redes Sociales se han convertido en una gran herramienta para tener una mejor interacción con tus clientes o usuarios y dar una mejor calidad de servicio, para poder ofrecer tu producto u oportunidad de negocio a través de Internet, sin llegar a ser intrusivo con las personas.</p>
			<p>Solo por el mero hecho de que las Redes Sociales, como su propio nombre indica son para sociabilizar con las personas, hacer amigos.</p>
			

			<div class="listado">
				<div class="row">
					<div class="col-md-6">
						<p><i class="fa fa-play"></i><span>Acción conjunta durante 3 meses.</span></p>
						<p><i class="fa fa-play"></i><span>Notas Editoriales: 1 por mes. Modelo de nota tipo entrevista o gacetilla de prensa.</span></p>
						<p><i class="fa fa-play"></i><span>Texto 500 a 1200 caracteres. 6/10 fotos. 1 video.</span></p>
						<p><i class="fa fa-play"></i><span>1 Publicación destacada en Instagram</span></p>
					</div>
					
					<div class="col-md-6">
						<p><i class="fa fa-play"></i><span>Publicación destacada en Home 1 semana.</span></p>
						<p><i class="fa fa-play"></i><span>Publicación en sección como nota destacada durante 1 mes.</span></p>
						<p><i class="fa fa-play"></i><span>6 publicaciones en Facebook (cada 15 días).</span></p>
						<p><i class="fa fa-play"></i><span>15 publicaciones en Twitter.</span></p>
					</div>
				</div><!-- .row -->	
			</div><!-- .listado -->
		</div><!-- #data -->
	</div><!-- #presencia_redes_sociales -->
	*/ ?>

</div><!-- .container -->
<?php include('footer_info.php'); ?>