<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
<title><?=isset($meta_title)&&$meta_title?$meta_title:"Casamientos Online ". date('Y');?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo !empty($meta_descripcion) ? str_replace('"', '', $meta_descripcion) : "Casamientos Online 2017"; ?>" />
<meta name="language" content="es" />
<meta name="author" content="Casamientosonline.com" />

<link rel="shortcut icon" href="<?php echo base_url('/favicon.png'); ?>" type="image/png" />
<link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:700i,900i" rel="stylesheet">
<link href="<?php echo base_url('/assets/css/bootstrap.css'); ?>" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url('/assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('/assets/css/gallery/blueimp-gallery.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/gallery/bootstrap-image-gallery.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/datepicker/bootstrap-datepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/datepicker/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('/assets/css/style.css'); ?>" type="text/css" rel="stylesheet" />
<link href="<?php echo base_url('/assets/css/responsive.css'); ?>" type="text/css" rel="stylesheet" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P3MV');</script>
<!-- End Google Tag Manager -->

<!-- Tweet Cards -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@Casamientos_OL" />
<meta name="twitter:title" content="<?=isset($twitter_title)&&$twitter_title ? $twitter_title : (isset($meta_title)&&$meta_title?$meta_title:"Casamientos Online 2016");?>" />
<meta name="twitter:description" content="<?=isset($twitter_descripcion)&&$twitter_descripcion ? str_replace('"', '', $twitter_descripcion): (!empty($meta_descripcion) ? str_replace('"', '', $meta_descripcion) : "Casamientos Online 2016") ;?>" />
<meta name="twitter:image" content="<?=isset($twitter_image)&&$twitter_image ? $twitter_image: base_url("/assets/images/logo_twitter.png"); ?>" />
<!-- End Tweet Cards -->

<!-- Open Graph data -->
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.casamientosonline.com/" />
<meta property="og:title" content="{título de página}" />
<meta property="og:image" content="http://www.casamientosonline.com/images/casamientos_online.png" />
<meta property="og:site_name" content="Casamientos Online" />
<meta property="og:description" name="description" content="<?php echo !empty($meta_descripcion) ? str_replace('"', '', $meta_descripcion) : "Casamientos Online 2016"; ?>" />
<meta property="fb:app_id" content="230987006941395" />
<meta property="fb:admins" content="100000469127452" />
<!-- End Open Graph data -->

<!-- Google Rich Snippet -->        
<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "WebSite",
	  "url": "http://www.casamientosonline.com/",
	  "potentialAction": {
	    "@type": "SearchAction",
	    "target": "http://www.casamientosonline.com/busqueda.php?tipo=1&term={search_term}",
	    "query-input": "required name=search_term"
	  }
	}
</script>
<!-- End Google Rich Snippet -->


<!-- Facebook Pixel Code -->
<script>
	!function(f,b,e,v,n,t,s){ if(f.fbq)return;n=f.fbq=function(){ n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '294684074060325');
	fbq('track', 'PageView');
</script>

<noscript>
	<img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=294684074060325&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="<?php echo $bodyclass;?> <?php echo strtolower(str_replace(' ', '_', $sucursal['sucursal'])); ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3MV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Pluggin Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7&appId=211692522224170";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<!-- End Pluggin Facebook -->
<link rel="canonical" href="<?php echo !empty($canonical) ? $canonical : ''; ?>">
<header id="header_site_proveedor">
	<section id="top_bar">
		<div class="inner">
			<span class="titulo pull-left"><i class="fa fa-suitcase"></i>Estás en Área Empresas</span>
			<a id="volver_portal" href="<?php echo base_url('/'); ?>" class="inline_b pull-left"><i class="fa fa-chevron-left"></i>Volver al portal</a>

			<div id="datos_contacto" class="pull-right">
				
				<a class="mail" href="mailto:info@casamientosonline.com"><i class="fa fa-envelope"></i>info@casamientosonline.com</a>
				<a class="phone" href="javascript:;" class="no_link"><i class="fa fa-phone"></i>(011) 5197-5525</a>
				
				<a href="http://www.plataformaeventos.com/login.php" class="login" target="_blank"><i class="fa fa-sign-in"></i>Ingresar al Panel</a>
				
			</div><!-- #login -->
		</div><!-- .inner -->
	</section>
	
	<div class="inner relative fix_height_header">
		<div>
			<div id="logo_col" class="col-md-3 clear no_padding">
				<a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>"><img src="<?php echo base_url('/assets/images/logo_col_2.png'); ?>" alt="Casamientos Online el Portal de los Novios" /></a>
			</div><!-- #logo_col -->
			
			<nav role="navigation" class="navbar navbar-default col-md-9 no_padding">
				<div class="navbar-header">
		            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		        </div>
		        <div id="navbarCollapse" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="<?php echo base_url('/empresas/como-anunciar'); ?>" <?=isset($tipo)&&$tipo=="como_anunciar"?'class="active"':''; ?>>Como Anunciar</a></li>
						<li><a href="<?php echo base_url('/empresas/nuestra-historia'); ?>" <?=isset($tipo)&&$tipo=="acerca_nuestro"?'class="active"':''; ?>>Acerca Nuestro</a></li>
						<li><a href="<?php echo base_url('/empresas/expo-novias'); ?>" <?=isset($tipo)&&$tipo=="expo_novias_proveedor"?'class="active"':''; ?>>Expo de Novias</a></li>
						<li><a href="<?php echo base_url('/empresas/publicaciones-prensa'); ?>" <?=isset($tipo)&&$tipo=="prensa_proveedor"?'class="active"':''; ?>>Prensa</a></li>
						<li><a href="<?php echo base_url('/empresas/contacto'); ?>" <?=isset($tipo)&&$tipo=="contacto_proveedor"?'class="active"':''; ?>>Contacto</a></li>
					</ul>
				</div><!-- #navbarCollapse -->
			</nav>
		</div><!-- .row -->
		<div class="clear"></div>
	</div><!-- .inner -->
</header>