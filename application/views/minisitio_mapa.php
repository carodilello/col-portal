<?php
$bodyclass = 'page minisitio';
include('header.php'); 
$scripts_javascript = array(
	'<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAbSuwVNqfo1GeWu1Q7r-MHAUs9gb88O_w"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/markerclusterer.js') . '"></script>',
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		include('mod_header_minisitio.php'); ?>
		<section id="wrapper_minisitio" class="row">
			<div id="content_minisitio" class="col-md-8">
				<div id="mapa_minisitio" class="box clear big">
					<h2>Ubicación de <?php echo $proveedor['proveedor']; ?>
						<div class="actions"><span>Cambiar vista: </span>
							<a href="javascript:;" class="vista-roadmap"><i class="fa fa-map-marker"></i></a>
							<a href="javascript:;" class="vista-street_view"><i class="fa fa-male"></i></a>
						</div>
					</h2>
					<div class="box_inner" id="content_listado_mapa">
						<span style="display:none;" id="id_minisitio"><?php echo $proveedor['id_minisitio']; ?></span>
						<span style="display:none;" id="minisitio_title"><?php echo $proveedor['titulo']; ?></span>
						<p class="direccion"><?php if(isset($dir['strong'])){ ?><strong><?php echo $dir['strong']; ?></strong><?php } ?>
							<?php if(isset($dir['normal']) && $dir['normal']) foreach ($dir['normal'] as $nor){
								echo ', '.trim($nor);
							} ?></p>
						<?php if($proveedor['latitud'] && $proveedor['longitud']){ ?>
							<div class="roadmap" id="content_listado_mapa_map" data-gmap data-latitude='<?php echo $proveedor['latitud']; ?>' data-longitude='<?php echo $proveedor['longitud']; ?>' style='height: 500px;'></div>
							<div style="display:none; height: 500px;" class="street_view" data-gmap data-latitude='<?php echo $proveedor['latitud']; ?>' data-longitude='<?php echo $proveedor['longitud']; ?>'></div>
						<?php } ?>		
					</div>
				</div><!-- #mapa_minisitio -->
			</div><!-- #content_minisitio -->
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php include('minisitio_acciones.php'); ?>
				<?php include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->
			<div class="col-md-12">
				<?php include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->
		</section><!-- #wrapper_minisitio -->
	</div><!-- .container -->
	<?php 
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php include('footer.php'); ?>