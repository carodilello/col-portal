<section id="newsletter">
	<div class="inner">
		<div class="">
			<?php if($registro_infonovia_corto){ ?>
				<p class="col-md-7 validate"><i class="fa fa-check-circle-o"></i>Gracias! tu email ya fue registrado en nuestra base de datos.</p>
			<?php }else{ ?>
				<p class="col-md-7">Suscribite al Infonovias para recibir ofertas y novedades</p>
			<?php } ?>
			<div class="col-md-5">
				<?php 
				echo form_open(base_url('/formulario/registro_infonovias'),' method="POST" role="form" class="form"'); ?>
					<input type="hidden" name="redirect" value="<?php echo base_url($_SERVER['REQUEST_URI']); ?>#newsletter">
					<input type="hidden" name="registro_corto" value="1" />
					
					<div class="col form-group has-feedback sucursal">
						<select name="id_sucursal" class="form-control" required>
							<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
								<option value="<?php echo $suc["id"]; ?>" <?=$sucursal['id']==$suc["id"]?'selected':'';?>><?=$suc["sucursal"];?></option>
							<?php } ?>
						</select>
					</div>
					
					<div class="col form-group has-feedback">
						<input type="email" name="email" placeholder="Ingresá tu Email" required class="form-control" />
						<span class="fa form-control-feedback" aria-hidden="true"></span>
					</div>
		
					<input class="col" type="submit" value="" />
				</form>
				<div class="clear"></div>
			</div><!-- .col-md-3 -->
		</div><!-- .row -->
	</div><!-- .inner -->
	<div class="clear"></div>
</section><!-- #newsletter  -->