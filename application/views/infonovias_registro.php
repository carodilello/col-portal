<?php
$bodyclass = 'contacto registro_infonovias';
include('header.php'); ?>
<div class="container">
<ul id="breadcrumbs">
	<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
	<li><a href="<?php echo base_url('/infonovias'); ?>">Infonovias</a></li>
	<li>Registro</li>
</ul>
<h1 class="title_sep">Registrarse al Infonovias en 
	<strong class="dropdown">
		<a class="green suc_selector" id="sucursales-nav-label-top" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=isset($sucursal["sucursal"])&&$sucursal["sucursal"]?$sucursal["sucursal"]:"Buenos Aires";?><i class="fa fa-angle-down"></i></a>
		<ul class="dropdown-menu dropdown_sucursal" aria-labelledby="sucursales-nav-label-top">
			<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
				<a class="block" href="<?php echo base_url('/proveedores/sucursal/'.$suc["id"].'/?redirect=' . (!empty($sucursal_seo) ? ($suc["nombre_seo"]) : '') . (isset($sucursal_redirect)&&$sucursal_redirect?($sucursal_redirect):'')); ?>#grid_home"><?=$suc["sucursal"];?></a>
			<?php } ?>
		</ul>
	</strong>
</h1>

<?php echo form_open(base_url('/infonovias/gracias#gracias'), 'method="POST" role="form" class="form" id="form_presupuesto_wrapper"'); ?>
	<input type="hidden" name="id_origen" value="0">
	<aside class="col-md-3">
		<h3>¿Qué es el Infonovias?</h3>
		<img src="<?php echo base_url('/assets/images/pic_infonovias.jpg'); ?>" />
		<p class="pink gum"><strong>Los infonovias son nuestro Medio de contacto!</strong></p>
		<p class="gum">A través de nuestros newsletters vas a recibir novedades, notas, promociones, invitaciones, a eventos, y muchos más...</p>
		<p><strong>Registrate para recibirla!</strong></p>
	</aside>
	<div id="form_presupuesto" class="col-md-9">
		<?php if(validation_errors()){ ?>
			<div class="errores-generales">
				<div><i class="fa fa-exclamation-circle"></i> 
					<?php echo validation_errors(); ?>
				</div>	
			</div> <!-- .errores-generales -->	
		<?php } ?>
	
		<fieldset class="row no_margin_b">
			<label class="col-md-12">Datos del Casamiento</label>
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-user"></span>
				<input type="text" name="dia_evento" placeholder="Fecha evento" value="<?php echo $data_form['dia_evento']; ?>" class="form-control date" pattern="<?php echo $er_fecha; ?>" data-pattern-error="Ingrese una fecha válida" data-remote="/mayor_hoy" required />
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-user"></span>
				<input type="text" name="invitados" placeholder="Cantidad de Invitados"  value="<?php echo $data_form['invitados']?$data_form['invitados']:set_value('invitados'); ?>" class="form-control" required>
				<div class="help-block with-errors"></div>
			</div>

			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-map-o"></span>
				<select name="id_sucursal" required class="form-control">
					<option value="">Sucursales</option>
					<?php if(isset($sucursales) && $sucursales) foreach ($sucursales as $suc){ ?>
						<option value="<?php echo $suc["id"]; ?>"  <?=set_value('id_sucursal')==$suc["id"]||(!set_value('id_provincia')&&$suc["id"]==$data_form['id_sucursal'])?'selected':'';?>><?php echo $suc["sucursal"]; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>
		</fieldset>
	
					
		<fieldset class="row no_margin_b">
			<label class="col-md-12">Datos de contacto</label>
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-user"></span>
				<input type="text" name="nombre" placeholder="Nombre" value="<?php echo $data_form['nombre']?$data_form['nombre']:set_value('nombre'); ?>" class="form-control" required>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-user"></span>
				<input type="text" name="apellido" placeholder="Apellido" value="<?php echo $data_form['apellido']?$data_form['apellido']:set_value('apellido'); ?>" class="form-control" required>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-phone"></span>
				<input type="text" name="telefono" placeholder="Teléfono" value="<?php echo $data_form['telefono']?$data_form['telefono']:set_value('telefono'); ?>" class="form-control" required pattern="^[0-9() -]*$" data-pattern-error="Ingrese un número telefónico válido" />
				<div class="help-block with-errors"></div>
			</div>
		</fieldset>
		
		<fieldset class="row no_margin_b">
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-envelope"></span>
				<input type="email" name="email" placeholder="Email" value="<?php echo $data_form['email']?$data_form['email']:set_value('email'); ?>" class="form-control" required />
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-map-marker"></span>
				<select name="id_provincia" required class="form-control provincias">
					<option value="">Provincia</option>
					<?php if($provincias) foreach ($provincias as $k => $provincia){ ?>
						<option value="<?php echo $k; ?>"  <?=set_value('id_provincia')==$k||(!set_value('id_provincia')&&$k==$data_form['id_provincia'])?'selected':'';?>><?php echo $provincia; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>
			
			<div class="col-md-4 input_icon form-group has-feedback">
				<span class="fa fa-location-arrow"></span>
				<select name="id_localidad" required class="form-control localidades">
					<option value="">Localidad</option>
					<?php if($localidades) foreach ($localidades as $k => $localidad){ ?>
						<option value="<?php echo $k; ?>"  <?=set_value('id_localidad')==$k||(!set_value('id_localidad')&&$k==$data_form['id_localidad'])?'selected':'';?>><?php echo $localidad; ?></option>
					<?php } ?>
				</select>
				<div class="help-block with-errors"></div>
			</div>
		</fieldset>

		<div class="clear" style="border-top:1px solid #ddd; padding-top: 20px;">
			<input type="submit" class="btn-default submit_presupuesto disabled" value="Registrarse al Infonovias">
			<div class="has-error has-danger has-feedback">
				<div style="display:none; float:right;" class="error-zonas help-block with-errors">
					<ul class="list-unstyled">
						<li>Debe seleccionar al menos una zona</li>
					</ul>
				</div>
			</div>
		</div><!-- .clear -->
	</div>
</form>
</div><!-- container -->
<?php include('footer.php'); ?>