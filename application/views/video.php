<?php $str_mas = '';
$str_mas_prov = '';
$cant_corte_proveedor = !isset($cant_corte_proveedor)?30:$cant_corte_proveedor;
$cant_corte_titulo    = !isset($cant_corte_titulo)?23:$cant_corte_titulo;
foreach ($videos_all as $k => $video){
	if($video['video_prov']){
		$tipo = 'empresas';
	}else{
		$tipo = 'casamientos';
	}
	$nombre_proveedor = '';
	if($video['proveedores']){
		$tmp_proveedor = explode('@', $video['proveedores']);
		$nombre_proveedor = $tmp_proveedor[1];
		$id_proveedor = $tmp_proveedor[2]; 	
	}
	switch ($tipo) {
		case 'empresas':
			$codigo = 'e';
			break;
		case 'casamientos':
			$codigo = 'c';
			break;
	}
	if(strlen($video['titulo']) >= $cant_corte_titulo)       $str_mas = '...';
	if(strlen($nombre_proveedor) >= $cant_corte_proveedor) $str_mas_prov = '...'; ?>
	<div class="col-md-4 col">
		<a href="<?php echo base_url('/casamientos-tv/' . $video["titulo_seo"] . '_CO_v' . $video["id"] . '_' . $codigo); ?>" class="thumb"><img src="http://img.youtube.com/vi/<?php echo $video['video_id']; ?>/0.jpg" alt="<?php echo $video['titulo']; ?>" /></a>
		<h3><a <?php echo strlen($video['titulo']) >= $cant_corte_titulo ? 'data-toggle="tooltip" data-placement="top"' : ''; ?> title="<?php echo $video['titulo']; ?>" href="<?php echo base_url('/casamientos-tv/' . $video["titulo_seo"] . '_CO_v' . $video["id"] . '_' . $codigo); ?>"><?php echo substr($video['titulo'], 0, $cant_corte_titulo).$str_mas; ?></a></h3>
		<?php if($video['proveedores']){ 
			$tmp = explode('@', $video['proveedores']); 
			if(!empty($tmp[9])){ ?>
				<a href="<?php echo str_replace('{?}', $tmp[9], $base_url_subdomain) . $tmp[8]; ?>"><span class="proveedor_rubro"><strong><?php echo substr($nombre_proveedor, 0, $cant_corte_proveedor).$str_mas_prov; ?></strong></span></a>
			<?php }
		} ?>
	</div>
<?php $str_mas = '';
	$str_mas_prov = '';
} ?>