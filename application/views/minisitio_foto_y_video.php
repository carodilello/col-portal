<?php
$bodyclass = 'page minisitio';
include('header.php'); 
$scripts_javascript = array(
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_video.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>'
); ?>
<div class="bg_grey">
	<div class="container">
		<?php include('breadcrumbs.php');
		include('mod_header_minisitio.php'); ?>
		
		<section id="wrapper_minisitio" class="row">
		
			<div id="content_minisitio" class="col-md-8">
				<div id="galeria_container" class="box">
					<h2>Fotos y Videos de <?php echo $proveedor['proveedor']; ?></h2>
					<?php if($fotos || $videos){ ?>
						<div class="box_inner">
							<div class="single galeria_standard">
								<div class="big_pic carousel slide" id="carousel-minisitio" data-ride="carousel" data-pause="hover" data-interval="false">
									<!-- Controls -->
									<a class="left carousel-control carousel-control-smaller" href="#carousel-minisitio" role="button" data-slide="prev">
										<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
										<span class="sr-only valign">Previous</span>
									</a>
									<a class="right carousel-control carousel-control-smaller" href="#carousel-minisitio" role="button" data-slide="next">
										<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
									
									<div class="carousel-inner" role="listbox">
										<?php if($videos){
											foreach ($videos as $k => $video){ ?>
												<div class="align_c item item_video <?=$k==0?'active':'';?>">
										      		<iframe src="http://www.youtube.com/embed/<?php echo $video['item'];?>?wmode=opaque&amp;rel=0&amp;enablejsapi=1" class="iframe_styled_smaller ytplayer"  allowfullscreen="" id="player<?php echo $k; ?>"></iframe>
										      		<div class="epigrafe">
														<span class="col-md-10"><?php echo $video['titulo']; ?></span>
														<!--<a class="col-md-2 hvr-icon-wobble-vertical" href="javascript:;">Guardar</a>-->
													</div>
									    		</div> 	
										<?php }
										}
										foreach ($fotos as $k => $foto){ ?>
											<div class="align_c item <?=$k==0&&!$videos?'active':'';?>">
												<a href="http://media.casamientosonline.com/images/<?php echo $foto['imagen_gde']; ?>" data-gallery title='<?php echo $foto['titulo']; ?>'
												data-urlminisitio="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto['rubro_seo']; ?>"
												data-url="<?php echo str_replace('{?}', $foto['subdominio'], $base_url_subdomain) . $foto["rubro_seo"] . '/presupuesto-foto_CO_r'.$rubro["id"].'_fo'.$foto['id']; ?>">
										      		<?php if($foto['titulo']){ ?>
										      			<img alt="<?php echo $foto['titulo']; ?> | Casamientos Online" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" src="http://media.casamientosonline.com/images/<?php echo $foto['imagen_gde']; ?>" />
										      		<?php }else{ ?>
										      			<img alt="Imagen de <?php echo $proveedor['proveedor']; ?> sobre <?php echo $proveedor['rubro']; ?> | Casamientos Online" onError="this.onerror=null;this.src=$('.base_url').val() + 'assets/images/pic_default.jpg';" src="http://media.casamientosonline.com/images/<?php echo $foto['imagen_gde']; ?>" />
										      		<?php } ?>
										    	</a>
									      		<div class="epigrafe">
													<span class="col-md-10">Vista del Salón Principal</span>
													<!--<a class="col-md-2 hvr-icon-wobble-vertical" href="javascript:;">Guardar</a>-->
												</div>
									    	</div>	
										<?php } ?>
									</div>
									
								</div><!-- #carousel-minisitio -->
								
								<div class="thumbs">
									<?php 
									$j = 0;
									if($videos){
										foreach ($videos as $k => $video){ ?>
											<a href="javascript:;" class="thumb_c thumb_video"><img class="col-md-2" data-target="#carousel-minisitio" data-slide-to="<?php echo $k; ?>" src="<?php echo $video['thumb']?$video['thumb']:('http://img.youtube.com/vi/'.$video['item'].'/0.jpg'); ?>" /></a>
									<? $j += 1; }
									}
									foreach ($fotos as $k => $foto){
										$k = $k+$j; ?>
										<a href="javascript:;" class="thumb_c"><img class="col-md-2" data-target="#carousel-minisitio" data-slide-to="<?php echo $k; ?>" src="http://media.casamientosonline.com/images/<?php echo $foto['imagen']; ?>" /></a>
									<?php } ?>
								</div><!-- .thumbs -->							
							</div><!-- #galeria_standard -->
						</div><!-- .box_inner -->
					<?php } ?>
				</div><!-- .box -->	
			</div><!-- #content_minisitio -->	
			
			<aside id="sidebar_minisitio" class="col-md-4">
				<?php include('minisitio_acciones.php'); ?>
			
				<?php include('minisitio_formulario.php'); ?>
			</aside> <!-- #sidebar_minisitio -->
			
			<div class="col-md-12">
				<?php include('mod_resumen_minisitio.php'); ?>
			</div><!-- .col-md-12 -->
	
			

		</section><!-- #wrapper_minisitio -->
		
	</div><!-- .container -->
	<?php 
	include('listado_proveedores_carousel.php');
	include('mod_listado_proveedores_footer.php'); ?>
</div>
<?php 
include('popup_fotos.php');
include('footer.php'); ?>