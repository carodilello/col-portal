<?php
$bodyclass = 'page jornadas_anteriores jornadas_registro';
include('header.php');

$slides = array();
if(isset($content[0]['galeria'][0]) && $content[0]['galeria'][0]){ // PRIMERA GALERIA
	$tmp_slides = explode('~', $content[0]['galeria'][0]['thumbs']);

	if(!empty($tmp_slides)) foreach ($tmp_slides as $k => $slide){
		$tmp_data_slide = explode('@', $slide);
		$slides[] = $tmp_data_slide;
	}
	$content[0]['galeria'] = $slides;
}

$slides = array();
if(isset($content[1]['galeria'][0]) && $content[1]['galeria'][0]){ // SEGUNDA GALERIA
	$tmp_slides = explode('~', $content[1]['galeria'][0]['thumbs']);

	if(!empty($tmp_slides)) foreach ($tmp_slides as $k => $slide){
		$tmp_data_slide = explode('@', $slide);
		$slides[] = $tmp_data_slide;
	}
	$content[1]['galeria'] = $slides;
}

$scripts_javascript = array(
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/jquery.blueimp-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/gallery/bootstrap-image-gallery.min.js') . '"></script>',
	'<script type="text/javascript" src="' . base_url('/assets/js/funciones_redes.js') . '"></script>',
	'<script async defer src="//assets.pinterest.com/js/pinit.js"></script>'
); ?>
<div class="container">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url('/' . $sucursal['nombre_seo']); ?>">Home</a></li>
		<li><a href="<?php echo base_url('/eventos'); ?>">Eventos</a></li>
		<li><a href="<?php echo base_url('/eventos/expo-novias-' . date('Y')); ?>">Jornadas</a></li>
		<li>Jornadas anteriores</li>
	</ul>
	<div class="row">
		<div class="content col-md-8">
			<h1 class="title_sep">Jornadas Anteriores</h1>
			
			<div id="ultima_jornada">
				<?php if(!empty($content)) foreach ($content as $k => $jornada){ ?>
					<div class="jornada_wrapper">
						<h2><?php echo $jornada['titulo']; ?></h2>
						
						<?php if(isset($jornada['video']) && $jornada['video']){ ?>
							<iframe width="560" height="315" src="<?php echo $jornada['video']; ?>" frameborder="0" allowfullscreen=""></iframe>
						<?php } ?>
						
						<p class="gum_40"><?php echo $jornada['texto']; ?></p>

						<?php
						if(!empty($jornada['galeria']) && !in_array($jornada['galeria'][0][1], array('mpeg'))){ ?>
							<h3 class="title_sep">Galería de Imagenes</h3>
							<div class="galeria_standard">	
								<div class="big_pic carousel slide" id="carousel-jornadas-<?php echo $k; ?>" data-ride="carousel" data-pause="hover" data-interval="false">
									<div class="carousel-inner" role="listbox">
										<?php foreach ($jornada['galeria'] as $key => $slide){
											if(isset($slide[0]) && isset($slide[1]) && isset($slide[2])){ ?>
												<div class="align_c item <?php echo $key == 0 ? 'active' : '' ?>">
											 		<a href="http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>" class="item_minis" data-gallery title="<?php echo $slide[2]; ?>">
												    	<img src="http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>">
												    </a>
												    <div class="epigrafe">
														<span class="col-md-10"><?php echo isset($slide[3]) && $slide[3] ? $slide[3] : $slide[2]; ?></span>
														<div class="social col-md-2">
															<input type="hidden" class="id_redes" value="eventos/expo-novias-<?php echo $anio; ?>">
															<a class="compartir_facebook no_margin" href="javascript:;"><i class="fa fa-facebook"></i></a>
															<a class="twitter" href="https://twitter.com/intent/tweet?url=<?php echo base_url('eventos/expo-novias-' . $anio); ?>&text=<?php echo urlencode($slide[2]);?>"><i class="fa fa-twitter"></i></a>
															<a class="boton-pinterest" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo base_url('eventos/expo-novias-'. $anio); ?>&description=<?php echo $slide[2];?>&media=http://media.casamientosonline.com/images/<?php echo $slide[0] . '.' . $slide[1]; ?>" data-pin-custom="true"><i class="fa fa-pinterest"></i></a>
														</div>
													</div>
												</div>
											<?php }
										} ?>
									</div><!-- .carousel-inner -->		
								</div><!--  .big_pic -->	
								<!-- Controls -->
								<a class="left carousel-control" href="#carousel-jornadas-<?php echo $k; ?>" role="button" data-slide="prev">
									<span class="fa fa-chevron-left valign" aria-hidden="true"></span>
									<span class="sr-only valign">Previous</span>
								</a>
								<a class="right carousel-control" href="#carousel-jornadas-<?php echo $k; ?>" role="button" data-slide="next">
									<span class="fa fa-chevron-right valign" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
								
							</div><!-- .galeria_standard -->
						<?php } ?>
					</div><!-- .jornada_wrapper -->	
				<?php } ?>
			</div><!-- #ultima_jornada -->
		</div><!-- .content -->
		
		<aside class="col-md-4">
			<?php include('jornadas_formulario.php'); ?>
			
			<div id="jornadas_anteriores">
				<?php include('jornadas_anteriores_listado.php'); ?>
			</div>
		</aside>
	</div><!-- .row -->
</div><!-- .container -->
<?php 
$no_presupuesto_foto = TRUE;
$no_presupuesto = TRUE;
$flechas = TRUE;
include('popup_fotos.php');
include('footer.php'); ?>