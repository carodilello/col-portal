<?php if($paginas > 1){ ?>
	<div class="paginator">
		<div class="pull-left">
			<span>Página: <strong><?php echo $pagina; ?></strong> de <strong><?php echo $paginas; ?></strong></span>
		</div>
		<div class="pull-right">
			<?php if($pagina > 1){ ?>
				<a title="Anterior" href="<?php 
					if(strpos('//',$url_paginacion)){
						$url_ant = str_replace('/[pag]',$pagina-1,$url_paginacion);
					}else{
						$url_ant = $url_paginacion;
					}
					$url_ant = str_replace('[pag]',$pagina-1,$url_ant);
					if(!empty($rubro['url'])) $url_ant = str_replace('_?_', $rubro['url'], $url_ant);
					echo $url_ant;
				?>"><span><i class="fa fa-caret-left"></i></span></a>
			<?php } ?>

			<?php
			for ($i = $desde; $i <= $hasta; $i++){ ?>
				<a title="<?php echo $i; ?>" 
					<?php if($pagina == $i){
						echo 'class="active"';
					}else{
						if(strpos('//',$url_paginacion)){
							$url_pag = str_replace('/[pag]',$i,$url_paginacion);
						}else{
							$url_pag = $url_paginacion;
						}
						$url_pag = str_replace('[pag]', $i, $url_pag);
						if(!empty($rubro['url'])) $url_pag = str_replace('_?_', $rubro['url'], $url_pag);
						echo 'href="' . $url_pag . '"';
					} ?>><?php echo $i; ?></a>
			<?php } ?>

			<?php if($pagina < $paginas){ ?>
				<a title="Siguiente" href="<?php 
					if(strpos('//',$url_paginacion)){
						$url_next = str_replace('/[pag]',$pagina+1,$url_paginacion);
					}else{
						$url_next = $url_paginacion;
					}
					$url_next = str_replace('[pag]',$pagina+1,$url_next);
					if(!empty($rubro['url'])) $url_next = str_replace('_?_', $rubro['url'], $url_next);
					echo $url_next;
				?>"><span><i class="fa fa-caret-right"></i></span></a>
			<?php } ?>
		</div>
	</div><!-- .paginator -->
<?php } ?>