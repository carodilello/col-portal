<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nav_hook{
    private $CI;

    public function __construct(){
        $this->CI = &get_instance();
    }

    public function get_nav(){
        $this->CI->load->model('navegacion_model');
        $this->CI->load->model('generales_model');

        $this->CI->load->library('config_library');
        $this->CI->load->library('parseo_library');

        $data = array();
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){ // Que no haga nada si es por ajax
            $data['base_url_subdomain'] = 'http://{?}.' . DOMAIN . '/';
        }else{
            // Con este algoritmo cambiamos la variable de sesión de las sucursales para la lógica de SEO
            $sucursales = array(
                1   => 'buenos-aires',
                4   => 'cordoba',
                10  => 'entre-rios',
                7   => 'la-plata',
                6   => 'mar-del-plata',
                5   => 'mendoza',
                12  => 'misiones',
                2   => 'rosario',
                11  => 'salta',
                3   => 'santa-fe',
                8   => 'tucuman'
            );

            $cambio_sucursal = FALSE;
            if(in_array($this->CI->uri->segment(1), $sucursales)){
                $nombre_sucursal = $this->CI->uri->segment(1);
                $cambio_sucursal = TRUE;
            }

            if($cambio_sucursal) $this->CI->config_library->cambiar_sucursal(array_search($nombre_sucursal, $sucursales));

            // FIN ALGORITMO SEO

            $nav_el = $this->CI->navegacion_model->get_elementos($this->CI->session->userdata('id_sucursal'));
            $doble_columna = $this->CI->generales_model->get_doble_columna($this->CI->session->userdata('id_sucursal'));
            $rubros_reordenados = $this->CI->generales_model->get_buscador_estructura($this->CI->session->userdata('id_sucursal'));
            $rubros_reordenados_nav = $this->CI->generales_model->get_estructura_nav($this->CI->session->userdata('id_sucursal'));

            $data = array(
                'nav'                       => $nav_el,
                'doble_columna'             => $doble_columna,
                'rubros_reordenados'        => $rubros_reordenados,
                'rubros_reordenados_nav'    => $rubros_reordenados_nav,
                'mostrar_banners'           => $this->CI->config->item('mostrar_banners'),
                'base_url_subdomain'        => 'http://{?}.' . DOMAIN . '/'
            );
        }
        $this->CI->load->vars($data);
    }
}