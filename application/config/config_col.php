<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['id_sucursal']    = 1;
$config['medir_banners']  = 1;
$config['medir_stats'] 	  = 0;
$config['paginas_margen'] = 2;

$config['mantenimiento_site'] 		 = FALSE;
$config['mantenimiento_presupuesto'] = FALSE;

$config['youtube_thumb'] = 'http://img.youtube.com/vi/{_id_}/2.jpg';

$config['cache_expiration'] = 3600; // 2 horas

$config['procesos_off'] 	= FALSE; // PONER "TRUE" PARA ETAPA DE DESARROLLO O PRUEBAS
$config['mostrar_banners'] 	= FALSE; // PONER "TRUE" PARA MOSTRAR UBICACIONES DE BANNERS