<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'generales/pagina_404';
$route['translate_uri_dashes'] = false;

$route['mayor_hoy'] = 'ajax/check_fecha_mayor_hoy';

// Formulario de contacto
$route['contacto']  = 'proveedores/contacto_proveedor/contacto';


// generador de token para instagram
$route['get_instagram_access_token']  = 'home/get_instagram_access_token';

/*


OPTIMIZACIÓN PARA SEO


*/

// HOME
$route['buenos-aires']  = 'home';
$route['cordoba'] 		= 'home';
$route['entre-rios'] 	= 'home';
$route['mendoza'] 		= 'home';
$route['mar-del-plata'] = 'home';
$route['la-plata'] 		= 'home';
$route['misiones'] 		= 'home';
$route['rosario'] 		= 'home';
$route['salta'] 		= 'home';
$route['santa-fe'] 		= 'home';
$route['tucuman'] 		= 'home';

// AJAX NAVEGADOR PRINCIPAL
$route['ajax/get_menu_sugerencias/buenos-aires']  	= 'ajax/get_menu_sugerencias/1';
$route['ajax/get_menu_sugerencias/cordoba'] 		= 'ajax/get_menu_sugerencias/4';
$route['ajax/get_menu_sugerencias/entre-rios'] 		= 'ajax/get_menu_sugerencias/10';
$route['ajax/get_menu_sugerencias/mendoza'] 		= 'ajax/get_menu_sugerencias/5';
$route['ajax/get_menu_sugerencias/mar-del-plata'] 	= 'ajax/get_menu_sugerencias/6';
$route['ajax/get_menu_sugerencias/la-plata'] 		= 'ajax/get_menu_sugerencias/7';
$route['ajax/get_menu_sugerencias/misiones'] 		= 'ajax/get_menu_sugerencias/12';
$route['ajax/get_menu_sugerencias/rosario'] 		= 'ajax/get_menu_sugerencias/2';
$route['ajax/get_menu_sugerencias/salta'] 			= 'ajax/get_menu_sugerencias/11';
$route['ajax/get_menu_sugerencias/santa-fe'] 		= 'ajax/get_menu_sugerencias/3';
$route['ajax/get_menu_sugerencias/tucuman'] 		= 'ajax/get_menu_sugerencias/8';

$route['ajax/sugerencias_buscador_home/(:any)/buenos-aires']  	= 'ajax/sugerencias_buscador_home/$1/1/buenos-aires';
$route['ajax/sugerencias_buscador_home/(:any)/cordoba'] 		= 'ajax/sugerencias_buscador_home/$1/4/cordoba';
$route['ajax/sugerencias_buscador_home/(:any)/entre-rios'] 		= 'ajax/sugerencias_buscador_home/$1/10/entre-rios';
$route['ajax/sugerencias_buscador_home/(:any)/mendoza'] 		= 'ajax/sugerencias_buscador_home/$1/5/mendoza';
$route['ajax/sugerencias_buscador_home/(:any)/mar-del-plata'] 	= 'ajax/sugerencias_buscador_home/$1/6/mar-del-plata';
$route['ajax/sugerencias_buscador_home/(:any)/la-plata'] 		= 'ajax/sugerencias_buscador_home/$1/7/la-plata';
$route['ajax/sugerencias_buscador_home/(:any)/misiones'] 		= 'ajax/sugerencias_buscador_home/$1/12/misiones';
$route['ajax/sugerencias_buscador_home/(:any)/rosario'] 		= 'ajax/sugerencias_buscador_home/$1/2/rosario';
$route['ajax/sugerencias_buscador_home/(:any)/salta'] 			= 'ajax/sugerencias_buscador_home/$1/11/salta';
$route['ajax/sugerencias_buscador_home/(:any)/santa-fe'] 		= 'ajax/sugerencias_buscador_home/$1/3/santa-fe';
$route['ajax/sugerencias_buscador_home/(:any)/tucuman'] 		= 'ajax/sugerencias_buscador_home/$1/8/tucuman';

// GUIA DE EMPRESAS (AHORA FIESTAS DE CASAMIENTO)
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/fiestas-de-casamiento']	= 'proveedores/guia';

// PRE RUBROS (BUENOS AIRES Y CORDOBA POR EL MOMENTO)
$route['buenos-aires/zonas/(:any)_CO_r(:num)']	= 'proveedores/mapa_de_guia/$2';
$route['cordoba/zonas/(:any)_CO_r(:num)']  		= 'proveedores/mapa_de_guia/$2';

//
// LISTADO EMPRESAS
//

// SIN FILTROS
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/proveedor/(:any)_CO_r(:num)']	= 'proveedores/listado_empresas/$3';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/proveedor/(:any)_CO_r(:num)/gracias']	= 'proveedores/gracias_externo/$3/0/empresas/1';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/proveedor/_CO_r(:num)']		= 'proveedores/listado_empresas/$2';

// CON FILTROS (CON O SIN PAGINACION)
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/proveedor/(:any)_CO_r(:num)_(:any)']	= 'proveedores/listado_empresas/$3/$4';

// SIN FILTROS Y EN SOLAPA
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/proveedor/(:any)_CO_t(promociones|productos|paquetes|fotos|mapa)_r(:num)']	= 'proveedores/listado_empresas/$4/0/$3';

// CON FILTROS (CON O SIN PAGINACION) Y EN SOLAPA
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/proveedor/(:any)_CO_t(promociones|productos|paquetes|fotos|mapa)_r(:num)_(:any)']	= 'proveedores/listado_empresas/$4/$5/$3';

// PRESUPUESTO A RUBRO
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/(:any)/solicitar-presupuesto-multiple_CO_r(:num)_t11']	= 'formulario/index/presupuesto/resultados/$3/11';

$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/(:any)/solicitar-presupuesto-multiple_CO_r(:num)_t13']	= 'formulario/index/presupuesto/resultados/$3/13';

//creada 05-06-2017
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/(:any)/solicitar-presupuesto-multiple_CO_r(:num)/infonovias']	= 'formulario/index/presupuesto/resultados/$3/13';

$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/(:any)/solicitar-presupuesto-multiple_CO_r(:num)_t(:num)_(promocion|producto|paquete|foto|mapa)']	= 'formulario/index/presupuesto/resultados/$3/$4/0/$5';


//
// MINISITIO
//
if($_SERVER['HTTP_HOST'] == BASE || $_SERVER['HTTP_HOST'] == 'www.' . BASE){
// if($_SERVER['HTTP_HOST'] == BASE){
	$route['(:any)_CO_m(:num)']							= 'proveedores/minisitio/$2';
	$route['(:any)/productos-y-promos_CO_m(:num)']		= 'proveedores/minisitio_promociones_productos/$2';
	$route['(:any)/fotos-y-videos_CO_m(:num)']			= 'proveedores/minisitio_foto_video/$2';
	$route['(:any)/paquetes_CO_m(:num)']				= 'proveedores/minisitio_paquetes/$2';
	$route['(:any)/ubicacion_CO_m(:num)']				= 'proveedores/minisitio_mapa/$2';
	$route['(:any)/testimonios_CO_m(:num)']				= 'proveedores/minisitio_testimonios/$2';
	$route['(:any)/preguntas-frecuentes_CO_m(:num)']	= 'proveedores/minisitio_preguntas_frecuentes/$2';
}else{ // SUBDOMINIO
	$tmp = explode('.',$_SERVER['HTTP_HOST']);

	$route['default_controller'] 						= 'proveedores/minisitio';

	if($tmp[0] == 'buscador'){
		// BUSQUEDA
		$route['buenos-aires/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_'] 	= 'home/buscar/1/$2/$1';
		$route['buenos-aires/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)'] 		= 'home/buscar/1/$2/$1';

		$route['cordoba/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  		= 'home/buscar/4/$2/$1';
		$route['cordoba/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  			= 'home/buscar/4/$2/$1';

		$route['entre-rios/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  	= 'home/buscar/10/$2/$1';
		$route['entre-rios/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  		= 'home/buscar/10/$2/$1';

		$route['mendoza/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  		= 'home/buscar/5/$2/$1';
		$route['mendoza/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  			= 'home/buscar/5/$2/$1';

		$route['mar-del-plata/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_'] = 'home/buscar/6/$2/$1';
		$route['mar-del-plata/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)'] 	= 'home/buscar/6/$2/$1';

		$route['la-plata/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  	= 'home/buscar/7/$2/$1';
		$route['la-plata/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  		= 'home/buscar/7/$2/$1';

		$route['misiones/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  	= 'home/buscar/12/$2/$1';
		$route['misiones/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  		= 'home/buscar/12/$2/$1';

		$route['rosario/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  		= 'home/buscar/2/$2/$1';
		$route['rosario/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  			= 'home/buscar/2/$2/$1';

		$route['salta/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  		= 'home/buscar/11/$2/$1';
		$route['salta/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  			= 'home/buscar/11/$2/$1';

		$route['santa-fe/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  	= 'home/buscar/3/$2/$1';
		$route['santa-fe/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  		= 'home/buscar/3/$2/$1';

		$route['tucuman/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)_CO_']  		= 'home/buscar/8/$2/$1';
		$route['tucuman/(todos|proveedores|productos|promociones|paquetes|editorial)/(:any)']  			= 'home/buscar/8/$2/$1';
	}elseif($tmp[0] == 'blogdelanovia' || $tmp[0] == 'weddingplanner' || $tmp[0] == 'reciencasada' || $tmp[0] == 'hacerfamilia'){
		$route['default_controller'] = 'editorial/to_detalle';
		// creada 08-06-2017
		$route['(:any)/(:any)/(:any)/(:any)'] = 'editorial/to_detalle';
	}else{
		$route['(:any)']									= 'proveedores/minisitio/0/' . $tmp[0];
		$route['(:any)/productos-y-promos']					= 'proveedores/minisitio_promociones_productos/0/' . $tmp[0];
		$route['(:any)/fotos-y-videos']						= 'proveedores/minisitio_foto_video/0/' . $tmp[0];
		$route['(:any)/paquetes']							= 'proveedores/minisitio_paquetes/0/' . $tmp[0];
		$route['(:any)/ubicacion']							= 'proveedores/minisitio_mapa/0/' . $tmp[0];
		$route['(:any)/testimonios']						= 'proveedores/minisitio_testimonios/0/' . $tmp[0];
		$route['(:any)/preguntas-frecuentes']				= 'proveedores/minisitio_preguntas_frecuentes/0/' . $tmp[0];
	}
}

$route['(:any)/(:any)_CO_pr(:num)']					= 'proveedores/producto/$3/promocion';
$route['(:any)/(:any)_CO_pd(:num)']					= 'proveedores/producto/$3/producto';
$route['(:any)/(:any)_CO_pa(:num)']					= 'proveedores/producto/$3/paquete';


// TODAS LAS PROMOS
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones/(la-fiesta|salones|vestidos|novias|novios|luna-de-miel)_CO_a(:num)'] 		= 'proveedores/pyp/promociones/$3';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones/(la-fiesta|salones|vestidos|novias|novios|luna-de-miel)_CO_a(:num)/(:num)']= 'proveedores/pyp/promociones/$3/0/$4';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones/(:any)_CO_a(:num)/(:num)-r(:num)']											= 'proveedores/pyp/promociones/$3/$5/$4/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones/(:any)_CO_a(:num)/(:num)-r(:num)-(:any)']									= 'proveedores/pyp/promociones/$3/$5/$4/$6/$2';

$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones_CO_pr']			= 'proveedores/pyp/promociones/0';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones_CO_pr/(:num)']		= 'proveedores/pyp/promociones/0/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones/(:any)_CO_pr/(:num)-r(:num)']												= 'proveedores/pyp/promociones/0/$4/$3/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/promociones/(:any)_CO_pr/(:num)-r(:num)-(:any)']										= 'proveedores/pyp/promociones/0/$4/$3/$5/$2';

// TODOS LOS PRODUCTOS
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos/(la-fiesta|salones|vestidos|novias|novios|luna-de-miel)_CO_a(:num)']			= 'proveedores/pyp/productos/$3';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos/(la-fiesta|salones|vestidos|novias|novios|luna-de-miel)_CO_a(:num)/(:num)']	= 'proveedores/pyp/productos/$3/0/$4';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos/(:any)_CO_a(:num)/(:num)-r(:num)']											= 'proveedores/pyp/productos/$3/$5/$4/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos/(:any)_CO_a(:num)/(:num)-r(:num)-(:any)']									= 'proveedores/pyp/productos/$3/$5/$4/$6/$2';

$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos_CO_pd']																		= 'proveedores/pyp/productos/0';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos_CO_pd/(:num)']																= 'proveedores/pyp/productos/0/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos/(:any)_CO_pd/(:num)-r(:num)']												= 'proveedores/pyp/productos/0/$4/$3/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/productos/(:any)_CO_pd/(:num)-r(:num)-(:any)']											= 'proveedores/pyp/productos/0/$4/$3/$5/$2';

// TODOS LOS PAQUETES
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes/(la-fiesta|salones|vestidos|novias|novios|luna-de-miel)_CO_a(:num)']			= 'proveedores/pyp/paquetes/$3';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes/(la-fiesta|salones|vestidos|novias|novios|luna-de-miel)_CO_a(:num)/(:num)']	= 'proveedores/pyp/paquetes/$3/0/$4';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes/(:any)_CO_a(:num)/(:num)-r(:num)']											= 'proveedores/pyp/paquetes/$3/$5/$4/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes/(:any)_CO_a(:num)/(:num)-r(:num)-(:any)']										= 'proveedores/pyp/paquetes/$3/$5/$4/$6/$2';

$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes_CO_pa']																		= 'proveedores/pyp/paquetes/0';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes_CO_pa/(:num)']																= 'proveedores/pyp/paquetes/0/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes/(:any)_CO_pa/(:num)-r(:num)']													= 'proveedores/pyp/paquetes/0/$4/$3/0/$2';
$route['(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/paquetes/(:any)_CO_pa/(:num)-r(:num)-(:any)']											= 'proveedores/pyp/paquetes/0/$4/$3/$5/$2';

//
// PEDIDO DE PRESUPUESTO
//
//creada 05-06-2017 para infonovias
$route['(:any)/presupuesto-empresa_CO_r(:num)_m(:num)/infonovias'] 	= 'formulario/index/presupuesto/empresa/$2/2/$3'; // infonovias

$route['(:any)/presupuesto-empresas_CO_r(:num)_m(:num)'] 	= 'formulario/index/presupuesto/empresa/$2/8/$3'; // EMPRESA (en listado) vista

$route['(:any)/presupuesto-empresa_CO_r(:num)_m(:num)']  	= 'formulario/index/presupuesto/empresa/$2/0/$3'; // EMPRESA
$route['(:any)/presupuesto-empresa_CO_r(:num)_m(:num)_2']  = 'formulario/index/presupuesto/empresa/$2/2/$3'; // EMPRESA. De news. Origen infonovias individual

$route['(:any)/presupuesto-paquetes_CO_r(:num)_pa(:num)'] 	= 'formulario/index/presupuesto/paquete/$2/35/$3'; 	// PAQUETE (en listado)
$route['(:any)/presupuesto-paquete_CO_r(:num)_pa(:num)'] 	= 'formulario/index/presupuesto/paquete/$2/40/$3'; 	// PAQUETE

$route['(:any)/presupuesto-productos_CO_r(:num)_pd(:num)'] 	= 'formulario/index/presupuesto/producto/$2/36/$3';	// PRODUCTO (en listado)
$route['(:any)/presupuesto-producto_CO_r(:num)_pd(:num)'] 	= 'formulario/index/presupuesto/producto/$2/38/$3';	// PRODUCTO

$route['(:any)/presupuesto-promociones_CO_r(:num)_pr(:num)']= 'formulario/index/presupuesto/producto/$2/36/$3';	// PROMOCION (en listado)
$route['(:any)/presupuesto-promocion_CO_r(:num)_pr(:num)']	= 'formulario/index/presupuesto/producto/$2/38/$3';	// PROMOCION

$route['(:any)/presupuesto-foto_CO_r(:num)_fo(:num)'] 		= 'formulario/index/presupuesto/foto/$2/23/$3';	// FOTOS

$route['(:any)/presupuesto-mapa_CO_r(:num)_ma(:num)'] 		= 'formulario/index/presupuesto/empresa/$2/25/$3';	// FOTOS

// PEDIDO PRESUPUESTO GENERAL
$route['solicitar-presupuesto-general'] = 'formulario/presupuesto_general';

//
// GALERIAS
//
$route['fotos-casamiento']		= 'galerias';
$route['fotos-casamiento_CO_']	= 'galerias';


$route['fotos-casamiento/(:num)/(:num)']					= 'galerias/index/$1/$2';
$route['fotos-casamiento/(:any)/(:any)_CO_t1_sc(:num)']		= 'galerias/index/1/$3';


$route['fotos-casamiento/(:num)/(:num)/(:num)']						= 'galerias/index/$1/$2/$3/0/0/0';
$route['fotos-casamiento/(:any)/(:any)_CO_t1_sc(:num)_sb(:num)']	= 'galerias/index/1/$3/$4/0/0/0';
$route['fotos-casamiento/(:any)/(:any)_CO_t2_su(:num)_r(:num)']		= 'galerias/index/2/0/0/$3/$4/0';



$route['fotos-casamiento/(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/(:any)_CO_r(:num)']			= 'galerias/index/0/0/0/$1/$3/0/$2';
$route['fotos-casamiento/(buenos-aires|cordoba|entre-rios|mendoza|mar-del-plata|la-plata|misiones|rosario|salta|santa-fe|tucuman)/(:any)_CO_r(:num)/(:num)']	= 'galerias/index/0/0/0/$1/$3/$4/$2';

$route['fotos-casamiento/(:any)_CO_g(:num)'] = 'galerias/detalle/$2';
$route['fotos-casamiento/(:any)_CO_t(:num)'] = 'galerias/tags/$2';

// JORNADAS
$route['eventos/expo-novias-(2017|2018|2019|2020)'] 		= 'eventos/jornadas';
$route['eventos/expo-novias-(2017|2018|2019|2020)/gracias'] = 'eventos/jornadas';

$route['eventos/expo-novias-(2017|2018|2019|2020)/registro/novios']  			= 'eventos/jornadas/0/novios';
$route['eventos/expo-novias-(2017|2018|2019|2020)/registro/novios/(:num)']  	= 'eventos/jornadas/0/novios/$2';
$route['eventos/expo-novias-(2017|2018|2019|2020)/registro/empresas']   		= 'eventos/jornadas/0/empresas';
$route['eventos/expo-novias-(2017|2018|2019|2020)/registro/empresas/(:num)']   	= 'eventos/jornadas/0/empresas/$2';

// JORNADAS ANTERIORES
$route['eventos/expo-novias-(2000|2001|2002|2003|2004|2005|2006|2007|2008|2009|2010|2011|2012|2013|2014|2015|2016)'] = 'eventos/jornadas_anteriores/$1';

// TE DE NOVIAS
$route['eventos/te-de-novias-en-buenos-aires'] 	= 'eventos/te_de_novias/420/1';
$route['eventos/te-de-novias-en-rosario']		= 'eventos/te_de_novias/422/2';
$route['eventos/te-de-novias-en-cordoba']  		= 'eventos/te_de_novias/421/4';

//
// PARA PROVEEDORES
//
$route['empresas/como-anunciar'] 					= 'proveedores/como_anunciar';
$route['empresas/nuestra-historia'] 				= 'proveedores/acerca_nuestro';
$route['empresas/expo-novias'] 						= 'proveedores/expo_novias_proveedor';
$route['empresas/publicaciones-prensa'] 			= 'proveedores/prensa_proveedor';
$route['empresas/publicaciones-prensa/diarios']		= 'proveedores/prensa_proveedor';
$route['empresas/publicaciones-prensa/revistas'] 	= 'proveedores/prensa_proveedor';
$route['empresas/publicaciones-prensa/internet'] 	= 'proveedores/prensa_proveedor';
$route['empresas/contacto'] 						= 'proveedores/contacto_proveedor';

// INFONOVIAS
$route['infonovias-(:num)']  = 'infonovias/index/$1';
$route['infonovias/gracias'] = 'infonovias';

// CEREMONIAS Y CIVILES
$route['ceremonias-y-civiles']  												= 'ceremonias';
$route['ceremonias-y-civiles/informacion-de-registros-civiles-en-argentina']	= 'ceremonias/registros_civiles';
$route['ceremonias-y-civiles/listado-de-registros-civiles-en-argentina']		= 'ceremonias/registros_civiles/directorio';
$route['ceremonias-y-civiles/informacion-de-iglesias-catolicas-en-argentina']	= 'ceremonias/iglesias';
$route['ceremonias-y-civiles/listado-de-iglesias-catolicas-en-argentina']		= 'ceremonias/iglesias/directorio';
$route['ceremonias-y-civiles/informacion-de-templos-judios-en-argentina']		= 'ceremonias/templos';
$route['ceremonias-y-civiles/listado-de-templos-judios-en-argentina']			= 'ceremonias/templos/directorio';
$route['ceremonias-y-civiles/informacion-de-ceremonias-no-religiosas']			= 'ceremonias/no_religiosas';

// POLÍTICAS DE PRIVACIDAD
$route['politicas-de-privacidad'] 	= 'generales/politicas_de_privacidad';
$route['terminos-y-condiciones']  	= 'generales/terminos_y_condiciones';
$route['mapa-de-sitio']  			= 'generales/mapa_sitio';

//
// EDITORIAL
//
$route['consejos'] 													= 'editorial/home';
$route['listado-de-consejos'] 										= 'editorial/listado';
$route['listado-de-consejos_CO_'] 									= 'editorial/listado';
$route['listado-de-consejos-(:any)_CO_cn(:num)'] 					= 'editorial/listado/0/$2';
$route['listado-de-consejos-(:any)/(:any)_CO_cn(:num)_s(:num)'] 	= 'editorial/listado/0/$3/$4';
$route['listado-de-consejos/(:any)_CO_t(:num)']						= 'editorial/tags/$2';

// LISTADOS
$route['consejos/(:any)_CO_cn(:num)']			= 'editorial/listado/$2';
$route['consejos/(:any)_CO_cn(:num)_s(:num)']	= 'editorial/listado/0/$2/$3';

// DETALLE NOTA
$route['consejos/(:any)_CO_n(:num)']	= 'editorial/detalle/$2';


// COMUNIDAD
$route['comunidad'] = 'editorial/comunidad';

//
// CASAMIENTOS TV
//
$route['casamientos-tv'] 								= 'casamientos_tv';
$route['casamientos-tv/(:any)_CO_ct(:num)'] 			= 'casamientos_tv/listado/$2';
$route['casamientos-tv/(:any)_CO_ct(:num)_ch(:num)'] 	= 'casamientos_tv/listado/$2/$3';
$route['casamientos-tv/(:any)_CO_v(:num)_e'] 			= 'casamientos_tv/detalle/$2/empresas';
$route['casamientos-tv/(:any)_CO_v(:num)_c']			= 'casamientos_tv/detalle/$2/casamientos';
$route['casamientos-tv/(:any)_CO_v(:num)']				= 'casamientos_tv/detalle/$2';
$route['casamientos-tv/(:any)_CO_t(:num)']				= 'casamientos_tv/tags/$2';