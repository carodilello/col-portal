<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions{
	private $CI;

    public function __construct(){
        parent::__construct();
        $this->CI =& get_instance();
    }

    public function show_404($page = '', $log_error = TRUE){
 		redirect(base_url($this->CI->router->routes['404_override']));
    }
}