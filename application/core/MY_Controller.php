<?php
class MY_Controller extends CI_Controller {
	
	public function __construct() {
		ob_start();
		parent::__construct();

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');
		header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

		$this->start_time = $this->microtime_float();
		$this->datetime_start = new DateTime();
		$this->datetime_start = $this->datetime_start->format('Y-m-d H:i:s');
	}

	public function __destruct() {
		if($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
			$this->total_time = number_format($this->microtime_float() - $this->start_time,4);

			$this->datetime_finish = new DateTime();
			$this->datetime_finish = $this->datetime_finish->format('Y-m-d H:i:s');
			// $this->saveLog();
		}
	}

	public function saveLog(){
		$log_data = array(
			"input" => file_get_contents("php://input"),
			"url" => uri_string(),
			"server" => json_encode($_SERVER),
			"headers" => json_encode(getallheaders()),
			"body" => ob_get_contents(),
			"process_time" => $this->total_time,
			"datetime" => $this->datetime_finish,
			"datetime_start" => $this->datetime_start
			);
		$this->db->insert('log',$log_data);
	}

	public function microtime_float(){
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
}