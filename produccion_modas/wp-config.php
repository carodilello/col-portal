<?php
/** 
 * Configuraci�n b�sica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener m�s informaci�n,
 * visita la p�gina del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionar� tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'conline_produccion_moda');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'conline_moda');

/** Tu contrase�a de MySQL */
define('DB_PASSWORD', 'prpass1908');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificaci�n de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves �nicas de autentificaci�n.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzar� a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'H6v-7k/6RSI{~=y]cER:koKq-EXf%sZ+*{w*}0x+/Tb9C0FGY+HCD-XJKhC$)i+A'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', '~GwF*rWjA~7-3Q@]9ZU)rge5w|F?r_`!5nM3Dx/(iOqRQvEX{SVVz;W|~^=U7/mQ'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', '%DSH$`x|I{IVE:Ki5{E-cic,_fL{WwQWCN*P6wJ+0LX@%JB67=#ss+=!H7R(|Mz|'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', '$Tc[&~=1P(4cw|nI[(|)b)6yc3K(|f^at)-:aBdhz.MRC@O+B<Y:lF|brGmw}g+O'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', 'G+B{L?**wZqreE4]+f]{62Oj@~^q=&(6}:+U:+:4(SP,k7Bt*+2vf)>~=d^*V.LT'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', '-IC,b>7a*>0Sh}C>~i;UOb}k_|.H|-.):/D*Tvuc6p3|6O4IhyYP }mON~+g35U`'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', '5ciQ|zlzm{4~R:*|AP1U}2$|u!^WEcA!N-!ZQWX-MdbZ#h]Qi5)JaDZeS~BFMs{)'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', 'ZJ BBh~seKm1icQFsOK2:tb=|Ry1l%[d=+T<m@j+*i#e8rmLCr1ID-%|{ECZ.Get'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo n�meros, letras y gui�n bajo.
 */
$table_prefix  = 'wp_pm_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copi�ndolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catal�n.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* �Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

	