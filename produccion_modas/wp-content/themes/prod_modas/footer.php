<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */


list($data_form['dia_evento'], $data_form["mes_evento"], $data_form["anio_evento"]) = explode("/", $_COOKIE['col_dia_evento']);
$data_form['ubicacion'] = $_COOKIE['col_ubicacion'];
$data_form['invitados'] = $_COOKIE['col_invitados'];
$data_form['nombre'] = $_COOKIE['col_nombre'];
$data_form['apellido'] = $_COOKIE['col_apellido'];
$data_form['anio_nacimiento'] = $_COOKIE['col_anio_nacimiento'];
$data_form['mes_nacimiento'] = $_COOKIE['col_mes_nacimiento'];
$data_form['dia_nacimiento'] = $_COOKIE['col_dia_nacimiento'];
$data_form['email'] = $_COOKIE['col_email'];
$data_form['telefono'] = $_COOKIE['col_telefono'];
$data_form['id_provincia'] = $_COOKIE['col_id_provincia'];
$data_form['id_localidad'] = $_COOKIE['col_id_localidad'];
$data_form['infonovias'] = 1;
$data_form['promociones'] = 1;
$data_form['verificador_captcha'] = $_COOKIE['col_verificador_captcha'];
?>

	</div><!-- #main -->

	<div class="clear"></div>

	<div id="sponsors">
	<div class="inner">
		<div class="inner_inner">
			<a href="http://www.casamientosonline.com/planea-tu-casamiento/1/buenos-aires/guia-de-servicios/8/foto-y-video/1074/estudio-mazza" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/logo_estudio_massa.jpg" /></a>
			<a href="http://www.casamientosonline.com/planea-tu-casamiento/1/buenos-aires/guia-de-servicios/17/salones-de-fiesta/4125/palacio-sans-souci" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/logo_palacio.jpg" class="no_margin" /></a>
			<a href="http://www.casamientosonline.com/produccion_modas/mapa-del-sitio/" class="sitemap">Mapa del sitio</a>
		</div>
	</div>
</div>
	</div><!-- #page -->

	<div id="form_presupuesto" style="display:none;">
		<form id="contacto" action="/formulario/index/presupuesto/empresa/<?
			$cf3 = get_post_custom_values("id_rubro");
			echo $cf3[0];
			?>/95/<?
			$cf2 = get_post_custom_values("id_minisitio");
			echo $cf2[0];
			//echo 10570;
			?>" method="post">
			<input type="hidden" name="from_produccion_modas" value="1" />
			<input type="hidden" name="id_pais"  value="10">
			<input type="hidden" name="id_rubro" value="<?php echo $cf3[0]; ?>" />
			<input type="hidden" name="id_minisitio" value="<?php echo $cf2[0]; ?>" />
			<input type="hidden" name="id_tipo_evento" value="1" />
			<input type="hidden" name="id_origen" value="95" />
			<input type="hidden" name="referer" value="<?php echo 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>">
			<input type="hidden" name="dia_evento" value="">


			<label>Fecha del casamiento (*)</label>
			<select class="dia required" name="d_evento">
				<option value="">Día</option>
				<? for($i = 1 ; $i <= 31 ; $i++){ ?>
					<option value="<?=$i;?>" <?=$data_form["dia_evento"]==$i?'selected="selected"':'';?>><?=$i;?></option>
				<? } ?>
			</select>
			<select class="mes required" name="m_evento">
				<option value="">Mes</option>
				<?
				$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
				for($i = 1 ; $i <= 12 ; $i++){ ?>
					<option value="<?=$i;?>" <?=$data_form["mes_evento"]==$i?'selected="selected"':'';?>><?=$meses[$i-1];?></option>
				<? } ?>
			</select>
			<select class="anio required" name="a_evento">
				<option value="">Año</option>
				<? for($i = date("Y") ; $i <= date("Y")+5 ; $i++){ ?>
					<option value="<?=$i;?>" <?=$data_form["anio_evento"]==$i?'selected="selected"':'';?>><?=$i;?></option>
				<? } ?>
			</select>
			<span class="block gum clear" style="margin-bottom:8px;"></span>
			<label class="clear">Datos de contacto</label>
			<input type="text" placeholder="Nombre (*)" value="<?=$data_form["nombre"];?>" class="required" name="nombre" />
			<input type="text" placeholder="Apellido (*)" value="<?=$data_form["apellido"];?>" class="required" name="apellido" />
			<input type="text" placeholder="Email (*)" value="<?=$data_form["email"];?>" class="required email" name="email" />
			<input type="text" placeholder="Teléfono (*)" value="<?=$data_form["telefono"];?>" class="required gum" name="telefono" />

			<input type="hidden" name="id_pais" value="10" />
			<label>Provincia (*)</label>
			<select name="id_provincia" class="required" onchange="javascript:update_localidad('localidades',this.value,'<?=$data_form['id_localidad'];?>')">
				<?
				$provincias = file_get_contents('http://' . $_SERVER['HTTP_HOST'] . '/ajax/get_provincias/' . $data_form['id_provincia']);
				echo $provincias;
				?>
			</select>

			<label>Localidad (*)</label>
			<select name="id_localidad" class="required" style="margin-bottom:16px;" id="localidades">
				<?
				if($data_form['id_provincia']){
					$localidades = file_get_contents('http://' . $_SERVER['HTTP_HOST'] . '/ajax/get_localidad_from_provincia/' . $data_form['id_provincia'] . '/' . $data_form['id_localidad']);
					echo $localidades;
				}
				?>
			</select>

			<input type="hidden" name="promociones" value="1" />
			<input type="hidden" name="infonovias" value="1" />

			<textarea name="comentario" style="display:none" placeholder="Comentarios (*)"></textarea>

			<div id="errorMessageBox" style="display:none"></div>

			<input type="submit" value="Enviar" />
			<img src="/images/ajax-loader.gif" id="ajax-loader" style="display:none" />

		</form>

		<div id="sentMessageBox" style="display:none">
			<h2>GRACIAS, TU CONSULTA YA HA SIDO ENVIADA!</h2>
			<p>El formulario se completó correctamente y tu consulta ya fue enviada a la diseñadora / diseñador. En breve recibirás novedades.</p>
		</div>
	</div>

<? /*
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
*/
?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-977138-21', 'casamientosonline.com');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>



<script>
function update_localidad(id, provID, val){
	if(provID){
		/*
		var qryStr = 'do=get_localidades&id_provincia='+provID+'&default='+val;
		$('#'+id).load('/includes/php/ajax_procesos.php',qryStr);
		*/

		$('#' + id).load('/ajax/get_localidad_from_provincia/' + provID + '/' + val);

	}else{
		$('#'+id).children().remove();
		$('#'+id).append('<option value="">Seleccionar una Localidad</option>');
	}
}
function estadisticaTelefono(val){
	$('.telefono').show();
	$('.boton.previo-tel').hide();

	if(val){
		var qryStr = '/ajax/estadistica/' + val + '/1';
		$.post(qryStr, function(data){console.log(data)});
	}
}

$(document).ready(function(){
	$("form#contacto").validate({
		submitHandler: function(form) {
			$("form#contacto input[type=submit]").attr("disabled","disabled");
			$("form#contacto img#ajax-loader").show();

			texto_para_textarea = '<strong>Este pedido de presupuesto fue generado desde la sección Producción de Modas, sobre el producto '+$("#titulo_producto").html()+'.</strong><br><br>La novia comentó:<br>';
			$("form#contacto textarea[name=comentario]").val(function(index, old) { return texto_para_textarea + old; });

			dia_evento = $('[name="d_evento"]').val() + '/' + $('[name="m_evento"]').val() + '/' + $('[name="a_evento"]').val();
			$("form#contacto input[name=dia_evento]").val(dia_evento);

			$('[name="d_evento"], [name="m_evento"], [name="a_evento"]').attr("disabled", "disabled");


			$.ajax({
				url: $("form#contacto").attr("action"),
				type: 'POST',
				data: $("form#contacto").serialize(),
				success: function(data){
					if(data == 'ok'){
						$('form#contacto').trigger("reset");
						$("form#contacto img#ajax-loader").hide();
						$("form#contacto input[type=submit]").removeAttr("disabled");

						$('form#contacto').hide();
						$("#sentMessageBox").show();

						$('[name="d_evento"], [name="m_evento"], [name="a_evento"]').attr("disabled", false);
					}

				}
			});
	    },
		rules: {
		},
		showErrors: function(errorMap, errorList) {
			var first;
			for (first in errorMap) break;
			if(first){
				$("#errorMessageBox").html(errorMap[first]);
				$("#errorMessageBox").show();
			}else{
				$("#errorMessageBox").hide();
			}
		},
		onkeyup: false
	});

	jQuery.extend($.validator.messages, {
		required: "(*) Complete todos los campos obligatorios",
		email: "Ingrese un e-mail valido"
	});


	$(".pscarousel img").bind('click',function(){

		src = $(this).data('full');
		orden = $(this).data('pos');
		$(".slideshow-content:eq("+orden+")").html('<img src="'+src+'" />');

		$(".snipe-lens").remove();
		$(".slideshow-content:eq("+orden+") img").snipe({
			css:{	'z-index':999,
					borderRadius:0,
					boxShadow: '0 0 10px #777'
			}
		});

		$('#pin-it-button').attr('href', buildPinUrl(src));

		$(".slideshow-content:eq("+orden+") img").load(function(){
			$(".portfolio-slideshow").width($(this).width());
			$(".portfolio-slideshow").height($(this).height());
		});

	});



});


</script>



<?php wp_footer(); ?>

</body>
</html>