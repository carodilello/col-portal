/******
* jQuery.fn.smartResize
* Fecha: Febrero 2013
*
* Autor Emilio Arcioni
* Version beta
*
* Ajustes: Agosto 2013
*
* Requerimientos:
* jquery.1.3.2.js o superior - http://jquery.com/
*
******/

(function($) {
	$.smartResize = function(options){
	    	
		var defaults = {
		    contenedor: '.contenedor',
		    elemento: '.elemento',
		    ancho_actual_elemento: 345,
		    success: function(a){}
		};
		var options = $.extend(defaults, options);
		
		// -- calculo el ancho del scrollbar
		var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
		$outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
		inner = $inner[0],
		outer = $outer[0];
		
		jQuery('body').append(outer);
		var ancho1 = inner.offsetWidth;
		$outer.css('overflow', 'scroll');
		var ancho2 = outer.clientWidth;
		$outer.remove();
		
		ancho_scrollbar =  (ancho1 - ancho2);
		// --
		
		var ancho_cont = $(options.contenedor).outerWidth() - ancho_scrollbar;
		var cuenta =  ancho_cont / options.ancho_actual_elemento;
		var decimal = cuenta - parseInt(cuenta);
		
	    
		/*
		if(decimal < 0.5){
			var divisor = parseInt(cuenta);
		}else{
			var divisor = (parseInt(cuenta) + 1);
		}
		*/
		var divisor = parseInt(cuenta);
		var nuevo_ancho_elemento = (ancho_cont / divisor);
		
		// fix para ajustar en caso que sean menos fotos 
		if( $(options.elemento).length < divisor && $(options.elemento).length > 2){
			nuevo_ancho_elemento = ancho_cont / $(options.elemento).length;
		}
		// --
		
		// fix para poner un limite maximo de 3 fotos de ancho
		if(divisor > 3){
			nuevo_ancho_elemento = ancho_cont / 3;
		}
		// --
		
		// fix para que se ajusten al ancho del elemento contenedor
		if($("html").hasScrollBar()){
			nuevo_ancho_elemento -= 7;
		}else{
			nuevo_ancho_elemento -= divisor;
		}
		// --
		
		$(options.elemento).css('width',nuevo_ancho_elemento);
		//console.log(verticalScrollPresent());
		
		$(options.success(nuevo_ancho_elemento));
	};
	
	function verticalScrollPresent(){
		//return (document.documentElement.scrollHeight !== document.documentElement.clientHeight);
		
		// Get the computed style of the body element
		var cStyle = document.body.currentStyle||window.getComputedStyle(document.body, "");
		
		// Check the overflow and overflowY properties for "auto" and "visible" values
		var hasVScroll = cStyle.overflow == "visible" || cStyle.overflowY == "visible" || (hasVScroll && cStyle.overflow == "auto") || (hasVScroll && cStyle.overflowY == "auto");
		
		return !hasVScroll;
	};
	
	$.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);