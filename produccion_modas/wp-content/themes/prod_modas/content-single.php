<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">
		<?php // the_content() ?>
		<div class="cont_magn">
			
		
			
			<div class="clear"></div>
			<!--<div class="magnify">-->
				<? /* <div class="large" style="background: url('<?php echo catch_that_image() ?>') no-repeat;"></div>
				<img class="small" src="<?php echo catch_that_image() ?>" /> */ ?>
				<?php 
				echo do_shortcode('[portfolio_slideshow click=openurl id='.$idpost.']');
				?>
				<div class="clear"></div>
			<!--</div>-->
			
			<?php if ( in_category( array( 'diego-escalante-novias', 'gabriel-lage', 'amelia-consol') )) {
				?><h2>Créditos</h2><?php
				include('asociados.php');
			}
			?>

		</div>
		
				
	</div><!-- .entry-content -->
	
	
	<?php
		$rec_cat = get_term_by('slug','ana-pugliesi','category');
		$postsInCat = $rec_cat->count;
	?>
	
	<header class="entry-header">
		
		
		<h1 class="entry-title"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h1>
		<h3 class="tel_oculto none">Teléfono:  <?php $auto = get_post_custom_values("telefono"); echo $auto[0]; ?></h3>
		<?php $idpost = get_the_ID(); ?>	

		
		
		<?php
			$the_cat = get_the_category();
			$category_name = $the_cat[0]->cat_name;
			$category_link = get_category_link( $the_cat[0]->cat_ID );
		?>
		
		<a class="ver_todas block" href="<?php echo $category_link ?>">(Ver todas sus producciones)</a>
		
		<?php 
		// Si hay id_minisitio y id_rubro cargado, muestra el botón
		$cf2 = get_post_custom_values("id_minisitio"); 
		$cf3 = get_post_custom_values("id_rubro");

		if ( in_category( array( 'diego-escalante-novias', 'lindissima', 'amelia-consol') ) && $cf2[0] && $cf3[0]) {
			?><a href="javascript:;" id="dialog_link" class="boton" style="background:#eb8440; font-weight:bold;"><span></span>Contactar</a><?php
}
?>
		
		
		<?php // Si hay minisitio muestra el botón 
			$minisitio = get_post_meta($post->ID, 'minisitio', true);
			 

			if ( $minisitio ) {
			 	?><a href="<?php $auto = get_post_custom_values("minisitio"); echo $auto[0]; ?>" class="boton" target="_blank"><span class="minisitio"></span>Ver Minisitio</a><?php
			}
			else {
			
			}
		?>
		<?
		$cf1 = get_post_custom_values("telefono"); 
		
		if($cf1[0]){ 
		?>
		<a href="javascript:;" class="boton previo-tel" onclick="javascript:estadisticaTelefono(<?=$cf2[0];?>);"><span class="tel"></span>Ver teléfono</a>
		<span class="telefono boton" style="display:none; background:#666;"><span class="tel"></span><?=$cf1[0];?></span>
		<? } ?>
		
		
		<div class="gum_max clear redes_sociales">
			<?php
	    		$social_sharing_toolkit = new MR_Social_Sharing_Toolkit();
	    		echo $social_sharing_toolkit->create_bookmarks();
			?>
			<span style="position:relative; top:7px;">
				<? /*
				<a data-pin-config="none" href="javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());" data-pin-do="buttonPin"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
				*/ ?>
				
				<a data-pin-config="none" href="" data-pin-do="buttonPin" target="_blank" id="pin-it-button"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
				
				
				
			</span>
			
			<a href="javascript:;" onclick="printpage()" class="imprimir">Imprimir</a>
			
			<div class="clear"></div>
		</div>
		<h2 class="clear" id="titulo_producto"><?php the_title();  ?></h2>
		<p class="extracto"><?php $excerpt = get_the_excerpt(); echo $excerpt; ?></p>
		
		
		
		<?php  $post_thumbnail_id = get_post_thumbnail_id( $post_id ); ?> 
		
		<?php $postid = get_the_ID(); ?>
		<?php 
		$gallery = do_shortcode('[gallery exclude="'.$post_thumbnail_id.'"]'); 
		if($gallery){
		?>
		<div id="todas_las_vistas">
			<h2 class="clear">Todas las vistas</h2>
			
			<?php echo do_shortcode('[portfolio_slideshow id='.$idpost.' mostrar_solo_thumbs=true]'); ?>
			<?php
			//echo $gallery;
			}
			?>
		
		</div>
		
		<div class="clear gum"></div>
		
		<?php
			if( has_tag() ) { ?>
				<div class="tags">
					<h3 class="clear">Tags</h3>
					<?php the_tags( '', '-', $after ); ?>
		</div>
		<?php  } else { } ?>

		<?php if (has_post_video()){ ?>
		<h3 class="clear">Video</h3>
		<?php the_post_video();
		}
 ?>
		
		<nav id="nav-single">
			<span class="nav-next"><?php next_post_link_plus( array(
                    'order_by' => 'post_date',
                    'order_2nd' => 'post_date',
                    'meta_key' => '',
                    'post_type' => '',
                    'loop' => false,
                    'end_post' => false,
                    'thumb' => false,
                    'max_length' => 0,
                    'format' => '%link',
                    'link' => '%title',
                    'date_format' => '',
                    'tooltip' => '',
                    'in_same_cat' => true,
                    'in_same_tax' => false,
                    'in_same_format' => false,
                    'in_same_author' => false,
                    'in_same_meta' => false,
                    'ex_cats' => '',
                    'ex_cats_method' => 'weak',
                    'in_cats' => '',
                    'ex_posts' => '',
                    'in_posts' => '',
                    'before' => '',
                    'after' => '',
                    'num_results' => 1,
                    'return' => ''
                    ) ); ?></span>
                    
			<span class="nav-previous"><?php previous_post_link_plus( array(
                    'order_by' => 'post_date',
                    'order_2nd' => 'post_date',
                    'meta_key' => '',
                    'post_type' => '',
                    'loop' => false,
                    'end_post' => false,
                    'thumb' => false,
                    'max_length' => 0,
                    'format' => '%link',
                    'link' => '%title',
                    'date_format' => '',
                    'tooltip' => '',
                    'in_same_cat' => true,
                    'in_same_tax' => false,
                    'in_same_format' => false,
                    'in_same_author' => false,
                    'in_same_meta' => false,
                    'ex_cats' => '',
                    'ex_cats_method' => 'weak',
                    'in_cats' => '',
                    'ex_posts' => '',
                    'in_posts' => '',
                    'before' => '',
                    'after' => '',
                    'num_results' => 1,
                    'return' => ''
                    ) ); ?></span>
			
			<p>Producción
			
			<?php
				$category = get_the_category();
				$currentcat = $category[0]->cat_ID;
			?>
			
			<?php 
			$posts = get_posts(array('category' => $currentcat ));   
					
			if ($posts) {
				
				$postid = get_the_ID();
				
				$p = array_reverse($posts);
				$c = 1;
				foreach($p as $post) {
					
					if($post->ID == $postid){
						$pos = $c;
					}else{
						$c ++;
					}
				}
				
				echo $pos;
			} ?>
					
				de <?php  
				$postsInCat = get_term_by('name',$category[0]->cat_name,'category');
				$postsInCat = $postsInCat->count;
				echo $postsInCat;
			?>	
			
					
			</p>
		</nav><!-- #nav-single -->
	</header><!-- .entry-header -->
	<div class="clear"></div>
	
	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'twentyeleven' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'twentyeleven' ) );
			if ( '' != $tag_list ) {
				$utility_text = __( 'This entry was posted in %1$s and tagged %2$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} elseif ( '' != $categories_list ) {
				$utility_text = __( 'This entry was posted in %1$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} else {
				$utility_text = __( 'This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			}

			printf(
				$utility_text,
				$categories_list,
				$tag_list,
				esc_url( get_permalink() ),
				the_title_attribute( 'echo=0' ),
				get_the_author(),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
			);
		?>
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>

		<?php if ( get_the_author_meta( 'description' ) && ( ! function_exists( 'is_multi_author' ) || is_multi_author() ) ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries ?>
		<div id="author-info">
			<div id="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyeleven_author_bio_avatar_size', 68 ) ); ?>
			</div><!-- #author-avatar -->
			<div id="author-description">
				<h2><?php printf( __( 'About %s', 'twentyeleven' ), get_the_author() ); ?></h2>
				<?php the_author_meta( 'description' ); ?>
				<div id="author-link">
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
						<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyeleven' ), get_the_author() ); ?>
					</a>
				</div><!-- #author-link	-->
			</div><!-- #author-description -->
		</div><!-- #author-info -->
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
