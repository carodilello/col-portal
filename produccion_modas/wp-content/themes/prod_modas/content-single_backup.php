<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">

		<div class="cont_magn">
			
			
			<a href="" class="print_icon"></a>
			<div class="clear"></div>
			<div class="magnify">
				<? /* <div class="large" style="background: url('<?php echo catch_that_image() ?>') no-repeat;"></div>
				<img class="small" src="<?php echo catch_that_image() ?>" /> */ ?>
				<?php 
				echo do_shortcode('[portfolio_slideshow id='.$idpost.']');
				?>
				<div class="clear"></div>
			</div>
		</div>
		<?php include('asociados.php'); ?>
				
	</div><!-- .entry-content -->
	
	
	<?php
		$rec_cat = get_term_by('slug','ana-pugliesi','category');
		$postsInCat = $rec_cat->count;
	?>
	
	<header class="entry-header">
		
		
		<h1 class="entry-title"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h1>
			
		<?php $idpost = get_the_ID(); ?>	

		
		
		
		<?php
			$the_cat = get_the_category();
			$category_name = $the_cat[0]->cat_name;
			$category_link = get_category_link( $the_cat[0]->cat_ID );
		?>
		
		<a class="ver_todas block" href="<?php echo $category_link ?>">(Ver todas sus producciones)</a>
		<a href="javascript:;" id="dialog_link" class="boton"><span></span>Contactar</a>
		
		<?php // Si hay minisitio cargado, muestra el botón 
			$minisitio = get_post_meta($post->ID, 'minisitio', true);
			if ( $minisitio ) {
			 	?><a href="<?php $auto = get_post_custom_values("minisitio"); echo $auto[0]; ?>" class="boton"><span class="minisitio"></span>Ver Minisitio</a><?php
			}
			else {
			
			}
		?>

		<a href="javascript:;" class="boton" onclick="javascript:$('.telefono').show(); "><span class="tel"></span>Ver teléfono</a>
		
		
		
		<span class="telefono" style="display:none;"><?php $auto = get_post_custom_values("Telefono"); echo $auto[0]; ?></span>
		
		
		
		<div class="gum_max clear redes_sociales">
			<?php
	    		$social_sharing_toolkit = new MR_Social_Sharing_Toolkit();
	    		echo $social_sharing_toolkit->create_bookmarks();
			?>
			<span style="position:relative; top:7px;">
				<a data-pin-config="none" href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.casamientosonline.com%2Fproduccion_modas%2F&media=<?php echo urlencode( catch_that_image()); ?>&description=Pin%20it!" data-pin-do="buttonPin"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
			</span>
			<div class="clear"></div>
		</div>
		<h2 class="clear"><?php the_title();  ?></h2>
		<p class="extracto"><?php $excerpt = get_the_excerpt(); echo $excerpt; ?></p>
		
		<?php  $post_thumbnail_id = get_post_thumbnail_id( $post_id ); ?> 
		
		<?php $postid = get_the_ID(); ?>
		<?php 
		$gallery = do_shortcode('[gallery exclude="'.$post_thumbnail_id.'"]'); 
		if($gallery){
		?>
		<h2 class="clear">Todas las vistas</h2>
		
		<?php echo do_shortcode('[portfolio_slideshow id='.$idpost.' mostrar_solo_thumbs=true]'); ?>
		
		<?php
		//echo $gallery;
		}
		?>
		
		
		
		<div class="clear gum"></div>
		
		<?php
			if( has_tag() ) { ?>
				<div class="tags">
					<h3 class="clear">Tags</h3>
					<?php the_tags( '', '-', $after ); ?>
		</div>
		<?php  } else { } ?>
		
		
		<?php if (has_post_video()){ ?>
		<h3 class="clear">Video</h3>
		<?php the_post_video();
		
		}

 ?>
		
		
		
		<nav id="nav-single">
						
			<span class="nav-next"><?php previous_post_smart('%link',''); ?></span>
			<span class="nav-previous"><?php next_post_smart('%link',''); ?> </span>
			
			<p>Producción  <?php 
			$posts = Smarter_Navigation::get_posts();
			$id_post_actual = get_the_ID();
			
			foreach($posts as $i=>$p){
				if($p->ID == $id_post_actual){
					$nro_post_actual = $i + 1;
				}
			}
			
			echo '<strong>'.$nro_post_actual.'</strong> de <strong>'.count($posts).'</strong>';
			
			if($cat_data = get_referrer_category()){
				echo ' de '.$cat_data->name;
			}else{
				if(home_url() != get_referrer_url() && home_url()."/" != get_referrer_url()){
					$cat = get_the_category();
					$parent = get_category($cat[0]->category_parent);
					echo ' de '.$parent->cat_name;
				}
			}
			
	?></p>
				
		</nav><!-- #nav-single -->
		
		<?php /* <?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php twentyeleven_posted_on(); ?>
		</div><!-- .entry-meta --> 
		<?php endif; ?> */?>
	</header><!-- .entry-header -->
	<div class="clear"></div>
	
	

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'twentyeleven' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'twentyeleven' ) );
			if ( '' != $tag_list ) {
				$utility_text = __( 'This entry was posted in %1$s and tagged %2$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} elseif ( '' != $categories_list ) {
				$utility_text = __( 'This entry was posted in %1$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} else {
				$utility_text = __( 'This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			}

			printf(
				$utility_text,
				$categories_list,
				$tag_list,
				esc_url( get_permalink() ),
				the_title_attribute( 'echo=0' ),
				get_the_author(),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
			);
		?>
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>

		<?php if ( get_the_author_meta( 'description' ) && ( ! function_exists( 'is_multi_author' ) || is_multi_author() ) ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries ?>
		<div id="author-info">
			<div id="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyeleven_author_bio_avatar_size', 68 ) ); ?>
			</div><!-- #author-avatar -->
			<div id="author-description">
				<h2><?php printf( __( 'About %s', 'twentyeleven' ), get_the_author() ); ?></h2>
				<?php the_author_meta( 'description' ); ?>
				<div id="author-link">
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
						<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyeleven' ), get_the_author() ); ?>
					</a>
				</div><!-- #author-link	-->
			</div><!-- #author-description -->
		</div><!-- #author-info -->
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
