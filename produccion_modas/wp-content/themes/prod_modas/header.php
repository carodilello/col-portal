<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/modify.css" />
<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo('template_directory'); ?>/print.css" />

<script>
function printpage()
  {
  window.print()
  }
</script>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/jquery-ui-1.10.3.custom.css" />
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-ui-1.10.3.custom.js"></script>

<script>
$(document).ready(function(){
	$('#dialog_link').click(function(){
		$( "#form_presupuesto" ).dialog({
			modal:true
		});
		
		$('#form_presupuesto').bind('dialogclose', function(event) {
			$('form#contacto').show();
			$("#sentMessageBox").hide();
		 });
	});
	
	$('.pscarousel img').attr('nopin','nopin');
	
});


</script>






<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_enqueue_script('jquery'); ?>


<script language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/masonry.js"></script>

<script language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.smartResize.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.snipe.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/glass.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/glass_pre.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.validate.min.js"></script>

<!--script  language="javascript" type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/valign.js"></script> -->

<script>
	function buildPinUrl(urlmedia) {
	return '//pinterest.com/pin/create/bookmarklet/?'+
	        'url='+encodeURIComponent(document.location.href)+
	        '&media='+encodeURIComponent(urlmedia)+
	        '&description='+encodeURIComponent('<?=str_replace("\r\n",'',strip_tags(get_the_excerpt()));?>');
}
	function redimensionar($container){
		$.smartResize({
			contenedor: '#content',
			elemento: '.hentry',
			ancho_actual_elemento: 345,
			success: function(nuevo_ancho_elemento){
					$(".hentry").show();
					$container.masonry({
						itemSelector : '.hentry',
						columnWidth: nuevo_ancho_elemento + 1,
						isResizable: true,
						isAnimated: false
					});
			}
		});
	}
	$(document).ready(function(){
		
		var $container = $('#content');
		$container.imagesLoaded(function(){
			redimensionar($container);
		});
		
		$(window).resize(function() {
			redimensionar($container);
		});
		
	});
</script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed">
	<header id="branding" role="banner">
			<a href="http://www.casamientosonline.com/produccion_modas/" class="logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo_col.png" nopin="nopin" /></a>

			
			<?php
				// Has the text been hidden?
				if ( 'blank' == get_header_textcolor() ) :
			?>
				<div class="only-search<?php if ( $header_image ) : ?> with-image<?php endif; ?>">
				<?php get_search_form(); ?>
				</div>
			<?php
				else :
			?>
				<?php get_search_form(); ?>
			<?php endif; ?>

			<nav id="access" role="navigation">
				<h3 class="assistive-text"><?php _e( 'Main menu', 'twentyeleven' ); ?></h3>
				<?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff. */ ?>
				<div class="skip-link"><a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to primary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to primary content', 'twentyeleven' ); ?></a></div>
				<div class="skip-link"><a class="assistive-text" href="#secondary" title="<?php esc_attr_e( 'Skip to secondary content', 'twentyeleven' ); ?>"><?php _e( 'Skip to secondary content', 'twentyeleven' ); ?></a></div>
				<?php /* Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assigned to the primary location is the one used. If one isn't assigned, the menu with the lowest ID is used. */ ?>
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			</nav><!-- #access -->
			
			
			<ul class="social_nav_icon">
				<li><a href="http://www.facebook.com/casamientos" class=""></a></li>
				<li><a href="http://www.twitter.com/casamientos_ol" class="twitter"></a></li>
				<li><a href="http://pinterest.com/casamientos/" class="pinterest"></a></li>
				<li><a href="https://plus.google.com/112129356908000548887/?prsrc=3" class="g_plus"></a></li>
				<li><a href="http://www.youtube.com/casamientosonline" class="youtube"></a></li>
			</ul>
	</header><!-- #branding -->


	<div id="main">
