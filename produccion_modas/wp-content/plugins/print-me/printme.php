<?php
/*
Plugin Name: Print me!
Plugin URI: http://zelenin.me
Description: This plugin adds a print version
Version: 0.4.2
Author: Aleksandr Zelenin
Author URI: http://zelenin.me
License: Free
*/

define( 'PRINTME_VERSION', '0.4.2' );
$printme_options = get_option( 'printme_options' );

if ( ( !isset( $_GET['print'] ) || !$_GET['print'] == 1 ) && !isset( $printme_options['auto'] )  ) add_filter( 'the_content', 'printme_add_link' );


function printme_add_link( $content ) {

	global $printme_options;

	if ( is_singular() ) {

		$size = empty( $printme_options['size'] ) ?  10 : $printme_options['size'];
		$margin_v = empty( $printme_options['margin_v'] ) ? 10 : $printme_options['margin_v'];
		$margin_h = empty( $printme_options['margin_h'] ) ? 0 : $printme_options['margin_h'];
		$print_stub = empty( $printme_options['print_text'] ) ? '<img style="width:' . $size . 'px;height:' . $size .'px;" src="' . plugins_url( '/printme.png', __FILE__ ) . '" title="print">' : $printme_options['print_text'];

		wp_register_script( 'printme', plugins_url( '/printme.js', __FILE__ ), array( 'jquery' ), PRINTME_VERSION, true );
		wp_enqueue_script( 'printme' );

		$link = add_query_arg( 'print', '1', get_permalink() );
		if ( !empty( $printme_options['position'] ) ) {
			$content = $content . '<p style="margin:' . $margin_v . 'px ' . $margin_h . 'px;" id="print-page-link"><a href="' . $link . '">' . $print_stub . ' </a></p>';
		} else {
			$content = '<p style="margin:' . $margin_v . 'px ' . $margin_h . 'px;" id="print-page-link"><a href="' . $link . '">' . $print_stub . ' </a></p>' . $content;
		}

	}

	return $content;

}

add_shortcode( 'printme', 'printme_link' );
function printme_link() {

	global $printme_options;

	if ( is_singular() ) {

		$size = empty( $printme_options['size'] ) ?  10 : $printme_options['size'];
		$margin_v = empty( $printme_options['margin_v'] ) ? 10 : $printme_options['margin_v'];
		$margin_h = empty( $printme_options['margin_h'] ) ? 0 : $printme_options['margin_h'];
		$print_stub = empty( $printme_options['print_text'] ) ? '<img style="width:' . $size . 'px;height:' . $size .'px;" src="' . plugins_url( '/printme.png', __FILE__ ) . '" title="print">' : $printme_options['print_text'];

		wp_register_script( 'printme', plugins_url( '/printme.js', __FILE__ ), array( 'jquery' ), PRINTME_VERSION, true );
		wp_enqueue_script( 'printme' );

		$link = add_query_arg( 'print', '1', get_permalink() );
		$content = '<p style="margin:' . $margin_v . 'px ' . $margin_h . 'px;" id="print-page-link"><a href="' . $link . '">' . $print_stub . ' </a></p>';
		
	}

	echo $content;

}

add_action( 'template_redirect', 'printme', 5 );
function printme() {
	if ( isset( $_GET['print'] ) && $_GET['print'] == 1 ) {
		include( plugin_dir_path(__FILE__) . 'print.php' );
		exit();
	}
}

add_action( 'admin_menu', 'printme_menu' );
function printme_menu() {
	add_options_page( 'Print me!', 'Print me!', 'manage_options', 'printme', 'printme_options_page' );
	add_filter( 'plugin_action_links', 'printme_options_links', 10, 2 );
	function printme_options_links( $links, $file ) {
		if ( $file != plugin_basename( __FILE__ )) return $links;
		$settings_link = '<a href="options-general.php?page=printme">Settings</a>';
		array_unshift( $links, $settings_link );
		return $links;
	}
}

add_action( 'admin_init', 'printme_settings' );
function printme_settings() {
	register_setting( 'printme', 'printme_options' );
}

function printme_options_page() {

	global $printme_options; ?>
	<div class="wrap">
		<div class="icon32" id="icon-options-general"></div>
		<h2>Print me!</h2>
		<form method="post" action="options.php">
			<?php settings_fields( 'printme' ); ?>
			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row"><label for="printme_options[position]">Button below the content</label></th>
						<td><input type="checkbox" name="printme_options[position]" id="printme_options[position]" <?php isset( $printme_options['position'] ) ? checked( $printme_options['position'], 'on', true ) : ''; ?>></td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="printme_options[size]">Width/height of icon</label></th>
						<td><input type="text" class="regular-text code" value="<?php echo $printme_options['size']; ?>" id="printme_options[size]" name="printme_options[size]"> <span class="description">Insert width/height of icon (<=64)</span></td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="printme_options[margin_v]">Margin-top/margin-bottom</label></th>
						<td><input type="text" class="regular-text code" value="<?php echo $printme_options['margin_v']; ?>" id="printme_options[margin_v]" name="printme_options[margin_v]"> <span class="description">Insert margin-top/margin-bottom of icon</span></td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="printme_options[margin_h]">Margin-left/margin-right</label></th>
						<td><input type="text" class="regular-text code" value="<?php echo $printme_options['margin_h']; ?>" id="printme_options[margin_h]" name="printme_options[margin_h]"> <span class="description">Insert margin-left/margin-right of icon</span></td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="printme_options[print_text]">Text instead of icon</label></th>
						<td><input type="text" class="regular-text code" value="<?php echo $printme_options['print_text']; ?>" id="printme_options[print_text]" name="printme_options[print_text]"> <span class="description">Type text</span></td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="printme_options[auto]">Turn off put icon/text automatically</label></th>
						<td><input type="checkbox" name="printme_options[auto]" id="printme_options[auto]" <?php isset( $printme_options['auto'] ) ? checked( $printme_options['auto'], 'on', true ) : ''; ?>></td>
					</tr>
					<tr valign="top">
						<th scope="row"><label for="printme_options[head]">Turn on 'wp_head()'</label></th>
						<td><input type="checkbox" name="printme_options[head]" id="printme_options[head]" <?php isset( $printme_options['head'] ) ? checked( $printme_options['head'], 'on', true ) : ''; ?>> <span class="description">Turn on if you have problems with third-party plugins</span></td>
					</tr>
				</tbody>
			</table>
			<?php submit_button( 'Update', 'primary', 'submit', 'true' ); ?>
		</form>
		<div class="updated settings-error" id="setting-error-settings_updated">
			<p>Shortcode: <strong>[printme]</strong></p>
			<p>php function: <strong>printme_link();</strong></p>
			<p><strong>Donate me via PayPal:</strong></p>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="LW23C9HMMDRSN">
				<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</div>
	</div>
<?php
}

register_activation_hook( __FILE__, 'printme_activate' );
function printme_activate() {

	$data = array(
		'plugin_name' => 'Print me!',
		'version' => PRINTME_VERSION,
		'url' => get_home_url(),
		'sitename' => get_option( 'blogname' )
	);
	$track = wp_remote_get( 'http://zelenin.me/wp-tracker.php?' . http_build_query( $data ) );

}