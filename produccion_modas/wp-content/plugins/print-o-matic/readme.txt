=== Print-O-Matic ===
Contributors: twinpictures, baden03
Donate link: http://plugins.twinpictures.de/flying-houseboat/
Tags: print, print element, print shortcode, send to print, jQuery, javascript, twinpictures
Requires at least: 3.3
Tested up to: 3.6-beta
Stable tag: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds the ability to print any post or post element using a simple [print-me] shortcode. Extra jQuery Jedi love given to filled out forms.

== Description ==
Print-O-Matic adds the ability to print any post, page or page element by using a simple [print-me] shortcode.  Bonus feature: filled out form elements are also printed using a kind of jQuery Jedi magic.  A <a href='http://plugins.twinpictures.de/plugins/print-o-matic/documentation/'>complete listing of shortcode options</a> are available, as well as <a href='http://wordpress.org/support/plugin/print-o-matic'>free community</a> and <a href='http://plugins.twinpictures.de/plugins/print-o-matic/support/'>premium support</a>.

== Installation ==

1. Old-school: upload the `print-o-matic` folder to the `/wp-content/plug-ins/` directory via FTP.  Hipster: Ironically add Print-O-Matic via the WordPress Plug-ins menu.
1. Activate the Plug-in
1. Add a the shortcode to your post like so: `[print-me target="div#id_of_element_to_print" title="Print Form"]`
1. Test that the this plug-in meets your demanding needs.
1. Tweak the CSS to match your flavor.
1. Rate the plug-in and verify if it works at wordpress.org.
1. Leave a comment regarding bugs, feature request, cocktail recipes at http://wordpress.org/tags/print-o-matic/

== Frequently Asked Questions ==

= I am a Social Netwookiee, might Twinpictures have a Facebook page? =
Yes, yes... <a href='http://www.facebook.com/twinpictures'>Twinpictures is on Facebook</a>.

= Does Twinpictures do the Twitter? =
Ah yes! <a href='http://twitter.com/#!/twinpictures'>@Twinpictures</a> does the twitter tweeting around here.

= How does one use the shortcode, exactly? =
A <a href='http://plugins.twinpictures.de/plugins/print-o-matic/documentation/'>complete listing of shortcode options</a> has been provided to answer this exact question.

= Who likes to rock the party? =
I like to rock the party.

== Screenshots ==
1. See the printer icon? Guess what happens when it's clicked?
1. Print-O-Matic Options screen for Ultimate Flexibility

== Changelog ==

= 1.4 =
* target may now use %ID% as a placeholder for the post ID

= 1.3 =
* Fixed so the print dialog box will display in IE (buggy, buggy IE)

= 1.2 =
* Added Printicon Attribute to insert text-only print link

= 1.1 =
* Added Options page with default target attribute and css style settings

= 1.0.1 =
* Removed space from title of new window to prevent the wonderful IE8 from throwing errors.

= 1.0 =
* The plug-in was forked and completely rewritten from Print Button Shortcode by MyWebsiteAdvisor.

== Upgrade Notice ==

= 1.4 =
* %ID% may now be used in the target a placeholder for the post ID

= 1.3 =
* Print dialog will now display in IE (the non-standards browser)

= 1.2 =
* New text only print link using printicon attribute

= 1.1 =
* New Options page where target attribute and css style settings can be set.

= 1.0.1 =
IE8 Bug work-around added.

= 1.0 =
Where once there was not, there now is.
