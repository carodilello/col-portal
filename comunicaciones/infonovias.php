<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
<meta charset="UTF-8">
<title>Infonovias</title>
</head>

<body bgcolor="#e3e3e3;" style="background:#e3e3e3;">
	
<table width="650" cellpadding="0" cellspacing="0" border="0" style="color:#887F6F; font-family: Arial, Helvetica, sans-serif;" align="center">
	<tr>
		<td colspan="3" align="center">
		<p style="font-size:13px; padding:5px;color:#887F6F; margin-bottom: 20px;">Si ten&eacute;s problemas para ver este correo electr&oacute;nico , por favor hace <a style="color:#eb5e89;" href="http://www.casamientosonline.com/newsletters/newsletter_<?=$_GET['id']?>.html">click aqu&iacute;</a></p>
		</td>
	</tr>
</table>

<table align="center" cellpadding="0" cellspacing="0" border="0" width="650" style="background:#fff; border-radius:10px; box-shadow: 0 1px 2px #ccc;">
	<tr>
		<td>
			<table id="nota_principal"  align="center" cellpadding="0" cellspacing="0" border="0" style="color:#5e5e5e; font-size: 15px; font-family: arial, helvetica, sans-serif; background:#fff; line-height:20px;">
				<tr>
					<td colspan="3" style="height: 32px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td align="center">
						<p style="font-size: 14px; font-style: italic; margin-top: 0;"><strong>Nº <?=$_GET['id']?></strong> - <?=$Fecha_ppal?></p>
						<img src="http://casamientosonline.com/comunicaciones/images/Infonovias_logo.png" />
					</td>
					<td width="40"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td style="height: 32px; border-bottom: 2px solid #d1d1d1;"></td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 32px;"></td>
				</tr>
				<tr>
					
					<td width="40"></td>
					<td>
						<img src="http://casamientosonline.com/comunicaciones/images/infonovias_pic_nota_principal.jpg" />	
					</td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 32px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td>
						<p class="titulo" style="color: #eb5e89; font-size: 34px; font-weight:bold; margin-top:0; margin-bottom: 12px; ">Foto y Video</p>
						<p class="subtitulo" style="color: #28b29e; font-size: 20px;margin-top:0; padding-bottom: 8px; margin-bottom: 22px; border-bottom: 2px solid #d1d1d1;">Una nueva forma de retratar a los novios!</p>
						
						<p>El fotoperiodismo de bodas es lo que va!!! Luego de esta nueva forma de retratar a los novios, ya nada volvió a ser como antes. El fotoperiodismo revela justamente esto: un registro cada vez más descontracturado, un estilo menos posado, novios relajados y fotógrafos atentos a registrar más emociones que poses clichés. Así, la redefinición estética que sufrió la fotografía de los casamientos la alejó para siempre del retrato tradicional.</p>
						<p>Los avances  tecnológicos y el cambio de rumbo en la edición transformaron el video de los casamientos para siempre. Las empresas de video de casamientos comenzaron a crear nuevos videos, videos que son como películas de cine. Con un comienzo, un climax, desenlace y final, estos nuevos videos de casamiento dejaron de ser esa 
			tradicional compaginación de imágenes sin relación entre sí. Ahora son historias dinámicas y divertidas, historias con mucho sentido, que tus familiares y amigos 
			querrán volver a ver.</p>
						<p>Pueden conseguir buenos paquetes para tener "foto y video" con un mismo proveedor e incluso armar un combo para proyectar videos o clips durante la fiesta. Hay videos de todo tipo: de los amigos, los familiares, de los novios para los invitados, la historia de la pareja, las filmaciones del civil, ¡la imaginación no tiene límites!</p>
						<p style="text-align:center; margin-bottom: 0;">
							 <!--[if mso]>
					          <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{MAILTO_RESPONDER}" style="height:36px;v-text-anchor:middle;width:350px;" arcsize="5%" strokecolor="#EB7035" fillcolor="#EB7035">
					            <w:anchorlock/>
					            <center style="color:#ffffff;font-family: font-family: Arial, Helvetica, sans-serif; font-size:15px; font-weight: bold; ">Consultar empresas de Foto y Video</center>
					          </v:roundrect> 
							 <![endif]-->
						     <a href="" style="background-color:#eb3d72; border:1px solid #EB7035; border-radius:3px; color:#fff; display:inline-block; font-family:sans-serif; font-size:15px; line-height:38px; text-align:center; text-decoration:none; width:350px; -webkit-text-size-adjust:none; mso-hide:all; font-weight: bold; box-shadow: 0 2px 0 #af4672;">Consultar empresas de Foto y Video</a>
						</p>
					</td>
					<td width="40"></td>
				</tr>
				
				<tr>
					<td colspan="3" style="height:40px;"></td>
				</tr>
				
				<tr>
					<td width="40"></td>	
					<td style="border-bottom:1px solid #ddd;"></td>
					<td width="40"></td>
				</tr>
				
				<tr>
					<td colspan="3" style="height:40px;"></td>
				</tr>
				
				<tr>
					<td width="40"></td>	
					<td><img src="http://casamientosonline.com/comunicaciones/images/banner_570x70.jpg" /></td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="3" style="height:40px;"></td>
				</tr>
				
			</table>
			
			<!---------- ## TERMINA NOTA PRINCIPAL ## ----------->
			
			<table align="center" bgcolor="#faf0f2" class="comercial" cellpadding="0" cellspacing="0" border="0" style=" background:#faf0f2; color:#5e5e5e; font-size: 15px; font-family: arial, helvetica, sans-serif; line-height:20px;">
				<tr>
					<td colspan="4" style="height: 40px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td colspan="3">
						<p class="epigrafe" style="color: #9f9f9f; font-size: 16px;margin-top:0; font-weight:bold; ">Foto y Video para Casamientos</p>
						<p class="titulo" style="color: #eb5e89; font-size: 34px; font-weight:bold; margin-top:0; padding-bottom: 20px; margin-bottom: 22px; border-bottom: 2px solid #d1d1d1; ">Foto y Video</p>
					</td>
					<td width="40"></td>
				</tr>
				
				<tr>
					<td width="40"></td>
					<td valign="top" class="contenido">
						<p style="margin-top:0;"><strong>Fotografía</strong><br />
						- Sesión previa a la boda (E-Sesión) en Exteriores<br />
						- Cobertura de Civil, Ceremonia Religiosa y Fiesta<br />
						- Cantidad ilimitada de fotos<br />
						- Entrega de TODAS las fotos retocadas en DVDs en Alta Calidad (36 megapixels),Sin marcas de Agua<br />
						</p>
						<p><strong>Video</strong><br />
						- Video en FullHD (1080p)<br /> 
						- Dos camaras (una fija y otra con operador)</p>
			
					</td>
					<td width="20"></td>
					<td valign="top" width="170" class="imagen">
						<img src="http://casamientosonline.com/comunicaciones/images/pic_170x230.jpg" />
					</td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="5" style="height: 26px;"></td>
				</tr>
				
				<tr>
					<td width="40"></td>
					<td colspan="3">
						<table class="datos_proveedor" align="center" bgcolor="#fff" width="100%" cellpadding="0" cellspacing="0" border="0" style=" background:#fff; color:#5e5e5e; font-size: 13px; font-family: arial, helvetica, sans-serif; line-height:20px; border-radius: 8px; ">
							<tr>
								<td width="26"></td>
								<td width="330">
									<p><img src="http://casamientosonline.com/comunicaciones/images/icon_phone.png" /><span>(0294) 4525044 (0294) 154587719 Int.</span><br />
									Whatsapp (0294) 15587719<br />
									<img src="http://casamientosonline.com/comunicaciones/images/icon_email.png" /><a href="" style="color:#5e5e5e;">reservas@apartdellago.com.ar</a></p>
								</td>
								<td width="20"></td>
								<td width="168">
									 <!--[if mso]>
					          <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{MAILTO_RESPONDER}" style="height:36px;v-text-anchor:middle;width:168px;" arcsize="5%" strokecolor="#EB7035" fillcolor="#EB7035">
					            <w:anchorlock/>
					            <center style="color:#ffffff;font-family: font-family: Arial, Helvetica, sans-serif; font-size:15px; font-weight: bold; ">Más información</center>
					          </v:roundrect> 
							 <![endif]-->
						     <a href="" style="background-color:#eb3d72; border:1px solid #EB7035; border-radius:3px; color:#fff; display:inline-block; font-family:sans-serif; font-size:15px; line-height:38px; text-align:center; text-decoration:none; width:168px; -webkit-text-size-adjust:none; mso-hide:all; font-weight: bold; box-shadow: 0 2px 0 #af4672;">Más información</a>
								</td>
								<td width="26"></td>
							</tr>
						</table>
					</td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="5" style="height: 40px;"></td>
				</tr>
				
			</table><!-- termina .comercial -->
			
			<table align="center" bgcolor="#fff" class="sep" cellpadding="0" cellspacing="0" style="background:#fff; ">
				<tr>
					<td style="height:10px;"></td>
				</tr>
			</table>
			
			<table align="center" bgcolor="#f7f7f7" class="editorial" cellpadding="0" cellspacing="0" border="0" style=" background:#f7f7f7; color:#5e5e5e; font-size: 15px; font-family: arial, helvetica, sans-serif; line-height:20px;">
				<tr>
					<td colspan="4" style="height: 40px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td colspan="3">
						<p class="epigrafe" style="color: #9f9f9f; font-size: 16px;margin-top:0; font-weight:bold; ">Foto y Video para Casamientos</p>
						<p class="titulo" style="color: #eb5e89; font-size: 34px; font-weight:bold; margin-top:0; padding-bottom: 20px; margin-bottom: 22px; border-bottom: 2px solid #d1d1d1; ">Foto y Video</p>
					</td>
					<td width="40"></td>
				</tr>
				
				<tr>
					<td width="40"></td>
					<td valign="top" class="contenido">
						<p style="margin-top: 0;"><strong>Tanto el vestido de novia, el ramo, el tocado y los accesorios, tienen que entrar en un todo armónico e integral. El ramo, tocado y 
			accesorios “hacen” a la novia, por eso tienen que entrar en íntima relación entre sí y decir algo en común.</strong></p>
					
						<p><strong>Ramo y Rosario:</strong> Para los casamientos católicos, mientras en su momento había una tajante división con respecto al ramo y al rosario (era ramo “o” rosario), hoy ya esa barrea empieza a desaparecer. El “o” se supo reemplazar por un “y”. Sí, hoy en día se usa llevar ambos elementos.</p>
						<p><strong>Tocado:</strong> Con flores naturales o de género, plumas, pedrería, metálica, cristales o mostacillas, materiales de todo tipo que nos permiten lograr y acabar el look de la novia con personalidad, estilo y elegancia. Por arriba o debajo de un rodete, al costado del pelo semi-recogido o el pelo suelto, hay muchas maneras de llevar un tocado de novia.</p>
			
					</td>
					<td width="20"></td>
					<td valign="top" width="170" class="imagen">
						<img src="http://casamientosonline.com/comunicaciones/images/pic_170x230.jpg" />
						<img src="http://casamientosonline.com/comunicaciones/images/spacer_10x10.png" />
						<img src="http://casamientosonline.com/comunicaciones/images/pic_170x110.jpg" />
					</td>
					<td width="40"></td>
				</tr>
					
				<tr>
					<td colspan="5" style="height: 40px;"></td>
				</tr>
				
			</table><!-- termina .editorial -->
			
			<table class="breves_triple_cont" align="center" bgcolor="#ffffff" width="650" cellpadding="0" cellspacing="0" border="0" style=" background:#fff; color:#5e5e5e; font-size: 15px; font-family: arial, helvetica, sans-serif; line-height:20px;">
				<tr>
					<td colspan="4" style="height: 40px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td width="170">
						<table class="breve_triple" bgcolor="#dff4ef;" style=" background: #dff4ef">
							<tr>
								<td colspan="3" style="height: 10px; "></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td width="150">
									<img src="http://casamientosonline.com/comunicaciones/images/pic_150x80.jpg" />
									<p style="font-size: 17px; font-weight: bold; color: #eb5e89; margin-bottom: 6px; text-align:center; margin-top: 8px;">Ambiente Jazz</p>
									<p style="font-size:12px; text-align:center; margin-top: 0; line-height: 17px;"><strong>¡Jazz en vivo!</strong><br />Marcá la diferencia en tu casamiento. Ofrecemos diferentes formaciones para las distintas ocasiones</p>
								</td>
								<td width="10"></td>
							</tr>
						</table>
					</td>
					<td width="20"></td>
					<td width="170">
						<table class="breve_triple" bgcolor="#dff4ef;" style=" background: #dff4ef">
							<tr>
								<td colspan="3" style="height: 10px; "></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td width="150">
									<img src="http://casamientosonline.com/comunicaciones/images/pic_150x80.jpg" />
									<p style="font-size: 17px; font-weight: bold; color: #eb5e89; margin-bottom: 6px; text-align:center; margin-top: 8px;">Ambiente Jazz</p>
									<p style="font-size:12px; text-align:center; margin-top: 0; line-height: 17px;"><strong>¡Jazz en vivo!</strong><br />Marcá la diferencia en tu casamiento. Ofrecemos diferentes formaciones para las distintas ocasiones</p>
								</td>
								<td width="10"></td>
							</tr>
						</table>
					</td>
					<td width="20"></td>
					<td width="170">
						<table class="breve_triple" bgcolor="#dff4ef;" style=" background: #dff4ef">
							<tr>
								<td colspan="3" style="height: 10px; "></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td width="150">
									<img src="http://casamientosonline.com/comunicaciones/images/pic_150x80.jpg" />
									<p style="font-size: 17px; font-weight: bold; color: #eb5e89; margin-bottom: 6px; text-align:center; margin-top: 8px;">Ambiente Jazz</p>
									<p style="font-size:12px; text-align:center; margin-top: 0; line-height: 17px;"><strong>¡Jazz en vivo!</strong><br />Marcá la diferencia en tu casamiento. Ofrecemos diferentes formaciones para las distintas ocasiones</p>
								</td>
								<td width="10"></td>
							</tr>
						</table>
					</td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="4" style="height: 40px;"></td>
				</tr>
			</table> <!-- termina .breves_triple_cont -->
			
			<table class="breve_simple_cont" align="center" bgcolor="#ffffff" width="650" cellpadding="0" cellspacing="0" border="0" style=" background:#fff; color:#5e5e5e; font-size: 15px; font-family: arial, helvetica, sans-serif; line-height:20px;">
				<tr>
					<td width="40"></td>
					<td>
						<table class="breve_triple" bgcolor="#dff4ef;" style=" background: #dff4ef">
							<tr>
								<td colspan="5" height="20px;"></td>
							</tr>
							<tr>	
								<td width="18"></td>	
								<td width="150">
									<img src="http://casamientosonline.com/comunicaciones/images/pic_150x170.jpg" />
								</td>	
								<td width="20"></td>	
								<td width="360">
									<p class="titulo" style="color: #eb5e89; font-size: 24px; font-weight:bold; margin-top:0; margin-bottom: 12px; ">Ambiente Jazz</p>
									<p class="titulo" style="font-size: 14px; font-weight:bold; margin-top:0; margin-bottom: 12px; "><strong>¡Jazz en vivo para eventos!</strong>
				Marcá la diferencia en tu casamiento.
				Ofrecemos diferentes formaciones y banda para las distintas ocasiones, según corresponda.</p>
								</td>
								<td width="20"></td>	
							</tr>
							<tr>
								<td colspan="5" height="14px;"></td>
							</tr>
						</table>
					</td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 20px;"></td>
				</tr>
			</table><!-- Termina .breve_simple -->
			
			<table width="650" align="center" id="footer" style="font-size: 15px; font-family: arial, helvetica, sans-serif; line-height:20px; background:#fff;">
				<tr>
					<td colspan="3">
						<p style="text-align:center; "><a href="" style="color:#5e5e5e; text-decoration:none; ">Guía de proveedores</a><span style="color: #ccc; border-right:1px solid #ddd; margin: 0 10px;"></span><a style="color:#5e5e5e; text-decoration:none;" href="">Enviar a un amigo</a><span style="color: #ccc; border-right:1px solid #ddd; margin: 0 10px;"></span><a style="color:#5e5e5e;  text-decoration:none;" href="">Ver infonovias anteriores</a></p>
					</td>	
				</tr>	
				<tr>
					<td colspan="3" style="height: 10px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td style="height: 1px; border-bottom:1px solid #ddd;"></td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="3" align="center"><img src="http://casamientosonline.com/comunicaciones/images/logo_col.png" /></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 15px;"></td>
				</tr>
				<tr>
					<td width="40"></td>
					<td style="height: 1px; border-bottom:1px solid #ddd;"></td>
					<td width="40"></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 10px;"></td>
				</tr>
				<tr>
					<td colspan="3">
						<table width="650">
							<tr>
								<td align="center">
									<a target="_blank" href="http://www.facebook.com/casamientos"><img src="http://casamientosonline.com/comunicaciones/images/icon_infonovias_fb.png" /></a>
									<a target="_blank" href="http://www.twitter.com/casamientos_ol"><img src="http://casamientosonline.com/comunicaciones/images/icon_infonovias_twitter.png" /></a>
									<a target="_blank" href="https://www.instagram.com/casamientosonline/"><img src="http://casamientosonline.com/comunicaciones/images/icon_infonovias_instagram.png" /></a>
									<a target="_blank" href="http://pinterest.com/casamientos/"><img src="http://casamientosonline.com/comunicaciones/images/icon_infonovias_pinterest.png" /></a>
									<a target="_blank" href="http://www.youtube.com/casamientosonline"><img src="http://casamientosonline.com/comunicaciones/images/icon_infonovias_youtube.png" /></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="height: 10px;"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>		

</body>
</html>







